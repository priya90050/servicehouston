package quacito.houston.adapter;

import java.util.ArrayList;


import quacito.houston.Flow.servicehouston.R;
import quacito.houston.model.ImgDto;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class GalleryImageAdapter  extends BaseAdapter
{
	LayoutInflater inflater;
	Context context;
	ArrayList<ImgDto>list = new ArrayList<ImgDto>();
	int which=0;

	public GalleryImageAdapter(Context context,ArrayList<ImgDto> list)    //0 for singale and 1 for maltipal 
	{
		this.context=context;
		this.list=list;
	}

	@Override
	public int getCount()
	{
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub

		return list.size();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		// TODO Auto-generated method stub
		inflater=LayoutInflater.from(context);
		convertView=inflater.inflate(R.layout.helpimage, null);
		ImageView img=(ImageView)convertView.findViewById(R.id.imgHelp);
		if(list.get(position).getImageBitMap()==null)
		{
			img.setImageDrawable(context.getResources().getDrawable(R.drawable.noimage));
		}
		else
		{
			img.setImageBitmap(list.get(position).getImageBitMap());

		}
		return convertView;

	}
}
