package quacito.houston.adapter;

import java.util.ArrayList;

import quacito.houston.CommonUtilities.CheckableRelativeLayout;
import quacito.houston.DBhelper.HoustonFlowFunctions;
import quacito.houston.Flow.servicehouston.R;

import quacito.houston.model.AgreementMailIdDTO;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class EmailAdapter extends ArrayAdapter<AgreementMailIdDTO> {

	View row;
	ArrayList<AgreementMailIdDTO> emailAddressList;
	int resLayout;
	ListView listViewEmail;
	Context context;
	LayoutInflater inflater;
  
	public EmailAdapter(Context context, int textViewResourceId, ArrayList<AgreementMailIdDTO> emailAddressList,ListView listViewEmail) {
	    super(context, textViewResourceId, emailAddressList);
	    this.emailAddressList = emailAddressList;
	    resLayout = textViewResourceId;
	    this.context = context;
	    this.listViewEmail=listViewEmail;
	}
    
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
	    row = convertView;
	   
	    if(row == null)
	    {   // inflate our custom layout. resLayout == R.layout.row_team_layout.xml
	    	inflater =  LayoutInflater.from(context);
			convertView = inflater.inflate(R.layout.row_team_layout, null);
	        LayoutInflater ll = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        row = ll.inflate(resLayout, parent, false);
	   
	        AgreementMailIdDTO item = emailAddressList.get(position); // Produce a row for each Team.
	        // final CheckBox checkBox;
	        //final CheckableRelativeLayout checkableRelativeLayout;
	   
	       if(item != null)
	       {   // Find our widgets and populate them with the Team data.
	    	
	    	listViewEmail.isEnabled();
	        TextView txtemailAddress = (TextView) row.findViewById(R.id.listview_TeamWins);                                      
	        if(txtemailAddress != null)
	        txtemailAddress.setText(item.getEmailId());
	        listViewEmail.setItemChecked( position, item.isSelected() );
	       }
	      }
	      return row;
	
	}	
	@Override
	public boolean isEnabled(int position) {
	    if(position == 0) {
	        return false;
	    }
	    return true;
	}
}
