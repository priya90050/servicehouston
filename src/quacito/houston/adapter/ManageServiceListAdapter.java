package quacito.houston.adapter;
import java.util.ArrayList;
import quacito.houston.Flow.servicehouston.R;

import quacito.houston.model.GeneralInfoDTO;
import quacito.houston.model.UserDTO;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.text.Html;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
public class ManageServiceListAdapter extends BaseAdapter 
{
	ArrayList<GeneralInfoDTO> serviceList;
	Activity context;
	LayoutInflater inflater;
	Spinner spnserviceBranch;
	UserDTO userDTO;
	public ManageServiceListAdapter(ArrayList<GeneralInfoDTO> serviceList,Activity context,UserDTO userDTO)
	{
		super();
		this.serviceList = serviceList;
		this.context = context;
		this.userDTO=userDTO;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return serviceList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		inflater=LayoutInflater.from(context);
		convertView=inflater.inflate(R.layout.service_manager_list_adapter1,null);

		//View view = super.getView(position, convertView, parent);  
		if (position % 2 == 1) 
		{
			convertView.setBackgroundColor(Color.WHITE);  
		} 
		else 
		{
			convertView.setBackgroundColor(Color.LTGRAY);  
		}

		TextView txtCurrentBalance,txtServiceAttribute,txtInvoiceAmount,txtSpecialInstruction,txtCompanyName,txtServiceCode,txtWorkOrder,txtDuration,txtFirstName,txtLastName,txtBranch,txtCity,txtState,txtOrderNo,txtEmailId,txtserviceCode,txtAccountNumber,txtaddress,txtzipCode,txttechName,txtServiceDate,txtResetReason;
		ImageButton imgSearch;
		LinearLayout resetReasonLayout;
		resetReasonLayout=(LinearLayout)convertView.findViewById(R.id.resetReasonLayout);
		spnserviceBranch=(Spinner)convertView.findViewById(R.id.spnserviceBranch);
		txtResetReason=(TextView)convertView.findViewById(R.id.txtReset_reason);
		txtFirstName=(TextView)convertView.findViewById(R.id.txtfirstName);
		txtAccountNumber=(TextView)convertView.findViewById(R.id.txtAccountNumber);
		txtLastName=(TextView)convertView.findViewById(R.id.txtLastName);
		txtaddress=(TextView)convertView.findViewById(R.id.txtAddress);
		txtEmailId=(TextView)convertView.findViewById(R.id.txtEmailId);
		txtBranch=(TextView)convertView.findViewById(R.id.txtBranch);
		txtServiceDate=(TextView)convertView.findViewById(R.id.txtServiceDate);
		txtInvoiceAmount=(TextView)convertView.findViewById(R.id.txtInvoiceAmount);
		
		txtCurrentBalance=(TextView)convertView.findViewById(R.id.txtCurrentBalance);
		txtServiceAttribute=(TextView)convertView.findViewById(R.id.txtServiceAttribute);
		
		txttechName=(TextView)convertView.findViewById(R.id.txtTech_Name);
		txtzipCode=(TextView)convertView.findViewById(R.id.txtZipcode);
		txtServiceCode=(TextView)convertView.findViewById(R.id.txtServiceCode);
		txtDuration=(TextView)convertView.findViewById(R.id.txtDuration);
		txtWorkOrder=(TextView)convertView.findViewById(R.id.txtWork_Order);
		txtCompanyName=(TextView)convertView.findViewById(R.id.txtCompanyName);
		
		txtOrderNo=(TextView)convertView.findViewById(R.id.txtOrderNo);
		txtCity=(TextView)convertView.findViewById(R.id.txtCity);
		txtState=(TextView)convertView.findViewById(R.id.txtState);
		txtSpecialInstruction=(TextView)convertView.findViewById(R.id.txtSpecialInstruction);
		
		txtCurrentBalance.setText(serviceList.get(position).getLocationBalance());
		txtInvoiceAmount.setText(serviceList.get(position).getInv_value());
		txtServiceAttribute.setText(serviceList.get(position).getService_Attribute());
		txtSpecialInstruction.setText(serviceList.get(position).getSpecialInstruction());
		txtFirstName.setText(serviceList.get(position).getFirst_Name());
		txtLastName.setText(serviceList.get(position).getLast_Name());
		txtEmailId.setText(serviceList.get(position).getEmail_Id());
		txtAccountNumber.setText(serviceList.get(position).getAccountNo());
		
		txtaddress.setText(serviceList.get(position).getAddress_Line1()+","+" "+serviceList.get(position).getCity()
		+","+" "+serviceList.get(position).getState()+" "+serviceList.get(position).getZipCode());
		
		txtCity.setText(serviceList.get(position).getCity());
		txtState.setText(serviceList.get(position).getState());
	
		txttechName.setText(serviceList.get(position).getFullName());
	    txtzipCode.setText(serviceList.get(position).getZipCode());
	    txtBranch.setText(serviceList.get(position).getBranch());
		txtServiceCode.setText(serviceList.get(position).getServiceCode());
		//txtDuration.setText(serviceList.get(position).getDepartment());
		txtWorkOrder.setText(serviceList.get(position).getOrder_Number());
		txtCompanyName.setText(serviceList.get(position).getCompanyName());
		
	    txtServiceDate.setText(serviceList.get(position).getServiceDate()+" "+serviceList.get(position).getServiceTime());
		if(serviceList.get(position).getServiceStatus().equals("Reset"))
		{
			String reason=serviceList.get(position).getService_Status_Reason();
			txtResetReason.setText(Html.fromHtml("<u>"+reason+"</u>"));
			resetReasonLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					showTechnicianCommentBoxDialog(serviceList.get(position));
				}
			});
		}
		else
		{
			resetReasonLayout.setVisibility(View.GONE);
		}

		return convertView;
	}
	public void showTechnicianCommentBoxDialog(GeneralInfoDTO dto)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(context.getResources().getString(R.string.RESET_WORK_ORDER));
		// Set up the input
		final TextView input = new TextView(context);
		// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
		//input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
		input.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
		input.setBackgroundResource(android.R.drawable.editbox_background_normal);
		input.setMinHeight(300);
		input.setSingleLine(false);
		input.setGravity(Gravity.TOP);
		input.setText(Html.fromHtml("<b>"+dto.getService_Status_Reason()+"</b>")+"\n\n"+dto.getResetComment());
		builder.setView(input);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
		{ 
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.cancel();
			}
		});
		builder.show();
	}
}
