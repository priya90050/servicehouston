package quacito.houston.CommonUtilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class SharedPreference {

	public static final String SERVICE_FLOW_PREF_FILE = "service_flow_pref";
	public static final String LOGIN_PREF = "login_pref";
	public static final String LOGOUT_PREF = "LOGOUT_PREF";
	public static final String USER_NAME="userName";
	public static final String USER_PASSWORD="password";
	public static final String USER_DETAIL="user_detail";
	public static final String COMPLETED_BY="completed_by";
	
	
	public static final String USER_ID="userID";
	public static final String EMPLOYEE_USER_NAME="empUserName";
	public static final String INPROGRESS_SERVICE="inprogress_service";
	// public static final String TERMS_OF_USE_LOSS_REPORT="terms_of_use";
	// public static final String SWACHHTA_PLEDGE_PREF="none";
	// public static final String TERMS_OF_USE_LOSS_REPORT="terms_of_use";
	
	//Set value in Shared prefrences
	public static void setSharedPrefer(Context context, String which,String value)
	{
		SharedPreferences preference = context.getSharedPreferences(SERVICE_FLOW_PREF_FILE, Context.MODE_APPEND);
		Editor ed = preference.edit();
		ed.putString(which, value);
		ed.commit();
	}
	
	//Get value from Shared prefrences
	public static String getSharedPrefer(Context context, String which)
	{
		SharedPreferences cureentMediaPrf = context.getSharedPreferences(SERVICE_FLOW_PREF_FILE, Context.MODE_APPEND);
		String password = cureentMediaPrf.getString(which, "0");
		return password;
	}
	
	public static void setSharedPrefer(Context context, String which,String value,int Mode)
	{
		SharedPreferences preference = context.getSharedPreferences(SERVICE_FLOW_PREF_FILE, Mode);
		Editor ed = preference.edit();
		ed.putString(which, value);
		ed.commit();
	}
	
	//Get value from Shared prefrences
	public static String getSharedPrefer(Context context, String which,int Mode)
	{
		SharedPreferences cureentMediaPrf = context.getSharedPreferences(SERVICE_FLOW_PREF_FILE, Mode);
		String password = cureentMediaPrf.getString(which, "0");
		return password;
	}
	
	
	//Check Downloaded Version
	public static boolean isDownload(String oldverion, String newversion) {
		int old = parseInt(oldverion);
		int newv = parseInt(newversion);
		if (newv > 0 && old < newv) {
			return true;
		}
		return false;
	}
	
	
	public static int parseInt(String s) {
		try {
			return Integer.parseInt(s.trim().replace(" ", ""));
		} catch (NumberFormatException e) {
			return 0;
		}
	}
	public static boolean isShowProgress(String old) {
		int oldVersion = parseInt(old);
		if (oldVersion == 0)
			return true;
		return false;
	}
}
