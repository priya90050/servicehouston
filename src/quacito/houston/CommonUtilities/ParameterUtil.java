package quacito.houston.CommonUtilities;

public class ParameterUtil {
	public static String newEmail = "newEmail";
	public static String Email = "Email";
	public static String Role = "Role";
	public static String isSync = "isSync";
	public static String ServiceNameType = "ServiceNameType";
	// Parameters for service table SR_tbl_AddressInfo
	public static String Row_Id = "Row_Id";
	public static String Assigned_WO = "Assigned_WO";
	public static String ExpirationDate = "ExpirationDate";
	public static String CheckFrontImage = "CheckFrontImage";
	public static String CheckBackImage = "CheckBackImage";
	public static String Id = "Id";
	public static String ServiceID_new = "ServiceID_new";
	public static String Commercial = "Commercial";
	public static String Residential = "Residential";
	// public static String ServiceID_New="ServiceID_New";

	public static String Address_Line1 = "Address_Line1";
	public static String Address_Line2 = "Address_Line2";
	public static String isCompleted = "isCompleted";
	public static String City = "City";
	public static String State = "State";
	public static String ZipCode = "ZipCode";
	public static String Poc_FName = "Poc_FName";
	public static String Poc_LName = "Poc_LName";
	public static String BillingAddress = "BillingAddress";
	public static String BAddress_Line1 = "BAddress_Line1";
	public static String BAddress_Line2 = "BAddress_Line2";
	public static String BCity = "BCity";
	public static String BState = "BState";
	public static String BZipCode = "BZipCode";
	public static String ServiceLatitude = "ServiceLatitude";
	public static String ServiceLongitude = "ServiceLongitude";
	public static String Create_By = "Create_By";
	public static String Update_By = "Update_By";
	public static String Create_Date = "Create_Date";
	public static String Update_Date = "Update_Date";
	public static String Created_By = "Created_By";
	public static String Updated_By = "Updated_By";
	public static String Create_date = "Create_date";
	public static String SDate = "SDate";
	public static String Edate = "Edate";
	public static String EmpId = "EmpId";

	// parameter for SR_tbl_GeneralInfo
	// public static String ServiceID_new="ServiceID_new";
	public static String ResetComment = "ResetComment";
	public static String First_Name = "First_Name";
	public static String Last_Name = "Last_Name";
	public static String FullName = "FullName";
	public static String Email_Id = "Email_Id";
	public static String SecondaryEmail = "SecondaryEmail";
	public static String AccountNo = "AccountNo";
	public static String Home_Phone = "Home_Phone";
	public static String Cell_Phone = "Cell_Phone";
	public static String Salesperson_ID = "Salesperson_ID";
	public static String ServiceDate = "ServiceDate";
	public static String ServiceStatus = "ServiceStatus";
	public static String Notes = "Notes";
	public static String ServiceCode = "ServiceCode";
	public static String department = "department";
	public static String BusinessName = "BusinessName";
	public static String Branch = "Branch";
	public static String TaxCode = "TaxCode";
	public static String EstimateTime = "EstimateTime";
	public static String Arrival_Duration = "Arrival_Duration";
	public static String ProblemDescription = "ProblemDescription";
	public static String PSPCustomer = "PSPCustomer";
	public static String BeforeImage = "BeforeImage";
	public static String AfterImage = "AfterImage";
	public static String AudioFile = "AudioFile";
	public static String Inspection_Medium = "Inspection_Medium";
	public static String Ref_Source = "Ref_Source";
	public static String Order_Number = "Order_Number";
	public static String Route_Name = "Route_Name";
	public static String Route_Number = "Route_Number";
	public static String KeyMap = "KeyMap";
	public static String Service_Tech_Num = "Service_Tech_Num";
	public static String Service_Status_Reason = "Service_Status_Reason";
	public static String CompanyName = "CompanyName";
	public static String ServiceDescription = "ServiceDescription";
	public static String OtherInstruction = "OtherInstruction";
	public static String specialInstruction = "specialInstruction";
	public static String Total = "Total";
	public static String LocationBalance = "LocationBalance";
	public static String TimeInDateTime = "TimeInDateTime";
	public static String Branch_ID = "Branch_ID";
	public static String Inv_value = "Inv_value";
	public static String Prod_value = "Prod_value";
	public static String Prv_Bal = "Prv_Bal";
	public static String Tax_value = "Tax_value";
	public static String Office_Note_ByRep = "Office_Note_ByRep";
	public static String Note_Status = "Note_Status";
	public static String Note_Closed_Text = "Note_Closed_Text";
	public static String Note_Closed_Date = "Note_Closed_Date";
	public static String Payment_Collection_Status = "Payment_Collection_Status";
	public static String Collection_Text = "Collection_Text";
	public static String Collection_Date = "Collection_Date";
	public static String Flag = "Flag";
	public static String IsElectronicSignatureAvailable = "IsElectronicSignatureAvailable";
	public static String OpenWONotificationFlag = "OpenWONotificationFlag";
	public static String TimeOutDateTime = "TimeOutDateTime";
	public static String App_Info = "App_Info";
	public static String Mobile_Info = "Mobile_Info";
	public static String ServiceClass = "ServiceClass";
	// for SR_tbl_Prevention
	public static String PestPrevention_ID = "PestPrevention_ID";
	public static String PreventationID = "PreventationID";
	// public static String ServiceID_new="ServiceID_new";
	public static String ProductCheck = "ProductCheck";
	public static String PercentPest = "PercentPest";
	public static String Amount = "Amount";
	public static String Target = "Target";
	public static String Unit = "Unit";
	public static String SAA = "SAA";
	public static String Method = "Method";
	public static String OtherText = "OtherText";
	public static String Service_Attribute = "Service_Attribute";
	// public static String Create_By="Create_By";
	// public static String Update_By="Update_By";
	// public static String Create_Date="Create_Date";
	// public static String Update_Date="Update_Date";

	// for SR_tbl_PreventionMaster
	// public static String PreventationID="PreventationID";
	public static String Product = "Product";
	// public static String PercentPest="PercentPest";
	public static String Status = "Status";

	public static String EPA_RegNo = "EPA_RegNo";
	// public static String Unit="Unit";

	// for SR_tbl_ResidentialPest
	public static String ResidentialId = "ResidentialId";
	// public static String ServiceID_new="ServiceID_new";
	public static String ServiceFor = "ServiceFor";
	public static String AreasInspected = "AreasInspected";
	public static String InsectActivity = "InsectActivity";
	public static String Ants = "Ants";
	public static String OtherPest = "OtherPest";
	public static String OutsideOnly = "OutsideOnly";
	public static String PestActivity = "PestActivity";
	public static String Fence = "Fence";
	public static String BackYard = "BackYard";
	public static String FrontYard = "FrontYard";
	public static String ExteriorPerimeter = "ExteriorPerimeter";
	public static String Roofline = "Roofline";
	public static String Garage = "Garage";
	public static String Attic = "Attic";
	public static String InteriorHouse = "InteriorHouse";
	public static String PestControl = "PestControl";
	public static String PestControlServices = "PestControlServices";
	public static String PlusTermite = "PlusTermite";

	public static String FencePest = "FencePest";
	public static String FenceActivity = "FenceActivity";
	public static String NumberPest = "NumberPest";
	public static String NumberText = "NumberText";
	public static String NumberActivity = "NumberActivity";
	public static String NumberActivitytext = "NumberActivitytext";
	public static String WoodPest = "WoodPest";
	public static String WoodOption = "WoodOption";
	public static String WoodActivity = "WoodActivity";
	public static String ExteriorPest = "ExteriorPest";
	public static String ExteriorActivity = "ExteriorActivity";
	public static String AtticPest = "AtticPest";
	public static String AtticActivity = "AtticActivity";
	public static String InteriorPest = "InteriorPest";
	public static String InteriorActivity = "InteriorActivity";
	public static String PlusRodent = "PlusRodent";
	public static String RoofRodent = "RoofRodent";
	public static String RoofNeeds = "RoofNeeds";

	public static String RoofVentRodent = "RoofVentRodent";
	public static String RoofVentNeeds = "RoofVentNeeds";
	public static String Flashing = "Flashing";
	public static String FlashingNeeds = "FlashingNeeds";
	public static String Fascia = "Fascia";
	public static String FasciaNeeds = "FasciaNeeds";
	public static String Siding = "Siding";
	public static String SidingNeeds = "SidingNeeds";
	public static String Breezway = "Breezway";
	public static String BreezwayNeeds = "BreezwayNeeds";
	public static String GarageDoor = "GarageDoor";
	public static String GarageDoorNeeds = "GarageDoorNeeds";
	public static String DryerVent = "DryerVent";
	public static String DryerVentNeeds = "DryerVentNeeds";
	public static String NumberLive = "NumberLive";
	public static String NumberLiveNeeds = "NumberLiveNeeds";
	public static String NumberSnap = "NumberSnap";
	public static String NumberSnapNeeds = "NumberSnapNeeds";

	public static String PlusMosquito = "PlusMosquito";
	public static String PlusMosquitoDetail = "PlusMosquitoDetail";
	public static String DurationTime = "DurationTime";
	public static String SystemRuns = "SystemRuns";
	public static String TreeBranches = "TreeBranches";
	public static String Firewood = "Firewood";
	public static String DebrisCrawl = "DebrisCrawl";
	public static String ExcessivePlant = "ExcessivePlant";
	public static String Soil = "Soil";
	public static String WoodSoil = "WoodSoil";
	public static String DebrisRoof = "DebrisRoof";
	public static String StandingWater = "StandingWater";
	public static String MoistureProblem = "MoistureProblem";
	public static String Openings = "Openings";
	public static String ExcessiveGaps = "ExcessiveGaps";
	public static String LeakyPlumbing = "LeakyPlumbing";
	public static String GarbageCans = "GarbageCans";
	public static String MoistureDamaged = "MoistureDamaged";

	public static String GroceryBags = "GroceryBags";
	public static String PetFood = "PetFood";
	public static String RecycledItems = "RecycledItems";
	public static String ExcessiveStorage = "ExcessiveStorage";
	public static String TechnicianComments = "TechnicianComments";
	public static String TimeIn = "TimeIn";
	public static String Timeout = "Timeout";
	// public static String TimeInDateTime="TimeInDateTime";
	public static String ServiceTime = "ServiceTime";
	public static String Technician = "Technician";
	public static String EmpNo = "EmpNo";
	public static String Customer = "Customer";
	public static String Date = "Date";
	public static String PaymentType = "PaymentType";
	// public static String Amount="Amount";
	public static String CheckNo = "CheckNo";
	public static String LicenseNo = "LicenseNo";
	public static String Signature = "Signature";
	public static String ServiceSignature = "ServiceSignature";
	public static String AgreementPath = "AgreementPath";
	// public static String Create_By="Create_By";
	// public static String Update_By="Update_By";
	// public static String Create_Date="Create_Date";
	// public static String Update_Date="Update_Date";

	// for tbl_Employee

	public static String EmpName = "EmpName";
	public static String EmployeeId = "EmployeeId";
	public static String Username = "UserName";
	public static String Username_employee = "Username";
	public static String Password = "Password";
	public static String FirstName = "FirstName";
	public static String MiddleName = "MiddleName";
	public static String LastName = "LastName";
	public static String RoleId = "RoleId";
	public static String RoleName = "RoleName";
	public static String Emailid = "Emailid";
	public static String PestPackId = "PestPackId";
	public static String DepartmentId = "DepartmentId";
	// public static String AccountNo="AccountNo";
	public static String HomePhone = "HomePhone";
	public static String CellPhone = "CellPhone";
	public static String Address1 = "Address1";
	public static String Address2 = "Address2";
	public static String SupervisorId = "SupervisorId";
	public static String InactiveDate = "InactiveDate";
	public static String LicenceNo = "LicenceNo";
	// public static String City="City";
	// public static String State="State";
	// public static String ZipCode="ZipCode";
	// public static String Status="Status";

	// chemicals
	// private String Amount="Amount";
	// private String OtherText="OtherText";
	private String PercentPes = "PercentPes";
	// private String PestPrevention_ID="PestPrevention_ID";
	// private String PreventationID="PreventationID";
	// private String Product="Product";
	// private String ProductCheck="ProductCheck";
	// private String ServiceID_new="ServiceID_new";
	// private String Status="Status";
	// private String Target="Target";
	// private String Unit="Unit";

	// for agreement table
	// Email Agreement
	public static String isSelected = "isSelected";
	public static String Member_Id = "Member_Id";
	public static String AgreementId = "AgreementId";
	public static String EmailId = "EmailId";
	public static String MailType = "MailType";
	public static String Subject = "Subject";
	public static String IsCustomerEmail = "IsCustomerEmail";

	// CREW MEMBER
	public static String Route_ID = "Route_ID";
	public static String CrewMember_ID = "CrewMember_ID";
	public static String Tech_Name = "Tech_Name";
	// public static String Status="Status";

	// log table
	public static String SID = "SID";
	public static String GeneralSR = "GeneralSR";
	public static String Residetial_Insp = "Residetial_Insp";
	public static String Commercial_Insp = "Commercial_Insp";
	public static String Chemical = "Chemical";

	public static String AgreeMentMailID = "AgreeMentMailID";
	public static String Crew_Member = "Crew_Member";

	// commercial table
	public static String CommercialId = "CommercialId";
	// public static String ServiceID_new="";
	// public static String ServiceFor="";
	public static String AreasInspectedInterior = "AreasInspectedInterior";
	public static String AreasInspectedExterior = "AreasInspectedExterior";
	/*
	 * public static String InsectActivity=""; public static String Ants="";
	 * public static String OtherPest=""; public static String PestActivity="";
	 */
	public static String Interior = "Interior";
	public static String Outsidef_Perimeter = "Outsidef_Perimeter";
	public static String Dumpster = "Dumpster";
	public static String Dining_Room = "Dining_Room";
	public static String Common_Areas = "Common_Areas";
	public static String Kitchen = "Kitchen";
	public static String Dry_Storage = "Dry_Storage";
	public static String Dish_Washing = "Dish_Washing";
	public static String Roof_Tops = "Roof_Tops";
	public static String Wait_Stations = "Wait_Stations";
	public static String Drop_Ceiling = "Drop_Ceiling";
	public static String Planters = "Planters";
	public static String Warehouse_Storage = "Warehouse_Storage";
	/*
	 * public static String PestControl=""; public static String
	 * PestControlServices=""; public static String PlusTermite="";
	 */
	public static String SignOfTermiteMS = "SignOfTermiteMS";
	public static String SignOfTermiteActivityMS = "SignOfTermiteActivityMS";
	public static String SignOfTermiteText = "SignOfTermiteText";
	public static String InstallMS = "InstallMS";
	public static String InstallMSText = "InstallMSText";
	public static String SignOfTermitePS = "SignOfTermitePS";
	public static String SignOfTermiteActivityPS = "SignOfTermiteActivityPS";
	public static String SignOfTermiteIS = "SignOfTermiteIS";
	public static String SignOfTermiteActivityIS = "SignOfTermiteActivityIS";
	public static String ReplacedMS = "ReplacedMS";
	public static String ReplacedActivityTextMS = "ReplacedActivityTextMS";
	public static String ReplacedTextMS = "ReplacedTextMS";
	public static String TreatedPS = "TreatedPS";
	public static String TreatedActivityPS = "TreatedActivityPS";
	public static String TreatedSP = "TreatedSP";
	public static String TreatedActivitySP = "TreatedActivitySP";
	public static String TreatedCC = "TreatedCC";
	public static String TreatedActivityCC = "TreatedActivityCC";
	// public static String PlusRodent="";
	public static String InspectedRodentStation = "InspectedRodentStation";
	public static String txtInspectedRodentStation = "txtInspectedRodentStation";
	public static String CleanedRodentStation = "CleanedRodentStation";
	public static String IsCleanedRodentStation = "IsCleanedRodentStation";
	public static String InstallRodentBaitStation = "InstallRodentBaitStation";
	public static String IsInstallRodentBaitStation = "IsInstallRodentBaitStation";
	public static String txtInstallRodentBaitStation = "txtInstallRodentBaitStation";
	public static String ReplacedBait = "ReplacedBait";
	public static String IsReplacedBait = "IsReplacedBait";
	public static String SNoReplacedBait = "SNoReplacedBait";
	public static String ReplacedBaitStation = "ReplacedBaitStation";
	public static String IsReplacedBaitStation = "IsReplacedBaitStation";
	public static String txtReplacedBaitStation = "txtReplacedBaitStation";
	public static String SNoReplacedBaitStation = "SNoReplacedBaitStation";
	public static String InspectedRodentTraps = "InspectedRodentTraps";
	public static String IsInspectedRodentTraps = "IsInspectedRodentTraps";
	public static String CleanedRodentTrap = "CleanedRodentTrap";
	public static String IsCleanedRodentTrap = "IsCleanedRodentTrap";
	public static String InstallRodentTrap = "InstallRodentTrap";
	public static String IsInstallRodentTrap = "IsInstallRodentTrap";
	public static String txtInstallRodentTrap = "txtInstallRodentTrap";
	public static String ReplacedRodentTrap = "ReplacedRodentTrap";
	public static String IsReplacedRodentTrap = "IsReplacedRodentTrap";
	public static String txtReplacedRodentTrap = "txtReplacedRodentTrap";
	public static String SNoReplacedRodentTrap = "SNoReplacedRodentTrap";
	public static String SealedEntryPoint = "SealedEntryPoint";
	public static String IsSealedEntryPoint = "IsSealedEntryPoint";
	public static String InspectedSnapTrap = "InspectedSnapTrap";
	public static String txtInspectedSnapTrap = "txtInspectedSnapTrap";
	public static String InspectedLiveCages = "InspectedLiveCages";
	public static String txtInspectedLiveCages = "txtInspectedLiveCages";
	// public static String InspectedSnapTraps="InspectedSnapTraps";
	// public static String txtInspectedSnapTraps="txtInspectedSnapTraps";
	public static String RemovedSnapTraps = "RemovedSnapTraps";
	public static String txtRemovedSnapTraps = "txtRemovedSnapTraps";
	public static String RemovedLiveCages = "RemovedLiveCages";
	public static String txtRemovedLiveCages = "txtRemovedLiveCages";
	public static String RemovedRodentBaitStation = "RemovedRodentBaitStation";
	public static String txtRemovedRodentBaitStation = "txtRemovedRodentBaitStation";
	public static String RemovedRodentTrap = "RemovedRodentTrap";
	public static String txtRemovedRodentTrap = "txtRemovedRodentTrap";
	public static String SetSnapTrap = "SetSnapTrap";
	public static String txtSetSnapTrap = "txtSetSnapTrap";
	public static String LstSetSnapTrap = "LstSetSnapTrap";
	public static String SetLiveCages = "SetLiveCages";
	public static String txtSetLiveCages = "txtSetLiveCages";
	public static String LstSetLiveCages = "LstSetLiveCages";
	/*
	 * public static String PlusMosquitoDetail=""; public static String
	 * DurationTime=""; public static String SystemRuns="";
	 */
	public static String Recommendations = "Recommendations";
	/*
	 * public static String TechnicianComments=""; public static String
	 * TimeIn=""; public static String Timeout=""; public static String
	 * TimeInDateTime=""; public static String TimeOutDateTime=""; public static
	 * String Technician=""; public static String EmpNo=""; public static String
	 * Customer=""; public static String Date=""; public static String
	 * PaymentType=""; public static String Amount=""; public static String
	 * CheckNo=""; public static String LicenseNo=""; public static String
	 * Signature=""; public static String ServiceSignature=""; public static
	 * String Create_By=""; public static String Update_By=""; public static
	 * String Create_Date=""; public static String Update_Date=""; public static
	 * String AgreementPath="";
	 */

	// orlando residential params
	public static String FloridaResidentialServiceId = "FloridaResidentialServiceId";
	/*
	 * public static String ServiceID_new=""; public static String
	 * ServiceFor=""; public static String AreasInspected=""; public static
	 * String InsectActivity=""; public static String Ants=""; public static
	 * String OtherPest=""; public static String OutsideOnly=""; public static
	 * String PestActivity=""; public static String Fence=""; public static
	 * String BackYard=""; public static String FrontYard=""; public static
	 * String ExteriorPerimeter=""; public static String Roofline=""; public
	 * static String Garage=""; public static String Attic=""; public static
	 * String InteriorHouse="";
	 */
	public static String Basement = "Basement";

	// public static String PestControl="";
	// public static String PestControlServices="";

	// public static String PlusTermite="";
	// public static String FencePest="";
	// public static String FenceActivity="";
	public static String BuilderConstruction = "BuilderConstruction";
	public static String BuilderConstructionActivity = "BuilderConstructionActivity";
	public static String CrawlSpaceDoor = "CrawlSpaceDoor";
	public static String CrawlSpaceDoorActivity = "CrawlSpaceDoorActivity";
	/*
	 * public static String NumberPest=""; public static String NumberText="";
	 * public static String NumberActivity=""; public static String
	 * NumberActivitytext=""; public static String WoodPest="";
	 * 
	 * public static String WoodOption=""; public static String WoodActivity="";
	 * public static String ExteriorPest=""; public static String
	 * ExteriorActivity=""; public static String AtticPest=""; public static
	 * String AtticActivity=""; public static String InteriorPest=""; public
	 * static String InteriorActivity="";
	 */
	public static String TreatmentOfSchool = "TreatmentOfSchool";

	public static String MonitoringDevices = "MonitoringDevices";
	public static String NumMonitoringDevices = "NumMonitoringDevices";
	public static String BasementTermite = "BasementTermite";
	public static String NumBasementTermite = "NumBasementTermite";
	public static String AdditionMonitoringDevice = "AdditionMonitoringDevice";
	public static String NumAdditionMonitoringDevice = "NumAdditionMonitoringDevice";
	public static String MonitoringDeviceRemoved = "MonitoringDeviceRemoved";
	public static String NumMonitoringDeviceRemoved = "NumMonitoringDeviceRemoved";

	// get route crew
	// public static String Created_By="Created_By";
	public static String Created_Date = "Created_Date";
	public static String Route_Crew_ID = "Route_Crew_ID";
	// public static String Route_ID="Route_ID";
	// public static String Status="Status";
	public static String Technician_ID = "Technician_ID";
	// public static String Updated_By="Updated_By";
	public static String Updated_Date = "Updated_Date";

	// get all route
	public static String Class_ID = "Class_ID";
	// public static String Created_By="Created_By";
	// public static String Created_Date="Created_Date";
	public static String Lead_ID = "Lead_ID";
	public static String RouteMaster_ID = "RouteMaster_ID";
	// public static String Route_Name="Route_Name";
	// public static String Status="Status";

	// public static String Updated_By="Updated_By";
	// public static String Updated_Date="Updated_Date";
	
	//assigned lead for the day
	public static String AssignForDayLead_ID = "AssignForDayLead_ID";
	public static String Assign_Date="Assign_Date";
	// public static String Created_By="Created_By";
	//public static String Created_Date = "Created_Date";
	public static String Emp_ID = "Emp_ID";
	// public static String Route_ID="Route_ID";
	// public static String Status="Status";
	// public static String Updated_By="Updated_By";
	// public static String Updated_Date="Updated_Date";

	// get assigned crew member for day
	
	public static String AssignForDayCrewMember_ID = "AssignForDayCrewMember_ID";
	public static String AssignForDayLeadID="AssignForDayLeadID";
	// public static String Created_By="Created_By";
	//public static String Created_Date = "Created_Date";
	//public static String CrewMember_ID = "CrewMember_ID";
	// public static String Status="Status";
	// public static String Updated_By="Updated_By";
	// public static String Updated_Date="Updated_Date";
   
}
