package quacito.houston.CommonUtilities;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.json.JSONArray;
import quacito.houston.DBhelper.HoustonFlowFunctions;
import quacito.houston.model.EmployeeDTO;
import quacito.houston.Flow.servicehouston.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;

public class CommonFunction {
	//public static String IPAD_ABC = "http://staging.quacito.com/houston/";
	 public static String IPAD_ABC = "http://ipad-abc.com/";

	
	 public static String UPLOAD_CHECK_FRONT_IMAGE_URL = "http://ipad-abc.com/CheckImgHandler.ashx"; 
	 public static String UPLOAD_IMAGE_URL = "http://ipad-abc.com/IHandler.ashx"; 
	 public static String UPLOAD_RESIDENTIAL_SIGNATURE = "http://ipad-abc.com/SignHandler.ashx"; 
	 public static String UPLOAD_COMMERCIAL_SIGNATURE = "http://ipad-abc.com/ComSignHandler.ashx";
	 public static String UPLOAD_ORLANDO_SIGNATURE = "http://ipad-abc.com/ComSignHandler.ashx";
	 public static String DOWNLOAD_ORLANDO_SIGNATURE = "http://ipad-abc.com/ComSignHandler.ashx";
	 public static String DOWNLOAD_RESIDENTIAL_SIGNATURE = "http://www.ipad-abc.com/document/ResidentialSign/";
	 public static String DOWNLOAD_COMMERCIAL_SIGNATURE = "http://www.ipad-abc.com/document/CommercialSign/";
	 public static String DOWNLOAD_IMAGE_URL = "http://ipad-abc.com/document/Inspection_Image/";
	 public static String DOWNLOAD_CHECK_IMAGE_URL = "http://ipad-abc.com/document/CheckImage/";
	 
	/*public static String UPLOAD_CHECK_FRONT_IMAGE_URL = "http://staging.quacito.com/houston/CheckImgHandler.ashx";
	public static String UPLOAD_IMAGE_URL = "http://staging.quacito.com/houston/IHandler.ashx";
	public static String UPLOAD_SIGNATURE_IMAGE_URL = "http://staging.quacito.com/houston/SignHandler.ashx";
	public static String DOWNLOAD_SIGNATURE_IMAGE_URL = "http://staging.quacito.com/houston/document/ResidentialSign/";
	public static String DOWNLOAD_IMAGE_URL = "http://staging.quacito.com/houston/document/Inspection_Image/";
	public static String DOWNLOAD_CHECK_IMAGE_URL = "http://staging.quacito.com/houston/CheckImage/";
    */
	public static File file = null;
	public static File dir = null;
	public static File root = null;
	public static final String AUDIO_RECORDER_FOLDER = "ABCHouston";

	public static boolean isSdPresent() {

		return android.os.Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED);
	}

	// Get Audio file path
	public static String getAudioFilePath(String AudioFileName) {
		String filepath = Environment.getExternalStorageDirectory().getPath();
		File file = new File(filepath, AUDIO_RECORDER_FOLDER);
		if (!file.exists()) {
			file.mkdirs();
		}
		// AudioFileName = System.currentTimeMillis() +
		// AUDIO_RECORDER_FILE_EXT_MP4;
		String FilePath = file.getAbsolutePath() + "/" + AudioFileName;

		return (FilePath);
	}

	public static String removeFirstCharIF(String data, String startWith) {
		data = data.startsWith(startWith) ? data.substring(1) : data;
		return data;
	}

	public static String createImageName() {

		String ImageName = String.format("%d.jpg", System.currentTimeMillis());
		LoggerUtil.e("image name in createImageName", "" + ImageName);
		return ImageName;
	}

	// Get list of All Employee Names
	public static ArrayList<String> getemployeeNameList(Context context) {
		HoustonFlowFunctions serviceFlowFunctions = new HoustonFlowFunctions(
				context);
		JSONArray jsEmployeList = serviceFlowFunctions.getAllEmployee();
		ArrayList<EmployeeDTO> employList = new Gson().fromJson(
				jsEmployeList.toString(), new TypeToken<List<EmployeeDTO>>() {
				}.getType());

		ArrayList<String> employeeNamelist = new ArrayList<String>();
		for (int i = 0; i < employList.size(); i++) {
			employeeNamelist.add(employList.get(i).getEmpName());
		}
		return employeeNamelist;
	}

	// Set Employee names in Spinner
	public static ArrayAdapter<String> setEmployenameList(Context context) {
		HoustonFlowFunctions serviceFlowFunctions = new HoustonFlowFunctions(
				context);
		JSONArray jsEmployeList = serviceFlowFunctions.getAllEmployee();
		ArrayList<EmployeeDTO> employeeList = new Gson().fromJson(
				jsEmployeList.toString(), new TypeToken<List<EmployeeDTO>>() {
				}.getType());
		ArrayList<String> employeNamelist = new ArrayList<String>();

		for (int i = 0; i < employeeList.size(); i++) {
			try {
				employeNamelist.add(employeeList.get(i).getEmpName() + "");
			} catch (Exception e) {
				LoggerUtil.e("Error in for loop", e.toString());
			}
		}
		ArrayAdapter<String> employeeAdapter = new ArrayAdapter<String>(
				context, android.R.layout.simple_spinner_dropdown_item,
				employeNamelist);
		return employeeAdapter;
	}

	// Get File Location from external storege
	public static File getFileLocation(Context con, String fileName) {
		if (isSdPresent() == true) {
			root = Environment.getExternalStorageDirectory();
		} else {
			root = con.getFilesDir();
		}
		try {
			if (root.canWrite()) {
				dir = new File(root.getAbsoluteFile() + "/ABCHouston");
				if (dir.exists()) {
					file = new File(dir, fileName);
				} else {
					dir.mkdir();
					file = new File(dir, fileName);
				}
			}
		} catch (Exception e) {

		}

		return file;
	}

	public static void saveBitmap(Bitmap bmp, Context con, String imageName) {
		try {
			if (isSdPresent() == true) {
				root = Environment.getExternalStorageDirectory();
			} else {
				root = con.getFilesDir();
			}
			if (root.canWrite()) {
				dir = new File(root.getAbsoluteFile() + "/ABCHouston");
				if (dir.exists()) {
					file = new File(dir, imageName);
				} else {
					dir.mkdir();
					file = new File(dir, imageName);
				}
			}
			if (!dir.exists())
				dir.mkdirs();
			File file = new File(dir, imageName);
			FileOutputStream fOut = new FileOutputStream(file);
			bmp.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
			fOut.flush();
			fOut.close();
		} catch (Exception e) {
			LoggerUtil.e("Problem in file saving", e + "");
		}
	}

	public static boolean haveInternet(Context ctx) {

		NetworkInfo info = (NetworkInfo) ((ConnectivityManager) ctx
				.getSystemService(Context.CONNECTIVITY_SERVICE))
				.getActiveNetworkInfo();

		if (info == null || !info.isConnected()) {
			return false;
		}
		/*
		 * if (info.isRoaming()) { // here is the roaming option you can change
		 * it if you want to // disable internet while roaming, just return
		 * false return false; }
		 */
		return true;
	}

	// Get Systems Current date
	public static String getCurrentDate() {

		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Calendar cal = Calendar.getInstance();
		String date = dateFormat.format(cal.getTime());
		return date;
	}

	// Get Systems Curent date + Time
	public static String getCurrentDateTime() {
		String dateTime = getCurrentDate() + " " + getCurrentTime();
		return dateTime;
	}

	// function to get systems current time in 24 hr format
	public static String getCurrentTime() {
		DateFormat dateFormat = new SimpleDateFormat("HH:mm");
		Calendar cal = Calendar.getInstance();
		String date = dateFormat.format(cal.getTime());
		return date;
	}
	
	// function to get systems current time in 12 hr format
		public static String getCurrentTimeForImage() {
			DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
			Calendar cal = Calendar.getInstance();
			String date = dateFormat.format(cal.getTime());
			return date;
		}
		// Get Systems Curent date + Time 12 hr format
		public static String getCurrentDateTimeForImage() {
			String dateTime = getCurrentDate() + " " + getCurrentTimeForImage();
			return dateTime;
		}
		// Function to check if string is null
	public static String checkString(String data, String returnData) {
		if (data == null || data.equalsIgnoreCase("")
				|| data.toString().equalsIgnoreCase("null")
				|| data.toString().equalsIgnoreCase("(null)")) {
			return returnData;
		} else {
			return data;
		}
	}

	public static void AboutBox(String Msg, Context con) {
		new AlertDialog.Builder(con)
				.setTitle(con.getResources().getString(R.string.alert))
				.setMessage(Msg)
				.setPositiveButton(con.getResources().getString(R.string.ok),
						null).show();

	}

	// Upload file to Server
	public static String uploadFile(String filetoString, String serverUrl) {
		String res = "";
		// String upLoadServerUri
		// ="http://citizencop.in/manage/CitizenCopUploadImage.ashx";
		String fileName = filetoString;
		LoggerUtil.e("FileName", "" + fileName);
		int serverResponseCode;
		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 2 * 1024 * 3072;
		LoggerUtil.e("Buffer Size", maxBufferSize + "");
		File sourceFile = new File(fileName);
		if (!sourceFile.isFile()) {
			LoggerUtil.e("uploadFile", "Source File Does not exist");
			return "not file";
		}

		try {
			FileInputStream fileInputStream = new FileInputStream(sourceFile);
			URL url = new URL(serverUrl);
			conn = (HttpURLConnection) url.openConnection(); // Open a HTTP
			LoggerUtil.e("File To Upload HERE ", fileName);
			conn.setDoInput(true); // Allow Inputs
			conn.setDoOutput(true); // Allow Outputs
			conn.setUseCaches(false); // Don't use a Cached Copy
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("ENCTYPE", "multipart/form-data");
			conn.setRequestProperty("Content-Type",
					"multipart/form-data;boundary=" + boundary);
			conn.setRequestProperty("imageToUpload", fileName); // fileName
			dos = new DataOutputStream(conn.getOutputStream());
			dos.writeBytes(twoHyphens + boundary + lineEnd);
			dos.writeBytes("Content-Disposition: form-data; name=\"imageToUpload\";filename=\""
					+ fileName + "\"" + lineEnd);
			dos.writeBytes(lineEnd);
			bytesAvailable = fileInputStream.available(); // create a buffer of
															// maximum size
			LoggerUtil.e("bytes Available", bytesAvailable + "");
			// bufferSize = Math.min(bytesAvailable, maxBufferSize);
			bufferSize = bytesAvailable * 2;
			LoggerUtil.e("bufferSize", bytesAvailable + "");
			buffer = new byte[bufferSize];

			// read file and write it into form...
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);

			while (bytesRead > 0) {
				dos.write(buffer, 0, bufferSize);
				bytesAvailable = fileInputStream.available();
				// bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bufferSize = bytesAvailable * 2;
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			}
			dos.writeBytes(lineEnd);
			dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
			serverResponseCode = conn.getResponseCode();
			String serverResponseMessage = conn.getResponseMessage() + "";
			res = serverResponseMessage;
			LoggerUtil.e("uploadFile", "HTTP Response is : "
					+ serverResponseMessage + ": " + serverResponseCode);
			if (serverResponseCode == 200) {
				// tv.setText("<span id="IL_AD6" class="IL_AD">File Upload</span> Completed.");
			}

			// close the streams //
			fileInputStream.close();
			dos.flush();
			dos.close();

		} catch (MalformedURLException ex) {

			Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
		} catch (Exception e) {
			Log.e("Upload file to server Exception",
					"Exception : " + e.getMessage(), e);
		}

		return res;
	}

	public static InputFilter[] limitchars(int i) {

		InputFilter[] FilterArray = new InputFilter[1];
		FilterArray[0] = new InputFilter.LengthFilter(i);
		return FilterArray;

	}

	public static InputFilter[] specialchars() {
		InputFilter[] filters = new InputFilter[1];
		filters[0] = new InputFilter() {
			@Override
			public CharSequence filter(CharSequence source, int start, int end,
					Spanned dest, int dstart, int dend) {
				if (end > start) {

					char[] acceptedChars = new char[] { 'a', 'b', 'c', 'd',
							'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
							'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
							'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
							'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
							'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1',
							'2', '3', '4', '5', '6', '7', '8', '9', ' ' };

					for (int index = start; index < end; index++) {
						if (!new String(acceptedChars).contains(String
								.valueOf(source.charAt(index)))) {
							return "";
						}
					}
				}
				return null;
			}

		};
		return filters;
	}

	public static InputFilter[] specialcharsForTarget() {
		InputFilter[] filters = new InputFilter[1];
		filters[0] = new InputFilter() {
			@Override
			public CharSequence filter(CharSequence source, int start, int end,
					Spanned dest, int dstart, int dend) {
				if (end > start) {

					char[] acceptedChars = new char[] { 'a', 'b', 'c', 'd',
							'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
							'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
							'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
							'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
							'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1',
							'2', '3', '4', '5', '6', '7', '8', '9', ',', '.',
							' ' };

					for (int index = start; index < end; index++) {
						if (!new String(acceptedChars).contains(String
								.valueOf(source.charAt(index)))) {
							return "";
						}
					}
				}
				return null;
			}

		};
		return filters;
	}

	public static int getAppVersion(Context context) {
		try {
			PackageInfo pInfo = context.getPackageManager().getPackageInfo(
					context.getPackageName(), PackageManager.GET_META_DATA);
			return pInfo.versionCode;
		} catch (NameNotFoundException e) {
			return 0;
		}
	}

	public static int getDeviceVersion(Context context) {
		int sdkVersion = android.os.Build.VERSION.SDK_INT;
		return sdkVersion;
	}

	/*
	 * public void hideKeybord(Context context) { InputMethodManager
	 * inputManager = (InputMethodManager)
	 * context.getSystemService(Context.INPUT_METHOD_SERVICE);
	 * 
	 * inputManager.hideSoftInputFromWindow(context.getCurrentFocus().getWindowToken
	 * (), InputMethodManager.HIDE_NOT_ALWAYS); }
	 */
}
