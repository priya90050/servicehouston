package quacito.houston.model;

public class GetAllRouteDTO {
	
	private String Class_ID="";
	private String Created_By="";
	private String Created_Date="";
	private String Lead_ID="";
	private String RouteMaster_ID="";
	private String Route_Name="";
	private String Status="";
	private String Updated_By="";
	private String Updated_Date="";
	/**
	 * @return the class_ID
	 */
	public String getClass_ID() {
		return Class_ID;
	}
	/**
	 * @param class_ID the class_ID to set
	 */
	public void setClass_ID(String class_ID) {
		Class_ID = class_ID;
	}
	/**
	 * @return the created_By
	 */
	public String getCreated_By() {
		return Created_By;
	}
	/**
	 * @param created_By the created_By to set
	 */
	public void setCreated_By(String created_By) {
		Created_By = created_By;
	}
	/**
	 * @return the created_Date
	 */
	public String getCreated_Date() {
		return Created_Date;
	}
	/**
	 * @param created_Date the created_Date to set
	 */
	public void setCreated_Date(String created_Date) {
		Created_Date = created_Date;
	}
	/**
	 * @return the lead_ID
	 */
	public String getLead_ID() {
		return Lead_ID;
	}
	/**
	 * @param lead_ID the lead_ID to set
	 */
	public void setLead_ID(String lead_ID) {
		Lead_ID = lead_ID;
	}
	/**
	 * @return the routeMaster_ID
	 */
	public String getRouteMaster_ID() {
		return RouteMaster_ID;
	}
	/**
	 * @param routeMaster_ID the routeMaster_ID to set
	 */
	public void setRouteMaster_ID(String routeMaster_ID) {
		RouteMaster_ID = routeMaster_ID;
	}
	/**
	 * @return the route_Name
	 */
	public String getRoute_Name() {
		return Route_Name;
	}
	/**
	 * @param route_Name the route_Name to set
	 */
	public void setRoute_Name(String route_Name) {
		Route_Name = route_Name;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return Status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		Status = status;
	}
	/**
	 * @return the updated_By
	 */
	public String getUpdated_By() {
		return Updated_By;
	}
	/**
	 * @param updated_By the updated_By to set
	 */
	public void setUpdated_By(String updated_By) {
		Updated_By = updated_By;
	}
	/**
	 * @return the updated_Date
	 */
	public String getUpdated_Date() {
		return Updated_Date;
	}
	/**
	 * @param updated_Date the updated_Date to set
	 */
	public void setUpdated_Date(String updated_Date) {
		Updated_Date = updated_Date;
	}
	
	
	
}
