package quacito.houston.model;

public class EmployeeDTO 
{
	private String EmployeeId;
    private String Username="";
	private String Password="";
	private String FirstName="";
	private String MiddleName="";
	private String LastName="";
	private String RoleId="";
	private String EmpName="";
	
	private String RoleName="";
    private String Emailid="";
	private String PestPackId="";
	private String DepartmentId="";
	private String AccountNo="";
	private String HomePhone="";
	private String CellPhone="";
	private  String Address1="";
	private  String Address2="";
	private  String SupervisorId="";
	private  String InactiveDate="";
	private  String LicenceNo="";
	private  String City="";
	private  String State="";
	private  String ZipCode="";
	private  String Status="";
	private  String Created_By="";
	private  String Updated_By="";
	public String getEmployeeId() {
		return EmployeeId;
	}
	public void setEmployeeId(String employeeId) {
		EmployeeId = employeeId;
	}
	public String getEmpName() {
		return EmpName;
	}
	public void setEmpName(String empName) {
		EmpName = empName;
	}
	public String getUsername() {
		return Username;
	}
	public void setUsername(String username) {
		Username = username;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getMiddleName() {
		return MiddleName;
	}
	public void setMiddleName(String middleName) {
		MiddleName = middleName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public String getRoleId() {
		return RoleId;
	}
	public void setRoleId(String roleId) {
		RoleId = roleId;
	}
	public String getRoleName() {
		return RoleName;
	}
	public void setRoleName(String roleName) {
		RoleName = roleName;
	}
	public String getEmailid() {
		return Emailid;
	}
	public void setEmailid(String emailid) {
		Emailid = emailid;
	}
	public String getPestPackId() {
		return PestPackId;
	}
	public void setPestPackId(String pestPackId) {
		PestPackId = pestPackId;
	}
	public String getDepartmentId() {
		return DepartmentId;
	}
	public void setDepartmentId(String departmentId) {
		DepartmentId = departmentId;
	}
	public String getAccountNo() {
		return AccountNo;
	}
	public void setAccountNo(String accountNo) {
		AccountNo = accountNo;
	}
	public String getHomePhone() {
		return HomePhone;
	}
	public void setHomePhone(String homePhone) {
		HomePhone = homePhone;
	}
	public String getCellPhone() {
		return CellPhone;
	}
	public void setCellPhone(String cellPhone) {
		CellPhone = cellPhone;
	}
	public String getAddress1() {
		return Address1;
	}
	public void setAddress1(String address1) {
		Address1 = address1;
	}
	public String getAddress2() {
		return Address2;
	}
	public void setAddress2(String address2) {
		Address2 = address2;
	}
	public String getSupervisorId() {
		return SupervisorId;
	}
	public void setSupervisorId(String supervisorId) {
		SupervisorId = supervisorId;
	}
	public String getInactiveDate() {
		return InactiveDate;
	}
	public void setInactiveDate(String inactiveDate) {
		InactiveDate = inactiveDate;
	}
	public String getLicenceNo() {
		return LicenceNo;
	}
	public void setLicenceNo(String licenceNo) {
		LicenceNo = licenceNo;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getZipCode() {
		return ZipCode;
	}
	public void setZipCode(String zipCode) {
		ZipCode = zipCode;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getCreated_By() {
		return Created_By;
	}
	public void setCreated_By(String created_By) {
		Created_By = created_By;
	}
	public String getUpdated_By() {
		return Updated_By;
	}
	public void setUpdated_By(String updated_By) {
		Updated_By = updated_By;
	}
	
	
	
	

}
