package quacito.houston.model;

import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class ChemicalsComponentDTO 
{
	CheckBox chemicalsCheckBox;
	TextView productTextView;
	EditText persentSpinner;
	EditText amountEditText;
	EditText otherEditText;
	Spinner unitSpinner;
	EditText targetSpinner;
	
	
	
	
	public EditText getOtherEditText() {
		return otherEditText;
	}
	public void setOtherEditText(EditText otherEditText) {
		this.otherEditText = otherEditText;
	}
	public CheckBox getChemicalsCheckBox() {
		return chemicalsCheckBox;
	}
	public void setChemicalsCheckBox(CheckBox chemicalsCheckBox) {
		this.chemicalsCheckBox = chemicalsCheckBox;
	}
	public TextView getProductTextView() {
		return productTextView;
	}
	public void setProductTextView(TextView productTextView) {
		this.productTextView = productTextView;
	}
	public EditText getPersentSpinner() {
		return persentSpinner;
	}
	public void setPersentSpinner(EditText persentSpinner) {
		this.persentSpinner = persentSpinner;
	}
	public EditText getAmountEditText() {
		return amountEditText;
	}
	public void setAmountEditText(EditText amountEditText) {
		this.amountEditText = amountEditText;
	}
	public Spinner getUnitSpinner() {
		return unitSpinner;
	}
	public void setUnitSpinner(Spinner unitSpinner) {
		this.unitSpinner = unitSpinner;
	}
	public EditText getTargetSpinner() {
		return targetSpinner;
	}
	public void setTargetSpinner(EditText targetSpinner) {
		this.targetSpinner = targetSpinner;
	}
	
	
}
