package quacito.houston.model;

public class LogDTO 
{
private String SID;
private String GeneralSR;
private String Residetial_Insp;
private String Chemical;
private String AgreeMentMailID;
private String Crew_Member;
private String Commercial_Insp;
/**
 * @return the sID
 */

public String getSID() {
	return SID;
}
/**
 * @return the commercial_Insp
 */
public String getCommercial_Insp() {
	return Commercial_Insp;
}
/**
 * @param commercial_Insp the commercial_Insp to set
 */
public void setCommercial_Insp(String commercial_Insp) {
	Commercial_Insp = commercial_Insp;
}
/**
 * @param sID the sID to set
 */
public void setSID(String sID) {
	SID = sID;
}
/**
 * @return the generalSR
 */
public String getGeneralSR() {
	return GeneralSR;
}
/**
 * @param generalSR the generalSR to set
 */
public void setGeneralSR(String generalSR) {
	GeneralSR = generalSR;
}
/**
 * @return the residetial_Insp
 */
public String getResidetial_Insp() {
	return Residetial_Insp;
}
/**
 * @param residetial_Insp the residetial_Insp to set
 */
public void setResidetial_Insp(String residetial_Insp) {
	Residetial_Insp = residetial_Insp;
}
/**
 * @return the chemical
 */
public String getChemical() {
	return Chemical;
}
/**
 * @param chemical the chemical to set
 */
public void setChemical(String chemical) {
	Chemical = chemical;
}
/**
 * @return the agreeMentMailID
 */
public String getAgreeMentMailID() {
	return AgreeMentMailID;
}
/**
 * @param agreeMentMailID the agreeMentMailID to set
 */
public void setAgreeMentMailID(String agreeMentMailID) {
	AgreeMentMailID = agreeMentMailID;
}
/**
 * @return the crew_Member
 */
public String getCrew_Member() {
	return Crew_Member;
}
/**
 * @param crew_Member the crew_Member to set
 */
public void setCrew_Member(String crew_Member) {
	Crew_Member = crew_Member;
}
}
