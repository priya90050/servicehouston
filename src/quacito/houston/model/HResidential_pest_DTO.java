package quacito.houston.model;

public class HResidential_pest_DTO 
{
	private String isCompleted="";
	
	/**
	 * @return the isCompleted
	 */
	public String getIsCompleted() {
		return isCompleted;
	}
	/**
	 * @param isCompleted the isCompleted to set
	 */
	public void setIsCompleted(String isCompleted) {
		this.isCompleted = isCompleted;
	}
	private String ResidentialId="";
	private String ServiceID_new="ServiceID_new";
	private String ServiceFor="";
	private String AreasInspected="";
	private String InsectActivity="";
	private String Ants="";
	private String OtherPest="";
	private String OutsideOnly="";
	private String PestActivity="";
	private String Fence="";
	private String BackYard="";
	private String FrontYard="";
	private String ExteriorPerimeter="";
	private String Roofline="";
	private String Garage="";
	private String Attic="";
	private String InteriorHouse="";
	private String PestControl="";
	private String PestControlServices="";
	private String PlusTermite="";

	private String FencePest="";
	private String FenceActivity="";
	private String NumberPest="";
	private String NumberText="";
	private String NumberActivity="";
	private String NumberActivitytext="";
	private String WoodPest="";
	private String WoodOption="";
	private String WoodActivity="";
	private String ExteriorPest="";
	private String ExteriorActivity="";
	private String AtticPest="";
	private String AtticActivity="";
	private String InteriorPest="";
	private String InteriorActivity="";
	private String PlusRodent="";
	private String RoofRodent="";
	private String RoofNeeds="";

	private String RoofVentRodent="";
	private String RoofVentNeeds="";
	private String Flashing="";
	private String FlashingNeeds="";
	private String Fascia="";
	private String FasciaNeeds="";
	private String Siding="";
	private String SidingNeeds="";
	private String Breezway="";
	private String BreezwayNeeds="";
	private String GarageDoor="";
	private String GarageDoorNeeds="";
	private String DryerVent="";
	private String DryerVentNeeds="";
	private String NumberLive="";
	private String NumberLiveNeeds="";
	private String NumberSnap="";
	private String NumberSnapNeeds="";

	private String PlusMosquito="";
	private String PlusMosquitoDetail="";
	private String DurationTime="";
	private String SystemRuns="";
	private String TreeBranches="";
	private String Firewood="";
	private String DebrisCrawl="";
	private String ExcessivePlant="";
	private String Soil="";
	private String WoodSoil="";
	private String DebrisRoof="";
	private String StandingWater="";
	private String MoistureProblem="";
	private String Openings="";
	private String ExcessiveGaps="";
	private String LeakyPlumbing="";
	private String GarbageCans="";
	private String MoistureDamaged="";

	private String GroceryBags="";
	private String PetFood="";
	private String RecycledItems="";
	private String ExcessiveStorage="";
	private String TechnicianComments="";
	private String TimeIn="";
	private String Timeout="";
	private String TimeInDateTime="";
	private String TimeOutDateTime="";
	private String Technician="";
	private String EmpNo="";
	private String Customer="";
	private String Date="";
	private String PaymentType="";
	private String Amount="";
	private String CheckNo="";
	private String LicenseNo="";
	private String Signature="";
	private String ServiceSignature="";
	private String AgreementPath="";
	private String Create_By="";
	private String Update_By="";
	private String Create_Date="";
	private String Update_Date="";
	
	
	
	public String getServiceID_new() {
		return ServiceID_new;
	}
	public void setServiceID_new(String serviceID_new) {
		ServiceID_new = serviceID_new;
	}
	public String getResidentialId() {
		return ResidentialId;
	}
	public void setResidentialId(String residentialId) {
		ResidentialId = residentialId;
	}
	public String getServiceFor() {
		return ServiceFor;
	}
	public void setServiceFor(String serviceFor) {
		ServiceFor = serviceFor;
	}
	public String getAreasInspected() {
		return AreasInspected;
	}
	public void setAreasInspected(String areasInspected) {
		AreasInspected = areasInspected;
	}
	public String getInsectActivity() {
		return InsectActivity;
	}
	public void setInsectActivity(String insectActivity) {
		InsectActivity = insectActivity;
	}
	public String getAnts() {
		return Ants;
	}
	public void setAnts(String ants) {
		Ants = ants;
	}
	public String getOtherPest() {
		return OtherPest;
	}
	public void setOtherPest(String otherPest) {
		OtherPest = otherPest;
	}
	public String getOutsideOnly() {
		return OutsideOnly;
	}
	public void setOutsideOnly(String outsideOnly) {
		OutsideOnly = outsideOnly;
	}
	public String getPestActivity() {
		return PestActivity;
	}
	public void setPestActivity(String pestActivity) {
		PestActivity = pestActivity;
	}
	public String getFence() {
		return Fence;
	}
	public void setFence(String fence) {
		Fence = fence;
	}
	public String getBackYard() {
		return BackYard;
	}
	public void setBackYard(String backYard) {
		BackYard = backYard;
	}
	public String getFrontYard() {
		return FrontYard;
	}
	public void setFrontYard(String frontYard) {
		FrontYard = frontYard;
	}
	public String getExteriorPerimeter() {
		return ExteriorPerimeter;
	}
	public void setExteriorPerimeter(String exteriorPerimeter) {
		ExteriorPerimeter = exteriorPerimeter;
	}
	public String getRoofline() {
		return Roofline;
	}
	public void setRoofline(String roofline) {
		Roofline = roofline;
	}
	public String getGarage() {
		return Garage;
	}
	public void setGarage(String garage) {
		Garage = garage;
	}
	public String getAttic() {
		return Attic;
	}
	public void setAttic(String attic) {
		Attic = attic;
	}
	public String getInteriorHouse() {
		return InteriorHouse;
	}
	public void setInteriorHouse(String interiorHouse) {
		InteriorHouse = interiorHouse;
	}
	public String getPestControl() {
		return PestControl;
	}
	public void setPestControl(String pestControl) {
		PestControl = pestControl;
	}
	public String getPestControlServices() {
		return PestControlServices;
	}
	public void setPestControlServices(String pestControlServices) {
		PestControlServices = pestControlServices;
	}
	public String getPlusTermite() {
		return PlusTermite;
	}
	public void setPlusTermite(String plusTermite) {
		PlusTermite = plusTermite;
	}
	public String getFencePest() {
		return FencePest;
	}
	public void setFencePest(String fencePest) {
		FencePest = fencePest;
	}
	public String getFenceActivity() {
		return FenceActivity;
	}
	public void setFenceActivity(String fenceActivity) {
		FenceActivity = fenceActivity;
	}
	public String getNumberPest() {
		return NumberPest;
	}
	public void setNumberPest(String numberPest) {
		NumberPest = numberPest;
	}
	public String getNumberText() {
		return NumberText;
	}
	public void setNumberText(String numberText) {
		NumberText = numberText;
	}
	public String getNumberActivity() {
		return NumberActivity;
	}
	public void setNumberActivity(String numberActivity) {
		NumberActivity = numberActivity;
	}
	public String getNumberActivitytext() {
		return NumberActivitytext;
	}
	public void setNumberActivitytext(String numberActivitytext) {
		NumberActivitytext = numberActivitytext;
	}
	public String getWoodPest() {
		return WoodPest;
	}
	public void setWoodPest(String woodPest) {
		WoodPest = woodPest;
	}
	public String getWoodOption() {
		return WoodOption;
	}
	public void setWoodOption(String woodOption) {
		WoodOption = woodOption;
	}
	public String getWoodActivity() {
		return WoodActivity;
	}
	public void setWoodActivity(String woodActivity) {
		WoodActivity = woodActivity;
	}
	public String getExteriorPest() {
		return ExteriorPest;
	}
	public void setExteriorPest(String exteriorPest) {
		ExteriorPest = exteriorPest;
	}
	public String getExteriorActivity() {
		return ExteriorActivity;
	}
	public void setExteriorActivity(String exteriorActivity) {
		ExteriorActivity = exteriorActivity;
	}
	public String getAtticPest() {
		return AtticPest;
	}
	public void setAtticPest(String atticPest) {
		AtticPest = atticPest;
	}
	public String getAtticActivity() {
		return AtticActivity;
	}
	public void setAtticActivity(String atticActivity) {
		AtticActivity = atticActivity;
	}
	public String getInteriorPest() {
		return InteriorPest;
	}
	public void setInteriorPest(String interiorPest) {
		InteriorPest = interiorPest;
	}
	public String getInteriorActivity() {
		return InteriorActivity;
	}
	public void setInteriorActivity(String interiorActivity) {
		InteriorActivity = interiorActivity;
	}
	public String getPlusRodent() {
		return PlusRodent;
	}
	public void setPlusRodent(String plusRodent) {
		PlusRodent = plusRodent;
	}
	public String getRoofRodent() {
		return RoofRodent;
	}
	public void setRoofRodent(String roofRodent) {
		RoofRodent = roofRodent;
	}
	public String getRoofNeeds() {
		return RoofNeeds;
	}
	public void setRoofNeeds(String roofNeeds) {
		RoofNeeds = roofNeeds;
	}
	public String getRoofVentRodent() {
		return RoofVentRodent;
	}
	public void setRoofVentRodent(String roofVentRodent) {
		RoofVentRodent = roofVentRodent;
	}
	public String getRoofVentNeeds() {
		return RoofVentNeeds;
	}
	public void setRoofVentNeeds(String roofVentNeeds) {
		RoofVentNeeds = roofVentNeeds;
	}
	public String getFlashing() {
		return Flashing;
	}
	public void setFlashing(String flashing) {
		Flashing = flashing;
	}
	public String getFlashingNeeds() {
		return FlashingNeeds;
	}
	public void setFlashingNeeds(String flashingNeeds) {
		FlashingNeeds = flashingNeeds;
	}
	public String getFascia() {
		return Fascia;
	}
	public void setFascia(String fascia) {
		Fascia = fascia;
	}
	public String getFasciaNeeds() {
		return FasciaNeeds;
	}
	public void setFasciaNeeds(String fasciaNeeds) {
		FasciaNeeds = fasciaNeeds;
	}
	public String getSiding() {
		return Siding;
	}
	public void setSiding(String siding) {
		Siding = siding;
	}
	public String getSidingNeeds() {
		return SidingNeeds;
	}
	public void setSidingNeeds(String sidingNeeds) {
		SidingNeeds = sidingNeeds;
	}
	public String getBreezway() {
		return Breezway;
	}
	public void setBreezway(String breezway) {
		Breezway = breezway;
	}
	public String getBreezwayNeeds() {
		return BreezwayNeeds;
	}
	public void setBreezwayNeeds(String breezwayNeeds) {
		BreezwayNeeds = breezwayNeeds;
	}
	public String getGarageDoor() {
		return GarageDoor;
	}
	public void setGarageDoor(String garageDoor) {
		GarageDoor = garageDoor;
	}
	public String getGarageDoorNeeds() {
		return GarageDoorNeeds;
	}
	public void setGarageDoorNeeds(String garageDoorNeeds) {
		GarageDoorNeeds = garageDoorNeeds;
	}
	public String getDryerVent() {
		return DryerVent;
	}
	public void setDryerVent(String dryerVent) {
		DryerVent = dryerVent;
	}
	public String getDryerVentNeeds() {
		return DryerVentNeeds;
	}
	public void setDryerVentNeeds(String dryerVentNeeds) {
		DryerVentNeeds = dryerVentNeeds;
	}
	public String getNumberLive() {
		return NumberLive;
	}
	public void setNumberLive(String numberLive) {
		NumberLive = numberLive;
	}
	public String getNumberLiveNeeds() {
		return NumberLiveNeeds;
	}
	public void setNumberLiveNeeds(String numberLiveNeeds) {
		NumberLiveNeeds = numberLiveNeeds;
	}
	public String getNumberSnap() {
		return NumberSnap;
	}
	public void setNumberSnap(String numberSnap) {
		NumberSnap = numberSnap;
	}
	public String getNumberSnapNeeds() {
		return NumberSnapNeeds;
	}
	public void setNumberSnapNeeds(String numberSnapNeeds) {
		NumberSnapNeeds = numberSnapNeeds;
	}
	public String getPlusMosquito() {
		return PlusMosquito;
	}
	public void setPlusMosquito(String plusMosquito) {
		PlusMosquito = plusMosquito;
	}
	public String getPlusMosquitoDetail() {
		return PlusMosquitoDetail;
	}
	public void setPlusMosquitoDetail(String plusMosquitoDetail) {
		PlusMosquitoDetail = plusMosquitoDetail;
	}
	public String getDurationTime() {
		return DurationTime;
	}
	public void setDurationTime(String durationTime) {
		DurationTime = durationTime;
	}
	public String getSystemRuns() {
		return SystemRuns;
	}
	public void setSystemRuns(String systemRuns) {
		SystemRuns = systemRuns;
	}
	public String getTreeBranches() {
		return TreeBranches;
	}
	public void setTreeBranches(String treeBranches) {
		TreeBranches = treeBranches;
	}
	public String getFirewood() {
		return Firewood;
	}
	public void setFirewood(String firewood) {
		Firewood = firewood;
	}
	public String getDebrisCrawl() {
		return DebrisCrawl;
	}
	public void setDebrisCrawl(String debrisCrawl) {
		DebrisCrawl = debrisCrawl;
	}
	public String getExcessivePlant() {
		return ExcessivePlant;
	}
	public void setExcessivePlant(String excessivePlant) {
		ExcessivePlant = excessivePlant;
	}
	public String getSoil() {
		return Soil;
	}
	public void setSoil(String soil) {
		Soil = soil;
	}
	public String getWoodSoil() {
		return WoodSoil;
	}
	public void setWoodSoil(String woodSoil) {
		WoodSoil = woodSoil;
	}
	public String getDebrisRoof() {
		return DebrisRoof;
	}
	public void setDebrisRoof(String debrisRoof) {
		DebrisRoof = debrisRoof;
	}
	public String getStandingWater() {
		return StandingWater;
	}
	public void setStandingWater(String standingWater) {
		StandingWater = standingWater;
	}
	public String getMoistureProblem() {
		return MoistureProblem;
	}
	public void setMoistureProblem(String moistureProblem) {
		MoistureProblem = moistureProblem;
	}
	public String getOpenings() {
		return Openings;
	}
	public void setOpenings(String openings) {
		Openings = openings;
	}
	public String getExcessiveGaps() {
		return ExcessiveGaps;
	}
	public void setExcessiveGaps(String excessiveGaps) {
		ExcessiveGaps = excessiveGaps;
	}
	public String getLeakyPlumbing() {
		return LeakyPlumbing;
	}
	public void setLeakyPlumbing(String leakyPlumbing) {
		LeakyPlumbing = leakyPlumbing;
	}
	public String getGarbageCans() {
		return GarbageCans;
	}
	public void setGarbageCans(String garbageCans) {
		GarbageCans = garbageCans;
	}
	public String getMoistureDamaged() {
		return MoistureDamaged;
	}
	public void setMoistureDamaged(String moistureDamaged) {
		MoistureDamaged = moistureDamaged;
	}
	public String getGroceryBags() {
		return GroceryBags;
	}
	public void setGroceryBags(String groceryBags) {
		GroceryBags = groceryBags;
	}
	public String getPetFood() {
		return PetFood;
	}
	public void setPetFood(String petFood) {
		PetFood = petFood;
	}
	public String getRecycledItems() {
		return RecycledItems;
	}
	public void setRecycledItems(String recycledItems) {
		RecycledItems = recycledItems;
	}
	public String getExcessiveStorage() {
		return ExcessiveStorage;
	}
	public void setExcessiveStorage(String excessiveStorage) {
		ExcessiveStorage = excessiveStorage;
	}
	public String getTechnicianComments() {
		return TechnicianComments;
	}
	public void setTechnicianComments(String technicianComments) {
		TechnicianComments = technicianComments;
	}
	public String getTimeIn() {
		return TimeIn;
	}
	public void setTimeIn(String timeIn) {
		TimeIn = timeIn;
	}
	public String getTimeout() {
		return Timeout;
	}
	public void setTimeout(String timeout) {
		Timeout = timeout;
	}
	public String getTimeInDateTime() {
		return TimeInDateTime;
	}
	public void setTimeInDateTime(String timeInDateTime) {
		TimeInDateTime = timeInDateTime;
	}
	public String getTimeOutDateTime() {
		return TimeOutDateTime;
	}
	public void setTimeOutDateTime(String timeOutDateTime) {
		TimeOutDateTime = timeOutDateTime;
	}
	public String getTechnician() {
		return Technician;
	}
	public void setTechnician(String technician) {
		Technician = technician;
	}
	public String getEmpNo() {
		return EmpNo;
	}
	public void setEmpNo(String empNo) {
		EmpNo = empNo;
	}
	public String getCustomer() {
		return Customer;
	}
	public void setCustomer(String customer) {
		Customer = customer;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getPaymentType() {
		return PaymentType;
	}
	public void setPaymentType(String paymentType) {
		PaymentType = paymentType;
	}
	public String getAmount() {
		return Amount;
	}
	public void setAmount(String amount) {
		Amount = amount;
	}
	public String getCheckNo() {
		return CheckNo;
	}
	public void setCheckNo(String checkNo) {
		CheckNo = checkNo;
	}
	public String getLicenseNo() {
		return LicenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		LicenseNo = licenseNo;
	}
	public String getSignature() {
		return Signature;
	}
	public void setSignature(String signature) {
		Signature = signature;
	}
	public String getServiceSignature() {
		return ServiceSignature;
	}
	public void setServiceSignature(String serviceSignature) {
		ServiceSignature = serviceSignature;
	}
	public String getAgreementPath() {
		return AgreementPath;
	}
	public void setAgreementPath(String agreementPath) {
		AgreementPath = agreementPath;
	}
	public String getCreate_By() {
		return Create_By;
	}
	public void setCreate_By(String create_By) {
		Create_By = create_By;
	}
	public String getUpdate_By() {
		return Update_By;
	}
	public void setUpdate_By(String update_By) {
		Update_By = update_By;
	}
	public String getCreate_Date() {
		return Create_Date;
	}
	public void setCreate_Date(String create_Date) {
		Create_Date = create_Date;
	}
	public String getUpdate_Date() {
		return Update_Date;
	}
	public void setUpdate_Date(String update_Date) {
		Update_Date = update_Date;
	}
	
	
}
