package quacito.houston.model;

public class OResidential_pest_DTO {
	
			private String FloridaResidentialServiceId="";
			private String isCompleted="";
			private String ServiceID_new=""; 
			private String ServiceFor=""; 
			private String AreasInspected ="";
			private String InsectActivity ="";
			private String Ants="";
			private String OtherPest=""; 
			private String OutsideOnly="";
			private String PestActivity="";
			private String Fence ="";
			private String BackYard=""; 
			private String FrontYard ="";
			private String ExteriorPerimeter="";
			private String Roofline=""; 
			private String Garage ="";
			private String Attic=""; 
			private String InteriorHouse ="";
			private String Basement="";
			private String PestControl=""; 
			private String PestControlServices ="";
			private String PlusTermite=""; 
			private String FencePest ="";
			private String FenceActivity=""; 
			private String BuilderConstruction ="";
			private String BuilderConstructionActivity="";
			private String CrawlSpaceDoor=""; 
			private String CrawlSpaceDoorActivity ="";
			private String NumberPest=""; 
			private String NumberText ="";
			private String NumberActivity="";
			private String NumberActivitytext="";
			private String WoodPest ="";
			private String WoodOption=""; 
			private String WoodActivity ="";
			private String ExteriorPest="";
			private String ExteriorActivity=""; 
			private String AtticPest ="";
			private String AtticActivity=""; 
			private String InteriorPest ="";
			private String InteriorActivity=""; 
			private String TreatmentOfSchool ="";
			private String MonitoringDevices="";
			private String NumMonitoringDevices=""; 
			private String BasementTermite ="";
			private String NumBasementTermite=""; 
			private String AdditionMonitoringDevice ="";
			private String NumAdditionMonitoringDevice="";
			private String MonitoringDeviceRemoved="";
			private String NumMonitoringDeviceRemoved ="";
			private String PlusRodent=""; 
			private String RoofRodent ="";
			private String RoofVentNeeds="";
			private String RoofVentRodent=""; 
			private String RoofNeeds ="";
			private String Flashing=""; 
			private String FlashingNeeds ="";
			private String Fascia=""; 
			private String FasciaNeeds ="";
			private String Siding=""; 
			private String SidingNeeds ="";
			private String Breezway="";
			private String BreezwayNeeds=""; 
			private String GarageDoor ="";
			private String GarageDoorNeeds=""; 
			private String DryerVent ="";
			private String DryerVentNeeds="";
			private String NumberLive=""; 
			private String NumberLiveNeeds ="";
			private String NumberSnap=""; 
			private String NumberSnapNeeds ="";
			private String PlusMosquito="";
			private String PlusMosquitoDetail=""; 
			private String DurationTime ="";
			private String SystemRuns=""; 
			private String TreeBranches ="";
			private String Firewood="";
			private String DebrisCrawl=""; 
			private String ExcessivePlant ="";
			private String Soil=""; 
			private String WoodSoil ="";
			private String DebrisRoof="";
			private String StandingWater=""; 
			private String MoistureProblem ="";
			private String Openings=""; 
			private String ExcessiveGaps ="";
			private String LeakyPlumbing="";
			private String GarbageCans=""; 
			private String MoistureDamaged ="";
			private String GroceryBags=""; 
			private String PetFood ="";
			private String RecycledItems=""; 
			private String ExcessiveStorage ="";
			private String TechnicianComments="";
			private String TimeIn=""; 
			
			private String Timeout=""; 
			private String TimeInDateTime ="";
			private String TimeOutDateTime=""; 
			private String Technician=""; 
			private String EmpNo ="";
			private String Customer="";
			private String Date=""; 
			private String PaymentType ="";
			private String Amount=""; 
			private String CheckNo ="";
			private String LicenseNo=""; 
			private String Signature ="";
			private String ServiceSignature="";
			private String Create_By=""; 
			private String Update_By ="";
			private String Create_date=""; 
			private String Update_Date ="";
			private String AgreementPath="";
			/**
			 * @return the floridaResidentialServiceId
			 */
			public String getFloridaResidentialServiceId() {
				return FloridaResidentialServiceId;
			}
			/**
			 * @param floridaResidentialServiceId the floridaResidentialServiceId to set
			 */
			public void setFloridaResidentialServiceId(String floridaResidentialServiceId) {
				FloridaResidentialServiceId = floridaResidentialServiceId;
			}
			/**
			 * @return the isCompleted
			 */
			public String getIsCompleted() {
				return isCompleted;
			}
			/**
			 * @param isCompleted the isCompleted to set
			 */
			public void setIsCompleted(String isCompleted) {
				this.isCompleted = isCompleted;
			}
			/**
			 * @return the serviceID_new
			 */
			public String getServiceID_new() {
				return ServiceID_new;
			}
			/**
			 * @param serviceID_new the serviceID_new to set
			 */
			public void setServiceID_new(String serviceID_new) {
				ServiceID_new = serviceID_new;
			}
			/**
			 * @return the serviceFor
			 */
			public String getServiceFor() {
				return ServiceFor;
			}
			/**
			 * @param serviceFor the serviceFor to set
			 */
			public void setServiceFor(String serviceFor) {
				ServiceFor = serviceFor;
			}
			/**
			 * @return the areasInspected
			 */
			public String getAreasInspected() {
				return AreasInspected;
			}
			/**
			 * @param areasInspected the areasInspected to set
			 */
			public void setAreasInspected(String areasInspected) {
				AreasInspected = areasInspected;
			}
			/**
			 * @return the insectActivity
			 */
			public String getInsectActivity() {
				return InsectActivity;
			}
			/**
			 * @param insectActivity the insectActivity to set
			 */
			public void setInsectActivity(String insectActivity) {
				InsectActivity = insectActivity;
			}
			/**
			 * @return the ants
			 */
			public String getAnts() {
				return Ants;
			}
			/**
			 * @param ants the ants to set
			 */
			public void setAnts(String ants) {
				Ants = ants;
			}
			/**
			 * @return the otherPest
			 */
			public String getOtherPest() {
				return OtherPest;
			}
			/**
			 * @param otherPest the otherPest to set
			 */
			public void setOtherPest(String otherPest) {
				OtherPest = otherPest;
			}
			/**
			 * @return the outsideOnly
			 */
			public String getOutsideOnly() {
				return OutsideOnly;
			}
			/**
			 * @param outsideOnly the outsideOnly to set
			 */
			public void setOutsideOnly(String outsideOnly) {
				OutsideOnly = outsideOnly;
			}
			/**
			 * @return the pestActivity
			 */
			public String getPestActivity() {
				return PestActivity;
			}
			/**
			 * @param pestActivity the pestActivity to set
			 */
			public void setPestActivity(String pestActivity) {
				PestActivity = pestActivity;
			}
			/**
			 * @return the fence
			 */
			public String getFence() {
				return Fence;
			}
			/**
			 * @param fence the fence to set
			 */
			public void setFence(String fence) {
				Fence = fence;
			}
			/**
			 * @return the backYard
			 */
			public String getBackYard() {
				return BackYard;
			}
			/**
			 * @param backYard the backYard to set
			 */
			public void setBackYard(String backYard) {
				BackYard = backYard;
			}
			/**
			 * @return the frontYard
			 */
			public String getFrontYard() {
				return FrontYard;
			}
			/**
			 * @param frontYard the frontYard to set
			 */
			public void setFrontYard(String frontYard) {
				FrontYard = frontYard;
			}
			/**
			 * @return the exteriorPerimeter
			 */
			public String getExteriorPerimeter() {
				return ExteriorPerimeter;
			}
			/**
			 * @param exteriorPerimeter the exteriorPerimeter to set
			 */
			public void setExteriorPerimeter(String exteriorPerimeter) {
				ExteriorPerimeter = exteriorPerimeter;
			}
			/**
			 * @return the roofline
			 */
			public String getRoofline() {
				return Roofline;
			}
			/**
			 * @param roofline the roofline to set
			 */
			public void setRoofline(String roofline) {
				Roofline = roofline;
			}
			/**
			 * @return the garage
			 */
			public String getGarage() {
				return Garage;
			}
			/**
			 * @param garage the garage to set
			 */
			public void setGarage(String garage) {
				Garage = garage;
			}
			/**
			 * @return the attic
			 */
			public String getAttic() {
				return Attic;
			}
			/**
			 * @param attic the attic to set
			 */
			public void setAttic(String attic) {
				Attic = attic;
			}
			/**
			 * @return the interiorHouse
			 */
			public String getInteriorHouse() {
				return InteriorHouse;
			}
			/**
			 * @param interiorHouse the interiorHouse to set
			 */
			public void setInteriorHouse(String interiorHouse) {
				InteriorHouse = interiorHouse;
			}
			/**
			 * @return the basement
			 */
			public String getBasement() {
				return Basement;
			}
			/**
			 * @param basement the basement to set
			 */
			public void setBasement(String basement) {
				Basement = basement;
			}
			/**
			 * @return the pestControl
			 */
			public String getPestControl() {
				return PestControl;
			}
			/**
			 * @param pestControl the pestControl to set
			 */
			public void setPestControl(String pestControl) {
				PestControl = pestControl;
			}
			/**
			 * @return the pestControlServices
			 */
			public String getPestControlServices() {
				return PestControlServices;
			}
			/**
			 * @param pestControlServices the pestControlServices to set
			 */
			public void setPestControlServices(String pestControlServices) {
				PestControlServices = pestControlServices;
			}
			/**
			 * @return the plusTermite
			 */
			public String getPlusTermite() {
				return PlusTermite;
			}
			/**
			 * @param plusTermite the plusTermite to set
			 */
			public void setPlusTermite(String plusTermite) {
				PlusTermite = plusTermite;
			}
			/**
			 * @return the fencePest
			 */
			public String getFencePest() {
				return FencePest;
			}
			/**
			 * @param fencePest the fencePest to set
			 */
			public void setFencePest(String fencePest) {
				FencePest = fencePest;
			}
			/**
			 * @return the fenceActivity
			 */
			public String getFenceActivity() {
				return FenceActivity;
			}
			/**
			 * @param fenceActivity the fenceActivity to set
			 */
			public void setFenceActivity(String fenceActivity) {
				FenceActivity = fenceActivity;
			}
			/**
			 * @return the builderConstruction
			 */
			public String getBuilderConstruction() {
				return BuilderConstruction;
			}
			/**
			 * @param builderConstruction the builderConstruction to set
			 */
			public void setBuilderConstruction(String builderConstruction) {
				BuilderConstruction = builderConstruction;
			}
			/**
			 * @return the builderConstructionActivity
			 */
			public String getBuilderConstructionActivity() {
				return BuilderConstructionActivity;
			}
			/**
			 * @param builderConstructionActivity the builderConstructionActivity to set
			 */
			public void setBuilderConstructionActivity(String builderConstructionActivity) {
				BuilderConstructionActivity = builderConstructionActivity;
			}
			/**
			 * @return the crawlSpaceDoor
			 */
			public String getCrawlSpaceDoor() {
				return CrawlSpaceDoor;
			}
			/**
			 * @param crawlSpaceDoor the crawlSpaceDoor to set
			 */
			public void setCrawlSpaceDoor(String crawlSpaceDoor) {
				CrawlSpaceDoor = crawlSpaceDoor;
			}
			/**
			 * @return the crawlSpaceDoorActivity
			 */
			public String getCrawlSpaceDoorActivity() {
				return CrawlSpaceDoorActivity;
			}
			/**
			 * @param crawlSpaceDoorActivity the crawlSpaceDoorActivity to set
			 */
			public void setCrawlSpaceDoorActivity(String crawlSpaceDoorActivity) {
				CrawlSpaceDoorActivity = crawlSpaceDoorActivity;
			}
			/**
			 * @return the numberPest
			 */
			public String getNumberPest() {
				return NumberPest;
			}
			/**
			 * @param numberPest the numberPest to set
			 */
			public void setNumberPest(String numberPest) {
				NumberPest = numberPest;
			}
			/**
			 * @return the numberText
			 */
			public String getNumberText() {
				return NumberText;
			}
			/**
			 * @param numberText the numberText to set
			 */
			public void setNumberText(String numberText) {
				NumberText = numberText;
			}
			/**
			 * @return the numberActivity
			 */
			public String getNumberActivity() {
				return NumberActivity;
			}
			/**
			 * @param numberActivity the numberActivity to set
			 */
			public void setNumberActivity(String numberActivity) {
				NumberActivity = numberActivity;
			}
			/**
			 * @return the numberActivitytext
			 */
			public String getNumberActivitytext() {
				return NumberActivitytext;
			}
			/**
			 * @param numberActivitytext the numberActivitytext to set
			 */
			public void setNumberActivitytext(String numberActivitytext) {
				NumberActivitytext = numberActivitytext;
			}
			/**
			 * @return the woodPest
			 */
			public String getWoodPest() {
				return WoodPest;
			}
			/**
			 * @param woodPest the woodPest to set
			 */
			public void setWoodPest(String woodPest) {
				WoodPest = woodPest;
			}
			/**
			 * @return the woodOption
			 */
			public String getWoodOption() {
				return WoodOption;
			}
			/**
			 * @param woodOption the woodOption to set
			 */
			public void setWoodOption(String woodOption) {
				WoodOption = woodOption;
			}
			/**
			 * @return the woodActivity
			 */
			public String getWoodActivity() {
				return WoodActivity;
			}
			/**
			 * @param woodActivity the woodActivity to set
			 */
			public void setWoodActivity(String woodActivity) {
				WoodActivity = woodActivity;
			}
			/**
			 * @return the exteriorPest
			 */
			public String getExteriorPest() {
				return ExteriorPest;
			}
			/**
			 * @param exteriorPest the exteriorPest to set
			 */
			public void setExteriorPest(String exteriorPest) {
				ExteriorPest = exteriorPest;
			}
			/**
			 * @return the exteriorActivity
			 */
			public String getExteriorActivity() {
				return ExteriorActivity;
			}
			/**
			 * @param exteriorActivity the exteriorActivity to set
			 */
			public void setExteriorActivity(String exteriorActivity) {
				ExteriorActivity = exteriorActivity;
			}
			/**
			 * @return the atticPest
			 */
			public String getAtticPest() {
				return AtticPest;
			}
			/**
			 * @param atticPest the atticPest to set
			 */
			public void setAtticPest(String atticPest) {
				AtticPest = atticPest;
			}
			/**
			 * @return the atticActivity
			 */
			public String getAtticActivity() {
				return AtticActivity;
			}
			/**
			 * @param atticActivity the atticActivity to set
			 */
			public void setAtticActivity(String atticActivity) {
				AtticActivity = atticActivity;
			}
			/**
			 * @return the interiorPest
			 */
			public String getInteriorPest() {
				return InteriorPest;
			}
			/**
			 * @param interiorPest the interiorPest to set
			 */
			public void setInteriorPest(String interiorPest) {
				InteriorPest = interiorPest;
			}
			/**
			 * @return the interiorActivity
			 */
			public String getInteriorActivity() {
				return InteriorActivity;
			}
			/**
			 * @param interiorActivity the interiorActivity to set
			 */
			public void setInteriorActivity(String interiorActivity) {
				InteriorActivity = interiorActivity;
			}
			/**
			 * @return the treatmentOfSchool
			 */
			public String getTreatmentOfSchool() {
				return TreatmentOfSchool;
			}
			/**
			 * @param treatmentOfSchool the treatmentOfSchool to set
			 */
			public void setTreatmentOfSchool(String treatmentOfSchool) {
				TreatmentOfSchool = treatmentOfSchool;
			}
			/**
			 * @return the monitoringDevices
			 */
			public String getMonitoringDevices() {
				return MonitoringDevices;
			}
			/**
			 * @param monitoringDevices the monitoringDevices to set
			 */
			public void setMonitoringDevices(String monitoringDevices) {
				MonitoringDevices = monitoringDevices;
			}
			/**
			 * @return the numMonitoringDevices
			 */
			public String getNumMonitoringDevices() {
				return NumMonitoringDevices;
			}
			/**
			 * @param numMonitoringDevices the numMonitoringDevices to set
			 */
			public void setNumMonitoringDevices(String numMonitoringDevices) {
				NumMonitoringDevices = numMonitoringDevices;
			}
			/**
			 * @return the basementTermite
			 */
			public String getBasementTermite() {
				return BasementTermite;
			}
			/**
			 * @param basementTermite the basementTermite to set
			 */
			public void setBasementTermite(String basementTermite) {
				BasementTermite = basementTermite;
			}
			/**
			 * @return the numBasementTermite
			 */
			public String getNumBasementTermite() {
				return NumBasementTermite;
			}
			/**
			 * @param numBasementTermite the numBasementTermite to set
			 */
			public void setNumBasementTermite(String numBasementTermite) {
				NumBasementTermite = numBasementTermite;
			}
			/**
			 * @return the additionMonitoringDevice
			 */
			public String getAdditionMonitoringDevice() {
				return AdditionMonitoringDevice;
			}
			/**
			 * @param additionMonitoringDevice the additionMonitoringDevice to set
			 */
			public void setAdditionMonitoringDevice(String additionMonitoringDevice) {
				AdditionMonitoringDevice = additionMonitoringDevice;
			}
			/**
			 * @return the numAdditionMonitoringDevice
			 */
			public String getNumAdditionMonitoringDevice() {
				return NumAdditionMonitoringDevice;
			}
			/**
			 * @param numAdditionMonitoringDevice the numAdditionMonitoringDevice to set
			 */
			public void setNumAdditionMonitoringDevice(String numAdditionMonitoringDevice) {
				NumAdditionMonitoringDevice = numAdditionMonitoringDevice;
			}
			/**
			 * @return the monitoringDeviceRemoved
			 */
			public String getMonitoringDeviceRemoved() {
				return MonitoringDeviceRemoved;
			}
			/**
			 * @param monitoringDeviceRemoved the monitoringDeviceRemoved to set
			 */
			public void setMonitoringDeviceRemoved(String monitoringDeviceRemoved) {
				MonitoringDeviceRemoved = monitoringDeviceRemoved;
			}
			/**
			 * @return the numMonitoringDeviceRemoved
			 */
			public String getNumMonitoringDeviceRemoved() {
				return NumMonitoringDeviceRemoved;
			}
			/**
			 * @param numMonitoringDeviceRemoved the numMonitoringDeviceRemoved to set
			 */
			public void setNumMonitoringDeviceRemoved(String numMonitoringDeviceRemoved) {
				NumMonitoringDeviceRemoved = numMonitoringDeviceRemoved;
			}
			/**
			 * @return the plusRodent
			 */
			public String getPlusRodent() {
				return PlusRodent;
			}
			/**
			 * @param plusRodent the plusRodent to set
			 */
			public void setPlusRodent(String plusRodent) {
				PlusRodent = plusRodent;
			}
			/**
			 * @return the roofRodent
			 */
			public String getRoofRodent() {
				return RoofRodent;
			}
			/**
			 * @param roofRodent the roofRodent to set
			 */
			public void setRoofRodent(String roofRodent) {
				RoofRodent = roofRodent;
			}
			/**
			 * @return the roofVentNeeds
			 */
			public String getRoofVentNeeds() {
				return RoofVentNeeds;
			}
			/**
			 * @param roofVentNeeds the roofVentNeeds to set
			 */
			public void setRoofVentNeeds(String roofVentNeeds) {
				RoofVentNeeds = roofVentNeeds;
			}
			/**
			 * @return the roofVentRodent
			 */
			public String getRoofVentRodent() {
				return RoofVentRodent;
			}
			/**
			 * @param roofVentRodent the roofVentRodent to set
			 */
			public void setRoofVentRodent(String roofVentRodent) {
				RoofVentRodent = roofVentRodent;
			}
			/**
			 * @return the roofNeeds
			 */
			public String getRoofNeeds() {
				return RoofNeeds;
			}
			/**
			 * @param roofNeeds the roofNeeds to set
			 */
			public void setRoofNeeds(String roofNeeds) {
				RoofNeeds = roofNeeds;
			}
			/**
			 * @return the flashing
			 */
			public String getFlashing() {
				return Flashing;
			}
			/**
			 * @param flashing the flashing to set
			 */
			public void setFlashing(String flashing) {
				Flashing = flashing;
			}
			/**
			 * @return the flashingNeeds
			 */
			public String getFlashingNeeds() {
				return FlashingNeeds;
			}
			/**
			 * @param flashingNeeds the flashingNeeds to set
			 */
			public void setFlashingNeeds(String flashingNeeds) {
				FlashingNeeds = flashingNeeds;
			}
			/**
			 * @return the fascia
			 */
			public String getFascia() {
				return Fascia;
			}
			/**
			 * @param fascia the fascia to set
			 */
			public void setFascia(String fascia) {
				Fascia = fascia;
			}
			/**
			 * @return the fasciaNeeds
			 */
			public String getFasciaNeeds() {
				return FasciaNeeds;
			}
			/**
			 * @param fasciaNeeds the fasciaNeeds to set
			 */
			public void setFasciaNeeds(String fasciaNeeds) {
				FasciaNeeds = fasciaNeeds;
			}
			/**
			 * @return the siding
			 */
			public String getSiding() {
				return Siding;
			}
			/**
			 * @param siding the siding to set
			 */
			public void setSiding(String siding) {
				Siding = siding;
			}
			/**
			 * @return the sidingNeeds
			 */
			public String getSidingNeeds() {
				return SidingNeeds;
			}
			/**
			 * @param sidingNeeds the sidingNeeds to set
			 */
			public void setSidingNeeds(String sidingNeeds) {
				SidingNeeds = sidingNeeds;
			}
			/**
			 * @return the breezway
			 */
			public String getBreezway() {
				return Breezway;
			}
			/**
			 * @param breezway the breezway to set
			 */
			public void setBreezway(String breezway) {
				Breezway = breezway;
			}
			/**
			 * @return the breezwayNeeds
			 */
			public String getBreezwayNeeds() {
				return BreezwayNeeds;
			}
			/**
			 * @param breezwayNeeds the breezwayNeeds to set
			 */
			public void setBreezwayNeeds(String breezwayNeeds) {
				BreezwayNeeds = breezwayNeeds;
			}
			/**
			 * @return the garageDoor
			 */
			public String getGarageDoor() {
				return GarageDoor;
			}
			/**
			 * @param garageDoor the garageDoor to set
			 */
			public void setGarageDoor(String garageDoor) {
				GarageDoor = garageDoor;
			}
			/**
			 * @return the garageDoorNeeds
			 */
			public String getGarageDoorNeeds() {
				return GarageDoorNeeds;
			}
			/**
			 * @param garageDoorNeeds the garageDoorNeeds to set
			 */
			public void setGarageDoorNeeds(String garageDoorNeeds) {
				GarageDoorNeeds = garageDoorNeeds;
			}
			/**
			 * @return the dryerVent
			 */
			public String getDryerVent() {
				return DryerVent;
			}
			/**
			 * @param dryerVent the dryerVent to set
			 */
			public void setDryerVent(String dryerVent) {
				DryerVent = dryerVent;
			}
			/**
			 * @return the dryerVentNeeds
			 */
			public String getDryerVentNeeds() {
				return DryerVentNeeds;
			}
			/**
			 * @param dryerVentNeeds the dryerVentNeeds to set
			 */
			public void setDryerVentNeeds(String dryerVentNeeds) {
				DryerVentNeeds = dryerVentNeeds;
			}
			/**
			 * @return the numberLive
			 */
			public String getNumberLive() {
				return NumberLive;
			}
			/**
			 * @param numberLive the numberLive to set
			 */
			public void setNumberLive(String numberLive) {
				NumberLive = numberLive;
			}
			/**
			 * @return the numberLiveNeeds
			 */
			public String getNumberLiveNeeds() {
				return NumberLiveNeeds;
			}
			/**
			 * @param numberLiveNeeds the numberLiveNeeds to set
			 */
			public void setNumberLiveNeeds(String numberLiveNeeds) {
				NumberLiveNeeds = numberLiveNeeds;
			}
			/**
			 * @return the numberSnap
			 */
			public String getNumberSnap() {
				return NumberSnap;
			}
			/**
			 * @param numberSnap the numberSnap to set
			 */
			public void setNumberSnap(String numberSnap) {
				NumberSnap = numberSnap;
			}
			/**
			 * @return the numberSnapNeeds
			 */
			public String getNumberSnapNeeds() {
				return NumberSnapNeeds;
			}
			/**
			 * @param numberSnapNeeds the numberSnapNeeds to set
			 */
			public void setNumberSnapNeeds(String numberSnapNeeds) {
				NumberSnapNeeds = numberSnapNeeds;
			}
			/**
			 * @return the plusMosquito
			 */
			public String getPlusMosquito() {
				return PlusMosquito;
			}
			/**
			 * @param plusMosquito the plusMosquito to set
			 */
			public void setPlusMosquito(String plusMosquito) {
				PlusMosquito = plusMosquito;
			}
			/**
			 * @return the plusMosquitoDetail
			 */
			public String getPlusMosquitoDetail() {
				return PlusMosquitoDetail;
			}
			/**
			 * @param plusMosquitoDetail the plusMosquitoDetail to set
			 */
			public void setPlusMosquitoDetail(String plusMosquitoDetail) {
				PlusMosquitoDetail = plusMosquitoDetail;
			}
			/**
			 * @return the durationTime
			 */
			public String getDurationTime() {
				return DurationTime;
			}
			/**
			 * @param durationTime the durationTime to set
			 */
			public void setDurationTime(String durationTime) {
				DurationTime = durationTime;
			}
			/**
			 * @return the systemRuns
			 */
			public String getSystemRuns() {
				return SystemRuns;
			}
			/**
			 * @param systemRuns the systemRuns to set
			 */
			public void setSystemRuns(String systemRuns) {
				SystemRuns = systemRuns;
			}
			/**
			 * @return the treeBranches
			 */
			public String getTreeBranches() {
				return TreeBranches;
			}
			/**
			 * @param treeBranches the treeBranches to set
			 */
			public void setTreeBranches(String treeBranches) {
				TreeBranches = treeBranches;
			}
			/**
			 * @return the firewood
			 */
			public String getFirewood() {
				return Firewood;
			}
			/**
			 * @param firewood the firewood to set
			 */
			public void setFirewood(String firewood) {
				Firewood = firewood;
			}
			/**
			 * @return the debrisCrawl
			 */
			public String getDebrisCrawl() {
				return DebrisCrawl;
			}
			/**
			 * @param debrisCrawl the debrisCrawl to set
			 */
			public void setDebrisCrawl(String debrisCrawl) {
				DebrisCrawl = debrisCrawl;
			}
			/**
			 * @return the excessivePlant
			 */
			public String getExcessivePlant() {
				return ExcessivePlant;
			}
			/**
			 * @param excessivePlant the excessivePlant to set
			 */
			public void setExcessivePlant(String excessivePlant) {
				ExcessivePlant = excessivePlant;
			}
			/**
			 * @return the soil
			 */
			public String getSoil() {
				return Soil;
			}
			/**
			 * @param soil the soil to set
			 */
			public void setSoil(String soil) {
				Soil = soil;
			}
			/**
			 * @return the woodSoil
			 */
			public String getWoodSoil() {
				return WoodSoil;
			}
			/**
			 * @param woodSoil the woodSoil to set
			 */
			public void setWoodSoil(String woodSoil) {
				WoodSoil = woodSoil;
			}
			/**
			 * @return the debrisRoof
			 */
			public String getDebrisRoof() {
				return DebrisRoof;
			}
			/**
			 * @param debrisRoof the debrisRoof to set
			 */
			public void setDebrisRoof(String debrisRoof) {
				DebrisRoof = debrisRoof;
			}
			/**
			 * @return the standingWater
			 */
			public String getStandingWater() {
				return StandingWater;
			}
			/**
			 * @param standingWater the standingWater to set
			 */
			public void setStandingWater(String standingWater) {
				StandingWater = standingWater;
			}
			/**
			 * @return the moistureProblem
			 */
			public String getMoistureProblem() {
				return MoistureProblem;
			}
			/**
			 * @param moistureProblem the moistureProblem to set
			 */
			public void setMoistureProblem(String moistureProblem) {
				MoistureProblem = moistureProblem;
			}
			/**
			 * @return the openings
			 */
			public String getOpenings() {
				return Openings;
			}
			/**
			 * @param openings the openings to set
			 */
			public void setOpenings(String openings) {
				Openings = openings;
			}
			/**
			 * @return the excessiveGaps
			 */
			public String getExcessiveGaps() {
				return ExcessiveGaps;
			}
			/**
			 * @param excessiveGaps the excessiveGaps to set
			 */
			public void setExcessiveGaps(String excessiveGaps) {
				ExcessiveGaps = excessiveGaps;
			}
			/**
			 * @return the leakyPlumbing
			 */
			public String getLeakyPlumbing() {
				return LeakyPlumbing;
			}
			/**
			 * @param leakyPlumbing the leakyPlumbing to set
			 */
			public void setLeakyPlumbing(String leakyPlumbing) {
				LeakyPlumbing = leakyPlumbing;
			}
			/**
			 * @return the garbageCans
			 */
			public String getGarbageCans() {
				return GarbageCans;
			}
			/**
			 * @param garbageCans the garbageCans to set
			 */
			public void setGarbageCans(String garbageCans) {
				GarbageCans = garbageCans;
			}
			/**
			 * @return the moistureDamaged
			 */
			public String getMoistureDamaged() {
				return MoistureDamaged;
			}
			/**
			 * @param moistureDamaged the moistureDamaged to set
			 */
			public void setMoistureDamaged(String moistureDamaged) {
				MoistureDamaged = moistureDamaged;
			}
			/**
			 * @return the groceryBags
			 */
			public String getGroceryBags() {
				return GroceryBags;
			}
			/**
			 * @param groceryBags the groceryBags to set
			 */
			public void setGroceryBags(String groceryBags) {
				GroceryBags = groceryBags;
			}
			/**
			 * @return the petFood
			 */
			public String getPetFood() {
				return PetFood;
			}
			/**
			 * @param petFood the petFood to set
			 */
			public void setPetFood(String petFood) {
				PetFood = petFood;
			}
			/**
			 * @return the recycledItems
			 */
			public String getRecycledItems() {
				return RecycledItems;
			}
			/**
			 * @param recycledItems the recycledItems to set
			 */
			public void setRecycledItems(String recycledItems) {
				RecycledItems = recycledItems;
			}
			/**
			 * @return the excessiveStorage
			 */
			public String getExcessiveStorage() {
				return ExcessiveStorage;
			}
			/**
			 * @param excessiveStorage the excessiveStorage to set
			 */
			public void setExcessiveStorage(String excessiveStorage) {
				ExcessiveStorage = excessiveStorage;
			}
			/**
			 * @return the technicianComments
			 */
			public String getTechnicianComments() {
				return TechnicianComments;
			}
			/**
			 * @param technicianComments the technicianComments to set
			 */
			public void setTechnicianComments(String technicianComments) {
				TechnicianComments = technicianComments;
			}
			/**
			 * @return the timeIn
			 */
			public String getTimeIn() {
				return TimeIn;
			}
			/**
			 * @param timeIn the timeIn to set
			 */
			public void setTimeIn(String timeIn) {
				TimeIn = timeIn;
			}
			/**
			 * @return the timeout
			 */
			public String getTimeout() {
				return Timeout;
			}
			/**
			 * @param timeout the timeout to set
			 */
			public void setTimeout(String timeout) {
				Timeout = timeout;
			}
			/**
			 * @return the timeInDateTime
			 */
			public String getTimeInDateTime() {
				return TimeInDateTime;
			}
			/**
			 * @param timeInDateTime the timeInDateTime to set
			 */
			public void setTimeInDateTime(String timeInDateTime) {
				TimeInDateTime = timeInDateTime;
			}
			/**
			 * @return the timeOutDateTime
			 */
			public String getTimeOutDateTime() {
				return TimeOutDateTime;
			}
			/**
			 * @param timeOutDateTime the timeOutDateTime to set
			 */
			public void setTimeOutDateTime(String timeOutDateTime) {
				TimeOutDateTime = timeOutDateTime;
			}
			/**
			 * @return the technician
			 */
			public String getTechnician() {
				return Technician;
			}
			/**
			 * @param technician the technician to set
			 */
			public void setTechnician(String technician) {
				Technician = technician;
			}
			/**
			 * @return the empNo
			 */
			public String getEmpNo() {
				return EmpNo;
			}
			/**
			 * @param empNo the empNo to set
			 */
			public void setEmpNo(String empNo) {
				EmpNo = empNo;
			}
			/**
			 * @return the customer
			 */
			public String getCustomer() {
				return Customer;
			}
			/**
			 * @param customer the customer to set
			 */
			public void setCustomer(String customer) {
				Customer = customer;
			}
			/**
			 * @return the date
			 */
			public String getDate() {
				return Date;
			}
			/**
			 * @param date the date to set
			 */
			public void setDate(String date) {
				Date = date;
			}
			/**
			 * @return the paymentType
			 */
			public String getPaymentType() {
				return PaymentType;
			}
			/**
			 * @param paymentType the paymentType to set
			 */
			public void setPaymentType(String paymentType) {
				PaymentType = paymentType;
			}
			/**
			 * @return the amount
			 */
			public String getAmount() {
				return Amount;
			}
			/**
			 * @param amount the amount to set
			 */
			public void setAmount(String amount) {
				Amount = amount;
			}
			/**
			 * @return the checkNo
			 */
			public String getCheckNo() {
				return CheckNo;
			}
			/**
			 * @param checkNo the checkNo to set
			 */
			public void setCheckNo(String checkNo) {
				CheckNo = checkNo;
			}
			/**
			 * @return the licenseNo
			 */
			public String getLicenseNo() {
				return LicenseNo;
			}
			/**
			 * @param licenseNo the licenseNo to set
			 */
			public void setLicenseNo(String licenseNo) {
				LicenseNo = licenseNo;
			}
			/**
			 * @return the signature
			 */
			public String getSignature() {
				return Signature;
			}
			/**
			 * @param signature the signature to set
			 */
			public void setSignature(String signature) {
				Signature = signature;
			}
			/**
			 * @return the serviceSignature
			 */
			public String getServiceSignature() {
				return ServiceSignature;
			}
			/**
			 * @param serviceSignature the serviceSignature to set
			 */
			public void setServiceSignature(String serviceSignature) {
				ServiceSignature = serviceSignature;
			}
			/**
			 * @return the create_By
			 */
			public String getCreate_By() {
				return Create_By;
			}
			/**
			 * @param create_By the create_By to set
			 */
			public void setCreate_By(String create_By) {
				Create_By = create_By;
			}
			/**
			 * @return the update_By
			 */
			public String getUpdate_By() {
				return Update_By;
			}
			/**
			 * @param update_By the update_By to set
			 */
			public void setUpdate_By(String update_By) {
				Update_By = update_By;
			}
			/**
			 * @return the create_date
			 */
			public String getCreate_date() {
				return Create_date;
			}
			/**
			 * @param create_date the create_date to set
			 */
			public void setCreate_date(String create_date) {
				Create_date = create_date;
			}
			/**
			 * @return the update_Date
			 */
			public String getUpdate_Date() {
				return Update_Date;
			}
			/**
			 * @param update_Date the update_Date to set
			 */
			public void setUpdate_Date(String update_Date) {
				Update_Date = update_Date;
			}
			/**
			 * @return the agreementPath
			 */
			public String getAgreementPath() {
				return AgreementPath;
			}
			/**
			 * @param agreementPath the agreementPath to set
			 */
			public void setAgreementPath(String agreementPath) {
				AgreementPath = agreementPath;
			}
			
			

}
