package quacito.houston.model;

import android.graphics.Bitmap;
import android.net.Uri;

public class ImgDto 
{
	private int imagedrawable;
	private Bitmap imageBitMap;
private String Iname;
private String IString;
private Uri imageUri;
private String imageurl;
public String getImageurl() {
	return imageurl;
}
public void setImageurl(String imageurl) {
	this.imageurl = imageurl;
}


public Bitmap getImageBitMap() {
	return imageBitMap;
}
public void setImageBitMap(Bitmap imageBitMap) {
	this.imageBitMap = imageBitMap;
}
public int getImagedrawable() {
	return imagedrawable;
}
public void setImagedrawable(int imagedrawable) {
	this.imagedrawable = imagedrawable;
}
public Uri getImageUri() {
	return imageUri;
}
public void setImageUri(Uri imageUri) {
	this.imageUri = imageUri;
}
public String getIname() {
	return Iname;
}
public void setIname(String iname) {
	Iname = iname;
}
public String getIString() {
	return IString;
}
public void setIString(String iString) {
	IString = iString;
}
}
