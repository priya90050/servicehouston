package quacito.houston.model;

public class PreventionMasterDTO 
{
	
	
	private String PreventationID="";
	private String Product="";
	private String PercentPest="";
	private String Status="";
	private String EPA_RegNo="";
	private String Unit="";
	public String getPreventationID() {
		return PreventationID;
	}
	public void setPreventationID(String preventationID) {
		PreventationID = preventationID;
	}
	public String getProduct() {
		return Product;
	}
	public void setProduct(String product) {
		Product = product;
	}
	public String getPercentPest() {
		return PercentPest;
	}
	public void setPercentPest(String percentPest) {
		PercentPest = percentPest;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getEPA_RegNo() {
		return EPA_RegNo;
	}
	public void setEPA_RegNo(String ePA_RegNo) {
		EPA_RegNo = ePA_RegNo;
	}
	public String getUnit() {
		return Unit;
	}
	public void setUnit(String unit) {
		Unit = unit;
	}
	
	

}
