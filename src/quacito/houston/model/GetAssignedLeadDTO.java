package quacito.houston.model;

public class GetAssignedLeadDTO {
	private String AssignForDayLead_ID="";
	private String Assign_Date="";
	private String Created_By="";
	private String Created_Date="";
	private String Emp_ID="";
	private String Route_ID="";
	private String Status="";
	private String Updated_By="";
	private String Updated_Date="";
	/**
	 * @return the assignForDayLead_ID
	 */
	public String getAssignForDayLead_ID() {
		return AssignForDayLead_ID;
	}
	/**
	 * @param assignForDayLead_ID the assignForDayLead_ID to set
	 */
	public void setAssignForDayLead_ID(String assignForDayLead_ID) {
		AssignForDayLead_ID = assignForDayLead_ID;
	}
	/**
	 * @return the assign_Date
	 */
	public String getAssign_Date() {
		return Assign_Date;
	}
	/**
	 * @param assign_Date the assign_Date to set
	 */
	public void setAssign_Date(String assign_Date) {
		Assign_Date = assign_Date;
	}
	/**
	 * @return the created_By
	 */
	public String getCreated_By() {
		return Created_By;
	}
	/**
	 * @param created_By the created_By to set
	 */
	public void setCreated_By(String created_By) {
		Created_By = created_By;
	}
	/**
	 * @return the created_Date
	 */
	public String getCreated_Date() {
		return Created_Date;
	}
	/**
	 * @param created_Date the created_Date to set
	 */
	public void setCreated_Date(String created_Date) {
		Created_Date = created_Date;
	}
	/**
	 * @return the emp_ID
	 */
	public String getEmp_ID() {
		return Emp_ID;
	}
	/**
	 * @param emp_ID the emp_ID to set
	 */
	public void setEmp_ID(String emp_ID) {
		Emp_ID = emp_ID;
	}
	/**
	 * @return the route_ID
	 */
	public String getRoute_ID() {
		return Route_ID;
	}
	/**
	 * @param route_ID the route_ID to set
	 */
	public void setRoute_ID(String route_ID) {
		Route_ID = route_ID;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return Status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		Status = status;
	}
	/**
	 * @return the updated_By
	 */
	public String getUpdated_By() {
		return Updated_By;
	}
	/**
	 * @param updated_By the updated_By to set
	 */
	public void setUpdated_By(String updated_By) {
		Updated_By = updated_By;
	}
	/**
	 * @return the updated_Date
	 */
	public String getUpdated_Date() {
		return Updated_Date;
	}
	/**
	 * @param updated_Date the updated_Date to set
	 */
	public void setUpdated_Date(String updated_Date) {
		Updated_Date = updated_Date;
	}
	
	
}
