package quacito.houston.model;

public class ChemicalDto 
{
	private String Amount="";
	private String OtherText="";
	private String PercentPest="";
	private String PestPrevention_ID="";
	private String PreventationID="";
	private String Product="";
	private String ProductCheck;
	private String ServiceID_new="";
	private String Status="";
	private String Target="";
	private String Unit="";
	private String Create_By ="";
	private String Create_Date ="";
	private String isCompleted="";
	
	
	
	public String getIsCompleted() {
		return isCompleted;
	}
	public void setIsCompleted(String isCompleted) {
		this.isCompleted = isCompleted;
	}
	/**
	 * @return the create_Date
	 */
	public String getCreate_Date() {
		return Create_Date;
	}
	/**
	 * @param create_Date the create_Date to set
	 */
	public void setCreate_Date(String create_Date) {
		Create_Date = create_Date;
	}
	/**
	 * @return the amount
	 */
	
	
	public String getAmount() {
		return Amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		Amount = amount;
	}
	/**
	 * @return the create_By
	 */
	public String getCreate_By() {
		return Create_By;
	}
	/**
	 * @param create_By the create_By to set
	 */
	public void setCreate_By(String create_By) {
		Create_By = create_By;
	}
	/**
	 * @return the otherText
	 */
	public String getOtherText() {
		return OtherText;
	}
	/**
	 * @param otherText the otherText to set
	 */
	public void setOtherText(String otherText) {
		OtherText = otherText;
	}
	/**
	 * @return the percentPes
	 */
	public String getPercentPes() {
		return PercentPest;
	}
	/**
	 * @param percentPes the percentPes to set
	 */
	public void setPercentPes(String percentPes) {
		PercentPest = percentPes;
	}
	/**
	 * @return the pestPrevention_ID
	 */
	public String getPestPrevention_ID() {
		return PestPrevention_ID;
	}
	/**
	 * @param pestPrevention_ID the pestPrevention_ID to set
	 */
	public void setPestPrevention_ID(String pestPrevention_ID) {
		PestPrevention_ID = pestPrevention_ID;
	}
	/**
	 * @return the preventationID
	 */
	public String getPreventationID() {
		return PreventationID;
	}
	/**
	 * @param preventationID the preventationID to set
	 */
	public void setPreventationID(String preventationID) {
		PreventationID = preventationID;
	}
	/**
	 * @return the product
	 */
	public String getProduct() {
		return Product;
	}
	/**
	 * @param product the product to set
	 */
	public void setProduct(String product) {
		Product = product;
	}
	/**
	 * @return the productCheck
	 */
	/*public Boolean getProductCheck() {
		return ProductCheck;
	}
	*//**
	 * @param productCheck the productCheck to set
	 *//*
	public void setProductCheck(Boolean productCheck) {
		ProductCheck = productCheck;
	}*/
	/**
	 * @return the serviceID_new
	 */
	public String getServiceID_new() {
		return ServiceID_new;
	}
	/**
	 * @return the percentPest
	 */
	/*public String getPercentPest() {
		return PercentPest;
	}
	*//**
	 * @param percentPest the percentPest to set
	 *//*
	public void setPercentPest(String percentPest) {
		PercentPest = percentPest;
	}*/
	/**
	 * @return the productCheck
	 */
	public String getProductCheck() {
		return ProductCheck;
	}
	/**
	 * @param productCheck the productCheck to set
	 */
	public void setProductCheck(String productCheck) {
		ProductCheck = productCheck;
	}
	/**
	 * @param serviceID_new the serviceID_new to set
	 */
	public void setServiceID_new(String serviceID_new) {
		ServiceID_new = serviceID_new;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return Status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		Status = status;
	}
	/**
	 * @return the target
	 */
	public String getTarget() {
		return Target;
	}
	/**
	 * @param target the target to set
	 */
	public void setTarget(String target) {
		Target = target;
	}
	/**
	 * @return the unit
	 */
	public String getUnit() {
		return Unit;
	}
	/**
	 * @param unit the unit to set
	 */
	public void setUnit(String unit) {
		Unit = unit;
	}
	
	
}
