package quacito.houston.model;

public class AddressDTO 
{
	private String Row_Id="";
	private String ServiceID_new="";
	private String Address_Line1="";
	private String Address_Line2="";
	private String City="";
	private String State="";
	private String ZipCode="";
	private String Poc_FName="";
	private String Poc_LName="";
	private String BAddress_Line1="";
	private String BAddress_Line2="";
	private String BCity="";
	private String BState="";
	private String BZipCode="";
	private String ServiceLatitude="";
	private String ServiceLongitude="";
	private String Create_By="";
	private String Update_By="";
	private String Create_Date="";
	private String Update_Date="";
	private String Created_By="";
	private String Updated_By="";
	public String getRow_Id() {
		return Row_Id;
	}
	public void setRow_Id(String row_Id) {
		Row_Id = row_Id;
	}
	public String getServiceID_new() {
		return ServiceID_new;
	}
	public void setServiceID_new(String serviceID_new) {
		ServiceID_new = serviceID_new;
	}
	public String getAddress_Line1() {
		return Address_Line1;
	}
	public void setAddress_Line1(String address_Line1) {
		Address_Line1 = address_Line1;
	}
	public String getAddress_Line2() {
		return Address_Line2;
	}
	public void setAddress_Line2(String address_Line2) {
		Address_Line2 = address_Line2;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getZipCode() {
		return ZipCode;
	}
	public void setZipCode(String zipCode) {
		ZipCode = zipCode;
	}
	public String getPoc_FName() {
		return Poc_FName;
	}
	public void setPoc_FName(String poc_FName) {
		Poc_FName = poc_FName;
	}
	public String getPoc_LName() {
		return Poc_LName;
	}
	public void setPoc_LName(String poc_LName) {
		Poc_LName = poc_LName;
	}
	public String getBAddress_Line1() {
		return BAddress_Line1;
	}
	public void setBAddress_Line1(String bAddress_Line1) {
		BAddress_Line1 = bAddress_Line1;
	}
	public String getBAddress_Line2() {
		return BAddress_Line2;
	}
	public void setBAddress_Line2(String bAddress_Line2) {
		BAddress_Line2 = bAddress_Line2;
	}
	public String getBCity() {
		return BCity;
	}
	public void setBCity(String bCity) {
		BCity = bCity;
	}
	public String getBState() {
		return BState;
	}
	public void setBState(String bState) {
		BState = bState;
	}
	public String getBZipCode() {
		return BZipCode;
	}
	public void setBZipCode(String bZipCode) {
		BZipCode = bZipCode;
	}
	public String getServiceLatitude() {
		return ServiceLatitude;
	}
	public void setServiceLatitude(String serviceLatitude) {
		ServiceLatitude = serviceLatitude;
	}
	public String getServiceLongitude() {
		return ServiceLongitude;
	}
	public void setServiceLongitude(String serviceLongitude) {
		ServiceLongitude = serviceLongitude;
	}
	public String getCreate_By() {
		return Create_By;
	}
	public void setCreate_By(String create_By) {
		Create_By = create_By;
	}
	public String getUpdate_By() {
		return Update_By;
	}
	public void setUpdate_By(String update_By) {
		Update_By = update_By;
	}
	public String getCreate_Date() {
		return Create_Date;
	}
	public void setCreate_Date(String create_Date) {
		Create_Date = create_Date;
	}
	public String getUpdate_Date() {
		return Update_Date;
	}
	public void setUpdate_Date(String update_Date) {
		Update_Date = update_Date;
	}
	public String getCreated_By() {
		return Created_By;
	}
	public void setCreated_By(String created_By) {
		Created_By = created_By;
	}
	public String getUpdated_By() {
		return Updated_By;
	}
	public void setUpdated_By(String updated_By) {
		Updated_By = updated_By;
	}
	
	
}
