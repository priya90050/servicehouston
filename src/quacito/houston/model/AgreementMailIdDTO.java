package quacito.houston.model;

public class AgreementMailIdDTO
{
	//private int RowId=0;
	private int Id;
	private String Member_Id;
	private String AgreementId;
	private String EmailId;
	private String MailType;
	private String Subject;
	private String IsCustomerEmail;
	private String Create_By;
	private String Create_date;
	private boolean isSelected;
	
	
	public boolean isSelected() {
		return isSelected;
	}
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	/*public int getRowId() {
		return RowId;
	}
	public void setRowId(int rowId) {
		RowId = rowId;
	}*/
	public String getMember_Id() {
		return Member_Id;
	}
	public void setMember_Id(String member_Id) {
		Member_Id = member_Id;
	}
	public String getAgreementId() {
		return AgreementId;
	}
	public void setAgreementId(String agreementId) {
		AgreementId = agreementId;
	}
	public String getEmailId() {
		return EmailId;
	}
	public void setEmailId(String emailId) {
		EmailId = emailId;
	}
	public String getMailType() {
		return MailType;
	}
	public void setMailType(String mailType) {
		MailType = mailType;
	}
	public String getSubject() {
		return Subject;
	}
	public void setSubject(String subject) {
		Subject = subject;
	}
	public String getIsCustomerEmail() {
		return IsCustomerEmail;
	}
	public void setIsCustomerEmail(String isCustomerEmail) {
		IsCustomerEmail = isCustomerEmail;
	}
	public String getCreate_By() {
		return Create_By;
	}
	public void setCreate_By(String create_By) {
		Create_By = create_By;
	}
	public String getCreate_date() {
		return Create_date;
	}
	public void setCreate_date(String create_date) {
		Create_date = create_date;
	}
	
	
	
}
