package quacito.houston.model;

public class GetRouteCrewDTO {
	private String Created_By="";
	private String Created_Date="";
	private String Route_Crew_ID="";
	private String Route_ID="";
	private String Status="";
	private String Technician_ID="";
	private String Updated_By="";
	private String Updated_Date="";
	/**
	 * @return the created_By
	 */
	public String getCreated_By() {
		return Created_By;
	}
	/**
	 * @param created_By the created_By to set
	 */
	public void setCreated_By(String created_By) {
		Created_By = created_By;
	}
	/**
	 * @return the created_Date
	 */
	public String getCreated_Date() {
		return Created_Date;
	}
	/**
	 * @param created_Date the created_Date to set
	 */
	public void setCreated_Date(String created_Date) {
		Created_Date = created_Date;
	}
	/**
	 * @return the route_Crew_ID
	 */
	public String getRoute_Crew_ID() {
		return Route_Crew_ID;
	}
	/**
	 * @param route_Crew_ID the route_Crew_ID to set
	 */
	public void setRoute_Crew_ID(String route_Crew_ID) {
		Route_Crew_ID = route_Crew_ID;
	}
	/**
	 * @return the route_ID
	 */
	public String getRoute_ID() {
		return Route_ID;
	}
	/**
	 * @param route_ID the route_ID to set
	 */
	public void setRoute_ID(String route_ID) {
		Route_ID = route_ID;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return Status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		Status = status;
	}
	/**
	 * @return the technician_ID
	 */
	public String getTechnician_ID() {
		return Technician_ID;
	}
	/**
	 * @param technician_ID the technician_ID to set
	 */
	public void setTechnician_ID(String technician_ID) {
		Technician_ID = technician_ID;
	}
	/**
	 * @return the updated_By
	 */
	public String getUpdated_By() {
		return Updated_By;
	}
	/**
	 * @param updated_By the updated_By to set
	 */
	public void setUpdated_By(String updated_By) {
		Updated_By = updated_By;
	}
	/**
	 * @return the updated_Date
	 */
	public String getUpdated_Date() {
		return Updated_Date;
	}
	/**
	 * @param updated_Date the updated_Date to set
	 */
	public void setUpdated_Date(String updated_Date) {
		Updated_Date = updated_Date;
	}
	
	
}
