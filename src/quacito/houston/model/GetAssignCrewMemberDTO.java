package quacito.houston.model;

public class GetAssignCrewMemberDTO {
	
	
	private String AssignForDayCrewMember_ID;
	private String AssignForDayLeadID;
	private String Created_By;
	private String Created_Date;
	private String CrewMember_ID;
	private String Status;
	private String Updated_By;
	private String Updated_Date;
	/**
	 * @return the assignForDayCrewMember_ID
	 */
	public String getAssignForDayCrewMember_ID() {
		return AssignForDayCrewMember_ID;
	}
	/**
	 * @param assignForDayCrewMember_ID the assignForDayCrewMember_ID to set
	 */
	public void setAssignForDayCrewMember_ID(String assignForDayCrewMember_ID) {
		AssignForDayCrewMember_ID = assignForDayCrewMember_ID;
	}
	/**
	 * @return the assignForDayLeadID
	 */
	public String getAssignForDayLeadID() {
		return AssignForDayLeadID;
	}
	/**
	 * @param assignForDayLeadID the assignForDayLeadID to set
	 */
	public void setAssignForDayLeadID(String assignForDayLeadID) {
		AssignForDayLeadID = assignForDayLeadID;
	}
	/**
	 * @return the created_By
	 */
	public String getCreated_By() {
		return Created_By;
	}
	/**
	 * @param created_By the created_By to set
	 */
	public void setCreated_By(String created_By) {
		Created_By = created_By;
	}
	/**
	 * @return the created_Date
	 */
	public String getCreated_Date() {
		return Created_Date;
	}
	/**
	 * @param created_Date the created_Date to set
	 */
	public void setCreated_Date(String created_Date) {
		Created_Date = created_Date;
	}
	/**
	 * @return the crewMember_ID
	 */
	public String getCrewMember_ID() {
		return CrewMember_ID;
	}
	/**
	 * @param crewMember_ID the crewMember_ID to set
	 */
	public void setCrewMember_ID(String crewMember_ID) {
		CrewMember_ID = crewMember_ID;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return Status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		Status = status;
	}
	/**
	 * @return the updated_By
	 */
	public String getUpdated_By() {
		return Updated_By;
	}
	/**
	 * @param updated_By the updated_By to set
	 */
	public void setUpdated_By(String updated_By) {
		Updated_By = updated_By;
	}
	/**
	 * @return the updated_Date
	 */
	public String getUpdated_Date() {
		return Updated_Date;
	}
	/**
	 * @param updated_Date the updated_Date to set
	 */
	public void setUpdated_Date(String updated_Date) {
		Updated_Date = updated_Date;
	}
	

}
