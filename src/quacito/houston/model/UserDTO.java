package quacito.houston.model;
import java.io.Serializable;
public class UserDTO implements Serializable
{
	private int id;
	private int EmployeeId;
	private String Assigned_WO="";
	private String Username="";
	private String Password="";
	private String FirstName="";
	private String MiddleName="";
	private String LastName="";
	private int RoleId;
	private String LicenceNo="";
	private String RoleName="";
	private String Emailid="";
	private String PestPackId="";
	private int DepartmentId;
	private String AccountNo="";
	private String HomePhone="";
	private String CellPhone="";
	private String Address1="";
	private String Address2="";
	private String City="";
	private String State="";
	private String ZipCode="";
	private String Status="";
	private int SupervisorId;
	private String Create_Date="";
	private String Update_Date="";
	
	
	/**
	 * @return the licenceNo
	 */
	public String getLicenceNo() {
		return LicenceNo;
	}
	/**
	 * @return the assigned_WO
	 */
	public String getAssigned_WO() {
		return Assigned_WO;
	}
	/**
	 * @param assigned_WO the assigned_WO to set
	 */
	public void setAssigned_WO(String assigned_WO) {
		Assigned_WO = assigned_WO;
	}
	/**
	 * @param licenceNo the licenceNo to set
	 */
	public void setLicenceNo(String licenceNo) {
		LicenceNo = licenceNo;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getEmployeeId() {
		return EmployeeId;
	}
	public void setEmployeeId(int employeeId) {
		EmployeeId = employeeId;
	}
	public String getUsername() {
		return Username;
	}
	public void setUsername(String username) {
		Username = username;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getMiddleName() {
		return MiddleName;
	}
	public void setMiddleName(String middleName) {
		MiddleName = middleName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public int getRoleId() {
		return RoleId;
	}
	public void setRoleId(int roleId) {
		RoleId = roleId;
	}
	public String getRoleName() {
		return RoleName;
	}
	public void setRoleName(String roleName) {
		RoleName = roleName;
	}
	public String getEmailid() {
		return Emailid;
	}
	public void setEmailid(String emailid) {
		Emailid = emailid;
	}
	public String getPestPackId() {
		return PestPackId;
	}
	public void setPestPackId(String pestPackId) {
		PestPackId = pestPackId;
	}
	public int getDepartmentId() {
		return DepartmentId;
	}
	public void setDepartmentId(int departmentId) {
		DepartmentId = departmentId;
	}
	public String getAccountNo() {
		return AccountNo;
	}
	public void setAccountNo(String accountNo) {
		AccountNo = accountNo;
	}
	public String getHomePhone() {
		return HomePhone;
	}
	public void setHomePhone(String homePhone) {
		HomePhone = homePhone;
	}
	public String getCellPhone() {
		return CellPhone;
	}
	public void setCellPhone(String cellPhone) {
		CellPhone = cellPhone;
	}
	public String getAddress1() {
		return Address1;
	}
	public void setAddress1(String address1) {
		Address1 = address1;
	}
	public String getAddress2() {
		return Address2;
	}
	public void setAddress2(String address2) {
		Address2 = address2;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getZipCode() {
		return ZipCode;
	}
	public void setZipCode(String zipCode) {
		ZipCode = zipCode;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public int getSupervisorId() {
		return SupervisorId;
	}
	public void setSupervisorId(int supervisorId) {
		SupervisorId = supervisorId;
	}
	public String getCreate_Date() {
		return Create_Date;
	}
	public void setCreate_Date(String create_Date) {
		Create_Date = create_Date;
	}
	public String getUpdate_Date() {
		return Update_Date;
	}
	public void setUpdate_Date(String update_Date) {
		Update_Date = update_Date;
	}
	
}
