package quacito.houston.model;

public class PreventionDTO
{
	private String PestPrevention_ID="";
	private String PreventationID="";
	private String ServiceID_new="";
	private String ProductCheck="";
	private String PercentPest="";
	private String Amount="";
	private String Target="";
	private String Unit="";
	private String SAA="";
	private String Method="";
	private String OtherText="";
	private String Create_By="";
	private String Update_By="";
	private String Create_Date="";
	private String Update_Date="";
	public String getPestPrevention_ID() {
		return PestPrevention_ID;
	}
	public void setPestPrevention_ID(String pestPrevention_ID) {
		PestPrevention_ID = pestPrevention_ID;
	}
	public String getPreventationID() {
		return PreventationID;
	}
	public void setPreventationID(String preventationID) {
		PreventationID = preventationID;
	}
	public String getServiceID_new() {
		return ServiceID_new;
	}
	public void setServiceID_new(String serviceID_new) {
		ServiceID_new = serviceID_new;
	}
	public String getProductCheck() {
		return ProductCheck;
	}
	public void setProductCheck(String productCheck) {
		ProductCheck = productCheck;
	}
	public String getPercentPest() {
		return PercentPest;
	}
	public void setPercentPest(String percentPest) {
		PercentPest = percentPest;
	}
	public String getAmount() {
		return Amount;
	}
	public void setAmount(String amount) {
		Amount = amount;
	}
	public String getTarget() {
		return Target;
	}
	public void setTarget(String target) {
		Target = target;
	}
	public String getUnit() {
		return Unit;
	}
	public void setUnit(String unit) {
		Unit = unit;
	}
	public String getSAA() {
		return SAA;
	}
	public void setSAA(String sAA) {
		SAA = sAA;
	}
	public String getMethod() {
		return Method;
	}
	public void setMethod(String method) {
		Method = method;
	}
	public String getOtherText() {
		return OtherText;
	}
	public void setOtherText(String otherText) {
		OtherText = otherText;
	}
	public String getCreate_By() {
		return Create_By;
	}
	public void setCreate_By(String create_By) {
		Create_By = create_By;
	}
	public String getUpdate_By() {
		return Update_By;
	}
	public void setUpdate_By(String update_By) {
		Update_By = update_By;
	}
	public String getCreate_Date() {
		return Create_Date;
	}
	public void setCreate_Date(String create_Date) {
		Create_Date = create_Date;
	}
	public String getUpdate_Date() {
		return Update_Date;
	}
	public void setUpdate_Date(String update_Date) {
		Update_Date = update_Date;
	}
	
	
}
