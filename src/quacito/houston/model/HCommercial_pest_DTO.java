package quacito.houston.model;

public class HCommercial_pest_DTO {
	private String CommercialId="";
	private String isCompleted ="";
	private String ServiceID_new="";
	private String ServiceFor="";
	private String AreasInspectedInterior="";
	private String AreasInspectedExterior="";
	private String InsectActivity="";
	private String Ants="";
	private String OtherPest="";
	private String PestActivity="";
	private String Interior="";
	private String Outsidef_Perimeter="";
	private String Dumpster="";
	private String Dining_Room="";
	private String Common_Areas="";
	private String Kitchen="";
	private String Dry_Storage="";
	private String Dish_Washing="";
	private String Roof_Tops="";
	private String Wait_Stations="";
	private String Drop_Ceiling="";
	private String Planters="";
	private String Warehouse_Storage="";
	private String PestControl="";
	private String PestControlServices="";
	private String PlusTermite="";
	private String SignOfTermiteMS="";
	private String SignOfTermiteActivityMS="";
	private String SignOfTermiteText="";
	private String InstallMS="";
	private String InstallMSText="";
	private String SignOfTermitePS="";
	private String SignOfTermiteActivityPS="";
	private String SignOfTermiteIS="";
	private String SignOfTermiteActivityIS="";
	private String ReplacedMS="";
	private String ReplacedActivityTextMS="";
	private String ReplacedTextMS="";
	private String TreatedPS="";
	private String TreatedActivityPS="";
	private String TreatedSP="";
	private String TreatedActivitySP="";
	private String TreatedCC="";
	private String TreatedActivityCC="";
	private String PlusRodent="";
	private String InspectedRodentStation="";
	private String txtInspectedRodentStation="";
	private String CleanedRodentStation="";
	private String IsCleanedRodentStation="";
	private String InstallRodentBaitStation="";
	private String IsInstallRodentBaitStation="";
	private String txtInstallRodentBaitStation="";
	private String ReplacedBait="";
	private String IsReplacedBait="";
	private String SNoReplacedBait="";
	private String ReplacedBaitStation="";
	private String IsReplacedBaitStation="";
	private String txtReplacedBaitStation="";
	private String SNoReplacedBaitStation="";
	private String InspectedRodentTraps="";
	private String IsInspectedRodentTraps="";
	private String CleanedRodentTrap="";
	private String IsCleanedRodentTrap="";
	private String InstallRodentTrap="";
	private String IsInstallRodentTrap="";
	private String txtInstallRodentTrap="";
	private String ReplacedRodentTrap="";
	private String IsReplacedRodentTrap="";
	private String txtReplacedRodentTrap="";
	private String SNoReplacedRodentTrap="";
	private String SealedEntryPoint="";
	private String IsSealedEntryPoint="";
	private String InspectedSnapTrap="";
	private String txtInspectedSnapTrap="";
	private String InspectedLiveCages="";
	private String txtInspectedLiveCages="";
	/*private String InspectedSnapTraps="";
	private String txtInspectedSnapTraps="";*/
	private String RemovedSnapTraps="";
	private String txtRemovedSnapTraps="";
	private String RemovedLiveCages="";
	private String txtRemovedLiveCages="";
	private String RemovedRodentBaitStation="";
	private String txtRemovedRodentBaitStation="";
	private String RemovedRodentTrap="";
	private String txtRemovedRodentTrap="";
	private String SetSnapTrap="";
	private String txtSetSnapTrap="";
	private String LstSetSnapTrap="";
	private String SetLiveCages="";
	private String txtSetLiveCages="";
	private String LstSetLiveCages="";
	private String PlusMosquitoDetail="";
	private String DurationTime="";
	private String SystemRuns="";
	private String Recommendations="";
	private String TechnicianComments="";
	private String TimeIn="";
	private String Timeout="";
	private String TimeInDateTime="";
	private String TimeOutDateTime="";
	private String Technician="";
	private String EmpNo="";
	private String Customer="";
	private String Date="";
	private String PaymentType="";
	private String Amount="";
	private String CheckNo="";
	private String LicenseNo="";
	private String Signature="";
	private String ServiceSignature="";
	private String Create_By="";
	private String Update_By="";
	//private String Create_Date="";
	//private String Update_Date="";
	private String AgreementPath="";
	
	
	
	/**
	 * @return the timeInDateTime
	 */
	public String getTimeInDateTime() {
		return TimeInDateTime;
	}
	/**
	 * @param timeInDateTime the timeInDateTime to set
	 */
	public void setTimeInDateTime(String timeInDateTime) {
		TimeInDateTime = timeInDateTime;
	}
	/**
	 * @return the timeOutDateTime
	 */
	public String getTimeOutDateTime() {
		return TimeOutDateTime;
	}
	/**
	 * @param timeOutDateTime the timeOutDateTime to set
	 */
	public void setTimeOutDateTime(String timeOutDateTime) {
		TimeOutDateTime = timeOutDateTime;
	}
	/**
	 * @return the isCompleted
	 */
	public String getIsCompleted() {
		return isCompleted;
	}
	/**
	 * @param isCompleted the isCompleted to set
	 */
	public void setIsCompleted(String isCompleted) {
		this.isCompleted = isCompleted;
	}
	/**
	 * @return the commercialId
	 */
	public String getCommercialId() {
		return CommercialId;
	}
	/**
	 * @param commercialId the commercialId to set
	 */
	public void setCommercialId(String commercialId) {
		CommercialId = commercialId;
	}
	/**
	 * @return the serviceID_new
	 */
	public String getServiceID_new() {
		return ServiceID_new;
	}
	/**
	 * @param serviceID_new the serviceID_new to set
	 */
	public void setServiceID_new(String serviceID_new) {
		ServiceID_new = serviceID_new;
	}
	/**
	 * @return the serviceFor
	 */
	public String getServiceFor() {
		return ServiceFor;
	}
	/**
	 * @param serviceFor the serviceFor to set
	 */
	public void setServiceFor(String serviceFor) {
		ServiceFor = serviceFor;
	}
	/**
	 * @return the areasInspectedInterior
	 */
	public String getAreasInspectedInterior() {
		return AreasInspectedInterior;
	}
	/**
	 * @param areasInspectedInterior the areasInspectedInterior to set
	 */
	public void setAreasInspectedInterior(String areasInspectedInterior) {
		AreasInspectedInterior = areasInspectedInterior;
	}
	/**
	 * @return the areasInspectedExterior
	 */
	public String getAreasInspectedExterior() {
		return AreasInspectedExterior;
	}
	/**
	 * @param areasInspectedExterior the areasInspectedExterior to set
	 */
	public void setAreasInspectedExterior(String areasInspectedExterior) {
		AreasInspectedExterior = areasInspectedExterior;
	}
	/**
	 * @return the insectActivity
	 */
	public String getInsectActivity() {
		return InsectActivity;
	}
	/**
	 * @param insectActivity the insectActivity to set
	 */
	public void setInsectActivity(String insectActivity) {
		InsectActivity = insectActivity;
	}
	/**
	 * @return the ants
	 */
	public String getAnts() {
		return Ants;
	}
	/**
	 * @param ants the ants to set
	 */
	public void setAnts(String ants) {
		Ants = ants;
	}
	/**
	 * @return the otherPest
	 */
	public String getOtherPest() {
		return OtherPest;
	}
	/**
	 * @param otherPest the otherPest to set
	 */
	public void setOtherPest(String otherPest) {
		OtherPest = otherPest;
	}
	/**
	 * @return the pestActivity
	 */
	public String getPestActivity() {
		return PestActivity;
	}
	/**
	 * @param pestActivity the pestActivity to set
	 */
	public void setPestActivity(String pestActivity) {
		PestActivity = pestActivity;
	}
	/**
	 * @return the interior
	 */
	public String getInterior() {
		return Interior;
	}
	/**
	 * @param interior the interior to set
	 */
	public void setInterior(String interior) {
		Interior = interior;
	}
	/**
	 * @return the outsidef_Perimeter
	 */
	public String getOutsidef_Perimeter() {
		return Outsidef_Perimeter;
	}
	/**
	 * @param outsidef_Perimeter the outsidef_Perimeter to set
	 */
	public void setOutsidef_Perimeter(String outsidef_Perimeter) {
		Outsidef_Perimeter = outsidef_Perimeter;
	}
	/**
	 * @return the dumpster
	 */
	public String getDumpster() {
		return Dumpster;
	}
	/**
	 * @param dumpster the dumpster to set
	 */
	public void setDumpster(String dumpster) {
		Dumpster = dumpster;
	}
	/**
	 * @return the dining_Room
	 */
	public String getDining_Room() {
		return Dining_Room;
	}
	/**
	 * @param dining_Room the dining_Room to set
	 */
	public void setDining_Room(String dining_Room) {
		Dining_Room = dining_Room;
	}
	/**
	 * @return the common_Areas
	 */
	public String getCommon_Areas() {
		return Common_Areas;
	}
	/**
	 * @param common_Areas the common_Areas to set
	 */
	public void setCommon_Areas(String common_Areas) {
		Common_Areas = common_Areas;
	}
	/**
	 * @return the kitchen
	 */
	public String getKitchen() {
		return Kitchen;
	}
	/**
	 * @param kitchen the kitchen to set
	 */
	public void setKitchen(String kitchen) {
		Kitchen = kitchen;
	}
	/**
	 * @return the dry_Storage
	 */
	public String getDry_Storage() {
		return Dry_Storage;
	}
	/**
	 * @param dry_Storage the dry_Storage to set
	 */
	public void setDry_Storage(String dry_Storage) {
		Dry_Storage = dry_Storage;
	}
	/**
	 * @return the dish_Washing
	 */
	public String getDish_Washing() {
		return Dish_Washing;
	}
	/**
	 * @param dish_Washing the dish_Washing to set
	 */
	public void setDish_Washing(String dish_Washing) {
		Dish_Washing = dish_Washing;
	}
	/**
	 * @return the roof_Tops
	 */
	public String getRoof_Tops() {
		return Roof_Tops;
	}
	/**
	 * @param roof_Tops the roof_Tops to set
	 */
	public void setRoof_Tops(String roof_Tops) {
		Roof_Tops = roof_Tops;
	}
	/**
	 * @return the wait_Stations
	 */
	public String getWait_Stations() {
		return Wait_Stations;
	}
	/**
	 * @param wait_Stations the wait_Stations to set
	 */
	public void setWait_Stations(String wait_Stations) {
		Wait_Stations = wait_Stations;
	}
	/**
	 * @return the drop_Ceiling
	 */
	public String getDrop_Ceiling() {
		return Drop_Ceiling;
	}
	/**
	 * @param drop_Ceiling the drop_Ceiling to set
	 */
	public void setDrop_Ceiling(String drop_Ceiling) {
		Drop_Ceiling = drop_Ceiling;
	}
	/**
	 * @return the planters
	 */
	public String getPlanters() {
		return Planters;
	}
	/**
	 * @param planters the planters to set
	 */
	public void setPlanters(String planters) {
		Planters = planters;
	}
	/**
	 * @return the warehouse_Storage
	 */
	public String getWarehouse_Storage() {
		return Warehouse_Storage;
	}
	/**
	 * @param warehouse_Storage the warehouse_Storage to set
	 */
	public void setWarehouse_Storage(String warehouse_Storage) {
		Warehouse_Storage = warehouse_Storage;
	}
	/**
	 * @return the pestControl
	 */
	public String getPestControl() {
		return PestControl;
	}
	/**
	 * @param pestControl the pestControl to set
	 */
	public void setPestControl(String pestControl) {
		PestControl = pestControl;
	}
	/**
	 * @return the pestControlServices
	 */
	public String getPestControlServices() {
		return PestControlServices;
	}
	/**
	 * @param pestControlServices the pestControlServices to set
	 */
	public void setPestControlServices(String pestControlServices) {
		PestControlServices = pestControlServices;
	}
	/**
	 * @return the plusTermite
	 */
	public String getPlusTermite() {
		return PlusTermite;
	}
	/**
	 * @param plusTermite the plusTermite to set
	 */
	public void setPlusTermite(String plusTermite) {
		PlusTermite = plusTermite;
	}
	/**
	 * @return the signOfTermiteMS
	 */
	public String getSignOfTermiteMS() {
		return SignOfTermiteMS;
	}
	/**
	 * @param signOfTermiteMS the signOfTermiteMS to set
	 */
	public void setSignOfTermiteMS(String signOfTermiteMS) {
		SignOfTermiteMS = signOfTermiteMS;
	}
	/**
	 * @return the signOfTermiteActivityMS
	 */
	public String getSignOfTermiteActivityMS() {
		return SignOfTermiteActivityMS;
	}
	/**
	 * @param signOfTermiteActivityMS the signOfTermiteActivityMS to set
	 */
	public void setSignOfTermiteActivityMS(String signOfTermiteActivityMS) {
		SignOfTermiteActivityMS = signOfTermiteActivityMS;
	}
	/**
	 * @return the signOfTermiteText
	 */
	public String getSignOfTermiteText() {
		return SignOfTermiteText;
	}
	/**
	 * @param signOfTermiteText the signOfTermiteText to set
	 */
	public void setSignOfTermiteText(String signOfTermiteText) {
		SignOfTermiteText = signOfTermiteText;
	}
	/**
	 * @return the installMS
	 */
	public String getInstallMS() {
		return InstallMS;
	}
	/**
	 * @param installMS the installMS to set
	 */
	public void setInstallMS(String installMS) {
		InstallMS = installMS;
	}
	/**
	 * @return the installMSText
	 */
	public String getInstallMSText() {
		return InstallMSText;
	}
	/**
	 * @param installMSText the installMSText to set
	 */
	public void setInstallMSText(String installMSText) {
		InstallMSText = installMSText;
	}
	/**
	 * @return the signOfTermitePS
	 */
	public String getSignOfTermitePS() {
		return SignOfTermitePS;
	}
	/**
	 * @param signOfTermitePS the signOfTermitePS to set
	 */
	public void setSignOfTermitePS(String signOfTermitePS) {
		SignOfTermitePS = signOfTermitePS;
	}
	/**
	 * @return the signOfTermiteActivityPS
	 */
	public String getSignOfTermiteActivityPS() {
		return SignOfTermiteActivityPS;
	}
	/**
	 * @param signOfTermiteActivityPS the signOfTermiteActivityPS to set
	 */
	public void setSignOfTermiteActivityPS(String signOfTermiteActivityPS) {
		SignOfTermiteActivityPS = signOfTermiteActivityPS;
	}
	/**
	 * @return the signOfTermiteIS
	 */
	public String getSignOfTermiteIS() {
		return SignOfTermiteIS;
	}
	/**
	 * @param signOfTermiteIS the signOfTermiteIS to set
	 */
	public void setSignOfTermiteIS(String signOfTermiteIS) {
		SignOfTermiteIS = signOfTermiteIS;
	}
	/**
	 * @return the signOfTermiteActivityIS
	 */
	public String getSignOfTermiteActivityIS() {
		return SignOfTermiteActivityIS;
	}
	/**
	 * @param signOfTermiteActivityIS the signOfTermiteActivityIS to set
	 */
	public void setSignOfTermiteActivityIS(String signOfTermiteActivityIS) {
		SignOfTermiteActivityIS = signOfTermiteActivityIS;
	}
	/**
	 * @return the replacedMS
	 */
	public String getReplacedMS() {
		return ReplacedMS;
	}
	/**
	 * @param replacedMS the replacedMS to set
	 */
	public void setReplacedMS(String replacedMS) {
		ReplacedMS = replacedMS;
	}
	/**
	 * @return the replacedActivityTextMS
	 */
	public String getReplacedActivityTextMS() {
		return ReplacedActivityTextMS;
	}
	/**
	 * @param replacedActivityTextMS the replacedActivityTextMS to set
	 */
	public void setReplacedActivityTextMS(String replacedActivityTextMS) {
		ReplacedActivityTextMS = replacedActivityTextMS;
	}
	/**
	 * @return the replacedTextMS
	 */
	public String getReplacedTextMS() {
		return ReplacedTextMS;
	}
	/**
	 * @param replacedTextMS the replacedTextMS to set
	 */
	public void setReplacedTextMS(String replacedTextMS) {
		ReplacedTextMS = replacedTextMS;
	}
	/**
	 * @return the treatedPS
	 */
	public String getTreatedPS() {
		return TreatedPS;
	}
	/**
	 * @param treatedPS the treatedPS to set
	 */
	public void setTreatedPS(String treatedPS) {
		TreatedPS = treatedPS;
	}
	/**
	 * @return the treatedActivityPS
	 */
	public String getTreatedActivityPS() {
		return TreatedActivityPS;
	}
	/**
	 * @param treatedActivityPS the treatedActivityPS to set
	 */
	public void setTreatedActivityPS(String treatedActivityPS) {
		TreatedActivityPS = treatedActivityPS;
	}
	/**
	 * @return the treatedSP
	 */
	public String getTreatedSP() {
		return TreatedSP;
	}
	/**
	 * @param treatedSP the treatedSP to set
	 */
	public void setTreatedSP(String treatedSP) {
		TreatedSP = treatedSP;
	}
	/**
	 * @return the treatedActivitySP
	 */
	public String getTreatedActivitySP() {
		return TreatedActivitySP;
	}
	/**
	 * @param treatedActivitySP the treatedActivitySP to set
	 */
	public void setTreatedActivitySP(String treatedActivitySP) {
		TreatedActivitySP = treatedActivitySP;
	}
	/**
	 * @return the treatedCC
	 */
	public String getTreatedCC() {
		return TreatedCC;
	}
	/**
	 * @param treatedCC the treatedCC to set
	 */
	public void setTreatedCC(String treatedCC) {
		TreatedCC = treatedCC;
	}
	/**
	 * @return the treatedActivityCC
	 */
	public String getTreatedActivityCC() {
		return TreatedActivityCC;
	}
	/**
	 * @param treatedActivityCC the treatedActivityCC to set
	 */
	public void setTreatedActivityCC(String treatedActivityCC) {
		TreatedActivityCC = treatedActivityCC;
	}
	/**
	 * @return the plusRodent
	 */
	public String getPlusRodent() {
		return PlusRodent;
	}
	/**
	 * @param plusRodent the plusRodent to set
	 */
	public void setPlusRodent(String plusRodent) {
		PlusRodent = plusRodent;
	}
	/**
	 * @return the inspectedRodentStation
	 */
	public String getInspectedRodentStation() {
		return InspectedRodentStation;
	}
	/**
	 * @param inspectedRodentStation the inspectedRodentStation to set
	 */
	public void setInspectedRodentStation(String inspectedRodentStation) {
		InspectedRodentStation = inspectedRodentStation;
	}
	/**
	 * @return the txtInspectedRodentStation
	 */
	public String getTxtInspectedRodentStation() {
		return txtInspectedRodentStation;
	}
	/**
	 * @param txtInspectedRodentStation the txtInspectedRodentStation to set
	 */
	public void setTxtInspectedRodentStation(String txtInspectedRodentStation) {
		this.txtInspectedRodentStation = txtInspectedRodentStation;
	}
	/**
	 * @return the cleanedRodentStation
	 */
	public String getCleanedRodentStation() {
		return CleanedRodentStation;
	}
	/**
	 * @param cleanedRodentStation the cleanedRodentStation to set
	 */
	public void setCleanedRodentStation(String cleanedRodentStation) {
		CleanedRodentStation = cleanedRodentStation;
	}
	/**
	 * @return the isCleanedRodentStation
	 */
	public String getIsCleanedRodentStation() {
		return IsCleanedRodentStation;
	}
	/**
	 * @param isCleanedRodentStation the isCleanedRodentStation to set
	 */
	public void setIsCleanedRodentStation(String isCleanedRodentStation) {
		IsCleanedRodentStation = isCleanedRodentStation;
	}
	/**
	 * @return the installRodentBaitStation
	 */
	public String getInstallRodentBaitStation() {
		return InstallRodentBaitStation;
	}
	/**
	 * @param installRodentBaitStation the installRodentBaitStation to set
	 */
	public void setInstallRodentBaitStation(String installRodentBaitStation) {
		InstallRodentBaitStation = installRodentBaitStation;
	}
	/**
	 * @return the isInstallRodentBaitStation
	 */
	public String getIsInstallRodentBaitStation() {
		return IsInstallRodentBaitStation;
	}
	
	public void setIsInstallRodentBaitStation(String isInstallRodentBaitStation) {
		IsInstallRodentBaitStation = isInstallRodentBaitStation;
	}
	/**
	 * @return the txtInstallRodentBaitStation
	 */
	public String getTxtInstallRodentBaitStation() {
		return txtInstallRodentBaitStation;
	}
	/**
	 * @param txtInstallRodentBaitStation the txtInstallRodentBaitStation to set
	 */
	public void setTxtInstallRodentBaitStation(String txtInstallRodentBaitStation) {
		this.txtInstallRodentBaitStation = txtInstallRodentBaitStation;
	}
	/**
	 * @return the replacedBait
	 */
	public String getReplacedBait() {
		return ReplacedBait;
	}
	/**
	 * @param replacedBait the replacedBait to set
	 */
	public void setReplacedBait(String replacedBait) {
		ReplacedBait = replacedBait;
	}
	/**
	 * @return the isReplacedBait
	 */
	public String getIsReplacedBait() {
		return IsReplacedBait;
	}
	/**
	 * @param isReplacedBait the isReplacedBait to set
	 */
	public void setIsReplacedBait(String isReplacedBait) {
		IsReplacedBait = isReplacedBait;
	}
	/**
	 * @return the sNoReplacedBait
	 */
	public String getSNoReplacedBait() {
		return SNoReplacedBait;
	}
	/**
	 * @param sNoReplacedBait the sNoReplacedBait to set
	 */
	public void setSNoReplacedBait(String sNoReplacedBait) {
		SNoReplacedBait = sNoReplacedBait;
	}
	/**
	 * @return the replacedBaitStation
	 */
	public String getReplacedBaitStation() {
		return ReplacedBaitStation;
	}
	/**
	 * @param replacedBaitStation the replacedBaitStation to set
	 */
	public void setReplacedBaitStation(String replacedBaitStation) {
		ReplacedBaitStation = replacedBaitStation;
	}
	/**
	 * @return the isReplacedBaitStation
	 */
	public String getIsReplacedBaitStation() {
		return IsReplacedBaitStation;
	}
	/**
	 * @param isReplacedBaitStation the isReplacedBaitStation to set
	 */
	public void setIsReplacedBaitStation(String isReplacedBaitStation) {
		IsReplacedBaitStation = isReplacedBaitStation;
	}
	/**
	 * @return the txtReplacedBaitStation
	 */
	public String getTxtReplacedBaitStation() {
		return txtReplacedBaitStation;
	}
	/**
	 * @param txtReplacedBaitStation the txtReplacedBaitStation to set
	 */
	public void setTxtReplacedBaitStation(String txtReplacedBaitStation) {
		this.txtReplacedBaitStation = txtReplacedBaitStation;
	}
	/**
	 * @return the sNoReplacedBaitStation
	 */
	public String getSNoReplacedBaitStation() {
		return SNoReplacedBaitStation;
	}
	/**
	 * @param sNoReplacedBaitStation the sNoReplacedBaitStation to set
	 */
	public void setSNoReplacedBaitStation(String sNoReplacedBaitStation) {
		SNoReplacedBaitStation = sNoReplacedBaitStation;
	}
	/**
	 * @return the inspectedRodentTraps
	 */
	public String getInspectedRodentTraps() {
		return InspectedRodentTraps;
	}
	/**
	 * @param inspectedRodentTraps the inspectedRodentTraps to set
	 */
	public void setInspectedRodentTraps(String inspectedRodentTraps) {
		InspectedRodentTraps = inspectedRodentTraps;
	}
	/**
	 * @return the isInspectedRodentTraps
	 */
	public String getIsInspectedRodentTraps() {
		return IsInspectedRodentTraps;
	}
	/**
	 * @param isInspectedRodentTraps the isInspectedRodentTraps to set
	 */
	public void setIsInspectedRodentTraps(String isInspectedRodentTraps) {
		IsInspectedRodentTraps = isInspectedRodentTraps;
	}
	/**
	 * @return the cleanedRodentTrap
	 */
	public String getCleanedRodentTrap() {
		return CleanedRodentTrap;
	}
	/**
	 * @param cleanedRodentTrap the cleanedRodentTrap to set
	 */
	public void setCleanedRodentTrap(String cleanedRodentTrap) {
		CleanedRodentTrap = cleanedRodentTrap;
	}
	/**
	 * @return the isCleanedRodentTrap
	 */
	public String getIsCleanedRodentTrap() {
		return IsCleanedRodentTrap;
	}
	/**
	 * @param isCleanedRodentTrap the isCleanedRodentTrap to set
	 */
	public void setIsCleanedRodentTrap(String isCleanedRodentTrap) {
		IsCleanedRodentTrap = isCleanedRodentTrap;
	}
	/**
	 * @return the installRodentTrap
	 */
	public String getInstallRodentTrap() {
		return InstallRodentTrap;
	}
	/**
	 * @param installRodentTrap the installRodentTrap to set
	 */
	public void setInstallRodentTrap(String installRodentTrap) {
		InstallRodentTrap = installRodentTrap;
	}
	/**
	 * @return the isInstallRodentTrap
	 */
	public String getIsInstallRodentTrap() {
		return IsInstallRodentTrap;
	}
	/**
	 * @param isInstallRodentTrap the isInstallRodentTrap to set
	 */
	public void setIsInstallRodentTrap(String isInstallRodentTrap) {
		IsInstallRodentTrap = isInstallRodentTrap;
	}
	/**
	 * @return the txtInstallRodentTrap
	 */
	public String getTxtInstallRodentTrap() {
		return txtInstallRodentTrap;
	}
	/**
	 * @param txtInstallRodentTrap the txtInstallRodentTrap to set
	 */
	public void setTxtInstallRodentTrap(String txtInstallRodentTrap) {
		this.txtInstallRodentTrap = txtInstallRodentTrap;
	}
	/**
	 * @return the replacedRodentTrap
	 */
	public String getReplacedRodentTrap() {
		return ReplacedRodentTrap;
	}
	/**
	 * @param replacedRodentTrap the replacedRodentTrap to set
	 */
	public void setReplacedRodentTrap(String replacedRodentTrap) {
		ReplacedRodentTrap = replacedRodentTrap;
	}
	/**
	 * @return the isReplacedRodentTrap
	 */
	public String getIsReplacedRodentTrap() {
		return IsReplacedRodentTrap;
	}
	/**
	 * @param isReplacedRodentTrap the isReplacedRodentTrap to set
	 */
	public void setIsReplacedRodentTrap(String isReplacedRodentTrap) {
		IsReplacedRodentTrap = isReplacedRodentTrap;
	}
	/**
	 * @return the txtReplacedRodentTrap
	 */
	public String getTxtReplacedRodentTrap() {
		return txtReplacedRodentTrap;
	}
	/**
	 * @param txtReplacedRodentTrap the txtReplacedRodentTrap to set
	 */
	public void setTxtReplacedRodentTrap(String txtReplacedRodentTrap) {
		this.txtReplacedRodentTrap = txtReplacedRodentTrap;
	}
	/**
	 * @return the sNoReplacedRodentTrap
	 */
	public String getSNoReplacedRodentTrap() {
		return SNoReplacedRodentTrap;
	}
	/**
	 * @param sNoReplacedRodentTrap the sNoReplacedRodentTrap to set
	 */
	public void setSNoReplacedRodentTrap(String sNoReplacedRodentTrap) {
		SNoReplacedRodentTrap = sNoReplacedRodentTrap;
	}
	/**
	 * @return the sealedEntryPoint
	 */
	public String getSealedEntryPoint() {
		return SealedEntryPoint;
	}
	/**
	 * @param sealedEntryPoint the sealedEntryPoint to set
	 */
	public void setSealedEntryPoint(String sealedEntryPoint) {
		SealedEntryPoint = sealedEntryPoint;
	}
	/**
	 * @return the isSealedEntryPoint
	 */
	public String getIsSealedEntryPoint() {
		return IsSealedEntryPoint;
	}
	/**
	 * @param isSealedEntryPoint the isSealedEntryPoint to set
	 */
	public void setIsSealedEntryPoint(String isSealedEntryPoint) {
		IsSealedEntryPoint = isSealedEntryPoint;
	}
	
	/**
	 * @return the inspectedLiveCages
	 */
	public String getInspectedLiveCages() {
		return InspectedLiveCages;
	}
	/**
	 * @param inspectedLiveCages the inspectedLiveCages to set
	 */
	public void setInspectedLiveCages(String inspectedLiveCages) {
		InspectedLiveCages = inspectedLiveCages;
	}
	/**
	 * @return the txtInspectedLiveCages
	 */
	public String getTxtInspectedLiveCages() {
		return txtInspectedLiveCages;
	}
	/**
	 * @param txtInspectedLiveCages the txtInspectedLiveCages to set
	 */
	public void setTxtInspectedLiveCages(String txtInspectedLiveCages) {
		this.txtInspectedLiveCages = txtInspectedLiveCages;
	}
	
	
	/**
	 * @return the inspectedSnapTrap
	 */
	public String getInspectedSnapTrap() {
		return InspectedSnapTrap;
	}
	/**
	 * @param inspectedSnapTrap the inspectedSnapTrap to set
	 */
	public void setInspectedSnapTrap(String inspectedSnapTrap) {
		InspectedSnapTrap = inspectedSnapTrap;
	}
	/**
	 * @return the txtInspectedSnapTrap
	 */
	public String getTxtInspectedSnapTrap() {
		return txtInspectedSnapTrap;
	}
	/**
	 * @param txtInspectedSnapTrap the txtInspectedSnapTrap to set
	 */
	public void setTxtInspectedSnapTrap(String txtInspectedSnapTrap) {
		this.txtInspectedSnapTrap = txtInspectedSnapTrap;
	}
	public String getRemovedSnapTraps() {
		return RemovedSnapTraps;
	}
	/**
	 * @param removedSnapTraps the removedSnapTraps to set
	 */
	public void setRemovedSnapTraps(String removedSnapTraps) {
		RemovedSnapTraps = removedSnapTraps;
	}
	/**
	 * @return the txtRemovedSnapTraps
	 */
	public String getTxtRemovedSnapTraps() {
		return txtRemovedSnapTraps;
	}
	/**
	 * @param txtRemovedSnapTraps the txtRemovedSnapTraps to set
	 */
	public void setTxtRemovedSnapTraps(String txtRemovedSnapTraps) {
		this.txtRemovedSnapTraps = txtRemovedSnapTraps;
	}
	/**
	 * @return the removedLiveCages
	 */
	public String getRemovedLiveCages() {
		return RemovedLiveCages;
	}
	/**
	 * @param removedLiveCages the removedLiveCages to set
	 */
	public void setRemovedLiveCages(String removedLiveCages) {
		RemovedLiveCages = removedLiveCages;
	}
	/**
	 * @return the txtRemovedLiveCages
	 */
	public String getTxtRemovedLiveCages() {
		return txtRemovedLiveCages;
	}
	/**
	 * @param txtRemovedLiveCages the txtRemovedLiveCages to set
	 */
	public void setTxtRemovedLiveCages(String txtRemovedLiveCages) {
		this.txtRemovedLiveCages = txtRemovedLiveCages;
	}
	/**
	 * @return the removedRodentBaitStation
	 */
	public String getRemovedRodentBaitStation() {
		return RemovedRodentBaitStation;
	}
	/**
	 * @param removedRodentBaitStation the removedRodentBaitStation to set
	 */
	public void setRemovedRodentBaitStation(String removedRodentBaitStation) {
		RemovedRodentBaitStation = removedRodentBaitStation;
	}
	/**
	 * @return the txtRemovedRodentBaitStation
	 */
	public String getTxtRemovedRodentBaitStation() {
		return txtRemovedRodentBaitStation;
	}
	/**
	 * @param txtRemovedRodentBaitStation the txtRemovedRodentBaitStation to set
	 */
	public void setTxtRemovedRodentBaitStation(String txtRemovedRodentBaitStation) {
		this.txtRemovedRodentBaitStation = txtRemovedRodentBaitStation;
	}
	/**
	 * @return the removedRodentTrap
	 */
	public String getRemovedRodentTrap() {
		return RemovedRodentTrap;
	}
	/**
	 * @param removedRodentTrap the removedRodentTrap to set
	 */
	public void setRemovedRodentTrap(String removedRodentTrap) {
		RemovedRodentTrap = removedRodentTrap;
	}
	/**
	 * @return the txtRemovedRodentTrap
	 */
	public String getTxtRemovedRodentTrap() {
		return txtRemovedRodentTrap;
	}
	/**
	 * @param txtRemovedRodentTrap the txtRemovedRodentTrap to set
	 */
	public void setTxtRemovedRodentTrap(String txtRemovedRodentTrap) {
		this.txtRemovedRodentTrap = txtRemovedRodentTrap;
	}
	/**
	 * @return the setSnapTrap
	 */
	public String getSetSnapTrap() {
		return SetSnapTrap;
	}
	/**
	 * @param setSnapTrap the setSnapTrap to set
	 */
	public void setSetSnapTrap(String setSnapTrap) {
		SetSnapTrap = setSnapTrap;
	}
	/**
	 * @return the txtSetSnapTrap
	 */
	public String getTxtSetSnapTrap() {
		return txtSetSnapTrap;
	}
	/**
	 * @param txtSetSnapTrap the txtSetSnapTrap to set
	 */
	public void setTxtSetSnapTrap(String txtSetSnapTrap) {
		this.txtSetSnapTrap = txtSetSnapTrap;
	}
	/**
	 * @return the lstSetSnapTrap
	 */
	public String getLstSetSnapTrap() {
		return LstSetSnapTrap;
	}
	/**
	 * @param lstSetSnapTrap the lstSetSnapTrap to set
	 */
	public void setLstSetSnapTrap(String lstSetSnapTrap) {
		LstSetSnapTrap = lstSetSnapTrap;
	}
	/**
	 * @return the setLiveCages
	 */
	public String getSetLiveCages() {
		return SetLiveCages;
	}
	/**
	 * @param setLiveCages the setLiveCages to set
	 */
	public void setSetLiveCages(String setLiveCages) {
		SetLiveCages = setLiveCages;
	}
	/**
	 * @return the txtSetLiveCages
	 */
	public String getTxtSetLiveCages() {
		return txtSetLiveCages;
	}
	/**
	 * @param txtSetLiveCages the txtSetLiveCages to set
	 */
	public void setTxtSetLiveCages(String txtSetLiveCages) {
		this.txtSetLiveCages = txtSetLiveCages;
	}
	/**
	 * @return the lstSetLiveCages
	 */
	public String getLstSetLiveCages() {
		return LstSetLiveCages;
	}
	/**
	 * @param lstSetLiveCages the lstSetLiveCages to set
	 */
	public void setLstSetLiveCages(String lstSetLiveCages) {
		LstSetLiveCages = lstSetLiveCages;
	}
	/**
	 * @return the plusMosquitoDetail
	 */
	public String getPlusMosquitoDetail() {
		return PlusMosquitoDetail;
	}
	/**
	 * @param plusMosquitoDetail the plusMosquitoDetail to set
	 */
	public void setPlusMosquitoDetail(String plusMosquitoDetail) {
		PlusMosquitoDetail = plusMosquitoDetail;
	}
	/**
	 * @return the durationTime
	 */
	public String getDurationTime() {
		return DurationTime;
	}
	/**
	 * @param durationTime the durationTime to set
	 */
	public void setDurationTime(String durationTime) {
		DurationTime = durationTime;
	}
	/**
	 * @return the systemRuns
	 */
	public String getSystemRuns() {
		return SystemRuns;
	}
	/**
	 * @param systemRuns the systemRuns to set
	 */
	public void setSystemRuns(String systemRuns) {
		SystemRuns = systemRuns;
	}
	/**
	 * @return the recommendations
	 */
	public String getRecommendations() {
		return Recommendations;
	}
	/**
	 * @param recommendations the recommendations to set
	 */
	public void setRecommendations(String recommendations) {
		Recommendations = recommendations;
	}
	/**
	 * @return the technicianComments
	 */
	public String getTechnicianComments() {
		return TechnicianComments;
	}
	/**
	 * @param technicianComments the technicianComments to set
	 */
	public void setTechnicianComments(String technicianComments) {
		TechnicianComments = technicianComments;
	}
	/**
	 * @return the timeIn
	 */
	public String getTimeIn() {
		return TimeIn;
	}
	/**
	 * @param timeIn the timeIn to set
	 */
	public void setTimeIn(String timeIn) {
		TimeIn = timeIn;
	}
	/**
	 * @return the timeout
	 */
	public String getTimeout() {
		return Timeout;
	}
	/**
	 * @param timeout the timeout to set
	 */
	public void setTimeout(String timeout) {
		Timeout = timeout;
	}
	
	
	/**
	 * @return the technician
	 */
	public String getTechnician() {
		return Technician;
	}
	/**
	 * @param technician the technician to set
	 */
	public void setTechnician(String technician) {
		Technician = technician;
	}
	/**
	 * @return the empNo
	 */
	public String getEmpNo() {
		return EmpNo;
	}
	/**
	 * @param empNo the empNo to set
	 */
	public void setEmpNo(String empNo) {
		EmpNo = empNo;
	}
	/**
	 * @return the customer
	 */
	public String getCustomer() {
		return Customer;
	}
	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(String customer) {
		Customer = customer;
	}
	/**
	 * @return the date
	 */
	public String getDate() {
		return Date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		Date = date;
	}
	/**
	 * @return the paymentType
	 */
	public String getPaymentType() {
		return PaymentType;
	}
	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(String paymentType) {
		PaymentType = paymentType;
	}
	/**
	 * @return the amount
	 */
	public String getAmount() {
		return Amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		Amount = amount;
	}
	/**
	 * @return the checkNo
	 */
	public String getCheckNo() {
		return CheckNo;
	}
	/**
	 * @param checkNo the checkNo to set
	 */
	public void setCheckNo(String checkNo) {
		CheckNo = checkNo;
	}
	/**
	 * @return the licenseNo
	 */
	public String getLicenseNo() {
		return LicenseNo;
	}
	/**
	 * @param licenseNo the licenseNo to set
	 */
	public void setLicenseNo(String licenseNo) {
		LicenseNo = licenseNo;
	}
	/**
	 * @return the signature
	 */
	public String getSignature() {
		return Signature;
	}
	/**
	 * @param signature the signature to set
	 */
	public void setSignature(String signature) {
		Signature = signature;
	}
	/**
	 * @return the serviceSignature
	 */
	public String getServiceSignature() {
		return ServiceSignature;
	}
	/**
	 * @param serviceSignature the serviceSignature to set
	 */
	public void setServiceSignature(String serviceSignature) {
		ServiceSignature = serviceSignature;
	}
	/**
	 * @return the create_By
	 */
	public String getCreate_By() {
		return Create_By;
	}
	/**
	 * @param create_By the create_By to set
	 */
	public void setCreate_By(String create_By) {
		Create_By = create_By;
	}
	/**
	 * @return the update_By
	 */
	public String getUpdate_By() {
		return Update_By;
	}
	/**
	 * @param update_By the update_By to set
	 */
	public void setUpdate_By(String update_By) {
		Update_By = update_By;
	}
	/**
	 * @return the create_Date
	 *//*
	public String getCreate_Date() {
		return Create_Date;
	}
	*//**
	 * @param create_Date the create_Date to set
	 *//*
	public void setCreate_Date(String create_Date) {
		Create_Date = create_Date;
	}
	*//**
	 * @return the update_Date
	 *//*
	public String getUpdate_Date() {
		return Update_Date;
	}
	*//**
	 * @param update_Date the update_Date to set
	 *//*
	public void setUpdate_Date(String update_Date) {
		Update_Date = update_Date;
	}*/
	/**
	 * @return the agreementPath
	 */
	public String getAgreementPath() {
		return AgreementPath;
	}
	/**
	 * @param agreementPath the agreementPath to set
	 */
	public void setAgreementPath(String agreementPath) {
		AgreementPath = agreementPath;
	}
	
	
}
