package quacito.houston.model;

import java.io.Serializable;

/**
 * @author priya
 *
 */
public class GeneralInfoDTO implements Serializable 
{
	
	private String isCompleted="";
	/*
	*//**
	 * @return the isCompleted
	 *//*
	public String getIsCompleted() {
		return isCompleted;
	}
	*//**
	 * @param isCompleted the isCompleted to set
	 *//*
	public void setIsCompleted(String isCompleted) {
		this.isCompleted = isCompleted;
	}*/
	
	
	
	private String ServiceID_new="";
		public String getIsCompleted() {
		return isCompleted;
	}
	public void setIsCompleted(String isCompleted) {
		this.isCompleted = isCompleted;
	}
	    private String ServiceNameType="";
	    private String newEmail="";
	    private String ResetComment="";
		private String First_Name="";
		private String Last_Name="";
		private String FullName="";
		private String ServiceClass="";
		private String Email_Id="";
		private String SecondaryEmail="";
		private String AccountNo="";
		private String Home_Phone="";
		private String Cell_Phone="";
		private String Salesperson_ID="";
		private String ServiceDate="";
		private String ServiceTime="";
		private String ServiceStatus="";
		private String Notes="";
		private String ServiceCode="";
		private String department="";
		private String BusinessName="";
		private String Branch="";
		private String TaxCode="";
		private String EstimateTime="";
		private String Arrival_Duration="";
		private String ProblemDescription="";
		private String PSPCustomer="";
		private String Create_By="";
		private String Update_By="";
		private String Create_Date="";
		private String Update_Date="";
		private String Ref_Source="";
		private String Order_Number="";
		private String Route_Name="";
		private String Route_Number="";
		private String KeyMap="";
		private String Service_Tech_Num="";
		private String Service_Status_Reason="";
		private String CompanyName="";
		private String ServiceDescription="";
		private String OtherInstruction="";
		private String specialInstruction="";
		private String Total="";
		private String LocationBalance="";
		private String TimeInDateTime="";
		private String Branch_ID="";
		private String Inv_value="";
		private String Prod_value="";
		private String Prv_Bal="";
		private String Tax_value="";
		private String Service_Attribute="";
		private String Office_Note_ByRep="";
		private String Note_Status="";
		private String Note_Closed_Text="";
		private String Note_Closed_Date="";
		private String Payment_Collection_Status="";
		private String Collection_Text="";
		private String Collection_Date="";
		private String Flag="";
		private String IsElectronicSignatureAvailable="";
		private String OpenWONotificationFlag="";
		//private String TimeOutDateTime="";
		private String TimeIn="";
		//private String ServiceAddress="";
		//private String BillingAddress="";
		private String Address_Line1="";
		private String Address_Line2="";
		private String City="";
		private String State="";
		private String ZipCode="";
		private String Poc_FName="";
		private String Poc_LName="";
		private String BAddress_Line1="";
		private String BAddress_Line2="";
		private String BCity="";
		private String BState="";
		private String BZipCode="";
		private String ServiceLatitude="";
		private String ServiceLongitude="";
		//private String Created_By="";
		private String BeforeImage="";
		private String AfterImage="";
		private String AudioFile="";
		//private String Updated_By="";
		private String CheckNo="";
		private String LicenseNo="";
		private String ExpirationDate="";
		private String CheckFrontImage="";
		private String CheckBackImage="";
		private String Mobile_Info="";
		private String App_Info="";
			/**
		 * @return the serviceNameType
		 */
		public String getServiceNameType() {
			return ServiceNameType;
		}
		/**
		 * @param serviceNameType the serviceNameType to set
		 */
		public void setServiceNameType(String serviceNameType) {
			ServiceNameType = serviceNameType;
		}
				
		/**
		 * @return the newEmail
		 */
		public String getNewEmail() {
			return newEmail;
		}
		/**
		 * @param newEmail the newEmail to set
		 */
		public void setNewEmail(String newEmail) {
			this.newEmail = newEmail;
		}
		public String getResetComment() {
			return ResetComment;
		}
		/**
		 * @return the service_Attribute
		 */
		
		/**
		 * @param resetComment the resetComment to set
		 */
		public void setResetComment(String resetComment) {
			ResetComment = resetComment;
		}
		
		/**
		 * @return the service_Attribute
		 */
		public String getService_Attribute() {
			return Service_Attribute;
		}
		/**
		 * @param service_Attribute the service_Attribute to set
		 */
		public void setService_Attribute(String service_Attribute) {
			Service_Attribute = service_Attribute;
		}
		public String getServiceClass() {
			return ServiceClass;
		}
		/**
		 * @param serviceClass the serviceClass to set
		 */
		public void setServiceClass(String serviceClass) {
			ServiceClass = serviceClass;
		}
		/**
		 * @return the mobile_Info
		 */
		public String getMobile_Info() {
			return Mobile_Info;
		}
		/**
		 * @param mobile_Info the mobile_Info to set
		 */
		public void setMobile_Info(String mobile_Info) {
			Mobile_Info = mobile_Info;
		}
		/**
		 * @return the app_Info
		 */
		public String getApp_Info() {
			return App_Info;
		}
		/**
		 * @param app_Info the app_Info to set
		 */
		public void setApp_Info(String app_Info) {
			App_Info = app_Info;
		}
		public String getCheckNo() {
			return CheckNo;
		}
		/**
		 * @param checkNo the checkNo to set
		 */
		public void setCheckNo(String checkNo) {
			CheckNo = checkNo;
		}
		/**
		 * @return the licenseNo
		 */
		public String getLicenseNo() {
			return LicenseNo;
		}
		/**
		 * @param licenseNo the licenseNo to set
		 */
		public void setLicenseNo(String licenseNo) {
			LicenseNo = licenseNo;
		}
		/**
		 * @return the expirationDate
		 */
		public String getExpirationDate() {
			return ExpirationDate;
		}
		/**
		 * @param expirationDate the expirationDate to set
		 */
		public void setExpirationDate(String expirationDate) {
			ExpirationDate = expirationDate;
		}
		/**
		 * @return the checkFrontImage
		 */
		public String getCheckFrontImage() {
			return CheckFrontImage;
		}
		/**
		 * @param checkFrontImage the checkFrontImage to set
		 */
		public void setCheckFrontImage(String checkFrontImage) {
			CheckFrontImage = checkFrontImage;
		}
		/**
		 * @return the checkBackImage
		 */
		public String getCheckBackImage() {
			return CheckBackImage;
		}
		/**
		 * @param checkBackImage the checkBackImage to set
		 */
		public void setCheckBackImage(String checkBackImage) {
			CheckBackImage = checkBackImage;
		}
		public String getServiceID_new() {
			return ServiceID_new;
		}
		public void setServiceID_new(String serviceID_new) {
			ServiceID_new = serviceID_new;
		}
		
		
		public String getBeforeImage() {
			return BeforeImage;
		}
		public void setBeforeImage(String beforeImage) {
			BeforeImage = beforeImage;
		}
		public String getAfterImage() {
			return AfterImage;
		}
		public void setAfterImage(String afterImage) {
			AfterImage = afterImage;
		}
		public String getAudioFile() {
			return AudioFile;
		}
		public void setAudioFile(String audioFile) {
			AudioFile = audioFile;
		}
		public String getFirst_Name() {
			return First_Name;
		}
		public void setFirst_Name(String first_Name) {
			First_Name = first_Name;
		}
		public String getLast_Name() {
			return Last_Name;
		}
		public void setLast_Name(String last_Name) {
			Last_Name = last_Name;
		}
		public String getFullName() {
			return FullName;
		}
		public void setFullName(String full_Name) {
			FullName = full_Name;
		}
		/*public String getServicePerson() {
			return ServicePerson;
		}
		public void setServicePerson(String servicePerson) {
			ServicePerson = servicePerson;
		}*/
		public String getEmail_Id() {
			return Email_Id;
		}
		public void setEmail_Id(String email_Id) {
			Email_Id = email_Id;
		}
		public String getSecondaryEmail() {
			return SecondaryEmail;
		}
		public void setSecondaryEmail(String secondaryEmail) {
			SecondaryEmail = secondaryEmail;
		}
		public String getAccountNo() {
			return AccountNo;
		}
		public void setAccountNo(String accountNo) {
			AccountNo = accountNo;
		}
		public String getHome_Phone() {
			return Home_Phone;
		}
		public void setHome_Phone(String home_Phone) {
			Home_Phone = home_Phone;
		}
		public String getCell_Phone() {
			return Cell_Phone;
		}
		public void setCell_Phone(String cell_Phone) {
			Cell_Phone = cell_Phone;
		}
		public String getSalesperson_ID() {
			return Salesperson_ID;
		}
		public void setSalesperson_ID(String salesperson_ID) {
			Salesperson_ID = salesperson_ID;
		}
		
		public String getServiceTime() {
			return ServiceTime;
		}
		public void setServiceTime(String serviceTime) {
			ServiceTime = serviceTime;
		}
		public String getServiceDate() {
			return ServiceDate;
		}
		public void setServiceDate(String serviceDate) {
			ServiceDate = serviceDate;
		}
		public String getServiceStatus() {
			return ServiceStatus;
		}
		public void setServiceStatus(String serviceStatus) {
			ServiceStatus = serviceStatus;
		}
		public String getNotes() {
			return Notes;
		}
		public void setNotes(String notes) {
			Notes = notes;
		}
		public String getServiceCode() {
			return ServiceCode;
		}
		public void setServiceCode(String serviceCode) {
			ServiceCode = serviceCode;
		}
		
		public String getBusinessName() {
			return BusinessName;
		}
		public void setBusinessName(String businessName) {
			BusinessName = businessName;
		}
		public String getBranch() {
			return Branch;
		}
		public void setBranch(String branch) {
			Branch = branch;
		}
		public String getTaxCode() {
			return TaxCode;
		}
		public void setTaxCode(String taxCode) {
			TaxCode = taxCode;
		}
		public String getEstimateTime() {
			return EstimateTime;
		}
		public void setEstimateTime(String estimateTime) {
			EstimateTime = estimateTime;
		}
		public String getArrival_Duration() {
			return Arrival_Duration;
		}
		public void setArrival_Duration(String arrival_Duration) {
			Arrival_Duration = arrival_Duration;
		}
		public String getProblemDescription() {
			return ProblemDescription;
		}
		public void setProblemDescription(String problemDescription) {
			ProblemDescription = problemDescription;
		}
		public String getPSPCustomer() {
			return PSPCustomer;
		}
		public void setPSPCustomer(String pSPCustomer) {
			PSPCustomer = pSPCustomer;
		}
		public String getCreate_By() {
			return Create_By;
		}
		public void setCreate_By(String create_By) {
			Create_By = create_By;
		}
		public String getUpdate_By() {
			return Update_By;
		}
		public void setUpdate_By(String update_By) {
			Update_By = update_By;
		}
		public String getCreate_Date() {
			return Create_Date;
		}
		public void setCreate_Date(String create_Date) {
			Create_Date = create_Date;
		}
		public String getUpdate_Date() {
			return Update_Date;
		}
		public void setUpdate_Date(String update_Date) {
			Update_Date = update_Date;
		}
		public String getRef_Source() {
			return Ref_Source;
		}
		public void setRef_Source(String ref_Source) {
			Ref_Source = ref_Source;
		}
		public String getOrder_Number() {
			return Order_Number;
		}
		public void setOrder_Number(String order_Number) {
			Order_Number = order_Number;
		}
		public String getRoute_Name() {
			return Route_Name;
		}
		public void setRoute_Name(String route_Name) {
			Route_Name = route_Name;
		}
		public String getRoute_Number() {
			return Route_Number;
		}
		public void setRoute_Number(String route_Number) {
			Route_Number = route_Number;
		}
		public String getKeyMap() {
			return KeyMap;
		}
		public void setKeyMap(String keyMap) {
			KeyMap = keyMap;
		}
		public String getService_Tech_Num() {
			return Service_Tech_Num;
		}
		public void setService_Tech_Num(String service_Tech_Num) {
			Service_Tech_Num = service_Tech_Num;
		}
		public String getService_Status_Reason() {
			return Service_Status_Reason;
		}
		public void setService_Status_Reason(String service_Status_Reason) {
			Service_Status_Reason = service_Status_Reason;
		}
		public String getCompanyName() {
			return CompanyName;
		}
		public void setCompanyName(String companyName) {
			CompanyName = companyName;
		}
		public String getServiceDescription() {
			return ServiceDescription;
		}
		public void setServiceDescription(String serviceDescription) {
			ServiceDescription = serviceDescription;
		}
		public String getOtherInstruction() {
			return OtherInstruction;
		}
		public void setOtherInstruction(String otherInstruction) {
			OtherInstruction = otherInstruction;
		}
		
		public String getTotal() {
			return Total;
		}
		public void setTotal(String total) {
			Total = total;
		}
		public String getLocationBalance() {
			return LocationBalance;
		}
		public void setLocationBalance(String locationBalance) {
			LocationBalance = locationBalance;
		}
		public String getTimeInDateTime() {
			return TimeInDateTime;
		}
		public void setTimeInDateTime(String timeInDateTime) {
			TimeInDateTime = timeInDateTime;
		}
		public String getBranch_ID() {
			return Branch_ID;
		}
		public void setBranch_ID(String branch_ID) {
			Branch_ID = branch_ID;
		}
		
		public String getPrv_Bal() {
			return Prv_Bal;
		}
		public void setPrv_Bal(String prv_Bal) {
			Prv_Bal = prv_Bal;
		}
		
		/**
		 * @return the department
		 */
		public String getDepartment() {
			return department;
		}
		/**
		 * @param department the department to set
		 */
		public void setDepartment(String department) {
			this.department = department;
		}
		/**
		 * @return the specialInstruction
		 */
		public String getSpecialInstruction() {
			return specialInstruction;
		}
		/**
		 * @param specialInstruction the specialInstruction to set
		 */
		public void setSpecialInstruction(String specialInstruction) {
			this.specialInstruction = specialInstruction;
		}
		/**
		 * @return the inv_value
		 */
		public String getInv_value() {
			return Inv_value;
		}
		/**
		 * @param inv_value the inv_value to set
		 */
		public void setInv_value(String inv_value) {
			Inv_value = inv_value;
		}
		/**
		 * @return the prod_value
		 */
		public String getProd_value() {
			return Prod_value;
		}
		/**
		 * @param prod_value the prod_value to set
		 */
		public void setProd_value(String prod_value) {
			Prod_value = prod_value;
		}
		/**
		 * @return the tax_value
		 */
		public String getTax_value() {
			return Tax_value;
		}
		/**
		 * @param tax_value the tax_value to set
		 */
		public void setTax_value(String tax_value) {
			Tax_value = tax_value;
		}
		public String getOffice_Note_ByRep() {
			return Office_Note_ByRep;
		}
		public void setOffice_Note_ByRep(String office_Note_ByRep) {
			Office_Note_ByRep = office_Note_ByRep;
		}
		public String getNote_Status() {
			return Note_Status;
		}
		public void setNote_Status(String note_Status) {
			Note_Status = note_Status;
		}
		public String getNote_Closed_Text() {
			return Note_Closed_Text;
		}
		public void setNote_Closed_Text(String note_Closed_Text) {
			Note_Closed_Text = note_Closed_Text;
		}
		public String getNote_Closed_Date() {
			return Note_Closed_Date;
		}
		public void setNote_Closed_Date(String note_Closed_Date) {
			Note_Closed_Date = note_Closed_Date;
		}
		public String getPayment_Collection_Status() {
			return Payment_Collection_Status;
		}
		public void setPayment_Collection_Status(String payment_Collection_Status) {
			Payment_Collection_Status = payment_Collection_Status;
		}
		public String getCollection_Text() {
			return Collection_Text;
		}
		public void setCollection_Text(String collection_Text) {
			Collection_Text = collection_Text;
		}
		public String getCollection_Date() {
			return Collection_Date;
		}
		public void setCollection_Date(String collection_Date) {
			Collection_Date = collection_Date;
		}
		public String getFlag() {
			return Flag;
		}
		public void setFlag(String flag) {
			Flag = flag;
		}
		public String getIsElectronicSignatureAvailable() {
			return IsElectronicSignatureAvailable;
		}
		public void setIsElectronicSignatureAvailable(
				String isElectronicSignatureAvailable) {
			IsElectronicSignatureAvailable = isElectronicSignatureAvailable;
		}
		public String getOpenWONotificationFlag() {
			return OpenWONotificationFlag;
		}
		public void setOpenWONotificationFlag(String openWONotificationFlag) {
			OpenWONotificationFlag = openWONotificationFlag;
		}
		/*public String getTimeOutDateTime() {
			return TimeOutDateTime;
		}
		public void setTimeOutDateTime(String timeOutDateTime) {
			TimeOutDateTime = timeOutDateTime;
		}*/
		/*public String getServiceAddress() {
			return ServiceAddress;
		}
		public void setServiceAddress(String serviceAddress) {
			ServiceAddress = serviceAddress;
		}*/
		/*public String getBillingAddress() {
			return BillingAddress;
		}
		public void setBillingAddress(String billingAddress) {
			BillingAddress = billingAddress;
		}*/
		public String getTimeIn() {
			return TimeIn;
		}
		public void setTimeIn(String timeIn) {
			TimeIn = timeIn;
		}
		public String getAddress_Line1() {
			return Address_Line1;
		}
		public void setAddress_Line1(String address_Line1) {
			Address_Line1 = address_Line1;
		}
		public String getAddress_Line2() {
			return Address_Line2;
		}
		public void setAddress_Line2(String address_Line2) {
			Address_Line2 = address_Line2;
		}
		public String getCity() {
			return City;
		}
		public void setCity(String city) {
			City = city;
		}
		public String getState() {
			return State;
		}
		public void setState(String state) {
			State = state;
		}
		public String getZipCode() {
			return ZipCode;
		}
		public void setZipCode(String zipCode) {
			ZipCode = zipCode;
		}
		public String getPoc_FName() {
			return Poc_FName;
		}
		public void setPoc_FName(String poc_FName) {
			Poc_FName = poc_FName;
		}
		public String getPoc_LName() {
			return Poc_LName;
		}
		public void setPoc_LName(String poc_LName) {
			Poc_LName = poc_LName;
		}
		public String getBAddress_Line1() {
			return BAddress_Line1;
		}
		public void setBAddress_Line1(String bAddress_Line1) {
			BAddress_Line1 = bAddress_Line1;
		}
		public String getBAddress_Line2() {
			return BAddress_Line2;
		}
		public void setBAddress_Line2(String bAddress_Line2) {
			BAddress_Line2 = bAddress_Line2;
		}
		public String getBCity() {
			return BCity;
		}
		public void setBCity(String bCity) {
			BCity = bCity;
		}
		public String getBState() {
			return BState;
		}
		public void setBState(String bState) {
			BState = bState;
		}
		public String getBZipCode() {
			return BZipCode;
		}
		public void setBZipCode(String bZipCode) {
			BZipCode = bZipCode;
		}
		public String getServiceLatitude() {
			return ServiceLatitude;
		}
		public void setServiceLatitude(String serviceLatitude) {
			ServiceLatitude = serviceLatitude;
		}
		public String getServiceLongitude() {
			return ServiceLongitude;
		}
		public void setServiceLongitude(String serviceLongitude) {
			ServiceLongitude = serviceLongitude;
		}
		/*public String getCreated_By() {
			return Created_By;
		}
		public void setCreated_By(String created_By) {
			Created_By = created_By;
		}*/
		/*public String getUpdated_By() {
			return Updated_By;
		}
		public void setUpdated_By(String updated_By) {
			Updated_By = updated_By;
		}*/
	}
