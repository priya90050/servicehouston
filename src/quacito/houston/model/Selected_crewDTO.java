package quacito.houston.model;

public class Selected_crewDTO 
{
	private String ServiceID_new="";
	private String CrewMembers="";
	private String Lead_ID="";
	/**
	 * @return the serviceID_new
	 */
	public String getServiceID_new() {
		return ServiceID_new;
	}
	/**
	 * @param serviceID_new the serviceID_new to set
	 */
	public void setServiceID_new(String serviceID_new) {
		ServiceID_new = serviceID_new;
	}
	/**
	 * @return the crewMembers
	 */
	public String getCrewMembers() {
		return CrewMembers;
	}
	/**
	 * @param crewMembers the crewMembers to set
	 */
	public void setCrewMembers(String crewMembers) {
		CrewMembers = crewMembers;
	}
	/**
	 * @return the lead_ID
	 */
	public String getLead_ID() {
		return Lead_ID;
	}
	/**
	 * @param lead_ID the lead_ID to set
	 */
	public void setLead_ID(String lead_ID) {
		Lead_ID = lead_ID;
	}
	
	
}
