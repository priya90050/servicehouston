package quacito.houston.model;

public class CrewMemberDTO 
{
	private String ServiceID_new="";
	private String Route_ID="";
	private String CrewMember_ID="";
	private String Tech_Name="";
	private String Status="";
	private String IsChecked="";
	/**
	 * @return the isChecked
	 */
	public String getIsChecked() {
		return IsChecked;
	}
	/**
	 * @param isChecked the isChecked to set
	 */
	public void setIsChecked(String isChecked) {
		IsChecked = isChecked;
	}
	/**
	 * @return the serviceID_new
	 */
	
	
	public String getServiceID_new() {
		return ServiceID_new;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return Status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		Status = status;
	}
	/**
	 * @param serviceID_new the serviceID_new to set
	 */
	public void setServiceID_new(String serviceID_new) {
		ServiceID_new = serviceID_new;
	}
	/**
	 * @return the route_ID
	 */
	public String getRoute_ID() {
		return Route_ID;
	}
	/**
	 * @param route_ID the route_ID to set
	 */
	public void setRoute_ID(String route_ID) {
		Route_ID = route_ID;
	}
	/**
	 * @return the crewMember_ID
	 */
	public String getCrewMember_ID() {
		return CrewMember_ID;
	}
	/**
	 * @param crewMember_ID the crewMember_ID to set
	 */
	public void setCrewMember_ID(String crewMember_ID) {
		CrewMember_ID = crewMember_ID;
	}
	/**
	 * @return the tech_Name
	 */
	public String getTech_Name() {
		return Tech_Name;
	}
	/**
	 * @param tech_Name the tech_Name to set
	 */
	public void setTech_Name(String tech_Name) {
		Tech_Name = tech_Name;
	}
	/**
	 * @return the status
	 */
	
	
	
}
