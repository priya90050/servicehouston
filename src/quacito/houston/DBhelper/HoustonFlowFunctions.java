package quacito.houston.DBhelper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import quacito.houston.CommonUtilities.CommonFunction;
import quacito.houston.CommonUtilities.LoggerUtil;
import quacito.houston.CommonUtilities.ParameterUtil;
import quacito.houston.model.ChemicalDto;
import quacito.houston.model.CrewMemberDTO;
import quacito.houston.model.HCommercial_pest_DTO;
import quacito.houston.model.EmployeeDTO;
import quacito.houston.model.GeneralInfoDTO;
import quacito.houston.model.HResidential_pest_DTO;
import quacito.houston.model.LogDTO;
import quacito.houston.model.OResidential_pest_DTO;

import quacito.houston.model.Selected_crewDTO;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.DatabaseUtils.InsertHelper;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.gson.Gson;

public class HoustonFlowFunctions {
	static Context context;
	static int version = 0;

	public HoustonFlowFunctions(Context context) {
		super();
		this.context = context;

		try {
			version = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0).versionCode;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
	}

	// Change///
	public long addServicesGeneralSRDetail(JSONObject jsonStr) {
		long vals = 0;
		try {
			Gson gson = new Gson();
			GeneralInfoDTO generalinfodto = gson.fromJson(jsonStr.toString(),
					GeneralInfoDTO.class);
			JSONObject jsonStr2 = new JSONObject(gson.toJson(generalinfodto));
			// LoggerUtil.e("residential added1..", jsonStr.toString());
			SQLiteDatabase db = getWritableDB();
			ContentValues cv = objectToContentValues(jsonStr2);
			vals = db.insert(DatabaseHelper.TABLE_GENERAL_INFO, null, cv);
			// Log.e("addServicesGeneralSRDetail", ""+vals);
			db.close();
		} catch (Exception e) {
			LoggerUtil.e("Exception addServicesGeneralSRDetail", e.toString());
		}

		return vals;
	}

	public GeneralInfoDTO getServiceDTODetial(String serviceNew_ID) {
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select * from "
				+ DatabaseHelper.TABLE_GENERAL_INFO + " where "
				+ ParameterUtil.ServiceID_new + "='" + serviceNew_ID + "'";
		LoggerUtil.e("select query", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONObject resultSet = cursorToJSONOjbect(cursor);
		//LoggerUtil.e("resultSet", resultSet + "");
		cursor.close();
		db.close();
		if (resultSet != null) {
			GeneralInfoDTO dto = new Gson().fromJson(resultSet.toString(),
					GeneralInfoDTO.class);
			return dto;
		}

		return null;
	}
	public long inserCrewDynamically(CrewMemberDTO crewMemberDTO) {
		// TODO Auto-generated method stub
		long vals = 0;
		try {
			Gson gson = new Gson();
			
			JSONObject jsonStr2 = new JSONObject(
					gson.toJson(crewMemberDTO));
			// LoggerUtil.e("residential added1..", jsonStr.toString());
			SQLiteDatabase db = getWritableDB();
			ContentValues cv = objectToContentValues(jsonStr2);
			vals = db.insert(DatabaseHelper.TABLE_CREW_MEMBER, null,
					cv);
			Log.e("TABLE_CREW_MEMBER added1. values", ""+vals);
			db.close();

		} catch (Exception e) {
			LoggerUtil.e("Exception", e.toString());
		}

		return vals;
	}
	public List<String> getCrewNameFromEmpTbl(String strCrewIds) {
		// TODO Auto-generated method stub
        List<String> listtechName = new ArrayList<String>();
		
		SQLiteDatabase db = getWritableDB();
		
		String selectQuerty = "select " + ParameterUtil.EmpName
				+ " from " + DatabaseHelper.TABLE_EMPLOYEE_INFO + " where "
				+ ParameterUtil.EmployeeId + " in (" + strCrewIds +")";
		
		//SELECT * FROM users where id in (1,2,3,4,5);
		
		LoggerUtil.e("select query", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			listtechName.add(cursor.getString(cursor
					.getColumnIndex(ParameterUtil.EmpName))); // add
																		
			cursor.moveToNext();
		}
		
		cursor.close();
		db.close();
		return listtechName;
	}
	
	public List<String> getAssignedCrewByAssignForDayLead_ID(String assignForDayLead_ID) {
		// TODO Auto-generated method stub
        List<String> listtechId = null;
		
		SQLiteDatabase db = getWritableDB();
		
		String selectQuerty = "select " + ParameterUtil.AssignForDayCrewMember_ID
				+ " from " + DatabaseHelper.TABLE_GET_ASSIGN_CREW_FOR_DAY + " where "
				+ ParameterUtil.AssignForDayLeadID + "='" + assignForDayLead_ID + "'";
		
		LoggerUtil.e("select query", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			listtechId.add(cursor.getString(cursor
					.getColumnIndex(ParameterUtil.AssignForDayCrewMember_ID))); // add
																		
			cursor.moveToNext();
		}
		
		cursor.close();
		db.close();
		return listtechId;
	}
	public List<String> getAllCrewByRouteId(String routeId) {
		// TODO Auto-generated method stub
		List<String> listtechId = new ArrayList<String>();
		
		SQLiteDatabase db = getWritableDB();
		
		String selectQuerty = "select " + ParameterUtil.Technician_ID
				+ " from " + DatabaseHelper.TABLE_GET_ALL_ROUTE_CREW + " where "
				+ ParameterUtil.Route_ID + "='" + routeId + "'";
		
		LoggerUtil.e("select query", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		cursor.moveToFirst();
		if(cursor.getCount() > 0) {
		while (!cursor.isAfterLast()) {
			listtechId.add(cursor.getString(cursor
					.getColumnIndex(ParameterUtil.Technician_ID))); // add
																		
			cursor.moveToNext();
		}
		}
		cursor.close();
		db.close();
		return listtechId;
		
	}
	
	public int checkInRouteMaster(String routeId, String salesperson_ID) {
		
		// TODO Auto-generated method stub
				SQLiteDatabase db = getWritableDB();
				String selectQuerty = "select "+ParameterUtil.RouteMaster_ID+" from " + DatabaseHelper.TABLE_GET_ALL_ROUTE + " where "
						+ ParameterUtil.RouteMaster_ID + "='" + routeId + "'"+" AND "
						+ ParameterUtil.Lead_ID + "='" + salesperson_ID + "'COLLATE NOCASE ";
				
				
				LoggerUtil.e("select query", selectQuerty);
				Cursor cursor = db.rawQuery(selectQuerty, null);
				LoggerUtil.e("select query", cursor.getCount()+"");
				int i=0;
				if(cursor.getCount() > 0) {
					
		             cursor.moveToFirst();
		             i = cursor.getInt(cursor.getColumnIndex(ParameterUtil.RouteMaster_ID));
		             LoggerUtil.e("checkInRouteMaster", i + "");
		         }
				
				cursor.close();
				db.close();
				
				return i;
		
	}
	public int getAssignedLeadForDay(String valueOf, String serviceDate,
			String salesperson_ID) {
		// TODO Auto-generated method stub
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select "+ParameterUtil.AssignForDayLead_ID+" from " + DatabaseHelper.TABLE_GET_ASSIGN_LEAD_FOR_DAY + " where "
				+ ParameterUtil.Route_ID + "='" + valueOf + "'"+" AND "
				+ ParameterUtil.Emp_ID + "='" + salesperson_ID + "'" + " AND "
				+ ParameterUtil.Assign_Date + "='" + serviceDate + "'COLLATE NOCASE ";
		
		
		LoggerUtil.e("select query", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		LoggerUtil.e("select query", cursor.getCount()+"");
		int i=0;
		if(cursor.getCount() > 0) {
			
             cursor.moveToFirst();
             i = cursor.getInt(cursor.getColumnIndex(ParameterUtil.AssignForDayLead_ID));
             LoggerUtil.e("getAssignedLeadForDay", i + "");
         }
		
		cursor.close();
		db.close();
		
		return i;
	}
	
	public int getRouteId_Tbl_GetAllRoute(String routeName) {
		//routeName = "Te";
		SQLiteDatabase db = getWritableDB();
		String selectQuertyRouteId = "select "+ParameterUtil.RouteMaster_ID+" from "
				+ DatabaseHelper.TABLE_GET_ALL_ROUTE + " where "
				+ ParameterUtil.Route_Name + "='" + routeName + "' COLLATE NOCASE";
		
		LoggerUtil.e("select query", selectQuertyRouteId);
		Cursor cursor = db.rawQuery(selectQuertyRouteId, null);
		LoggerUtil.e("select query", cursor.getCount()+"");
		int i=0;
		if(cursor.getCount() > 0) {
			
             cursor.moveToFirst();
             i = cursor.getInt(cursor.getColumnIndex(ParameterUtil.RouteMaster_ID));
             LoggerUtil.e("RouteId", i + "");
         }
		
		cursor.close();
		db.close();
		
		return i;
	}
	
	public HResidential_pest_DTO getResidential_Pest_DTO(String serviceNew_ID) {
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select * from "
				+ DatabaseHelper.TABLE_HR_PEST_INFO + " where "
				+ ParameterUtil.ServiceID_new + "='" + serviceNew_ID + "'";
		LoggerUtil.e("select query", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONObject resultSet = cursorToJSONOjbect(cursor);
		//LoggerUtil.e("resultSet", resultSet + "");
		cursor.close();
		db.close();
		if (resultSet != null) {
			HResidential_pest_DTO dto = new Gson().fromJson(
					resultSet.toString(), HResidential_pest_DTO.class);
			return dto;
		}

		return null;
	}

	public HCommercial_pest_DTO getCommercial_Pest_DTO(String serviceNew_ID) {
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select * from "
				+ DatabaseHelper.TABLE_HC_PEST_INFO + " where "
				+ ParameterUtil.ServiceID_new + "='" + serviceNew_ID + "'";
		LoggerUtil.e("select query", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONObject resultSet = cursorToJSONOjbect(cursor);
		//LoggerUtil.e("resultSet", resultSet + "");
		cursor.close();
		db.close();
		if (resultSet != null) {
			HCommercial_pest_DTO dto = new Gson().fromJson(resultSet.toString(),
					HCommercial_pest_DTO.class);
			return dto;
		}

		return null;
	}

	public OResidential_pest_DTO getOrlandoRes_Pest_DTO(String serviceNew_ID) {
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select * from "
				+ DatabaseHelper.TABLE_HC_PEST_INFO + " where "
				+ ParameterUtil.ServiceID_new + "='" + serviceNew_ID + "'";
		LoggerUtil.e("select query", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONObject resultSet = cursorToJSONOjbect(cursor);
		//LoggerUtil.e("resultSet", resultSet + "");
		cursor.close();
		db.close();
		if (resultSet != null) {
			OResidential_pest_DTO dto = new Gson().fromJson(resultSet.toString(),
					OResidential_pest_DTO.class);
			return dto;
		}

		return null;
	}

	
	public long addChemicals(JSONObject jsonStr) {
		SQLiteDatabase db = getWritableDB();
		ContentValues cv = objectToContentValues(jsonStr);
		long vals = db.insert(DatabaseHelper.TABLE_CHEMICALS, null, cv);
		// LoggerUtil.e("addChemicals vals", ""+vals);
		db.close();
		return vals;
	}

	public long addServicesForResidential(JSONObject jsonStr) {
		long vals = 0;
		try {
			Gson gson = new Gson();
			HResidential_pest_DTO residential_PestDTO = gson.fromJson(
					jsonStr.toString(), HResidential_pest_DTO.class);
			JSONObject jsonStr2 = new JSONObject(
					gson.toJson(residential_PestDTO));
			// LoggerUtil.e("residential added1..", jsonStr.toString());
			SQLiteDatabase db = getWritableDB();
			ContentValues cv = objectToContentValues(jsonStr2);
			vals = db.insert(DatabaseHelper.TABLE_HR_PEST_INFO, null,
					cv);
			// Log.e("residential added1. values", ""+vals);
			db.close();

		} catch (Exception e) {
			LoggerUtil.e("Exception", e.toString());
		}

		return vals;
	}

	public long addServicesForCommercial(JSONObject jsonStr) {
		long vals = 0;
		try {
			Gson gson = new Gson();
			HCommercial_pest_DTO hCommercial_pest_DTO = gson.fromJson(jsonStr.toString(),
					HCommercial_pest_DTO.class);
			JSONObject jsonStr2 = new JSONObject(gson.toJson(hCommercial_pest_DTO));
			// LoggerUtil.e("residential added1..", jsonStr.toString());
			SQLiteDatabase db = getWritableDB();
			ContentValues cv = objectToContentValues(jsonStr2);
			vals = db.insert(DatabaseHelper.TABLE_HC_PEST_INFO, null,
					cv);
			Log.e("commercial added1. values", "" + vals);
			db.close();

		} catch (Exception e) {
			LoggerUtil.e("Exception", e.toString());
		}

		return vals;
	}
	public long addServicesForOrlandoResi(JSONObject jsonStr) {
		long vals = 0;
		try {
			Gson gson = new Gson();
			OResidential_pest_DTO oResidential_pest_DTO = gson.fromJson(jsonStr.toString(),
					OResidential_pest_DTO.class);
			JSONObject jsonStr2 = new JSONObject(gson.toJson(oResidential_pest_DTO));
			// LoggerUtil.e("residential added1..", jsonStr.toString());
			SQLiteDatabase db = getWritableDB();
			ContentValues cv = objectToContentValues(jsonStr2);
			vals = db.insert(DatabaseHelper.TABLE_OR_PEST_INFO, null,
					cv);
			Log.e("orlando residential added1. values", "" + vals);
			db.close();

		} catch (Exception e) {
			LoggerUtil.e("Exception", e.toString());
		}

		return vals;
	}

	// End

	// Get list of all employees
	public JSONArray getAllEmployee() {
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select * from "
				+ DatabaseHelper.TABLE_EMPLOYEE_INFO;
		// LoggerUtil.e("select query", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONArray resultSet = cursorToJSONArray(cursor);
		// LoggerUtil.e("resultSet", resultSet.toString());
		cursor.close();
		db.close();
		return resultSet;
	}

	// Get All service list WHERE ISCOMPLEATED is True
	public JSONArray getAllServiceChangeLocaly() {
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select * from "
				+ DatabaseHelper.TABLE_GENERAL_INFO + " where "
				+ ParameterUtil.isCompleted + "='true'";
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONArray resultSet = cursorToJSONArray(cursor);
		cursor.close();
		db.close();
		return resultSet;
	}

	// Get all records from service list table where EmployeeId is equlas to
	// strSalesperson_ID
	public EmployeeDTO getSelectedEmploye(String strSalesperson_ID) {
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select * from "
				+ DatabaseHelper.TABLE_EMPLOYEE_INFO + " where "
				+ ParameterUtil.EmployeeId + "='" + strSalesperson_ID + "'";
		LoggerUtil.e("select query", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONObject resultSet = cursorToJSONOjbect(cursor);
		//LoggerUtil.e("resultSet", resultSet + "");
		cursor.close();
		db.close();
		if (resultSet != null) {
			EmployeeDTO dto = new Gson().fromJson(resultSet.toString(),
					EmployeeDTO.class);
			return dto;
		}

		return null;
	}

	// Get all records from service list table where ServiceID_new is true
	public JSONObject getSelectedService(String strServiceIdNew) {
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select * from "
				+ DatabaseHelper.TABLE_GENERAL_INFO + " where "
				+ ParameterUtil.ServiceID_new + " = " + strServiceIdNew;
		// LoggerUtil.e("select query", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONObject resultSet = cursorToJSONOjbect(cursor);
		// LoggerUtil.e("resultSet", resultSet.toString());
		cursor.close();
		db.close();
		return resultSet;
	}

	// check if records are already present in orlando residential table
		public static JSONObject getOrlandoResidentialReport(String strServiceIdNew) {
			SQLiteDatabase db = getWritableDB();
			String selectQuerty = "select * from "
					+ DatabaseHelper.TABLE_OR_PEST_INFO + " where "
					+ ParameterUtil.ServiceID_new + " =" + strServiceIdNew;
			// LoggerUtil.e("select query", selectQuerty);
			Cursor cursor = db.rawQuery(selectQuerty, null);
			JSONObject resultSet = cursorToJSONOjbect(cursor);
			// LoggerUtil.e("resultSet getTodaysReport", resultSet+"");
			cursor.close();
			db.close();
			return resultSet;
		}
	
	// check if records are already present in residential table
	public static JSONObject getTodaysReport(String strServiceIdNew) {
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select * from "
				+ DatabaseHelper.TABLE_HR_PEST_INFO + " where "
				+ ParameterUtil.ServiceID_new + " =" + strServiceIdNew;
		// LoggerUtil.e("select query", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONObject resultSet = cursorToJSONOjbect(cursor);
		// LoggerUtil.e("resultSet getTodaysReport", resultSet+"");
		cursor.close();
		db.close();
		return resultSet;
	}

	// check if records are already present in residential table
	public static JSONObject getTodaysCommercialReport(String strServiceIdNew) {
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select * from "
				+ DatabaseHelper.TABLE_HC_PEST_INFO + " where "
				+ ParameterUtil.ServiceID_new + " =" + strServiceIdNew;
		// LoggerUtil.e("select query", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONObject resultSet = cursorToJSONOjbect(cursor);
		// LoggerUtil.e("resultSet getTodaysReport", resultSet+"");
		cursor.close();
		db.close();
		return resultSet;
	}
	// check if records are already present in orlando table
		public static JSONObject getTodaysOrlandoReport(String strServiceIdNew) {
			SQLiteDatabase db = getWritableDB();
			String selectQuerty = "select * from "
					+ DatabaseHelper.TABLE_OR_PEST_INFO + " where "
					+ ParameterUtil.ServiceID_new + " =" + strServiceIdNew;
			// LoggerUtil.e("select query", selectQuerty);
			Cursor cursor = db.rawQuery(selectQuerty, null);
			JSONObject resultSet = cursorToJSONOjbect(cursor);
			// LoggerUtil.e("resultSet getTodaysReport", resultSet+"");
			cursor.close();
			db.close();
			return resultSet;
		}
	public static JSONObject getCommercialReport(String strServiceIdNew) {
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select * from "
				+ DatabaseHelper.TABLE_HC_PEST_INFO + " where "
				+ ParameterUtil.ServiceID_new + " =" + strServiceIdNew;
		 LoggerUtil.e("select query", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONObject resultSet = cursorToJSONOjbect(cursor);
		// LoggerUtil.e("resultSet getTodaysReport", resultSet+"");
		cursor.close();
		db.close();
		return resultSet;
	}

	public int update(GeneralInfoDTO generalInfoDTO) {
		int i = 0;

		SQLiteDatabase db = getWritableDB();
		// String p= cursor.getString(2);
		Gson gson = new Gson();

		JSONObject jsonServiceDto = null;
		try {
			jsonServiceDto = new JSONObject(gson.toJson(generalInfoDTO));
			LoggerUtil.e("jsonServiceDto", jsonServiceDto.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ContentValues values = objectToContentValues(jsonServiceDto);
		i = db.update(
				DatabaseHelper.TABLE_GENERAL_INFO,
				values,
				ParameterUtil.ServiceID_new + "='"
						+ generalInfoDTO.getServiceID_new() + "'", null);
		db.close();

		return i;
	}

	// convert cursor to json object
	public static JSONObject cursorToJSONOjbect(Cursor cursor) {
		if (cursor.moveToFirst()) {

			int totalColumn = cursor.getColumnCount();
			// LoggerUtil.e("totalColumn", totalColumn+"");
			JSONObject rowObject = new JSONObject();
			for (int i = 0; i < totalColumn; i++) {
				if (cursor.getColumnName(i) != null) {
					try {
						rowObject.put(cursor.getColumnName(i),
								cursor.getString(i));
					} catch (Exception e) {
						LoggerUtil.e("Exception", e.getMessage());
					}
				}
			}
			return rowObject;
		}
		return null;
	}

	public static int updateResidential_Pest(
			HResidential_pest_DTO residential_PestDTO) {
		// TODO Auto-generated method stub

		int i = 0;

		SQLiteDatabase db = getWritableDB();
		// String p= cursor.getString(2);
		Gson gson = new Gson();

		JSONObject jsonResidentialDto = null;
		try {
			jsonResidentialDto = new JSONObject(
					gson.toJson(residential_PestDTO));
			// LoggerUtil.e("jsonResidential;Dto",
			// jsonResidentialDto.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ContentValues values = objectToContentValues(jsonResidentialDto);
		i = db.update(
				DatabaseHelper.TABLE_HR_PEST_INFO,
				values,
				ParameterUtil.ServiceID_new + "='"
						+ residential_PestDTO.getServiceID_new() + "'", null);

		db.close();

		return i;
	}

	public int updateCommercial_Pest(HCommercial_pest_DTO hCommercial_pest_DTO) {
		// TODO Auto-generated method stub

		int i = 0;

		SQLiteDatabase db = getWritableDB();
		// String p= cursor.getString(2);
		Gson gson = new Gson();

		JSONObject jsoncommercialDTO = null;
		try {
			jsoncommercialDTO = new JSONObject(gson.toJson(hCommercial_pest_DTO));
			// LoggerUtil.e("jsonResidential;Dto",
			// jsonResidentialDto.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ContentValues values = objectToContentValues(jsoncommercialDTO);
		i = db.update(
				DatabaseHelper.TABLE_HC_PEST_INFO,
				values,
				ParameterUtil.ServiceID_new + "='"
						+ hCommercial_pest_DTO.getServiceID_new() + "'", null);

		db.close();

		return i;
	}

	public static int updateLog(LogDTO logDTO) {
		// TODO Auto-generated method stub

		int i = 0;

		SQLiteDatabase db = getWritableDB();
		// String p= cursor.getString(2);
		Gson gson = new Gson();

		JSONObject jsonLogDto = null;
		try {
			jsonLogDto = new JSONObject(gson.toJson(logDTO));
			// LoggerUtil.e("jsonResidential;Dto",
			// jsonResidentialDto.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ContentValues values = objectToContentValues(jsonLogDto);
		i = db.update(DatabaseHelper.TABLE_LOG, values, ParameterUtil.SID
				+ "='" + logDTO.getSID() + "'", null);

		db.close();

		return i;
	}

	public static long addLog(JSONObject jsonStr) {
		long vals = 0;
		try {
			Gson gson = new Gson();
			LogDTO logDTO = gson.fromJson(jsonStr.toString(), LogDTO.class);
			JSONObject jsonStr2 = new JSONObject(gson.toJson(logDTO));
			// LoggerUtil.e("residential added1..", jsonStr.toString());
			SQLiteDatabase db = getWritableDB();
			ContentValues cv = objectToContentValues(jsonStr2);
			vals = db.insert(DatabaseHelper.TABLE_LOG, null, cv);
			// LoggerUtilgerUtil.e("residential added1. values", ""+vals);
			db.close();
		} catch (Exception e) {
			LoggerUtil.e("Exception", e.toString());
		}

		return vals;
	}

	/*// insert values in residential table...
	public static long addResidentialData(JSONObject jsonStr) {
		long vals = 0;
		try {
			Gson gson = new Gson();
			Residential_pest_DTO residential_PestDTO = gson.fromJson(
					jsonStr.toString(), Residential_pest_DTO.class);
			JSONObject jsonStr2 = new JSONObject(
					gson.toJson(residential_PestDTO));
			// LoggerUtil.e("residential added1..", jsonStr.toString());
			SQLiteDatabase db = getWritableDB();
			ContentValues cv = objectToContentValues(jsonStr2);
			vals = db.insert(DatabaseHelper.TABLE_HR_PEST_INFO, null,
					cv);
			// LoggerUtilgerUtil.e("residential added1. values", ""+vals);
			db.close();
		} catch (Exception e) {
			LoggerUtil.e("Exception", e.toString());
		}

		return vals;
	}*/
	/*// insert values in commercial table...
		.public long addCommercialData(JSONObject jsonStr) {
			long vals = 0;
			try {
				Gson gson = new Gson();
				CommercialDTO commercial_PestDTO = gson.fromJson(
						jsonStr.toString(), CommercialDTO.class);
				JSONObject jsonStr2 = new JSONObject(
						gson.toJson(commercial_PestDTO));
				// LoggerUtil.e("commercial_PestDTO added1..", jsonStr.toString());
				SQLiteDatabase db = getWritableDB();
				ContentValues cv = objectToContentValues(jsonStr2);
				vals = db.insert(DatabaseHelper.TABLE_HC_PEST_INFO, null,
						cv);
				// LoggerUtilgerUtil.e("commercial_PestDTO added1. values", ""+vals);
				db.close();
			} catch (Exception e) {
				LoggerUtil.e("Exception", e.toString());
			}

			return vals;
		}*/

	// Convert json object to content values
	public static ContentValues objectToContentValues(JSONObject jsObject) {
		ContentValues contentValues = new ContentValues();
		Iterator<String> iter = jsObject.keys();
		while (iter.hasNext()) {
			String columnName = iter.next();
			try {
				if (!columnName.equals(ParameterUtil.Id)) {
					String value = jsObject.get(columnName).toString();
					// LoggerUtilgerUtil.e("Values", value+"");
					contentValues.put(columnName,
							CommonFunction.checkString(value, ""));

				}
			} catch (JSONException e) {
				LoggerUtil.e("Exception", e.toString());
			}
		}
		return contentValues;
	}

	public boolean isAlreadyAdded(String strServiceIdNew, String TableName) {
		SQLiteDatabase db = getReadableDB();
		String selectQuerty = "select * from " + TableName + " where "
				+ ParameterUtil.ServiceID_new + "=" + strServiceIdNew;
		// LoggerUtil.e("selectQuerty isAlreadyAdded",selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		if (cursor != null) {
			if (cursor.getCount() != 0) {
				cursor.close();
				db.close();

				return true;
			}
		}
		cursor.close();
		db.close();
		// cursor.close();
		return false;
	}

	public boolean ischemicalAlreadyAdded(String strServiceIdNew,
			String preventionId, String TableName) {
		SQLiteDatabase db = getReadableDB();
		String selectQuerty = "select * from " + TableName + " where "
				+ ParameterUtil.ServiceID_new + "=" + strServiceIdNew + " AND "
				+ ParameterUtil.PreventationID + "='" + preventionId + "'";
		// LoggerUtil.e("selectQuerty isAlreadyAdded",selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		if (cursor != null) {
			if (cursor.getCount() != 0) {
				cursor.close();
				db.close();
				// cursor.close();
				return true;
			}
		}
		cursor.close();
		db.close();
		// cursor.close();
		return false;
	}

	public ChemicalDto getchemicalAlreadyAdded(String strStatus,
			String strServiceIdNew, String preventionId, String TableName) {
		SQLiteDatabase db = getReadableDB();
		String selectQuerty = "select * from " + TableName + " where "
				+ ParameterUtil.ServiceID_new + "=" + strServiceIdNew + " AND "
				+ ParameterUtil.Status + "='" + strStatus + "'" + " AND "
				+ ParameterUtil.PreventationID + "='" + preventionId + "'";
		//LoggerUtil.e("selectQuerty", selectQuerty + "");
		/* "+ParameterUtil.Status+"='"+strStatus+"'"+" AND */
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONObject resultSet = cursorToJSONOjbect(cursor);
		//LoggerUtil.e("resultSet", resultSet + "");
		cursor.close();
		db.close();
		if (resultSet != null) {
			ChemicalDto dto = new Gson().fromJson(resultSet.toString(),
					ChemicalDto.class);
			return dto;
		}
		cursor.close();
		db.close();
		// cursor.close();
		return null;
	}

	// Delete All table
	public int deleteAllTableData(String tableName) {
		SQLiteDatabase db = getWritableDB();
		int a = db.delete(tableName, null, null);
		// LoggerUtilgerUtil.e("Deleted", a+"");
		db.close();
		return a;
	}

	public static SQLiteDatabase getWritableDB() {
		DatabaseHelper dBhelper = new DatabaseHelper(context,
				DatabaseHelper.DATABASE_NAME, null, version);
		SQLiteDatabase db = dBhelper.getWritableDatabase();
		return db;
	}

	public static SQLiteDatabase getReadableDB() {
		DatabaseHelper dBhelper = new DatabaseHelper(context,
				DatabaseHelper.DATABASE_NAME, null, version);
		SQLiteDatabase db = dBhelper.getReadableDatabase();
		return db;
	}

	// Insert multiple records in table
	public void insertMultipleRecords(JSONArray jsarray, String tableName) {
		SQLiteDatabase db = getWritableDB();

		db.beginTransaction();
		InsertHelper ih = new InsertHelper(db, tableName);
		try {
			for (int i = 0; i < jsarray.length(); i++) {
				ih.prepareForInsert();
				try {
					JSONObject jsObject = jsarray.getJSONObject(i);
					Iterator<String> iter = jsObject.keys();
					while (iter.hasNext()) {
						String columnName = iter.next();
						try {
							String value = jsObject.get(columnName).toString();
							if (value != null && !value.equals("")) {
								ih.bind(ih.getColumnIndex(columnName), value);
							}

						} catch (JSONException e) {
							LoggerUtil.e("Exception", e.toString());
						}
					}
					// LoggerUtil.e(tableName, i+"");
					ih.execute();
				} catch (Exception e) {
					LoggerUtil.e("Exception", e.toString());
				}
			}
			db.setTransactionSuccessful();
		} catch (Exception e) {

		} finally {
			ih.close();
			db.endTransaction();
			db.close();
			// See comment below from Stefan Anca
		}
	}

	public JSONArray getAllServicelist(String status) {
		// TODO Auto-generated method stub
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select * from "
				+ DatabaseHelper.TABLE_GENERAL_INFO + " where "
				+ ParameterUtil.ServiceStatus + "='" + status + "'";
		LoggerUtil.e("select query", "select query from get all service list.."
				+ selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONArray resultSet = cursorToJSONArray(cursor);

		cursor.close();
		db.close();
		return resultSet;

	}

	// Convert cursor to json Array
	public JSONArray cursorToJSONArray(Cursor cursor) {
		JSONArray resultSet = new JSONArray();
		if (cursor.moveToFirst()) {
			while (cursor.isAfterLast() == false) {
				int totalColumn = cursor.getColumnCount();
				JSONObject rowObject = new JSONObject();
				for (int i = 0; i < totalColumn; i++) {
					if (cursor.getColumnName(i) != null) {
						try {
							rowObject.put(cursor.getColumnName(i),
									cursor.getString(i));
						} catch (Exception e) {
							LoggerUtil.e("Exception", e.getMessage());
						}
					}
				}
				resultSet.put(rowObject);
				cursor.moveToNext();
			}
		}
		return resultSet;
	}

	// Get list of all records in jsonArray
	public JSONArray getChemicals(String TABLE_Chemical, String serivce_id_new) {
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select * from " + TABLE_Chemical + " where "
				+ ParameterUtil.ServiceID_new + "=" + serivce_id_new;
		LoggerUtil.e("select query from TABLE_CHEMICALS_WHEN_NO_RECORD",
				selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONArray resultSet = cursorToJSONArray(cursor);
		// Log.e("resultSet", resultSet.toString());
		cursor.close();
		db.close();
		return resultSet;
	}

	// Get list of all records in jsonArray
	public JSONArray getChemicalsWhenNoRecord(String TABLE_Chemical,
			String status) {
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select * from " + TABLE_Chemical + " where "
				+ ParameterUtil.Status + "='" + status + "'";
		// LoggerUtil.e("select query from TABLE_CHEMICALS_WHEN_NO_RECORD",
		// selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONArray resultSet = cursorToJSONArray(cursor);
		// Log.e("resultSet", resultSet.toString());
		cursor.close();
		db.close();
		return resultSet;
	}

	public JSONArray getCrewMembers(String tableCrewMember, String service_idNew) {
		// TODO Auto-generated method stub

		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select * from " + tableCrewMember + " where "
				+ ParameterUtil.ServiceID_new + " = '" + service_idNew + "'";
		// LoggerUtil.e("select query", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONArray resultSet = cursorToJSONArray(cursor);
		// Log.e("resultSet", resultSet.toString());
		cursor.close();
		db.close();
		return resultSet;
	}

	public void updateSelectedCrewStatus(String serviceNewId,
			String crewMemberId, String status) {
		SQLiteDatabase db = getWritableDB();
		db.execSQL("UPDATE " + DatabaseHelper.TABLE_SELECTED_CREW_MEMBER
				+ " SET IsChecked=" + "'" + status + "'" + " WHERE "
				+ ParameterUtil.ServiceID_new + "='" + serviceNewId + "' AND "
				+ ParameterUtil.CrewMember_ID + "='" + crewMemberId + "'");

	}

	// Get list of all records in jsonArray
	public JSONArray getAllSelectedChemicals(String TABLE_Chemical,
			String serviceId_new,String status) {
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = 
				"select * from " + TABLE_Chemical + " where "
				+ ParameterUtil.ServiceID_new + " = " + serviceId_new + " and "
				+ ParameterUtil.Status + " = '" +status+ "' and "
				+ ParameterUtil.ProductCheck + "='true'";
		// LoggerUtil.e("select query", selectQuerty)
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONArray resultSet = cursorToJSONArray(cursor);
		// Log.e("resultSet", resultSet.toString());
		cursor.close();
		db.close();
		return resultSet;
	}

	// Get list of all records in jsonArray
	public ArrayList<String> getAllSelectedPestPreventionId(
			String TABLE_Chemical, String ServiceId_New) {
		ArrayList<String> id = new ArrayList<String>();
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select " + ParameterUtil.PestPrevention_ID
				+ " from " + TABLE_Chemical + " where "
				+ ParameterUtil.ServiceID_new + "=" + ServiceId_New + " and "
				+ ParameterUtil.ProductCheck + "='true'";
		LoggerUtil.e("select query", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			id.add(cursor.getString(cursor
					.getColumnIndex(ParameterUtil.PestPrevention_ID))); // add
																		// the
																		// item
			cursor.moveToNext();
		}
		// ArrayList<String> resultSet=cursorToJSONArray(cursor);
		// Log.e("resultSet", resultSet.toString());
		cursor.close();
		db.close();
		return id;
	}

	// check if records are already present in residential table
	public static JSONObject getAllSelectedChemicalsObjects() {
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select * from "
				+ DatabaseHelper.TABLE_SELECTED_CHEMICALS;
		LoggerUtil.e("select query", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONObject resultSet = cursorToJSONOjbect(cursor);
		// LoggerUtil.e("resultSet getTodaysReport", resultSet+"");
		cursor.close();
		db.close();
		return resultSet;
	}

	// Get list of all records in jsonArray
	public JSONArray getSelectedChemicals(String TABLE_Chemical, String id) {
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select * from " + TABLE_Chemical + " where "
				+ ParameterUtil.PestPrevention_ID + " =" + id;
		LoggerUtil.e("select query", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONArray resultSet = cursorToJSONArray(cursor);
		// Log.e("resultSet", resultSet.toString());
		cursor.close();
		db.close();
		return resultSet;
	}

	public JSONArray updateChemicalTable(String parameter, String value,
			String PreventionId) {
		// TODO Auto-generated method stub
		SQLiteDatabase db = getWritableDB();
		String updateQuerty = "UPDATE "
				+ DatabaseHelper.TABLE_SELECTED_CHEMICALS + " SET " + parameter
				+ " = " + value + "  WHERE " + ParameterUtil.PestPrevention_ID
				+ " = " + PreventionId;
		// Log.e("select query", updateQuerty);
		Cursor cursor = db.rawQuery(updateQuerty, null);
		JSONArray resultSet = cursorToJSONArray(cursor);
		// LoggerUtil.e("resultSet", resultSet.toString());
		cursor.close();
		db.close();
		return resultSet;

	}

	public int deleteSelectedChemicals(String tableChemicals,
			String pestPrevention_ID) {
		// TODO Auto-generated method stub
		/*
		 * SQLiteDatabase db =getWritableDB(); String
		 * deleteQuerty="delete from "
		 * +tableChemicals+" WHERE "+ParameterUtil.PestPrevention_ID
		 * +" = "+pestPrevention_ID; Log.e("select query", deleteQuerty); Cursor
		 * cursor=db.rawQuery(deleteQuerty, null); //JSONArray
		 * resultSet=cursorToJSONArray(cursor);
		 * LoggerUtil.e("resultSet","cursor.."+ cursor.getCount());
		 * cursor.close(); db.close();
		 */

		SQLiteDatabase db = getWritableDB();
		int a = db.delete(tableChemicals, ParameterUtil.PestPrevention_ID
				+ " = " + pestPrevention_ID, null);
		// Log.e("Deleted", a+"");
		db.close();
		return a;
	}

	// Get all records from residential pest table where isCompleted is true
	public JSONArray getCompleteResidential_Commercial_pestService(
			String tableName) {
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select * from " + tableName + " where "
				+ ParameterUtil.isCompleted + "='true'";
		// String
		// selectQuerty="select * from "+DatabaseHelper.TABLE_HR_PEST_INFO;

		// LoggerUtil.e("select query", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONArray resultSet = cursorToJSONArray(cursor);
		// LoggerUtil.e("resultSet", resultSet.toString());
		cursor.close();
		db.close();
		return resultSet;
	}

	// get list of all chemicals where ProductCheck matches
	public JSONArray getAllServiceSelectedChemicalList(String tableName) {
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select * from " + tableName + " where "
				+ ParameterUtil.ProductCheck + "='true'";
		// LoggerUtil.e("select query", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONArray resultSet = cursorToJSONArray(cursor);
		// LoggerUtil.e("resultSet for chemical list....",
		// resultSet.toString());
		cursor.close();
		db.close();
		return resultSet;
	}

	public JSONArray getAllServiceSelectedChemicalListToUpload(String tableName) {
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select * from " + tableName + " where "
				+ ParameterUtil.ProductCheck + "='true'" + " and "
				+ ParameterUtil.isCompleted + "='true'";
		// LoggerUtil.e("select query", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONArray resultSet = cursorToJSONArray(cursor);
		LoggerUtil.e("resultSet for chemical list....", resultSet.toString());
		cursor.close();
		db.close();
		/*
		 * for(int i=0;i<resultSet.length();i++) { try { String
		 * id=resultSet.getJSONObject(i).getString(ParameterUtil.ServiceID_new);
		 * if(id.equals("187415")) { Log.e("id",id+""); } }catch(Exception E) {
		 * 
		 * }
		 * 
		 * 
		 * 
		 * }
		 */

		return resultSet;
	}

	public boolean isEmailAdressAlreadyAdded(String strServiceIdNew,
			String emailAddress) {
		SQLiteDatabase db = getReadableDB();
		String selectQuerty = "select * from "
				+ DatabaseHelper.TABLE_AGREEMENTMAILID + " where "
				+ ParameterUtil.Member_Id + "=" + strServiceIdNew + " AND "
				+ ParameterUtil.Emailid + "='" + emailAddress + "'";
		LoggerUtil.e("selectQuerty isAlreadyAdded", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		if (cursor != null) {
			if (cursor.getCount() != 0) {
				return true;
			}
		}
		db.close();
		cursor.close();
		return false;
	}

	public int getMaxId() {
		int maxId = 0;
		SQLiteDatabase db = getWritableDB();
		Cursor c = db.rawQuery("select max(Id) from "
				+ DatabaseHelper.TABLE_AGREEMENTMAILID, null);
		if (c.getCount() > 0) {
			c.moveToFirst();
			maxId = c.getInt(0);
		}
		c.close();
		db.close();
		return maxId;
	}

	public long agreementMail(JSONObject jsonStr) {
		SQLiteDatabase db = getWritableDB();
		ContentValues cv = objectToContentValues(jsonStr);
		long vals = db.insert(DatabaseHelper.TABLE_AGREEMENTMAILID, null, cv);
		LoggerUtil.e("agreementMail vals", "" + vals);
		db.close();
		return vals;
	}

	public JSONArray getAllSelectedServiceEmaiLID(String serviceNewId) {
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select * from "
				+ DatabaseHelper.TABLE_AGREEMENTMAILID + " where "
				+ ParameterUtil.Member_Id + "='" + serviceNewId + "'";
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONArray resultSet = cursorToJSONArray(cursor);
		LoggerUtil.e("getAllSelectedServiceEmaiLID", resultSet.toString());
		cursor.close();
		db.close();
		return resultSet;
	}

	public int deleteSelectedTableData(String tableName, String columnNameby,
			String value) {
		SQLiteDatabase db = getWritableDB();
		int a = db.delete(tableName, columnNameby + " = ?" ,
				new String[] { value });
		// int a= db.delete(tableName, null , null );
		// LoggerUtil.e("DELETE MAIL TABLE "+tableName,a+"");
		db.close();
		return a;
	}
	
	public int deleteSelectedChemicalData(String tableName, String columnNameby,String columnNameTwo,
			String value,String val2) {
		SQLiteDatabase db = getWritableDB();
		int a = db.delete(tableName, columnNameby + " = ? AND "+ columnNameTwo + " = ?" ,
				new String[] { value, val2 });
		// int a= db.delete(tableName, null , null );
		// LoggerUtil.e("DELETE MAIL TABLE "+tableName,a+"");
		db.close();
		return a;
	}

	public JSONArray getAllSelectedEmaiLID() {
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select * from "
				+ DatabaseHelper.TABLE_AGREEMENTMAILID + " where "
				+ ParameterUtil.isSelected + "='true'";
		;
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONArray resultSet = cursorToJSONArray(cursor);
		LoggerUtil.e("getAllSelectedServiceEmaiLID", resultSet.toString());
		cursor.close();
		db.close();
		return resultSet;
	}

	public JSONArray getSelectedServicelist(String firstName, String lastName,
			String emailAddress, String accountNo, String address, String city,
			String State, String techName, String techId, String fromDate,
			String toDate, String workOrder, String serviceStatus, String branch) {

		// Log.e("date", fromDate+" "+toDate);
		String selectQuerty;
		if (serviceStatus.equals("All")) {
			serviceStatus = "";
			// toDate="";
			// fromDate="";
			selectQuerty = "select * from " + DatabaseHelper.TABLE_GENERAL_INFO
					+ " where " + ParameterUtil.ServiceDate + " between '"
					+ fromDate + "' and '" + toDate + "'";
			// Log.e("date", " selectQuerty..."+selectQuerty);
		} else if (branch.equals("All")) {
			branch = "";
			selectQuerty = "select * from " + DatabaseHelper.TABLE_GENERAL_INFO
					+ " where " +

					ParameterUtil.ServiceStatus + " ='" + serviceStatus
					+ "' AND " + ParameterUtil.ServiceDate + " between '"
					+ fromDate + "' and '" + toDate
					+ "'"
					+ setQueryPart(ParameterUtil.First_Name, firstName)
					+ setQueryPart(ParameterUtil.Last_Name, lastName)
					+ setQueryPart(ParameterUtil.Email_Id, emailAddress)
					+ setQueryPart(ParameterUtil.AccountNo, accountNo)
					+ setQueryPart(ParameterUtil.Address_Line1, address)
					+ setQueryPart(ParameterUtil.City, city)
					+ setQueryPart(ParameterUtil.Order_Number, workOrder)
					+ setQueryPart(ParameterUtil.Service_Tech_Num, techId)
					+
					// setQueryPart(ParameterUtil.ServiceStatus, serviceStatus)+
					setQueryPart(ParameterUtil.FullName, techName)
					+ setQueryPart(ParameterUtil.Service_Tech_Num, techId)
					+ setQueryPart(ParameterUtil.State, State);
		} else {
			selectQuerty = "select * from " + DatabaseHelper.TABLE_GENERAL_INFO
					+ " where " +

					ParameterUtil.ServiceStatus + " ='" + serviceStatus
					+ "' AND (" + ParameterUtil.ServiceDate + " between "
					+ fromDate + " and "
					+ toDate
					+ ")"
					+ setQueryPart(ParameterUtil.First_Name, firstName)
					+ setQueryPart(ParameterUtil.Last_Name, lastName)
					+ setQueryPart(ParameterUtil.Email_Id, emailAddress)
					+ setQueryPart(ParameterUtil.AccountNo, accountNo)
					+ setQueryPart(ParameterUtil.Address_Line1, address)
					+ setQueryPart(ParameterUtil.City, city)
					+ setQueryPart(ParameterUtil.Order_Number, workOrder)
					+ setQueryPart(ParameterUtil.Service_Tech_Num, techId)
					+
					// setQueryPart(ParameterUtil.ServiceStatus, serviceStatus)+
					setQueryPart(ParameterUtil.FullName, techName)
					+ setQueryPart(ParameterUtil.Service_Tech_Num, techId)
					+ setQueryPart(ParameterUtil.State, State)
					+ setQueryPart(ParameterUtil.Branch, branch);
		}

		// TODO Auto-generated method stub
		SQLiteDatabase db = getWritableDB();
		// String
		// selectQuerty="select * from "+DatabaseHelper.TABLE_SERVICE_LIST+" where "+ParameterUtil.First_Name+"="+firstName;
		// Log.e("workOrder  in database", workOrder);
		//LoggerUtil.e("select query", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONArray resultSet = cursorToJSONArray(cursor);
		//LoggerUtil.e("resultSet", resultSet.toString());
		cursor.close();
		db.close();
		return resultSet;
	}

	public String setQueryPart(String columnName, String value) {
		String queryPart = "";
		if (!CommonFunction.checkString(value, "").equals("")) {
			queryPart = " AND " + columnName + " LIKE '%" + value + "%' ";
			// queryPart=" AND "+columnName+"='"+value+"' ";
		} else {
			queryPart = "";
		}

		return queryPart;
	}

	public int updateCrewTable(String selected, String serviceidNew) {
		// TODO Auto-generated method stub
		int i = 0;
		// Log.e("Update crew ", serviceidNew+""+selected);
		SQLiteDatabase db = getWritableDB();
		ContentValues values = new ContentValues();
		values.put("IsChecked", selected);

		i = db.update(DatabaseHelper.TABLE_CREW_MEMBER, values,
				"ServiceID_new=" + serviceidNew, null);
		
			// Log.e("updateCrewTable",i+"");
		db.close();

		return i;
	}
	
	public static int updateEmailTables(String selected, String emailid,
			String strserviceIdNew) {
			int i = 0;
		// Log.e("Update crew ", serviceidNew+""+selected);
		SQLiteDatabase db = getWritableDB();
		ContentValues values = new ContentValues();
		values.put(ParameterUtil.isSelected, selected);

		i = db.update(DatabaseHelper.TABLE_AGREEMENTMAILID, values,
				 ( ParameterUtil.Emailid + "=" + emailid + " AND " +ParameterUtil.Member_Id + "=" + strserviceIdNew ), null);
	
		
			 Log.e("update email",i+"");
		db.close();

		return i;
		/*// TODO Auto-generated method stub
		int i = 0;
		// Log.e("Update email status ", emailid+""+selected);
		SQLiteDatabase db = getWritableDB();
		String UpdateQuery = "UPDATE table_agreementmailid SET isSelected='"
				+ selected + "' WHERE EmailId='" + emailid + "' AND "
				+ ParameterUtil.Member_Id + "=" + strserviceIdNew;
		Cursor cursor = db.rawQuery(UpdateQuery, null);
		// i =
		// db.update(DatabaseHelper.TABLE_AGREEMENTMAILID,values,"EmailId="+emailid,null);
		// Log.e("Update email status ", UpdateQuery);
		db.close();

		return i;*/
	}

	public JSONArray getAllSelectedCrewMember(String strserviceIdNew) {
		SQLiteDatabase db = getWritableDB();
		// String
		// selectQuerty="select * from "+DatabaseHelper.TABLE_CREW_MEMBER+" where IsChecked='true'";
		String selectQuerty = "select CrewMember_ID from "
				+ DatabaseHelper.TABLE_CREW_MEMBER + " where "
				+ ParameterUtil.ServiceID_new + "=" + strserviceIdNew
				+ " AND IsChecked='true'";
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONArray resultSet = cursorToJSONArray(cursor);
		LoggerUtil.e("getAllSelectedCrewMember", resultSet.toString());
		cursor.close();
		db.close();
		return resultSet;
	}

	public JSONArray getAllSelectedLogRow(String strserviceIdNew) {
		SQLiteDatabase db = getWritableDB();
		// String
		// selectQuerty="select * from "+DatabaseHelper.TABLE_CREW_MEMBER+" where IsChecked='true'";
		String selectQuerty = "select * from " + DatabaseHelper.TABLE_LOG
				+ " where " + ParameterUtil.SID + "=" + strserviceIdNew;
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONArray resultSet = cursorToJSONArray(cursor);
		LoggerUtil.e("getAllSelectedLog", resultSet.toString());
		cursor.close();
		db.close();
		return resultSet;
	}

	public String getEmpId(String empName) {
		String emp = null;
		SQLiteDatabase db = getWritableDB();
		// String
		// selectQuerty="select * from "+DatabaseHelper.TABLE_CREW_MEMBER+" where IsChecked='true'";
		String selectQuerty = "select " + ParameterUtil.EmployeeId + " from "
				+ DatabaseHelper.TABLE_EMPLOYEE_INFO + " where "
				+ ParameterUtil.EmpName + "='" + empName + "'";
		LoggerUtil.e("selectQuerty ", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		LoggerUtil.e("cursor ", "" + cursor);
		if (cursor.moveToFirst()) {
			emp = cursor.getString(0); // Here you can get the id of respective
										// person (i.e. the name which I have
										// entered )
		}

		LoggerUtil.e("emp id", emp);
		cursor.close();
		db.close();
		return emp;
	}

	public static long addSelectedCrewMember(Selected_crewDTO selected_crewDTO) {
		long vals = 0;
		try {
			Gson gson = new Gson();
			JSONObject jsonStr2 = new JSONObject(gson.toJson(selected_crewDTO));
			// LoggerUtil.e("residential added1..", jsonStr.toString());
			SQLiteDatabase db = getWritableDB();
			ContentValues cv = objectToContentValues(jsonStr2);
			vals = db.insert(DatabaseHelper.TABLE_SELECTED_CREW_MEMBER, null,
					cv);
			// LoggerUtilgerUtil.e("residential added1. values", ""+vals);

		} catch (Exception e) {
			LoggerUtil.e("Exception", e.toString());
		}

		return vals;
	}

	public JSONArray getSelectedCrewMember() {
		// TODO Auto-generated method stub
		SQLiteDatabase db = getWritableDB();
		// String
		// selectQuerty="select * from "+DatabaseHelper.TABLE_CREW_MEMBER+" where IsChecked='true'";
		String selectQuerty = "select * from "
				+ DatabaseHelper.TABLE_SELECTED_CREW_MEMBER;
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONArray resultSet = cursorToJSONArray(cursor);
		LoggerUtil.e("getAllSelectedCrewMember", resultSet.toString());
		cursor.close();
		db.close();
		return resultSet;
	}

	public int seleteSelectedCrewTable(String tableSelectedCrewMember,
			String strServiceIdNew) {
		SQLiteDatabase db = getWritableDB();
		int a = db.delete(tableSelectedCrewMember, ParameterUtil.ServiceID_new
				+ " = " + strServiceIdNew, null);
		// Log.e("Deleted", a+"");
		db.close();
		return a;
	}

	// Get All INCOMPLETE service list
	public JSONArray getAllInCompleteService() {
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select * from "
				+ DatabaseHelper.TABLE_GENERAL_INFO + " where "
				+ ParameterUtil.ServiceStatus + " ='InComplete'";
		// LoggerUtil.e("select query", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONArray resultSet = cursorToJSONArray(cursor);
		// LoggerUtil.e("resultSet", resultSet.toString());
		cursor.close();
		db.close();
		return resultSet;
	}

	public JSONArray getServiceDetailDTOList() {
		SQLiteDatabase db = getWritableDB();
		String selectQuerty = "select * from " + DatabaseHelper.TABLE_LOG;
		// LoggerUtil.e("select query", selectQuerty);
		Cursor cursor = db.rawQuery(selectQuerty, null);
		JSONArray resultSet = cursorToJSONArray(cursor);
		// LoggerUtil.e("resultSet", resultSet.toString());
		cursor.close();
		db.close();
		return resultSet;
	}

	// Dinesh..

	public void updateServiceUploadedToServer(String tableName,
			String serviceNewId, String uploadStatus) {
		try {
			SQLiteDatabase db = getWritableDB();
			db.execSQL("UPDATE " + tableName + " SET "
					+ ParameterUtil.isCompleted + "=" + uploadStatus
					+ " WHERE " + ParameterUtil.ServiceID_new + "='"
					+ serviceNewId + "'");
			db.close();
		} catch (Exception e) {
			Log.e("Exception", e.toString());
		}

	}

	public int deleteOldServiceData(String currentDate) {
		SQLiteDatabase db = getWritableDB();
		int a = db.delete(DatabaseHelper.TABLE_GENERAL_INFO,
				ParameterUtil.ServiceDate + " != ? AND ("
						+ ParameterUtil.isCompleted + " = ? OR "
						+ ParameterUtil.isCompleted + " = ?)", new String[] {
						"X", "", "0" });
		LoggerUtil.e("Delete OldData " + DatabaseHelper.TABLE_GENERAL_INFO, a
				+ "");
		db.close();
		return a;
	}

	

	

	

	
	

	

}
