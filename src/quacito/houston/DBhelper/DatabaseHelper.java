package quacito.houston.DBhelper;


import quacito.houston.CommonUtilities.ParameterUtil;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper{

	// Database Name
	String Status="Status";
	public static final String DATABASE_NAME = "ABCHoustonDB";
	public static final String TABLE_LOG = "table_log";
	public static final String TABLE_LOGIN = "table_login";
	public static final String TABLE_ADDRESS_INFO = "SR_tbl_AddressInfo";
	public static final String TABLE_GENERAL_INFO = "SR_tbl_GeneralInfo";
	public static final String TABLE_PREVENTION_INFO = "SR_tbl_Prevention";
	public static final String TABLE_PREVENTION_MASTER = "SR_tbl_PreventionMaster";
	public static final String TABLE_HR_PEST_INFO = "SR_tbl_ResidentialPest";
	public static final String TABLE_HC_PEST_INFO = "SR_tbl_CommercialPest";
	public static final String TABLE_OR_PEST_INFO = "SR_tbl_OrlandoResidentialPest";
	public static final String TABLE_EMPLOYEE_INFO = "tbl_Employee";
	public static final String TABLE_CHEMICALS = "tbl_Chemical";
	public static final String TABLE_CHEMICALS_WHEN_NO_RECORD = "tbl_chemical_WhenNoRecord";
	public static final String TABLE_SELECTED_CHEMICALS = "tbl_Selected_Chemical";
	public static final String TABLE_AGREEMENTMAILID="table_agreementmailid";
	public static final String TABLE_CREW_MEMBER="table_crewmember";
	public static final String TABLE_SELECTED_CREW_MEMBER="table_selected_crewmember";
	
	public static final String TABLE_GET_ALL_ROUTE ="table_get_all_route";
	public static final String TABLE_GET_ALL_ROUTE_CREW ="table_get_all_route_crew";
	public static final String TABLE_GET_ASSIGN_LEAD_FOR_DAY ="table_get_assign_lead_for_day";
	public static final String TABLE_GET_ASSIGN_CREW_FOR_DAY ="table_get_assign_crew_for_day";
	
	public DatabaseHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		try 
		{   
			db.setLockingEnabled(false);
		}
		finally
		{
			db.setLockingEnabled(true);
		}

		//address info detail table
		String create_table_AddresInfo = "create table IF NOT EXISTS "
				+ TABLE_ADDRESS_INFO + " (" 
				+ ParameterUtil.Row_Id+ " INTEGER PRIMARY KEY,"
				+ ParameterUtil.ServiceID_new+ " INTEGER ,"
				+ ParameterUtil.Address_Line1 + " text," 
				+ ParameterUtil.Address_Line2 + " text, "
				+ ParameterUtil.City+ " text, " 
				+ ParameterUtil.State + " text, "
				+ ParameterUtil.ZipCode+ " text, "
				+ ParameterUtil.Poc_FName + " text," 
				+ ParameterUtil.Poc_LName + " text, "
				+ ParameterUtil.BAddress_Line1+ " text, " 
				+ ParameterUtil.BAddress_Line2 + " text, "
				+ ParameterUtil.BCity+ " text, "
				+ ParameterUtil.BState + " text," 
				+ ParameterUtil.BZipCode + " text, "
				+ ParameterUtil.ServiceLatitude+ " text, " 
				+ ParameterUtil.ServiceLongitude + " text, "
				+ ParameterUtil.Create_By+ " text, "
				+ ParameterUtil.Update_By + " text," 
				+ ParameterUtil.Create_Date + " text, "
				+ ParameterUtil.Update_Date+ " text) " ;

		db.execSQL(create_table_AddresInfo);

		//login table
		String create_table_Login = "create table IF NOT EXISTS "
				+ TABLE_LOGIN + " (" 
				+ ParameterUtil.Id+ " INTEGER PRIMARY KEY,"
				+ ParameterUtil.EmployeeId+ " INTEGER ,"
				+ ParameterUtil.Assigned_WO+ " INTEGER ,"
				+ ParameterUtil.Username + " text," 
				+ ParameterUtil.Password + " text, "
				+ ParameterUtil.FirstName+ " text, " 
				+ ParameterUtil.MiddleName + " text, "
				+ ParameterUtil.LastName+ " text, "
				+ ParameterUtil.RoleId + "INTEGER," 
				+ ParameterUtil.RoleName + " text, "
				+ ParameterUtil.Emailid+ " text, " 
				+ ParameterUtil.PestPackId + "INTEGER, "
				+ ParameterUtil.AccountNo+ " text, "
				+ ParameterUtil.DepartmentId + " INTEGER," 
				+ ParameterUtil.HomePhone + " text, "
				+ ParameterUtil.CellPhone+ " text, " 
				+ ParameterUtil.Address1 + " text, "
				+ ParameterUtil.Address2+ " text, "
				+ ParameterUtil.City + " text," 
				+ ParameterUtil.State + " text, "
				+ ParameterUtil.ZipCode+ " text, " 
				+ ParameterUtil.Status + " text, "
				+ ParameterUtil.SupervisorId + " INTEGER, "
				+ ParameterUtil.Created_By + " text, "
				+ ParameterUtil.Update_Date+ " text, " 
				+ ParameterUtil.LicenceNo+ " text, " 
				+ ParameterUtil.Updated_By + " text)";

		db.execSQL(create_table_Login);

		//address info detail table
		String create_table_GeneralInfo = "create table IF NOT EXISTS "
				+ TABLE_GENERAL_INFO + " (" 
				+ ParameterUtil.ServiceID_new+ " INTEGER,"
				+ ParameterUtil.newEmail + " text,"
				+ ParameterUtil.ServiceNameType + " text," 
				+ ParameterUtil.ResetComment + " text," 
				+ ParameterUtil.First_Name + " text," 
				+ ParameterUtil.Last_Name + " text, "
				+ ParameterUtil.FullName+ " text, "
				+ ParameterUtil.Email_Id+ " text, " 
				+ ParameterUtil.isCompleted+ " text, "
				+ ParameterUtil.SecondaryEmail + " text, "
				+ ParameterUtil.AccountNo+ " text, "
				+ ParameterUtil.Home_Phone + " text," 
				+ ParameterUtil.Cell_Phone + " text, "
				+ ParameterUtil.Salesperson_ID+ " text, " 
				+ ParameterUtil.ServiceDate + " text, "
				+ ParameterUtil.ServiceStatus+ " text, "
				+ ParameterUtil.Notes+ " text," 
				+ ParameterUtil.ServiceCode + " text, "
				+ ParameterUtil.department+ " text, " 
				+ ParameterUtil.BusinessName + " text, "
				+ ParameterUtil.Branch+ " text, "
				+ ParameterUtil.TaxCode + " text," 
				+ ParameterUtil.EstimateTime + " text, "
				+ ParameterUtil.TimeIn+ " text, " 
				+ ParameterUtil.Arrival_Duration + " text," 
				+ ParameterUtil.ProblemDescription + " text, "
				+ ParameterUtil.PSPCustomer+ " text, " 
				+ ParameterUtil.Create_By + " text, "
				+ ParameterUtil.Update_By+ " text, "
				+ ParameterUtil.Create_Date + " text," 
				+ ParameterUtil.Update_Date + " text, "
				+ ParameterUtil.Ref_Source+ " text, " 
				+ ParameterUtil.Order_Number + " text, "
				+ ParameterUtil.Route_Name+ " text, "
				+ ParameterUtil.Route_Number+ " text," 
				+ ParameterUtil.KeyMap + " text, "
				+ ParameterUtil.Service_Tech_Num+ " text, " 
				+ ParameterUtil.Service_Status_Reason + " text, "
				+ ParameterUtil.CompanyName+ " text, "
				+ ParameterUtil.ServiceDescription + " text," 
				+ ParameterUtil.OtherInstruction + " text, "
				+ ParameterUtil.specialInstruction + " text," 
				+ ParameterUtil.Total + " text, "
				+ ParameterUtil.LocationBalance+ " text, " 
				+ ParameterUtil.TimeInDateTime + " text, "
				+ ParameterUtil.Branch_ID+ " text, "
				+ ParameterUtil.Inv_value + " text," 
				+ ParameterUtil.Prod_value + " text, "
				+ ParameterUtil.Prv_Bal+ " text, " 
				+ ParameterUtil.Tax_value + " text, "
				+ ParameterUtil.Service_Attribute + " text, "
				+ ParameterUtil.Office_Note_ByRep+ " text, "
				+ ParameterUtil.Note_Status+ " text," 
				+ ParameterUtil.Note_Closed_Text + " text, "
				+ ParameterUtil.Note_Closed_Date+ " text, " 
				+ ParameterUtil.Payment_Collection_Status + " text, "
				+ ParameterUtil.Collection_Text+ " text, "
				+ ParameterUtil.Collection_Date + " text," 
				+ ParameterUtil.Flag + " text, "
				+ ParameterUtil.IsElectronicSignatureAvailable + " text," 
				+ ParameterUtil.OpenWONotificationFlag + " text, "
				+ ParameterUtil.Address_Line1 + " text," 
				+ ParameterUtil.Address_Line2 + " text, "
				+ ParameterUtil.City+ " text, " 
				+ ParameterUtil.State + " text, "
				+ ParameterUtil.ZipCode+ " text, "
				+ ParameterUtil.Poc_FName + " text," 
				+ ParameterUtil.Poc_LName + " text, "
				+ ParameterUtil.BAddress_Line1+ " text, " 
				+ ParameterUtil.BAddress_Line2 + " text, "
				+ ParameterUtil.BCity+ " text, "
				+ ParameterUtil.BState + " text," 
				+ ParameterUtil.BZipCode + " text, "
				+ ParameterUtil.ServiceLatitude+ " text, " 
				+ ParameterUtil.ServiceLongitude + " text, "
				+ ParameterUtil.BeforeImage + " text," 
				+ ParameterUtil.AfterImage+ " text, "
				+ ParameterUtil.AudioFile+ " text, " 
				+ ParameterUtil.CheckNo+ " text, " 
				+ ParameterUtil.LicenseNo + " text, "
				+ ParameterUtil.ExpirationDate + " text," 
				+ ParameterUtil.CheckBackImage+ " text, "
				+ ParameterUtil.CheckFrontImage+ " text, " 
				+ ParameterUtil.Inspection_Medium + " text, "
				+ ParameterUtil.Mobile_Info+ " text, " 
				+ ParameterUtil.App_Info + " text, "
				+ ParameterUtil.ServiceClass + " text, "
				+ ParameterUtil.ServiceTime+ " text) " ;

		db.execSQL(create_table_GeneralInfo);

		//for tb prevention 
		String create_table_prevention = "create table IF NOT EXISTS "
				+ TABLE_PREVENTION_INFO + " (" 
				+ ParameterUtil.PestPrevention_ID+ " INTEGER PRIMARY KEY,"
				+ ParameterUtil.PreventationID+ " INTEGER ,"
				+ ParameterUtil.ServiceID_new + " INTEGER," 
				+ ParameterUtil.ProductCheck + " text, "
				+ ParameterUtil.PercentPest+ " text, " 
				+ ParameterUtil.Amount + " text, "
				+ ParameterUtil.Target+ " text, "
				+ ParameterUtil.Unit + " text," 
				+ ParameterUtil.SAA + " text, "
				+ ParameterUtil.Method+ " text, " 
				+ ParameterUtil.OtherText + " text, "
				+ ParameterUtil.Create_By+ " text, "
				+ ParameterUtil.Update_By + " text," 
				+ ParameterUtil.Create_Date + " text, "
				+ ParameterUtil.Update_Date+ " text) " ;

		db.execSQL(create_table_prevention);

		//for SR_tbl_PreventionMaster
		String create_table_Chemical = "create table IF NOT EXISTS "
				+ TABLE_CHEMICALS + " (" 
				+ ParameterUtil.PreventationID+ " INTEGER ,"
				+ ParameterUtil.PestPrevention_ID+ " INTEGER ,"
				+ ParameterUtil.ServiceID_new + " text," 
				+ ParameterUtil.Amount + " text, "
				+ ParameterUtil.OtherText+ " text, " 
				+ ParameterUtil.ProductCheck+ " text, " 
				+ ParameterUtil.Product+ " text, " 
				+ ParameterUtil.Status+ " text, " 
				+ ParameterUtil.Target + " text, "
				+ ParameterUtil.Unit+ " text, " 
				+ ParameterUtil.Create_By+ " text, " 
				+ ParameterUtil.Create_Date+ " text, "
				+ ParameterUtil.isCompleted+ " text, " 
				+ ParameterUtil.PercentPest + " text) " ;

		db.execSQL(create_table_Chemical);


		//for SR_tbl_PreventionMaster
		String create_table_Chemical_WhenNoRecord = "create table IF NOT EXISTS "
				+ TABLE_CHEMICALS_WHEN_NO_RECORD + " (" 
				+ ParameterUtil.PreventationID+ " INTEGER ,"
				+ ParameterUtil.PestPrevention_ID+ " INTEGER ,"
				+ ParameterUtil.ServiceID_new + " text," 
				+ ParameterUtil.Amount + " text, "
				+ ParameterUtil.OtherText+ " text, " 
				+ ParameterUtil.ProductCheck+ " text, " 
				+ ParameterUtil.Product+ " text, " 
				+ ParameterUtil.Status+ " text, " 
				+ ParameterUtil.Target + " text, "
				+ ParameterUtil.Unit+ " text, " 
				+ ParameterUtil.Method+ " text, " 
				+ ParameterUtil.SAA+ " text, " 
				+ ParameterUtil.PercentPest + " text) " ;

		db.execSQL(create_table_Chemical_WhenNoRecord);


		//for SR_tbl_PreventionMaster
		String create_table_Selected_Chemical = "create table IF NOT EXISTS "
				+ TABLE_SELECTED_CHEMICALS + " (" 
				+ ParameterUtil.PreventationID+ " INTEGER ,"
				+ ParameterUtil.PestPrevention_ID+ " INTEGER ,"
				+ ParameterUtil.ServiceID_new + " text," 
				+ ParameterUtil.Amount + " text, "
				+ ParameterUtil.OtherText+ " text, " 
				+ ParameterUtil.ProductCheck+ " text, " 
				+ ParameterUtil.Product+ " text, " 
				+ ParameterUtil.Status+ " text, " 
				+ ParameterUtil.Target + " text, "
				+ ParameterUtil.Unit+ " text, " 
				+ ParameterUtil.Create_By+ " text, " 
				+ ParameterUtil.Create_Date+ " text, "
				+ ParameterUtil.isCompleted+ " text, "
				+ ParameterUtil.PercentPest + " text) " ;

		db.execSQL(create_table_Selected_Chemical);

		//for SR_tbl_PreventionMaster
		String create_table_PreventionMaster = "create table IF NOT EXISTS "
				+ TABLE_PREVENTION_MASTER + " (" 
				+ ParameterUtil.PreventationID+ " INTEGER PRIMARY KEY,"
				+ ParameterUtil.Product+ " INTEGER ,"
				+ ParameterUtil.PercentPest + " text," 
				+ ParameterUtil.Status + " text, "
				+ ParameterUtil.EPA_RegNo+ " text, " 
				+ ParameterUtil.Unit + " text) " ;

		db.execSQL(create_table_PreventionMaster);

		//for SR_tbl_PreventionMaster
		String create_table_Log = "create table IF NOT EXISTS "
				+ TABLE_LOG + " (" 
				+ ParameterUtil.Id+ " INTEGER PRIMARY KEY,"
				+ ParameterUtil.SID+ " text,"
				+ ParameterUtil.GeneralSR+ " text,"
				+ ParameterUtil.Residetial_Insp+ " text,"
				+ ParameterUtil.Commercial_Insp+ " text,"
				+ ParameterUtil.Chemical+ " text,"
				+ ParameterUtil.Crew_Member+ " text,"
				+ ParameterUtil.AgreeMentMailID + " text)";
		db.execSQL(create_table_Log);


		//FOR SR_tbl_ResidentialPest
		String create_table_ResidentialPest= "create table IF NOT EXISTS "
				+ TABLE_HR_PEST_INFO + " (" 
				+ ParameterUtil.Id+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ ParameterUtil.ResidentialId+ " text, "
				+ ParameterUtil.ServiceID_new + " text," 
				+ ParameterUtil.ServiceFor + " text," 
				+ ParameterUtil.AreasInspected + " text, "
				+ ParameterUtil.InsectActivity+ " text, " 
				+ ParameterUtil.Ants + " text, "
				+ ParameterUtil.OtherPest+ " text, "
				+ ParameterUtil.OutsideOnly + " text," 
				+ ParameterUtil.isCompleted+ " text, "
				+ ParameterUtil.PestActivity + " text, "
				+ ParameterUtil.Fence+ " text, " 
				+ ParameterUtil.BackYard + " text, "
				+ ParameterUtil.FrontYard+ " text, "
				+ ParameterUtil.ExteriorPerimeter+ " text, " 
				+ ParameterUtil.Roofline + " text, "
				+ ParameterUtil.Garage+ " text, " 
				+ ParameterUtil.Attic + " text, "
				+ ParameterUtil.InteriorHouse+ " text, "
				+ ParameterUtil.PestControl + " text," 
				+ ParameterUtil.PestControlServices + " text, "
				+ ParameterUtil.PlusTermite + " text," 
				+ ParameterUtil.FencePest + " text, "
				+ ParameterUtil.FenceActivity+ " text, " 
				+ ParameterUtil.NumberPest + " text, "
				+ ParameterUtil.NumberText+ " text, "
				+ ParameterUtil.NumberActivity + " text," 
				+ ParameterUtil.NumberActivitytext + " text, "
				+ ParameterUtil.WoodPest+ " text, " 
				+ ParameterUtil.WoodOption + " text, "
				+ ParameterUtil.WoodActivity+ " text, "
				+ ParameterUtil.ExteriorPest+ " text," 
				+ ParameterUtil.ExteriorActivity + " text, "
				+ ParameterUtil.AtticPest+ " text, " 
				+ ParameterUtil.AtticActivity + " text, "
				+ ParameterUtil.InteriorPest+ " text, "
				+ ParameterUtil.InteriorActivity + " text," 
				+ ParameterUtil.PlusRodent + " text, "
				+ ParameterUtil.RoofRodent + " text," 
				+ ParameterUtil.RoofNeeds + " text, "
				+ ParameterUtil.RoofVentNeeds+ " text, " 
				+ ParameterUtil.RoofVentRodent + " text, "
				+ ParameterUtil.Flashing+ " text, "
				+ ParameterUtil.FlashingNeeds + " text," 
				+ ParameterUtil.Fascia + " text, "
				+ ParameterUtil.FasciaNeeds+ " text, " 
				+ ParameterUtil.Siding + " text, "
				+ ParameterUtil.SidingNeeds+ " text, "
				+ ParameterUtil.Breezway+ " text," 
				+ ParameterUtil.BreezwayNeeds + " text, "
				+ ParameterUtil.GarageDoor+ " text, " 
				+ ParameterUtil.GarageDoorNeeds + " text, "
				+ ParameterUtil.DryerVent+ " text, "
				+ ParameterUtil.DryerVentNeeds + " text," 
				+ ParameterUtil.NumberLive + " text, "
				+ ParameterUtil.NumberLiveNeeds + " text," 
				+ ParameterUtil.NumberSnap + " text, "
				+ ParameterUtil.NumberSnapNeeds + " text," 
				+ ParameterUtil.PlusMosquito + " text, "
				+ ParameterUtil.PlusMosquitoDetail+ " text, " 
				+ ParameterUtil.DurationTime + " text, "
				+ ParameterUtil.SystemRuns+ " text, "
				+ ParameterUtil.TreeBranches + " text," 
				+ ParameterUtil.Firewood + " text, "
				+ ParameterUtil.DebrisCrawl+ " text, " 
				+ ParameterUtil.ExcessivePlant + " text, "
				+ ParameterUtil.Soil+ " text, "
				+ ParameterUtil.WoodSoil+ " text," 
				+ ParameterUtil.DebrisRoof + " text, "
				+ ParameterUtil.StandingWater+ " text, " 
				+ ParameterUtil.MoistureProblem + " text, "
				+ ParameterUtil.Openings+ " text, "
				+ ParameterUtil.ExcessiveGaps + " text," 
				+ ParameterUtil.LeakyPlumbing + " text, "
				+ ParameterUtil.GarbageCans + " text," 
				+ ParameterUtil.GroceryBags + " text, "
				+ ParameterUtil.MoistureDamaged+ " text, " 
				+ ParameterUtil.PetFood + " text, "
				+ ParameterUtil.RecycledItems+ " text, "
				+ ParameterUtil.ExcessiveStorage + " text," 
				+ ParameterUtil.TechnicianComments + " text, "
				+ ParameterUtil.TimeIn+ " text, " 
				+ ParameterUtil.Timeout + " text, "
				+ ParameterUtil.TimeInDateTime+ " text, "
				+ ParameterUtil.TimeOutDateTime+ " text," 
				+ ParameterUtil.Technician + " text, "
				+ ParameterUtil.EmpNo+ " text, " 
				+ ParameterUtil.Customer + " text, "
				+ ParameterUtil.PaymentType+ " text, "
				+ ParameterUtil.Date + " text," 
				+ ParameterUtil.Amount + " text, "
				+ ParameterUtil.CheckNo + " text," 
				+ ParameterUtil.LicenseNo + " text, "
				+ ParameterUtil.Signature+ " text, " 
				+ ParameterUtil.ServiceSignature + " text, "
				+ ParameterUtil.Create_By+ " text, "
				+ ParameterUtil.Create_Date + " text," 
				+ ParameterUtil.Update_By + " text, "
				+ ParameterUtil.Update_Date+ " text, " 
				+ ParameterUtil.AgreementPath + " text) " ;

		db.execSQL(create_table_ResidentialPest);

		//FOR SR_tbl_CommercialPest
				String create_table_CommercialPest= "create table IF NOT EXISTS "
						+ TABLE_HC_PEST_INFO + " (" 
						+ ParameterUtil.Id+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
						+ ParameterUtil.CommercialId+ " text, "
						+ ParameterUtil.isCompleted+ " text, "
						+ ParameterUtil.ServiceID_new + " text," 
						+ ParameterUtil.ServiceFor + " text," 
						+ ParameterUtil.AreasInspectedInterior + " text, "
						+ ParameterUtil.AreasInspectedExterior+ " text, " 
						+ ParameterUtil.InsectActivity + " text, "
						+ ParameterUtil.Ants+ " text, "
						+ ParameterUtil.OtherPest + " text," 
						+ ParameterUtil.PestActivity+ " text, "
						+ ParameterUtil.Interior + " text, "
						+ ParameterUtil.Outsidef_Perimeter+ " text, " 
						+ ParameterUtil.Dumpster + " text, "
						+ ParameterUtil.Dining_Room+ " text, "
						+ ParameterUtil.Common_Areas+ " text, " 
						+ ParameterUtil.Kitchen + " text, "
						+ ParameterUtil.Dry_Storage+ " text, " 
						+ ParameterUtil.Dish_Washing + " text, "
						+ ParameterUtil.Roof_Tops+ " text, "
						+ ParameterUtil.Wait_Stations + " text," 
						+ ParameterUtil.Drop_Ceiling + " text, "
						+ ParameterUtil.Planters + " text," 
						+ ParameterUtil.Warehouse_Storage + " text, "
						+ ParameterUtil.PestControl+ " text, " 
						+ ParameterUtil.PestControlServices + " text, "
						+ ParameterUtil.PlusTermite+ " text, "
						+ ParameterUtil.SignOfTermiteMS + " text," 
						+ ParameterUtil.SignOfTermiteActivityMS + " text, "
						+ ParameterUtil.SignOfTermiteText+ " text, " 
						+ ParameterUtil.InstallMS + " text, "
						+ ParameterUtil.InstallMSText+ " text, "
						+ ParameterUtil.SignOfTermitePS+ " text," 
						+ ParameterUtil.SignOfTermiteActivityPS + " text, "
						+ ParameterUtil.SignOfTermiteIS+ " text, " 
						+ ParameterUtil.SignOfTermiteActivityIS + " text, "
						+ ParameterUtil.ReplacedMS+ " text, "
						+ ParameterUtil.ReplacedTextMS + " text," 
						+ ParameterUtil.ReplacedActivityTextMS + " text, "
						+ ParameterUtil.TreatedPS + " text," 
						+ ParameterUtil.TreatedActivityPS + " text, "
						+ ParameterUtil.TreatedSP+ " text, " 
						+ ParameterUtil.TreatedActivitySP + " text, "
						+ ParameterUtil.TreatedCC+ " text, "
						+ ParameterUtil.TreatedActivityCC + " text," 
						+ ParameterUtil.PlusRodent + " text, "
						+ ParameterUtil.InspectedRodentStation+ " text, " 
						+ ParameterUtil.txtInspectedRodentStation + " text, "
						+ ParameterUtil.CleanedRodentStation+ " text, "
						+ ParameterUtil.IsCleanedRodentStation+ " text," 
						+ ParameterUtil.InstallRodentBaitStation + " text, "
						+ ParameterUtil.IsInstallRodentBaitStation+ " text, " 
						+ ParameterUtil.txtInstallRodentBaitStation + " text, "
						+ ParameterUtil.ReplacedBait+ " text, "
						+ ParameterUtil.IsReplacedBait + " text," 
						+ ParameterUtil.SNoReplacedBait + " text, "
						+ ParameterUtil.ReplacedBaitStation + " text," 
						+ ParameterUtil.IsReplacedBaitStation + " text, "
						+ ParameterUtil.txtReplacedBaitStation + " text," 
						+ ParameterUtil.SNoReplacedBaitStation + " text, "
						+ ParameterUtil.InspectedRodentTraps+ " text, " 
						+ ParameterUtil.IsInspectedRodentTraps + " text, "
						+ ParameterUtil.CleanedRodentTrap+ " text, "
						+ ParameterUtil.IsCleanedRodentTrap + " text," 
						+ ParameterUtil.InstallRodentTrap + " text, "
						+ ParameterUtil.IsInstallRodentTrap+ " text, " 
						+ ParameterUtil.txtInstallRodentTrap + " text, "
						+ ParameterUtil.ReplacedRodentTrap+ " text, "
						+ ParameterUtil.IsReplacedRodentTrap+ " text," 
						+ ParameterUtil.txtReplacedRodentTrap + " text, "
						+ ParameterUtil.SNoReplacedRodentTrap+ " text, " 
						+ ParameterUtil.SealedEntryPoint + " text, "
						+ ParameterUtil.IsSealedEntryPoint+ " text, "
						+ ParameterUtil.InspectedLiveCages + " text," 
						+ ParameterUtil.txtInspectedLiveCages + " text, "
						+ ParameterUtil.InspectedSnapTrap+ " text, " 
						+ ParameterUtil.txtInspectedSnapTrap + " text, "
						+ ParameterUtil.RemovedSnapTraps+ " text, "
						+ ParameterUtil.txtRemovedSnapTraps + " text," 
						+ ParameterUtil.RemovedLiveCages + " text, "
						+ ParameterUtil.txtRemovedLiveCages+ " text, " 
						+ ParameterUtil.RemovedRodentBaitStation + " text, "
						+ ParameterUtil.txtRemovedRodentBaitStation+ " text, "
						+ ParameterUtil.RemovedRodentTrap+ " text," 
						+ ParameterUtil.txtRemovedRodentTrap + " text, "
						+ ParameterUtil.SetSnapTrap+ " text, " 
						+ ParameterUtil.txtSetSnapTrap + " text, "
						+ ParameterUtil.LstSetSnapTrap+ " text, "
						+ ParameterUtil.SetLiveCages + " text," 
						+ ParameterUtil.txtSetLiveCages + " text, "
						+ ParameterUtil.LstSetLiveCages + " text," 
						+ ParameterUtil.PlusMosquitoDetail + " text, "
						+ ParameterUtil.DurationTime+ " text, " 
						+ ParameterUtil.SystemRuns + " text, "
						+ ParameterUtil.Recommendations+ " text, "
						+ ParameterUtil.TechnicianComments + " text," 
						+ ParameterUtil.TimeIn + " text, "
						+ ParameterUtil.Timeout+ " text, " 
						+ ParameterUtil.TimeInDateTime + " text, "
						+ ParameterUtil.TimeOutDateTime+ " text, " 
						+ ParameterUtil.Technician+ " text, " 
						+ ParameterUtil.EmpNo + " text, "
						+ ParameterUtil.Customer+ " text, "
						+ ParameterUtil.Date + " text," 
						+ ParameterUtil.PaymentType + " text, "
						+ ParameterUtil.Amount + " text," 
						+ ParameterUtil.CheckNo + " text, "
						+ ParameterUtil.LicenseNo+ " text, " 
						+ ParameterUtil.Signature + " text, "
						+ ParameterUtil.ServiceSignature+ " text, "
						+ ParameterUtil.Create_By + " text," 
						+ ParameterUtil.Update_By + " text, "
						//+ ParameterUtil.Create_date + " text," 
						//+ ParameterUtil.Update_Date + " text, "
						+ ParameterUtil.AgreementPath + " text) " ;

				db.execSQL(create_table_CommercialPest);
				
				//FOR SR_tbl_CommercialPest
				String create_table_OrlandoResidencialPest= "create table IF NOT EXISTS "
						+ TABLE_OR_PEST_INFO + " (" 
						+ ParameterUtil.Id+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
						+ ParameterUtil.FloridaResidentialServiceId+ " text, "
						+ ParameterUtil.isCompleted+ " text, "
						+ ParameterUtil.ServiceID_new + " text," 
						+ ParameterUtil.ServiceFor + " text," 
						+ ParameterUtil.AreasInspected + " text, "
						+ ParameterUtil.InsectActivity + " text, "
						+ ParameterUtil.Ants+ " text, "
						+ ParameterUtil.OtherPest + " text," 
						+ ParameterUtil.OutsideOnly + " text,"
						+ ParameterUtil.PestActivity+ " text, "
						+ ParameterUtil.Fence + " text, "
						+ ParameterUtil.BackYard+ " text, " 
						+ ParameterUtil.FrontYard + " text, "
						+ ParameterUtil.ExteriorPerimeter+ " text, "
						+ ParameterUtil.Roofline+ " text, " 
						+ ParameterUtil.Garage + " text, "
						+ ParameterUtil.Attic+ " text, " 
						+ ParameterUtil.InteriorHouse + " text, "
						+ ParameterUtil.Basement+ " text, "
						+ ParameterUtil.PestControl + " text," 
						+ ParameterUtil.PestControlServices + " text, "
						+ ParameterUtil.PlusTermite + " text," 
						+ ParameterUtil.FencePest + " text, "
						+ ParameterUtil.FenceActivity+ " text, " 
						+ ParameterUtil.BuilderConstruction + " text, "
						+ ParameterUtil.BuilderConstructionActivity+ " text, "
						+ ParameterUtil.CrawlSpaceDoor + " text," 
						+ ParameterUtil.CrawlSpaceDoorActivity + " text, "
						+ ParameterUtil.NumberPest+ " text, " 
						+ ParameterUtil.NumberText + " text, "
						+ ParameterUtil.NumberActivity+ " text, "
						+ ParameterUtil.NumberActivitytext+ " text," 
						+ ParameterUtil.WoodPest + " text, "
						+ ParameterUtil.WoodOption+ " text, " 
						+ ParameterUtil.WoodActivity + " text, "
						+ ParameterUtil.ExteriorPest+ " text, "
						+ ParameterUtil.ExteriorActivity + " text," 
						+ ParameterUtil.AtticPest + " text, "
						+ ParameterUtil.AtticActivity + " text," 
						+ ParameterUtil.InteriorPest + " text, "
						+ ParameterUtil.InteriorActivity+ " text, " 
						+ ParameterUtil.TreatmentOfSchool + " text, "
						+ ParameterUtil.MonitoringDevices+ " text, "
						+ ParameterUtil.NumMonitoringDevices + " text," 
						+ ParameterUtil.BasementTermite + " text, "
						+ ParameterUtil.NumBasementTermite+ " text, " 
						+ ParameterUtil.AdditionMonitoringDevice + " text, "
						+ ParameterUtil.NumAdditionMonitoringDevice+ " text, "
						+ ParameterUtil.MonitoringDeviceRemoved+ " text," 
						+ ParameterUtil.NumMonitoringDeviceRemoved + " text, "
						+ ParameterUtil.PlusRodent+ " text, " 
						+ ParameterUtil.RoofRodent + " text, "
						+ ParameterUtil.RoofVentNeeds+ " text, "
						+ ParameterUtil.RoofVentRodent + " text," 
						+ ParameterUtil.RoofNeeds + " text, "
						+ ParameterUtil.Flashing + " text," 
						+ ParameterUtil.FlashingNeeds + " text, "
						+ ParameterUtil.Fascia + " text," 
						+ ParameterUtil.FasciaNeeds + " text, "
						+ ParameterUtil.Siding+ " text, " 
						+ ParameterUtil.SidingNeeds + " text, "
						+ ParameterUtil.Breezway+ " text, "
						+ ParameterUtil.BreezwayNeeds + " text," 
						+ ParameterUtil.GarageDoor + " text, "
						+ ParameterUtil.GarageDoorNeeds+ " text, " 
						+ ParameterUtil.DryerVent + " text, "
						+ ParameterUtil.DryerVentNeeds+ " text, "
						+ ParameterUtil.NumberLive+ " text," 
						+ ParameterUtil.NumberLiveNeeds + " text, "
						+ ParameterUtil.NumberSnap+ " text, " 
						+ ParameterUtil.NumberSnapNeeds + " text, "
						+ ParameterUtil.PlusMosquito+ " text, "
						+ ParameterUtil.PlusMosquitoDetail + " text," 
						+ ParameterUtil.DurationTime + " text, "
						+ ParameterUtil.SystemRuns+ " text, " 
						+ ParameterUtil.TreeBranches + " text, "
						+ ParameterUtil.Firewood+ " text, "
						+ ParameterUtil.DebrisCrawl + " text," 
						+ ParameterUtil.ExcessivePlant + " text, "
						+ ParameterUtil.Soil+ " text, " 
						+ ParameterUtil.WoodSoil + " text, "
						+ ParameterUtil.DebrisRoof+ " text, "
						+ ParameterUtil.StandingWater+ " text," 
						+ ParameterUtil.MoistureProblem + " text, "
						+ ParameterUtil.Openings+ " text, " 
						+ ParameterUtil.ExcessiveGaps + " text, "
						+ ParameterUtil.LeakyPlumbing+ " text, "
						+ ParameterUtil.GarbageCans + " text," 
						+ ParameterUtil.MoistureDamaged + " text, "
						+ ParameterUtil.GroceryBags + " text," 
						+ ParameterUtil.PetFood + " text, "
						+ ParameterUtil.RecycledItems+ " text, " 
						+ ParameterUtil.ExcessiveStorage + " text, "
						+ ParameterUtil.TechnicianComments+ " text, "
						+ ParameterUtil.TimeIn + " text," 
						
						+ ParameterUtil.Timeout+ " text, " 
						+ ParameterUtil.TimeInDateTime + " text, "
						+ ParameterUtil.TimeOutDateTime+ " text, " 
						+ ParameterUtil.Technician+ " text, " 
						+ ParameterUtil.EmpNo + " text, "
						+ ParameterUtil.Customer+ " text, "
						+ ParameterUtil.Date + " text," 
						+ ParameterUtil.PaymentType + " text, "
						+ ParameterUtil.Amount + " text," 
						+ ParameterUtil.CheckNo + " text, "
						+ ParameterUtil.LicenseNo+ " text, " 
						+ ParameterUtil.Signature + " text, "
						+ ParameterUtil.ServiceSignature+ " text, "
						+ ParameterUtil.Create_By + " text," 
						+ ParameterUtil.Update_By + " text, "
						+ ParameterUtil.Create_date + " text," 
						+ ParameterUtil.Update_Date + " text, "
						+ ParameterUtil.AgreementPath + " text) " ;

				db.execSQL(create_table_OrlandoResidencialPest);

		String create_table_agreementmailid = "create table IF NOT EXISTS "
				+ TABLE_AGREEMENTMAILID + "(" 
				+ ParameterUtil.Id+ " INTEGER PRIMARY KEY,"
				+ ParameterUtil.AgreementId + " text,"
				+ ParameterUtil.Member_Id + " text, "
				+ ParameterUtil.EmailId + " text, "
				+ ParameterUtil.MailType+ " text, " 
				+ ParameterUtil.IsCustomerEmail+ " text, "
				+ ParameterUtil.Create_By+ " text, "
				+ ParameterUtil.Create_date+ " text, "
				+ ParameterUtil.isSelected+ " text, "
				+ ParameterUtil.Subject + " text)";


		db.execSQL(create_table_agreementmailid);

		//address info detail table
		String create_table_EmployeInfo = "create table IF NOT EXISTS "
				+ TABLE_EMPLOYEE_INFO + " (" 
				+ ParameterUtil.EmployeeId+ " INTEGER PRIMARY KEY,"
				+ ParameterUtil.Username_employee+ " text ,"
				+ ParameterUtil.Password + " text," 
				+ ParameterUtil.FirstName + " text, "
				+ ParameterUtil.MiddleName+ " text, " 
				+ ParameterUtil.LastName + " text, "
				+ ParameterUtil.EmpName+ " text, "
				+ ParameterUtil.RoleId+ " text, "
				+ ParameterUtil.RoleName + " text," 
				+ ParameterUtil.Emailid + " text, "
				+ ParameterUtil.PestPackId+ " text, " 
				+ ParameterUtil.DepartmentId + " text, "
				+ ParameterUtil.AccountNo+ " text, "
				+ ParameterUtil.HomePhone + " text," 
				+ ParameterUtil.CellPhone + " text, "
				+ ParameterUtil.Address1+ " text, " 
				+ ParameterUtil.Address2 + " text, "
				+ ParameterUtil.City+ " text, "
				+ ParameterUtil.State + " text," 
				+ ParameterUtil.ZipCode + " text, "
				+ ParameterUtil.Status+ " text,"
				+ ParameterUtil.SupervisorId + " INTEGER," 
				+ ParameterUtil.Create_Date + " text, "
				+ ParameterUtil.Update_Date+ " text, " 
				+ ParameterUtil.Created_By + " text, "
				+ ParameterUtil.Updated_By+ " text, "
				+ ParameterUtil.InactiveDate + " text," 
				+ ParameterUtil.LicenceNo + " text) " ;

		db.execSQL(create_table_EmployeInfo);

		//for table crew member
		String create_table_CrewMember = "create table IF NOT EXISTS "
				+ TABLE_CREW_MEMBER + " (" 
				+ "ServiceID_new"+ " text, " 
				+ "Route_ID" + " text, "
				+ "CrewMember_ID" + " text, "
				+ "Tech_Name" + " text," 
				+ "IsChecked" + " text," 
				+ "Status" + " text) " ;

		db.execSQL(create_table_CrewMember);

		//for table crew member
		String create_table_Selected_CrewMember = "create table IF NOT EXISTS "
				+ TABLE_SELECTED_CREW_MEMBER + " (" 
				+ "ServiceID_new"+ " text, " 
				+ "CrewMembers" + " text, "
				+ "Lead_ID" + " text) ";


		db.execSQL(create_table_Selected_CrewMember);
		
		//for table get all route
				String create_table_get_All_Route = "create table IF NOT EXISTS "
						+ TABLE_GET_ALL_ROUTE + " (" 
						+ ParameterUtil.Class_ID + " text, "
						+ ParameterUtil.Created_By+ " text, "
						+ ParameterUtil.Created_Date + " text, "
						+ ParameterUtil.Lead_ID + " text, "
						+ ParameterUtil.RouteMaster_ID + " text, "
						+ ParameterUtil.Route_Name + " text, "
						+ ParameterUtil.Updated_By + " text, "
						+ ParameterUtil.Updated_Date + " text, "
						+ ParameterUtil.Status + " text) ";

				db.execSQL(create_table_get_All_Route);
				
				//for table get all route
				String create_table_get_All_Route_Crew = "create table IF NOT EXISTS "
						+ TABLE_GET_ALL_ROUTE_CREW + " (" 
						+ ParameterUtil.Route_Crew_ID + " text, "
						+ ParameterUtil.Created_By+ " text, "
						+ ParameterUtil.Created_Date + " text, "
						+ ParameterUtil.Route_ID + " text, "
						+ ParameterUtil.Status + " text, "
						+ ParameterUtil.Technician_ID + " text, "
						+ ParameterUtil.Updated_By + " text, "
						+ ParameterUtil.Updated_Date + " text) ";

				db.execSQL(create_table_get_All_Route_Crew);
				
				//for table get all route
				String create_table_get_Assigned_Lead_Forday = "create table IF NOT EXISTS "
						+ TABLE_GET_ASSIGN_LEAD_FOR_DAY + " (" 
						+ ParameterUtil.AssignForDayLead_ID+ " text, "
						+ ParameterUtil.Assign_Date+ " text, "
						+ ParameterUtil.Created_By + " text, "
						+ ParameterUtil.Created_Date+ " text, "
						+ ParameterUtil.Emp_ID + " text, "
						+ ParameterUtil.Route_ID + " text, "
						+ ParameterUtil.Status + " text, "
						+ ParameterUtil.Updated_By + " text, "
						+ ParameterUtil.Updated_Date + " text) ";

				db.execSQL(create_table_get_Assigned_Lead_Forday);
				
				//for table get all route
				String create_table_get_Assigned_Crew_Forday = "create table IF NOT EXISTS "
						+ TABLE_GET_ASSIGN_CREW_FOR_DAY + " (" 
						+ ParameterUtil.AssignForDayCrewMember_ID+ " text, "
						+ ParameterUtil.AssignForDayLeadID+ " text, "
						+ ParameterUtil.Created_By + " text, "
						+ ParameterUtil.Created_Date+ " text, "
						+ ParameterUtil.CrewMember_ID + " text, "
						+ ParameterUtil.Status + " text, "
						+ ParameterUtil.Updated_By + " text, "
						+ ParameterUtil.Updated_Date + " text) ";

				db.execSQL(create_table_get_Assigned_Crew_Forday);
				

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
				
		 db.execSQL("DROP TABLE IF EXISTS "+TABLE_LOG);
		 db.execSQL("DROP TABLE IF EXISTS "+TABLE_LOGIN);
		 db.execSQL("DROP TABLE IF EXISTS "+TABLE_ADDRESS_INFO);
		 db.execSQL("DROP TABLE IF EXISTS "+TABLE_GENERAL_INFO);
		 db.execSQL("DROP TABLE IF EXISTS "+TABLE_HR_PEST_INFO);
		 db.execSQL("DROP TABLE IF EXISTS "+TABLE_HC_PEST_INFO);
		 db.execSQL("DROP TABLE IF EXISTS "+TABLE_PREVENTION_INFO);
		 db.execSQL("DROP TABLE IF EXISTS "+TABLE_PREVENTION_MASTER);
		 db.execSQL("DROP TABLE IF EXISTS "+TABLE_EMPLOYEE_INFO);
		 db.execSQL("DROP TABLE IF EXISTS "+TABLE_CHEMICALS);
		 db.execSQL("DROP TABLE IF EXISTS "+TABLE_CHEMICALS_WHEN_NO_RECORD);
		 db.execSQL("DROP TABLE IF EXISTS "+TABLE_SELECTED_CHEMICALS);
		 db.execSQL("DROP TABLE IF EXISTS "+TABLE_AGREEMENTMAILID);
		 db.execSQL("DROP TABLE IF EXISTS "+TABLE_CREW_MEMBER);
		 db.execSQL("DROP TABLE IF EXISTS "+TABLE_SELECTED_CREW_MEMBER);
		 
		 db.execSQL("DROP TABLE IF EXISTS "+TABLE_GET_ALL_ROUTE);
		 db.execSQL("DROP TABLE IF EXISTS "+TABLE_GET_ALL_ROUTE_CREW);
		 db.execSQL("DROP TABLE IF EXISTS "+TABLE_GET_ASSIGN_LEAD_FOR_DAY);
		 db.execSQL("DROP TABLE IF EXISTS "+TABLE_GET_ASSIGN_CREW_FOR_DAY);
		 
		 onCreate(db);
	}

}
