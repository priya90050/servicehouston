package quacito.houston.Flow;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import quacito.houston.CommonUtilities.CommonFunction;
import quacito.houston.CommonUtilities.LoggerUtil;
import quacito.houston.CommonUtilities.ParameterUtil;
import quacito.houston.Flow.servicehouston.R;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
public class AudioRecordingActivity extends Activity 
{
	private MediaRecorder myRecorder;
	//private MediaPlayer myPlayer;
	private String outputFile = null;
	private Button startBtn;
	private Button stopBtn;
	private ImageView playBtn;
	private ImageView stopPlayBtn;
	private TextView text;
	Button btnDiscard,btnSave;


	// Animation
	AnimationDrawable Anim;
	private static final String AUDIO_RECORDER_FILE_EXT_3GP = ".3gp";
	private static final String AUDIO_RECORDER_FILE_EXT_MP4 = ".mp4";
	//	private static final String AUDIO_RECORDER_FOLDER = "AudioRecorder";
	protected static final int MEDIA_RECORDER_INFO_MAX_DURATION_REACHED = 0;
	ImageView micImage;
	String AudioFileName="";
	String serviceNewId;
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.audio_record);
		Thread.setDefaultUncaughtExceptionHandler(new TopExceptionHandler(this));
		serviceNewId=getIntent().getExtras().getString(ParameterUtil.ServiceID_new);
		initialize();

	}
	public void initialize()
	{
		text = (TextView) findViewById(R.id.text1);
		micImage=(ImageView)findViewById(R.id.micImage);
		Anim = new AnimationDrawable();
		btnSave=(Button)findViewById(R.id.btnSave);
		btnDiscard=(Button)findViewById(R.id.btnDiscard);
		startBtn = (Button)findViewById(R.id.start);
		stopBtn = (Button)findViewById(R.id.stop);
		playBtn = (ImageView)findViewById(R.id.play);
		stopPlayBtn = (ImageView)findViewById(R.id.stopPlay);
		btnSave.setOnClickListener(new OnButtonClick());
		btnDiscard.setOnClickListener(new OnButtonClick());
		startBtn.setOnClickListener(new OnButtonClick());
		stopBtn.setOnClickListener(new OnButtonClick());
		playBtn.setOnClickListener(new OnButtonClick());
		stopPlayBtn.setOnClickListener(new OnButtonClick());
	}
	class OnButtonClick implements OnClickListener
	{
		@Override
		public void onClick(final View v)
		{
			switch (v.getId()) {
			case R.id.btnSave:
				stop(v);
				stopAnimation();
				saveAudioFile();
				if(!AudioFileName.equals(""))
				{
					Intent intent = new Intent();
					intent.putExtra(ParameterUtil.AudioFile,AudioFileName);
					setResult(RESULT_OK, intent);
					finish();
				}
				else
				{
					Intent intent = new Intent();
					intent.putExtra(ParameterUtil.AudioFile,AudioFileName);
					setResult(RESULT_CANCELED, intent);
					finish();
				}
				break;
			case R.id.btnDiscard:

				// TODO Auto-generated method stub
				stop(v);
				stopAnimation();
				Intent intent = new Intent();
				intent.putExtra(ParameterUtil.AudioFile,AudioFileName);
				setResult(RESULT_CANCELED, intent);
				finish();
				//startActivity(getIntent());

				break;
			case R.id.start:

				// TODO Auto-generated method stub
				outputFile =getFilename();
				myRecorder = new MediaRecorder();
				myRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
				myRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
				myRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
				myRecorder.setMaxDuration(60000);
				myRecorder.setOutputFile(outputFile);
				//myRecorder.setOnErrorListener(errorListener);
				myRecorder.setOnInfoListener(infoListener);
				
				startAnimation();
				start(v);
				
				break;
			case R.id.stop:

				// TODO Auto-generated method stub
				playBtn.setVisibility(v.VISIBLE);
				stop(v);
				stopAnimation();

				break;
			case R.id.play:

				// TODO Auto-generated method stub
				stopPlayBtn.setVisibility(v.VISIBLE);
				//play(v);	

				break;
			case R.id.stopPlay:
				//stopPlay(v);
				break;
			default:
				break;
			}	
		}
	}
	private MediaRecorder.OnInfoListener infoListener = new MediaRecorder.OnInfoListener() {
	    @Override
	    public void onInfo(MediaRecorder mr, int what, int extra) {
	      //AppLog.logString("Warning: " + what + ", " + extra);
	      if(what == 800)
	      {
	    	mr.stop();
			mr.release();
			stopAnimation();
			saveAudioFile();
			if(!AudioFileName.equals(""))
			{
				Intent intent = new Intent();
				intent.putExtra(ParameterUtil.AudioFile,AudioFileName);
				setResult(RESULT_OK, intent);
				finish();
			}
			else
			{
				Intent intent = new Intent();
				intent.putExtra(ParameterUtil.AudioFile,AudioFileName);
				setResult(RESULT_CANCELED, intent);
				finish();
			}
	    	  	Intent i= getIntent();
				setResult(RESULT_OK, i);
				finish();
	      }
	    	  
	    }
	  };
	public void saveAudioFile()
	{
		//String sourcePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/TongueTwister/tt_temp.3gp";
		if(!CommonFunction.checkString(outputFile, "").equals(""))
		{
			File source = new File(outputFile);
			String destinationPath =getFilenameToSave();
			if(destinationPath!=null)
			{
				File destination = new File(destinationPath);
				try 
				{
					FileUtils.copyFile(source, destination);
					source.delete();
				} 
				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}
	public void startAnimation()
	{
		BitmapDrawable frame1 = (BitmapDrawable) getResources().getDrawable(
				R.drawable.recording_icon);
		BitmapDrawable frame2 = (BitmapDrawable) getResources().getDrawable(
				R.drawable.startedrecording_icon);
		Anim.addFrame(frame1, 200);
		Anim.addFrame(frame2, 200);
		Anim.setOneShot(false);
		micImage.setImageDrawable(Anim);
		LoggerUtil.e("", "start");
		try {

			final Handler handler = new Handler();
			handler.postDelayed(new Runnable()
			{
				public void run() {
					Anim.start();
				}
			}, 5000);

		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void stopAnimation()
	{
		LoggerUtil.e("", "stop");
		Anim.stop();
		micImage.setImageResource(R.drawable.recording_icon);
	}
	private String getFilename()
	{
		String filepath = Environment.getExternalStorageDirectory().getPath();
		File file = new File(filepath,CommonFunction.AUDIO_RECORDER_FOLDER);
		if(!file.exists())
		{
			file.mkdirs();
		}
		//AudioFileName = serviceNewId + AUDIO_RECORDER_FILE_EXT_MP4;
		outputFile = System.currentTimeMillis() + AUDIO_RECORDER_FILE_EXT_MP4;
		outputFile = file.getAbsolutePath() + "/" + outputFile;

		return (outputFile);
	}
	private String getFilenameToSave()
	{
		AudioFileName="";
		String filepath = Environment.getExternalStorageDirectory().getPath();
		File file = new File(filepath,CommonFunction.AUDIO_RECORDER_FOLDER);
		if(!file.exists())
		{
			file.mkdirs();
		}
		AudioFileName = serviceNewId + AUDIO_RECORDER_FILE_EXT_MP4;
		//outputFile = System.currentTimeMillis() + AUDIO_RECORDER_FILE_EXT_MP4;
		String  FilePath = file.getAbsolutePath() + "/" + AudioFileName;

		return (FilePath);
	}

	public void start(View view)
	{
		try {
			myRecorder.prepare();
			myRecorder.start();
		} catch (IllegalStateException e) {
			// start:it is called before prepare()
			// prepare: it is called after start() or before setOutputFormat() 
			e.printStackTrace();
		} catch (IOException e) {
			// prepare() fails
			e.printStackTrace();
		}

		//text.setText("Recording Point: Recording");
		startBtn.setEnabled(false);
		stopBtn.setEnabled(true);

		Toast.makeText(getApplicationContext(), "Start recording...", 
				Toast.LENGTH_SHORT).show();
	}
	public void stop(View view)
	{
		if(myRecorder!=null)
		{
			try {
				myRecorder.stop();
				myRecorder.release();
				myRecorder  = null;
				stopBtn.setEnabled(false);
				playBtn.setEnabled(true);
				startBtn.setEnabled(true);
				//text.setText("Recording Point: Stop recording");
				Toast.makeText(getApplicationContext(), "Stop recording...",
						Toast.LENGTH_SHORT).show();
			} catch (IllegalStateException e) {
				//  it is called before start()
				e.printStackTrace();
			} catch (RuntimeException e) 
			{
				// no valid audio/video data has been received
				e.printStackTrace();
			}
		}
	}
}
