package quacito.houston.Flow;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import quacito.houston.CommonUtilities.CommonFunction;
import quacito.houston.CommonUtilities.DecimalDigitsInputFilter;
import quacito.houston.CommonUtilities.LoggerUtil;
import quacito.houston.CommonUtilities.ParameterUtil;
import quacito.houston.DBhelper.DatabaseHelper;
import quacito.houston.DBhelper.HoustonFlowFunctions;

import quacito.houston.Flow.servicehouston.R;
import quacito.houston.adapter.GalleryImageAdapter;
import quacito.houston.horizontallistview.HorizontalListView;
import quacito.houston.model.ChemicalDto;
import quacito.houston.model.ChemicalsComponentDTO;
import quacito.houston.model.GeneralInfoDTO;
import quacito.houston.model.HResidential_pest_DTO;
import quacito.houston.model.ImgDto;
import quacito.houston.model.LogDTO;

import quacito.houston.model.UserDTO;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class HR_PestControlServiceReport extends Activity
{ 
	int checkChange = 0;
	LogDTO logDTO;
	long mLastClickTime = 0;
	int imageCountFront=1,imageCountBack=1;
	String CheckFrontImageName,CheckBackImageName;
	public static final int REQUEST_Camera_ACTIVITY_CODE_Front=101;
	public static final int REQUEST_Camera_ACTIVITY_CODE_Back=102;
	public static final int REQUEST_Gallery_ACTIVITY_CODE_Front=103;
	public static final int REQUEST_Gallery_ACTIVITY_CODE_Back=104;
	public static Bitmap bitmapFront,bitmapBack;
	GalleryImageAdapter adapterFront,adapterBack;
	ArrayList<ImgDto> listFrontImg = new ArrayList<ImgDto>();
	ArrayList<String> tempFrontImagList = new ArrayList<String>();
	ArrayList<String> tempStoreFrontImageName = new ArrayList<String>();
	ArrayList<ImgDto> listBackImg = new ArrayList<ImgDto>();
	ArrayList<String> tempBackImagList = new ArrayList<String>();
	ArrayList<String> tempStoreBackImageName = new ArrayList<String>();
	ArrayList<ChemicalsComponentDTO> chemicalsComponentList=new ArrayList<ChemicalsComponentDTO>();
	HorizontalListView imageFrontListView,imageBackListView;

	//public static Bitmap bitmapFront,bitmapBack,outputBack,outputFront;
	/*public static final int REQUEST_Gallery_ACTIVITY_CODE_FRONT = 149;
	public static final int REQUEST_Camera_ACTIVITY_CODE_FRONT =101;
	public static final int REQUEST_Gallery_ACTIVITY_CODE_BACK = 150;
	public static final int REQUEST_Camera_ACTIVITY_CODE_BACK =102;*/
	ArrayList<CheckBox> checkList=new ArrayList<CheckBox>();
	LinearLayout LinearlayoutCHECK_IMAGE,mainLayout,trExpirationDateValidation;
	Uri imageUri;
	//Bitmap bitmapFromG;
	CheckBox chemicalcheckBox;
	ArrayList<ChemicalDto> listChemicals;
	ArrayList<ChemicalDto> listSelectedChemicals=new ArrayList<ChemicalDto>();
	ArrayList<ChemicalDto> listPreviousSelectedChemicals=new ArrayList<ChemicalDto>();
	ArrayList<String> listRemoveSelectedChemicals=new ArrayList<String>();
	ArrayList<String> listPreviousPestId=new ArrayList<String>();
	Button btnSave,btnTermiteDisclosure,btnBack,btnCancel,btnCHECK_FRONT_IMAGE,btnCHECK_BACK_IMAGE;
	Dialog dialogTermiteClosure,dialogFrontImage,dialogBackImage;
	HResidential_pest_DTO residential_pest_DTO;
	GeneralInfoDTO generalInfoDTO;
	boolean isValidated=true;
	HoustonFlowFunctions houstonFlowFunctions;
	ArrayList<String> validationList=new ArrayList<String>();
	UserDTO userDTO;

	//boolean isCheckedServiceFor=false;
	ArrayList<CheckBox> serviceForCheckboxList =new ArrayList<CheckBox>();
	ArrayList<CheckBox> AreasInspectedCheckboxList =new ArrayList<CheckBox>();
	ArrayList<CheckBox> InspectedTreatedForCheckboxList =new ArrayList<CheckBox>();
	ArrayList<CheckBox> PestActivityZoneCheckboxList =new ArrayList<CheckBox>();
	ArrayList<CheckBox> pestControlServicesCheckboxList =new ArrayList<CheckBox>();
	ArrayList<CheckBox> mosquitoCheckboxList =new ArrayList<CheckBox>();
	ArrayList<CheckBox> OutsidepestControlServicesCheckboxList =new ArrayList<CheckBox>();
	ArrayList<CheckBox> InsidepestControlServicesCheckboxList =new ArrayList<CheckBox>();

	TableRow  trcheckno,tramount,trDrivingLicence,trExpirationDate;

	CheckBox 
	//for service for
	checkBoxSignaturePestControl,checkBoxPlusFleas,checkBoxPlusTermites,checkBoxPlusMosquitoMistingSystem,checkBoxPlusfireAnts,
	checkBoxPlusRodents,checkBoxPlusSignatureControl,checkBoxTermitetreatment,checkBoxRodentExclusion,
	checkBoxBedBudTreatment,checkBoxMoaquitoSupression,

	//for ares inspected
	checkBoxExteriorPerimeterHouseAreasInspected,checkBoxFrontyardAreasInspected
	,checkBoxBackYardAreasInspected,checkBoxExteriorPerimeterGarage,checkBoxOutBuildings,checkBoxFenceLines
	,checkBoxLivingRoomsAreasInspected,checkBoxInteriorOfHouseAreasInspected,checkBoxExteriorHouseAreasInspected,checkboxFenceLinesAreasInspected,checkBoxAtticAreasInspected,

	//for  inspected and treated for
	checkBoxRoaches,checkBoxSpiders,
	checkBoxScorpions,checkBoxSilverFish,checkBoxPantryPests,checkBoxPharaohAnts,checkBoxFireAnts,
	checkBoxCarepenterAnts,checkBoxMillipedes,checkBoxEarWigs,checkBoxPillBugs,
	checkboxCrickets,checkboxWasps,checkboxSubTermities,checkboxDryWoodTermities,
	checkboxRoofRats,checkboxMice,checkboxRacoons,checkBoxOppsumss,checkBoxSkunks,
	checkBoxSqirrel,checkBoxTicks,checkBoxBedBugs,checkBoxTwanyCrazyAnts,checkBoxAnts,
	checkBoxOtherPests,

	// for outside service performed
	checkBoxOutsideOnlySerivcePerformed,

	// for pest activity by zone
	checkBoxFrontyardByZone,checkBoxBackYardByZone,checkBoxExteriorPerimeterHouseByZone,
	checkBoxRoofLine,checkBoxGarageBYZone,checkBoxAtticByZone,checkBoxInteriorOfHouseByZone
	,checkBoxShedByZone,checkBoxOtherByZone,

	//for pest control serivces
	checkBoxExteriorPestControl,checkBoxInteriorPestControl,checkBoxTreatedFrontPerimeter,
	checkBoxTreatedbackPerimeter,checkBoxTreatedInspectedAttic,checkBoxTreatedInspectedKitchen
	,checkBoxTreatedWhepholes,checkBoxTreatedInspectedLaundryRooms,checkBoxTreatedCracksCreves,
	checkBoxTreatedInpectedlivingRooms,checkBoxTreatedYardHaouborageBreedingSites,
	checkBoxTreatedInspectedOtherLivingAreas,checkBoxTreatedFireAntsInYards,
	checkBoxInspectedReplacedZoneMonitors,checkBoxTreatedInspectedDooeWaysWindows,
	checkBoxTreatedInspectedBedroomsBathrooms,

	//for termite services
	checkBoxFenceInspected,checkBoxNumberofMonitoringDevicesInspectedTermiteServices,
	checkBoxMonitoringDevicesTermiteServices,checkBoxTermiteMonitoringDevicereplaced,
	checkBoxExteriorPerimeterofhouseTermiteServices,checkBoxAttictermiteServices,
	checkBoxInteriorOfHousetermiteServices,

	//for rodent services
	checkBoxRoofPitches,checkBoxRoofVents,
	checkBoxSoffitVents,checkBoxWeepHoles,
	checkBoxSidingofHouse,checkBoxBreezewayGarage,
	checkBoxGarageDoor,checkBoxAcDryerVentsPhoneline,checkBoxNumberofLiveAnimalTrapsinstalled,
	checkBoxNumberofSnaptrapsplaced,

	//for mosquito services
	checkBoxToppedOfftank,checkBoxLeadsAtTank,checkBoxTubingLeaking,checkBoxInspectedNozzles,
	checkBoxRanSystem,checkBoxCheckedRunDurationTime,checkBoxNozzlesLeaking,checkBoxCheckedTimeSystemRuns,

	//for recommendations
	checkBoxBranchHouse,checkBoxFirewood,checkBoxDebrisCrawl,checkBoxExcessiveplant,checkBoxSoilAbove,
	checkBoxWoodSoil,checkBoxDebrisRoof,checkBoxStandingwater,checkBoxMoistureProblem,
	checkBoxOpeningspumbing,checkBoxLeackPumbing,checkBoxExcessiveGaps,checkBoxMoisrtureDamage,
	checkBoxgarbageCans,checkBoxPetFood,checkBoxGroceryBags,checkBoxRecycledItems,
	checkBoxExcessiveStorage;

	RadioGroup radioGroupInvoice
	,radioGroupFenceInspected,radioGroupMonitoringDevices,radioGroupTermiteMonitoringDevices,
	radioGroupExteriorPerimeterOfHouse,radioGroupAttic,radioGroupInteriorHouse,

	radioGroupRoofPitches,radioGroupRoofVents,radioGroupSoffitvents,
	radioGroupWeepHoles,radioGroupSidingHouse,radioGroupBreezyWay,
	radioGroupGarageDoor,radioGroupACDRYER;

	RadioButton 
	//for invoice
	radioCash,radioCheck,radioCreditcard,radioBillLater,radioPreBill,radioNoCharge,
	radioDrivingLicence,
	//for termite
	radioYesFenceInspected,
	radioYesMonitoringDevicesTermiteServices,radioYesTermiteMonitoringDevicereplaced,
	radioYesExteriorPerimeterofhouseTermiteServices,radioYesAttictermiteServices,
	radioYesInteriorOfHousetermiteServices,
	radioNoFenceInspected,
	radioNoMonitoringDevicesTermiteServices,radioNoTermiteMonitoringDevicereplaced,
	radioNoExteriorPerimeterofhouseTermiteServices,radioNoAttictermiteServices,
	radioNoInteriorOfHousetermiteServices,

	//for rodent services
	radioYesRoofPitches,radioYesRoofVents,
	radioYesSoffitVents,radioYesWeepHoles,
	radioYesSidingofHouse,radioYesBreezewayGarage,
	radioYesGarageDoor,radioYesAcDryerVentsPhoneline,

	radioNoRoofPitches,radioNoRoofVents,
	radioNoSoffitVents,radioNoWeepHoles,
	radioNoSidingofHouse,radioNoBreezewayGarage,
	radioNoGarageDoor,radioNoAcDryerVentsPhoneline;

	EditText objectFocus,edtOtherText,edttxtInspectedandTreatedFor1,edttxtInspectedandTreatedFor2,
	edttxtNumberofMonitoringDevicesInspected,

	// for pest activity by zone
	edttxtFenceLinesByZone,edttxtFrontyardByZone,edttxtBackYardByZone,edttxtExteriorPerimeterHouseByZone,
	edttxtRoofGutterLine,edttxtGarageBYZone,edttxtAtticByZone,edttxtInteriorOfHouseByZone,
	edttxtShedByZone,edttxtOtherByZone,
	//termite
	edttxtMonitorReplaced,
	percent,amount,
	// for rodent services
	edttxtNumberofLiveAnimalTrapsinstalled,edttxtNumberofSnaptrapsplaced,
	//mosquito
	edttxtMosquitoServices1,edttxtMosquitoServices2,
	//invoice page
	edttxtInvValue,edttxtProdValue,edttxtTax,edttxtTotal,
	edttxtTechComments,edttxtServiceTech,edttxtEmp,edttxtCustomer,edttxtdate,edtxtTimeIn,
	edttxtAmount,edttxtCheckNo,edttxtDrivingLicence,edttxtExpirationDate;

	TextView txtproduct,txtName,txtAccountNumber,txtTimeIn,txtServiceDescription,txtService_Attributes,
	txtTechnicianName,txtTechnicianAccount,txtOrderNumber,textviewCharactersTyped,
	product,PercentHead,AmountHead,UnitHead,TargetHead; 

	String
	//for service for
	strCheckPlusFleas,strCheckPlusTermites,strPlusMosquitoMistingSystem,strCheckPlusfireAnts,
	strCheckRodents,strCheckPlusSignatureControl,strCheckTermitetreatment,strCheckRodentExclusion,
	strCheckBedBudTreatment,strCheckMoaquitoSupression,

	//for ares inspected
	strCheckExteriorPerimeterHouseAreasInspected,strCheckFrontyardAreasInspected,strOtherPest,strAnts
	,strCheckBackYardAreasInspected,strCheckExteriorPerimeterGarage,strCheckOutBuildings,strCheckFenceLines
	,strCheckLivingRoomsAreasInspected,strCheckInteriorOfHouseAreasInspected,strCheckAtticAreasInspected,

	//for  inspected and treated for
	strCheckRoaches,strCheckSpiders,
	strCheckScorpions,strCheckSilverFish,strCheckPantryPests,strCheckPharaohAnts,strCheckFireAnts,
	strCheckCarepenterAnts,strCheckMillipedes,strCheckEarWigs,strCheckPillBugs,
	strCheckCrickets,strCheckWasps,strCheckSubTermities,strCheckDryWoodTermities,
	strCheckRoofRats,strCheckMice,strCheckRacoons,strCheckOppsumss,strCheckSkunks,
	strCheckSqirrel,strCheckTicks,strCheckBedBugs,strCheckTwanyCrazyAnts,strCheckAnts,
	strCheckOtherPests,
	// for outside service performed
	strCheckOutsideOnlySerivcePerformed,
	// for pest activity by zone
	strCheckFrontyardByZone,strCheckBackYardByZone,strCheckExteriorPerimeterHouseByZone,
	strCheckRoofGutterLine,strCheckGarageBYZone,strCheckAtticByZone,strCheckInteriorOfHouseByZone
	,strCheckShedByZone,strCheckOtherByZone,
	//for pest control serivces
	strCheckExteriorPestControl,strCheckInteriorPestControl,strCheckTreatedFrontPerimeter,
	strCheckTreatedbackPerimeter,strCheckTreatedInspectedAttic,strCheckTreatedInspectedKitchen
	,strCheckTreatedWhepholes,strCheckTreatedInspectedLaundryRooms,strCheckTreatedCracksCreves,
	strCheckTreatedInpectedlivingRooms,strCheckTreatedYardHaouborageBreedingSites,
	strCheckTreatedInspectedOtherLivingAreas,strCheckTreatedFireAntsInYards,
	strCheckInspectedReplacedZoneMonitors,strCheckTreatedInspectedDooeWaysWindows,
	strCheckTreatedInspectedBedroomsBathrooms,
	//for termite services
	strCheckFenceInspected,strCheckNumberofMonitoringDevicesInspectedTermiteServices="",
	strCheckMonitoringDevicesTermiteServices,strCheckTermiteMonitoringDevicereplaced,
	strCheckExteriorPerimeterofhouseTermiteServices,strCheckAttictermiteServices,
	strCheckInteriorOfHousetermiteServices,
	//for rodent services
	strCheckRoofPitches,strCheckRoofVents,
	strCheckSoffitVents,strCheckWeepHoles,
	strCheckSidingofHouse,strCheckBreezewayGarage,
	strCheckGarageDoor,strCheckAcDryerVentsPhoneline,strCheckNumberofLiveAnimalTrapsinstalled,
	strCheckNumberofSnaptrapsplaced,
	//for mosquito services
	strCheckToppedOfftank,strCheckLeadsAtTank,strCheckTubingLeaking,strCheckInspectedNozzles,
	strCheckRanSystem,strCheckCheckedRunDurationTime,strCheckNozzlesLeaking,strCheckCheckedTimeSystemRuns,
	//for recomendations
	strCheckTreeBranches,strCheckFirewood,strCheckDebrisInCrawl,strCheckExcessiveplant,strCheckSoilAbove,
	strCheckWoodSoil,strCheckDebrisRoof,strCheckStandingWater,strCheckMoistureProblem,strCheckOpeningPlumbing
	,strCheckExcessiveGaps,strCheckLaekyPlumbing,strCheckGarbageCans,strCheckMoistureDamage,strCheckGrocerybags
	,strCheckPetFood,strCheckRecycledItems,strCheckExcessiveStorage,
	//for invoice
	strRadioCash,strRadioCheck,strRadioCreditcard,strRadioBill,strRadioAutoCharge,strRadiopaymentPending,strRadioNoCharge,
	//for termite
	strRadioFenceInspected,strRadioMonitoringDevicesTermiteServices,strRadioTermiteMonitoringDevicereplaced,
	strRadioExteriorPerimeterofhouseTermiteServices,strRadioAttictermiteServices,
	strRadioInteriorOfHouse,
	strRadioNoMonitoringDevicesTermiteServices,strRadioNoTermiteMonitoringDevicereplaced,
	strRadioNoExteriorPerimeterofhouseTermiteServices,strRadioNoAttictermiteServices,
	strRadioNoInteriorOfHousetermiteServices,
	//for rodent services
	strRadioRoofPitches,strRadioRoofVents,strRadioSoffitVents,strRadioWeepHoles,
	strRadioSidingofHouse,strRadioBreezewayGarage,strRadioGarageDoor,strRadioAcDryerVentsPhoneline,
	strRadioNumberofLiveAnimalTraps,strRadioNumberofSnaptrapsplaced,
	strRadioNoRoofPitches,strRadioNoRoofVents,
	strRadioNoSoffitVents,strRadioNoWeepHolesstrRadioNoSidingofHouse,strRadioNoBreezewayGarage,
	strRadioNoGarageDoor,strRadioNoAcDryerVentsPhoneline,strRadioNoNumberofLiveAnimalTrapsinstalled,
	strEdttxtInspectedandTreatedFor1="",strEdttxtInspectedandTreatedFor2="",
	strEdttxtNumberofMonitoringDevicesInspected,

	// for pest activity by zone
	strEdttxtFenceLinesByZone,strEdttxtFrontyardByZone,strEdttxtBackYardByZone,strEdttxtExteriorPerimeterHouseByZone,
	strEdttxtRoofGutterLine,strEdttxtGarageBYZone,strEdttxtAtticByZone,strEdttxtInteriorOfHouseByZone,
	strEdttxtShedByZone,strEdttxtOtherByZone,
	// for rodent services
	strEdttxtNumberofLiveAnimalTrapsinstalled,strEdttxtNumberofSnaptrapsplaced,
	//mosquito
	strEdttxtMosquitoServices1,strEdttxtMosquitoServices2,
	//invoice page
	strEdttxtInvValue,strEdttxtProdValue,strEdttxtTax,strEdttxtTotal,
	strEdttxtTechComments,strEdttxtServiceTech,strEdttxtEmp,strEdttxtCustomer,strEdttxtdate,stredtxtTimeIn,
	strEdttxtAmount,strEdttxtCheckNo,strEdttxtDrivingLicence,strEdtExpirationDate
	,strTxtproduct,strTxtName,strTxtAcctNo,strTxtTimeIn,strTxtServiceDescription,strTxtService_Attributes,
	strTxtTechnicianName,strTxtTechnicianAccount,
	strServiceFor="",strAreasInspected="",strInspectedadnTreatedFor="",strOutsideService="",
	strpestActivityByZone="",strpestControlServices="",strMonitorInspected="", strMosquito="",strOutsidepestControlServices="",
	strInsidepestControlServices="",strRadioInvoice="",serviceID_new="",
	strResidentialId;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		setContentView(R.layout.hr_servicereport);
		
		Thread.setDefaultUncaughtExceptionHandler(new TopExceptionHandler(this));
		
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setDisplayShowHomeEnabled(true);

		initialize();
		JSONObject jsGeneralInfo=houstonFlowFunctions.getSelectedService(serviceID_new);
		if(jsGeneralInfo!=null)
		{
			generalInfoDTO=new Gson().fromJson(jsGeneralInfo.toString(),GeneralInfoDTO.class);
			LoggerUtil.e("", "generalInfoDTO inside if..."+generalInfoDTO.getCheckNo());
		}
		addCheckBoxexToList();
		generateChemicaltable();
		setPreviousFilledData();
		checkChange=1;
		if(generalInfoDTO.getServiceStatus().equals("Complete"))
		{
			setViewForComplete();
		}
		super.onCreate(savedInstanceState);
	}
	private void setViewForComplete() {
		// TODO Auto-generated method stub

		//CHECKBOX servicee for
		checkBoxSignaturePestControl.setEnabled(false);
		checkBoxPlusTermites.setEnabled(false);
		checkBoxPlusMosquitoMistingSystem.setEnabled(false);
		checkBoxPlusfireAnts.setEnabled(false);
		checkBoxPlusRodents.setEnabled(false);

		checkBoxPlusFleas.setEnabled(false);
		checkBoxTermitetreatment.setEnabled(false);
		checkBoxRodentExclusion.setEnabled(false);
		checkBoxBedBudTreatment.setEnabled(false);
		checkBoxMoaquitoSupression.setEnabled(false);

		//Components areas inspected
		checkBoxExteriorPerimeterHouseAreasInspected.setEnabled(false);
		checkBoxFrontyardAreasInspected.setEnabled(false);
		checkBoxBackYardAreasInspected.setEnabled(false);
		checkBoxExteriorPerimeterGarage.setEnabled(false);
		checkBoxOutBuildings.setEnabled(false);
		checkboxFenceLinesAreasInspected.setEnabled(false);
		checkBoxInteriorOfHouseAreasInspected.setEnabled(false);
		checkBoxExteriorHouseAreasInspected.setEnabled(false);
		checkBoxAtticAreasInspected.setEnabled(false);

		// components inspected and treated for
		checkBoxRoaches.setEnabled(false);
		checkBoxSpiders.setEnabled(false);
		checkBoxScorpions.setEnabled(false);
		checkBoxSilverFish.setEnabled(false);
		checkBoxPantryPests.setEnabled(false);
		checkBoxPharaohAnts.setEnabled(false);
		checkBoxFireAnts.setEnabled(false);
		checkBoxCarepenterAnts.setEnabled(false);
		checkBoxMillipedes.setEnabled(false);
		checkBoxEarWigs.setEnabled(false);
		checkBoxPillBugs.setEnabled(false);
		checkboxCrickets.setEnabled(false);
		checkboxWasps.setEnabled(false);
		checkboxSubTermities.setEnabled(false);
		checkboxDryWoodTermities.setEnabled(false);
		checkboxRoofRats.setEnabled(false);
		checkboxMice.setEnabled(false);
		checkboxRacoons.setEnabled(false);
		checkBoxOppsumss.setEnabled(false);
		checkBoxSkunks.setEnabled(false);
		checkBoxSqirrel.setEnabled(false);
		checkBoxTicks.setEnabled(false);
		checkBoxBedBugs.setEnabled(false);
		checkBoxTwanyCrazyAnts.setEnabled(false);
		checkBoxAnts.setEnabled(false);
		checkBoxOtherPests.setEnabled(false);
		edttxtInspectedandTreatedFor1.setEnabled(false);
		edttxtInspectedandTreatedFor1.setEnabled(false);

		//components outside only
		checkBoxOutsideOnlySerivcePerformed.setEnabled(false);

		//components pest activity by zone
		checkBoxFenceLines.setEnabled(false);
		checkBoxBackYardByZone.setEnabled(false);
		checkBoxFrontyardByZone.setEnabled(false);
		checkBoxExteriorPerimeterHouseByZone.setEnabled(false);
		checkBoxRoofLine.setEnabled(false);
		checkBoxGarageBYZone.setEnabled(false);
		checkBoxAtticByZone.setEnabled(false);
		checkBoxInteriorOfHouseByZone.setEnabled(false);

		edttxtFenceLinesByZone.setEnabled(false);
		edttxtFrontyardByZone.setEnabled(false);
		edttxtBackYardByZone.setEnabled(false);
		edttxtExteriorPerimeterHouseByZone.setEnabled(false);
		edttxtRoofGutterLine.setEnabled(false);
		edttxtGarageBYZone.setEnabled(false);
		edttxtAtticByZone.setEnabled(false);
		edttxtInteriorOfHouseByZone.setEnabled(false);

		//components pest ctrl services
		checkBoxExteriorPestControl.setEnabled(false);
		checkBoxInteriorPestControl.setEnabled(false);
		checkBoxTreatedFrontPerimeter.setEnabled(false);
		checkBoxTreatedInspectedAttic.setEnabled(false);

		checkBoxTreatedbackPerimeter.setEnabled(false);
		checkBoxTreatedInspectedKitchen.setEnabled(false);
		checkBoxTreatedWhepholes.setEnabled(false);
		checkBoxTreatedWhepholes.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		checkBoxTreatedInspectedLaundryRooms.setEnabled(false);

		checkBoxTreatedCracksCreves.setEnabled(false);
		checkBoxTreatedInpectedlivingRooms.setEnabled(false);
		checkBoxTreatedYardHaouborageBreedingSites.setEnabled(false);
		checkBoxTreatedInspectedOtherLivingAreas.setEnabled(false);
		checkBoxTreatedFireAntsInYards.setEnabled(false);
		checkBoxInspectedReplacedZoneMonitors.setEnabled(false);
		checkBoxTreatedInspectedDooeWaysWindows.setEnabled(false);
		checkBoxTreatedInspectedBedroomsBathrooms.setEnabled(false);

		//components termite services
		checkBoxFenceInspected.setEnabled(false);
		checkBoxNumberofMonitoringDevicesInspectedTermiteServices.setEnabled(false);
		edttxtNumberofMonitoringDevicesInspected.setEnabled(false);
		checkBoxMonitoringDevicesTermiteServices.setEnabled(false);
		checkBoxTermiteMonitoringDevicereplaced.setEnabled(false);
		checkBoxExteriorPerimeterofhouseTermiteServices.setEnabled(false);
		checkBoxAttictermiteServices.setEnabled(false);
		checkBoxInteriorOfHousetermiteServices.setEnabled(false);

		// components rodent services
		checkBoxRoofPitches.setEnabled(false);

		checkBoxRoofVents.setEnabled(false);


		checkBoxSoffitVents.setEnabled(false);


		checkBoxWeepHoles.setEnabled(false);


		checkBoxSidingofHouse.setEnabled(false);


		checkBoxBreezewayGarage.setEnabled(false);


		checkBoxGarageDoor.setEnabled(false);


		checkBoxAcDryerVentsPhoneline.setEnabled(false);


		checkBoxNumberofLiveAnimalTrapsinstalled.setEnabled(false);
		edttxtNumberofLiveAnimalTrapsinstalled.setEnabled(false);

		checkBoxNumberofSnaptrapsplaced.setEnabled(false);
		edttxtNumberofSnaptrapsplaced.setEnabled(false);

		radioYesFenceInspected.setEnabled(false);
		radioYesMonitoringDevicesTermiteServices.setEnabled(false);
		radioYesTermiteMonitoringDevicereplaced.setEnabled(false);
		radioYesExteriorPerimeterofhouseTermiteServices.setEnabled(false);
		radioYesAttictermiteServices.setEnabled(false);
		radioYesInteriorOfHousetermiteServices.setEnabled(false);
		radioNoFenceInspected.setEnabled(false);
		radioNoMonitoringDevicesTermiteServices.setEnabled(false);
		radioNoTermiteMonitoringDevicereplaced.setEnabled(false);
		radioNoExteriorPerimeterofhouseTermiteServices.setEnabled(false);
		radioNoAttictermiteServices.setEnabled(false);
		radioNoInteriorOfHousetermiteServices.setEnabled(false);

		//for rodent services
		radioYesRoofPitches.setEnabled(false);
		radioYesRoofVents.setEnabled(false);
		radioYesSoffitVents.setEnabled(false);
		radioYesWeepHoles.setEnabled(false);
		radioYesSidingofHouse.setEnabled(false);
		radioYesBreezewayGarage.setEnabled(false);
		radioYesGarageDoor.setEnabled(false);
		radioYesAcDryerVentsPhoneline.setEnabled(false);
		//radioYesNumberofSnaptrapsplaced.setEnabled(false);
		radioNoRoofPitches.setEnabled(false);
		radioNoRoofVents.setEnabled(false);
		radioNoSoffitVents.setEnabled(false);
		radioNoWeepHoles.setEnabled(false);
		radioNoSidingofHouse.setEnabled(false);
		radioNoBreezewayGarage.setEnabled(false);
		radioNoGarageDoor.setEnabled(false);
		radioNoAcDryerVentsPhoneline.setEnabled(false);
		//radioNoNumberofLiveAnimalTrapsinstalled.setEnabled(false);


		//components mosquito services
		checkBoxToppedOfftank.setEnabled(false);
		checkBoxLeadsAtTank.setEnabled(false);
		checkBoxTubingLeaking.setEnabled(false);
		checkBoxInspectedNozzles.setEnabled(false);
		checkBoxRanSystem.setEnabled(false);
		checkBoxNozzlesLeaking.setEnabled(false);
		checkBoxCheckedRunDurationTime.setEnabled(false);
		checkBoxCheckedTimeSystemRuns.setEnabled(false);
		edttxtMosquitoServices1.setEnabled(false);
		edttxtMosquitoServices2.setEnabled(false);

		// components recommendations
		checkBoxRecycledItems.setEnabled(false);
		checkBoxExcessiveStorage.setEnabled(false);
		checkBoxPetFood.setEnabled(false);

		checkBoxGroceryBags.setEnabled(false);

		checkBoxMoisrtureDamage.setEnabled(false);

		checkBoxgarbageCans.setEnabled(false);

		checkBoxLeackPumbing.setEnabled(false);

		checkBoxExcessiveGaps.setEnabled(false);

		checkBoxWoodSoil.setEnabled(false);

		checkBoxDebrisRoof.setEnabled(false);

		checkBoxStandingwater.setEnabled(false);

		checkBoxMoistureProblem.setEnabled(false);

		checkBoxOpeningspumbing.setEnabled(false);


		checkBoxBranchHouse.setEnabled(false);

		checkBoxFirewood.setEnabled(false);

		checkBoxDebrisCrawl.setEnabled(false);

		checkBoxExcessiveplant.setEnabled(false);

		checkBoxSoilAbove.setEnabled(false);

		//components invoice
		radioCash.setEnabled(false);
		radioCheck.setEnabled(false);
		radioCreditcard.setEnabled(false);
		radioNoCharge.setEnabled(false);
		radioBillLater.setEnabled(false);
		radioPreBill.setEnabled(false);
		edttxtAmount.setEnabled(false);
		edttxtCheckNo.setEnabled(false);
		edttxtDrivingLicence.setEnabled(false);
		btnCHECK_BACK_IMAGE.setEnabled(false);
		btnCHECK_FRONT_IMAGE.setEnabled(false);

		edttxtTechComments.setEnabled(false);
		edttxtServiceTech.setEnabled(false);
		edttxtEmp.setEnabled(false);
		edttxtCustomer.setEnabled(false);
		edttxtdate.setEnabled(false);
		radioGroupACDRYER.setEnabled(false);
		radioGroupInvoice.setEnabled(false);

		for(int i=0 ; i < chemicalsComponentList.size() ; i++)
		{
			ChemicalsComponentDTO chemicalsComponentDTO=chemicalsComponentList.get(i);
			chemicalsComponentDTO.getAmountEditText().setEnabled(false);
			chemicalsComponentDTO.getChemicalsCheckBox().setEnabled(false);
			chemicalsComponentDTO.getPersentSpinner().setEnabled(false);
			chemicalsComponentDTO.getTargetSpinner().setEnabled(false);
			chemicalsComponentDTO.getUnitSpinner().setEnabled(false);
		}
	}
	private void generateChemicaltable() 
	{
		// TODO Auto-generated method stub
		//JSONArray arrayRecordsSelected = houstonFlowFunctions.getChemicals(DatabaseHelper.TABLE_CHEMICALS,serviceID_new);
		//Log.e("", "ARRAY SELECTED CHEMICALS.LENGTH 1.."+arrayRecords.length());
		/*		if(arrayRecords == null || arrayRecords.length()<= 0)
		{*/
		JSONArray arrayRecords = houstonFlowFunctions.getChemicalsWhenNoRecord(DatabaseHelper.TABLE_CHEMICALS_WHEN_NO_RECORD,"Residential");
		listChemicals = new Gson().fromJson(arrayRecords.toString(), new TypeToken<List<ChemicalDto>>(){}.getType());
		/*}
		else
		{
			listChemicals = new Gson().fromJson(arrayRecords.toString(), new TypeToken<List<ChemicalDto>>(){}.getType());

		}*/
		//LoggerUtil.e("LIST CHEMICALS SIDZE...","LIST CHEMICALS SIDZE..."+ listChemicals.size());
		ArrayList<ChemicalDto> chemicalListSelected=new ArrayList<ChemicalDto>();

		for(int i=0;i<listChemicals.size();i++)
		{
			ChemicalDto chemicalMasterDTO=listChemicals.get(i);
			ChemicalDto chemicalSelectedDTO=houstonFlowFunctions.getchemicalAlreadyAdded("Residential",serviceID_new, chemicalMasterDTO.getPreventationID(),DatabaseHelper.TABLE_CHEMICALS);
			if(chemicalSelectedDTO!=null)
			{
				chemicalListSelected.add(chemicalSelectedDTO);
			}else
			{
				chemicalListSelected.add(chemicalMasterDTO);
			}

			//AddChemicalsDemo(i);
		}
		listChemicals.clear();
		listChemicals.addAll(chemicalListSelected);
		for(int i=0;i<listChemicals.size();i++)
		{
			AddChemicals(i);
		}	
	}

	int i=0;
	TableLayout tb;
	TableRow deleteTableRow;
	int numberofaudit;
	TableRow tr,tr1;
	ArrayList<TableRow> trlist=new ArrayList<TableRow>();
	public void AddChemicals(final int listindex)
	{
		numberofaudit++;
		//LoggerUtil.e("serviceID_new", serviceID_new);
		if(i==0)
		{
			tb=new TableLayout(HR_PestControlServiceReport.this);
			//addHeader();
			mainLayout.addView(tb);
		}
		i=i+1;
		tr = new TableRow(HR_PestControlServiceReport.this);
		trlist.add(tr);
		tr1 = new TableRow(HR_PestControlServiceReport.this);
		trlist.add(tr1);
		//if(i%2!=0)
		tr.setBackgroundColor(Color.parseColor("#EDEDEF"));
		tr.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
		tr1.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));

		LayoutParams params1 = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.FILL_PARENT,1f);
		final LinearLayout MainLayout=new LinearLayout(getApplicationContext());
		//LayoutParams params1 = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.FILL_PARENT,1f);
		MainLayout.setOrientation(LinearLayout.HORIZONTAL);
		MainLayout.setLayoutParams(params1);	
		MainLayout.setGravity(Gravity.CENTER_VERTICAL);
		//MainLayout.setVisibility(View.GONE);

		final LinearLayout belowLayout=new LinearLayout(getApplicationContext());
		//LayoutParams params1 = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.FILL_PARENT,1f);
		//((MarginLayoutParams) params1).setMargins(5, 0, 5, 0);
		belowLayout.setOrientation(LinearLayout.VERTICAL);
		belowLayout.setLayoutParams(params1);	
		belowLayout.setGravity(Gravity.CENTER_VERTICAL);
		belowLayout.setVisibility(View.GONE);

		chemicalcheckBox = new CheckBox(HR_PestControlServiceReport.this);
		checkList.add(listindex,chemicalcheckBox);
		chemicalcheckBox.setTag(Integer.valueOf(i));
		chemicalcheckBox.setText("");
		chemicalcheckBox.setId(i+10000);
		//chemicalcheckBox.setBackgroundResource(R.drawable.checked);
		chemicalcheckBox.setGravity(Gravity.CENTER_VERTICAL);

		//LoggerUtil.e("con", "contains or not"+listPreviousSelectedChemicals.contains(listChemicals.get(listindex).getPestPrevention_ID()+".."+listChemicals.get(listindex).getPestPrevention_ID()));

		if(listChemicals.get(listindex).getProductCheck().equals("true"))
		{
			belowLayout.setVisibility(View.VISIBLE);
			checkList.get(listindex).setChecked(true);
		}
		else
		{
			checkList.get(listindex).setChecked(false);
		}

		LayoutParams params0 = new TableRow.LayoutParams(android.widget.LinearLayout.LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		final ChemicalDto chemicalDto=new ChemicalDto();
		chemicalcheckBox.setLayoutParams(params0);
		MainLayout.addView(chemicalcheckBox);

		chemicalcheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked)
				{
					belowLayout.setVisibility(View.VISIBLE);
				}
				else
				{
					belowLayout.setVisibility(View.GONE);
				}
			}
		});

		LinearLayout productLayout=new LinearLayout(getApplicationContext());
		productLayout.setOrientation(LinearLayout.HORIZONTAL);
		productLayout.setLayoutParams(params1);	
		productLayout.setGravity(Gravity.CENTER_VERTICAL);
		String productName=listChemicals.get(listindex).getProduct();
		product = new TextView(getApplicationContext());
		product.setText(productName);
		product.setTextColor(Color.BLACK);

		LayoutParams params2 = new TableRow.LayoutParams( LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT,.3f);

		productLayout.addView(product);
		if(productName.equals("Other"))
		{
			//LoggerUtil.e("edtOtherText", "edtOtherText");
			edtOtherText = new EditText(HR_PestControlServiceReport.this);
			edtOtherText.setId(i+3000);
			edtOtherText.setVisibility(View.VISIBLE);
			edtOtherText.setSingleLine();
			edtOtherText.setCursorVisible(true);
			edtOtherText.setFilters(CommonFunction.specialchars());

			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);
			lp.setMargins(3, 0, 3, 0);
			edtOtherText.setLayoutParams(lp);

			edtOtherText.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
			edtOtherText.setBackgroundResource(android.R.drawable.editbox_background_normal);
			final float scale = this.getResources().getDisplayMetrics().density;
			/*edtOtherText.setPadding(edtOtherText.getPaddingLeft() + (int)(10.0f * scale + 0.5f),
					5,
					5,
					5);*/
			edtOtherText.setText(listChemicals.get(listindex).getOtherText());
			if(listChemicals.get(listindex).getProductCheck().equals("true"))
			{
				edtOtherText.setText(listChemicals.get(listindex).getOtherText());
				listChemicals.get(listindex).setOtherText(edtOtherText.getText().toString());
			}
			else
			{
				edtOtherText.setText(listChemicals.get(listindex).getOtherText());
				listChemicals.get(listindex).setOtherText(edtOtherText.getText().toString());
			}
			edtOtherText.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					
					listChemicals.get(listindex).setOtherText(s.toString().trim());
					chemicalDto.setOtherText(listChemicals.get(listindex).getOtherText());
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					
				}
			});
			productLayout.addView(edtOtherText);
		}

		MainLayout.addView(productLayout);
		tr.addView(MainLayout);

		final LinearLayout PercentLayout=new LinearLayout(getApplicationContext());
		//LayoutParams params1 = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.FILL_PARENT,1f);
		PercentLayout.setOrientation(LinearLayout.HORIZONTAL);
		PercentLayout.setLayoutParams(params1);	
		PercentLayout.setGravity(Gravity.CENTER_VERTICAL);

		PercentHead = new TextView(getApplicationContext());
		PercentHead.setText("Percent");
		PercentHead.setTextColor(Color.BLACK);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		lp.setMargins(5, 0, 5, 0);
		PercentHead.setLayoutParams(lp);
		PercentLayout.addView(PercentHead);

		percent = new EditText(HR_PestControlServiceReport.this);
		percent.setId(i+3000);
		percent.setMaxWidth(60);
		percent.setSingleLine();
		percent.setGravity(Gravity.LEFT);
		percent.setCursorVisible(true);
		percent.setFilters(CommonFunction.limitchars(6));
		percent.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
		percent.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
		percent.setBackgroundResource(android.R.drawable.editbox_background_normal);
		if(listChemicals.get(listindex).getProductCheck().equals("true"))
		{

			percent.setText(listChemicals.get(listindex).getPercentPes());
			listChemicals.get(listindex).setPercentPes(percent.getText().toString());

		}
		else
		{
			percent.setText(listChemicals.get(listindex).getPercentPes());
			listChemicals.get(listindex).setPercentPes(percent.getText().toString());

		}
		percent.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				listChemicals.get(listindex).setPercentPes(s.toString().trim());
				chemicalDto.setPercentPes(listChemicals.get(listindex).getPercentPes());
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

				listChemicals.get(listindex).setPercentPes(s.toString().trim());
			}
		});
		//amount.requestFocus();
		//availroom.setPadding(5, 0, 5, 0);
		percent.setTextColor(Color.BLACK);
		percent.setLayoutParams(params2);
		PercentLayout.addView(percent);

		belowLayout.addView(PercentLayout);

		final LinearLayout AmountLayout=new LinearLayout(getApplicationContext());
		//LayoutParams params1 = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.FILL_PARENT,1f);
		AmountLayout.setOrientation(LinearLayout.HORIZONTAL);
		AmountLayout.setLayoutParams(params1);	
		AmountLayout.setGravity(Gravity.CENTER_VERTICAL);

		AmountHead = new TextView(getApplicationContext());
		AmountHead.setText("Amount");
		AmountHead.setTextColor(Color.BLACK);
		LinearLayout.LayoutParams lpAmt = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		lpAmt.setMargins(5, 0, 3, 0);
		AmountHead.setLayoutParams(lpAmt);
		AmountLayout.addView(AmountHead);

		amount = new EditText(HR_PestControlServiceReport.this);
		amount.setId(i+3000);
		amount.setMaxWidth(60);
		amount.setSingleLine();
		amount.setGravity(Gravity.LEFT);
		amount.setCursorVisible(true);
		amount.setFilters(CommonFunction.limitchars(8));
		amount.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
		amount.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
		amount.setBackgroundResource(android.R.drawable.editbox_background_normal);

		//availroom.setPadding(5, 0, 5, 0);
		if(listChemicals.get(listindex).getProductCheck().equals("true"))
		{
			amount.setText(listChemicals.get(listindex).getAmount());
			listChemicals.get(listindex).setAmount(amount.getText().toString());
		}
		else
		{
			amount.setText(listChemicals.get(listindex).getAmount());
			listChemicals.get(listindex).setAmount(amount.getText().toString());                          
		}
		amount.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

				listChemicals.get(listindex).setAmount(s.toString().trim());
				chemicalDto.setAmount(listChemicals.get(listindex).getAmount());
			}
		});
		amount.setTextColor(Color.BLACK);
		//amount.setText(preventionDTO.getAmount());
		amount.setLayoutParams(params2);
		AmountLayout.addView(amount);
		belowLayout.addView(AmountLayout);


		final LinearLayout UnitLayout=new LinearLayout(getApplicationContext());
		//LayoutParams params1 = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.FILL_PARENT,1f);
		UnitLayout.setOrientation(LinearLayout.HORIZONTAL);
		UnitLayout.setLayoutParams(params1);	
		UnitLayout.setGravity(Gravity.CENTER_VERTICAL);
		UnitLayout.setVisibility(View.GONE);

		UnitHead = new TextView(getApplicationContext());
		UnitHead.setText("Unit   ");
		UnitHead.setTextColor(Color.BLACK);
		LinearLayout.LayoutParams lpUnit = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		lpUnit.setMargins(5, 0,10, 0);
		UnitHead.setLayoutParams(lpUnit);
		PercentLayout.addView(UnitHead);
		String[] units=getResources().getStringArray(R.array.units);

		final ArrayList<String> UNIT=new ArrayList<String>(Arrays.asList(units));
		ArrayAdapter<String> unitAdapter = new ArrayAdapter<String>(HR_PestControlServiceReport.this,android.R.layout.simple_spinner_item,UNIT);
		final Spinner  spnUNIT = new Spinner (HR_PestControlServiceReport.this);
		spnUNIT.setAdapter(unitAdapter);
		spnUNIT.setPrompt("Select ");
		spnUNIT.setLayoutParams(params2);
		spnUNIT.setEnabled(true);
		//LoggerUtil.e("listPreviousPestId.contains...","listPreviousPestId.contains..."+listPreviousPestId.contains(listChemicals.get(listindex).getPestPrevention_ID()));
		//LoggerUtil.e("listPreviousPestId.indexOf...","listPreviousPestId.contains..."+listPreviousPestId.indexOf(listChemicals.get(listindex).getPestPrevention_ID()));

		if(listChemicals.get(listindex).getProductCheck().equals("true"))
		{
			int index = Arrays.asList(units).indexOf(listChemicals.get(listindex).getUnit());	
			spnUNIT.setSelection(index);
			listChemicals.get(listindex).setUnit(spnUNIT.getSelectedItem().toString());
		}

		spnUNIT.setOnItemSelectedListener(new OnItemSelectedListener()
		{
			public void onItemSelected(AdapterView<?> arg0, View arg1,int arg2, long arg3) 
			{
				listChemicals.get(listindex).setUnit(spnUNIT.getSelectedItem().toString());
			}
			public void onNothingSelected(AdapterView<?> arg0)
			{
				// TODO Auto-generated method stub

			}
		});
		PercentLayout.addView(spnUNIT);
		belowLayout.addView(UnitLayout);

		final LinearLayout TargetLayout=new LinearLayout(getApplicationContext());
		//LayoutParams params1 = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.FILL_PARENT,1f);
		TargetLayout.setOrientation(LinearLayout.HORIZONTAL);
		TargetLayout.setLayoutParams(params1);	
		TargetLayout.setGravity(Gravity.CENTER_VERTICAL);

		TargetHead = new TextView(getApplicationContext());
		TargetHead.setText("Target ");
		TargetHead.setTextColor(Color.BLACK);
		LinearLayout.LayoutParams lpTarget = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		lpTarget.setMargins(5, 0, 8, 0);
		TargetHead.setLayoutParams(lpTarget);
		TargetLayout.addView(TargetHead);

		final EditText target = new EditText(HR_PestControlServiceReport.this);
		target.setId(i+3000);
		target.setSingleLine(false);
		target.setMaxWidth(60);
		//target.setKeyListener(DigitsKeyListener.getInstance("a bcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"));
		target.setInputType(InputType.TYPE_CLASS_TEXT);
		target.setGravity(Gravity.LEFT);
		target.setCursorVisible(true);
		target.setFilters(CommonFunction.specialcharsForTarget());

		//target.setFilters(CommonFunction.limitchars(20));
		target.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
		if(listChemicals.get(listindex).getProductCheck().equals("true"))
		{
			target.setText(listChemicals.get(listindex).getTarget());
			listChemicals.get(listindex).setTarget(target.getText().toString());
		}
		else
		{
			target.setText(listChemicals.get(listindex).getTarget());
			listChemicals.get(listindex).setTarget(target.getText().toString());
		}
		target.setBackgroundResource(android.R.drawable.editbox_background_normal);
		target.setTextColor(Color.BLACK);

		target.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				if(s.length()>= 55)
				{   
					s = s.subSequence(0, s.length()-1);
					target.setText(s.toString());
					target.setSelection(s.length(), s.length());
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

				listChemicals.get(listindex).setTarget(s.toString().trim());
				listChemicals.get(listindex).setTarget(target.getText().toString());
			}
		}); 
		target.setLayoutParams(params2);
		TargetLayout.addView(target);
		belowLayout.addView(TargetLayout);

		tr1.addView(belowLayout);

		tb.addView(tr);
		tb.addView(tr1);

		ChemicalsComponentDTO chemicalsComponentDTO=new ChemicalsComponentDTO();
		chemicalsComponentDTO.setAmountEditText(amount);
		chemicalsComponentDTO.setChemicalsCheckBox(chemicalcheckBox);
		chemicalsComponentDTO.setPersentSpinner(percent);
		chemicalsComponentDTO.setProductTextView(product);
		chemicalsComponentDTO.setTargetSpinner(target);
		chemicalsComponentDTO.setUnitSpinner(spnUNIT);
		chemicalsComponentDTO.setOtherEditText(edtOtherText);

		chemicalsComponentList.add(chemicalsComponentDTO);
	}
	public void addHeader()
	{
		TableRow tr = new TableRow(getApplicationContext());
		if(i%2!=0)

		tr.setBackgroundColor(Color.parseColor("#2F80B7"));
		//tr.setOrientation(LinearLayout.HORIZONTAL);
		tr.setBackgroundColor(Color.parseColor("#2F80B7"));
		tr.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.FILL_PARENT));
		//tr.setWeightSum(1);
		tr.setPadding(0, 10, 0, 10);
		LayoutParams params1 = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.FILL_PARENT,1f);
		LayoutParams params2 = new TableRow.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT,.3f);
		TextView products = new TextView(getApplicationContext());
		/*products.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
		//products.setVisibility(View.INVISIBLE);
		products.setText("Product");
		products.setTextColor(Color.WHITE);
		LayoutParams paramss1 = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.FILL_PARENT,1f);
		products.setLayoutParams(params2);
		tr.addView(products);
		 */

		TextView product = new TextView(getApplicationContext());
		product.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
		product.setText(getApplicationContext().getResources().getString(R.string.PRODUCT));
		product.setTextColor(Color.WHITE);
		product.setLayoutParams(params2);
		tr.addView(product);

		TextView percent = new TextView(getApplicationContext());
		percent.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
		percent.setText(getApplicationContext().getResources().getString(R.string.percent));
		percent.setLayoutParams(params2);
		percent.setTextColor(Color.WHITE);
		tr.addView(percent);

		TextView AMT = new TextView(getApplicationContext());
		AMT.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
		AMT.setText(getApplicationContext().getResources().getString(R.string.AMT));
		AMT.setPadding(0, 0,5, 0);
		AMT.setLayoutParams(params2);
		AMT.setTextColor(Color.WHITE);
		tr.addView(AMT);

		TextView UNIT = new TextView(getApplicationContext());
		UNIT.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
		UNIT.setText(getApplicationContext().getResources().getString(R.string.UNIT));
		UNIT.setPadding(0, 0,5, 0);
		UNIT.setLayoutParams(params2);
		UNIT.setTextColor(Color.WHITE);
		tr.addView(UNIT);

		TextView TARGET = new TextView(getApplicationContext());
		TARGET.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
		TARGET.setText(getApplicationContext().getResources().getString(R.string.TARGET));
		TARGET.setPadding(0, 0,5, 0);
		TARGET.setLayoutParams(params2);
		TARGET.setTextColor(Color.WHITE);
		tr.addView(TARGET);

		tb.addView(tr, new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));

	}
	private void addCheckBoxexToList() 
	{
		// TODO Auto-generated method stub
		serviceForCheckboxList.add(checkBoxSignaturePestControl);
		serviceForCheckboxList.add(checkBoxPlusTermites);
		serviceForCheckboxList.add(checkBoxPlusMosquitoMistingSystem);
		serviceForCheckboxList.add(checkBoxPlusfireAnts);
		serviceForCheckboxList.add(checkBoxPlusRodents);
		serviceForCheckboxList.add(checkBoxPlusFleas);
		serviceForCheckboxList.add(checkBoxTermitetreatment);
		serviceForCheckboxList.add(checkBoxRodentExclusion);
		serviceForCheckboxList.add(checkBoxBedBudTreatment);
		serviceForCheckboxList.add(checkBoxMoaquitoSupression);


		AreasInspectedCheckboxList.add(checkBoxExteriorPerimeterHouseAreasInspected);
		AreasInspectedCheckboxList.add(checkBoxExteriorPerimeterGarage);
		AreasInspectedCheckboxList.add(checkBoxInteriorOfHouseAreasInspected);
		AreasInspectedCheckboxList.add(checkBoxFrontyardAreasInspected);
		AreasInspectedCheckboxList.add(checkBoxOutBuildings);
		AreasInspectedCheckboxList.add(checkBoxExteriorHouseAreasInspected);
		AreasInspectedCheckboxList.add(checkBoxBackYardAreasInspected);
		AreasInspectedCheckboxList.add(checkboxFenceLinesAreasInspected);
		AreasInspectedCheckboxList.add(checkBoxAtticAreasInspected);


		InspectedTreatedForCheckboxList.add(checkBoxRoaches);
		InspectedTreatedForCheckboxList.add(checkBoxPantryPests);
		InspectedTreatedForCheckboxList.add(checkBoxMillipedes);
		InspectedTreatedForCheckboxList.add(checkboxWasps);
		InspectedTreatedForCheckboxList.add(checkboxMice);
		InspectedTreatedForCheckboxList.add(checkBoxSqirrel);
		InspectedTreatedForCheckboxList.add(checkBoxAnts);
		InspectedTreatedForCheckboxList.add(checkBoxSpiders);
		InspectedTreatedForCheckboxList.add(checkBoxPharaohAnts);

		InspectedTreatedForCheckboxList.add(checkBoxEarWigs);
		InspectedTreatedForCheckboxList.add(checkboxSubTermities);
		InspectedTreatedForCheckboxList.add(checkboxRacoons);
		InspectedTreatedForCheckboxList.add(checkBoxTicks);
		InspectedTreatedForCheckboxList.add(checkBoxOtherPests);
		InspectedTreatedForCheckboxList.add(checkBoxScorpions);
		InspectedTreatedForCheckboxList.add(checkBoxFireAnts);
		InspectedTreatedForCheckboxList.add(checkBoxPillBugs);
		InspectedTreatedForCheckboxList.add(checkboxDryWoodTermities);

		InspectedTreatedForCheckboxList.add(checkBoxOppsumss);
		InspectedTreatedForCheckboxList.add(checkBoxBedBugs);
		InspectedTreatedForCheckboxList.add(checkBoxSilverFish);
		InspectedTreatedForCheckboxList.add(checkBoxCarepenterAnts);
		InspectedTreatedForCheckboxList.add(checkboxCrickets);
		InspectedTreatedForCheckboxList.add(checkboxRoofRats);
		InspectedTreatedForCheckboxList.add(checkBoxSkunks);
		InspectedTreatedForCheckboxList.add(checkBoxTwanyCrazyAnts);

		PestActivityZoneCheckboxList.add(checkBoxFenceLines);
		PestActivityZoneCheckboxList.add(checkBoxFrontyardByZone);
		PestActivityZoneCheckboxList.add(checkBoxBackYardByZone);
		PestActivityZoneCheckboxList.add(checkBoxExteriorPerimeterHouseByZone);
		PestActivityZoneCheckboxList.add(checkBoxRoofLine);
		PestActivityZoneCheckboxList.add(checkBoxGarageBYZone);
		PestActivityZoneCheckboxList.add(checkBoxAtticByZone);
		PestActivityZoneCheckboxList.add(checkBoxInteriorOfHouseByZone);

		pestControlServicesCheckboxList.add(checkBoxExteriorPestControl);
		pestControlServicesCheckboxList.add(checkBoxTreatedFrontPerimeter);
		pestControlServicesCheckboxList.add(checkBoxTreatedbackPerimeter);
		pestControlServicesCheckboxList.add(checkBoxTreatedWhepholes);
		pestControlServicesCheckboxList.add(checkBoxTreatedCracksCreves);
		pestControlServicesCheckboxList.add(checkBoxTreatedYardHaouborageBreedingSites);
		pestControlServicesCheckboxList.add(checkBoxTreatedFireAntsInYards);
		pestControlServicesCheckboxList.add(checkBoxInspectedReplacedZoneMonitors);
		pestControlServicesCheckboxList.add(checkBoxTreatedInspectedDooeWaysWindows);

		pestControlServicesCheckboxList.add(checkBoxInteriorPestControl);
		pestControlServicesCheckboxList.add(checkBoxTreatedInspectedAttic);
		pestControlServicesCheckboxList.add(checkBoxTreatedInspectedKitchen);
		pestControlServicesCheckboxList.add(checkBoxTreatedInspectedLaundryRooms);
		pestControlServicesCheckboxList.add(checkBoxTreatedInpectedlivingRooms);
		pestControlServicesCheckboxList.add(checkBoxTreatedInspectedOtherLivingAreas);
		pestControlServicesCheckboxList.add(checkBoxTreatedInspectedBedroomsBathrooms);

		OutsidepestControlServicesCheckboxList.add(checkBoxExteriorPestControl);
		OutsidepestControlServicesCheckboxList.add(checkBoxTreatedFrontPerimeter);
		OutsidepestControlServicesCheckboxList.add(checkBoxTreatedbackPerimeter);
		OutsidepestControlServicesCheckboxList.add(checkBoxTreatedWhepholes);
		OutsidepestControlServicesCheckboxList.add(checkBoxTreatedCracksCreves);
		OutsidepestControlServicesCheckboxList.add(checkBoxTreatedYardHaouborageBreedingSites);
		OutsidepestControlServicesCheckboxList.add(checkBoxTreatedFireAntsInYards);
		
		OutsidepestControlServicesCheckboxList.add(checkBoxTreatedInspectedDooeWaysWindows);
		
		InsidepestControlServicesCheckboxList.add(checkBoxInteriorPestControl);
		InsidepestControlServicesCheckboxList.add(checkBoxTreatedInspectedAttic);
		InsidepestControlServicesCheckboxList.add(checkBoxTreatedInspectedKitchen);
		InsidepestControlServicesCheckboxList.add(checkBoxTreatedInspectedLaundryRooms);
		InsidepestControlServicesCheckboxList.add(checkBoxTreatedInpectedlivingRooms);
		InsidepestControlServicesCheckboxList.add(checkBoxTreatedInspectedOtherLivingAreas);
		InsidepestControlServicesCheckboxList.add(checkBoxInspectedReplacedZoneMonitors);
		InsidepestControlServicesCheckboxList.add(checkBoxTreatedInspectedBedroomsBathrooms);
		
		mosquitoCheckboxList.add(checkBoxToppedOfftank);
		mosquitoCheckboxList.add(checkBoxTubingLeaking);
		mosquitoCheckboxList.add(checkBoxRanSystem);
		mosquitoCheckboxList.add(checkBoxCheckedRunDurationTime);
		mosquitoCheckboxList.add(checkBoxLeadsAtTank);
		mosquitoCheckboxList.add(checkBoxInspectedNozzles);
		mosquitoCheckboxList.add(checkBoxNozzlesLeaking);
		mosquitoCheckboxList.add(checkBoxCheckedTimeSystemRuns);

	}
	private void setPreviousFilledData() 
	{
		// TODO Auto-generated method stub
		txtName.setText(generalInfoDTO.getFirst_Name());
		txtAccountNumber.setText(generalInfoDTO.getAccountNo());
		txtTimeIn.setText(generalInfoDTO.getTimeIn());
		txtOrderNumber.setText(generalInfoDTO.getOrder_Number());

		JSONObject residential_pest_string=HoustonFlowFunctions.getTodaysReport(serviceID_new);
		//Log.e("residentil string.. to string 1..", "residentil string..."+residential_pest_string);

		if(!(residential_pest_string == null))
		{
			//Log.e("residentil string.. to string..", "residentil string..."+residential_pest_string.toString());
			residential_pest_DTO= new Gson().fromJson(residential_pest_string.toString(), HResidential_pest_DTO.class);

			setPreviousServiceForData();
			setPreviousAreaseInspected();
			setPreviousInspectedTreatedFor();
			setPreviousOutsideOnly();
			setPreviousPestByZone();
			setPreviousPestControlServices();
			setPreviousTermiteServices();
			setPreviousRodentServices();
			setPreviousMosquitoServices();
			setpreviousRecommendations();

		}
		setPreviousInvoice();
	}
	private void setPreviousInvoice()
	{
		// TODO Auto-generated method stub
		ArrayList<RadioButton> listRadioButton = new ArrayList<RadioButton>();
		listRadioButton.add(radioCash);
		listRadioButton.add(radioCheck);
		listRadioButton.add(radioCreditcard);
		listRadioButton.add(radioNoCharge);
		listRadioButton.add(radioBillLater);
		listRadioButton.add(radioPreBill);
		//Log.e("getPaymentType...",listRadioButton.size()+"");

		for(int i=0; i<listRadioButton.size() ; i++)
		{

			String selection =	listRadioButton.get(i).getText().toString();
			//	Log.v("","getPaymentType..."+selection+"..."+residential_pest_DTO.getPaymentType());
			/*if(residential_pest_DTO.getPaymentType().equals("Bill Later"))
			{
				residential_pest_DTO.setPaymentType("Payment due at time of service");
			}*/
			if(selection.equals(residential_pest_DTO.getPaymentType()))
			{
				listRadioButton.get(i).setChecked(true);
				break;
			}
		}
		edttxtTechComments.setText(residential_pest_DTO.getTechnicianComments());
		edttxtServiceTech.setText(generalInfoDTO.getService_Tech_Num());
		edttxtEmp.setText(residential_pest_DTO.getEmpNo());
		edttxtdate.setText(generalInfoDTO.getServiceDate());

		if(CommonFunction.checkString(generalInfoDTO.getFirst_Name(), "").equals("") && CommonFunction.checkString(generalInfoDTO.getLast_Name(), "").equals(""))
		{
			edttxtCustomer.setText(generalInfoDTO.getCompanyName());

		}
		else
		{
			edttxtCustomer.setText(generalInfoDTO.getFirst_Name()+" "+generalInfoDTO.getLast_Name());

		}
		edttxtInvValue.setText(generalInfoDTO.getInv_value());
		edttxtProdValue.setText(generalInfoDTO.getProd_value());

		String taxValue;
		double roundOff = 0;
		if(CommonFunction.checkString(generalInfoDTO.getInv_value(), "").equals("") || CommonFunction.checkString(generalInfoDTO.getTaxCode(), "").equals("") )
		{
			taxValue="0";
		}
		else
		{
			Float taxValueFloat=Float.valueOf(generalInfoDTO.getInv_value())*Float.valueOf(generalInfoDTO.getTaxCode());
			roundOff = Math.round(taxValueFloat * 100.0) / 100.0;
			taxValue=String.valueOf(roundOff);

		}

		edttxtTax.setText(taxValue);
		//Log.e("taxValue", "taxValue.."+taxValue);
		Double totlaD = roundOff+Double.valueOf(generalInfoDTO.getInv_value());
		Double totalFinal = Math.round(totlaD * 100.0) / 100.0;
		edttxtTotal.setText(String.valueOf(totalFinal));
		edttxtCheckNo.setText(generalInfoDTO.getCheckNo());
		//Toast.makeText(getApplicationContext(), generalInfoDTO.getLicenseNo()+"", Toast.LENGTH_LONG).show();

		edttxtDrivingLicence.setText(generalInfoDTO.getLicenseNo());
		if(!generalInfoDTO.getExpirationDate().equals("01/01/1900") && !generalInfoDTO.getExpirationDate().equals(""))
		{
			edttxtExpirationDate.setText(generalInfoDTO.getExpirationDate());
		}

		edttxtAmount.setText(residential_pest_DTO.getAmount());



		strResidentialId=residential_pest_DTO.getResidentialId();
		//Log.e("", "strResidentialId.."+residential_pest_DTO.getResidentialId());
		if(CommonFunction.checkString(strResidentialId, "").equals(""))
		{
			strResidentialId="0";
		}
		//edttxtDrivingLicence.setText(generalInfoDTO.getDr);
		//edttxtTotal.setText(generalInfoDTO.getTotal());
		//Log.e("", "generalInfoDTO.getCheckFrontImage()..."+generalInfoDTO.getCheckFrontImage());
		if(!CommonFunction.checkString(generalInfoDTO.getCheckFrontImage(), "").equals(""))
		{
			RefreshFrontImage();
			reloadPriviousCheckFront();
		}
		if(!CommonFunction.checkString(generalInfoDTO.getCheckBackImage(), "").equals(""))
		{
			RefreshBackImage();
			reloadPriviousCheckBack();
		}

	}
	public void reloadPriviousCheckFront()
	{
		String imageNames=generalInfoDTO.getCheckFrontImage();

		//Log.e("", "imageNames.."+imageNames);

		if(!CommonFunction.checkString(imageNames, "").equals(""))
		{
			String[] imageArray=imageNames.split(",");
			for(int i=0;i<imageArray.length;i++)
			{
				if(!imageArray[i].equals(""))
				{
					tempStoreFrontImageName.add(imageArray[i]);
					RefreshFrontImage();
				}
			}
		}
	}
	public void reloadPriviousCheckBack()
	{
		String imageNames=generalInfoDTO.getCheckBackImage();

		//Log.e("", "imageNames.."+imageNames);

		if(!CommonFunction.checkString(imageNames, "").equals(""))
		{
			String[] imageArray=imageNames.split(",");
			for(int i=0;i<imageArray.length;i++)
			{
				if(!imageArray[i].equals(""))
				{
					tempStoreBackImageName.add(imageArray[i]);
					RefreshBackImage();
				}
			}
		}
	}
	private void setpreviousRecommendations()
	{
		// TODO Auto-generated method stub
		if(CommonFunction.checkString(residential_pest_DTO.getTreeBranches(),"").equals(""))
		{
			checkBoxBranchHouse.setChecked(false);
		}
		else
		{
			checkBoxBranchHouse.setChecked(true);
		}
		if(CommonFunction.checkString(residential_pest_DTO.getFirewood(),"").equals(""))
		{
			checkBoxFirewood.setChecked(false);
		}
		else
		{
			checkBoxFirewood.setChecked(true);
		}
		if(CommonFunction.checkString(residential_pest_DTO.getDebrisCrawl(),"").equals(""))
		{
			checkBoxDebrisCrawl.setChecked(false);
		}
		else
		{
			checkBoxDebrisCrawl.setChecked(true);
		}
		if(CommonFunction.checkString(residential_pest_DTO.getExcessivePlant(),"").equals(""))
		{
			checkBoxExcessiveplant.setChecked(false);
		}
		else
		{
			checkBoxExcessiveplant.setChecked(true);
		}
		if(CommonFunction.checkString(residential_pest_DTO.getSoil(),"").equals(""))
		{
			checkBoxSoilAbove.setChecked(false);
		}
		else
		{
			checkBoxSoilAbove.setChecked(true);
		}
		if(CommonFunction.checkString(residential_pest_DTO.getWoodSoil(),"").equals(""))
		{
			checkBoxWoodSoil.setChecked(false);
		}
		else
		{
			checkBoxWoodSoil.setChecked(true);
		}
		if(CommonFunction.checkString(residential_pest_DTO.getDebrisRoof(),"").equals(""))
		{
			checkBoxDebrisRoof.setChecked(false);
		}
		else
		{
			checkBoxDebrisRoof.setChecked(true);
		}
		if(CommonFunction.checkString(residential_pest_DTO.getStandingWater(),"").equals(""))
		{
			checkBoxStandingwater.setChecked(false);
		}
		else
		{
			checkBoxStandingwater.setChecked(true);
		}
		if(CommonFunction.checkString(residential_pest_DTO.getMoistureProblem(),"").equals(""))
		{
			checkBoxMoistureProblem.setChecked(false);
		}
		else
		{
			checkBoxMoistureProblem.setChecked(true);
		}
		if(CommonFunction.checkString(residential_pest_DTO.getOpenings(),"").equals(""))
		{
			checkBoxOpeningspumbing.setChecked(false);
		}
		else
		{
			checkBoxOpeningspumbing.setChecked(true);
		}
		if(CommonFunction.checkString(residential_pest_DTO.getExcessiveGaps(),"").equals(""))
		{
			checkBoxExcessiveGaps.setChecked(false);
		}
		else
		{
			checkBoxExcessiveGaps.setChecked(true);
		}
		if(CommonFunction.checkString(residential_pest_DTO.getLeakyPlumbing(),"").equals(""))
		{
			checkBoxLeackPumbing.setChecked(false);
		}
		else
		{
			checkBoxLeackPumbing.setChecked(true);
		}
		if(CommonFunction.checkString(residential_pest_DTO.getGarbageCans(),"").equals(""))
		{
			checkBoxgarbageCans.setChecked(false);
		}
		else
		{
			checkBoxgarbageCans.setChecked(true);
		}
		if(CommonFunction.checkString(residential_pest_DTO.getMoistureDamaged(),"").equals(""))
		{
			checkBoxMoisrtureDamage.setChecked(false);
		}
		else
		{
			checkBoxMoisrtureDamage.setChecked(true);
		}
		if(CommonFunction.checkString(residential_pest_DTO.getGroceryBags(),"").equals(""))
		{
			checkBoxGroceryBags.setChecked(false);
		}
		else
		{
			checkBoxGroceryBags.setChecked(true);
		}
		if(CommonFunction.checkString(residential_pest_DTO.getPetFood(),"").equals(""))
		{
			checkBoxPetFood.setChecked(false);
		}
		else
		{
			checkBoxPetFood.setChecked(true);
		}
		if(CommonFunction.checkString(residential_pest_DTO.getRecycledItems(),"").equals(""))
		{
			checkBoxRecycledItems.setChecked(false);
		}
		else
		{
			checkBoxRecycledItems.setChecked(true);
		}
		if(CommonFunction.checkString(residential_pest_DTO.getExcessiveStorage(),"").equals(""))
		{
			checkBoxExcessiveStorage.setChecked(false);
		}
		else
		{
			checkBoxExcessiveStorage.setChecked(true);
		}
	}
	private void setPreviousMosquitoServices() 
	{
		// TODO Auto-generated method stub
		strMosquito = residential_pest_DTO.getPlusMosquitoDetail();
		ArrayList<String> mosquitoList= new ArrayList<String>(Arrays.asList(strMosquito.split(",")));
		for(int s=0;s<mosquitoList.size();s++)
		{
			for(int c=0;c<mosquitoCheckboxList.size();c++)
			{
				if(mosquitoList.get(s).equals(mosquitoCheckboxList.get(c).getText()))
				{
					mosquitoCheckboxList.get(c).setChecked(true);
				}
			}
		}
		edttxtMosquitoServices1.setText(residential_pest_DTO.getDurationTime());
		edttxtMosquitoServices2.setText(residential_pest_DTO.getSystemRuns());
	}
	private void setPreviousRodentServices() 
	{
		// TODO Auto-generated method stub
		if(residential_pest_DTO.getRoofRodent().equals(checkBoxRoofPitches.getText()))
		{
			checkBoxRoofPitches.setChecked(true);
			//Log.e("", "ROOF PITCHES..."+residential_pest_DTO.getRoofNeeds());
			if(residential_pest_DTO.getRoofNeeds().equals("Yes"))
			{
				radioYesRoofPitches.setChecked(true);
			}
			else
			{
				radioNoRoofPitches.setChecked(true);
			}
		}
		if(residential_pest_DTO.getRoofVentRodent().equals(checkBoxRoofVents.getText()))
		{
			checkBoxRoofVents.setChecked(true);
			if(residential_pest_DTO.getRoofVentNeeds().equals("Yes"))
			{
				radioYesRoofVents.setChecked(true);
			}
			else
			{
				radioNoRoofVents.setChecked(true);
			}
		}
		if(residential_pest_DTO.getFlashing().equals(checkBoxSoffitVents.getText()))
		{
			checkBoxSoffitVents.setChecked(true);
			if(residential_pest_DTO.getFlashingNeeds().equals("Yes"))
			{
				radioYesSoffitVents.setChecked(true);
			}
			else
			{
				radioNoSoffitVents.setChecked(true);
			}
		}
		if(residential_pest_DTO.getFascia().equals(checkBoxWeepHoles.getText()))
		{
			checkBoxWeepHoles.setChecked(true);
			if(residential_pest_DTO.getFasciaNeeds().equals("Yes"))
			{
				radioYesWeepHoles.setChecked(true);
			}
			else
			{
				radioNoWeepHoles.setChecked(true);
			}
		}
		if(residential_pest_DTO.getSiding().equals(checkBoxSidingofHouse.getText()))
		{
			checkBoxSidingofHouse.setChecked(true);
			if(residential_pest_DTO.getSidingNeeds().equals("Yes"))
			{
				radioYesSidingofHouse.setChecked(true);
			}
			else
			{
				radioNoSidingofHouse.setChecked(true);
			}
		}
		if(residential_pest_DTO.getBreezway().equals(checkBoxBreezewayGarage.getText()))
		{
			checkBoxBreezewayGarage.setChecked(true);
			if(residential_pest_DTO.getBreezwayNeeds().equals("Yes"))
			{
				radioYesBreezewayGarage.setChecked(true);
			}
			else
			{
				radioNoBreezewayGarage.setChecked(true);
			}
		}
		if(residential_pest_DTO.getGarageDoor().equals(checkBoxGarageDoor.getText()))
		{
			checkBoxGarageDoor.setChecked(true);
			if(residential_pest_DTO.getGarageDoorNeeds().equals("Yes"))
			{
				radioYesGarageDoor.setChecked(true);
			}
			else
			{
				radioNoGarageDoor.setChecked(true);
			}
		}
		if(residential_pest_DTO.getDryerVent().equals(checkBoxAcDryerVentsPhoneline.getText()))
		{
			checkBoxAcDryerVentsPhoneline.setChecked(true);
			if(residential_pest_DTO.getDryerVentNeeds().equals("Yes"))
			{
				radioYesAcDryerVentsPhoneline.setChecked(true);
			}
			else
			{
				radioNoAcDryerVentsPhoneline.setChecked(true);
			}
		}
		if(residential_pest_DTO.getNumberLive().equals(checkBoxNumberofLiveAnimalTrapsinstalled.getText()))
		{
			checkBoxNumberofLiveAnimalTrapsinstalled.setChecked(true);
			edttxtNumberofLiveAnimalTrapsinstalled.setText(residential_pest_DTO.getNumberLiveNeeds());
		}
		if(residential_pest_DTO.getNumberSnap().equals(checkBoxNumberofSnaptrapsplaced.getText()))
		{
			checkBoxNumberofSnaptrapsplaced.setChecked(true);
			edttxtNumberofSnaptrapsplaced.setText(residential_pest_DTO.getNumberSnapNeeds());
		}
	}
	private void setPreviousTermiteServices() 
	{
		// TODO Auto-generated method stub
		if(residential_pest_DTO.getFencePest().equals(checkBoxFenceInspected.getText()))
		{
			checkBoxFenceInspected.setChecked(true);
			if(residential_pest_DTO.getFenceActivity().equals("Yes"))
			{
				radioYesFenceInspected.setChecked(true);
			}
			else
			{
				radioNoFenceInspected.setChecked(true);
			}
		}

		if(residential_pest_DTO.getNumberPest().equals(checkBoxNumberofMonitoringDevicesInspectedTermiteServices.getText()))
		{
			checkBoxNumberofMonitoringDevicesInspectedTermiteServices.setChecked(true);
			edttxtNumberofMonitoringDevicesInspected.setText(residential_pest_DTO.getNumberActivitytext());
		}

		if(residential_pest_DTO.getWoodPest().equals(checkBoxMonitoringDevicesTermiteServices.getText()))
		{
			checkBoxMonitoringDevicesTermiteServices.setChecked(true);
			if(residential_pest_DTO.getWoodActivity().equals("Yes"))
			{
				radioYesMonitoringDevicesTermiteServices.setChecked(true);
			}
			else
			{
				radioNoMonitoringDevicesTermiteServices.setChecked(true);
			}
		}

		if(residential_pest_DTO.getNumberActivity().equals(checkBoxTermiteMonitoringDevicereplaced.getText()))
		{
			checkBoxTermiteMonitoringDevicereplaced.setChecked(true);
			//Log.e("residential_pest_DTO.getWoodOption()..", "..."+residential_pest_DTO.getWoodOption());

			if(residential_pest_DTO.getWoodOption().equals("Yes"))
			{
				radioYesTermiteMonitoringDevicereplaced.setChecked(true);
			}
			else
			{
				radioNoTermiteMonitoringDevicereplaced.setChecked(true);
			}
		}

		if(residential_pest_DTO.getExteriorPest().equals(checkBoxExteriorPerimeterofhouseTermiteServices.getText()))
		{
			checkBoxExteriorPerimeterofhouseTermiteServices.setChecked(true);
			if(residential_pest_DTO.getExteriorActivity().equals("Yes"))
			{
				radioYesExteriorPerimeterofhouseTermiteServices.setChecked(true);
			}
			else
			{
				radioNoExteriorPerimeterofhouseTermiteServices.setChecked(true);
			}
		}

		if(residential_pest_DTO.getAtticPest().equals(checkBoxAttictermiteServices.getText()))
		{
			checkBoxAttictermiteServices.setChecked(true);
			if(residential_pest_DTO.getAtticActivity().equals("Yes"))
			{
				radioYesAttictermiteServices.setChecked(true);
			}
			else
			{
				radioNoAttictermiteServices.setChecked(true);
			}
		}

		if(residential_pest_DTO.getInteriorPest().equals(checkBoxInteriorOfHousetermiteServices.getText()))
		{
			checkBoxInteriorOfHousetermiteServices.setChecked(true);
			if(residential_pest_DTO.getInteriorActivity().equals("Yes"))
			{
				radioYesInteriorOfHousetermiteServices.setChecked(true);
			}
			else
			{
				radioNoInteriorOfHousetermiteServices.setChecked(true);
			}
		}

	}
	private void setPreviousPestControlServices() 
	{
		// TODO Auto-generated method stub
		strpestControlServices = residential_pest_DTO.getPestControlServices();
		ArrayList<String> pestControlServicesList= new ArrayList<String>(Arrays.asList(strpestControlServices.split(",")));
		for(int s=0;s<pestControlServicesList.size();s++)
		{
			for(int c=0;c<pestControlServicesCheckboxList.size();c++)
			{
				if(pestControlServicesList.get(s).equals(pestControlServicesCheckboxList.get(c).getText()))
				{
					pestControlServicesCheckboxList.get(c).setChecked(true);
				}
			}
		}
	}
	private void setPreviousPestByZone() 
	{
		// TODO Auto-generated method stub
		strpestActivityByZone = residential_pest_DTO.getPestActivity();
		//Log.e("strpestActivityByZone", ""+strpestActivityByZone);

		ArrayList<String>	pestActivityList= new ArrayList<String>(Arrays.asList(strpestActivityByZone.split(",")));
	//	Log.e("pestActivityList.size()", ""+pestActivityList.size());

		for(int s=0;s<pestActivityList.size();s++)
		{
			for(int c=0;c<PestActivityZoneCheckboxList.size();c++)
			{
				if(pestActivityList.get(s).equals(PestActivityZoneCheckboxList.get(c).getText()))
				{
					//Log.e("PestActivityZoneCheckboxList.get(c).getText()", ""+pestActivityList.get(s).equals(PestActivityZoneCheckboxList.get(c).getText()));

					PestActivityZoneCheckboxList.get(c).setChecked(true);
				}
			}
		}
		edttxtFenceLinesByZone.setText(residential_pest_DTO.getFence());
		edttxtBackYardByZone.setText(residential_pest_DTO.getBackYard());

		edttxtFrontyardByZone.setText(residential_pest_DTO.getFrontYard());
		edttxtExteriorPerimeterHouseByZone.setText(residential_pest_DTO.getExteriorPerimeter());

		edttxtRoofGutterLine.setText(residential_pest_DTO.getRoofline());
		edttxtGarageBYZone.setText(residential_pest_DTO.getGarage());

		edttxtAtticByZone.setText(residential_pest_DTO.getAttic());
		edttxtInteriorOfHouseByZone.setText(residential_pest_DTO.getInteriorHouse());
	}
	private void setPreviousOutsideOnly() 
	{
		// TODO Auto-generated method stub
		strOutsideService = residential_pest_DTO.getOutsideOnly();
		if(strOutsideService.equals("true"))
		{
			checkBoxOutsideOnlySerivcePerformed.setChecked(true);    
		}
	}
	private void setPreviousInspectedTreatedFor() 
	{
		// TODO Auto-generated method stub
		strInspectedadnTreatedFor = residential_pest_DTO.getInsectActivity();
		ArrayList<String>	InspectedTreatedList= new ArrayList<String>(Arrays.asList(strInspectedadnTreatedFor.split(",")));
		for(int s=0;s<InspectedTreatedList.size();s++)
		{
			for(int c=0;c<InspectedTreatedForCheckboxList.size();c++)
			{
				if(InspectedTreatedList.get(s).equals(InspectedTreatedForCheckboxList.get(c).getText()))
				{
					InspectedTreatedForCheckboxList.get(c).setChecked(true);
				}
			}
		}
		if(!CommonFunction.checkString(residential_pest_DTO.getAnts(), "").equals(""))
		{	
			edttxtInspectedandTreatedFor1.setText(residential_pest_DTO.getAnts());
			checkBoxAnts.setChecked(true);
		}

		if(!CommonFunction.checkString(residential_pest_DTO.getOtherPest(), "").equals(""))
		{	
			edttxtInspectedandTreatedFor2.setText(residential_pest_DTO.getOtherPest());
			checkBoxOtherPests.setChecked(true);
		}
	}
	private void setPreviousAreaseInspected() 
	{
		// TODO Auto-generated method stub
		strAreasInspected = residential_pest_DTO.getAreasInspected();
		ArrayList<String>	areasInspectedList= new ArrayList<String>(Arrays.asList(strAreasInspected.split(",")));
		for(int s=0;s<areasInspectedList.size();s++)
		{
			for(int c=0;c<AreasInspectedCheckboxList.size();c++)
			{
				if(areasInspectedList.get(s).equals(AreasInspectedCheckboxList.get(c).getText()))
				{
					AreasInspectedCheckboxList.get(c).setChecked(true);
				}
			}
		}
	}
	private void setPreviousServiceForData() 
	{
		strServiceFor = residential_pest_DTO.getServiceFor();
		//	Log.e("", "service for records...."+strServiceFor);
		ArrayList<String>	serviceForList= new ArrayList<String>(Arrays.asList(strServiceFor.split(",")));
		for(int s=0;s<serviceForList.size();s++)
		{
			//Log.e("", "service for records...inside for 1."+strServiceFor);
			for(int c=0;c<serviceForCheckboxList.size();c++)
			{
				//	Log.e("", "service for records...inside for 2."+strServiceFor);
				if(serviceForList.get(s).equals(serviceForCheckboxList.get(c).getText()))
				{
					//Log.e("", "service for records...inside if."+strServiceFor);
					serviceForCheckboxList.get(c).setChecked(true);
				}
			}
		}
	}
	private void initialize() {
		// TODO Auto-generated method stub
		logDTO = new LogDTO();
		userDTO =(UserDTO)this.getIntent().getExtras().getSerializable("userDTO");
		generalInfoDTO =(GeneralInfoDTO)this.getIntent().getExtras().getSerializable("generalInfoDTO");

		//Log.e(userDTO.getEmailid(), userDTO.getEmailid());
		//Log.e(generalInfoDTO.getEmail_Id(), generalInfoDTO.getEmail_Id());

		//Change By Dinesh
		serviceID_new = this.getIntent().getExtras().getString(ParameterUtil.ServiceID_new);

		mainLayout=(LinearLayout)findViewById(R.id.mainLayout);
		imageFrontListView=(HorizontalListView)findViewById(R.id.listImg_front);

		imageFrontListView.setOnItemLongClickListener(new OnGalleryClickFront());
		imageFrontListView.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view,int position, long id)
			{
				Bitmap bmp= listFrontImg.get(position).getImageBitMap();
				if(bmp!=null)
				{
					bitmapFront=bmp;
					Intent intent=new Intent(HR_PestControlServiceReport.this, FullScreenViewActivity.class);
					intent.putExtra("from","frontCheck");
					startActivity(intent);
				}
			}

		});
		imageBackListView=(HorizontalListView)findViewById(R.id.listImg_back);
		imageBackListView.setOnItemLongClickListener(new OnGalleryClickBack());
		imageBackListView.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view,int position, long id)
			{
				Bitmap bmp= listBackImg.get(position).getImageBitMap();
				if(bmp!=null)
				{
					bitmapBack=bmp;
					Intent intent=new Intent(HR_PestControlServiceReport.this, FullScreenViewActivity.class);
					intent.putExtra("from","backCheck");
					startActivity(intent);
				}
			}

		});

		//LinearlayoutCHECK_BACK_IMAGE=(LinearLayout)findViewById(R.id.LinearlayoutCHECK_BACK_IMAGE);
		//LinearlayoutCHECK_FRONT_IMAGE=(LinearLayout)findViewById(R.id.LinearlayoutCHECK_FRONT_IMAGE);
		LinearlayoutCHECK_IMAGE=(LinearLayout)findViewById(R.id.LinearlayoutCHECK_IMAGE);
		btnCHECK_BACK_IMAGE=(Button)findViewById(R.id.btnCHECK_BACK_IMAGE);
		btnCHECK_BACK_IMAGE.setOnClickListener(new OnButtonClick());
		btnCHECK_FRONT_IMAGE=(Button)findViewById(R.id.btnCHECK_FRONT_IMAGE);
		btnCHECK_FRONT_IMAGE.setOnClickListener(new OnButtonClick());
		btnCancel=(Button)findViewById(R.id.btnCancel);
		btnCancel.setOnClickListener(new OnButtonClick());
		btnSave=(Button)findViewById(R.id.btnsaveandcontinueINVOICE);
		btnTermiteDisclosure=(Button)findViewById(R.id.btnTermiteDisclosure);
		btnSave.setOnClickListener(new OnButtonClick()); 
		btnTermiteDisclosure.setOnClickListener(new OnButtonClick());
		btnBack=(Button)findViewById(R.id.btnBack);
		btnBack.setOnClickListener(new OnButtonClick());

		residential_pest_DTO=new HResidential_pest_DTO();
		//generalInfoDTO= new GeneralInfoDTO();
		//userDTO = new UserDTO();
		//chemicalDto=new ChemicalDto();
		houstonFlowFunctions=new HoustonFlowFunctions(getApplicationContext());
		txtName=(TextView)findViewById(R.id.txtName);
		//txtAccountNumber=(TextView)findViewById(R.id.txtAcctNo);
		txtTimeIn=(TextView)findViewById(R.id.txtTimeIn);
		txtOrderNumber=(TextView)findViewById(R.id.txtOrderNumber);
		trcheckno=(TableRow)findViewById(R.id.trcheckno);
		tramount=(TableRow)findViewById(R.id.tramount);
		trDrivingLicence=(TableRow)findViewById(R.id.trDrivingLicence);
		trExpirationDate=(TableRow)findViewById(R.id.trExpirationDate);
		trExpirationDateValidation=(LinearLayout)findViewById(R.id.trExpirationDateValidation);
		//CHECKBOX INITIALIZATION
		checkBoxSignaturePestControl = (CheckBox)findViewById(R.id.checkboxSignaturePestcontrol);
		checkBoxSignaturePestControl.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxPlusTermites =(CheckBox)findViewById(R.id.checkboxPlusTermites);
		checkBoxPlusTermites.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxPlusMosquitoMistingSystem =(CheckBox)findViewById(R.id.checkboxPlusMosquitomistingSystem);
		checkBoxPlusMosquitoMistingSystem.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxPlusfireAnts =(CheckBox)findViewById(R.id.checkboxPlusFireAnts);
		checkBoxPlusfireAnts.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxPlusRodents =(CheckBox)findViewById(R.id.checkboxPluSRodents);
		checkBoxPlusRodents.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxPlusFleas =(CheckBox)findViewById(R.id.checkboxPlusFleas);
		checkBoxPlusFleas.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxPlusFleas =(CheckBox)findViewById(R.id.checkboxPlusFleas);
		checkBoxPlusFleas.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTermitetreatment =(CheckBox)findViewById(R.id.checkboxTermiteTreatment);
		checkBoxTermitetreatment.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxRodentExclusion =(CheckBox)findViewById(R.id.checkboxRodentExclusion);
		checkBoxRodentExclusion.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxBedBudTreatment =(CheckBox)findViewById(R.id.checkboxBedBugTreatment);
		checkBoxBedBudTreatment.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxMoaquitoSupression =(CheckBox)findViewById(R.id.checkBoxMosquitoSuppression);
		checkBoxMoaquitoSupression.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxExteriorPerimeterHouseAreasInspected =(CheckBox)findViewById(R.id.checkboxEXTERIOR_PERIMRTER_OF_HOUSE);
		checkBoxExteriorPerimeterHouseAreasInspected.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxFrontyardAreasInspected =(CheckBox)findViewById(R.id.checkboxFRONTYARDAreasInspected);
		checkBoxFrontyardAreasInspected.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxBackYardAreasInspected =(CheckBox)findViewById(R.id.checkboxBackYardAreasInspected);
		checkBoxBackYardAreasInspected.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxExteriorPerimeterGarage =(CheckBox)findViewById(R.id.checkboxExteriorPerimeterofGarage);
		checkBoxExteriorPerimeterGarage.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxOutBuildings =(CheckBox)findViewById(R.id.checkboxOutBuildings);
		checkBoxOutBuildings.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxFenceLinesAreasInspected =(CheckBox)findViewById(R.id.checkboxFenceLinesAreasInspected);
		checkboxFenceLinesAreasInspected.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxInteriorOfHouseAreasInspected =(CheckBox)findViewById(R.id.checkboxInteriorHouseAreasInspected);
		checkBoxInteriorOfHouseAreasInspected.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxExteriorHouseAreasInspected =(CheckBox)findViewById(R.id.checkboxExteriorHouseAreasInspected);
		checkBoxExteriorHouseAreasInspected.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxAtticAreasInspected =(CheckBox)findViewById(R.id.checkboxAtticAreasinspected);
		checkBoxAtticAreasInspected.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxRoaches =(CheckBox)findViewById(R.id.checkboxRoaches);
		checkBoxRoaches.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxSpiders =(CheckBox)findViewById(R.id.checkboxSpiders);
		checkBoxSpiders.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxScorpions =(CheckBox)findViewById(R.id.checkboxScorpions);
		checkBoxScorpions.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxSilverFish=(CheckBox)findViewById(R.id.checkboxSilverfish);
		checkBoxSilverFish.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxPantryPests=(CheckBox)findViewById(R.id.checkboxPANTRY_PESTS);
		checkBoxPantryPests.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxPharaohAnts=(CheckBox)findViewById(R.id.checkboxPHARAOH_ANTS);
		checkBoxPharaohAnts.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxFireAnts=(CheckBox)findViewById(R.id.checkboxFIRE_ANTS);
		checkBoxFireAnts.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxCarepenterAnts=(CheckBox)findViewById(R.id.checkboxCARPENTER_ANTS);
		checkBoxCarepenterAnts.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxMillipedes=(CheckBox)findViewById(R.id.checkboxMILLIPEDES);
		checkBoxMillipedes.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxEarWigs=(CheckBox)findViewById(R.id.checkboxEARWIGS);
		checkBoxEarWigs.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxPillBugs=(CheckBox)findViewById(R.id.checkboxPILL_BUGS);
		checkBoxPillBugs.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxCrickets=(CheckBox)findViewById(R.id.checkboxCRICKETS);
		checkboxCrickets.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxWasps=(CheckBox)findViewById(R.id.checkboxWASPS);
		checkboxWasps.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxSubTermities=(CheckBox)findViewById(R.id.checkboxSUB_TERMITIES);
		checkboxSubTermities.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxDryWoodTermities=(CheckBox)findViewById(R.id.checkboxDRYWOOD_TERMITIES);
		checkboxDryWoodTermities.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxRoofRats=(CheckBox)findViewById(R.id.checkboxROOF_RATS);
		checkboxRoofRats.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxMice=(CheckBox)findViewById(R.id.checkboxMICE);
		checkboxMice.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxRacoons=(CheckBox)findViewById(R.id.checkboxRACCOONS);
		checkboxRacoons.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxOppsumss=(CheckBox)findViewById(R.id.checkboxOPOSSUMS);
		checkBoxOppsumss.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxSkunks=(CheckBox)findViewById(R.id.checkboxSKUNKS);
		checkBoxSkunks.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxSqirrel=(CheckBox)findViewById(R.id.checkboxSquirrels);
		checkBoxSqirrel.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTicks=(CheckBox)findViewById(R.id.checkboxTicks);
		checkBoxTicks.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxBedBugs=(CheckBox)findViewById(R.id.checkboxBEDBUGS);
		checkBoxBedBugs.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTwanyCrazyAnts=(CheckBox)findViewById(R.id.checkboxTawnyCrazyAnts);
		checkBoxTwanyCrazyAnts.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxAnts=(CheckBox)findViewById(R.id.checkBoxANTS);
		checkBoxAnts.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxOtherPests=(CheckBox)findViewById(R.id.checkBoxOTHERPESTS);
		checkBoxOtherPests.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxOutsideOnlySerivcePerformed=(CheckBox)findViewById(R.id.or_checkboxOutSideService);
		checkBoxOutsideOnlySerivcePerformed.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxFrontyardByZone=(CheckBox)findViewById(R.id.checkBoxFrontyardByZOne);
		checkBoxFrontyardByZone.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxBackYardByZone=(CheckBox)findViewById(R.id.checkBoxBackYardByZone);
		checkBoxBackYardByZone.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxExteriorPerimeterHouseByZone=(CheckBox)findViewById(R.id.checkBoxExteriorParametresofhouseByZone);
		checkBoxExteriorPerimeterHouseByZone.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxRoofLine=(CheckBox)findViewById(R.id.checkBoxRofGutter);
		checkBoxRoofLine.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxGarageBYZone=(CheckBox)findViewById(R.id.checkBoxGarage);
		checkBoxGarageBYZone.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxAtticByZone=(CheckBox)findViewById(R.id.checkBoxAtticByZone);
		checkBoxAtticByZone.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxInteriorOfHouseByZone=(CheckBox)findViewById(R.id.checkBoxInteriorHouse);
		checkBoxInteriorOfHouseByZone.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxFenceLines=(CheckBox)findViewById(R.id.checkBoxFenceLines);
		checkBoxFenceLines.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		//for pest control..
		checkBoxExteriorPestControl=(CheckBox)findViewById(R.id.checkBoxEXTERIOR);
		checkBoxExteriorPestControl.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxInteriorPestControl=(CheckBox)findViewById(R.id.checkBoxINTERIOR);
		checkBoxInteriorPestControl.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTreatedFrontPerimeter=(CheckBox)findViewById(R.id.checkBoxTreatedFront);
		checkBoxTreatedFrontPerimeter.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTreatedbackPerimeter=(CheckBox)findViewById(R.id.checkBoxTreatedback);
		checkBoxTreatedbackPerimeter.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTreatedInspectedAttic=(CheckBox)findViewById(R.id.checkBoxTreatedAcct);
		checkBoxTreatedInspectedAttic.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTreatedInspectedKitchen=(CheckBox)findViewById(R.id.checkBoxTreatedKitchen);
		checkBoxTreatedInspectedKitchen.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTreatedWhepholes=(CheckBox)findViewById(R.id.checkBoxTreatedWhepholes);
		checkBoxTreatedWhepholes.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTreatedInspectedLaundryRooms=(CheckBox)findViewById(R.id.checkBoxTreatedLaundry);
		checkBoxTreatedInspectedLaundryRooms.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTreatedCracksCreves=(CheckBox)findViewById(R.id.checkBoxTreatedCracks);
		checkBoxTreatedCracksCreves.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTreatedInpectedlivingRooms=(CheckBox)findViewById(R.id.checkBoxLivingRooms);
		checkBoxTreatedInpectedlivingRooms.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTreatedYardHaouborageBreedingSites=(CheckBox)findViewById(R.id.checkBoxtreatedyards);
		checkBoxTreatedYardHaouborageBreedingSites.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTreatedInspectedOtherLivingAreas=(CheckBox)findViewById(R.id.checkBoxTreatedliving);
		checkBoxTreatedInspectedOtherLivingAreas.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTreatedFireAntsInYards=(CheckBox)findViewById(R.id.checkBoxTreatedfirenets);
		checkBoxTreatedFireAntsInYards.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxInspectedReplacedZoneMonitors=(CheckBox)findViewById(R.id.checkBoxTreatedZoneMonitors);
		checkBoxInspectedReplacedZoneMonitors.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTreatedInspectedDooeWaysWindows=(CheckBox)findViewById(R.id.checkBoxTreatedDoorways);
		checkBoxTreatedInspectedDooeWaysWindows.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTreatedInspectedBedroomsBathrooms=(CheckBox)findViewById(R.id.checkBoxTreatedBedrooms);
		checkBoxTreatedInspectedBedroomsBathrooms.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		//for termite
		checkBoxFenceInspected=(CheckBox)findViewById(R.id.checkBoxFenceInspected);
		checkBoxFenceInspected.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxNumberofMonitoringDevicesInspectedTermiteServices=(CheckBox)findViewById(R.id.checkBoxNumberMonitoringDevicesInspected);
		checkBoxNumberofMonitoringDevicesInspectedTermiteServices.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxMonitoringDevicesTermiteServices=(CheckBox)findViewById(R.id.checkBoxMonitoringDevices);
		checkBoxMonitoringDevicesTermiteServices.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTermiteMonitoringDevicereplaced=(CheckBox)findViewById(R.id.checkBoxTermiteMonitoringDevicereplaced);
		checkBoxTermiteMonitoringDevicereplaced.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxExteriorPerimeterofhouseTermiteServices=(CheckBox)findViewById(R.id.checkBoxExteriorPerimeterofhouseTermite);
		checkBoxExteriorPerimeterofhouseTermiteServices.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxAttictermiteServices=(CheckBox)findViewById(R.id.checkBoxATTICTermite);
		checkBoxAttictermiteServices.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxInteriorOfHousetermiteServices=(CheckBox)findViewById(R.id.checkBoxINTERIOROFHOUSETermite);
		checkBoxInteriorOfHousetermiteServices.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		//for rodent

		checkBoxRoofPitches=(CheckBox)findViewById(R.id.checkBoxROOF_PITCHES);
		checkBoxRoofPitches.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxRoofVents=(CheckBox)findViewById(R.id.checkBoxROOF_VENTS);
		checkBoxRoofVents.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxSoffitVents=(CheckBox)findViewById(R.id.checkBoxSOFFIT_VENTS);
		checkBoxSoffitVents.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxWeepHoles=(CheckBox)findViewById(R.id.checkBoxWEEPHOLES);
		checkBoxWeepHoles.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxSidingofHouse=(CheckBox)findViewById(R.id.checkBoxSIDEING_HOUSE);
		checkBoxSidingofHouse.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxBreezewayGarage=(CheckBox)findViewById(R.id.checkBoxBREEZYWAY_FROM_GARAGE);
		checkBoxBreezewayGarage.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxGarageDoor=(CheckBox)findViewById(R.id.checkBoxGARAGE_DOOR);
		checkBoxGarageDoor.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxAcDryerVentsPhoneline=(CheckBox)findViewById(R.id.checkBoxAC_DRYER_VENTS);
		checkBoxAcDryerVentsPhoneline.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxNumberofLiveAnimalTrapsinstalled=(CheckBox)findViewById(R.id.checkBoxNO_LIVEANIMAL_TRAPS_INSTALLES);
		checkBoxNumberofLiveAnimalTrapsinstalled.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxNumberofSnaptrapsplaced=(CheckBox)findViewById(R.id.checkBoxNumber_SNAPTRAPS);
		checkBoxNumberofSnaptrapsplaced.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		//for mosquito
		checkBoxToppedOfftank=(CheckBox)findViewById(R.id.checkBoxTopped_TANK);
		checkBoxToppedOfftank.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxLeadsAtTank=(CheckBox)findViewById(R.id.checkBoxLEACKY_TANK);
		checkBoxLeadsAtTank.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTubingLeaking=(CheckBox)findViewById(R.id.checkBoxTUBING_LEACKY);
		checkBoxTubingLeaking.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxInspectedNozzles=(CheckBox)findViewById(R.id.checkBoxINSPECTED_NOZZLES);
		checkBoxInspectedNozzles.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxRanSystem=(CheckBox)findViewById(R.id.checkBoxRAN_SYATEMS);
		checkBoxRanSystem.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxCheckedRunDurationTime=(CheckBox)findViewById(R.id.checkBoxCHECKED_RUN_DURACTION_TIME);
		checkBoxCheckedRunDurationTime.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxNozzlesLeaking=(CheckBox)findViewById(R.id.checkBoxNOZZLES_LEAKING);
		checkBoxNozzlesLeaking.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxCheckedTimeSystemRuns=(CheckBox)findViewById(R.id.checkBoxCHECKED_TIME_SYSTEM_RUNS);
		checkBoxCheckedTimeSystemRuns.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		// for recommendations
		checkBoxExcessiveStorage=(CheckBox)findViewById(R.id.checkBoxExcessiveStorage);
		checkBoxExcessiveStorage.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		checkBoxRecycledItems=(CheckBox)findViewById(R.id.checkBoxRecycledItems);
		checkBoxRecycledItems.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		checkBoxPetFood=(CheckBox)findViewById(R.id.checkBoxPetFood);
		checkBoxPetFood.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		checkBoxGroceryBags=(CheckBox)findViewById(R.id.checkBoxGroceryBags);
		checkBoxGroceryBags.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		checkBoxMoisrtureDamage=(CheckBox)findViewById(R.id.checkBoxMoisrtureDamage);
		checkBoxMoisrtureDamage.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		checkBoxgarbageCans=(CheckBox)findViewById(R.id.checkBoxgarbageCans);
		checkBoxgarbageCans.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		checkBoxLeackPumbing=(CheckBox)findViewById(R.id.checkBoxLeackPumbing);
		checkBoxLeackPumbing.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		checkBoxExcessiveGaps=(CheckBox)findViewById(R.id.checkBoxExcessiveGaps);
		checkBoxExcessiveGaps.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		checkBoxWoodSoil=(CheckBox)findViewById(R.id.checkBoxWoodSoil);
		checkBoxWoodSoil.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		checkBoxDebrisRoof=(CheckBox)findViewById(R.id.checkBoxDebrisRoof);
		checkBoxDebrisRoof.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		checkBoxStandingwater=(CheckBox)findViewById(R.id.checkBoxStandingwater);
		checkBoxStandingwater.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		checkBoxMoistureProblem=(CheckBox)findViewById(R.id.checkBoxMoistureProblem);
		checkBoxMoistureProblem.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		checkBoxOpeningspumbing=(CheckBox)findViewById(R.id.checkBoxOpeningspumbing);
		checkBoxOpeningspumbing.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		checkBoxBranchHouse=(CheckBox)findViewById(R.id.checkBoxBranchHouse);
		checkBoxFirewood=(CheckBox)findViewById(R.id.checkBoxFirewood);
		checkBoxFirewood.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		checkBoxDebrisCrawl=(CheckBox)findViewById(R.id.checkBoxDebrisCrawl);
		checkBoxDebrisCrawl.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		checkBoxExcessiveplant=(CheckBox)findViewById(R.id.checkBoxExcessiveplant);
		checkBoxExcessiveplant.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		checkBoxSoilAbove=(CheckBox)findViewById(R.id.checkBoxSoilAbove);
		checkBoxSoilAbove.setOnCheckedChangeListener(new OnCheckedboxChangeListener());


		//radio button and group
		//RADIO BUTTON INITIALIZATION
		radioGroupInvoice=(RadioGroup)findViewById(R.id.radioGroupInvoice);
		radioGroupInvoice.setOnCheckedChangeListener(new OnRadioChange());

		radioCash=(RadioButton)findViewById(R.id.radioCash);
		radioCheck=(RadioButton)findViewById(R.id.radioCheck);
		radioCreditcard=(RadioButton)findViewById(R.id.radioCreditcard);
		radioNoCharge=(RadioButton)findViewById(R.id.radioNocharge);
		radioBillLater=(RadioButton)findViewById(R.id.radioBillLater);
		radioPreBill=(RadioButton)findViewById(R.id.radioPreBill);

		//for termite
		radioGroupFenceInspected=(RadioGroup)findViewById(R.id.radioGroupFenceInspected);
		radioGroupFenceInspected.setOnCheckedChangeListener(new OnRadioChange());

		radioGroupMonitoringDevices=(RadioGroup)findViewById(R.id.radioGroupMonitoringdevices);
		radioGroupMonitoringDevices.setOnCheckedChangeListener(new OnRadioChange());

		radioGroupTermiteMonitoringDevices=(RadioGroup)findViewById(R.id.radioGroupREPLACED);
		radioGroupTermiteMonitoringDevices.setOnCheckedChangeListener(new OnRadioChange());


		radioGroupExteriorPerimeterOfHouse=(RadioGroup)findViewById(R.id.radioGroupExteriorPerimeterofhouse);
		radioGroupExteriorPerimeterOfHouse.setOnCheckedChangeListener(new OnRadioChange());

		radioGroupAttic=(RadioGroup)findViewById(R.id.radioGroupAttic);
		radioGroupAttic.setOnCheckedChangeListener(new OnRadioChange());

		radioGroupInteriorHouse=(RadioGroup)findViewById(R.id.radioGroupINTERIOROFHOUSE);
		radioGroupInteriorHouse.setOnCheckedChangeListener(new OnRadioChange());

		radioYesFenceInspected=(RadioButton)findViewById(R.id.radiobtnFenceInspected1);
		radioYesMonitoringDevicesTermiteServices=(RadioButton)findViewById(R.id.radiobtnMonitorindDevices1);
		radioYesTermiteMonitoringDevicereplaced=(RadioButton)findViewById(R.id.radiobtnREPLACED1);
		radioYesExteriorPerimeterofhouseTermiteServices=(RadioButton)findViewById(R.id.radiobtnExteriorPerimeterofhouse1);
		radioYesAttictermiteServices=(RadioButton)findViewById(R.id.radiobtnAttic1);
		radioYesInteriorOfHousetermiteServices=(RadioButton)findViewById(R.id.radiobtnINTERIOROFHOUSE1);

		radioNoFenceInspected=(RadioButton)findViewById(R.id.radiobtnFenceInspected2);
		radioNoMonitoringDevicesTermiteServices=(RadioButton)findViewById(R.id.radiobtnMonitorindDevices2);
		radioNoTermiteMonitoringDevicereplaced=(RadioButton)findViewById(R.id.radiobtnREPLACES2);
		radioNoExteriorPerimeterofhouseTermiteServices=(RadioButton)findViewById(R.id.radiobtnExteriorPerimeterofhouse2);
		radioNoAttictermiteServices=(RadioButton)findViewById(R.id.radiobtnAttic2);
		radioNoInteriorOfHousetermiteServices=(RadioButton)findViewById(R.id.radiobtnINTERIOROFHOUSE2);


		//for rodent services
		radioGroupRoofPitches=(RadioGroup)findViewById(R.id.radioGroupROOF_PITCHES);
		radioGroupRoofPitches.setOnCheckedChangeListener(new OnRadioChange());

		radioGroupRoofVents=(RadioGroup)findViewById(R.id.radioGroupROOF_VENTS);
		radioGroupRoofVents.setOnCheckedChangeListener(new OnRadioChange());

		radioGroupSoffitvents=(RadioGroup)findViewById(R.id.radioGroupSOFFIT_VENTS);
		radioGroupSoffitvents.setOnCheckedChangeListener(new OnRadioChange());

		radioGroupWeepHoles=(RadioGroup)findViewById(R.id.radioGroupWEEPHOLES);
		radioGroupWeepHoles.setOnCheckedChangeListener(new OnRadioChange());

		radioGroupSidingHouse=(RadioGroup)findViewById(R.id.radioGroupSIDEING_HOUSE);
		radioGroupSidingHouse.setOnCheckedChangeListener(new OnRadioChange());

		radioGroupBreezyWay=(RadioGroup)findViewById(R.id.radioGroupBREEZYWAY_FROM_GARAGE);
		radioGroupBreezyWay.setOnCheckedChangeListener(new OnRadioChange());

		radioGroupGarageDoor=(RadioGroup)findViewById(R.id.radioGroupGARAGE_DOOR);
		radioGroupGarageDoor.setOnCheckedChangeListener(new OnRadioChange());

		radioGroupACDRYER=(RadioGroup)findViewById(R.id.radioGroupAC_DRYER_VENTS);
		radioGroupACDRYER.setOnCheckedChangeListener(new OnRadioChange());



		radioYesRoofPitches=(RadioButton)findViewById(R.id.radiobtnROOF_PITCHES1);
		radioYesRoofVents=(RadioButton)findViewById(R.id.radiobtnROOF_VENTS1);
		radioYesSoffitVents=(RadioButton)findViewById(R.id.radiobtnSOFFIT_VENTS1);
		radioYesWeepHoles=(RadioButton)findViewById(R.id.radiobtnWEEPHOLES1);
		radioYesSidingofHouse=(RadioButton)findViewById(R.id.radiobtnSIDEING_HOUSE1);
		radioYesBreezewayGarage=(RadioButton)findViewById(R.id.radiobtnBREEZYWAY_FROM_GARAGE1);
		radioYesGarageDoor=(RadioButton)findViewById(R.id.radiobtnGARAGE_DOOR1);
		radioYesAcDryerVentsPhoneline=(RadioButton)findViewById(R.id.radiobtnAC_DRYER_VENTSR1);
		//radioYesNumberofSnaptrapsplaced=(RadioButton)findViewById(R.id.radiobtnNO_SNAPTRAPS1);

		radioNoRoofPitches=(RadioButton)findViewById(R.id.radiobtnROOF_PITCHES2);
		radioNoRoofVents=(RadioButton)findViewById(R.id.radiobtnROOF_VENTS2);
		radioNoSoffitVents=(RadioButton)findViewById(R.id.radiobtnSOFFIT_VENTS2);
		radioNoWeepHoles=(RadioButton)findViewById(R.id.radiobtnWEEPHOLES2);
		radioNoSidingofHouse=(RadioButton)findViewById(R.id.radiobtnSIDEING_HOUSE2);
		radioNoBreezewayGarage=(RadioButton)findViewById(R.id.radiobtnBREEZYWAY_FROM_GARAGE2);
		radioNoGarageDoor=(RadioButton)findViewById(R.id.radiobtnGARAGE_DOOR2);
		radioNoAcDryerVentsPhoneline=(RadioButton)findViewById(R.id.radiobtnAC_DRYER_VENTSR2);

		// edit text initialization
		edttxtInspectedandTreatedFor1=(EditText)findViewById(R.id.edtiNSPECTEDANDtREATEDFOR1);
		edttxtInspectedandTreatedFor1.addTextChangedListener(new CustomTextWatcher(edttxtInspectedandTreatedFor1));
		edttxtInspectedandTreatedFor1.setFilters(CommonFunction.limitchars(20));

		edttxtInspectedandTreatedFor2=(EditText)findViewById(R.id.edtiNSPECTEDANDtREATEDFOR2);
		edttxtInspectedandTreatedFor2.addTextChangedListener(new CustomTextWatcher(edttxtInspectedandTreatedFor2));
		edttxtInspectedandTreatedFor2.setFilters(CommonFunction.limitchars(20));

		edttxtNumberofMonitoringDevicesInspected=(EditText)findViewById(R.id.edtNUMBER_OF_MONITERING_DEVICES_INSPECTED);
		edttxtNumberofMonitoringDevicesInspected.addTextChangedListener(new CustomTextWatcher(edttxtNumberofMonitoringDevicesInspected));
		
		// for pest activity by zone
		textviewCharactersTyped = (TextView) findViewById(R.id.textviewCharactersTyped);
		
		edttxtFenceLinesByZone=(EditText)findViewById(R.id.edtFenceLines);
		edttxtFenceLinesByZone.addTextChangedListener(new CustomTextWatcher(edttxtFenceLinesByZone));
		edttxtFenceLinesByZone.setFilters(CommonFunction.limitchars(50));

		edttxtFrontyardByZone=(EditText)findViewById(R.id.edtFrontYard);
		edttxtFrontyardByZone.addTextChangedListener(new CustomTextWatcher(edttxtFrontyardByZone));
		edttxtFrontyardByZone.setFilters(CommonFunction.limitchars(50));

		edttxtBackYardByZone=(EditText)findViewById(R.id.edtBackyard);
		edttxtBackYardByZone.addTextChangedListener(new CustomTextWatcher(edttxtBackYardByZone));
		edttxtBackYardByZone.setFilters(CommonFunction.limitchars(50));

		edttxtExteriorPerimeterHouseByZone=(EditText)findViewById(R.id.edtExteriorPara);
		edttxtExteriorPerimeterHouseByZone.addTextChangedListener(new CustomTextWatcher(edttxtExteriorPerimeterHouseByZone));
		edttxtExteriorPerimeterHouseByZone.setFilters(CommonFunction.limitchars(50));

		edttxtRoofGutterLine=(EditText)findViewById(R.id.edtRoofGutter);
		edttxtRoofGutterLine.addTextChangedListener(new CustomTextWatcher(edttxtRoofGutterLine));
		edttxtRoofGutterLine.setFilters(CommonFunction.limitchars(50));

		edttxtGarageBYZone=(EditText)findViewById(R.id.edtGarage);
		edttxtGarageBYZone.addTextChangedListener(new CustomTextWatcher(edttxtGarageBYZone));
		edttxtGarageBYZone.setFilters(CommonFunction.limitchars(50));

		edttxtAtticByZone=(EditText)findViewById(R.id.edtAtticPestbyZone);
		edttxtAtticByZone.addTextChangedListener(new CustomTextWatcher(edttxtAtticByZone));
		edttxtAtticByZone.setFilters(CommonFunction.limitchars(50));

		edttxtInteriorOfHouseByZone=(EditText)findViewById(R.id.edtInteriorHouse);
		edttxtInteriorOfHouseByZone.addTextChangedListener(new CustomTextWatcher(edttxtInteriorOfHouseByZone));
		edttxtInteriorOfHouseByZone.setFilters(CommonFunction.limitchars(50));
		//edttxtShedByZone=(EditText)findViewById(R.id.edtShed);
		//edttxtOtherByZone=(EditText)findViewById(R.id.edtO);
		//

		//for terimte
		edttxtMonitorReplaced = (EditText)findViewById(R.id.edtNUMBER_OF_MONITERING_DEVICES_INSPECTED);

		// for rodent services
		edttxtNumberofLiveAnimalTrapsinstalled=(EditText)findViewById(R.id.edtNO_LIVEANIMAL_TRAPS_INSTALLES);
		edttxtNumberofLiveAnimalTrapsinstalled.addTextChangedListener(new CustomTextWatcher(edttxtNumberofLiveAnimalTrapsinstalled));

		edttxtNumberofSnaptrapsplaced=(EditText)findViewById(R.id.edtNumber_SNAPTRAPS);
		edttxtNumberofSnaptrapsplaced.addTextChangedListener(new CustomTextWatcher(edttxtNumberofSnaptrapsplaced));

		//mosquito
		edttxtMosquitoServices1=(EditText)findViewById(R.id.edtMosquitoService1);
		edttxtMosquitoServices1.addTextChangedListener(new CustomTextWatcher(edttxtMosquitoServices1));

		edttxtMosquitoServices2=(EditText)findViewById(R.id.edtMosquitoService2);
		edttxtMosquitoServices2.addTextChangedListener(new CustomTextWatcher(edttxtMosquitoServices2));

		//invoice page
		edttxtInvValue=(EditText)findViewById(R.id.edtIntValue);
		edttxtProdValue=(EditText)findViewById(R.id.edtProductvalue);
		edttxtTax=(EditText)findViewById(R.id.edtTaxvalue);
		edttxtTotal=(EditText)findViewById(R.id.edtTotal);
		
		edttxtTechComments=(EditText)findViewById(R.id.edtTechComment);
		edttxtTechComments.addTextChangedListener(new CustomTextWatcher(edttxtTechComments));
		edttxtTechComments.setFilters(CommonFunction.limitchars(400));
		
		edttxtServiceTech=(EditText)findViewById(R.id.edtServiceTech);
		edttxtServiceTech.setFilters(CommonFunction.limitchars(50));
		edttxtEmp=(EditText)findViewById(R.id.edtEMP);
		edttxtEmp.setFilters(CommonFunction.limitchars(50));
		edttxtCustomer=(EditText)findViewById(R.id.edtCustomer);
		edttxtCustomer.setEnabled(false);

		edttxtCustomer.setFilters(CommonFunction.limitchars(50));
		
		edttxtExpirationDate=(EditText)findViewById(R.id.edtEXPIRATION_DATE);
		edttxtExpirationDate.setInputType(InputType.TYPE_NULL);
		edttxtExpirationDate.setOnClickListener(new OnButtonClick());
		edttxtdate=(EditText)findViewById(R.id.edtDate);
		edttxtdate.setInputType(InputType.TYPE_NULL);
		edttxtdate.setOnClickListener(new OnButtonClick());
		edtxtTimeIn=(EditText)findViewById(R.id.edtTimeIn);
		edttxtAmount=(EditText)findViewById(R.id.edtAmount);
		edttxtAmount.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(8,2)});
		edttxtCheckNo=(EditText)findViewById(R.id.edtCheckNo);
		edttxtCheckNo.setFilters(CommonFunction.limitchars(20));
		edttxtDrivingLicence=(EditText)findViewById(R.id.edtDRIVING_LICENCE);
		edttxtDrivingLicence.setFilters(CommonFunction.limitchars(50));
		txtAccountNumber=(TextView)findViewById(R.id.txtAccountNumber);
		txtTechnicianName=(TextView)findViewById(R.id.txtTech_Name);
	}
	
	class OnGalleryClickFront implements OnItemLongClickListener 
	{
		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
				int position, long arg3) 
		{
			if(generalInfoDTO.getServiceStatus().equals("InComplete"))
			{
				if(listFrontImg.size()>0)
				{
					listFrontImg.remove(position);
					if(tempFrontImagList.size()>0)
					{
						tempFrontImagList.remove(position);
					}
					if(tempStoreFrontImageName.size()>0)
					{
						tempStoreFrontImageName.remove(position);
					}
					RefreshFrontImage();
					listFrontImg.clear();
					generalInfoDTO.setCheckFrontImage("");
				}
			}
			return false;
		}
	}


	class OnGalleryClickBack implements OnItemLongClickListener 
	{
		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
				int position, long arg3) 
		{
			if(generalInfoDTO.getServiceStatus().equals("InComplete"))
			{
				if(listBackImg.size()>0)
				{
					listBackImg.remove(position);
					if(tempBackImagList.size()>0)
					{
						tempBackImagList.remove(position);

					}
					if(tempStoreBackImageName.size()>0)
					{
						tempStoreBackImageName.remove(position);
					}
					RefreshBackImage();
					listBackImg.clear();
					generalInfoDTO.setCheckBackImage("");
				}
			}
			return false;
		}
	}
	private void RefreshBackImage()
	{
		try
		{ 
			listBackImg.clear();
			Bitmap bmp = null;
			for (int i = 0; i < tempStoreBackImageName.size(); i++) {
				bmp = null;
				ImgDto dto = new ImgDto();
				try 
				{
					LoggerUtil.e("tempStoreBackImageName in refresh",tempStoreBackImageName.get(i));
					/*	bmp = BitmapFactory.decodeStream(getContentResolver()
						.openInputStream(Uri.fromFile(CommanFunction.getFileLocation(IWitnessActivity.this,tempStoreFrontImageName.get(i)))));
					 */    
					bmp=decodeSmallUri(Uri.fromFile(CommonFunction.getFileLocation(HR_PestControlServiceReport.this,tempStoreBackImageName.get(i))));

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Bitmap bitmap = scaleBitmap(bmp, 100, 100);
				dto.setImageBitMap(bitmap);
				if (!tempBackImagList.contains(tempStoreBackImageName.get(i))) {

					if (bmp != null) 
					{

						//CompressImage(bmp);
					}
				}// dto.setImageUri(Uri.fromFile(CommanFunction.getFileLocation(IWitnessActivity.this,
				// tempStoreFrontImageName.get(i))));

				tempBackImagList.add(tempStoreBackImageName.get(i));
				listBackImg.add(dto);
			}

			//LoggerUtil.e("List Size", listImg.size() + "");

			/*if (listBackImg.size() == 0) {
				ImgDto dto = new ImgDto();
				dto.setImagedrawable(R.drawable.noimage);
				listBackImg.add(dto);
			}else
			{

			}*/
			adapterBack=new GalleryImageAdapter(getApplicationContext(), listBackImg) ;
			imageBackListView.setAdapter(adapterBack);
			//adapter.notifyDataSetChanged(); // TODO Auto-generated method stub
		}
		catch (Exception e) 
		{
			LoggerUtil.e("Exception in Refresh Image", e+"");
		}
	}
	private void RefreshFrontImage()
	{
		try
		{ 
			listFrontImg.clear();
			Bitmap bmp = null;
			for (int i = 0; i < tempStoreFrontImageName.size(); i++) {
				bmp = null;
				ImgDto dto = new ImgDto();
				try {
					LoggerUtil.e("tempStoreFrontImageName in refresh",tempStoreFrontImageName.get(i));
					/*	bmp = BitmapFactory.decodeStream(getContentResolver()
						.openInputStream(Uri.fromFile(CommanFunction.getFileLocation(IWitnessActivity.this,tempStoreFrontImageName.get(i)))));
					 */    
					bmp=decodeSmallUri(Uri.fromFile(CommonFunction.getFileLocation(HR_PestControlServiceReport.this,tempStoreFrontImageName.get(i))));

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Bitmap bitmap = scaleBitmap(bmp, 100, 100);
				dto.setImageBitMap(bitmap);
				if (!tempFrontImagList.contains(tempStoreFrontImageName.get(i))) {

					if (bmp != null) {

						//CompressImage(bmp);
					}
				}// dto.setImageUri(Uri.fromFile(CommanFunction.getFileLocation(IWitnessActivity.this,
				// tempStoreFrontImageName.get(i))));

				tempFrontImagList.add(tempStoreFrontImageName.get(i));
				listFrontImg.add(dto);
			}

			//LoggerUtil.e("List Size", listImg.size() + "");

			/*if (listFrontImg.size() == 0)
			{
				ImgDto dto = new ImgDto();
				dto.setImagedrawable(R.drawable.noimage);
				listFrontImg.add(dto);
			}
			else
			{

			}*/
			adapterFront=new GalleryImageAdapter(getApplicationContext(), listFrontImg) ;
			imageFrontListView.setAdapter(adapterFront);
			//adapter.notifyDataSetChanged(); // TODO Auto-generated method stub
		}
		catch (Exception e) 
		{
			LoggerUtil.e("Exception in Refresh Image", e+"");
		}
	}
	private Bitmap decodeSmallUri(Uri selectedImage) throws FileNotFoundException 
	{
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(HR_PestControlServiceReport.this.getContentResolver().openInputStream(selectedImage), null, o);

		final int REQUIRED_SIZE = 100;

		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeStream(HR_PestControlServiceReport.this.getContentResolver().openInputStream(selectedImage), null, o2);
	}

	public void getfile(String fileName)
	{
		try
		{
			LoggerUtil.e("Get File Called", fileName);

			File imagefile=CommonFunction.getFileLocation(HR_PestControlServiceReport.this, fileName);
			if(imagefile.exists())
			{
				//Bitmap myBitmap = BitmapFactory.decodeFile(imagefile.getAbsolutePath());

				Bitmap myBitmap=decodeUri(Uri.fromFile(CommonFunction.getFileLocation(HR_PestControlServiceReport.this,fileName)));
				LoggerUtil.e("Input Bitmap width and heigth  ", myBitmap.getWidth()+" "+myBitmap.getWidth());
				//	edtDescription.setText(fileName+" "+myBitmap.getWidth()+" and "+myBitmap.getHeight());
				Bitmap output = Bitmap.createScaledBitmap(myBitmap, 800, 800, true);
				LoggerUtil.e("Out Put Bitmap width and heigth  ", output.getWidth()+" "+output.getWidth());
				imagefile.delete();
				CommonFunction.saveBitmap(output, HR_PestControlServiceReport.this, fileName);
			}}catch(Exception e)
			{

			}
	}


	private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException 
	{
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(HR_PestControlServiceReport.this.getContentResolver().openInputStream(selectedImage), null, o);

		final int REQUIRED_SIZE = 800;

		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeStream(HR_PestControlServiceReport.this.getContentResolver().openInputStream(selectedImage), null, o2);
	}
	public static Bitmap scaleBitmap(Bitmap bitmapToScale, float newWidth,float newHeight) 
	{
		if (bitmapToScale == null)
			return null;
		// get the original width and height
		int width = bitmapToScale.getWidth();
		int height = bitmapToScale.getHeight();
		// create a matrix for the manipulation
		Matrix matrix = new Matrix();

		// resize the bit map
		matrix.postScale(newWidth / width, newHeight / height);

		// recreate the new Bitmap and set it back
		return Bitmap.createBitmap(bitmapToScale, 0, 0, bitmapToScale
				.getWidth(), bitmapToScale.getHeight(), matrix, true);
	}

	class OnRadioChange implements RadioGroup.OnCheckedChangeListener
	{
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId)
		{
			//Log.e("", "inside radio click");
			// TODO Auto-generated method stub
			switch (checkedId) 
			{
			case R.id.radioCash:
				//edttxtAmount.getText().clear();
				edttxtDrivingLicence.getText().clear();
				edttxtExpirationDate.getText().clear();
				tramount.setVisibility(View.VISIBLE);
				trcheckno.setVisibility(View.GONE);
				trDrivingLicence.setVisibility(View.GONE);
				trExpirationDate.setVisibility(View.GONE);
				LinearlayoutCHECK_IMAGE.setVisibility(View.GONE);
				break;
			case R.id.radioCheck:

				//edttxtAmount.getText().clear();
				edttxtDrivingLicence.setText(generalInfoDTO.getLicenseNo());
				if(!generalInfoDTO.getExpirationDate().equals("01/01/1900") && !generalInfoDTO.getExpirationDate().equals(""))
				{
					edttxtExpirationDate.setText(generalInfoDTO.getExpirationDate());
				}
				tramount.setVisibility(View.VISIBLE);
				trcheckno.setVisibility(View.VISIBLE);
				trDrivingLicence.setVisibility(View.VISIBLE);
				trExpirationDate.setVisibility(View.VISIBLE);
				LinearlayoutCHECK_IMAGE.setVisibility(View.VISIBLE);
				break;
			case R.id.radioCreditcard:
				//edttxtAmount.getText().clear();
				edttxtDrivingLicence.getText().clear();
				edttxtExpirationDate.getText().clear();
				tramount.setVisibility(View.VISIBLE);
				trcheckno.setVisibility(View.GONE);
				trDrivingLicence.setVisibility(View.GONE);
				trExpirationDate.setVisibility(View.GONE);
				LinearlayoutCHECK_IMAGE.setVisibility(View.GONE);
				break;
			case R.id.radioBillLater:
				//edttxtAmount.getText().clear();
				edttxtDrivingLicence.getText().clear();
				edttxtExpirationDate.getText().clear();
				tramount.setVisibility(View.GONE);
				trcheckno.setVisibility(View.GONE);
				trDrivingLicence.setVisibility(View.GONE);
				trExpirationDate.setVisibility(View.GONE);
				LinearlayoutCHECK_IMAGE.setVisibility(View.GONE);
				break;
			case R.id.radioPreBill:
				//edttxtAmount.getText().clear();
				edttxtDrivingLicence.getText().clear();
				edttxtExpirationDate.getText().clear();
				tramount.setVisibility(View.GONE);
				trcheckno.setVisibility(View.GONE);
				trDrivingLicence.setVisibility(View.GONE);
				trExpirationDate.setVisibility(View.GONE);
				LinearlayoutCHECK_IMAGE.setVisibility(View.GONE);
				break;
			case R.id.radioNocharge:
				//edttxtAmount.getText().clear();
				edttxtDrivingLicence.getText().clear();
				edttxtExpirationDate.getText().clear();
				tramount.setVisibility(View.GONE);
				trcheckno.setVisibility(View.GONE);
				trDrivingLicence.setVisibility(View.GONE);
				trExpirationDate.setVisibility(View.GONE);
				LinearlayoutCHECK_IMAGE.setVisibility(View.GONE);
				break;
			case R.id.radiobtnFenceInspected1:
				if(radioYesFenceInspected.isChecked())
				{
					checkBoxFenceInspected.setChecked(true);
				}
				else
				{
					checkBoxFenceInspected.setChecked(false);
				}
				break;
			case R.id.radiobtnFenceInspected2:
				if(radioNoFenceInspected.isChecked())
				{
					checkBoxFenceInspected.setChecked(true);
				}
				else
				{
					checkBoxFenceInspected.setChecked(false);
				}
				break;
			case R.id.radiobtnMonitorindDevices1:
				if(radioYesMonitoringDevicesTermiteServices.isChecked())
				{
					checkBoxMonitoringDevicesTermiteServices.setChecked(true);
				}
				else
				{
					checkBoxMonitoringDevicesTermiteServices.setChecked(false);
				}
				break;
			case R.id.radiobtnMonitorindDevices2:
				if(radioNoMonitoringDevicesTermiteServices.isChecked())
				{
					checkBoxMonitoringDevicesTermiteServices.setChecked(true);
				}
				else
				{
					checkBoxMonitoringDevicesTermiteServices.setChecked(false);
				}
				break;
			case R.id.radiobtnREPLACED1:
				if(radioYesTermiteMonitoringDevicereplaced.isChecked())
				{
					checkBoxTermiteMonitoringDevicereplaced.setChecked(true);
				}
				else
				{
					checkBoxTermiteMonitoringDevicereplaced.setChecked(false);
				}
				break;
			case R.id.radiobtnREPLACES2:
				if(radioNoTermiteMonitoringDevicereplaced.isChecked())
				{
					checkBoxTermiteMonitoringDevicereplaced.setChecked(true);
				}
				else
				{
					checkBoxTermiteMonitoringDevicereplaced.setChecked(false);
				}
				break;
			case R.id.radiobtnExteriorPerimeterofhouse1:
				if(radioYesExteriorPerimeterofhouseTermiteServices.isChecked())
				{
					checkBoxExteriorPerimeterofhouseTermiteServices.setChecked(true);
				}
				else
				{
					checkBoxExteriorPerimeterofhouseTermiteServices.setChecked(false);
				}
				break;
			case R.id.radiobtnExteriorPerimeterofhouse2:
				if(radioNoExteriorPerimeterofhouseTermiteServices.isChecked())
				{
					checkBoxExteriorPerimeterofhouseTermiteServices.setChecked(true);
				}
				else
				{
					checkBoxExteriorPerimeterofhouseTermiteServices.setChecked(false);
				}
				break;

			case R.id.radiobtnAttic1:
				if(radioYesAttictermiteServices.isChecked())
				{
					checkBoxAttictermiteServices.setChecked(true);
				}
				else
				{
					checkBoxAttictermiteServices.setChecked(false);
				}
				break;
			case R.id.radiobtnAttic2:
				if(radioNoAttictermiteServices.isChecked())
				{
					checkBoxAttictermiteServices.setChecked(true);
				}
				else
				{
					checkBoxAttictermiteServices.setChecked(false);
				}
				break;
			case R.id.radiobtnINTERIOROFHOUSE1:
				if(radioYesInteriorOfHousetermiteServices.isChecked())
				{
					checkBoxInteriorOfHousetermiteServices.setChecked(true);
				}
				else
				{
					checkBoxInteriorOfHousetermiteServices.setChecked(false);
				}
				break;
			case R.id.radiobtnINTERIOROFHOUSE2:
				if(radioNoInteriorOfHousetermiteServices.isChecked())
				{
					checkBoxInteriorOfHousetermiteServices.setChecked(true);
				}
				else
				{
					checkBoxInteriorOfHousetermiteServices.setChecked(false);
				}
				break;

			case R.id.radiobtnROOF_PITCHES1:
				if(radioYesRoofPitches.isChecked())
				{
					checkBoxRoofPitches.setChecked(true);
				}
				else
				{
					checkBoxRoofPitches.setChecked(false);
				}
				break;
			case R.id.radiobtnROOF_PITCHES2:
				if(radioNoRoofPitches.isChecked())
				{
					checkBoxRoofPitches.setChecked(true);
				}
				else
				{
					checkBoxRoofPitches.setChecked(false);
				}
				break;

			case R.id.radiobtnROOF_VENTS1:
				if(radioYesRoofVents.isChecked())
				{
					checkBoxRoofVents.setChecked(true);
				}
				else
				{
					checkBoxRoofVents.setChecked(false);
				}
				break;
			case R.id.radiobtnROOF_VENTS2:
				if(radioNoRoofVents.isChecked())
				{
					checkBoxRoofVents.setChecked(true);
				}
				else
				{
					checkBoxRoofVents.setChecked(false);
				}
				break;

			case R.id.radiobtnSOFFIT_VENTS1:
				if(radioYesSoffitVents.isChecked())
				{
					checkBoxSoffitVents.setChecked(true);
				}
				else
				{
					checkBoxSoffitVents.setChecked(false);
				}
				break;
			case R.id.radiobtnSOFFIT_VENTS2:
				if(radioNoSoffitVents.isChecked())
				{
					checkBoxSoffitVents.setChecked(true);
				}
				else
				{
					checkBoxSoffitVents.setChecked(false);
				}
				break;

			case R.id.radiobtnWEEPHOLES1:
				if(radioYesWeepHoles.isChecked())
				{
					checkBoxWeepHoles.setChecked(true);
				}
				else
				{
					checkBoxWeepHoles.setChecked(false);
				}
				break;
			case R.id.radiobtnWEEPHOLES2:
				if(radioNoWeepHoles.isChecked())
				{
					checkBoxWeepHoles.setChecked(true);
				}
				else
				{
					checkBoxWeepHoles.setChecked(false);
				}
				break;

			case R.id.radiobtnSIDEING_HOUSE1:
				if(radioYesSidingofHouse.isChecked())
				{
					checkBoxSidingofHouse.setChecked(true);
				}
				else
				{
					checkBoxSidingofHouse.setChecked(false);
				}
				break;
			case R.id.radiobtnSIDEING_HOUSE2:
				if(radioNoSidingofHouse.isChecked())
				{
					checkBoxSidingofHouse.setChecked(true);
				}
				else
				{
					checkBoxSidingofHouse.setChecked(false);
				}
				break;

			case R.id.radiobtnBREEZYWAY_FROM_GARAGE1:
				if(radioYesBreezewayGarage.isChecked())
				{
					checkBoxBreezewayGarage.setChecked(true);
				}
				else
				{
					checkBoxBreezewayGarage.setChecked(false);
				}
				break;
			case R.id.radiobtnBREEZYWAY_FROM_GARAGE2:
				if(radioNoBreezewayGarage.isChecked())
				{
					checkBoxBreezewayGarage.setChecked(true);
				}
				else
				{
					checkBoxBreezewayGarage.setChecked(false);
				}
				break;

			case R.id.radiobtnGARAGE_DOOR1:
				if(radioYesGarageDoor.isChecked())
				{
					checkBoxGarageDoor.setChecked(true);
				}
				else
				{
					checkBoxGarageDoor.setChecked(false);
				}
				break;
			case R.id.radiobtnGARAGE_DOOR2:
				if(radioNoGarageDoor.isChecked())
				{
					checkBoxGarageDoor.setChecked(true);
				}
				else
				{
					checkBoxGarageDoor.setChecked(false);
				}
				break;

			case R.id.radiobtnAC_DRYER_VENTSR1:
				if(radioYesAcDryerVentsPhoneline.isChecked())
				{
					checkBoxAcDryerVentsPhoneline.setChecked(true);
				}
				else
				{
					checkBoxAcDryerVentsPhoneline.setChecked(false);
				}
				break;
			case R.id.radiobtnAC_DRYER_VENTSR2:
				if(radioNoAcDryerVentsPhoneline.isChecked())
				{
					checkBoxAcDryerVentsPhoneline.setChecked(true);
				}
				else
				{
					checkBoxAcDryerVentsPhoneline.setChecked(false);
				}
			break;
			default:
				break;
			}
		}
	}
	public class OnCheckedboxChangeListener implements android.widget.CompoundButton.OnCheckedChangeListener
	{
		@Override
		public void onCheckedChanged(CompoundButton buttonView,boolean isChecked)
		{
			// TODO Auto-generated method stub
			switch (buttonView.getId()) 
			{
			case R.id.checkBoxFenceInspected:
				if(!checkBoxFenceInspected.isChecked())
				{
					radioGroupFenceInspected.clearCheck();
				}
				break;
			case R.id.checkBoxMonitoringDevices:
				if(!checkBoxMonitoringDevicesTermiteServices.isChecked())
				{
					radioGroupMonitoringDevices.clearCheck();
				}
				break;
			case R.id.checkBoxTermiteMonitoringDevicereplaced:
				if(!checkBoxTermiteMonitoringDevicereplaced.isChecked())
				{
					radioGroupTermiteMonitoringDevices.clearCheck();
				}
				break;	
			case R.id.checkBoxExteriorPerimeterofhouseTermite:
				if(!checkBoxExteriorPerimeterofhouseTermiteServices.isChecked())
				{
					radioGroupExteriorPerimeterOfHouse.clearCheck();
				}
				break;
			case R.id.checkBoxATTICTermite:
				if(!checkBoxAttictermiteServices.isChecked())
				{
					radioGroupAttic.clearCheck();
				}
				break;
			case R.id.checkBoxINTERIOROFHOUSETermite:
				if(!checkBoxInteriorOfHousetermiteServices.isChecked())
				{
					radioGroupInteriorHouse.clearCheck();
				}
				break;
			case R.id.checkBoxROOF_PITCHES:
				if(!checkBoxRoofPitches.isChecked())
				{
					radioGroupRoofPitches.clearCheck();
				}
				break;
			case R.id.checkBoxROOF_VENTS:
				if(!checkBoxRoofVents.isChecked())
				{
					radioGroupRoofVents.clearCheck();
				}
				break;
			case R.id.checkBoxSOFFIT_VENTS:
				if(!checkBoxSoffitVents.isChecked())
				{
					radioGroupSoffitvents.clearCheck();
				}
				break;
			case R.id.checkBoxWEEPHOLES:
				if(!checkBoxWeepHoles.isChecked())
				{
					radioGroupWeepHoles.clearCheck();
				}
				break;
			case R.id.checkBoxSIDEING_HOUSE:
				if(!checkBoxSidingofHouse.isChecked())
				{
					radioGroupSidingHouse.clearCheck();
				}
				break;
			case R.id.checkBoxBREEZYWAY_FROM_GARAGE:
				if(!checkBoxBreezewayGarage.isChecked())
				{
					radioGroupBreezyWay.clearCheck();
				}
				break;
			case R.id.checkBoxGARAGE_DOOR:
				if(!checkBoxGarageDoor.isChecked())
				{
					radioGroupGarageDoor.clearCheck();
				}
				break;
			case R.id.checkBoxAC_DRYER_VENTS:
				if(!checkBoxAcDryerVentsPhoneline.isChecked())
				{
					radioGroupACDRYER.clearCheck();
				}
				break;
			case R.id.checkBoxANTS:
				if(!checkBoxAnts.isChecked())
				{
					edttxtInspectedandTreatedFor1.getText().clear();
				}
				else
				{
					if(checkChange==1)
					{
						edttxtInspectedandTreatedFor1.requestFocus();
					}
				}
				break;
			case R.id.checkBoxOTHERPESTS:
				if(!checkBoxOtherPests.isChecked())
				{
					edttxtInspectedandTreatedFor2.getText().clear();
				}
				else
				{
					if(checkChange==1)
					{
						edttxtInspectedandTreatedFor2.requestFocus();
					}
				}
				break;
			case R.id.checkBoxCHECKED_RUN_DURACTION_TIME:
				if(!checkBoxCheckedRunDurationTime.isChecked())
				{
					edttxtMosquitoServices1.getText().clear();
				}
				else
				{
					if(checkChange==1)
					{
						edttxtMosquitoServices1.requestFocus();
					}
				}
				break;
			case R.id.checkBoxCHECKED_TIME_SYSTEM_RUNS:
				if(!checkBoxCheckedTimeSystemRuns.isChecked())
				{
					edttxtMosquitoServices2.getText().clear();
				}
				else
				{
					if(checkChange==1)
					{
						edttxtMosquitoServices2.requestFocus();
					}
				}
				break;
			case R.id.checkBoxFenceLines:
				if(!checkBoxFenceLines.isChecked())
				{
					edttxtFenceLinesByZone.getText().clear();
				}
				else
				{
					if(checkChange==1)
					{
						edttxtFenceLinesByZone.requestFocus();
					}
				}
				break;
			case R.id.checkBoxFrontyardByZOne:
				if(!checkBoxFrontyardByZone.isChecked())
				{
					edttxtFrontyardByZone.getText().clear();
				}
				else
				{
					if(checkChange==1)
					{
						edttxtFrontyardByZone.requestFocus();
					}
				}
				break;
			case R.id.checkBoxBackYardByZone:
				if(!checkBoxBackYardByZone.isChecked())
				{
					edttxtBackYardByZone.getText().clear();
				}
				else
				{
					if(checkChange==1)
					{
						edttxtBackYardByZone.requestFocus();
					}
				}
				break;
			case R.id.checkBoxExteriorParametresofhouseByZone:
				if(!checkBoxExteriorPerimeterHouseByZone.isChecked())
				{
					edttxtExteriorPerimeterHouseByZone.getText().clear();
				}
				else
				{
					if(checkChange==1)
					{
						edttxtExteriorPerimeterHouseByZone.requestFocus();
					}
				}
				break;
			case R.id.checkBoxRofGutter:
				if(!checkBoxRoofLine.isChecked())
				{
					edttxtRoofGutterLine.getText().clear();
				}
				else
				{
					if(checkChange==1)
					{
						edttxtRoofGutterLine.requestFocus();
					}
				}
				break;
			case R.id.checkBoxGarage:
				if(!checkBoxGarageBYZone.isChecked())
				{
					edttxtGarageBYZone.getText().clear();
				}
				else
				{
					if(checkChange==1)
					{
						edttxtGarageBYZone.requestFocus();
					}
				}
				break;
			case R.id.checkBoxAtticByZone:
				if(!checkBoxAtticByZone.isChecked())
				{
					edttxtAtticByZone.getText().clear();
				}
				else
				{
					if(checkChange==1)
					{
						edttxtAtticByZone.requestFocus();
					}
				}
				break;
			case R.id.checkBoxInteriorHouse:
				if(!checkBoxInteriorOfHouseByZone.isChecked())
				{
					edttxtInteriorOfHouseByZone.getText().clear();
				}
				else
				{
					if(checkChange==1)
					{
						edttxtInteriorOfHouseByZone.requestFocus();
					}
				}
			break;
			
			case R.id.checkBoxTreatedAcct:
				if(checkBoxTreatedInspectedAttic.isChecked())
				{
					checkBoxInteriorPestControl.setChecked(true);
				}
				break;
				
			case R.id.checkBoxTreatedKitchen:
				if(checkBoxTreatedInspectedKitchen.isChecked())
				{
					checkBoxInteriorPestControl.setChecked(true);
				}
				break;
			case R.id.checkBoxTreatedLaundry:
				if(checkBoxTreatedInspectedLaundryRooms.isChecked())
				{
					checkBoxInteriorPestControl.setChecked(true);
				}
				break;
			case R.id.checkBoxLivingRooms:
				if(checkBoxTreatedInpectedlivingRooms.isChecked())
				{
					checkBoxInteriorPestControl.setChecked(true);
				}
				break;
			case R.id.checkBoxTreatedliving:
				if(checkBoxTreatedInspectedOtherLivingAreas.isChecked())
				{
					checkBoxInteriorPestControl.setChecked(true);
				}
				break;
			case R.id.checkBoxTreatedBedrooms:
				if(checkBoxTreatedInspectedBedroomsBathrooms.isChecked())
				{
					checkBoxInteriorPestControl.setChecked(true);
				}
				break;
				
			case R.id.checkBoxTreatedFront:
				if(checkBoxTreatedFrontPerimeter.isChecked())
				{
					checkBoxExteriorPestControl.setChecked(true);
				}
				break;
				
			case R.id.checkBoxTreatedback:
				if(checkBoxTreatedbackPerimeter.isChecked())
				{
					checkBoxExteriorPestControl.setChecked(true);
				}
				break;
			case R.id.checkBoxTreatedWhepholes:
				if(checkBoxTreatedWhepholes.isChecked())
				{
					checkBoxExteriorPestControl.setChecked(true);
				}
				break;
			case R.id.checkBoxTreatedCracks:
				if(checkBoxTreatedCracksCreves.isChecked())
				{
					checkBoxExteriorPestControl.setChecked(true);
				}
				break;
			case R.id.checkBoxtreatedyards:
				if(checkBoxTreatedYardHaouborageBreedingSites.isChecked())
				{
					checkBoxExteriorPestControl.setChecked(true);
				}
				break;
			case R.id.checkBoxTreatedfirenets:
				if(checkBoxTreatedFireAntsInYards.isChecked())
				{
					checkBoxExteriorPestControl.setChecked(true);
				}
				break;
			case R.id.checkBoxTreatedZoneMonitors:
				if(checkBoxInspectedReplacedZoneMonitors.isChecked())
				{
					checkBoxExteriorPestControl.setChecked(true);
				}
				break;
			case R.id.checkBoxTreatedDoorways:
				if(checkBoxTreatedInspectedDooeWaysWindows.isChecked())
				{
					checkBoxExteriorPestControl.setChecked(true);
				}
				break;
			case R.id.checkBoxINTERIOR:
				if(checkBoxInteriorPestControl.isChecked())
				{
					//checkBoxExteriorPestControl.setChecked(true);
				}
				else
				{
					checkBoxTreatedInspectedAttic.setChecked(false);
					checkBoxTreatedInspectedKitchen.setChecked(false);
					checkBoxTreatedInspectedLaundryRooms.setChecked(false);
					checkBoxTreatedInpectedlivingRooms.setChecked(false);
					checkBoxTreatedInspectedOtherLivingAreas.setChecked(false);
					checkBoxTreatedInspectedBedroomsBathrooms.setChecked(false);
				}
				break;
			case R.id.checkBoxEXTERIOR:
				if(checkBoxExteriorPestControl.isChecked())
				{
					//checkBoxExteriorPestControl.setChecked(true);
				}
				else
				{
					checkBoxTreatedFrontPerimeter.setChecked(false);
					checkBoxTreatedbackPerimeter.setChecked(false);
					checkBoxTreatedWhepholes.setChecked(false);
					checkBoxTreatedCracksCreves.setChecked(false);
					checkBoxTreatedYardHaouborageBreedingSites.setChecked(false);
					checkBoxTreatedFireAntsInYards.setChecked(false);
					checkBoxInspectedReplacedZoneMonitors.setChecked(false);
					checkBoxTreatedInspectedDooeWaysWindows.setChecked(false);
				}
				break;
			default:
				
				break;
			}
		}
	}
	private class CustomTextWatcher implements TextWatcher 
	{
		private View view;
		private CustomTextWatcher(View view) 
		{
			this.view = view;
		}
		@Override
		public void afterTextChanged(Editable s) 
		{
			// TODO Auto-generated method stub
			String newString = s.toString();

			switch(view.getId())
			{
			
			case R.id.edtNUMBER_OF_MONITERING_DEVICES_INSPECTED:
			if(newString.equals(""))
			{
				//	LoggerUtil.e("checked newString","false");
				checkBoxNumberofMonitoringDevicesInspectedTermiteServices.setChecked(false);

			}
			else
			{
				//	LoggerUtil.e("checked newString","true");
				checkBoxNumberofMonitoringDevicesInspectedTermiteServices.setChecked(true);
			}
			break;
			case R.id.edtTechComment:
				LoggerUtil.e("edtTechComment", "");
				if (newString.length() > 0) {
					LoggerUtil.e("newString.length()", ""
							+ edttxtTechComments.getText().toString().length());
					textviewCharactersTyped.setText(edttxtTechComments.getText()
							.toString().length()
							+ " " + "Characters typed");
				}
				
				break;

			case R.id.edtiNSPECTEDANDtREATEDFOR1:

				if(newString.equals(""))
				{
					//	LoggerUtil.e("checked newString","false");
					checkBoxAnts.setChecked(false);

				}
				else
				{
					//	LoggerUtil.e("checked newString","true");
					checkBoxAnts.setChecked(true);
				}
				break;
			case R.id.edtiNSPECTEDANDtREATEDFOR2:

				if(newString.equals(""))
				{
					//	LoggerUtil.e("checked newString","false");
					checkBoxOtherPests.setChecked(false);

				}
				else
				{
					//	LoggerUtil.e("checked newString","true");
					checkBoxOtherPests.setChecked(true);
				}
				break;

			case R.id.edtFenceLines:
				if(newString.equals(""))
				{
					//	LoggerUtil.e("checked newString","false");
					//checkBoxFenceLines.setChecked(false);

				}
				else
				{
					//	LoggerUtil.e("checked newString","true");
					checkBoxFenceLines.setChecked(true);
				}
				break;

			case R.id.edtFrontYard:
				if(newString.equals(""))
				{
					//	LoggerUtil.e("checked newString","false");
					//checkBoxFrontyardByZone.setChecked(false);

				}
				else
				{
					//	LoggerUtil.e("checked newString","true");
					checkBoxFrontyardByZone.setChecked(true);
				}
				break;

			case R.id.edtBackyard:
				if(newString.equals(""))
				{
					//	LoggerUtil.e("checked newString","false");
					//checkBoxBackYardByZone.setChecked(false);

				}
				else
				{
					//	LoggerUtil.e("checked newString","true");
					checkBoxBackYardByZone.setChecked(true);
				}
				break;

			case R.id.edtExteriorPara:
				if(newString.equals(""))
				{
					//	LoggerUtil.e("checked newString","false");
					//checkBoxExteriorPerimeterHouseByZone.setChecked(false);

				}
				else
				{
					//	LoggerUtil.e("checked newString","true");
					checkBoxExteriorPerimeterHouseByZone.setChecked(true);
				}
				break;

			case R.id.edtRoofGutter:
				if(newString.equals(""))
				{
					//	LoggerUtil.e("checked newString","false");
					//checkBoxRoofLine.setChecked(false);

				}
				else
				{
					//	LoggerUtil.e("checked newString","true");
					checkBoxRoofLine.setChecked(true);
				}
				break;

			case R.id.edtGarage:
				if(newString.equals(""))
				{
					//	LoggerUtil.e("checked newString","false");
					//checkBoxGarageBYZone.setChecked(false);

				}
				else
				{
					//	LoggerUtil.e("checked newString","true");
					checkBoxGarageBYZone.setChecked(true);
				}
				break;

			case R.id.edtAtticPestbyZone:
				if(newString.equals(""))
				{
					//	LoggerUtil.e("checked newString","false");
					//checkBoxAtticByZone.setChecked(false);

				}
				else
				{
					//	LoggerUtil.e("checked newString","true");
					checkBoxAtticByZone.setChecked(true);
				}
				break;

			case R.id.edtInteriorHouse:
				if(newString.equals(""))
				{
					//	LoggerUtil.e("checked newString","false");
					//checkBoxInteriorOfHouseByZone.setChecked(false);

				}
				else
				{
					//	LoggerUtil.e("checked newString","true");
					checkBoxInteriorOfHouseByZone.setChecked(true);
				}
				break;
			case R.id.edtNO_LIVEANIMAL_TRAPS_INSTALLES:
				if(newString.equals(""))
				{
					//	LoggerUtil.e("checked newString","false");
					checkBoxNumberofLiveAnimalTrapsinstalled.setChecked(false);

				}
				else
				{
					//	LoggerUtil.e("checked newString","true");
					checkBoxNumberofLiveAnimalTrapsinstalled.setChecked(true);
				}
				break;

			case R.id.edtNumber_SNAPTRAPS:
				if(newString.equals(""))
				{
					//	LoggerUtil.e("checked newString","false");
					checkBoxNumberofSnaptrapsplaced.setChecked(false);

				}
				else
				{
					//	LoggerUtil.e("checked newString","true");
					checkBoxNumberofSnaptrapsplaced.setChecked(true);
				}
				break;

			case R.id.edtMosquitoService1:
				if(newString.equals(""))
				{
					//	LoggerUtil.e("checked newString","false");
					checkBoxCheckedRunDurationTime.setChecked(false);

				}
				else
				{
					//	LoggerUtil.e("checked newString","true");
					checkBoxCheckedRunDurationTime.setChecked(true);
				}
				break;

			case R.id.edtMosquitoService2:
				if(newString.equals(""))
				{
					//	LoggerUtil.e("checked newString","false");
					checkBoxCheckedTimeSystemRuns.setChecked(false);

				}
				else
				{
					//	LoggerUtil.e("checked newString","true");
					checkBoxCheckedTimeSystemRuns.setChecked(true);
				}
				break;
			default:
				break;
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			/*System.out.println("1wf"+s);
			if(s.equals(" "))
			{
				checkBoxFrontyard.setChecked(false);
			}
			if(!s.equals(" "))
			{
			checkBoxFrontyard.setChecked(true);
			}*/
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			/*System.out.println("1wf..."+s);
			if(s.equals(" "))
			{
				checkBoxFrontyard.setChecked(false);
			}
			if(!s.equals(" "))
			{
			checkBoxFrontyard.setChecked(true);
			}*/
		}
	}
	CharSequence[] charSequenceItems;
	class OnButtonClick implements OnClickListener
	{
		@Override
		public void onClick(View v)
		{
			switch (v.getId()) {
			case R.id.btnCHECK_BACK_IMAGE:
				if(listBackImg.size()>0)
				{
					new AlertDialog.Builder(HR_PestControlServiceReport.this)
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setTitle("Change Check Back Image")
					.setMessage("Are you sure want to replace it?")
					.setCancelable(false)
					.setPositiveButton("Yes", new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int which) 
						{
             				final CharSequence[] items = {" Take New Photo "," Choose from Gallery "};

							// Creating and Building the Dialog 
							AlertDialog.Builder builder = new AlertDialog.Builder(HR_PestControlServiceReport.this);
							builder.setTitle("Upload Image");
							builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int item) {

									switch(item)
									{
									case 0:

										if (CommonFunction.isSdPresent())
										{
											takeBackPicture();
										} else
										{
											Toast.makeText(HR_PestControlServiceReport.this,"SdCard Not Present", 1000).show();
										}


										break;
									case 1:

										if (CommonFunction.isSdPresent()) 
										{
											pickImage("CHECK_BACK_IMAGE");
										} else 
										{
											Toast.makeText(HR_PestControlServiceReport.this,"SdCard Not Present", 1000).show();
										}

										break;
									}
									dialogBackImage.dismiss();    
								}
							});
							dialogBackImage = builder.create();
							dialogBackImage.show();

						}

					})
					.setNegativeButton("No", new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int which) 
						{
							dialog.dismiss();
						}
					})
					.show();	
				}
				else
				{
					final CharSequence[] items = {" Take New Photo "," Choose from Gallery "};

					// Creating and Building the Dialog 
					AlertDialog.Builder builder = new AlertDialog.Builder(HR_PestControlServiceReport.this);
					builder.setTitle("Upload Image");
					builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int item) {

							switch(item)
							{
							case 0:

								if (CommonFunction.isSdPresent())
								{
									takeBackPicture();
								} else
								{
									Toast.makeText(HR_PestControlServiceReport.this,"SdCard Not Present", 1000).show();
								}

								break;
							case 1:

								if (CommonFunction.isSdPresent()) 
								{
									pickImage("CHECK_BACK_IMAGE");
								} 
								else 
								{
									Toast.makeText(HR_PestControlServiceReport.this,"SdCard Not Present", 1000).show();
								}

								break;
							}
							dialogBackImage.dismiss();    
						}
					});
					dialogBackImage = builder.create();
					dialogBackImage.show();
				}

				break;
			case R.id.btnCHECK_FRONT_IMAGE:
			//	Log.e("listFrontImg.size()", ""+listFrontImg.size());
				if(listFrontImg.size()>0)
				{
					new AlertDialog.Builder(HR_PestControlServiceReport.this)
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setTitle("Change Check Front Image")
					.setMessage("Are you sure want to replace it?")
					.setCancelable(false)
					.setPositiveButton("Yes", new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int which) 
						{


							final CharSequence[] items = {" Take New Photo "," Choose from Gallery "};

							// Creating and Building the Dialog 
							AlertDialog.Builder builder = new AlertDialog.Builder(HR_PestControlServiceReport.this);
							builder.setTitle("Upload Image");
							builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int item) {

									switch(item)
									{

									case 0:


										if (CommonFunction.isSdPresent())
										{
											takeFrontPicture();
										} else
										{
											Toast.makeText(HR_PestControlServiceReport.this,"SdCard Not Present", 1000).show();
										}

										break;
									case 1:


										if (CommonFunction.isSdPresent()) 
										{
											pickImage("CHECK_FRONT_IMAGE");
										} else 
										{
											Toast.makeText(HR_PestControlServiceReport.this,"SdCard Not Present", 1000).show();
										}

										break;
									}
									dialogFrontImage.dismiss();    
								}
							});
							dialogFrontImage = builder.create();
							dialogFrontImage.show();

						}

					})
					.setNegativeButton("No", new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int which) 
						{
							dialog.dismiss();
						}
					})
					.show();	
				}
				else
				{
					final CharSequence[] items = {" Take New Photo "," Choose from Gallery "};

					// Creating and Building the Dialog 
					AlertDialog.Builder builder = new AlertDialog.Builder(HR_PestControlServiceReport.this);
					builder.setTitle("Upload Image");
					builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int item) {

							switch(item)
							{
							case 0:
								if (CommonFunction.isSdPresent())
								{
									takeFrontPicture();
								} else
								{
									Toast.makeText(HR_PestControlServiceReport.this,"SdCard Not Present", 1000).show();
								}
								break;
							case 1:

								if (CommonFunction.isSdPresent()) 
								{
									pickImage("CHECK_FRONT_IMAGE");
								} else 
								{
									Toast.makeText(HR_PestControlServiceReport.this,"SdCard Not Present", 1000).show();
								}

								break;
							}
							dialogFrontImage.dismiss();    
						}
					});
					dialogFrontImage = builder.create();
					dialogFrontImage.show();
				}

				break;
			case R.id.edtEXPIRATION_DATE:
				// String strFromDate=edtFromdate.getText().toString();
				//  String strToDate=edtTodate.getText().toString();
				DialogFragment DatePickerFragment = new DatePickerFragment(edttxtExpirationDate);
				DatePickerFragment.show(getFragmentManager(), "Date Picker");
				DatePickerFragment.setCancelable(false);
				LoggerUtil.e("edttxtExpirationDate.getText().toString()", ""+edttxtExpirationDate.getText().toString());

				break;
			case R.id.edtDate:
				// String strFromDate=edtFromdate.getText().toString();
				//  String strToDate=edtTodate.getText().toString();
				DialogFragment TodateFragment = new DatePickerFragment(edttxtdate,edttxtdate,2);
				TodateFragment.show(getFragmentManager(), "Date Picker");
				TodateFragment.setCancelable(false);
				break;
			case R.id.btnsaveandcontinueINVOICE:

				//JSONArray jsChemical = houstonFlowFunctions.getSelectedChemicals(DatabaseHelper.TABLE_CHEMICALS);
				//LoggerUtil.e("jsChemical", jsChemical.toString());
				/*if (SystemClock.elapsedRealtime() - mLastClickTime < 4000) 
				{
					break;
				}
				mLastClickTime = SystemClock.elapsedRealtime();*/
				getValuesFromComponents();
				setValuesToResidentialDTO();
				validationList.clear();
				isValidated=true;
				objectFocus=null;
				validateComponents();
				if(isValidated)
				{    
					checkChange=0;
					insertUpadteResidentialTable();
					insertUpdateLog();
					Intent i= new Intent(HR_PestControlServiceReport.this,HR_Chemical_Preview.class);
					i.putExtra("userDTO",userDTO);
					i.putExtra("generalInfoDTO", generalInfoDTO);
					i.putExtra("strOutsideService",strOutsidepestControlServices);
					i.putExtra("strInsideService", strInsidepestControlServices);
					i.putExtra(ParameterUtil.ServiceID_new, generalInfoDTO.getServiceID_new());
					startActivity(i);
				}
				else
				{
					mLastClickTime = 0;
					charSequenceItems = validationList.toArray(new CharSequence[validationList.size()]);
					createMessageList(charSequenceItems,objectFocus);
				}
				break;
			case R.id.btnBack:
				/*Intent intent=new Intent(ResidentialPestControlServiceReport.this, GeneralInformationActivity.class);
				//Log.e("", ""+serviceList.get(arg2));
				intent.putExtra("serviceIdNew",serviceID_new);
				intent.putExtra("userDTO", userDTO);
				intent.putExtra("back", "yes");
				startActivity(intent);*/
				finish();
				break;
			case R.id.btnCancel:
				finish();
				break;

			case R.id.btnTermiteDisclosure:
				dialogTermiteClosure = new Dialog(HR_PestControlServiceReport.this);
				dialogTermiteClosure.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialogTermiteClosure.setContentView(R.layout.termite_disclosure_popup);
				Button Save=(Button)dialogTermiteClosure.findViewById(R.id.btnSave);
				Button cancel=(Button)dialogTermiteClosure.findViewById(R.id.btnCancel);
				Save.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialogTermiteClosure.dismiss();
					}
				});
				cancel.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialogTermiteClosure.dismiss();
					}
				});
				dialogTermiteClosure.show();
				break;
			default:
				break;
			}
		}
		public void pickImage(String CHECK_FRONT_IMAGE) 
		{
			//Log.e("CHECK_FRONT_IMAGE", ""+CHECK_FRONT_IMAGE);
			if(CHECK_FRONT_IMAGE.equals("CHECK_FRONT_IMAGE"))
			{
				Intent intent = new Intent();
				intent.setType("image/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				intent.addCategory(Intent.CATEGORY_OPENABLE);
				startActivityForResult(intent, REQUEST_Gallery_ACTIVITY_CODE_Front);
			}
			else
			{
				Intent intent = new Intent();
				intent.setType("image/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				intent.addCategory(Intent.CATEGORY_OPENABLE);
				startActivityForResult(intent, REQUEST_Gallery_ACTIVITY_CODE_Back);
			}
		}
		public void takeBackPicture() 
		{
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			CheckBackImageName =generalInfoDTO.getServiceID_new()+"_CheckBack.jpg"; //CommonFunction.CreateImageName();
			Uri imageUri = Uri.fromFile(CommonFunction.getFileLocation(HR_PestControlServiceReport.this, CheckBackImageName));
			intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
			intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
			intent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, Integer.toString(1024*1024));
			startActivityForResult(intent, REQUEST_Camera_ACTIVITY_CODE_Back);
			//LoggerUtil.e("Image Url", imageUri + "");
		}

		public void takeFrontPicture() 
		{
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			CheckFrontImageName =generalInfoDTO.getServiceID_new()+"_CheckFront.jpg"; //CommonFunction.CreateImageName();
			Uri imageUri = Uri.fromFile(CommonFunction.getFileLocation(HR_PestControlServiceReport.this, CheckFrontImageName));
			intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
			intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
			intent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, Integer.toString(1024*1024));
			startActivityForResult(intent, REQUEST_Camera_ACTIVITY_CODE_Front);
			//LoggerUtil.e("Image Url", imageUri + "");
		}

		private final Dialog createMessageList(final CharSequence[] messageList,final EditText object)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(HR_PestControlServiceReport.this);
			builder.setTitle(getResources().getString(R.string.REQUIRED_INFORMATION));
			builder.setItems(charSequenceItems,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton)
				{
					//  LoggerUtil.e("","E' stato premuto il pulsante: "+messageList[whichButton]);   
				}
			});
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton)
				{
					if(object!=null)
					{
						object.requestFocus();
					}
				}
			});
			return builder.show();
		}
		private void validateComponents() 
		{
			// TODO Auto-generated method stub
			if(radioCreditcard.isChecked())
			{
				if(edttxtAmount.getText().toString().equals("") || edttxtAmount.getText().toString().equals("0"))
				{
					validationList.add("Enter Amount.");
					objectFocus=edttxtAmount;
					isValidated=false;
				}
				else if(edttxtAmount.getText().toString().contains(".") && edttxtAmount.getText().toString().length()==1)
				{
					validationList.add("Amount format should be: 00.00");
					objectFocus=edttxtAmount;
					isValidated=false;
				}
			}
			if(radioCash.isChecked())
			{
				if(edttxtAmount.getText().toString().equals("") || edttxtAmount.getText().toString().equals("0"))
				{
					validationList.add("Enter Amount.");
					objectFocus=edttxtAmount;
					isValidated=false;
				}
				else if(edttxtAmount.getText().toString().contains(".") && edttxtAmount.getText().toString().length()==1)
				{
					validationList.add("Amount format should be: 00:00");
					objectFocus=edttxtAmount;
					isValidated=false;
				}
			}
			if(radioCheck.isChecked())
			{
				if(edttxtAmount.getText().toString().equals("") || edttxtAmount.getText().toString().equals("0"))
				{
					validationList.add("Enter Amount.");
					objectFocus=edttxtAmount;
					isValidated=false;
				}
				if(edttxtAmount.getText().toString().contains(".") && edttxtAmount.getText().toString().length()==1)
				{
					validationList.add("Amount format should be: 00:00");
					objectFocus=edttxtAmount;
					isValidated=false;
				}
				if(edttxtCheckNo.getText().toString().equals(""))
				{
					validationList.add("Enter Check#.");
					objectFocus=edttxtCheckNo;
					isValidated=false;
				}
				if(strEdttxtDrivingLicence.equals(""))
				{
					validationList.add("Enter Driving  License#.");
					objectFocus=edttxtDrivingLicence;
					isValidated=false;
				}
				if(strEdtExpirationDate.equals(""))
				{
					validationList.add("Enter Expiration Date.");
					objectFocus=edttxtExpirationDate;
					isValidated=false;
				}
				if(!edttxtExpirationDate.getText().toString().equals(""))
				{
					try 
					{
						SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

						Date dateCurrent = sdf.parse(CommonFunction.getCurrentDate());
						Date dateExpiry = sdf.parse(edttxtExpirationDate.getText().toString());
						if(dateExpiry.before(dateCurrent))
						{
							validationList.add("Expiration Date should not be smaller then Today.");
							objectFocus=edttxtExpirationDate;
							isValidated=false;
						}
					} 
					catch (Exception e) 
					{


					}
				}

				//Log.e("", "generalInfoDTO.getCheckFrontImage().inside validate.."+generalInfoDTO.getCheckFrontImage());
				if(CommonFunction.checkString(generalInfoDTO.getCheckFrontImage(), "").equals(""))
				{
					//Log.e("", "ImageNameFront..."+ImageNameFront);
					validationList.add("Upload Check Front Image.");
					isValidated=false;
				}
				if(CommonFunction.checkString(generalInfoDTO.getCheckBackImage(), "").equals(""))
				{
					//Log.e("", "ImageNameBack..."+ImageNameBack);
					validationList.add("Upload Check Back Image.");
					isValidated=false;
				}
			}

			if(checkBoxCheckedTimeSystemRuns.isChecked())
			{
				if(edttxtMosquitoServices2.getText().toString().equals(""))
				{
					validationList.add("Enter Checked Time System Runs Mosquito Services Tab.");
					objectFocus=edttxtMosquitoServices2;
					isValidated=false;
				}
			}
			if(checkBoxCheckedRunDurationTime.isChecked())
			{
				if(edttxtMosquitoServices1.getText().toString().equals(""))
				{
					validationList.add("Enter Checked Run Duration Time Under Mosquito Services Tab.");
					objectFocus=edttxtMosquitoServices1;
					isValidated=false;
				}
			}

			if(checkBoxNumberofSnaptrapsplaced.isChecked())
			{
				if(edttxtNumberofSnaptrapsplaced.getText().toString().equals(""))
				{
					validationList.add("Enter Number of Snap traps placed in Rodent Control Tab.");
					isValidated=false;
				}
			}
			if(checkBoxNumberofLiveAnimalTrapsinstalled.isChecked())
			{
				if(edttxtNumberofLiveAnimalTrapsinstalled.getText().toString().equals(""))
				{
					validationList.add("Enter Number of Live animal traps installed in Rodent Control Tab.");
					isValidated=false;
				}
			}

			if(checkBoxNumberofMonitoringDevicesInspectedTermiteServices.isChecked())
			{
				if(edttxtNumberofMonitoringDevicesInspected.getText().toString().equals(""))
				{
					validationList.add("Enter Number of Monitoring Devices Inspected in Termite Control Tab.");
					isValidated=false;
				}
			}
			if(checkBoxOtherPests.isChecked() && CommonFunction.checkString(edttxtInspectedandTreatedFor2.getText().toString().trim(), "").equals(""))
			{
				//Log.e("", "ImageNameFront..."+ImageNameFront);
				validationList.add("Enter Number of Other Pests under Inspected and Treated For Tab");
				objectFocus=edttxtInspectedandTreatedFor2;
				isValidated=false;
			}
			if(checkBoxAnts.isChecked() && CommonFunction.checkString(edttxtInspectedandTreatedFor1.getText().toString().trim(), "").equals(""))
			{
				//Log.e("", "ImageNameFront..."+ImageNameFront);
				validationList.add("Enter Number of Ants under Inspected and Treated For Tab");
				objectFocus=edttxtInspectedandTreatedFor1;
				isValidated=false;
			}

			/*if(radioCreditcard.isChecked())
			{
				if(edttxtAmount.getText().toString().equals("") || edttxtAmount.getText().toString().equals("0"))
				{
					validationList.add("Enter Amount.");
					isValidated=false;
				}
			}*/
			validateUnderTermite();
			validateUnderRodent();
			validateUnderChemicalTable();
		}
		private void validateUnderRodent() {
			// TODO Auto-generated method stub
			if(checkBoxRoofPitches.isChecked())
			{

				int idRoofPitches = radioGroupRoofPitches.getCheckedRadioButtonId();
				if(idRoofPitches == -1)
				{

					validationList.add("Select Sealed for Roof Pitches In Rodent Services");
					isValidated=false;
				}
			}

			if(checkBoxRoofVents.isChecked())
			{
				int idRoofVents = radioGroupRoofVents.getCheckedRadioButtonId();
				if(idRoofVents == -1)
				{

					validationList.add("Select Sealed for Roof Vents In Rodent Services");
					isValidated=false;
				}
			}

			if(checkBoxSoffitVents.isChecked())
			{
				int idSoffitVents = radioGroupSoffitvents.getCheckedRadioButtonId();
				if(idSoffitVents == -1)
				{

					validationList.add("Select Sealed for Soffit Vents In Rodent Services");
					isValidated=false;
				}
			}


			if(checkBoxWeepHoles.isChecked())
			{
				int idWeepHoles = radioGroupWeepHoles.getCheckedRadioButtonId();
				if(idWeepHoles == -1)
				{

					validationList.add("Select Sealed for Weep Holes In Rodent Services");
					isValidated=false;
				}
			}


			if(checkBoxSidingofHouse.isChecked())
			{

				int idSidingofHouse = radioGroupSidingHouse.getCheckedRadioButtonId();
				if(idSidingofHouse == -1)
				{

					validationList.add("Select Sealed for Siding of House In Rodent Services");
					isValidated=false;
				}
			}


			if(checkBoxBreezewayGarage.isChecked())
			{
				int idBreezewayGarage = radioGroupBreezyWay.getCheckedRadioButtonId();

				if(idBreezewayGarage == -1)
				{
					validationList.add("Select Sealed for Breezy Way In Rodent Services");
					isValidated=false;
				}
			}


			if(checkBoxGarageDoor.isChecked())
			{

				int idGarageDoor = radioGroupGarageDoor.getCheckedRadioButtonId();
				if(idGarageDoor == -1)
				{

					validationList.add("Select Sealed for Garage Door In Rodent Services");
					isValidated=false;
				}
			}


			if(checkBoxAcDryerVentsPhoneline.isChecked())
			{

				int idACDRYER = radioGroupACDRYER.getCheckedRadioButtonId();
				if(idACDRYER == -1)
				{

					validationList.add("Select Sealed for AC Dryer In Rodent Services");
					isValidated=false;
				}
			}
		}
		private void validateUnderTermite() {
			// TODO Auto-generated method stub
			if(checkBoxFenceInspected.isChecked())
			{

				int idFenceInspected = radioGroupFenceInspected.getCheckedRadioButtonId();
				if(idFenceInspected == -1)
				{
					validationList.add("Select Visible Termite Activity for Fence Inspected in Service Termite Tab");
					isValidated=false;
				}
			}


			if(checkBoxMonitoringDevicesTermiteServices.isChecked())
			{

				int idMoniterDevices = radioGroupMonitoringDevices.getCheckedRadioButtonId();
				if(idMoniterDevices == -1)
				{
					validationList.add("Select Visible Termite Activity for Monitoring Devices in Service Termite Tab");
					isValidated=false;
				}
			}

			if(checkBoxTermiteMonitoringDevicereplaced.isChecked())
			{

				int idTermiteMoniter = radioGroupTermiteMonitoringDevices.getCheckedRadioButtonId();
				if(idTermiteMoniter == -1)
				{
					validationList.add("Select Visible Termite Activity for Termite Monitoring Devices Replaced in Service Termite Tab");
					isValidated=false;
				}
			}

			if(checkBoxExteriorPerimeterofhouseTermiteServices.isChecked())
			{

				int idExteriorPerimeter = radioGroupExteriorPerimeterOfHouse.getCheckedRadioButtonId();
				if(idExteriorPerimeter == -1)
				{
					validationList.add("Select Visible Termite Activity for Exterior Perimeter in Service Termite Tab");
					isValidated=false;
				}
			}

			if(checkBoxAttictermiteServices.isChecked())
			{
				int idAttic = radioGroupAttic.getCheckedRadioButtonId();
				if(idAttic == -1)
				{
					validationList.add("Select Visible Termite Activity for Attic Devices in Service Termite Tab");
					isValidated=false;
				}
			}


			if(checkBoxInteriorOfHousetermiteServices.isChecked())
			{

				int idInteriorHouse = radioGroupInteriorHouse.getCheckedRadioButtonId();
				if(idInteriorHouse == -1)
				{
					validationList.add("Select Visible Termite Activity for Interior of House in Service Termite Tab");
					isValidated=false;
				}
			}

		}
	}
	public void getValuesFromComponents() 
	{
		// TODO Auto-generated method stub
		getValuesFromServiceForHeader();
		getValuesformAreasInspected();
		grtValuesforInspectedTreatedFor();
		getValueForoutsideOnlyServiceFor();
		getValuesPestActivitybyZone();
		getValuesPestControlServices();
		getValuesTermiteServices();
		getValuesRodentServices();
		getValuesMosquitoServices();
		getValuesForRecommendations();
		getValuesForInvoice();

	}



	public void validateUnderChemicalTable() {
		// TODO Auto-generated method stub
		for(int i=0;i<checkList.size();i++)
		{
			if(checkList.get(i).isChecked())
			{
				if(listChemicals.get(i).getPercentPes().contains(".") && listChemicals.get(i).getPercentPes().length()==1)
				{
					validationList.add("Please enter some Percent under the chemical product: "+listChemicals.get(i).getProduct());
					isValidated=false;
					//break;
				}
				if(listChemicals.get(i).getAmount().contains(".") && listChemicals.get(i).getAmount().length()==1)
				{
					validationList.add("Please enter some Amount under the chemical product: "+listChemicals.get(i).getProduct());
					isValidated=false;
					//break;
				}
				if(!listChemicals.get(i).getPercentPes().equals("") && Float.valueOf(listChemicals.get(i).getPercentPes())>100.00)
				{
					validationList.add("Percent value cannot be greater than 100 under the chemical product: "+listChemicals.get(i).getProduct());
					isValidated=false;
				}
			}
		}
	}
	private String blockCharacterSet = "~#^|$%&*!";
	private InputFilter filter = new InputFilter() {

		@Override
		public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

			if (source != null && blockCharacterSet.contains(("" + source))) {
				return "";
			}
			return null;
		}
	};
	public void insertUpadteResidentialTable() 
	{
		// TODO Auto-generated method stub
		Gson gson=new Gson();

		for(int i=0;i<checkList.size();i++)
		{
			//LoggerUtil.e("checkList.size()..","checkList.size()..."+checkList.size());

			ChemicalDto chemicalDto=new ChemicalDto();
			//LoggerUtil.e("listChemicals.get(i).getPestPrevention_ID()..","listChemicals.get(i).getPestPrevention_ID()..."+listChemicals.get(i).getPestPrevention_ID());
			if(checkList.get(i).isChecked())
			{
				logDTO.setChemical("True");
				chemicalDto.setIsCompleted("true");
				if(CommonFunction.checkString(listChemicals.get(i).getPestPrevention_ID(), "").equals(""))
				{
					chemicalDto.setPestPrevention_ID("0");
				}
				else
				{
					chemicalDto.setPestPrevention_ID(listChemicals.get(i).getPestPrevention_ID());
				}
				
				chemicalDto.setPreventationID(listChemicals.get(i).getPreventationID());
				chemicalDto.setProduct(listChemicals.get(i).getProduct());
				if (CommonFunction.checkString(
						listChemicals.get(i).getPercentPes(), "").equals("")) {
					chemicalDto.setPercentPes("0.00");
				} else {
					chemicalDto.setPercentPes(listChemicals.get(i).getPercentPes());
				}
				if(CommonFunction.checkString(listChemicals.get(i).getAmount(), "").equals(""))
				{
					chemicalDto.setAmount("0.00");
				}
				else
				{
					chemicalDto.setAmount(listChemicals.get(i).getAmount());
				}
				chemicalDto.setStatus("Residential");
				chemicalDto.setUnit(listChemicals.get(i).getUnit());
				chemicalDto.setTarget(listChemicals.get(i).getTarget());
				chemicalDto.setCreate_By(userDTO.getPestPackId());
				chemicalDto.setServiceID_new(serviceID_new);
				LoggerUtil.e("listChemicals.get(i).getOtherText()",listChemicals.get(i).getOtherText());

				chemicalDto.setOtherText(listChemicals.get(i).getOtherText());
				chemicalDto.setCreate_Date(CommonFunction.getCurrentDateTime());
				listChemicals.get(i).setProductCheck("true");
				chemicalDto.setProductCheck(listChemicals.get(i).getProductCheck());
			}
			else
			{

				chemicalDto.setIsCompleted("true");
				chemicalDto.setPestPrevention_ID(listChemicals.get(i).getPestPrevention_ID());
				chemicalDto.setPreventationID(listChemicals.get(i).getPreventationID());
				chemicalDto.setProduct(listChemicals.get(i).getProduct());
				chemicalDto.setPercentPes(listChemicals.get(i).getPercentPes());
				chemicalDto.setAmount(listChemicals.get(i).getAmount());
				chemicalDto.setUnit(listChemicals.get(i).getUnit());
				chemicalDto.setTarget(listChemicals.get(i).getTarget());
				chemicalDto.setCreate_By(userDTO.getPestPackId());
				chemicalDto.setServiceID_new(serviceID_new);

				listChemicals.get(i).setOtherText("");	
				chemicalDto.setOtherText(listChemicals.get(i).getOtherText());
				chemicalDto.setCreate_Date(CommonFunction.getCurrentDateTime());
				listChemicals.get(i).setProductCheck("false");
				chemicalDto.setProductCheck(listChemicals.get(i).getProductCheck());

			}
			listSelectedChemicals.add(chemicalDto);
		}
		try 
		{

			houstonFlowFunctions.deleteSelectedChemicalData(DatabaseHelper.TABLE_SELECTED_CHEMICALS, ParameterUtil.ServiceID_new,ParameterUtil.Status, serviceID_new, "Residential");
			houstonFlowFunctions.deleteSelectedChemicalData(DatabaseHelper.TABLE_CHEMICALS, ParameterUtil.ServiceID_new,ParameterUtil.Status, serviceID_new, "Residential");

			JSONArray jsSelectedChemical=new JSONArray(gson.toJson(listSelectedChemicals));
			//long a =	HoustonFlowFunctions.addSelectedChemicalData(new JSONObject(chemicalString));
			houstonFlowFunctions.insertMultipleRecords(jsSelectedChemical, DatabaseHelper.TABLE_SELECTED_CHEMICALS);
			houstonFlowFunctions.insertMultipleRecords(jsSelectedChemical, DatabaseHelper.TABLE_CHEMICALS);
			//JSONArray jsChemical = houstonFlowFunctions.getAllSelectedChemicals(DatabaseHelper.TABLE_SELECTED_CHEMICALS,serviceID_new);
			JSONArray jsChemical1 = houstonFlowFunctions.getAllSelectedChemicals(DatabaseHelper.TABLE_CHEMICALS,serviceID_new,"Residential");

			Log.e("jsChemical in residential", "jsChemical in residential.."+jsChemical1);
			//checkList.clear();
			//listChemicals.clear();
			listSelectedChemicals.clear();

		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		residential_pest_DTO.setServiceID_new(serviceID_new);
		String residentialString =	gson.toJson(residential_pest_DTO);
		try 
		{
			//LoggerUtil.e("generalInfoDTO?","generalInfoDTO..1.."+generalInfoDTO.getCheckNo());
			int in = houstonFlowFunctions.update(generalInfoDTO);
			//	Toast.makeText(getApplicationContext(), in+"", Toast.LENGTH_LONG).show();
			//LoggerUtil.e("generalInfoDTO?","generalInfoDTO..2.."+generalInfoDTO.getCheckNo());
			//LoggerUtil.e("updated?",""+in);
			if(houstonFlowFunctions.isAlreadyAdded(serviceID_new,DatabaseHelper.TABLE_HR_PEST_INFO))
			{
				//LoggerUtil.e("Updateed","Update");
				int u =	HoustonFlowFunctions.updateResidential_Pest(residential_pest_DTO);

				//LoggerUtil.e("Updateed", u+" ...Update residential");
			}else
			{
				//LoggerUtil.e("New Record","New Record");
				long a = houstonFlowFunctions.addServicesForResidential(new JSONObject(residentialString));
				//Log.e("New Record Added", a+"");
			}
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void insertUpdateLog() 
	{
		Gson gson=new Gson();
		// TODO Auto-generated method stub

		logDTO.setResidetial_Insp("True");
		logDTO.setSID(serviceID_new);
		//String logString =	gson.toJson(logDTO);
		try
		{
			//if(logArray.length()>0)
			//{
			int u =	HoustonFlowFunctions.updateLog(logDTO);
			LoggerUtil.e("log updated. values", ""+u);

			JSONArray logArray=houstonFlowFunctions.getAllSelectedLogRow(serviceID_new);
			//Log.e("", "logArray...."+logArray);

			//}
			//else
			//{
			//	long a = HoustonFlowFunctions.addLog(new JSONObject(logString));
			//	LoggerUtil.e("log added1. values", ""+a);
			//}
		}
		catch(Exception e)
		{
			Log.e("logArray", "logArray...."+e);
		}
	}



	public void setValuesToResidentialDTO() 
	{
		// TODO Auto-generated method stub
		residential_pest_DTO.setResidentialId(strResidentialId);
		residential_pest_DTO.setServiceID_new(serviceID_new);
		//set service for
		residential_pest_DTO.setServiceFor(strServiceFor);

		//set Areas inspected
		residential_pest_DTO.setAreasInspected(strAreasInspected);

		//set inspected treated for
		residential_pest_DTO.setInsectActivity(strInspectedadnTreatedFor);
		residential_pest_DTO.setAnts(strEdttxtInspectedandTreatedFor1);
		residential_pest_DTO.setOtherPest(strEdttxtInspectedandTreatedFor2);
		//set outside service performed
		residential_pest_DTO.setOutsideOnly(strOutsideService);

		//for pest activity by zone
		// Log.e("", "strpestActivityByZone..set."+strpestActivityByZone);
		residential_pest_DTO.setPestActivity(strpestActivityByZone);
		residential_pest_DTO.setFence(strEdttxtFenceLinesByZone);
		residential_pest_DTO.setBackYard(strEdttxtBackYardByZone);
		residential_pest_DTO.setFrontYard(strEdttxtFrontyardByZone);
		residential_pest_DTO.setExteriorPerimeter(strEdttxtExteriorPerimeterHouseByZone);
		residential_pest_DTO.setRoofline(strEdttxtRoofGutterLine);
		residential_pest_DTO.setGarage(strEdttxtGarageBYZone);
		residential_pest_DTO.setAttic(strEdttxtAtticByZone);
		residential_pest_DTO.setInteriorHouse(strEdttxtInteriorOfHouseByZone);

		//for pest control services
		residential_pest_DTO.setPestControlServices(strpestControlServices);

		//for termite services
		// check box
		//Log.e("","strCheckNumberofMonitoringDevicesInspectedTermiteServices..."+strCheckNumberofMonitoringDevicesInspectedTermiteServices);
		residential_pest_DTO.setFencePest(strCheckFenceInspected);
		residential_pest_DTO.setNumberPest(strCheckNumberofMonitoringDevicesInspectedTermiteServices);
		residential_pest_DTO.setWoodPest(strCheckMonitoringDevicesTermiteServices);
		residential_pest_DTO.setNumberActivity(strCheckTermiteMonitoringDevicereplaced);
		residential_pest_DTO.setExteriorPest(strCheckExteriorPerimeterofhouseTermiteServices);
		residential_pest_DTO.setAtticPest(strCheckAttictermiteServices);
		residential_pest_DTO.setInteriorPest(strCheckInteriorOfHousetermiteServices);

		//edit text
		residential_pest_DTO.setFenceActivity(strRadioFenceInspected);
		residential_pest_DTO.setNumberActivitytext(strMonitorInspected);
		residential_pest_DTO.setWoodActivity(strRadioMonitoringDevicesTermiteServices);
		residential_pest_DTO.setWoodOption(strRadioTermiteMonitoringDevicereplaced);
		residential_pest_DTO.setExteriorActivity(strRadioExteriorPerimeterofhouseTermiteServices);
		residential_pest_DTO.setAtticActivity(strRadioAttictermiteServices);
		residential_pest_DTO.setInteriorActivity(strRadioInteriorOfHouse);

		// for rodent services
		//check box
		residential_pest_DTO.setRoofRodent(strCheckRoofPitches);
		residential_pest_DTO.setRoofVentRodent(strCheckRoofVents);
		residential_pest_DTO.setFlashing(strCheckSoffitVents);
		residential_pest_DTO.setFascia(strCheckWeepHoles);
		residential_pest_DTO.setSiding(strCheckSidingofHouse);

		residential_pest_DTO.setBreezway(strCheckBreezewayGarage);
		residential_pest_DTO.setGarageDoor(strCheckGarageDoor);
		residential_pest_DTO.setDryerVent(strCheckAcDryerVentsPhoneline);
		residential_pest_DTO.setNumberLive(strCheckNumberofLiveAnimalTrapsinstalled);
		residential_pest_DTO.setNumberSnap(strCheckNumberofSnaptrapsplaced);

		//edit text
		residential_pest_DTO.setRoofNeeds(strRadioRoofPitches);
		residential_pest_DTO.setRoofVentNeeds(strRadioRoofVents);
		residential_pest_DTO.setFlashingNeeds(strRadioSoffitVents);
		residential_pest_DTO.setFasciaNeeds(strRadioWeepHoles);
		residential_pest_DTO.setSidingNeeds(strRadioSidingofHouse);
		residential_pest_DTO.setBreezwayNeeds(strRadioBreezewayGarage);
		residential_pest_DTO.setGarageDoorNeeds(strRadioGarageDoor);
		residential_pest_DTO.setDryerVentNeeds(strRadioAcDryerVentsPhoneline);
		residential_pest_DTO.setNumberLiveNeeds(strRadioNumberofLiveAnimalTraps);
		residential_pest_DTO.setNumberSnapNeeds(strRadioNumberofSnaptrapsplaced);
		//for mosquito
		residential_pest_DTO.setPlusMosquitoDetail(strMosquito);
		//Log.e("strEdttxtMosquitoServices1...", ""+strEdttxtMosquitoServices1+".."+strEdttxtMosquitoServices2);
		residential_pest_DTO.setDurationTime(strEdttxtMosquitoServices1);
		residential_pest_DTO.setSystemRuns(strEdttxtMosquitoServices2);
		// for recommendations
		//Log.e("", ""+strCheckTreeBranches);
		residential_pest_DTO.setTreeBranches(strCheckTreeBranches);
		residential_pest_DTO.setFirewood(strCheckFirewood);
		residential_pest_DTO.setDebrisCrawl(strCheckDebrisInCrawl);
		residential_pest_DTO.setExcessivePlant(strCheckExcessiveplant);
		residential_pest_DTO.setSoil(strCheckSoilAbove);
		residential_pest_DTO.setWoodSoil(strCheckWoodSoil);
		residential_pest_DTO.setDebrisRoof(strCheckDebrisRoof);
		residential_pest_DTO.setStandingWater(strCheckStandingWater);
		residential_pest_DTO.setMoistureProblem(strCheckMoistureProblem);
		residential_pest_DTO.setOpenings(strCheckOpeningPlumbing);
		residential_pest_DTO.setExcessiveGaps(strCheckExcessiveGaps);
		residential_pest_DTO.setLeakyPlumbing(strCheckLaekyPlumbing);
		residential_pest_DTO.setGarbageCans(strCheckGarbageCans);
		residential_pest_DTO.setMoistureDamaged(strCheckMoistureDamage);
		residential_pest_DTO.setGroceryBags(strCheckGrocerybags);
		residential_pest_DTO.setPetFood(strCheckPetFood);
		residential_pest_DTO.setRecycledItems(strCheckRecycledItems);
		residential_pest_DTO.setExcessiveStorage(strCheckExcessiveStorage);

		//set values for invoice
		residential_pest_DTO.setTechnicianComments(strEdttxtTechComments);
		residential_pest_DTO.setTechnician(strEdttxtServiceTech);
		residential_pest_DTO.setEmpNo(strEdttxtEmp);
		residential_pest_DTO.setCustomer(strEdttxtCustomer);
		residential_pest_DTO.setDate(strEdttxtdate);
		residential_pest_DTO.setAmount(strEdttxtAmount);

		generalInfoDTO.setServiceID_new(serviceID_new);
		generalInfoDTO.setInv_value(strEdttxtInvValue);
		generalInfoDTO.setProd_value(strEdttxtProdValue);
		generalInfoDTO.setTax_value(strEdttxtTax);
		generalInfoDTO.setTotal(generalInfoDTO.getInv_value());
		generalInfoDTO.setCheckNo(strEdttxtCheckNo);
		generalInfoDTO.setLicenseNo(strEdttxtDrivingLicence);

		residential_pest_DTO.setCheckNo(strEdttxtCheckNo);
		residential_pest_DTO.setLicenseNo(strEdttxtDrivingLicence);


		if(!CommonFunction.checkString(strEdtExpirationDate,"").equals(""))
		{
			generalInfoDTO.setExpirationDate(strEdtExpirationDate);
		}
		else
		{
			generalInfoDTO.setExpirationDate("01/01/1900");
		}
		if(tempStoreBackImageName.size() != 0)
		{
			generalInfoDTO.setCheckBackImage(tempStoreBackImageName.get(0));
		}
		if(tempStoreFrontImageName.size() != 0)
		{
			generalInfoDTO.setCheckFrontImage(tempStoreFrontImageName.get(0));
		}

		residential_pest_DTO.setIsCompleted("true");
		//Log.e("strRadioInvoice...", "strRadioInvoice..."+strRadioInvoice);
		residential_pest_DTO.setPaymentType(strRadioInvoice);
		//residential_pest_DTO.setCheckNo(strEdttxtCheckNo);
		//residential_pest_DTO.setLicenseNo(strEdttxtDrivingLicence);
		residential_pest_DTO.setCreate_By(userDTO.getPestPackId());
		residential_pest_DTO.setUpdate_Date(CommonFunction.getCurrentDateTime());
		residential_pest_DTO.setCreate_Date(CommonFunction.getCurrentDateTime());

		residential_pest_DTO.setTimeout(CommonFunction.getCurrentTime());
		residential_pest_DTO.setTimeOutDateTime(CommonFunction.getCurrentDateTime());
		//"010101"
		residential_pest_DTO.setTimeIn(generalInfoDTO.getTimeIn());
		residential_pest_DTO.setTimeInDateTime(generalInfoDTO.getTimeInDateTime());

		//driving licence is not there
	}
	private void getValuesForInvoice() 
	{
		// TODO Auto-generated method stub.getC
		strEdttxtTechComments=edttxtTechComments.getText().toString().trim();
		strEdttxtServiceTech=edttxtServiceTech.getText().toString().trim();
		strEdttxtEmp=edttxtEmp.getText().toString().trim();
		strEdttxtCustomer=edttxtCustomer.getText().toString().trim();
		//strEdttxtdate=edttxtdate.getText().toString();
		strEdttxtdate=generalInfoDTO.getServiceDate().trim();
		strEdttxtInvValue=edttxtInvValue.getText().toString().trim();
		strEdttxtProdValue=edttxtProdValue.getText().toString().trim();
		strEdttxtTax=edttxtTax.getText().toString().trim();
		strEdttxtTotal=edttxtTotal.getText().toString().trim();

		//strEdttxtAmount=edttxtAmount.getText().toString().trim();
		if(!edttxtAmount.getText().toString().trim().equals(""))
		{
			strEdttxtAmount=edttxtAmount.getText().toString().trim();
		}
		else
		{
			strEdttxtAmount="0";
		}
		strEdttxtDrivingLicence=edttxtDrivingLicence.getText().toString().trim();
		strEdttxtCheckNo=edttxtCheckNo.getText().toString().trim();
		strEdtExpirationDate=edttxtExpirationDate.getText().toString().trim();

		int idInvoiceRadio = radioGroupInvoice.getCheckedRadioButtonId();
		RadioButton radioButtonInvoice = (RadioButton) findViewById(idInvoiceRadio);
		strRadioInvoice= radioButtonInvoice.getText().toString().trim();
		//Log.e("strRadioInvoice..get value.",strRadioInvoice+"");
	}
	private void getValuesForRecommendations() 
	{
		// TODO Auto-generated method stub
		if(checkBoxBranchHouse.isChecked())
		{
			strCheckTreeBranches = checkBoxBranchHouse.getText().toString().trim();
			//Log.e("", "strCheckTreeBranches inside if"+strCheckTreeBranches);
		}
		else
		{
			strCheckTreeBranches="".trim();
		}
		if(checkBoxFirewood.isChecked())
		{
			strCheckFirewood = checkBoxFirewood.getText().toString().trim();
		}
		else
		{
			strCheckFirewood="".trim();
		}
		if(checkBoxDebrisCrawl.isChecked())
		{
			strCheckDebrisInCrawl = checkBoxDebrisCrawl.getText().toString().trim();
		}
		else
		{
			strCheckDebrisInCrawl="".trim();
		}
		if(checkBoxExcessiveplant.isChecked())
		{
			strCheckExcessiveplant = checkBoxExcessiveplant.getText().toString().trim();

		}
		else
		{
			strCheckExcessiveplant="".trim();
		}
		if(checkBoxSoilAbove.isChecked())
		{
			strCheckSoilAbove = checkBoxSoilAbove.getText().toString().trim();
		}
		else
		{
			strCheckSoilAbove="".trim();
		}
		if(checkBoxWoodSoil.isChecked())
		{
			strCheckWoodSoil = checkBoxWoodSoil.getText().toString().trim();

		}
		else
		{
			strCheckWoodSoil="".trim();
		}
		if(checkBoxDebrisRoof.isChecked())
		{
			strCheckDebrisRoof = checkBoxDebrisRoof.getText().toString().trim();
		}
		else
		{
			strCheckDebrisRoof="".trim();
		}
		if(checkBoxStandingwater.isChecked())
		{
			strCheckStandingWater = checkBoxStandingwater.getText().toString().trim();

		}
		else
		{
			strCheckStandingWater="".trim();
		}
		if(checkBoxMoistureProblem.isChecked())
		{
			strCheckMoistureProblem = checkBoxMoistureProblem.getText().toString().trim();

		}
		else
		{
			strCheckMoistureProblem="".trim();
		}
		if(checkBoxOpeningspumbing.isChecked())
		{
			strCheckOpeningPlumbing = checkBoxOpeningspumbing.getText().toString().trim();

		}
		else
		{
			strCheckOpeningPlumbing="".trim();
		}
		if(checkBoxExcessiveGaps.isChecked())
		{
			strCheckExcessiveGaps = checkBoxExcessiveGaps.getText().toString().trim();

		}
		else
		{
			strCheckExcessiveGaps="".trim();
		}
		if(checkBoxLeackPumbing.isChecked())
		{
			strCheckLaekyPlumbing = checkBoxLeackPumbing.getText().toString().trim();

		}
		else
		{
			strCheckLaekyPlumbing="".trim();
		}
		if(checkBoxgarbageCans.isChecked())
		{
			strCheckGarbageCans = checkBoxgarbageCans.getText().toString().trim();

		}
		else
		{
			strCheckGarbageCans="".trim();
		}
		if(checkBoxMoisrtureDamage.isChecked())
		{
			strCheckMoistureDamage = checkBoxMoisrtureDamage.getText().toString().trim();

		}
		else
		{
			strCheckMoistureDamage="".trim();
		}
		if(checkBoxGroceryBags.isChecked())
		{
			strCheckGrocerybags = checkBoxGroceryBags.getText().toString().trim();

		}
		else
		{
			strCheckGrocerybags="".trim();
		}
		if(checkBoxPetFood.isChecked())
		{
			strCheckPetFood = checkBoxPetFood.getText().toString().trim();

		}
		else
		{
			strCheckPetFood="".trim();
		}
		if(checkBoxRecycledItems.isChecked())
		{
			strCheckRecycledItems = checkBoxRecycledItems.getText().toString().trim();

		}
		else
		{
			strCheckRecycledItems="".trim();
		}
		if(checkBoxExcessiveStorage.isChecked())
		{
			strCheckExcessiveStorage = checkBoxExcessiveStorage.getText().toString().trim();

		}
		else
		{
			strCheckExcessiveStorage="".trim();
		}
	}
	private void getValuesMosquitoServices() 
	{
		// TODO Auto-generated method stub
		strMosquito="";



		for(int i=0;i < mosquitoCheckboxList.size();i++)
		{
			//	LoggerUtil.e("inside for loop 1..", "inside for loop 1..");
			if(mosquitoCheckboxList.get(i).isChecked())
			{

				strMosquito=strMosquito+","+mosquitoCheckboxList.get(i).getText().toString().trim();	

			}
		}
		strMosquito=CommonFunction.removeFirstCharIF(strMosquito, ",").trim();
		if(mosquitoCheckboxList.get(3).isChecked())
		{
			strEdttxtMosquitoServices1=edttxtMosquitoServices1.getText().toString().trim();
		}
		else
		{
			strEdttxtMosquitoServices1="";
		}
		if(mosquitoCheckboxList.get(7).isChecked())
		{
			strEdttxtMosquitoServices2=edttxtMosquitoServices2.getText().toString().trim();
		}
		else
		{
			strEdttxtMosquitoServices2="";
		}

	}
	private void getValuesRodentServices() 
	{
		// TODO Auto-generated method stub
		if(checkBoxRoofPitches.isChecked())
		{
			strCheckRoofPitches = checkBoxRoofPitches.getText().toString().trim();
			int idRoofPitches = radioGroupRoofPitches.getCheckedRadioButtonId();
			//Log.e("", "idRoofPitches..."+idRoofPitches);

			if(idRoofPitches != -1)
			{

				RadioButton radioRoofPitches = (RadioButton) findViewById(idRoofPitches);
				strRadioRoofPitches = radioRoofPitches.getText().toString().trim();
			}
		}
		else
		{
			strCheckRoofPitches="".trim();
		}

		if(checkBoxRoofVents.isChecked())
		{
			strCheckRoofVents = checkBoxRoofVents.getText().toString().trim();

			int idRoofVents = radioGroupRoofVents.getCheckedRadioButtonId();
			if(idRoofVents != -1)
			{

				RadioButton radioRoofVents = (RadioButton) findViewById(idRoofVents);
				strRadioRoofVents = radioRoofVents.getText().toString().trim();
			}
		}
		else
		{
			strCheckRoofVents="".trim();
		}

		if(checkBoxSoffitVents.isChecked())
		{
			strCheckSoffitVents = checkBoxSoffitVents.getText().toString().trim();

			int idSoffitVents = radioGroupSoffitvents.getCheckedRadioButtonId();
			if(idSoffitVents != -1)
			{

				RadioButton radioSoffitVents = (RadioButton) findViewById(idSoffitVents);
				strRadioSoffitVents = radioSoffitVents.getText().toString().trim();
			}
		}
		else
		{
			strCheckSoffitVents="".trim();
		}

		if(checkBoxWeepHoles.isChecked())
		{
			strCheckWeepHoles = checkBoxWeepHoles.getText().toString().trim();

			int idWeepHoles = radioGroupWeepHoles.getCheckedRadioButtonId();
			if(idWeepHoles != -1)
			{

				RadioButton radioWeepHoles = (RadioButton) findViewById(idWeepHoles);
				strRadioWeepHoles = radioWeepHoles.getText().toString().trim();
			}
		}
		else
		{
			strCheckWeepHoles="".trim();
		}

		if(checkBoxSidingofHouse.isChecked())
		{
			strCheckSidingofHouse = checkBoxSidingofHouse.getText().toString().trim();

			int idSidingofHouse = radioGroupSidingHouse.getCheckedRadioButtonId();
			if(idSidingofHouse != -1)
			{

				RadioButton radioSidingofHouse = (RadioButton) findViewById(idSidingofHouse);
				strRadioSidingofHouse = radioSidingofHouse.getText().toString().trim();
			}
		}
		else
		{
			strCheckSidingofHouse="".trim();
		}

		if(checkBoxBreezewayGarage.isChecked())
		{
			strCheckBreezewayGarage = checkBoxBreezewayGarage.getText().toString().trim();

			int idBreezewayGarage = radioGroupBreezyWay.getCheckedRadioButtonId();

			if(idBreezewayGarage != -1)
			{
				RadioButton radioBreezewayGarage = (RadioButton) findViewById(idBreezewayGarage);
				strRadioBreezewayGarage = radioBreezewayGarage.getText().toString().trim();
			}
		}
		else
		{
			strCheckBreezewayGarage="".trim();
		}

		if(checkBoxGarageDoor.isChecked())
		{
			strCheckGarageDoor = checkBoxGarageDoor.getText().toString().trim();

			int idGarageDoor = radioGroupGarageDoor.getCheckedRadioButtonId();
			if(idGarageDoor != -1)
			{
				RadioButton radioGarageDoor= (RadioButton) findViewById(idGarageDoor);
				strRadioGarageDoor = radioGarageDoor.getText().toString().trim();
			}
		}
		else
		{
			strCheckGarageDoor="".trim();
		}

		if(checkBoxAcDryerVentsPhoneline.isChecked())
		{
			strCheckAcDryerVentsPhoneline = checkBoxAcDryerVentsPhoneline.getText().toString().trim();

			int idACDRYER = radioGroupACDRYER.getCheckedRadioButtonId();
			if(idACDRYER != -1)
			{

				RadioButton radioACDRYER = (RadioButton) findViewById(idACDRYER);
				strRadioAcDryerVentsPhoneline = radioACDRYER.getText().toString().trim();
			}}
		else
		{
			strCheckAcDryerVentsPhoneline="".trim();
		}
		if(checkBoxNumberofLiveAnimalTrapsinstalled.isChecked())
		{
			strCheckNumberofLiveAnimalTrapsinstalled = checkBoxNumberofLiveAnimalTrapsinstalled.getText().toString().trim();
			strRadioNumberofLiveAnimalTraps = edttxtNumberofLiveAnimalTrapsinstalled.getText().toString().trim();

		}
		else
		{
			strCheckNumberofLiveAnimalTrapsinstalled="".trim();
		}

		if(checkBoxNumberofSnaptrapsplaced.isChecked())
		{
			strCheckNumberofSnaptrapsplaced = checkBoxNumberofSnaptrapsplaced.getText().toString().trim();
			strRadioNumberofSnaptrapsplaced = edttxtNumberofSnaptrapsplaced.getText().toString().trim();

		}
		else
		{
			strCheckNumberofSnaptrapsplaced="".trim();
		}
	}
	private void getValuesTermiteServices() 
	{
		// TODO Auto-generated method stub
		if(checkBoxFenceInspected.isChecked())
		{
			strCheckFenceInspected = checkBoxFenceInspected.getText().toString().trim();

			int idFenceInspected = radioGroupFenceInspected.getCheckedRadioButtonId();
			if(idFenceInspected != -1)
			{
				//Log.e("idFenceInspected", ""+idFenceInspected);

				RadioButton radioButtonFence = (RadioButton) findViewById(idFenceInspected);
				strRadioFenceInspected = radioButtonFence.getText().toString().trim();
			}
		}
		else
		{
			strCheckFenceInspected="".trim();
		}

		if(checkBoxNumberofMonitoringDevicesInspectedTermiteServices.isChecked())
		{
			strCheckNumberofMonitoringDevicesInspectedTermiteServices = checkBoxNumberofMonitoringDevicesInspectedTermiteServices.getText().toString().trim();

			strMonitorInspected = edttxtMonitorReplaced.getText().toString().trim();
		}
		else
		{
			strCheckNumberofMonitoringDevicesInspectedTermiteServices="".trim();
		}

		if(checkBoxMonitoringDevicesTermiteServices.isChecked())
		{
			strCheckMonitoringDevicesTermiteServices = checkBoxMonitoringDevicesTermiteServices.getText().toString().trim();

			int idMoniterDevices = radioGroupMonitoringDevices.getCheckedRadioButtonId();

			if(idMoniterDevices != -1)
			{
				RadioButton radioButtonMonitorDevices  = (RadioButton) findViewById(idMoniterDevices);
				strRadioMonitoringDevicesTermiteServices= radioButtonMonitorDevices .getText().toString().trim();
			}
		}
		else
		{
			strCheckMonitoringDevicesTermiteServices="";
		}

		if(checkBoxTermiteMonitoringDevicereplaced.isChecked())
		{
			strCheckTermiteMonitoringDevicereplaced = checkBoxTermiteMonitoringDevicereplaced.getText().toString().trim();

			int idTermiteMoniter = radioGroupTermiteMonitoringDevices.getCheckedRadioButtonId();

			if(idTermiteMoniter != -1)
			{
				RadioButton radioButtonTermiteMoniter  = (RadioButton) findViewById(idTermiteMoniter);
				strRadioTermiteMonitoringDevicereplaced= radioButtonTermiteMoniter .getText().toString().trim();
				//Log.e("strRadioTermiteMonitoringDevicereplaced..", "strRadioTermiteMonitoringDevicereplaced..."+strRadioTermiteMonitoringDevicereplaced);
			}
		}
		else
		{
			strCheckTermiteMonitoringDevicereplaced="".trim();
		}

		if(checkBoxExteriorPerimeterofhouseTermiteServices.isChecked())
		{
			strCheckExteriorPerimeterofhouseTermiteServices = checkBoxExteriorPerimeterofhouseTermiteServices.getText().toString().trim();

			int idExteriorPerimeter = radioGroupExteriorPerimeterOfHouse.getCheckedRadioButtonId();
			if(idExteriorPerimeter != -1)
			{
				RadioButton radioExteriorPerimeter  = (RadioButton) findViewById(idExteriorPerimeter);
				strRadioExteriorPerimeterofhouseTermiteServices= radioExteriorPerimeter .getText().toString().trim();
			}
		}
		else
		{
			strCheckExteriorPerimeterofhouseTermiteServices="".trim();
		}

		if(checkBoxAttictermiteServices.isChecked())
		{
			strCheckAttictermiteServices = checkBoxAttictermiteServices.getText().toString().trim();

			int idAttic = radioGroupAttic.getCheckedRadioButtonId();
			if(idAttic != -1)
			{
				RadioButton radioAttic  = (RadioButton) findViewById(idAttic);
				strRadioAttictermiteServices= radioAttic .getText().toString().trim();
			}}
		else
		{
			strCheckAttictermiteServices="".trim();
		}

		if(checkBoxInteriorOfHousetermiteServices.isChecked())
		{
			strCheckInteriorOfHousetermiteServices = checkBoxInteriorOfHousetermiteServices.getText().toString().trim();

			int idInteriorHouse = radioGroupInteriorHouse.getCheckedRadioButtonId();
			if(idInteriorHouse != -1)
			{
				RadioButton radioInteriorHouse  = (RadioButton) findViewById(idInteriorHouse);
				strRadioInteriorOfHouse = radioInteriorHouse .getText().toString().trim();
			}
		}
		else
		{
			strCheckInteriorOfHousetermiteServices="".trim();
		}
	}

	private void getValuesPestControlServices() 
	{
		// TODO Auto-generated method stub
		strpestControlServices="";
		strOutsidepestControlServices="";
		strInsidepestControlServices="";

		for(int i=0;i < pestControlServicesCheckboxList.size();i++)
		{
			//LoggerUtil.e("inside for loop 1..", "inside for loop 1..");
			if(pestControlServicesCheckboxList.get(i).isChecked())
			{
				strpestControlServices=strpestControlServices+","+pestControlServicesCheckboxList.get(i).getText().toString();	
			}
		}
		strpestControlServices=CommonFunction.removeFirstCharIF(strpestControlServices, ",").trim();
		
		for(int i=0;i < OutsidepestControlServicesCheckboxList.size();i++)
		{
			//LoggerUtil.e("inside for loop 1..", "inside for loop 1..");
			if(OutsidepestControlServicesCheckboxList.get(i).isChecked())
			{
				strOutsidepestControlServices=strOutsidepestControlServices+","+OutsidepestControlServicesCheckboxList.get(i).getText().toString();	
			}
		}
		strOutsidepestControlServices=CommonFunction.removeFirstCharIF(strOutsidepestControlServices, ",").trim();
	
		for(int i=0;i < InsidepestControlServicesCheckboxList.size();i++)
		{
			//LoggerUtil.e("inside for loop 1..", "inside for loop 1..");
			if(InsidepestControlServicesCheckboxList.get(i).isChecked())
			{
				strInsidepestControlServices=strInsidepestControlServices+","+InsidepestControlServicesCheckboxList.get(i).getText().toString();	
			}
		}
		strInsidepestControlServices=CommonFunction.removeFirstCharIF(strInsidepestControlServices, ",").trim();
	
		
	}
	private void getValuesPestActivitybyZone() 
	{
		// TODO Auto-generated method stub
		strpestActivityByZone="";


		for(int i=0;i < PestActivityZoneCheckboxList.size();i++)
		{
			//LoggerUtil.e("inside for loop 1..", "inside for loop 1..");
			//strpestActivityByZone="";
			if(PestActivityZoneCheckboxList.get(i).isChecked())
			{
				strpestActivityByZone=strpestActivityByZone+","+PestActivityZoneCheckboxList.get(i).getText().toString();	
			}
		}
		strpestActivityByZone=CommonFunction.removeFirstCharIF(strpestActivityByZone, ",").trim();
		//	Log.e("", "strpestActivityByZone..get."+strpestActivityByZone);

		strEdttxtFenceLinesByZone = edttxtFenceLinesByZone.getText().toString().trim();
		strEdttxtFrontyardByZone= edttxtFrontyardByZone.getText().toString().trim();
		strEdttxtBackYardByZone = edttxtBackYardByZone.getText().toString().trim();
		strEdttxtExteriorPerimeterHouseByZone = edttxtExteriorPerimeterHouseByZone.getText().toString().trim();
		strEdttxtRoofGutterLine = edttxtRoofGutterLine.getText().toString().trim();
		strEdttxtGarageBYZone = edttxtGarageBYZone.getText().toString().trim();
		strEdttxtAtticByZone = edttxtAtticByZone.getText().toString().trim();
		strEdttxtInteriorOfHouseByZone = edttxtInteriorOfHouseByZone.getText().toString().trim();
	}

	private void getValueForoutsideOnlyServiceFor() 
	{
		// TODO Auto-generated method stub
		strOutsideService="";
		if(checkBoxOutsideOnlySerivcePerformed.isChecked())
		{
			strOutsideService = "true";
		}
	}

	private void grtValuesforInspectedTreatedFor() 
	{
		// TODO Auto-generated method stub
		strInspectedadnTreatedFor="";
		for(int i=0;i < InspectedTreatedForCheckboxList.size();i++)
		{

			if(InspectedTreatedForCheckboxList.get(i).isChecked())
			{
				strInspectedadnTreatedFor=strInspectedadnTreatedFor+","+InspectedTreatedForCheckboxList.get(i).getText().toString();	
			}
		}
		strInspectedadnTreatedFor=CommonFunction.removeFirstCharIF(strInspectedadnTreatedFor, ",").trim();
		//	LoggerUtil.e("inside for loop 1..", "strInspectedadnTreatedFor...."+strInspectedadnTreatedFor);
		//strAnts=checkBoxAnts.getText().toString();
		//strOtherPest=checkBoxOtherPests.getText().toString();
		strEdttxtInspectedandTreatedFor1=edttxtInspectedandTreatedFor1.getText().toString().trim();
		strEdttxtInspectedandTreatedFor2=edttxtInspectedandTreatedFor2.getText().toString().trim();
		//	Log.e("", "strEdttxtInspectedandTreatedFor1...."+strEdttxtInspectedandTreatedFor1+"..."+strEdttxtInspectedandTreatedFor2);
	}
	private void getValuesformAreasInspected() 
	{
		// TODO Auto-generated method stub
		strAreasInspected="";

		for(int i=0;i < AreasInspectedCheckboxList.size();i++)
		{
			//LoggerUtil.e("inside for loop 1..", "inside for loop 1..");
			if(AreasInspectedCheckboxList.get(i).isChecked())
			{

				strAreasInspected=strAreasInspected+","+AreasInspectedCheckboxList.get(i).getText().toString();	
			}
		}
		strAreasInspected=CommonFunction.removeFirstCharIF(strAreasInspected, ",").trim();
	}

	private void getValuesFromServiceForHeader() 
	{
		// TODO Auto-generated method stub
		strServiceFor="";

		for(int i=0;i < serviceForCheckboxList.size();i++)
		{
			//	LoggerUtil.e("inside for loop 1..", "inside for loop 1..");
			if(serviceForCheckboxList.get(i).isChecked())
			{

				strServiceFor=strServiceFor+","+serviceForCheckboxList.get(i).getText().toString();	
			}
		}
		strServiceFor=CommonFunction.removeFirstCharIF(strServiceFor, ",").trim();

	}
	/* (non-Javadoc)
	 * @see android.app.Activity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		/*Intent intent=new Intent(ResidentialPestControlServiceReport.this, GeneralInformationActivity.class);
		//Log.e("", ""+serviceList.get(arg2));
		intent.putExtra("serviceIdNew",serviceID_new);
		intent.putExtra("userDTO", userDTO);
		intent.putExtra("back", "yes");
		startActivity(intent);*/
		finish();
		super.onBackPressed();
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.residential_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		/*if (id == R.id.action_back) {


			if (SystemClock.elapsedRealtime() - mLastClickTime < 4000) 
			{
				return true;
			}
			mLastClickTime = SystemClock.elapsedRealtime();
			Intent intent=new Intent(ResidentialPestControlServiceReport.this, GeneralInformationActivity.class);
			//Log.e("", ""+serviceList.get(arg2));
			intent.putExtra("serviceIdNew",serviceID_new);
			intent.putExtra("userDTO", userDTO);
			startActivity(intent);
			finish();	
			return true;
		}*/
		if(id==android.R.id.home)
		{
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_Gallery_ACTIVITY_CODE_Front) 
		{
			if (resultCode == GeneralInformationActivity.RESULT_OK) 
			{
				if(listFrontImg.size()>0)
				{
					listFrontImg.remove(0);

					if(tempFrontImagList.size()>0)
					{
						tempFrontImagList.remove(0);

					}
					if(tempStoreFrontImageName.size()>0)
					{
						tempStoreFrontImageName.remove(0);
					}
					RefreshFrontImage();
					listFrontImg.clear();
				}
				Toast.makeText(this, "Picture is taken from gallery", Toast.LENGTH_SHORT).show();
				imageUri=data.getData();
				try {
					//bitmapFromG = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
					Bitmap outputFront=decodeUri(imageUri);
					CheckFrontImageName = generalInfoDTO.getServiceID_new()+"_CheckFront.jpg";
					CommonFunction.saveBitmap(outputFront, getApplicationContext(), CheckFrontImageName);
					tempStoreFrontImageName.add(CheckFrontImageName);
					RefreshFrontImage();
				} 
				catch (FileNotFoundException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		if (requestCode == REQUEST_Gallery_ACTIVITY_CODE_Back) 
		{
			if (resultCode == GeneralInformationActivity.RESULT_OK) 
			{
				if(listBackImg.size()>0)
				{
					listBackImg.remove(0);

					if(tempBackImagList.size()>0)
					{
						tempBackImagList.remove(0);

					}
					if(tempStoreBackImageName.size()>0)
					{
						tempStoreBackImageName.remove(0);
					}
					RefreshBackImage();
					listBackImg.clear();
				}
				Toast.makeText(this, "Picture is  taken from gallery", Toast.LENGTH_SHORT).show();
				imageUri=data.getData();
				try 
				{
					//bitmapFromG = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
					Bitmap outputBack=decodeUri(imageUri);
					CheckBackImageName = generalInfoDTO.getServiceID_new()+"_CheckBack.jpg";
					CommonFunction.saveBitmap(outputBack, getApplicationContext(),CheckBackImageName);
					tempStoreBackImageName.add(CheckBackImageName);
					RefreshBackImage();
				} 
				catch (FileNotFoundException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
		if (requestCode == REQUEST_Camera_ACTIVITY_CODE_Front) 
		{
			if (resultCode == GeneralInformationActivity.RESULT_OK) 
			{
				if(listFrontImg.size()>0)
				{
					listFrontImg.remove(0);

					if(tempFrontImagList.size()>0)
					{
						tempFrontImagList.remove(0);

					}
					if(tempStoreFrontImageName.size()>0)
					{
						tempStoreFrontImageName.remove(0);
					}
					RefreshFrontImage();
					listFrontImg.clear();
				}
				imageCountFront++;
				Toast.makeText(HR_PestControlServiceReport.this, "Picture is  taken", Toast.LENGTH_SHORT);
				tempStoreFrontImageName.add(CheckFrontImageName);
				RefreshFrontImage();
				getfile(CheckFrontImageName);
				//	LoggerUtil.e("File Name on done camera", ImageName + "");
			}
			else if (resultCode == GeneralInformationActivity.RESULT_CANCELED) 
			{
				RefreshFrontImage();
			}
		}
		if (requestCode == REQUEST_Camera_ACTIVITY_CODE_Back) 
		{
			if (resultCode == GeneralInformationActivity.RESULT_OK) 
			{
				if(listBackImg.size()>0)
				{
					listBackImg.remove(0);

					if(tempBackImagList.size()>0)
					{
						tempBackImagList.remove(0);

					}
					if(tempStoreBackImageName.size()>0)
					{
						tempStoreBackImageName.remove(0);
					}
					RefreshBackImage();
					listBackImg.clear();
				}
				imageCountBack++;
				Toast.makeText(HR_PestControlServiceReport.this, "Picture is  taken", Toast.LENGTH_SHORT);
				tempStoreBackImageName.add(CheckBackImageName);
				RefreshBackImage();
				getfile(CheckBackImageName);
				//	LoggerUtil.e("File Name on done camera", ImageName + "");
			}
			else if (resultCode == GeneralInformationActivity.RESULT_CANCELED) 
			{
				RefreshBackImage();
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		JSONObject residential_pest_string=HoustonFlowFunctions.getTodaysReport(serviceID_new);
		residential_pest_DTO= new Gson().fromJson(residential_pest_string.toString(), HResidential_pest_DTO.class);

		//Log.e("onResume called","");
		super.onResume();
	}
}
//its always been my pleasure being with you in any circumstances.
