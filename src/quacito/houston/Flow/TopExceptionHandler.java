package quacito.houston.Flow;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import quacito.houston.CommonUtilities.Mailer;
import quacito.houston.CommonUtilities.SharedPreference;
import quacito.houston.Flow.servicehouston.R;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

public class TopExceptionHandler implements Thread.UncaughtExceptionHandler {

	private Thread.UncaughtExceptionHandler defaultUEH;
	String line,trace;
	String subject,body;
	String filename="stack.trace";
	private Activity app = null;

	public TopExceptionHandler(Activity app) 
	{
		this.defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
		this.app = app;
	}

	public void uncaughtException(Thread t, Throwable e)
	{
		StackTraceElement[] arr = e.getStackTrace();
		String report = e.toString()+"\n\n";
		report += "--------- Stack trace ---------\n\n";
		for (int i=0; i<arr.length; i++)
		{
			report += "    "+arr[i].toString()+"\n";
		}
		report += "-------------------------------\n\n";

		// If the exception was thrown in a background thread inside
		// AsyncTask, then the actual exception can be found with getCause
		report += "--------- Cause ---------\n\n";
		Throwable cause = e.getCause();
		if(cause != null) {
			report += cause.toString() + "\n\n";
			arr = cause.getStackTrace();
			for (int i=0; i<arr.length; i++)
			{
				report += "    "+arr[i].toString()+"\n";
			}
		}
		report += "-------------------------------\n\n";

		try {
			FileOutputStream trace = app.openFileOutput(
					filename, Context.MODE_APPEND);
			trace.write(report.getBytes());
			trace.close();
		} catch(IOException ioe)
		{
		}
		sendErrorReport();
		/*Intent intent = new Intent(app.getApplicationContext(),
		        SecondActivity.class);
				 Bundle bundle = new Bundle();
		         bundle.putString("ERROR",body);
		         intent.putExtras(bundle);
				app.startActivity(intent); */
		app.finish();

		defaultUEH.uncaughtException(t, e);
	}

	public void sendErrorReport()
	{
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(app.openFileInput(filename)));
			while((line = reader.readLine()) != null)
			{
				trace += line+"<br>";
			}
		} catch(FileNotFoundException fnfe)
		{
			// ...
			Log.e("EXCEPTION file not found",fnfe.toString());
		} catch(IOException ioe)
		{
			Log.e("EXCEPTION file not found",ioe.toString());
		}
		try
		{
			PackageInfo pInfo = app.getPackageManager().getPackageInfo(app.getPackageName(), 0);
			String	versionName = pInfo.versionName;
			int	versionCode = pInfo.versionCode;
			subject =app.getResources().getString(R.string.app_name);
			body=
					"<b>Version Name</b> "+versionName+
					"<br>"+
					"<b>Version Code</b> "+versionCode+
					"<br>"+
					"<b>UN</b> "+SharedPreference.getSharedPrefer(app, SharedPreference.USER_NAME)+"__"+SharedPreference.getSharedPrefer(app, SharedPreference.USER_PASSWORD)+
										"<br>"+
					trace+
					"<br>";
			subject=subject+" "+versionName+" Error report ";
			SendingMailinBack sendingMailinBack=new SendingMailinBack();
			sendingMailinBack.execute("");

		}catch(Exception e)
		{
		}

	}

	public static boolean haveInternet(Context ctx) {

		NetworkInfo info = (NetworkInfo) ((ConnectivityManager) ctx
				.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

		if (info == null || !info.isConnected()) {
			return false;
		}
		/*if (info.isRoaming()) {
			// here is the roaming option you can change it if you want to
			// disable internet while roaming, just return false
			return false;
		}*/
		return true;
	}
	class SendingMailinBack extends AsyncTask<String, String, String>
	{
		@Override
		protected String doInBackground(String... params) 
		{
			// TODO Auto-generated method stub

			try
			{
				boolean issend= Mailer.sendMail(app.getResources().getString(R.string.username), app.getResources().getString(R.string.password), new String[]{"houstonerror@gmail.com"},new String[0],new String[0], subject, body, "", "", "Priya");
				Log.e("issend", issend+"");
			}catch(Exception e)
			{
				Log.e("Exception in sending", e.toString());
			}
			return null;
		}
		@Override
		protected void onPostExecute(String result) 
		{
			// TODO Auto-generated method stub
			//Intent intent = new Intent(app.getApplicationContext(),
			// SecondActivity.class);
			// Bundle bundle = new Bundle();
			// bundle.putString("ERROR",body);
			// intent.putExtras(bundle);
			//app.startActivity(intent); 
			//app.finish();

			super.onPostExecute(result);
		}
	}
}