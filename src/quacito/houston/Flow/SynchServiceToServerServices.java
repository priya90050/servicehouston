package quacito.houston.Flow;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;
import quacito.houston.Flow.servicehouston.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import quacito.houston.CommonUtilities.CommonFunction;
import quacito.houston.CommonUtilities.LoggerUtil;
import quacito.houston.CommonUtilities.ParameterUtil;
import quacito.houston.CommonUtilities.SharedPreference;
import quacito.houston.DBhelper.DatabaseHelper;
import quacito.houston.DBhelper.HoustonFlowFunctions;
import quacito.houston.Flow.ServiceListAcitivity.DownloadAll_Chemical;
import quacito.houston.Flow.ServiceListAcitivity.DownloadAll_Chemical_WhenNoRecord;
import quacito.houston.Flow.ServiceListAcitivity.DownloadAll_Crew_Members;
import quacito.houston.Flow.ServiceListAcitivity.DownloadAll_Employee_Detail;
import quacito.houston.Flow.ServiceListAcitivity.DownloadAll_General_Information;
import quacito.houston.Flow.ServiceListAcitivity.DownloadAll_Residential_Pest;
import quacito.houston.Flow.ServiceListAcitivity.PostLogData;
import quacito.houston.model.AgreementMailIdDTO;
import quacito.houston.model.ChemicalDto;
import quacito.houston.model.Chemical_WhenNoRecordDTO;
import quacito.houston.model.HCommercial_pest_DTO;
import quacito.houston.model.CrewMemberDTO;
import quacito.houston.model.EmployeeDTO;
import quacito.houston.model.GeneralInfoDTO;
import quacito.houston.model.LogDTO;
import quacito.houston.model.HResidential_pest_DTO;
import quacito.houston.model.Selected_crewDTO;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class SynchServiceToServerServices extends Service{
	Timer timer;
	ParsingData parsingData;
	HoustonFlowFunctions houstonFlowFunctions;
	ArrayList<GeneralInfoDTO> serviceList;
	String fromDate,toDate;
	private final String NAMESPACE ="http://ipad-abc.com/";
	private final String URL="http://ipad-abc.com/Houston_Service.asmx?op";
	
	@Override
	public void onCreate() 
	{
	    super.onCreate();
	    houstonFlowFunctions=new HoustonFlowFunctions(getApplicationContext());
	    fromDate=getCurrentDate();
		toDate=getCurrentDate();
		parsingData=new ParsingData(getApplicationContext());
		
		Log.e("servi ce started","service started");
		timer = new Timer("HoustonFlow");
		try 
		{
		timer.schedule(updateTask, 20 * 60 * 1000L, 20 * 60 * 1000L);
		}
		catch (Exception e) 
		{
		}
	}
	private TimerTask updateTask = new TimerTask()
	{
		@Override
		public void run() 
		{
			synchronized (this)
			{
					if(haveInternet(getApplicationContext()))
					{
						POSTAll_Data_ServiceNewID post_data_ServiceNewID = new POSTAll_Data_ServiceNewID();
						post_data_ServiceNewID.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
						
						PostLogData postLogData = new PostLogData();
						postLogData.execute("");
					}
			}
		}
	};
	public static boolean haveInternet(Context ctx)
	{
		NetworkInfo info = (NetworkInfo) ((ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

		if (info == null || !info.isConnected())
		{
			return false;
		}
		if (info.isRoaming())
		{
			return false;
		}
		return true;
	}
	public String getCurrentDate()
	{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
		Calendar calendar1 = Calendar.getInstance();
		calendar1.setTimeInMillis(System.currentTimeMillis());
		String date=""+formatter.format(calendar1.getTime());
		//fromDate=date;
		//	toDate=date;
		return date;
	}
	class PostLogData  extends AsyncTask<String,String,String>
	{
		@Override
		protected String doInBackground(String... params) {
			postServiceDetailData(); 
			return "";
		}
	}
	private final String SOAP_ACTION_POST_SERVICE_DETAIL="http://ipad-abc.com/Save_One_WO";
	private final String METHOD_NAME_POST_SERVICE_DETAIL="Save_One_WO";

	public void postServiceDetailData()
	{
		try
		{
			JSONArray jsServiceDetail=houstonFlowFunctions.getServiceDetailDTOList();
			Log.e("jsServiceDetail", jsServiceDetail+"");
			ArrayList<LogDTO> jsServiceDetailToSynch = new Gson().fromJson(jsServiceDetail.toString(), new TypeToken<List<LogDTO>>(){}.getType());
			for(int i=0;i<jsServiceDetailToSynch.size();i++)
			{
				LogDTO detailsDto=jsServiceDetailToSynch.get(i);
				SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_POST_SERVICE_DETAIL);
				PropertyInfo SID = new PropertyInfo();
				SID.setName("SID");
				SID.setValue(detailsDto.getSID()+"");
				request.addProperty(SID);

				PropertyInfo GeneralSR = new PropertyInfo();
				GeneralSR.setName("GeneralSR");
				GeneralSR.setValue(detailsDto.getGeneralSR()+"");
				request.addProperty(GeneralSR);

				PropertyInfo Residetial_Insp = new PropertyInfo();
				Residetial_Insp.setName("Residetial_Insp");
				Residetial_Insp.setValue(detailsDto.getResidetial_Insp()+"");
				request.addProperty(Residetial_Insp);

				PropertyInfo Chemical = new PropertyInfo();
				Chemical.setName("Chemical");
				Chemical.setValue(detailsDto.getChemical()+"");
				request.addProperty(Chemical);

				PropertyInfo AgreeMentMailID = new PropertyInfo();
				AgreeMentMailID.setName("AgreeMentMailID");
				AgreeMentMailID.setValue(detailsDto.getAgreeMentMailID()+"");
				request.addProperty(AgreeMentMailID);
				
				
				PropertyInfo CrewMember = new PropertyInfo();
				CrewMember.setName("CrewMember");
				CrewMember.setValue(detailsDto.getCrew_Member()+"");
				request.addProperty(CrewMember);

				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				envelope.setOutputSoapObject(request);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				try 
				{
					androidHttpTransport.call(SOAP_ACTION_POST_SERVICE_DETAIL, envelope);
					SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
					String	res = response.toString();
					LoggerUtil.e("postServiceDetailData",res+"");
					if(res.contains("Ok"))
					{
						houstonFlowFunctions.deleteSelectedTableData(DatabaseHelper.TABLE_LOG,ParameterUtil.SID,detailsDto.getSID());
					}
					Log.e("Response", res.toLowerCase());
				} 
				catch (IOException  e) 
				{
					LoggerUtil.e("IOException", e.toString());
				}
			}
		}
		catch(XmlPullParserException e)
		{
			Log.e("Exception",e.toString());
		}
	}
	private final String SOAP_ACTION_POST_SERVICE="http://ipad-abc.com/Save_All_WO";
	private final String METHOD_NAME_POST_SERVICE="Save_All_WO";
	
	class POSTAll_Data_ServiceNewID extends AsyncTask<String,String,String>
	{
		@Override
		protected void onPreExecute() 
		{
			Log.e("onPreExecute","onPreExecute");
				super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params)
		{
			return Post_Service_Data();
		}

		@Override
		protected void onPostExecute(String result)
		{
			try
			{
				ParsingData parsingData=new ParsingData(getApplicationContext());
				parsingData.parseDataPostResponse(result);
			}
			catch(Exception e)
			{

			}
			try
			{
				JSONArray responseArray=new JSONArray(result);
				for(int i=0;i<responseArray.length();i++)
				{
					JSONObject jsResponse=responseArray.getJSONObject(i);
					String Response=jsResponse.getString("Response").trim();
					String From=jsResponse.getString("From").trim();
					String ServiceID_new=jsResponse.getString("ServiceID_new").trim();

					if(From.equals("GeneralSR")&&Response.equals("deleted"))
					{
						int deleteGenSr =	houstonFlowFunctions.deleteSelectedTableData(DatabaseHelper.TABLE_GENERAL_INFO,ParameterUtil.ServiceID_new, ServiceID_new);
						int deleteResiSr=	houstonFlowFunctions.deleteSelectedTableData(DatabaseHelper.TABLE_HR_PEST_INFO,ParameterUtil.ServiceID_new, ServiceID_new);
						int deleteCheSr	= houstonFlowFunctions.deleteSelectedTableData(DatabaseHelper.TABLE_SELECTED_CHEMICALS,ParameterUtil.ServiceID_new, ServiceID_new);
						int deleteCrew	= houstonFlowFunctions.deleteSelectedTableData(DatabaseHelper.TABLE_SELECTED_CREW_MEMBER,ParameterUtil.ServiceID_new, ServiceID_new);
						int deleteLogData	= houstonFlowFunctions.deleteSelectedTableData(DatabaseHelper.TABLE_LOG,ParameterUtil.ServiceID_new, ServiceID_new);
						
					
					}
					if(From.equals("GeneralSR")&&Response.equals("OK"))
					{
						int delete=	houstonFlowFunctions.deleteSelectedTableData(DatabaseHelper.TABLE_GENERAL_INFO,ParameterUtil.ServiceID_new, ServiceID_new);
						LoggerUtil.e("GeneralSR Delete",String.valueOf(delete));
					}
					if(From.equals("Residetial_Insp")&&Response.equals("OK"))
					{
						int delete=	houstonFlowFunctions.deleteSelectedTableData(DatabaseHelper.TABLE_HR_PEST_INFO,ParameterUtil.ServiceID_new, ServiceID_new);
						LoggerUtil.e("Residetial_Insp Delete",String.valueOf(delete));
					}

					if(From.equals("Chemical")&&Response.equals("OK"))
					{
						int delete	= houstonFlowFunctions.deleteSelectedTableData(DatabaseHelper.TABLE_SELECTED_CHEMICALS,ParameterUtil.ServiceID_new, ServiceID_new);
						//LoggerUtil.e("Chemical Delete",String.valueOf(delete));
					}
					if(From.equals("CrewMember")&&Response.equals("OK"))
					{
						int delete1	= houstonFlowFunctions.deleteSelectedTableData(DatabaseHelper.TABLE_SELECTED_CREW_MEMBER,ParameterUtil.ServiceID_new, ServiceID_new);
						LoggerUtil.e("CREW Delete",String.valueOf(delete1));
					}
					if(From.equals("LogData")&&Response.equals("OK"))
					{
						int delete1	= houstonFlowFunctions.deleteSelectedTableData(DatabaseHelper.TABLE_LOG,ParameterUtil.ServiceID_new, ServiceID_new);
						LoggerUtil.e("Log Data Delete",String.valueOf(delete1));
					}
				
					houstonFlowFunctions.deleteAllTableData(DatabaseHelper.TABLE_AGREEMENTMAILID);
    		}
			}
			catch(Exception e)
			{
             
			}
		getAllRecordFromServer();

			super.onPostExecute(result);
		}
	
		private void getAllRecordFromServer() {
			// TODO Auto-generated method stub
			DownloadAll_General_Information downloadAll_General_Information=new DownloadAll_General_Information();
			downloadAll_General_Information.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

			DownloadAll_Employee_Detail downloadAll_Employee_Detail=new DownloadAll_Employee_Detail();
			downloadAll_Employee_Detail.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

			DownloadAll_Residential_Pest downloadAll_Residential_Pest=new DownloadAll_Residential_Pest();
			downloadAll_Residential_Pest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

			DownloadAll_Chemical downloadAll_DownloadAll_Chemical=new DownloadAll_Chemical();
			downloadAll_DownloadAll_Chemical.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

			DownloadAll_Chemical_WhenNoRecord downloadAll_Download_Chemical_WhenNoRecord=new DownloadAll_Chemical_WhenNoRecord();
			downloadAll_Download_Chemical_WhenNoRecord.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

			
			DownloadAll_Crew_Members downloadAll_Crew_Members=new DownloadAll_Crew_Members();
			downloadAll_Crew_Members.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

		}
		private final String SOAP_ACTION_Get_GeneralInformation="http://ipad-abc.com/Get_Todays_Service_Gen_Info";
		private final String METHOD_NAME_Get_GeneralAddressInformation="Get_Todays_Service_Gen_Info";
		class DownloadAll_General_Information extends AsyncTask<String,String,String>
		{
			@Override
			protected void onPreExecute() 
			{
				super.onPreExecute();
			}
			@Override
			protected String doInBackground(String... params)
			{
				return downloadAll_General_Information();
			}
			@Override
			protected void onPostExecute(String result)
			{
				serviceList = new Gson().fromJson(houstonFlowFunctions.getAllInCompleteService().toString(), new TypeToken<List<GeneralInfoDTO>>(){}.getType());
				Intent broadcastIntent = new Intent();
				broadcastIntent.setAction("REFRESH_DATA_ACTION");
				sendBroadcast(broadcastIntent);
				super.onPostExecute(result);
			}
		}
		public String downloadAll_General_Information()
		{
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_Get_GeneralAddressInformation);

			PropertyInfo EmpId = new PropertyInfo();
			EmpId.setName(ParameterUtil.EmpId);
			EmpId.setValue(getEmployeeId());
			request.addProperty(EmpId);

			PropertyInfo SDate = new PropertyInfo();
			SDate.setName(ParameterUtil.SDate);
			SDate.setValue(getCurrentDate());
			request.addProperty(SDate);

			PropertyInfo Edate = new PropertyInfo();
			Edate.setName(ParameterUtil.Edate);
			Edate.setValue(getCurrentDate());
			request.addProperty(Edate);

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

			try 
			{
				androidHttpTransport.call(SOAP_ACTION_Get_GeneralInformation, envelope);
				SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
				String	res = response.toString();

				parsingData.parseServiceListGenralInformation(res);
				/*LoggerUtil.e("resp from gene info..",res+"");
				try
				{
					JSONArray jsServiceDataByServiceNewID=new JSONArray(res);
					if(jsServiceDataByServiceNewID.length()>0)
					{
						houstonFlowFunctions.deleteAllTableData(DatabaseHelper.TABLE_GENERAL_INFO);
					}
					ArrayList<GeneralInfoDTO>  serviceDataList = new Gson().fromJson(jsServiceDataByServiceNewID.toString(), new TypeToken<List<GeneralInfoDTO>>(){}.getType());
					jsServiceDataByServiceNewID=new JSONArray(new Gson().toJson(serviceDataList));
					houstonFlowFunctions.insertMultipleRecords(jsServiceDataByServiceNewID, DatabaseHelper.TABLE_GENERAL_INFO);

				}catch(Exception e)
				{
					LoggerUtil.e("Exception", e.toString());
				}*/

				return res;
			} 
			catch (IOException  e) 
			{
				LoggerUtil.e("IOException", e.toString());
			}catch (XmlPullParserException e)
			{
				LoggerUtil.e("Get_Service_Data_By_ServiceNewID", e.toString());
			}
			return "[{\"result\":\"No Record Found\"}]";
		}
		
		public void downloadImageByName(String imageNames,String url)
		{

			String[] imagesArray=imageNames.split(",");

			for(int b=0;b<imagesArray.length;b++)
			{
				String imageName=imagesArray[b];
				if(!CommonFunction.checkString(imageName, "").equals(""))
				{
					boolean	isFileExists=isFileExistsByName(imageName);

					if(isFileExists)
					{

					}else
					{
						downloadFile(url, imageName);
					}
				}
			}



		}

		public void donwloadAudioByName(String fileName)
		{
			if(!CommonFunction.checkString(fileName, "").equals(""))
			{
				boolean	isFileExists=isFileExistsByName(fileName);

				if(isFileExists)
				{

				}else
				{
					downloadFile(CommonFunction.DOWNLOAD_IMAGE_URL, fileName);
				}
			}
		}



		public boolean isFileExistsByName(String ImageName)
		{
			if(ImageName==null || ImageName.equals(""))
			{
				return false;
			}


			try {
				Log.e("Image Name in refresh",ImageName);
				File profileImage=CommonFunction.getFileLocation(getApplicationContext(),ImageName);
				if(profileImage.exists())
				{
					return true;
					//bmp = scaleBitmap(bmp, 100, 100);
				}else
				{
					return false;
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				//bmp=getResources().getDrawable(R.drawable.noimage);
				e.printStackTrace();
			}

			return false;
		}
		public void downloadFile(String fileUrl,String fileName)
		{
			URL myFileUrl = null;
			//	Bitmap bmImg = null;
			try 
			{
				myFileUrl = new URL(fileUrl+""+fileName);
			} catch (MalformedURLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				HttpURLConnection conn = (HttpURLConnection) myFileUrl.openConnection();
				conn.setDoInput(true);
				conn.connect();
				File file = CommonFunction.getFileLocation(getApplicationContext(), fileName);
				FileOutputStream fileOutput = new FileOutputStream(file);
				InputStream is = conn.getInputStream();
				//bmImg = BitmapFactory.decodeStream(is);
				byte[] buffer = new byte[1024];
				int bufferLength = 0;
				while ( (bufferLength = is.read(buffer)) > 0 )
				{
					fileOutput.write(buffer, 0, bufferLength);
				}
				//close the output stream when done
				fileOutput.close();
				try
				{/*
					bmImg=getBimapByName(fileName);
					if(bmImg!=null)
					{
						//imageview.setImageBitmap(bmImg);

					}else
					{
						//imageview.setImageResource(R.drawable.noimage);
						//pb.setVisibility(View.GONE);
					}

				 */
				} catch (Exception e)
				{
					Log.e("Exception in download file", e.toString());

				}
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				Log.e("Exception in download file", e.toString());
			}

		}

		private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException
		{
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(
					getApplicationContext().getContentResolver().openInputStream(selectedImage), null, o);

			final int REQUIRED_SIZE = 800;

			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			while (true) {
				if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
					break;
				}
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}

			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			return BitmapFactory.decodeStream(getApplicationContext().getContentResolver().openInputStream(selectedImage), null, o2);
		}


		public String Post_Service_Data()
		{
			//return METHOD_NAME_Get_AddressInformation;
			Gson gson=new Gson();

			//Upload After and Before Image
			ArrayList<GeneralInfoDTO> serviceListToSynch = new Gson().fromJson(houstonFlowFunctions.getAllServiceChangeLocaly().toString(), new TypeToken<List<GeneralInfoDTO>>(){}.getType());
			LoggerUtil.e("serviceListToSynch", "serviceListToSynch..."+serviceListToSynch.size());
			for(int i=0;i<serviceListToSynch.size();i++)
			{
				serviceListToSynch.get(i).setServiceDate(serviceListToSynch.get(i).getServiceDate()+" "+serviceListToSynch.get(i).getServiceTime());
				serviceListToSynch.get(i).setMobile_Info(String.valueOf(android.os.Build.MODEL+" "+android.os.Build.VERSION.RELEASE));
				serviceListToSynch.get(i).setApp_Info(String.valueOf("VERSION "+CommonFunction.getAppVersion(getApplicationContext())));
			
				String afterImage=serviceListToSynch.get(i).getAfterImage();
				String beforeImage=serviceListToSynch.get(i).getBeforeImage();
				String audioFileName=serviceListToSynch.get(i).getAudioFile();

				if(!CommonFunction.checkString(afterImage, "").equals(""))
				{
					String afterImages[]=afterImage.split(",");
					for(int a=0;a<afterImages.length;a++)
					{
						String filePath=CommonFunction.getFileLocation(getApplicationContext(),afterImages[a]).toString();
						String res=	uploadFile(filePath,CommonFunction.UPLOAD_IMAGE_URL);
						LoggerUtil.e("afterImage Upload Res", res);
					}
				}

				if(!CommonFunction.checkString(beforeImage, "").equals(""))
				{
					String beforeImages[]=beforeImage.split(",");
					for(int b=0;b<beforeImages.length;b++)
					{
						String filePath=CommonFunction.getFileLocation(getApplicationContext(),beforeImages[b]).toString();
						String res=	uploadFile(filePath,CommonFunction.UPLOAD_IMAGE_URL);
						LoggerUtil.e("beforeImage Upload Res", res);

					}	
				}

				if(!CommonFunction.checkString(audioFileName, "").equals(""))
				{
					String filePath=CommonFunction.getAudioFilePath(audioFileName).toString();
					String res=	uploadFile(filePath,CommonFunction.UPLOAD_IMAGE_URL);
					LoggerUtil.e("audioFileName Upload Res", res);
				}
			}

			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_POST_SERVICE);
			PropertyInfo genralInformationDATA = new PropertyInfo();
			genralInformationDATA.setName("GeneralSR");
			genralInformationDATA.setValue(gson.toJson(serviceListToSynch));
			request.addProperty(genralInformationDATA);
			LoggerUtil.e("GeneralSR",gson.toJson(serviceListToSynch));

			ArrayList<HResidential_pest_DTO> residentailDataToSynch = new Gson().fromJson(houstonFlowFunctions.getCompleteResidential_Commercial_pestService(DatabaseHelper.TABLE_HR_PEST_INFO).toString(), new TypeToken<List<HResidential_pest_DTO>>(){}.getType());
			PropertyInfo residential_pesttData = new PropertyInfo();
			residential_pesttData.setName("Residetial_Insp");

			//Upload Customer And Technician Signature Singnature
			for(int i=0;i<residentailDataToSynch.size();i++)
			{
				String technicianSignImage=residentailDataToSynch.get(i).getServiceSignature();
				String customerSignImage=residentailDataToSynch.get(i).getSignature();
				//Technician Sign Image Upload

				if(!CommonFunction.checkString(technicianSignImage, "").equals(""))
				{

					String filePath=CommonFunction.getFileLocation(getApplicationContext(),technicianSignImage).toString();
					String res=	uploadFile(filePath,CommonFunction.UPLOAD_RESIDENTIAL_SIGNATURE);
					LoggerUtil.e("technicianSignImage Upload Res", res);
				}
				//Customer Sign Image Upload
				if(!CommonFunction.checkString(customerSignImage, "").equals(""))
				{
					String filePath=CommonFunction.getFileLocation(getApplicationContext(),customerSignImage).toString();
					String res=	uploadFile(filePath,CommonFunction.UPLOAD_RESIDENTIAL_SIGNATURE);
					LoggerUtil.e("customerSignImage Upload Res", res);
				}
			}

			residential_pesttData.setValue(gson.toJson(residentailDataToSynch));
			request.addProperty(residential_pesttData);

			LoggerUtil.e("Residetial_Insp",gson.toJson(residentailDataToSynch));
			//ArrayList<ChemicalsDTO> chemicalDataToSynch = new Gson().fromJson(serviceFlowFunctions.getAllCompleteServiceSelectedChemicalList(DatabaseHelper.TABLE_CHEMICALS_SELECTED).toString(), new TypeToken<List<ChemicalsDTO>>(){}.getType());

			ArrayList<HCommercial_pest_DTO> commercialDataToSynch = new Gson().fromJson(
					houstonFlowFunctions
							.getCompleteResidential_Commercial_pestService(
									DatabaseHelper.TABLE_HC_PEST_INFO)
							.toString(), new TypeToken<List<HCommercial_pest_DTO>>() {
					}.getType());

			PropertyInfo commercial_pesttData = new PropertyInfo();
			commercial_pesttData.setName("Commercial_Insp");
			// Special Character...
			// Upload Customer And Technician Signature Singnature
			for (int i = 0; i < commercialDataToSynch.size(); i++) {
				String technicianSignImage = commercialDataToSynch.get(i)
						.getServiceSignature();
				String customerSignImage = commercialDataToSynch.get(i)
						.getSignature();
				// Technician Sign Image Upload

				if (!CommonFunction.checkString(technicianSignImage, "").equals("")) {

					String filePath = CommonFunction.getFileLocation(
							SynchServiceToServerServices.this, technicianSignImage)
							.toString();
					String res = uploadFile(filePath,
							CommonFunction.UPLOAD_COMMERCIAL_SIGNATURE);
					LoggerUtil.e("technicianSignImage Upload Res", res);
				}
				// Customer Sign Image Upload
				if (!CommonFunction.checkString(customerSignImage, "").equals("")) {
					String filePath = CommonFunction.getFileLocation(
							SynchServiceToServerServices.this, customerSignImage)
							.toString();
					String res = uploadFile(filePath,
							CommonFunction.UPLOAD_COMMERCIAL_SIGNATURE);
					LoggerUtil.e("customerSignImage Upload Res", res);
				}
			}

			commercial_pesttData.setValue(gson.toJson(commercialDataToSynch));
			request.addProperty(commercial_pesttData);

			LoggerUtil.e("Commercial_Insp", gson.toJson(commercialDataToSynch));
			
			
			
			JSONArray chemicalJSONArray = houstonFlowFunctions.getAllServiceSelectedChemicalList(DatabaseHelper.TABLE_SELECTED_CHEMICALS);
			PropertyInfo chemicalstData = new PropertyInfo();
			chemicalstData.setName("Chemical");
			//	JSONArray chemicalJSONArray=	CommonFunction.convertChemicalListToPrevetionJSONArray(chemicalDataToSynch);
			if(chemicalJSONArray.length()<0)
			{
				chemicalstData.setValue("");
			}else
			{
				chemicalstData.setValue(chemicalJSONArray.toString());
			}

			LoggerUtil.e("Chemical",chemicalJSONArray.toString());
			request.addProperty(chemicalstData);
			//return "[{\"result\":\"No Record Found\"}]";

			JSONArray crewListArray=houstonFlowFunctions.getSelectedCrewMember();
			ArrayList<Selected_crewDTO> crewList=new Gson().fromJson(crewListArray.toString(), new TypeToken<List<Selected_crewDTO>>(){}.getType());;
			PropertyInfo crewMemberData = new PropertyInfo();
			crewMemberData.setName("CrewMember");
			LoggerUtil.e("CrewMembers", gson.toJson(crewList));
			crewMemberData.setValue(gson.toJson(crewList));
			request.addProperty(crewMemberData);
		
			
			JSONArray emailIDListArray=houstonFlowFunctions.getAllSelectedEmaiLID();
			ArrayList<AgreementMailIdDTO> emailList=new Gson().fromJson(emailIDListArray.toString(), new TypeToken<List<AgreementMailIdDTO>>(){}.getType());;
			PropertyInfo emailAddressData = new PropertyInfo();
			emailAddressData.setName("AgreeMentMailID");
			LoggerUtil.e("AgreeMentMailID", gson.toJson(emailList));
			emailAddressData.setValue(gson.toJson(emailList));
			request.addProperty(emailAddressData);

			/*PropertyInfo emailAddressData = new PropertyInfo();
			emailAddressData.setName("AgreeMentMailID");
			//LoggerUtil.e("AgreeMentMailID", gson.toJson(emailList));
			emailAddressData.setValue("");*/

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

			try 
			{
				androidHttpTransport.call(SOAP_ACTION_POST_SERVICE, envelope);
				SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
				String	res = response.toString();
				LoggerUtil.e("Post_Service_Data",res+"");
				try
				{
					parsingData.parseDataPostResponse(res);
				}
				catch(Exception e)
				{
					LoggerUtil.e("Exception", e.toString());

				}
				return res;
			} 
			catch (IOException  e) 
			{
				LoggerUtil.e("IOException", e.toString());
			}
			catch (XmlPullParserException e)
			{
				LoggerUtil.e("Get_Service_Data_By_ServiceNewID", e.toString());
			}
			
			if(serviceListToSynch.size()==0&&residentailDataToSynch.size()==0)
			{
				stopSelf();
			}
			
			return "[{\"result\":\"No Record Found\"}]";
		}
		
		public String uploadFile(String filePath,String serverUrl)
		{
			String fileUploadResponse = CommonFunction.uploadFile(filePath,serverUrl);
			return fileUploadResponse;
		}
		
		public void onDestroy() {
			// TODO Auto-generated method stub
			//super.onDestroy();
			Log.e("Service", "Service destroying");
			try{
				timer.cancel();
			}catch (Exception e) {
				// TODO: handle exception
			}timer = null;
		}
		
		
		private final String SOAP_ACTION_Get_Employee_Detail="http://ipad-abc.com/All_Employee_List";
		private final String METHOD_NAME_Get_Employee_Detail="All_Employee_List";
		//ProgressDialog progressDialogGet_Employee_Detail;

		class DownloadAll_Employee_Detail extends AsyncTask<String,String,String>
		{
			@Override
			protected String doInBackground(String... params)
			{
				return downloadAll_Employee_Detail();
			}
			@Override
			protected void onPreExecute() 
			{
				//String downloading=getResources().getString(R.string.loading_employee_details);
				//String pleasewait=getResources().getString(R.string.please_wait);
				//progressDialogGet_Employee_Detail = ProgressDialog.show(getApplicationContext(),downloading, pleasewait, true); // TODO
				super.onPreExecute();
			}
			@Override
			protected void onPostExecute(String result)
			{
				//progressDialogGet_Employee_Detail.dismiss();
				//./setRecordtoServiceList();
				super.onPostExecute(result);
			}
		}
		public String downloadAll_Employee_Detail()
		{
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_Get_Employee_Detail);

			PropertyInfo UserName = new PropertyInfo();
			UserName.setName(ParameterUtil.Username);
			UserName.setValue(getEmployeeUserName());
			request.addProperty(UserName);

			PropertyInfo FirstName = new PropertyInfo();
			FirstName.setName(ParameterUtil.FirstName);
			FirstName.setValue("");
			request.addProperty(FirstName);

			PropertyInfo LastName = new PropertyInfo();
			LastName.setName(ParameterUtil.LastName);
			LastName.setValue("");
			request.addProperty(LastName);

			PropertyInfo Email = new PropertyInfo();
			Email.setName(ParameterUtil.Email);
			Email.setValue("");
			request.addProperty(Email);

			PropertyInfo Role = new PropertyInfo();
			Role.setName(ParameterUtil.Role);
			Role.setValue("");
			request.addProperty(Role);

			PropertyInfo Status = new PropertyInfo();
			Status.setName(ParameterUtil.Status);
			Status.setValue("");
			request.addProperty(Status);

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

			try 
			{
				androidHttpTransport.call(SOAP_ACTION_Get_Employee_Detail, envelope);
				SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
				String	res = response.toString();
				LoggerUtil.e("resp from all employees","employees..."+res);
				try
				{
					JSONArray jsServiceDataByServiceNewID=new JSONArray(res);
					if(jsServiceDataByServiceNewID.length()>0)
					{
						houstonFlowFunctions.deleteAllTableData(DatabaseHelper.TABLE_EMPLOYEE_INFO);
					}
					ArrayList<EmployeeDTO>  serviceDataList = new Gson().fromJson(jsServiceDataByServiceNewID.toString(), new TypeToken<List<EmployeeDTO>>(){}.getType());
					jsServiceDataByServiceNewID=new JSONArray(new Gson().toJson(serviceDataList));
					houstonFlowFunctions.insertMultipleRecords(jsServiceDataByServiceNewID, DatabaseHelper.TABLE_EMPLOYEE_INFO);

				}catch(Exception e)
				{
					LoggerUtil.e("Exception", e.toString());
				}
				return res;
			} 
			catch (IOException  e) 
			{
				LoggerUtil.e("IOException", e.toString());
			}catch (XmlPullParserException e)
			{
				LoggerUtil.e("Get_Service_Data_By_ServiceNewID", e.toString());
			}
			return "[{\"result\":\"No Record Found\"}]";
		}	
		private final String SOAP_ACTION_Get_ResidentialInformation="http://ipad-abc.com/Get_ResidentialServiceDetail";
		private final String METHOD_NAME_Get_ResidentialInformation="Get_ResidentialServiceDetail";
		//ProgressDialog progressDialogGet_ResidentialInfoDetail;
		class DownloadAll_Residential_Pest extends AsyncTask<String,String,String>
		{
			@Override
			protected String doInBackground(String... params)
			{
				return downloadAll_Residential_Information();
			}
			@Override
			protected void onPreExecute() 
			{
				//String downloading=getResources().getString(R.string.Loading_Residential_Detail);
				//String pleasewait=getResources().getString(R.string.please_wait);
				//progressDialogGet_ResidentialInfoDetail = ProgressDialog.show(getApplicationContext(),downloading, pleasewait, true); // TODO
				super.onPreExecute();
			}
			@Override
			protected void onPostExecute(String result)
			{
				//progressDialogGet_ResidentialInfoDetail.dismiss();
				//setRecordtoServiceList();
				super.onPostExecute(result);
			}
		}
		public String downloadAll_Residential_Information()
		{
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_Get_ResidentialInformation);

			PropertyInfo EmpId = new PropertyInfo();
			EmpId.setName(ParameterUtil.EmpId);
			EmpId.setValue(getEmployeeId());
			request.addProperty(EmpId);

			PropertyInfo SDate = new PropertyInfo();
			SDate.setName(ParameterUtil.SDate);
			SDate.setValue(getCurrentDate());
			request.addProperty(SDate);

			PropertyInfo Edate = new PropertyInfo();
			Edate.setName(ParameterUtil.Edate);
			Edate.setValue(getCurrentDate());
			request.addProperty(Edate);

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

			try 
			{
				androidHttpTransport.call(SOAP_ACTION_Get_ResidentialInformation, envelope);
				SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
				String	res = response.toString();
				LoggerUtil.e("resp from resi info..",res+"");
				
				try
				{
					parsingData.parseResidentialPestData(res);
				}
				catch(Exception e)
				{
					LoggerUtil.e("Exception", e.toString());

				}
				

				return res;
			} 
			catch (IOException  e) 
			{
				LoggerUtil.e("IOException..residential..", e.toString());
			}catch (XmlPullParserException e)
			{
				LoggerUtil.e("Get_Service_Data_By_ServiceNewID...residential..S", e.toString());
			}
			return "[{\"result\":\"No Record Found\"}]";
		}
		
		private final String SOAP_ACTION_Get_Chemical_WhenNoRecord="http://ipad-abc.com/Get_All_Chemical_Product";
		private final String METHOD_NAME_Get_Chemical_WhenNoRecord="Get_All_Chemical_Product";
		//ProgressDialog progressDialogGet_Chemical_WhenNoRecord;
		class DownloadAll_Chemical_WhenNoRecord extends AsyncTask<String,String,String>
		{
			@Override
			protected void onPreExecute() 
			{
				//String //downloading="Loading Data Chemicals..";
				//String //pleasewait=getResources().getString(R.string.please_wait);
				//progressDialogGet_Chemical_WhenNoRecord = ProgressDialog.show(getApplicationContext(),downloading, pleasewait, true); // TODO
				super.onPreExecute();
			}
			@Override
			protected String doInBackground(String... params)
			{
				return downloadAll_Chemicals_WhenNoRecord();
			}

			@Override
			protected void onPostExecute(String result)
			{
				//progressDialogGet_Chemical_WhenNoRecord.dismiss();

				super.onPostExecute(result);
			}
		}
		public String downloadAll_Chemicals_WhenNoRecord()
		{
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_Get_Chemical_WhenNoRecord);

			
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

			try 
			{
				androidHttpTransport.call(SOAP_ACTION_Get_Chemical_WhenNoRecord, envelope);
				SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
				String	res = response.toString();
				LoggerUtil.e("resp from data chemical..",res+"");
				try
				{
					JSONArray jsServiceDataByServiceNewID=new JSONArray(res);
					if(jsServiceDataByServiceNewID.length()>0)
					{
						houstonFlowFunctions.deleteAllTableData(DatabaseHelper.TABLE_CHEMICALS_WHEN_NO_RECORD);
					}
					ArrayList<Chemical_WhenNoRecordDTO>  serviceDataList = new Gson().fromJson(jsServiceDataByServiceNewID.toString(), new TypeToken<List<Chemical_WhenNoRecordDTO>>(){}.getType());
					jsServiceDataByServiceNewID=new JSONArray(new Gson().toJson(serviceDataList));
					houstonFlowFunctions.insertMultipleRecords(jsServiceDataByServiceNewID, DatabaseHelper.TABLE_CHEMICALS_WHEN_NO_RECORD);

				}
				catch(Exception e)
				{
					LoggerUtil.e("Exception", e.toString());
				}

				return res;
			} 
			catch (IOException  e) 
			{
				LoggerUtil.e("IOException", e.toString());
			}
			catch (XmlPullParserException e)
			{
				LoggerUtil.e("Get_Service_Data_By_ServiceNewID..Chemicals..", e.toString());
			}
			return "[{\"result\":\"No Record Found\"}]";
		}
		
		private final String SOAP_ACTION_Get_Chemical="http://ipad-abc.com/Get_Mobile_usp_Get_All_Chemicals";
		private final String METHOD_NAME_Get_Chemical="Get_Mobile_usp_Get_All_Chemicals";
		//ProgressDialog progressDialogGet_Chemical;
		class DownloadAll_Chemical extends AsyncTask<String,String,String>
		{
			@Override
			protected void onPreExecute() 
			{
				//String //downloading="Loading Chemicals..";
				//String //pleasewait=getResources().getString(R.string.please_wait);
				//progressDialogGet_Chemical = ProgressDialog.show(getApplicationContext(),downloading, pleasewait, true); // TODO
				super.onPreExecute();
			}
			@Override
			protected String doInBackground(String... params)
			{
				return downloadAll_Chemicals();
			}

			@Override
			protected void onPostExecute(String result)
			{
				//progressDialogGet_Chemical.dismiss();

				super.onPostExecute(result);
			}
		}
		public String downloadAll_Chemicals()
		{
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_Get_Chemical);

			PropertyInfo EmpId = new PropertyInfo();
			EmpId.setName(ParameterUtil.EmpId);
			EmpId.setValue("79");
			request.addProperty(EmpId);

			PropertyInfo SDate = new PropertyInfo();
			SDate.setName(ParameterUtil.SDate);
			SDate.setValue(getCurrentDate());
			request.addProperty(SDate);

			PropertyInfo Edate = new PropertyInfo();
			Edate.setName(ParameterUtil.Edate);
			Edate.setValue(getCurrentDate());
			request.addProperty(Edate);

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

			try 
			{
				androidHttpTransport.call(SOAP_ACTION_Get_Chemical, envelope);
				SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
				String	res = response.toString();
				LoggerUtil.e("resp from chemical..",res+"");
				parsingData.parsingchemicals(res);

				return res;
			} 
			catch (IOException  e) 
			{
				LoggerUtil.e("IOException", e.toString());
			}
			catch (XmlPullParserException e)
			{
				LoggerUtil.e("Get_Service_Data_By_ServiceNewID..Chemicals..", e.toString());
			}
			return "[{\"result\":\"No Record Found\"}]";
		}
		private final String SOAP_ACTION_Get_Crew_Member="http://ipad-abc.com/Mobile_Get_CrewMember";
		private final String METHOD_NAME_Get_Crew_Member="Mobile_Get_CrewMember";
		//ProgressDialog progressDialogGet_Crew_Member;

		class DownloadAll_Crew_Members extends AsyncTask<String,String,String>
		{
			@Override
			protected String doInBackground(String... params)
			{
				return downloadAll_Crew_Member();
			}
			@Override
			protected void onPreExecute() 
			{
				//String //downloading=getResources().getString(R.string.loading_crew_membwers);
				//String //pleasewait=getResources().getString(R.string.please_wait);
				//progressDialogGet_Crew_Member = ProgressDialog.show(getApplicationContext(),downloading, pleasewait, true); // TODO
				super.onPreExecute();
			}
			@Override
			protected void onPostExecute(String result)
			{
				//progressDialogGet_Crew_Member.dismiss();
				//./setRecordtoServiceList();
				super.onPostExecute(result);
			}
		}
		public String downloadAll_Crew_Member()
		{
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_Get_Crew_Member);

			PropertyInfo EmpId = new PropertyInfo();
			EmpId.setName(ParameterUtil.EmpId);
			EmpId.setValue("79");
			request.addProperty(EmpId);

			PropertyInfo SDate = new PropertyInfo();
			SDate.setName(ParameterUtil.SDate);
			SDate.setValue(getCurrentDate());
			request.addProperty(SDate);

			PropertyInfo Edate = new PropertyInfo();
			Edate.setName(ParameterUtil.Edate);
			Edate.setValue(getCurrentDate());
			request.addProperty(Edate);

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

			try 
			{
				androidHttpTransport.call(SOAP_ACTION_Get_Crew_Member, envelope);
				SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
				String	res = response.toString();
				LoggerUtil.e("resp from all crew","crew..."+res);
				try
				{
					JSONArray jsServiceDataByServiceNewID=new JSONArray(res);
					if(jsServiceDataByServiceNewID.length()>0)
					{
						houstonFlowFunctions.deleteAllTableData(DatabaseHelper.TABLE_CREW_MEMBER);
					}
					ArrayList<CrewMemberDTO>  serviceDataList = new Gson().fromJson(jsServiceDataByServiceNewID.toString(), new TypeToken<List<CrewMemberDTO>>(){}.getType());
					jsServiceDataByServiceNewID=new JSONArray(new Gson().toJson(serviceDataList));
					houstonFlowFunctions.insertMultipleRecords(jsServiceDataByServiceNewID, DatabaseHelper.TABLE_CREW_MEMBER);

				}catch(Exception e)
				{
					LoggerUtil.e("Exception in crew", e.toString());
				}
				return res;
			} 
			catch (IOException  e) 
			{
				LoggerUtil.e("IOException", e.toString());
			}catch (XmlPullParserException e)
			{
				LoggerUtil.e("Get_Service_Data_By_ServiceNewID", e.toString());
			}
			return "[{\"result\":\"No Record Found\"}]";
		}
    }
	public String getEmployeeUserName()
	{
		SharedPreferences userfile=getSharedPreferences(SharedPreference.SERVICE_FLOW_PREF_FILE,MODE_MULTI_PROCESS);
		String userId=userfile.getString(SharedPreference.EMPLOYEE_USER_NAME,"0");
		return userId;
	}
	public String getEmployeeId()
	{
		SharedPreferences userfile=getSharedPreferences(SharedPreference.SERVICE_FLOW_PREF_FILE,MODE_MULTI_PROCESS);
		String userId=userfile.getString(SharedPreference.USER_ID,"0");
		return userId;
	}
	@Override
	public IBinder onBind(Intent intent)
	{
		// TODO Auto-generated method stub
		return null;
	}
}
