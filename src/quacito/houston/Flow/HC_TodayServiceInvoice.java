package quacito.houston.Flow;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import quacito.houston.CommonUtilities.CheckableRelativeLayout;
import quacito.houston.CommonUtilities.CommonFunction;
import quacito.houston.CommonUtilities.LoggerUtil;
import quacito.houston.CommonUtilities.ParameterUtil;
import quacito.houston.CommonUtilities.SharedPreference;
import quacito.houston.DBhelper.DatabaseHelper;
import quacito.houston.DBhelper.HoustonFlowFunctions;
import quacito.houston.Flow.GeneralInformationActivity.OnGalleryClick;
import quacito.houston.Flow.HR_TodayServiceInvoice.OnButtonClick;
import quacito.houston.Flow.HR_TodayServiceInvoice.signature;
import quacito.houston.Flow.servicehouston.R;

import quacito.houston.adapter.EmailAdapter;
import quacito.houston.adapter.GalleryImageAdapter;
import quacito.houston.horizontallistview.HorizontalListView;
import quacito.houston.model.AgreementMailIdDTO;
import quacito.houston.model.ChemicalDto;
import quacito.houston.model.HCommercial_pest_DTO;
import quacito.houston.model.GeneralInfoDTO;
import quacito.houston.model.ImgDto;
import quacito.houston.model.LogDTO;

import quacito.houston.model.UserDTO;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Paint.Style;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;

public class HC_TodayServiceInvoice extends Activity {
	LogDTO logDTO;
	HoustonFlowFunctions houstonFlowFunctions;
	HCommercial_pest_DTO hCommercial_pest_DTO;
	GeneralInfoDTO generalInfoDTO;
	UserDTO userDTO;
	public String current = null;
	private Bitmap mBitmap;
	View mView;
	File mypath;
	private MediaPlayer myPlayer;
	boolean issignature = false, isValidate = true;
	private TextView yourName;
	LinearLayout mContent;
	signature mSignature;
	Dialog signatureDialog, sendEmailDialog;
	ArrayList<String> aList, aList1;
	ArrayList<String> listSelectedChemicals = new ArrayList<String>();
	ArrayList<ChemicalDto> listChemicals = new ArrayList<ChemicalDto>();
	ArrayList<String> tempStoreImageName = new ArrayList<String>();
	List<String> MandatoryList = new ArrayList<String>();
	public static int RECORD_AUDIO_REQUEST_CODE = 102;
	CharSequence[] charSequenceItems;
	String cussignName, serviceID_new, strInsidepestControlServices,
			strOutsidepestControlServices, strAreasInspectedExterior,
			strAreasInspectedInterior, strCustomerSignature,
			strTechnicianSignature, straudioFile, strmultipleImageName;
	Button btnBack, mClear, mGetSign, mCancel, btnRecordAudio, btnAfterImage,
			btnSignature, btnIagreeSignature, btnsubmitToPestpac, btnSendEmail;
	ImageView playBtn, stopPlayBtn, imageViewCustomerSign,
			imageViewTechnicianSign;
	HorizontalListView imageListView;
	EditText edtCheckNo, edtDrivingLic, edtAmount, edtCustomer, edtAddress,
			edtBillingAddress, edtHomePhone, edtCellPhone, edtAcct,
			edtChargerForCurrentServices, edtBalanceDue, edtTax, edtEmp,
			edtTotalAmount, edtZip, edtOpenOfficeNote;

	RadioButton radioCash, radioCheck, radioCreditCard, radioBillLater,
			radioPreBill, radioNoCharge;

	TableRow rowHeadingPestControl;

	TextView txtAcct, txtTimeIn, txtTimeInHeader, txtEmail, text1,
			txtOrderNumber, txtServiceReportAddress, txtSERVICE_FOR,
			txtAreasInspectedHeadInside, txtAreasInspectedInside,
			txtAreasInspectedHeadOutside, txtAreasInspectedOutside,
			txtINSPECTED_TREATED, txtAnts, txtOtherPest, txtInterior,
			txtOUTSIDE_PERIMETER, txtDUMPSTER, txtDINING_ROOM, txtCOMMON_AREAS,
			txtKITCHEN, txtDRY_STORAGE, txtDISH_WSHING, txtROOF_TOPS,
			txtWAIT_STATION, txtDROP_CEILINGS, txtPLANTERS,
			txtWAREHOUSE_STORAGE, txtTREATED_CC, txtTREATED_SP, txtTREATED_PS,
			txtREPLACED_MS_STATION, txtREPLACED_MS, txtVISIBLE_IS,
			txtVisibleTermite_PS, txtInstalled_MS, txt_MS_STATION,
			txtRadioVisibleSigns, text_view_ServicePageHeader,
			txtPestControlHeadInside, txtPestControlServiesInside,
			txtPestControlHeadOutside, txtPestControlServiesOutside,
			txtSet_Live_Cages_howmany, txtSet_Live_Cages,
			txtSet_Snap_Traps_howmany, txtSet_Snap_Traps,
			txtRemoved_Rodent_Traps, txtRemoved_Rodent_Bait_Stations,
			txtRemoved_Live_Cages, txtRemoved_Snap_Traps,
			txtInspected_Live_Cages, txtInspected_Snap_Traps,
			txtSealed_Entry_Points, txtReplaced_Rodent_Traps_stations,
			txtReplaced_Rodent_Traps_howmany, txtReplaced_Rodent_Traps,
			txtInstalled_Rodent_Traps, txtInstalled_Rodent_Traps_howmany,
			txtCleaned_Rodent_Traps, txtInspected_Rodent_Traps,
			txtReplaced_Bait_Stations_stations, txtReplaced_Bait_Station,
			txtCheckedRunDurationTime, txtCheckedTimeSystemRuns,
			txtMosquitoServices, txtTechComments, textRecordingPoint,
			txtServicetech, txtOrder, txtLicence, txtdate, txtEmp,
			txtReplaced_Bait_Stations_howmany, txtReplaced_Bait_station_number,
			txtReplaced_Bait, txtInstalled_Rodent_Bait_Stations,
			txtInstalled_Rodent_Bait_howmany, txtCleaned_Rodent_Stations,
			txtHowManyInspected_Rodent_Stations, txt_recommendations,
			txtInspected_Rodent_Stations;

	LinearLayout rowInterior, rowOUTSIDE_PERIMETER, rowDUMPSTER,
			rowDINING_ROOM, rowCOMMON_AREAS, rowKITCHEN, rowDRY_STORAGE,
			rowDISH_WSHING, rowROOF_TOPS, rowWAIT_STATION, rowDROP_CEILINGS,
			rowPLANTERS, rowWAREHOUSE_STORAGE, rowTREATED_CC, rowTREATED_SP,
			rowTREATED_PS, rowREPLACED_MS_STATION, rowREPLACED_MS,
			rowVISIBLE_IS, rowVisibleTermite_PS, rowINSTALLED_MS,
			rowVisibleSignsMS_STATION, rowVisibleSignsMS, rowMosquitoServices,
			rowCheckedRunDurationTime, rowCheckedTimeSystemRuns,
			linearlayout_amount, linearlayout_CheckNo, LAYOUT_AREASINSPECTED,
			linearlayout_DrivingLic, LAYOUT_SERVICEFOR,
			LAYOUT_INSPECTED_TREATED, layoutAnts, layoutOtherPest,
			LAYOUT_PEST_ZONE, layoutChemicalHeader, mainLayout,
			layoutRODENTSERVICES, layout_mosquito_services,
			layout_pestControlserivces, layout_termiteserivces,
			layoutInspected_Rodent_Stations, layoutCleaned_Rodent_Stations,
			layoutInstalled_Rodent_Bait_Stations, layoutReplaced_Bait_Station,
			layoutReplaced_Bait, layoutInspected_Rodent_Traps,
			layoutCleaned_Rodent_Traps, layoutInstalled_Rodent_Traps,
			layoutReplaced_Rodent_Traps, layoutSealed_Entry_Points,
			layoutSet_Live_Cages, layoutSet_Snap_Traps,
			layoutRemoved_Rodent_Traps, layoutRemoved_Rodent_Bait_Stations,
			layoutRemoved_Live_Cages, layoutRemoved_Snap_Traps,
			Layout_TechComment_desc, Layout_TechComment,
			layoutInspected_Live_Cages, layoutInspected_Snap_Traps,
			layout_recommendations;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.hc_service_invoice_report);
		Thread.setDefaultUncaughtExceptionHandler(new TopExceptionHandler(this));
		
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setDisplayShowHomeEnabled(true);

		initialize();
		setValuesInComponents();
		if (generalInfoDTO.getServiceStatus().equals("Complete")) {
			btnAfterImage.setEnabled(false);
			btnRecordAudio.setEnabled(false);
			playBtn.setEnabled(false);
			btnSignature.setEnabled(false);
			btnIagreeSignature.setEnabled(false);
			btnsubmitToPestpac.setEnabled(false);
		} else {
			btnAfterImage.setEnabled(true);
			btnRecordAudio.setEnabled(true);
			playBtn.setEnabled(true);
			btnSignature.setEnabled(true);
			btnIagreeSignature.setEnabled(true);
			btnsubmitToPestpac.setEnabled(true);
		}
		super.onCreate(savedInstanceState);
	}

	private void setValuesInComponents() {
		// TODO Auto-generated method stub

		JSONObject jsServiceObject = houstonFlowFunctions
				.getTodaysCommercialReport(serviceID_new);
		if (jsServiceObject != null) {
			hCommercial_pest_DTO = new Gson().fromJson(jsServiceObject.toString(),
					HCommercial_pest_DTO.class);
		}

		setValuesForHeader();

		setValuesForServiceFor();
		setValuesForAreasInspected();
		setValuesForInspectedTreatedFor();
		setValuesForPestActivityByZone();
		setChemicalTable();
		setValuesForPestControlServices();
		setValuesForTermiteServices();
		setValuesForRodentServices();
		setValuesForMosquitoServices();
		setValuesForRecommendations();
		setValuesForInvoice();
		reloadPriviousBeforeImage();

		straudioFile = generalInfoDTO.getAudioFile();
		if (!CommonFunction.checkString(straudioFile, "").equals("")) {
			textRecordingPoint.setText("Audio available");
		} else {
			textRecordingPoint.setText("Audio not available");
		}

	}

	// alert dialog
	private void ShowSignatureDailog(String title, int which) {
		signatureDialog = new Dialog(HC_TodayServiceInvoice.this);
		signatureDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		signatureDialog.setContentView(R.layout.signature);
		signatureDialog.setCancelable(false);
		mContent = (LinearLayout) signatureDialog
				.findViewById(R.id.linearLayout);
		InitializeSignature(signatureDialog, which);

		signatureDialog.show();

	}

	public void InitializeSignature(final Dialog signatureDialog,
			final int which) {
		mContent = (LinearLayout) signatureDialog
				.findViewById(R.id.linearLayout);
		mSignature = new signature(this, null);
		mSignature.setBackgroundColor(Color.WHITE);
		mContent.addView(mSignature, LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT);
		mClear = (Button) signatureDialog.findViewById(R.id.clear);
		mGetSign = (Button) signatureDialog.findViewById(R.id.getsign);
		mGetSign.setEnabled(false);
		mCancel = (Button) signatureDialog.findViewById(R.id.cancel);
		mView = mContent;
		yourName = (TextView) signatureDialog.findViewById(R.id.yourName);

		switch (which) {
		case 1:
			yourName.setText(generalInfoDTO.getFirst_Name() + " "
					+ generalInfoDTO.getLast_Name());
			break;
		case 2:
			yourName.setText(generalInfoDTO.getFirst_Name() + " "
					+ generalInfoDTO.getLast_Name());

			break;
		default:
			break;
		}

		mClear.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// Log.v("log_tag", "Panel Cleared");
				mSignature.clear();
				issignature = false;
				mGetSign.setEnabled(false);
			}
		});
		mGetSign.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// Log.v("log_tag", "Panel Saved");
				boolean error = captureSignature();
				if (!error) {
					String name = yourName.getText().toString();
					// int auditid=MainActivity.auditDto.getId();
					// Log.e("audit id", auditid+"");
					switch (which) {
					case 1:

						current = serviceID_new + ".jpg";
						// Log.e("iMAGE name", current+"");
						cussignName = current;
						mypath = CommonFunction.getFileLocation(
								getApplicationContext(), current);
						mView.setDrawingCacheEnabled(true);
						mSignature.save(mView);

						strTechnicianSignature = cussignName;
						Bitmap technicianSignBitmap = getBimapByName(cussignName);
						imageViewTechnicianSign
								.setImageBitmap(technicianSignBitmap);

						break;
					case 2:
						current = serviceID_new + "_sales.jpg";
						// Log.e("iMAGE name", current+"");
						cussignName = current;
						mypath = CommonFunction.getFileLocation(
								getApplicationContext(), current);
						mView.setDrawingCacheEnabled(true);
						mSignature.save(mView);

						strCustomerSignature = cussignName;
						hCommercial_pest_DTO.setSignature(cussignName);
						Bitmap customerSignBitmap = getBimapByName(cussignName);
						imageViewCustomerSign
								.setImageBitmap(customerSignBitmap);

						break;
					default:
						break;
					}
					signatureDialog.dismiss();
				}
			}
		});
		mCancel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				signatureDialog.dismiss();
			}
		});
	}

	public class signature extends View {
		private static final float STROKE_WIDTH = 5f;
		private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
		private Paint paint = new Paint();
		private Path path = new Path();

		private float lastTouchX;
		private float lastTouchY;
		private final RectF dirtyRect = new RectF();

		public signature(Context context, AttributeSet attrs) {
			super(context, attrs);
			paint.setAntiAlias(true);
			paint.setColor(Color.BLACK);
			paint.setStyle(Paint.Style.STROKE);
			paint.setStrokeJoin(Paint.Join.ROUND);
			paint.setStrokeWidth(STROKE_WIDTH);
		}

		public void save(View v) {
			// Log.v("log_tag", "Width: " + v.getWidth());
			// Log.v("log_tag", "Height: " + v.getHeight());
			if (mBitmap == null) {
				mBitmap = Bitmap.createBitmap(mContent.getWidth(),
						mContent.getHeight(), Bitmap.Config.RGB_565);
				;
			}
			Canvas canvas = new Canvas(mBitmap);
			try {
				FileOutputStream mFileOutStream = new FileOutputStream(mypath);

				v.draw(canvas);
				mBitmap.compress(Bitmap.CompressFormat.JPEG, 90, mFileOutStream);
				mFileOutStream.flush();
				mFileOutStream.close();
				// String url = Images.Media.insertImage(getContentResolver(),
				// mBitmap, "title", null);
				// Log.e("log_tag","url: " + url);
				// In case you want to delete the file
				// boolean deleted = mypath.delete();
				// Log.v("log_tag","deleted: " + mypath.toString() + deleted);
				// If you want to convert the image to string use base64
				// converter

			} catch (Exception e) {
				Log.v("log_tag", e.toString());
			}
		}

		public void clear() {
			path.reset();
			invalidate();
		}

		@Override
		protected void onDraw(Canvas canvas) {
			canvas.drawPath(path, paint);
		}

		@Override
		public boolean onTouchEvent(MotionEvent event) {
			float eventX = event.getX();
			float eventY = event.getY();
			mGetSign.setEnabled(true);
			issignature = true;
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				path.moveTo(eventX, eventY);
				lastTouchX = eventX;
				lastTouchY = eventY;
				return true;

			case MotionEvent.ACTION_MOVE:

			case MotionEvent.ACTION_UP:

				resetDirtyRect(eventX, eventY);
				int historySize = event.getHistorySize();
				for (int i = 0; i < historySize; i++) {
					float historicalX = event.getHistoricalX(i);
					float historicalY = event.getHistoricalY(i);
					expandDirtyRect(historicalX, historicalY);
					path.lineTo(historicalX, historicalY);
				}
				path.lineTo(eventX, eventY);
				break;

			default:
				debug("Ignored touch event: " + event.toString());
				return false;
			}

			invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
					(int) (dirtyRect.top - HALF_STROKE_WIDTH),
					(int) (dirtyRect.right + HALF_STROKE_WIDTH),
					(int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

			lastTouchX = eventX;
			lastTouchY = eventY;

			return true;
		}

		private void debug(String string) {
		}

		private void expandDirtyRect(float historicalX, float historicalY) {
			if (historicalX < dirtyRect.left) {
				dirtyRect.left = historicalX;
			} else if (historicalX > dirtyRect.right) {
				dirtyRect.right = historicalX;
			}

			if (historicalY < dirtyRect.top) {
				dirtyRect.top = historicalY;
			} else if (historicalY > dirtyRect.bottom) {
				dirtyRect.bottom = historicalY;
			}
		}

		private void resetDirtyRect(float eventX, float eventY) {
			dirtyRect.left = Math.min(lastTouchX, eventX);
			dirtyRect.right = Math.max(lastTouchX, eventX);
			dirtyRect.top = Math.min(lastTouchY, eventY);
			dirtyRect.bottom = Math.max(lastTouchY, eventY);
		}
	}

	private boolean captureSignature() {

		boolean error = false;
		String errorMessage = "";

		if (yourName.getText().toString().equalsIgnoreCase("")) {
			errorMessage = errorMessage + "Enter your Name\n";
			error = true;
		}

		if (error) {
			Toast toast = Toast
					.makeText(this, errorMessage, Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.TOP, 105, 50);
			toast.show();
		}

		return error;
	}

	public void reloadPriviousBeforeImage() {
		String imageNames = generalInfoDTO.getAfterImage();
		// Toast.makeText(getApplicationContext(),
		// imageNames+"",Toast.LENGTH_LONG).show();
		if (!CommonFunction.checkString(imageNames, "").equals("")) {
			String[] imageArray = imageNames.split(",");
			for (int i = 0; i < imageArray.length; i++) {
				if (!imageArray[i].equals("")) {
					tempStoreImageName.add(imageArray[i]);
					RefreshImage();
				}
			}
		}
		strCustomerSignature = hCommercial_pest_DTO.getSignature();
		strTechnicianSignature = hCommercial_pest_DTO.getServiceSignature();
		// Toast.makeText(getApplicationContext(),
		// strCustomerSignature,Toast.LENGTH_LONG).show();
		Bitmap customerSignBitmap = getBimapByName(strCustomerSignature);
		Bitmap technicianSignBitmap = getBimapByName(strTechnicianSignature);
		imageViewCustomerSign.setImageBitmap(customerSignBitmap);
		imageViewTechnicianSign.setImageBitmap(technicianSignBitmap);
	}

	public Bitmap getBimapByName(String ImageName) {
		if (ImageName == null || ImageName.equals("")) {
			return null;
		}

		Bitmap bmp = null;
		try {
			// Log.e("Image Name in refresh",ImageName);
			File profileImage = CommonFunction.getFileLocation(
					getApplicationContext(), ImageName);
			if (profileImage.exists()) {
				bmp = decodeUri(Uri.fromFile(profileImage));
				// bmp = scaleBitmap(bmp, 100, 100);
			} else {
				return bmp;
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			// bmp=getResources().getDrawable(R.drawable.noimage);
			e.printStackTrace();
		}

		return bmp;
	}

	ArrayList<String> tempImagList = new ArrayList<String>();
	ArrayList<ImgDto> listImg = new ArrayList<ImgDto>();
	GalleryImageAdapter adapter;
	String ImageName;
	public static Bitmap bitmap;
	public static int REQUEST_Camera_ACTIVITY_CODE = 101;
	int imageCount = 1;

	public void takePicture() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		ImageName = generalInfoDTO.getServiceID_new() + "_" + imageCount
				+ "after.jpg"; // CommonFunction.CreateImageName();
		Uri imageUri = Uri.fromFile(CommonFunction.getFileLocation(
				getApplicationContext(), ImageName));
		intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
		intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
		intent.putExtra(MediaStore.EXTRA_SIZE_LIMIT,
				Integer.toString(1024 * 1024));
		startActivityForResult(intent, REQUEST_Camera_ACTIVITY_CODE);

	}

	
	private Bitmap decodeSmallUri(Uri selectedImage)
			throws FileNotFoundException {
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(getApplicationContext().getContentResolver()
				.openInputStream(selectedImage), null, o);

		final int REQUIRED_SIZE = 500;

		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeStream(getApplicationContext()
				.getContentResolver().openInputStream(selectedImage), null, o2);
	}

	public void getfile(String fileName) {
		try {
			// Log.e("Get File Called", fileName);

			File imagefile = CommonFunction.getFileLocation(
					getApplicationContext(), fileName);
			if (imagefile.exists()) {
				// Bitmap myBitmap =
				// BitmapFactory.decodeFile(imagefile.getAbsolutePath());

				Bitmap myBitmap = decodeUri(Uri.fromFile(CommonFunction
						.getFileLocation(getApplicationContext(), fileName)));
				// Log.e("Input Bitmap width and heigth  ",
				// myBitmap.getWidth()+" "+myBitmap.getWidth());
				// edtDescription.setText(fileName+" "+myBitmap.getWidth()+" and "+myBitmap.getHeight());
				Bitmap output = Bitmap.createScaledBitmap(myBitmap, 800, 800,
						true);
				// Log.e("Out Put Bitmap width and heigth  ",
				// output.getWidth()+" "+output.getWidth());
				imagefile.delete();
				CommonFunction.saveBitmap(output, getApplicationContext(),
						fileName);
			}
		} catch (Exception e) {

		}
	}

	private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(getApplicationContext().getContentResolver()
				.openInputStream(selectedImage), null, o);

		final int REQUIRED_SIZE = 800;

		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeStream(getApplicationContext()
				.getContentResolver().openInputStream(selectedImage), null, o2);
	}

	public static Bitmap scaleBitmap(Bitmap bitmapToScale, float newWidth,
			float newHeight) {
		if (bitmapToScale == null)
			return null;
		// get the original width and height
		int width = bitmapToScale.getWidth();
		int height = bitmapToScale.getHeight();
		// create a matrix for the manipulation
		Matrix matrix = new Matrix();

		// resize the bit map
		matrix.postScale(newWidth / width, newHeight / height);

		// recreate the new Bitmap and set it back
		return Bitmap.createBitmap(bitmapToScale, 0, 0,
				bitmapToScale.getWidth(), bitmapToScale.getHeight(), matrix,
				true);
	}

	private void setValuesForInvoice() {
		// TODO Auto-generated method stub
		if (CommonFunction.checkString(hCommercial_pest_DTO.getTechnicianComments(),
				"").equals("")) {
			Layout_TechComment.setVisibility(View.GONE);
			Layout_TechComment_desc.setVisibility(View.GONE);
		} else {
			Layout_TechComment.setVisibility(View.VISIBLE);
			Layout_TechComment_desc.setVisibility(View.VISIBLE);
			txtTechComments.setText(hCommercial_pest_DTO.getTechnicianComments());
		}
		txtServicetech.setText(hCommercial_pest_DTO.getTechnician());
		txtTimeIn.setText(hCommercial_pest_DTO.getTimeIn());
		txtLicence.setText(userDTO.getLicenceNo());
		txtdate.setText(hCommercial_pest_DTO.getDate());
		txtEmp.setText(hCommercial_pest_DTO.getEmpNo());
		edtChargerForCurrentServices.setText("$ "
				+ generalInfoDTO.getInv_value());
		// edtBalanceDue.setText("$ "+String.valueOf(Float.valueOf(generalInfoDTO.getTotal())
		// - Float.valueOf(generalInfoDTO.getInv_value())));
		edtBalanceDue.setText("$ " + generalInfoDTO.getLocationBalance());
		edtTax.setText("$ " + generalInfoDTO.getTax_value());

		Log.e("check gene info",
				generalInfoDTO.getTax_value() + " "
						+ generalInfoDTO.getInv_value() + " "
						+ generalInfoDTO.getServiceID_new());

		Double totlaD = Double.valueOf(generalInfoDTO.getTax_value())
				+ Double.valueOf(generalInfoDTO.getInv_value());
		Double totalFinal = Math.round(totlaD * 100.0) / 100.0;
		edtTotalAmount.setText("$ " + String.valueOf(totalFinal));
		// edtTotalAmount.setText("$ "+generalInfoDTO.getTotal());
		edtOpenOfficeNote.setText(generalInfoDTO.getOffice_Note_ByRep());
	}

	private void setValuesForRecommendations() {
		// TODO Auto-generated method stub
		Log.e("txt_recommendations", "" + hCommercial_pest_DTO.getRecommendations());

		if (!CommonFunction.checkString(hCommercial_pest_DTO.getRecommendations(), "")
				.equals("")) {
			layout_recommendations.setVisibility(View.VISIBLE);
			txt_recommendations.setText(hCommercial_pest_DTO.getRecommendations());

		} else {
			layout_recommendations.setVisibility(View.GONE);
		}

	}

	private void setValuesForMosquitoServices() {
		// TODO Auto-generated method stub
		String strMosquitoServices = hCommercial_pest_DTO.getPlusMosquitoDetail();
		// straudioFile=serviceDTO.getAudioFile();
		if (strMosquitoServices.length() >= 1
				|| !CommonFunction.checkString(hCommercial_pest_DTO.getDurationTime(),
						"").equals("")
				|| !CommonFunction.checkString(hCommercial_pest_DTO.getSystemRuns(),
						"").equals("")) {
			layout_mosquito_services.setVisibility(View.VISIBLE);

			if (strMosquitoServices.length() >= 1) {
				txtMosquitoServices.setVisibility(View.VISIBLE);
				txtMosquitoServices.setText(strMosquitoServices);
			} else {
				txtMosquitoServices.setVisibility(View.GONE);
			}
			
			if (!CommonFunction
					.checkString(hCommercial_pest_DTO.getDurationTime(), "")
					.equals("")) {
				rowCheckedRunDurationTime.setVisibility(View.VISIBLE);
				txtCheckedRunDurationTime.setText(hCommercial_pest_DTO
						.getDurationTime());
			} else {
				rowCheckedRunDurationTime.setVisibility(View.GONE);
			}
			if (!CommonFunction.checkString(hCommercial_pest_DTO.getSystemRuns(), "")
					.equals("")) {
				rowCheckedTimeSystemRuns.setVisibility(View.VISIBLE);
				txtCheckedTimeSystemRuns.setText(hCommercial_pest_DTO.getSystemRuns());

			} else {
				rowCheckedTimeSystemRuns.setVisibility(View.GONE);
			}
		} else {
			layout_mosquito_services.setVisibility(View.GONE);
		}
	}

	private void setValuesForRodentServices() {
		// TODO Auto-generated method stub
		if (hCommercial_pest_DTO.getInspectedRodentStation().equals(
				"Inspected Rodent Stations")) {
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutInspected_Rodent_Stations.setVisibility(View.VISIBLE);
			txtHowManyInspected_Rodent_Stations.setText(hCommercial_pest_DTO
					.getTxtInspectedRodentStation());
		} else {
			layoutInspected_Rodent_Stations.setVisibility(View.GONE);
		}

		if (hCommercial_pest_DTO.getCleanedRodentStation().equals(
				"Cleaned Rodent Stations")) {
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutCleaned_Rodent_Stations.setVisibility(View.VISIBLE);
			txtCleaned_Rodent_Stations.setText(hCommercial_pest_DTO
					.getIsCleanedRodentStation());
		} else {
			layoutCleaned_Rodent_Stations.setVisibility(View.GONE);
		}

		if (hCommercial_pest_DTO.getInstallRodentBaitStation().equals(
				"Installed Rodent Bait Stations")) {
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutInstalled_Rodent_Bait_Stations.setVisibility(View.VISIBLE);
			txtInstalled_Rodent_Bait_Stations.setText(hCommercial_pest_DTO
					.getIsInstallRodentBaitStation());
			txtInstalled_Rodent_Bait_howmany.setText(hCommercial_pest_DTO
					.getTxtInstallRodentBaitStation());
		} else {
			layoutInstalled_Rodent_Bait_Stations.setVisibility(View.GONE);
		}
		if (hCommercial_pest_DTO.getReplacedBait().equals("Replaced Bait")) {
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutReplaced_Bait.setVisibility(View.VISIBLE);
			txtReplaced_Bait.setText(hCommercial_pest_DTO.getIsReplacedBait());
			txtReplaced_Bait_station_number.setText(hCommercial_pest_DTO
					.getSNoReplacedBait());
		} else {
			layoutReplaced_Bait.setVisibility(View.GONE);
		}

		if (hCommercial_pest_DTO.getReplacedBaitStation().equals(
				"Replaced Bait Station")) {
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutReplaced_Bait_Station.setVisibility(View.VISIBLE);
			txtReplaced_Bait_Station.setText(hCommercial_pest_DTO.getIsReplacedBait());
			txtReplaced_Bait_Stations_stations.setText(hCommercial_pest_DTO
					.getSNoReplacedBaitStation());
			txtReplaced_Bait_Stations_howmany.setText(hCommercial_pest_DTO
					.getTxtReplacedBaitStation());
		} else {
			layoutReplaced_Bait_Station.setVisibility(View.GONE);
		}

		if (hCommercial_pest_DTO.getInspectedRodentTraps().equals(
				"Inspected Rodent Traps")) {
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutInspected_Rodent_Traps.setVisibility(View.VISIBLE);
			txtInspected_Rodent_Traps.setText(hCommercial_pest_DTO
					.getIsInspectedRodentTraps());

		} else {
			layoutInspected_Rodent_Traps.setVisibility(View.GONE);
		}

		if (hCommercial_pest_DTO.getCleanedRodentTrap().equals("Cleaned Rodent Traps")) {
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutCleaned_Rodent_Traps.setVisibility(View.VISIBLE);
			txtCleaned_Rodent_Traps.setText(hCommercial_pest_DTO
					.getIsCleanedRodentTrap());

		} else {
			layoutCleaned_Rodent_Traps.setVisibility(View.GONE);
		}

		if (hCommercial_pest_DTO.getInstallRodentTrap().equals(
				"Installed Rodent Traps")) {
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutInstalled_Rodent_Traps.setVisibility(View.VISIBLE);
			txtInstalled_Rodent_Traps.setText(hCommercial_pest_DTO
					.getIsInstallRodentTrap());
			txtInstalled_Rodent_Traps_howmany.setText(hCommercial_pest_DTO
					.getTxtInstallRodentTrap());
		} else {
			layoutInstalled_Rodent_Traps.setVisibility(View.GONE);
		}

		if (hCommercial_pest_DTO.getReplacedRodentTrap().equals(
				"Replaced Rodent Traps")) {
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutReplaced_Rodent_Traps.setVisibility(View.VISIBLE);
			txtReplaced_Rodent_Traps.setText(hCommercial_pest_DTO
					.getIsReplacedRodentTrap());
			txtReplaced_Rodent_Traps_howmany.setText(hCommercial_pest_DTO
					.getTxtReplacedRodentTrap());
			txtReplaced_Rodent_Traps_stations.setText(hCommercial_pest_DTO
					.getSNoReplacedRodentTrap());
		} else {
			layoutReplaced_Rodent_Traps.setVisibility(View.GONE);
		}

		if (hCommercial_pest_DTO.getSealedEntryPoint().equals("Sealed Entry Points")) {
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutSealed_Entry_Points.setVisibility(View.VISIBLE);
			txtSealed_Entry_Points.setText(hCommercial_pest_DTO
					.getIsReplacedRodentTrap());

		} else {
			layoutSealed_Entry_Points.setVisibility(View.GONE);
		}

		if (hCommercial_pest_DTO.getInspectedSnapTrap().equals("Inspected Snap Traps")) {
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutInspected_Snap_Traps.setVisibility(View.VISIBLE);
			txtInspected_Snap_Traps.setText(hCommercial_pest_DTO
					.getTxtInspectedSnapTrap());

		} else {
			layoutInspected_Snap_Traps.setVisibility(View.GONE);
		}

		if (hCommercial_pest_DTO.getInspectedLiveCages()
				.equals("Inspected Live Cages")) {
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutInspected_Live_Cages.setVisibility(View.VISIBLE);
			txtInspected_Live_Cages.setText(hCommercial_pest_DTO
					.getTxtInspectedLiveCages());

		} else {
			layoutInspected_Live_Cages.setVisibility(View.GONE);
		}

		if (hCommercial_pest_DTO.getRemovedSnapTraps().equals("Removed Snap Traps")) {
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutRemoved_Snap_Traps.setVisibility(View.VISIBLE);
			txtRemoved_Snap_Traps.setText(hCommercial_pest_DTO
					.getTxtRemovedSnapTraps());

		} else {
			layoutRemoved_Snap_Traps.setVisibility(View.GONE);
		}

		if (hCommercial_pest_DTO.getRemovedLiveCages().equals("Removed Live Cages")) {
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutRemoved_Live_Cages.setVisibility(View.VISIBLE);
			txtRemoved_Live_Cages.setText(hCommercial_pest_DTO
					.getTxtRemovedLiveCages());

		} else {
			layoutRemoved_Live_Cages.setVisibility(View.GONE);
		}

		if (hCommercial_pest_DTO.getRemovedRodentBaitStation().equals(
				"Removed Rodent Bait Stations")) {
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutRemoved_Rodent_Bait_Stations.setVisibility(View.VISIBLE);
			txtRemoved_Rodent_Bait_Stations.setText(hCommercial_pest_DTO
					.getTxtRemovedRodentBaitStation());

		} else {
			layoutRemoved_Rodent_Bait_Stations.setVisibility(View.GONE);
		}

		if (hCommercial_pest_DTO.getRemovedRodentTrap().equals("Removed Rodent Traps")) {
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutRemoved_Rodent_Traps.setVisibility(View.VISIBLE);
			txtRemoved_Rodent_Traps.setText(hCommercial_pest_DTO
					.getTxtRemovedRodentTrap());

		} else {
			layoutRemoved_Rodent_Traps.setVisibility(View.GONE);
		}

		if (hCommercial_pest_DTO.getSetSnapTrap().equals("Set Snap Traps")) {
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutSet_Snap_Traps.setVisibility(View.VISIBLE);
			txtSet_Snap_Traps.setText(hCommercial_pest_DTO.getTxtSetSnapTrap());
			txtSet_Snap_Traps_howmany
					.setText(hCommercial_pest_DTO.getLstSetSnapTrap());

		} else {
			layoutSet_Snap_Traps.setVisibility(View.GONE);
		}

		if (hCommercial_pest_DTO.getSetLiveCages().equals("Set Live Cages")) {
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutSet_Live_Cages.setVisibility(View.VISIBLE);
			txtSet_Live_Cages.setText(hCommercial_pest_DTO.getTxtSetLiveCages());
			txtSet_Live_Cages_howmany.setText(hCommercial_pest_DTO
					.getLstSetLiveCages());

		} else {
			layoutSet_Live_Cages.setVisibility(View.GONE);
		}
	}

	private void setValuesForTermiteServices() {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		if (hCommercial_pest_DTO.getSignOfTermiteMS().equals(
				"Visible Signs of Termites in the Monitoring Stations")) {
			layout_termiteserivces.setVisibility(View.VISIBLE);
			rowVisibleSignsMS.setVisibility(View.VISIBLE);

			txtRadioVisibleSigns.setText(hCommercial_pest_DTO
					.getSignOfTermiteActivityMS());
			txt_MS_STATION.setText(hCommercial_pest_DTO.getSignOfTermiteText());
		} else {
			rowVisibleSignsMS.setVisibility(View.GONE);
		}

		if (hCommercial_pest_DTO.getSignOfTermitePS().equals(
				"Visible Signs of Termites on the Perimeter of the Structure")) {
			layout_termiteserivces.setVisibility(View.VISIBLE);
			rowVisibleTermite_PS.setVisibility(View.VISIBLE);

			txtVisibleTermite_PS.setText(hCommercial_pest_DTO
					.getSignOfTermiteActivityPS());

		} else {
			rowVisibleTermite_PS.setVisibility(View.GONE);
		}

		if (hCommercial_pest_DTO.getSignOfTermiteIS().equals(
				"Visible Signs of Termites on the Interior of the Structure")) {
			layout_termiteserivces.setVisibility(View.VISIBLE);
			rowVISIBLE_IS.setVisibility(View.VISIBLE);

			txtVISIBLE_IS.setText(hCommercial_pest_DTO.getSignOfTermiteActivityIS());

		} else {
			rowVISIBLE_IS.setVisibility(View.GONE);
		}

		if (hCommercial_pest_DTO.getInstallMS()
				.equals("Installed Monitoring stations")) {
			layout_termiteserivces.setVisibility(View.VISIBLE);
			rowINSTALLED_MS.setVisibility(View.VISIBLE);

			txtInstalled_MS.setText(hCommercial_pest_DTO.getInstallMSText());

		} else {
			rowINSTALLED_MS.setVisibility(View.GONE);
		}

		if (hCommercial_pest_DTO.getReplacedMS()
				.equals("Replaced Monitoring Stations")) {
			layout_termiteserivces.setVisibility(View.VISIBLE);
			rowREPLACED_MS.setVisibility(View.VISIBLE);

			txtREPLACED_MS.setText(hCommercial_pest_DTO.getReplacedActivityTextMS());
			txtREPLACED_MS_STATION.setText(hCommercial_pest_DTO.getReplacedTextMS());

		} else {
			rowREPLACED_MS.setVisibility(View.GONE);
		}

		if (hCommercial_pest_DTO.getTreatedPS().equals(
				"Treated Perimeter of Structure")) {
			layout_termiteserivces.setVisibility(View.VISIBLE);
			rowTREATED_PS.setVisibility(View.VISIBLE);

			txtTREATED_PS.setText(hCommercial_pest_DTO.getTreatedActivityPS());

		} else {
			rowTREATED_PS.setVisibility(View.GONE);
		}

		if (hCommercial_pest_DTO.getTreatedSP().equals("Treated Slab Penetrations")) {
			layout_termiteserivces.setVisibility(View.VISIBLE);
			rowTREATED_SP.setVisibility(View.VISIBLE);

			txtTREATED_SP.setText(hCommercial_pest_DTO.getTreatedActivitySP());

		} else {
			rowTREATED_SP.setVisibility(View.GONE);
		}

		if (hCommercial_pest_DTO.getTreatedCC().equals("Treated Condusive Conditions")) {
			layout_termiteserivces.setVisibility(View.VISIBLE);
			rowTREATED_CC.setVisibility(View.VISIBLE);

			txtTREATED_CC.setText(hCommercial_pest_DTO.getTreatedActivityCC());

		} else {
			rowTREATED_CC.setVisibility(View.GONE);
		}
	}

	private void setValuesForPestControlServices() {
		if (!CommonFunction.checkString(strInsidepestControlServices, "")
				.equals("")
				&& !CommonFunction.checkString(strInsidepestControlServices,
						"Interior").equals("Interior")) {
			String str = strInsidepestControlServices;
			String kept = str.substring(0, 8);
			String remainder = str.substring(9, str.length());
			layout_pestControlserivces.setVisibility(View.VISIBLE);
			txtPestControlHeadInside.setText(kept);
			txtPestControlServiesInside.setText(remainder);
		}
		if (!CommonFunction.checkString(strOutsidepestControlServices, "")
				.equals("")
				&& !CommonFunction.checkString(strOutsidepestControlServices,
						"Exterior").equals("Exterior")) {

			String str = strOutsidepestControlServices;
			String kept = str.substring(0, 8);
			String remainder = str.substring(9, str.length());
			layout_pestControlserivces.setVisibility(View.VISIBLE);
			txtPestControlHeadOutside.setText(kept);
			txtPestControlServiesOutside.setText(remainder);
		}
	}

	private void setChemicalTable() {
		LinearLayout header = (LinearLayout) findViewById(R.id.layoutChemicalHeader);
		// header.setVisibility(View.GONE);
		// TODO Auto-generated method stub
		JSONArray jsChemical = houstonFlowFunctions.getAllSelectedChemicals(
				DatabaseHelper.TABLE_SELECTED_CHEMICALS, serviceID_new,"Commercial");
		listChemicals.clear();
		listChemicals = new Gson().fromJson(jsChemical.toString(),
				new TypeToken<List<ChemicalDto>>() {
				}.getType());
		// Log.e("",
		// "dfdf"+listChemicals.get(0).getProduct()+".."+listChemicals.get(1).getProduct());
		if (listChemicals.size() == 0) {
			layoutChemicalHeader.setVisibility(View.GONE);
		}
		for (int i = 0; i < listChemicals.size(); i++) {
			AddChemicals(i);
		}
	}

	int i = 0;
	TableLayout tb;
	TableRow deleteTableRow;
	int numberofaudit;
	TableRow tr;
	ArrayList<TableRow> trlist = new ArrayList<TableRow>();

	private void AddChemicals(final int listindex) {
		// TODO Auto-generated method stub
		numberofaudit++;
		LoggerUtil.e("serviceID_new", serviceID_new);
		ChemicalDto dto = listChemicals.get(listindex);
		if (i == 0) {
			tb = new TableLayout(HC_TodayServiceInvoice.this);
			// addHeader();
			mainLayout.addView(tb);
		}
		i = i + 1;
		tr = new TableRow(getApplicationContext());
		trlist.add(tr);
		if (i % 2 != 0)

			tr.setBackgroundColor(Color.parseColor("#EDEDEF"));
		tr.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.FILL_PARENT));
		tr.setPadding(0, 10, 0, 10);

		LayoutParams params2 = new TableRow.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT, 1.0f);

		LayoutParams paramsProd = new TableRow.LayoutParams(70,
				LayoutParams.FILL_PARENT, 1.0f);

		TextView product = new TextView(getApplicationContext());
		product.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
		product.setTextColor(Color.BLACK);
		product.setLayoutParams(paramsProd);

		if (listChemicals.get(listindex).getProduct().equals("Other")
				&& !CommonFunction.checkString(
						listChemicals.get(listindex).getOtherText(), "")
						.equals("")) {
			product.setText(listChemicals.get(listindex).getOtherText());
		} else {
			product.setText(listChemicals.get(listindex).getProduct());
		}
		tr.addView(product);

		TextView percent = new TextView(getApplicationContext());
		percent.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
		percent.setText(listChemicals.get(listindex).getPercentPes());
		percent.setLayoutParams(params2);
		percent.setTextColor(Color.BLACK);
		tr.addView(percent);

		TextView AMT = new TextView(getApplicationContext());
		AMT.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
		AMT.setText(listChemicals.get(listindex).getAmount());
		// AMT.setPadding(0, 0,5, 0);
		AMT.setLayoutParams(params2);
		AMT.setTextColor(Color.BLACK);
		tr.addView(AMT);

		TextView UNIT = new TextView(getApplicationContext());
		UNIT.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
		UNIT.setText(listChemicals.get(listindex).getUnit());
		// UNIT.setPadding(0, 0,5, 0);
		UNIT.setLayoutParams(params2);
		UNIT.setTextColor(Color.BLACK);
		tr.addView(UNIT);

		LayoutParams paramstarget = new TableRow.LayoutParams(50,
				LayoutParams.FILL_PARENT, 1.0f);

		TextView TARGET = new TextView(getApplicationContext());
		TARGET.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
		TARGET.setText(listChemicals.get(listindex).getTarget());
		// TARGET.setPadding(0, 0,5, 0);
		TARGET.setLayoutParams(paramstarget);
		TARGET.setTextColor(Color.BLACK);
		tr.addView(TARGET);
		tb.addView(tr, new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT));
	}

	public void addHeader() {
		TableRow tr = new TableRow(getApplicationContext());
		if (i % 2 != 0)

			tr.setBackgroundColor(Color.parseColor("#2F80B7"));
		// tr.setOrientation(LinearLayout.HORIZONTAL);
		tr.setBackgroundColor(Color.parseColor("#2F80B7"));
		tr.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.FILL_PARENT));
		// tr.setWeightSum(1);
		tr.setPadding(0, 10, 0, 10);

		// LayoutParams params0 = new
		// TableRow.LayoutParams(android.widget.LinearLayout.LayoutParams.WRAP_CONTENT,
		// LayoutParams.FILL_PARENT);

		TextView product = new TextView(getApplicationContext());
		product.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
		product.setText(getApplicationContext().getResources().getString(
				R.string.PRODUCT));
		product.setTextColor(Color.WHITE);
		LayoutParams params1 = new TableRow.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.FILL_PARENT, 1f);
		product.setLayoutParams(params1);
		tr.addView(product);
		LayoutParams params2 = new TableRow.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT, .3f);

		TextView percent = new TextView(getApplicationContext());
		percent.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
		percent.setText(getApplicationContext().getResources().getString(
				R.string.percent));
		percent.setLayoutParams(params2);
		percent.setTextColor(Color.WHITE);
		tr.addView(percent);

		TextView AMT = new TextView(getApplicationContext());
		AMT.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
		AMT.setText(getApplicationContext().getResources().getString(
				R.string.AMT));
		AMT.setPadding(0, 0, 5, 0);
		AMT.setLayoutParams(params2);
		AMT.setTextColor(Color.WHITE);
		tr.addView(AMT);

		TextView UNIT = new TextView(getApplicationContext());
		UNIT.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
		UNIT.setText(getApplicationContext().getResources().getString(
				R.string.UNIT));
		UNIT.setPadding(0, 0, 5, 0);
		UNIT.setLayoutParams(params2);
		UNIT.setTextColor(Color.WHITE);
		tr.addView(UNIT);

		TextView TARGET = new TextView(getApplicationContext());
		TARGET.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
		TARGET.setText(getApplicationContext().getResources().getString(
				R.string.TARGET));
		TARGET.setPadding(0, 0, 5, 0);
		TARGET.setLayoutParams(params2);
		TARGET.setTextColor(Color.WHITE);
		tr.addView(TARGET);

		tb.addView(tr, new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT));
	}

	private void setValuesForPestActivityByZone() {
		// TODO Auto-generated method stub
		// set value for pest rodent observed today
		String strPestActivityZone = hCommercial_pest_DTO.getPestActivity();
		// Log.e("", "strpestActivityByZone..in final."+strPestActivityZone);
		aList = null;
		aList = new ArrayList<String>(Arrays.asList(strPestActivityZone
				.split(",")));
		// Log.e("", "aList.size().."+aList.get(0));
		if (aList.size() > 0 && !aList.get(0).equals("")) {
			LAYOUT_PEST_ZONE.setVisibility(View.VISIBLE);
			for (int i = 0; i < aList.size(); i++) {
				// Log.e("", "alist.geti.."+aList.get(i));
				if (aList.get(i).equals("Interior")) {
					rowInterior.setVisibility(View.VISIBLE);
					txtInterior.setText(hCommercial_pest_DTO.getInterior());
				}

				if (aList.get(i).equals("Outside Perimeter")) {
					rowOUTSIDE_PERIMETER.setVisibility(View.VISIBLE);
					txtOUTSIDE_PERIMETER.setText(hCommercial_pest_DTO
							.getOutsidef_Perimeter());
				}

				if (aList.get(i).equals("Dumpster")) {
					rowDUMPSTER.setVisibility(View.VISIBLE);
					txtDUMPSTER.setText(hCommercial_pest_DTO.getDumpster());
				}

				if (aList.get(i).equals("Dining Room")) {
					rowDINING_ROOM.setVisibility(View.VISIBLE);
					txtDINING_ROOM.setText(hCommercial_pest_DTO.getDining_Room());
				}

				if (aList.get(i).equals("Common Areas")) {
					rowCOMMON_AREAS.setVisibility(View.VISIBLE);
					txtCOMMON_AREAS.setText(hCommercial_pest_DTO.getCommon_Areas());
				}

				if (aList.get(i).equals("Kitchen")) {
					rowKITCHEN.setVisibility(View.VISIBLE);
					txtKITCHEN.setText(hCommercial_pest_DTO.getKitchen());
				}

				if (aList.get(i).equals("Dry Storage")) {
					rowDRY_STORAGE.setVisibility(View.VISIBLE);
					txtDRY_STORAGE.setText(hCommercial_pest_DTO.getDry_Storage());
				}

				if (aList.get(i).equals("Dish Washing")) {
					rowDISH_WSHING.setVisibility(View.VISIBLE);
					txtDISH_WSHING.setText(hCommercial_pest_DTO.getDish_Washing());
				}

				if (aList.get(i).equals("Roof Tops")) {
					rowROOF_TOPS.setVisibility(View.VISIBLE);
					txtROOF_TOPS.setText(hCommercial_pest_DTO.getRoof_Tops());
				}

				if (aList.get(i).equals("Wait Stations")) {
					rowWAIT_STATION.setVisibility(View.VISIBLE);
					txtWAIT_STATION.setText(hCommercial_pest_DTO.getWait_Stations());
				}

				if (aList.get(i).equals("Drop Ceiling")) {
					rowDROP_CEILINGS.setVisibility(View.VISIBLE);
					txtDROP_CEILINGS.setText(hCommercial_pest_DTO.getDrop_Ceiling());
				}

				if (aList.get(i).equals("Planters")) {
					rowPLANTERS.setVisibility(View.VISIBLE);
					txtPLANTERS.setText(hCommercial_pest_DTO.getPlanters());
				}

				if (aList.get(i).equals("Warehouse Storage")) {
					rowWAREHOUSE_STORAGE.setVisibility(View.VISIBLE);
					txtWAREHOUSE_STORAGE.setText(hCommercial_pest_DTO
							.getWarehouse_Storage());
				}

			}
		}

		else {
			LAYOUT_PEST_ZONE.setVisibility(View.GONE);
		}
	}

	private void setValuesForInspectedTreatedFor() {
		// TODO Auto-generated method stub
		String strInspectedTreatedFor = hCommercial_pest_DTO.getInsectActivity();
		// straudioFile=serviceDTO.getAudioFile();
		if (strInspectedTreatedFor.length() >= 1) {
			LAYOUT_INSPECTED_TREATED.setVisibility(View.VISIBLE);
			txtINSPECTED_TREATED.setText(strInspectedTreatedFor);
		} else {
			LAYOUT_INSPECTED_TREATED.setVisibility(View.GONE);
		}
		if (!CommonFunction.checkString(hCommercial_pest_DTO.getAnts(), "").equals("")) {
			layoutAnts.setVisibility(View.VISIBLE);
			txtAnts.setText(hCommercial_pest_DTO.getAnts());
		} else {
			layoutAnts.setVisibility(View.GONE);
		}
		if (!CommonFunction.checkString(hCommercial_pest_DTO.getOtherPest(), "")
				.equals("")) {
			layoutOtherPest.setVisibility(View.VISIBLE);
			txtOtherPest.setText(hCommercial_pest_DTO.getOtherPest());
		} else {
			layoutOtherPest.setVisibility(View.GONE);
		}
	}

	private void setValuesForAreasInspected() {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		// String strAreasInspectedExterior = commercialDTO.getAreasInspected();
		// straudioFile=serviceDTO.getAudioFile();
		if (!CommonFunction.checkString(
				hCommercial_pest_DTO.getAreasInspectedExterior(), "").equals("")) {
			LAYOUT_AREASINSPECTED.setVisibility(View.VISIBLE);
			txtAreasInspectedHeadOutside.setVisibility(View.VISIBLE);
			txtAreasInspectedOutside.setVisibility(View.VISIBLE);
			txtAreasInspectedHeadOutside.setText("Exterior");
			txtAreasInspectedOutside.setText(hCommercial_pest_DTO
					.getAreasInspectedExterior());
		}
		if (!CommonFunction.checkString(
				hCommercial_pest_DTO.getAreasInspectedInterior(), "").equals("")) {

			LAYOUT_AREASINSPECTED.setVisibility(View.VISIBLE);
			txtAreasInspectedHeadInside.setVisibility(View.VISIBLE);
			txtAreasInspectedInside.setVisibility(View.VISIBLE);
			txtAreasInspectedHeadInside.setText("Interior");
			txtAreasInspectedInside.setText(hCommercial_pest_DTO
					.getAreasInspectedInterior());
		}

	}

	private void setValuesForServiceFor() {
		// TODO Auto-generated method stub
		String strServiceFor = hCommercial_pest_DTO.getServiceFor();
		// straudioFile=serviceDTO.getAudioFile();
		if (strServiceFor.length() >= 1) {
			LAYOUT_SERVICEFOR.setVisibility(View.VISIBLE);
			txtSERVICE_FOR.setText(strServiceFor);
		} else {
			LAYOUT_SERVICEFOR.setVisibility(View.GONE);
		}
	}

	private void setValuesForHeader() {
		// TODO Auto-generated method stub
		// txtServiceReportAddress.setText(generalInfoDTO.getServiceAddress());

		// txtName.setText(generalInfoDTO.getFirst_Name());
		txtAcct.setText(generalInfoDTO.getAccountNo());
		// txtTimeIn.setText(generalInfoDTO.getTimeIn());
		txtOrderNumber.setText(generalInfoDTO.getOrder_Number());
		edtAmount.setText("$ " + hCommercial_pest_DTO.getAmount());
		edtCheckNo.setText(generalInfoDTO.getCheckNo());
		edtDrivingLic.setText(generalInfoDTO.getLicenseNo());
		txtTimeInHeader.setText(generalInfoDTO.getTimeIn());
		edtCustomer.setText(hCommercial_pest_DTO.getCustomer());
		// txtAcct.setText(generalInfoDTO.getAccountNo());

		String strServiceAddress = generalInfoDTO.getAddress_Line1() + ", "
				+ generalInfoDTO.getAddress_Line2() + " "
				+ generalInfoDTO.getCity() + ", " + generalInfoDTO.getState()
				+ ", " + generalInfoDTO.getZipCode();
		edtAddress.setText(strServiceAddress);

		String strBillingAddress = generalInfoDTO.getBAddress_Line1() + ", "
				+ generalInfoDTO.getBAddress_Line2() + " "
				+ generalInfoDTO.getBCity() + ", " + generalInfoDTO.getBState()
				+ ", " + generalInfoDTO.getBZipCode();
		edtBillingAddress.setText(strBillingAddress);

		edtHomePhone.setText(generalInfoDTO.getHome_Phone());
		edtCellPhone.setText(generalInfoDTO.getCell_Phone());
		edtZip.setText(generalInfoDTO.getZipCode());
		edtAcct.setText(generalInfoDTO.getAccountNo());
		txtEmail.setText(generalInfoDTO.getNewEmail());
		txtOrder.setText(generalInfoDTO.getOrder_Number());
		txtOrderNumber.setText(generalInfoDTO.getOrder_Number());

		ArrayList<RadioButton> listRadioButton = new ArrayList<RadioButton>();
		listRadioButton.add(radioCash);
		listRadioButton.add(radioCheck);
		listRadioButton.add(radioCreditCard);
		listRadioButton.add(radioNoCharge);
		listRadioButton.add(radioBillLater);
		listRadioButton.add(radioPreBill);
		// Log.e("getPaymentType...",listRadioButton.size()+"");

		for (int i = 0; i < listRadioButton.size(); i++) {

			String selection = listRadioButton.get(i).getText().toString();
		
			if (selection.equals(hCommercial_pest_DTO.getPaymentType())) {
				listRadioButton.get(i).setChecked(true);
				setView(i);
			}
		}
	}

	public void setView(int checkedId) {
		switch (checkedId) {
		case 0:
			linearlayout_DrivingLic.setVisibility(View.GONE);
			linearlayout_CheckNo.setVisibility(View.GONE);
			linearlayout_amount.setVisibility(View.VISIBLE);
			break;
		case 1:
			linearlayout_DrivingLic.setVisibility(View.VISIBLE);
			linearlayout_CheckNo.setVisibility(View.VISIBLE);
			linearlayout_amount.setVisibility(View.VISIBLE);

			break;
		case 2:
			linearlayout_DrivingLic.setVisibility(View.GONE);
			linearlayout_CheckNo.setVisibility(View.GONE);
			linearlayout_amount.setVisibility(View.VISIBLE);
			break;
		case 3:
			linearlayout_DrivingLic.setVisibility(View.GONE);
			linearlayout_CheckNo.setVisibility(View.GONE);
			linearlayout_amount.setVisibility(View.GONE);

			break;
		case 4:
			linearlayout_DrivingLic.setVisibility(View.GONE);
			linearlayout_CheckNo.setVisibility(View.GONE);
			linearlayout_amount.setVisibility(View.GONE);

			break;
		case 5:
			linearlayout_DrivingLic.setVisibility(View.GONE);
			linearlayout_CheckNo.setVisibility(View.GONE);
			linearlayout_amount.setVisibility(View.GONE);

			break;
		default:
			break;
		}
	}

	private void initialize() {
		// TODO Auto-generated method stub
		logDTO = new LogDTO();
		generalInfoDTO = (GeneralInfoDTO) this.getIntent().getExtras()
				.getSerializable("generalInfoDTO");
		userDTO = (UserDTO) this.getIntent().getExtras()
				.getSerializable("userDTO");
		serviceID_new = getIntent().getExtras().getString(
				ParameterUtil.ServiceID_new);

		strInsidepestControlServices = getIntent().getExtras().getString(
				"strInsideService");
		strOutsidepestControlServices = getIntent().getExtras().getString(
				"strOutsideService");

		/*
		 * strAreasInspectedExterior = getIntent().getExtras().getString(
		 * "strAreasInspectedExterior"); strAreasInspectedInterior =
		 * getIntent().getExtras().getString( "strAreasInspectedInterior");
		 */

		layout_pestControlserivces = (LinearLayout) findViewById(R.id.layout_pestControlserivces);
		layoutOtherPest = (LinearLayout) findViewById(R.id.layoutOtherPest);
		layoutAnts = (LinearLayout) findViewById(R.id.layoutAnts);
		LAYOUT_INSPECTED_TREATED = (LinearLayout) findViewById(R.id.LAYOUT_INSPECTED_TREATED);
		Layout_TechComment_desc = (LinearLayout) findViewById(R.id.Layout_TechComment_desc);
		Layout_TechComment = (LinearLayout) findViewById(R.id.Layout_TechComment);
		layout_recommendations = (LinearLayout) findViewById(R.id.layout_recommendations);
		layout_mosquito_services = (LinearLayout) findViewById(R.id.layout_mosquito_services);
		layoutRODENTSERVICES = (LinearLayout) findViewById(R.id.layoutRODENTSERVICES);
		mainLayout = (LinearLayout) findViewById(R.id.mainLayout);
		layoutChemicalHeader = (LinearLayout) findViewById(R.id.layoutChemicalHeader);
		LAYOUT_PEST_ZONE = (LinearLayout) findViewById(R.id.LAYOUT_PEST_ZONE);
		linearlayout_amount = (LinearLayout) findViewById(R.id.linearlayout_amount);
		linearlayout_CheckNo = (LinearLayout) findViewById(R.id.linearlayout_CheckNo);
		linearlayout_DrivingLic = (LinearLayout) findViewById(R.id.linearlayout_DrivingLic);
		LAYOUT_SERVICEFOR = (LinearLayout) findViewById(R.id.LAYOUT_SERVICEFOR);
		LAYOUT_AREASINSPECTED = (LinearLayout) findViewById(R.id.LAYOUT_AREASINSPECTED);
		layout_termiteserivces = (LinearLayout) findViewById(R.id.layout_termiteserivces);
		layoutInspected_Rodent_Stations = (LinearLayout) findViewById(R.id.layoutInspected_Rodent_Stations);
		layoutCleaned_Rodent_Stations = (LinearLayout) findViewById(R.id.layoutCleaned_Rodent_Stations);
		layoutInstalled_Rodent_Bait_Stations = (LinearLayout) findViewById(R.id.layoutInstalled_Rodent_Bait_Stations);
		layoutReplaced_Bait_Station = (LinearLayout) findViewById(R.id.layoutReplaced_Bait_Station);
		layoutReplaced_Bait = (LinearLayout) findViewById(R.id.layoutReplaced_Bait);
		layoutInspected_Rodent_Traps = (LinearLayout) findViewById(R.id.layoutInspected_Rodent_Traps);
		layoutCleaned_Rodent_Traps = (LinearLayout) findViewById(R.id.layoutCleaned_Rodent_Traps);
		layoutInstalled_Rodent_Traps = (LinearLayout) findViewById(R.id.layoutInstalled_Rodent_Traps);
		layoutReplaced_Rodent_Traps = (LinearLayout) findViewById(R.id.layoutReplaced_Rodent_Traps);
		layoutSealed_Entry_Points = (LinearLayout) findViewById(R.id.layoutSealed_Entry_Points);
		layoutSet_Live_Cages = (LinearLayout) findViewById(R.id.layoutSet_Live_Cages);
		layoutSet_Snap_Traps = (LinearLayout) findViewById(R.id.layoutSet_Snap_Traps);
		layoutRemoved_Rodent_Traps = (LinearLayout) findViewById(R.id.layoutRemoved_Rodent_Traps);
		layoutRemoved_Rodent_Bait_Stations = (LinearLayout) findViewById(R.id.layoutRemoved_Rodent_Bait_Stations);
		layoutRemoved_Live_Cages = (LinearLayout) findViewById(R.id.layoutRemoved_Live_Cages);
		layoutRemoved_Snap_Traps = (LinearLayout) findViewById(R.id.layoutRemoved_Snap_Traps);
		layoutInspected_Live_Cages = (LinearLayout) findViewById(R.id.layoutInspected_Live_Cages);
		layoutInspected_Snap_Traps = (LinearLayout) findViewById(R.id.layoutInspected_Snap_Traps);
		rowVisibleSignsMS = (LinearLayout) findViewById(R.id.rowVisibleSignsMS);

		rowTREATED_CC = (LinearLayout) findViewById(R.id.rowTREATED_CC);
		rowTREATED_SP = (LinearLayout) findViewById(R.id.rowTREATED_SP);
		rowTREATED_PS = (LinearLayout) findViewById(R.id.rowTREATED_PS);
		rowREPLACED_MS_STATION = (LinearLayout) findViewById(R.id.rowREPLACED_MS_STATION);
		rowREPLACED_MS = (LinearLayout) findViewById(R.id.rowREPLACED_MS);
		rowVISIBLE_IS = (LinearLayout) findViewById(R.id.rowVISIBLE_IS);
		rowVisibleTermite_PS = (LinearLayout) findViewById(R.id.rowVisibleTermite_PS);
		rowINSTALLED_MS = (LinearLayout) findViewById(R.id.rowINSTALLED_MS);
		rowVisibleSignsMS_STATION = (LinearLayout) findViewById(R.id.rowVisibleSignsMS_STATION);

		// textview
		// txtAcct = (TextView) findViewById(R.id.txtAccountNumber);
		txtOtherPest = (TextView) findViewById(R.id.txtOtherPest);
		txtAnts = (TextView) findViewById(R.id.txtAnts);
		txt_recommendations = (TextView) findViewById(R.id.txt_recommendations);
		textRecordingPoint = (TextView) findViewById(R.id.textRecordingPoint);
		txtINSPECTED_TREATED = (TextView) findViewById(R.id.txtINSPECTED_TREATED);
		txtTechComments = (TextView) findViewById(R.id.txtTechComment);
		txtServicetech = (TextView) findViewById(R.id.txtServiceTech);
		txtOrder = (TextView) findViewById(R.id.txtOrder);
		txtEmp = (TextView) findViewById(R.id.txtEmp);
		txtLicence = (TextView) findViewById(R.id.txtLIC);
		txtdate = (TextView) findViewById(R.id.txtDate);
		txtOrderNumber = (TextView) findViewById(R.id.txtOrderNumber);
		txt_recommendations = (TextView) findViewById(R.id.txt_recommendations);
		txtMosquitoServices = (TextView) findViewById(R.id.txtMosquitoServices);
		txtCheckedRunDurationTime = (TextView) findViewById(R.id.txtCheckedRunDurationTime);
		txtCheckedTimeSystemRuns = (TextView) findViewById(R.id.txtCheckedTimeSystemRuns);

		txtReplaced_Bait_Station = (TextView) findViewById(R.id.txtReplaced_Bait_Station);
		txtPestControlHeadOutside = (TextView) findViewById(R.id.txtPestControlHeadOutside);
		txtPestControlHeadInside = (TextView) findViewById(R.id.txtPestControlHeadInside);
		txtPestControlServiesInside = (TextView) findViewById(R.id.txtPestControlServiesInside);
		txtPestControlServiesOutside = (TextView) findViewById(R.id.txtPestControlServiesOutside);

		txtAcct = (TextView) findViewById(R.id.txtAccountNumber);
		txtServiceReportAddress = (TextView) findViewById(R.id.txtServiceReportAddress);
		txtTimeIn = (TextView) findViewById(R.id.txtTimeIn);
		txtTimeInHeader = (TextView) findViewById(R.id.txtTimeInHeader);
		txtEmail = (TextView) findViewById(R.id.txtEmail);
		txtSERVICE_FOR = (TextView) findViewById(R.id.txtSERVICE_FOR);

		txtAreasInspectedHeadInside = (TextView) findViewById(R.id.txtAreasInspectedHeadInside);
		txtAreasInspectedInside = (TextView) findViewById(R.id.txtAreasInspectedInside);
		txtAreasInspectedHeadOutside = (TextView) findViewById(R.id.txtAreasInspectedHeadOutside);
		txtAreasInspectedOutside = (TextView) findViewById(R.id.txtAreasInspectedOutside);

		txtInterior = (TextView) findViewById(R.id.txtInterior);
		txtOUTSIDE_PERIMETER = (TextView) findViewById(R.id.txtOUTSIDE_PERIMETER);
		txtDUMPSTER = (TextView) findViewById(R.id.txtDUMPSTER);
		txtDINING_ROOM = (TextView) findViewById(R.id.txtDINING_ROOM);
		txtCOMMON_AREAS = (TextView) findViewById(R.id.txtCOMMON_AREAS);
		txtKITCHEN = (TextView) findViewById(R.id.txtKITCHEN);
		txtDRY_STORAGE = (TextView) findViewById(R.id.txtDRY_STORAGE);
		txtDISH_WSHING = (TextView) findViewById(R.id.txtDISH_WSHING);
		txtROOF_TOPS = (TextView) findViewById(R.id.txtROOF_TOPS);
		txtWAIT_STATION = (TextView) findViewById(R.id.txtWAIT_STATION);
		txtDROP_CEILINGS = (TextView) findViewById(R.id.txtDROP_CEILINGS);
		txtPLANTERS = (TextView) findViewById(R.id.txtPLANTERS);
		txtWAREHOUSE_STORAGE = (TextView) findViewById(R.id.txtWAREHOUSE_STORAGE);

		txtTREATED_CC = (TextView) findViewById(R.id.txtTREATED_CC);
		txtTREATED_SP = (TextView) findViewById(R.id.txtTREATED_SP);
		txtTREATED_PS = (TextView) findViewById(R.id.txtTREATED_PS);
		txtREPLACED_MS_STATION = (TextView) findViewById(R.id.txtREPLACED_MS_STATION);
		txtREPLACED_MS = (TextView) findViewById(R.id.txtREPLACED_MS);
		txtVISIBLE_IS = (TextView) findViewById(R.id.txtVISIBLE_IS);
		txtVisibleTermite_PS = (TextView) findViewById(R.id.txtVisibleTermite_PS);
		txtInstalled_MS = (TextView) findViewById(R.id.txtInstalled_MS);
		txt_MS_STATION = (TextView) findViewById(R.id.txt_MS_STATION);
		txtRadioVisibleSigns = (TextView) findViewById(R.id.txtRadioVisibleSigns);

		txtSet_Live_Cages_howmany = (TextView) findViewById(R.id.txtSet_Live_Cages_howmany);
		txtSet_Live_Cages = (TextView) findViewById(R.id.txtSet_Live_Cages);
		txtSet_Snap_Traps_howmany = (TextView) findViewById(R.id.txtSet_Snap_Traps_howmany);
		txtSet_Snap_Traps = (TextView) findViewById(R.id.txtSet_Snap_Traps);
		txtRemoved_Rodent_Traps = (TextView) findViewById(R.id.txtRemoved_Rodent_Traps);
		txtRemoved_Rodent_Bait_Stations = (TextView) findViewById(R.id.txtRemoved_Rodent_Bait_Stations);
		txtRemoved_Live_Cages = (TextView) findViewById(R.id.txtRemoved_Live_Cages);
		txtRemoved_Snap_Traps = (TextView) findViewById(R.id.txtRemoved_Snap_Traps);
		txtInspected_Live_Cages = (TextView) findViewById(R.id.txtInspected_Live_Cages);
		txtInspected_Snap_Traps = (TextView) findViewById(R.id.txtInspected_Snap_Traps);
		txtSealed_Entry_Points = (TextView) findViewById(R.id.txtSealed_Entry_Points);
		txtReplaced_Rodent_Traps_stations = (TextView) findViewById(R.id.txtReplaced_Rodent_Traps_stations);
		txtReplaced_Rodent_Traps_howmany = (TextView) findViewById(R.id.txtReplaced_Rodent_Traps_howmany);
		txtReplaced_Rodent_Traps = (TextView) findViewById(R.id.txtReplaced_Rodent_Traps);
		txtInstalled_Rodent_Traps = (TextView) findViewById(R.id.txtInstalled_Rodent_Traps);
		txtInstalled_Rodent_Traps_howmany = (TextView) findViewById(R.id.txtInstalled_Rodent_Traps_howmany);
		txtCleaned_Rodent_Traps = (TextView) findViewById(R.id.txtCleaned_Rodent_Traps);
		txtInspected_Rodent_Traps = (TextView) findViewById(R.id.txtInspected_Rodent_Traps);
		txtReplaced_Bait_Stations_stations = (TextView) findViewById(R.id.txtReplaced_Bait_Stations_stations);
		txtReplaced_Bait_Stations_howmany = (TextView) findViewById(R.id.txtReplaced_Bait_Stations_howmany);
		txtReplaced_Bait_station_number = (TextView) findViewById(R.id.txtReplaced_Bait_station_number);
		txtReplaced_Bait = (TextView) findViewById(R.id.txtReplaced_Bait);
		txtInstalled_Rodent_Bait_Stations = (TextView) findViewById(R.id.txtInstalled_Rodent_Bait_Stations);
		txtInstalled_Rodent_Bait_howmany = (TextView) findViewById(R.id.txtInstalled_Rodent_Bait_howmany);
		txtCleaned_Rodent_Stations = (TextView) findViewById(R.id.txtCleaned_Rodent_Stations);
		txtHowManyInspected_Rodent_Stations = (TextView) findViewById(R.id.txtHowManyInspected_Rodent_Stations);
		txtInspected_Rodent_Stations = (TextView) findViewById(R.id.txtInspected_Rodent_Stations);

		// table row
		rowMosquitoServices = (LinearLayout) findViewById(R.id.rowMosquitoServices);
		rowCheckedRunDurationTime = (LinearLayout) findViewById(R.id.rowCheckedRunDurationTime);
		rowCheckedTimeSystemRuns = (LinearLayout) findViewById(R.id.rowCheckedTimeSystemRuns);

		rowHeadingPestControl = (TableRow) findViewById(R.id.rowHeadingPestControl);

		rowInterior = (LinearLayout) findViewById(R.id.rowInterior);
		rowOUTSIDE_PERIMETER = (LinearLayout) findViewById(R.id.rowOUTSIDE_PERIMETER);
		rowDUMPSTER = (LinearLayout) findViewById(R.id.rowDUMPSTER);
		rowDINING_ROOM = (LinearLayout) findViewById(R.id.rowDINING_ROOM);
		rowCOMMON_AREAS = (LinearLayout) findViewById(R.id.rowCOMMON_AREAS);
		rowKITCHEN = (LinearLayout) findViewById(R.id.rowKITCHEN);
		rowDRY_STORAGE = (LinearLayout) findViewById(R.id.rowDRY_STORAGE);
		rowDISH_WSHING = (LinearLayout) findViewById(R.id.rowDISH_WSHING);
		rowROOF_TOPS = (LinearLayout) findViewById(R.id.rowInterior);
		rowWAIT_STATION = (LinearLayout) findViewById(R.id.rowWAIT_STATION);
		rowDROP_CEILINGS = (LinearLayout) findViewById(R.id.rowDROP_CEILINGS);
		rowPLANTERS = (LinearLayout) findViewById(R.id.rowPLANTERS);
		rowWAREHOUSE_STORAGE = (LinearLayout) findViewById(R.id.rowWAREHOUSE_STORAGE);
		// radio button
		radioCash = (RadioButton) findViewById(R.id.radioCash);
		radioCheck = (RadioButton) findViewById(R.id.radioCheck);
		radioCreditCard = (RadioButton) findViewById(R.id.radioCreditCard);
		radioNoCharge = (RadioButton) findViewById(R.id.radioNoCharge);
		radioPreBill = (RadioButton) findViewById(R.id.radioPreBill);
		radioBillLater = (RadioButton) findViewById(R.id.radioBillLater);

		// edit text
		edtZip = (EditText) findViewById(R.id.edtZIP);
		edtAmount = (EditText) findViewById(R.id.edtAmount);
		edtCheckNo = (EditText) findViewById(R.id.edtCheckNo);
		edtDrivingLic = (EditText) findViewById(R.id.edtDrivingLic);
		edtCustomer = (EditText) findViewById(R.id.edtCustomer);
		edtAddress = (EditText) findViewById(R.id.edtADDRESS);
		edtBillingAddress = (EditText) findViewById(R.id.edtBILLING_ADRESS);
		edtHomePhone = (EditText) findViewById(R.id.edtHOME_PHONE);
		edtCellPhone = (EditText) findViewById(R.id.edtCELL_PHONE);
		edtAcct = (EditText) findViewById(R.id.edtACCT);
		edtChargerForCurrentServices = (EditText) findViewById(R.id.edtChargesCurentService);
		edtBalanceDue = (EditText) findViewById(R.id.edtBALANCE_DUE);
		edtTax = (EditText) findViewById(R.id.edtTax);
		edtEmp = (EditText) findViewById(R.id.edtEMP);
		edtTotalAmount = (EditText) findViewById(R.id.edtTotal);
		edtOpenOfficeNote = (EditText) findViewById(R.id.edtOpenOfficeNote);
		edtOpenOfficeNote.setFilters(CommonFunction.limitchars(500));

		btnBack = (Button) findViewById(R.id.btnBack);
		btnBack.setOnClickListener(new OnButtonClick());

		btnSendEmail = (Button) findViewById(R.id.btnSendEmail);
		btnSendEmail.setOnClickListener(new OnButtonClick());

		btnsubmitToPestpac = (Button) findViewById(R.id.btnsubmitToPestpac);
		btnsubmitToPestpac.setOnClickListener(new OnButtonClick());

		btnSignature = (Button) findViewById(R.id.btnServiceSpecialistSignature);
		btnSignature.setOnClickListener(new OnButtonClick());
		btnIagreeSignature = (Button) findViewById(R.id.btnIAgreetoPricing);
		btnIagreeSignature.setOnClickListener(new OnButtonClick());
		btnRecordAudio = (Button) findViewById(R.id.btnRecord);
		btnRecordAudio.setOnClickListener(new OnButtonClick());
		imageListView = (HorizontalListView) findViewById(R.id.listImg);
		imageListView.setOnItemLongClickListener(new OnGalleryClick());
		imageListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Bitmap bmp = listImg.get(position).getImageBitMap();
				if (bmp != null) {
					bitmap = bmp;
					Intent intent = new Intent(HC_TodayServiceInvoice.this,
							FullImageCommercial.class);
					intent.putExtra("from", "report");
					startActivity(intent);
				}
			}

		});

		btnAfterImage = (Button) findViewById(R.id.btnCamera);
		btnAfterImage.setOnClickListener(new OnButtonClick());
		playBtn = (ImageView) findViewById(R.id.play);
		stopPlayBtn = (ImageView) findViewById(R.id.stopPlay);
		playBtn.setOnClickListener(new OnButtonClick());
		stopPlayBtn.setOnClickListener(new OnButtonClick());

		imageViewTechnicianSign = (ImageView) findViewById(R.id.imageViewTechnicianSign);
		imageViewCustomerSign = (ImageView) findViewById(R.id.imageViewCustomerSign);

		// generalInfoDTO = new GeneralInfoDTO();
		hCommercial_pest_DTO = new HCommercial_pest_DTO();
		houstonFlowFunctions = new HoustonFlowFunctions(getApplicationContext());

	}
	class OnGalleryClick implements OnItemLongClickListener {

		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
				int position, long arg3) {

			// Toast.makeText(getApplicationContext(),IWitnessActivity.position+
			// "  "+position, Toast.LENGTH_LONG).show();

			if (generalInfoDTO.getServiceStatus().equals("InComplete")) {
				if (listImg.size() > 0) {
					listImg.remove(position);
					if (tempImagList.size() > 0) {
						tempImagList.remove(position);
					}
					if (tempStoreImageName.size() > 0) {
						tempStoreImageName.remove(position);
					}
					RefreshImage();
					// alertboxClearGalleary("Confirmation",
					// "Are you want to remove selected image ?");
				}
			}

			return false;
		}

	}

	class OnButtonClick implements OnClickListener {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			// TODO Auto-generated method stub

			switch (v.getId()) {
			case R.id.play:
				// TODO Auto-generated method stub
				// stopPlayBtn.setVisibility(v.VISIBLE);
				if (!CommonFunction.checkString(straudioFile, "").equals("")) {
					play(v);
				} else {
					// .
					// text.setText("Recording Point: Audio File Not Available");
				}
				break;
			case R.id.stopPlay:
				stopPlay();
				break;
			case R.id.btnBack:
				finish();
				break;
			case R.id.btnsubmitToPestpac:
				// btnsubmitToPestpac.setVisibility(View.GONE);
				// btnSendEmail.setVisibility(View.VISIBLE);
				// submitService();
				isValidate = true;
				MandatoryList.clear();
				validate();
				if (isValidate) {

					btnsubmitToPestpac.setEnabled(false);
					showSendEmailDailog();
				} else {
					charSequenceItems = MandatoryList
							.toArray(new CharSequence[MandatoryList.size()]);
					createMessageList(charSequenceItems, "");
				}
				break;

			case R.id.btnSendEmail:
				break;

			case R.id.btnServiceSpecialistSignature:
				ShowSignatureDailog("Service Specialist Signature", 1);
				break;

			case R.id.btnIAgreetoPricing:
				ShowSignatureDailog("I Agree to Pricing and Terms & Condition",
						2);
				break;

			case R.id.btnRecord:
				stopPlay();
				Intent intent = new Intent(getApplicationContext(),
						AudioRecordingActivity.class);
				intent.putExtra(ParameterUtil.ServiceID_new,
						String.valueOf(serviceID_new));
				startActivityForResult(intent, RECORD_AUDIO_REQUEST_CODE);
				break;
			case R.id.btnCamera:
				if (tempStoreImageName.size() != 3) {
					if (CommonFunction.isSdPresent()) {
						takePicture();
					} else {
						Toast.makeText(getApplicationContext(),
								"SdCard Not Present", Toast.LENGTH_SHORT)
								.show();
					}
				} else {
					Toast.makeText(getApplicationContext(),
							"Max number of upload image exceeded.",
							Toast.LENGTH_SHORT).show();
				}
				break;
			default:
				break;
			}
		}
	}

	private final Dialog createMessageList(final CharSequence[] messageList, /*
																			 * final
																			 * EditText
																			 * object
																			 */
			String string) {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				HC_TodayServiceInvoice.this);
		builder.setTitle(getResources()
				.getString(R.string.REQUIRED_INFORMATION));
		builder.setItems(charSequenceItems,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// LoggerUtil.e("","E' stato premuto il pulsante: "+messageList[whichButton]);
					}
				});
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {

			}
		});
		return builder.show();
	}

	EmailAdapter myAdapter;

	private void showSendEmailDailog() {
		Button btnSendMail, btnadd, btncancel;
		TextView txtFrom, txtmessage;
		final EditText edtEmailAddress;
		final ListView lstemailIdList;
		sendEmailDialog = new Dialog(HC_TodayServiceInvoice.this);
		sendEmailDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		sendEmailDialog.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
		sendEmailDialog.setContentView(R.layout.send_email_dialog);
		// InitializeSignature(sendEmailDialog,which);
		btnSendMail = (Button) sendEmailDialog.findViewById(R.id.btnSendEmail);
		btnadd = (Button) sendEmailDialog.findViewById(R.id.btnAdd);
		btncancel = (Button) sendEmailDialog.findViewById(R.id.btnCancel);
		txtFrom = (TextView) sendEmailDialog.findViewById(R.id.txtFrom);
		txtmessage = (TextView) sendEmailDialog.findViewById(R.id.txtMessage);
		edtEmailAddress = (EditText) sendEmailDialog
				.findViewById(R.id.edtemailAddress);
		edtEmailAddress.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				String str = s.toString();
				if (str.length() > 0 && str.startsWith(" ")) {
					// Log.v("","Cannot begin with space");
					edtEmailAddress.setText("");
				} else {
					// Log.v("","Doesn't contain space, good to go!");
				}
			}

		});

		lstemailIdList = (ListView) sendEmailDialog
				.findViewById(R.id.lstEmailList);
		txtFrom.setText(userDTO.getEmailid());

		houstonFlowFunctions.deleteSelectedTableData(
				DatabaseHelper.TABLE_AGREEMENTMAILID, ParameterUtil.Member_Id,
				serviceID_new);
		String[] emailIDS = generalInfoDTO.getNewEmail().split(",");
		for (int i = 0; i < emailIDS.length; i++) {
			if (CommonFunction.checkString(emailIDS[i], "").equals("")) {
				Log.e(" in if", "" + " in if");
				addEmailAddress(userDTO.getEmailid(), true);
				addEmailAddress("mobile2@goanteater.com", true);
			} else {
				Log.e(" in else", "" + " in else");
				addEmailAddress(userDTO.getEmailid(), true);
				addEmailAddress(emailIDS[i], true);
				addEmailAddress("mobile2@goanteater.com", true);
			}
		}
		if (emailIDS.length == 0) {
			addEmailAddress("noemail@goanteater.com", true);
		}
		// JSONArray emailJSONArray=
		// serviceFlowFunctions.getAllSelectedServiceEmaiLID(serviceID_new);
		ArrayList<AgreementMailIdDTO> emailAddressList = new Gson()
				.fromJson(
						houstonFlowFunctions.getAllSelectedServiceEmaiLID(
								serviceID_new).toString(),
						new TypeToken<List<AgreementMailIdDTO>>() {
						}.getType());
		myAdapter = new EmailAdapter(HC_TodayServiceInvoice.this,
				R.layout.row_team_layout, emailAddressList, lstemailIdList);
		lstemailIdList.setAdapter(myAdapter);
		lstemailIdList.setItemsCanFocus(false);

		btnSendMail.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				SharedPreference
						.setSharedPrefer(getApplicationContext(),
								SharedPreference.COMPLETED_BY,
								ParameterUtil.Commercial);
				btnsubmitToPestpac.setEnabled(true);
				ArrayList<AgreementMailIdDTO> selectedTeams = new ArrayList<AgreementMailIdDTO>();
				final SparseBooleanArray checkedItems = lstemailIdList
						.getCheckedItemPositions();
				// Log.e("checkedItemsCount ",""+checkedItems.size());

				int checkedItemsCount = checkedItems.size();

				for (int i = 0; i < checkedItemsCount; ++i) {
					// Item position in adapter
					int position = checkedItems.keyAt(i);
					// Add team if item is checked == TRUE!
					if (checkedItems.valueAt(i)) {
						// Log.e("checkedItemsCount ",""+checkedItems.size()+" "+position);

						selectedTeams.add(myAdapter.getItem(position));

					}
				}
				// Log.e("selectedTeams ",""+selectedTeams.size());
				if (selectedTeams.size() < 1) {
					/*--------Commented By Ankit on 16-April-2015 based on client comment------*/
					// CommonFunction.AboutBox("Need to select one or more emailId.",TodayServiceReportActivity.this);
					// houstonFlowFunctions.deleteSelectedTableData(DatabaseHelper.TABLE_AGREEMENTMAILID,ParameterUtil.Member_Id,serviceID_new);
					// submitService();
					// goToHome("Yes");
					Toast.makeText(getBaseContext(),
							"Need to select one or more Items.",
							Toast.LENGTH_SHORT).show();
				} else {
					// Just logging the output.
					houstonFlowFunctions.deleteSelectedTableData(
							DatabaseHelper.TABLE_AGREEMENTMAILID,
							ParameterUtil.Member_Id, serviceID_new);

					for (AgreementMailIdDTO t : selectedTeams) {
						addEmailAddress(t.getEmailId(), true);

					}

					submitService();
					goToHome("Yes");
					sendEmailDialog.dismiss();
				}
				// TODO Auto-generated method stub

				// goToHome("Yes");
			}

		});

		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				btnsubmitToPestpac.setEnabled(true);
				// serviceFlowFunctions.deleteSelectedTableData(DatabaseHelper.TABLE_AGREEMENTMAILID,ParameterUtil.Member_Id,serviceID_new);
				sendEmailDialog.dismiss();
			}
		});

		btnadd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String emailAddress = edtEmailAddress.getText().toString()
						.trim();
				if (eMailValidation(emailAddress)) {
					addEmailAddress(emailAddress, true);
					setEmailAddressList(lstemailIdList);
					edtEmailAddress.setText("");
				} else {
					CommonFunction.AboutBox("Invalid Email ID",
							HC_TodayServiceInvoice.this);
				}

				Context context = HC_TodayServiceInvoice.this;
				InputMethodManager imm = (InputMethodManager) getSystemService(context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(edtEmailAddress.getWindowToken(), 0);
			}
		});
		sendEmailDialog.show();
		sendEmailDialog.setCancelable(false);
	}

	public void submitService() {
		hCommercial_pest_DTO.setTimeout(CommonFunction.getCurrentTime());
		hCommercial_pest_DTO.setTimeOutDateTime(CommonFunction.getCurrentDateTime());
		hCommercial_pest_DTO.setIsCompleted("true");
		hCommercial_pest_DTO.setTimeIn(generalInfoDTO.getTimeIn());
		hCommercial_pest_DTO.setTimeInDateTime(generalInfoDTO.getTimeInDateTime());
		strmultipleImageName = "";
		for (int i = 0; i < tempStoreImageName.size(); i++) {
			strmultipleImageName += tempStoreImageName.get(i).toString().trim()
					+ ",";
		}
		if (strmultipleImageName.length() > 0) {
			strmultipleImageName = strmultipleImageName.substring(0,
					strmultipleImageName.length() - 1);
		}
		LoggerUtil.e("Before ImageName", strmultipleImageName);
		LoggerUtil.e("Audio ImamgeName", straudioFile + "");
		generalInfoDTO.setAudioFile(straudioFile);
		generalInfoDTO.setAfterImage(strmultipleImageName);
		generalInfoDTO.setIsCompleted("true");
		generalInfoDTO.setIsElectronicSignatureAvailable("false");
		generalInfoDTO.setOpenWONotificationFlag("false");
		if (hCommercial_pest_DTO.getPaymentType().equals("Cash")
				|| hCommercial_pest_DTO.getPaymentType().equals("Check")) {
			generalInfoDTO.setPayment_Collection_Status("Open");
		} else {
			generalInfoDTO.setPayment_Collection_Status("Closed");
		}
		generalInfoDTO.setNote_Status("Open");
		generalInfoDTO.setServiceNameType("HoustonCommercial");
		generalInfoDTO.setServiceStatus("Complete");
		generalInfoDTO.setOffice_Note_ByRep(edtOpenOfficeNote.getText()
				.toString());
		generalInfoDTO.setLocationBalance(generalInfoDTO.getLocationBalance());

		int i = houstonFlowFunctions.update(generalInfoDTO);
		// Log.e("Update", i+"");
		// commercialDTO.setLicenseNo(txtLicence.getText().toString());
		hCommercial_pest_DTO.setSignature(strCustomerSignature);
		hCommercial_pest_DTO.setServiceSignature(strTechnicianSignature);
		int j = houstonFlowFunctions.updateCommercial_Pest(hCommercial_pest_DTO);

		// Log.e("Update", j+"");
		// startService(new Intent(getApplicationContext(),
		// SynchServiceToServerServices.class));
		Toast.makeText(getApplicationContext(), "Record Saved",
				Toast.LENGTH_LONG).show();
	}

	public void goToHome(String isSync) {
		Intent intent = new Intent(getApplicationContext(),
				ServiceListAcitivity.class);
		intent.putExtra("userDTO", ServiceListAcitivity.userDTO);

		Bundle extras = new Bundle();
		extras.putString(ParameterUtil.isSync, isSync);

		// 4. add bundle to intent
		intent.putExtras(extras);
		// intent.putExtra(ParameterUtil.isSync,isSync);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
	}

	public boolean eMailValidation(String emailstring) {
		/*
		 * Pattern emailPattern = Pattern.compile(".+@.+\\.[a-z]+"); Matcher
		 * emailMatcher = emailPattern.matcher(emailstring); return
		 * emailMatcher.matches();
		 */
		// Pattern emailPattern = Pattern.compile(".+@.+\\.[a-z]+");
		String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern emailPattern = Pattern.compile(EMAIL_PATTERN);
		Matcher emailMatcher = emailPattern.matcher(emailstring);
		return emailMatcher.matches();

	}

	public EmailAdapter setEmailAddressList(final ListView lstEmailAdress) {
		JSONArray emailJSONArray = houstonFlowFunctions
				.getAllSelectedServiceEmaiLID(serviceID_new);
		ArrayList<AgreementMailIdDTO> emailAddressList = new Gson()
				.fromJson(
						houstonFlowFunctions.getAllSelectedServiceEmaiLID(
								serviceID_new).toString(),
						new TypeToken<List<AgreementMailIdDTO>>() {
						}.getType());
		myAdapter = new EmailAdapter(this, R.layout.row_team_layout,
				emailAddressList, lstEmailAdress);
		lstEmailAdress.setAdapter(myAdapter);
		lstEmailAdress.setItemsCanFocus(false);
		return myAdapter;
	}

	public void addEmailAddress(String emailAddress, boolean isSelected) {
		// Log.e("emailAddress..",isSelected+"emailAddress..."+emailAddress);
		try {
			if (!houstonFlowFunctions.isEmailAdressAlreadyAdded(serviceID_new,
					emailAddress)) {
				// Log.e("emailAddress..",isSelected+"emailAddress..if.."+emailAddress);
				AgreementMailIdDTO agreementMailIdDTO = new AgreementMailIdDTO();
				agreementMailIdDTO.setAgreementId("0");
				// agreementMailId.setCreate_By(String.valueOf(GeneralInformationActivity.userDTO.getEmployeeId()));
				agreementMailIdDTO.setCreate_By(String.valueOf(userDTO
						.getPestPackId()));
				agreementMailIdDTO.setCreate_date(CommonFunction
						.getCurrentDateTime());
				agreementMailIdDTO.setEmailId(emailAddress);
				agreementMailIdDTO.setMailType("Commercial");
				agreementMailIdDTO.setId(houstonFlowFunctions.getMaxId() + 1);
				agreementMailIdDTO.setMember_Id(serviceID_new);
				agreementMailIdDTO.setSubject("");
				agreementMailIdDTO.setSelected(isSelected);
				try {
					Gson gson = new Gson();
					JSONObject agreementMailJSON = new JSONObject(
							gson.toJson(agreementMailIdDTO));
					// Log.e("agreementMailJSON", agreementMailJSON.toString());
					long i = houstonFlowFunctions
							.agreementMail(agreementMailJSON);
					// Log.e("addEmailAddress",i+"");
				} catch (Exception e) {
					LoggerUtil.e("Exception in AgreementMailId", e.toString());

				}
			} else {
				Log.e("inside alert addEmailAddress", i + "");
				// Toast.makeText(getApplicationContext(),
				// "Email Address Already Added",Toast.LENGTH_LONG).show();
				CommonFunction.AboutBox("Email Address Already Added",
						HC_TodayServiceInvoice.this);
			}
		} catch (Exception e) {
			Toast.makeText(getApplicationContext(), "Invalid Email Address",
					Toast.LENGTH_LONG).show();
			// Log.e("EMAIL EXCEPTION",e+"");
		}
	}

	public void validate() {
		// TODO Auto-generated method stub
		if (tempStoreImageName.size() == 0) {
			MandatoryList.add("Add atleast one After Image");
			isValidate = false;
		}
	}

	public void play(View view) {
		try {
			if (myPlayer != null) {
				myPlayer.stop();
				myPlayer.release();
				myPlayer = null;
			}
			myPlayer = new MediaPlayer();
			// Log.e("straudioFile", straudioFile);
			myPlayer.setDataSource(CommonFunction
					.getAudioFilePath(straudioFile).toString());
			myPlayer.prepare();
			myPlayer.start();
			playBtn.setEnabled(false);
			stopPlayBtn.setEnabled(true);
			// text.setText("Recording Point: Playing");
			stopPlayBtn.setVisibility(View.VISIBLE);
			playBtn.setVisibility(View.GONE);
			Toast.makeText(getApplicationContext(),
					"Start play the recording...", Toast.LENGTH_SHORT).show();
		} catch (Exception e) {
			// text.setText("Recording Point: Audio File Not Available");
			Log.e("Exception", e + "..");
		}

		myPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
			public void onCompletion(MediaPlayer mp) {
				// Perform your steps here, eg.
				stopPlayBtn.setVisibility(View.GONE);
				playBtn.setVisibility(View.VISIBLE);
				playBtn.setEnabled(true);
				// stopPlayBtn.setEnabled(true);
				// (Make sure you make v final before)
			}
		});
	}

	public void stopPlay() {
		try

		{
			if (myPlayer != null) {
				myPlayer.stop();
				myPlayer.release();
				myPlayer = null;
				playBtn.setEnabled(true);

				stopPlayBtn.setEnabled(false);
				stopPlayBtn.setVisibility(View.GONE);
				playBtn.setVisibility(View.VISIBLE);
				// text.setText("Recording Point: Stop playing");

				// Toast.makeText(getApplicationContext(),
				// "Stop playing the recording...",Toast.LENGTH_SHORT).show();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == RECORD_AUDIO_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				straudioFile = data.getExtras().getString(
						ParameterUtil.AudioFile);
				textRecordingPoint.setText("Audio available");

				Toast.makeText(getApplicationContext(),
						"Audio Recorded " + straudioFile, Toast.LENGTH_LONG)
						.show();
				stopPlay();
			}
		}

		
		if (requestCode == REQUEST_Camera_ACTIVITY_CODE) {
			if (resultCode == RESULT_OK) {
				imageCount++;
				Toast.makeText(getApplicationContext(), "Picture is  taken",
						Toast.LENGTH_SHORT);
				tempStoreImageName.add(ImageName);
				RefreshSaveImage();
				//getfile(ImageName);
				// Log.e("File Name on done camera", ImageName + "");
			} else if (resultCode == RESULT_CANCELED) {

				RefreshImage();
			}

		}

		
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	private void RefreshSaveImage() {
		try {
			listImg.clear();
			Bitmap mutableBitmap = null;
			for (int i = 0; i < tempStoreImageName.size(); i++) {
				mutableBitmap = null;
				ImgDto dto = new ImgDto();
				try {
					LoggerUtil.e("Image Name in refresh",
							tempStoreImageName.get(i));
					mutableBitmap = decodeSmallUri(Uri.fromFile(CommonFunction
							.getFileLocation(HC_TodayServiceInvoice.this,
									tempStoreImageName.get(i))));
					if(i == tempStoreImageName.size()-1)
					{
					mutableBitmap = mutableBitmap.copy(Bitmap.Config.ARGB_8888,
							true);
					mutableBitmap =  rotateImage(mutableBitmap, 90);
					Canvas cs = new Canvas(mutableBitmap);
					Paint tPaint = new Paint();
					tPaint.setTextSize(35);
					//#B43104
					tPaint.setColor(getResources().getColor(R.color.red_orange));
					tPaint.setStyle(Style.FILL);
					float height = tPaint.measureText("xX");
					cs.drawText(CommonFunction.getCurrentDateTimeForImage(),height+300f,
							height+1200f, tPaint);		
				    //Bitmap bitmap = scaleBitmap(mutableBitmap, 100, 100);
					}									
					dto.setImageBitMap(mutableBitmap);

					//get file
					File imagefile = CommonFunction.getFileLocation(
							HC_TodayServiceInvoice.this, tempStoreImageName.get(i));
		            imagefile.delete();
					CommonFunction.saveBitmap(mutableBitmap,
							HC_TodayServiceInvoice.this, tempStoreImageName.get(i));
                    
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if (!tempImagList.contains(tempStoreImageName.get(i))) {

					if (mutableBitmap != null) {

						// CompressImage(bmp);
					}
				}

				tempImagList.add(tempStoreImageName.get(i));
				listImg.add(dto);
			}
			// LoggerUtil.e("List Size", listImg.size() + "");

			if (listImg.size() == 0) {
				ImgDto dto = new ImgDto();
				dto.setImagedrawable(R.drawable.noimage);
				listImg.add(dto);
			} else {

			}
			adapter = new GalleryImageAdapter(getApplicationContext(), listImg);
			imageListView.setAdapter(adapter);
			// adapter.notifyDataSetChanged(); // TODO Auto-generated method
			// stub
		} catch (Exception e) {
			LoggerUtil.e("Exception in Refresh Image", e + "");
		}
	}
	public static Bitmap rotateImage(Bitmap source, float angle) {
	    Bitmap retVal;

	    Matrix matrix = new Matrix();
	    matrix.postRotate(angle);
	    retVal = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);

	    return retVal;
	}

	private void RefreshImage() {
		try {
			listImg.clear();
			Bitmap bmp = null;
			for (int i = 0; i < tempStoreImageName.size(); i++) {
				bmp = null;
				ImgDto dto = new ImgDto();
				try {
					// Log.e("Image Name in refresh",tempStoreImageName.get(i));
					
					bmp = decodeSmallUri(Uri.fromFile(CommonFunction.getFileLocation(getApplicationContext(),
									tempStoreImageName.get(i))));

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//Bitmap bitmap = scaleBitmap(bmp, 100, 100);
				dto.setImageBitMap(bmp);
				if (!tempImagList.contains(tempStoreImageName.get(i))) {

					if (bmp != null) {

						// CompressImage(bmp);
					}
				}// dto.setImageUri(Uri.fromFile(CommanFunction.getFileLocation(IWitnessActivity.this,
					// tempStoreImageName.get(i))));

				tempImagList.add(tempStoreImageName.get(i));
				listImg.add(dto);

			}

			// Log.e("List Size", listImg.size() + "");

			if (listImg.size() == 0) {
				ImgDto dto = new ImgDto();
				dto.setImagedrawable(R.drawable.noimage);
				listImg.add(dto);
			} else {

			}
			adapter = new GalleryImageAdapter(getApplicationContext(), listImg);
			imageListView.setAdapter(adapter);
			// adapter.notifyDataSetChanged(); // TODO Auto-generated method
			// stub
		} catch (Exception e) {
			Log.e("Exception in Refresh Image", e + "");
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.today_service_invoice, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		if (id == R.id.action_home) {
			goToHome("No");
			return true;
		}
		/*
		 * if (id == R.id.action_back) { finish(); return true; }
		 */
		if (id == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}

}