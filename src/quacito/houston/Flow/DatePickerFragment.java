package quacito.houston.Flow;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
//import android.util.Log;
import android.widget.DatePicker;
import android.widget.EditText;


public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener
{

	EditText edtFromDate;
	EditText edtToDate;
	Date fromDate;
	Date toDate;
	int forWhich;
	public DatePickerFragment(EditText edtFromDate,EditText edtToDate,int forWhich )
	{
		super();
		this.edtFromDate = edtFromDate;
		this.edtToDate=edtToDate;
		this.forWhich=forWhich;
	}

	public DatePickerFragment(EditText edtFromDate) 
	{
		super();
		this.edtFromDate = edtFromDate;
		this.forWhich=3;
		//System.out.println("Date1 is before Date2...1");
	}




	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		// Use the current date as the default date in the picker
		
		final Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);
		DatePickerDialog datePickerDialog=new DatePickerDialog(getActivity(), this, year, month, day);
		datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis()-1000);
		return datePickerDialog ;
	}

	public void onDateSet(DatePicker view, int year, int month, int day) 
	{
		//MM/dd/YYYY
		SimpleDateFormat dateFormat=new SimpleDateFormat("MM/dd/yyyy");
		Date date=new Date();
		date.setDate(day);
		date.setYear(year-1900);
		date.setMonth(month);
		String formatedDate=dateFormat.format(date);
		//System.out.println("Date1 is before Date2...3");
		//Log.e("Date", formatedDate);
		switch (forWhich) 
		{
		case 0:
			edtFromDate.setText(formatedDate);
			edtToDate.setText(formatedDate);
			break;
		case 1:
			edtFromDate.setText(formatedDate);
			break;
		case 2:
			edtToDate.setText(formatedDate);
			break;
		case 3:
			//view.setMinDate(System.currentTimeMillis());
			
			edtFromDate.setText(formatedDate);
			
			break;
		default:
			break;
		}
	
		
	}
	

}
