package quacito.houston.Flow;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import org.json.JSONObject;
import quacito.houston.CommonUtilities.CommonFunction;
import quacito.houston.CommonUtilities.DecimalDigitsInputFilter;
import quacito.houston.CommonUtilities.LoggerUtil;
import quacito.houston.CommonUtilities.ParameterUtil;
import quacito.houston.DBhelper.HoustonFlowFunctions;
import quacito.houston.Flow.HR_PestControlServiceReport.OnButtonClick;
import quacito.houston.Flow.HR_PestControlServiceReport.OnCheckedboxChangeListener;
import quacito.houston.Flow.HR_PestControlServiceReport.OnRadioChange;
import quacito.houston.Flow.servicehouston.R;
import quacito.houston.adapter.GalleryImageAdapter;
import quacito.houston.horizontallistview.HorizontalListView;
import quacito.houston.model.GeneralInfoDTO;
import quacito.houston.model.ImgDto;
import quacito.houston.model.OResidential_pest_DTO;
import quacito.houston.model.UserDTO;
import com.google.gson.Gson;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;

public class OR_PestControlServiceReport extends Activity {
	
	TextView textViewApppVer, txtAccountNumber, txtOrderNumber;
	Button btnCHECK_FRONT_IMAGE, btnCHECK_BACK_IMAGE,
			btnsaveandcontinueINVOICE, btnCancel;
	CheckBox or_checkboxSignaturePestcontrol, or_checkboxPlusTermites,
			or_checkboxPlusMosquitomistingSystem, or_checkboxPlusFireAnts,
			or_checkboxPluSRodents, or_checkboxPlusFleas,
			or_checkboxTermiteTreatment, or_checkboxRodentExclusion,
			or_checkboxBedBugTreatment, or_checkboxOutSideService,
			or_checkboxFenceLines, or_checkboxBackYardByZone,
			or_checkboxFrontyardByZOne,
			or_checkboxExteriorParametresofhouseByZone, or_checkboxRofGutter,
			or_checkboxGarage, or_checkboxAtticByZone,
			or_checkboxInteriorHouseByZone, or_checkbox_Basement_PestActivityZone,
			or_checkboxINTERIOR, or_checkboxTreatedAcct,
			or_checkboxTreatedKitchen, or_checkboxTreatedCracks,
			or_checkboxBasements, or_checkboxTreatedliving,
			or_checkboxTreatedZoneMonitors, or_checkboxTreatedBedrooms,
			or_checkboxEXTERIOR, or_checkboxTreatedFront,
			or_checkboxTreatedback, or_checkboxTreatedLaundryRooms,
			or_checkboxTreatedLivingRooms, or_checkboxtreatedyards,
			or_checkboxTreatedfirenets, or_checkboxTreatedDoorways,
			or_checkboxTreatedCrawlSpace, or_checkboxFenceInspectedTermite,
			or_checkboxNumberBaitingDevicesInspectedTermite,
			or_checkboxBaitingDevicesTermite,
			or_checkboxTermiteBaitingDevicereplacedTermite,
			or_checkboxExteriorPerimeterofhouseTermite,
			or_checkboxATTICTermite, or_checkboxINTERIOROFHOUSETermite,
			or_checkbox_Basement_Termite,
			or_checkboxAdditionalDeviceAddedTermite,
			or_checkboxNumberDeviceRemovedTermite,
			or_checkboxBUILDERS_CONTRUCTION_GAP, or_checkboxSOFFIT_RETURNS,
			or_checkboxBREEZYWAY_FROM_GARAGE, or_checkboxROOF_VENTS,
			or_checkboxGARAGE_DOOR, or_checkboxSOFFIT_VENTS,
			or_checkboxAC_DRYER_VENTS,
			or_checkboxNO_LIVEANIMAL_TRAPS_INSTALLES, or_checkboxSIDEING_HOUSE,
			or_checkboxNumber_SNAPTRAPS, or_checkboxCRAWL_SPACE_VENTS,
			or_checkboxCRAWL_SPACE_DOOR, or_checkboxTopped_TANK,
			or_checkboxLEACKY_TANK, or_checkboxTUBING_LEACKY,
			or_checkboxINSPECTED_NOZZLES, or_checkboxRAN_SYATEMS,
			or_checkboxNOZZLES_LEAKING, or_checkboxCHECKED_RUN_DURACTION_TIME,
			or_checkboxCHECKED_TIME_SYSTEM_RUNS, or_checkboxBranchHouse,
			or_checkboxFirewood, or_checkboxDebrisCrawl,
			or_checkboxExcessiveplant, or_checkboxSoilAbove,
			or_checkboxWoodSoil, or_checkboxDebrisRoof,
			or_checkboxStandingwater, or_checkboxMoistureProblem,
			or_checkboxOpeningspumbing, or_checkboxExcessiveGaps,
			or_checkboxLeackPumbing, or_checkboxgarbageCans,
			or_checkboxMoisrtureDamage, or_checkboxGroceryBags,
			or_checkboxPetFood, or_checkboxRecycledItems,
			or_checkboxExcessiveStorage, or_checkboxSCHOOL_HOLIDAY,
			or_checkboxRE_ENTRY_SIGN, or_checkboxWARNING_SIGN,
			or_checkboxDOORS_LOCKED_AREA_SECURED,
			or_checkboxNO_CHILDREN_STUDENT_PRESENT, or_checkboxRoaches,
			or_checkboxSpiders, or_checkboxScorpions, or_checkboxSilverfish,
			or_checkboxPANTRY_PESTS, or_checkboxPHARAOH_ANTS,
			or_checkboxFIRE_ANTS, or_checkboxCARPENTER_ANTS,
			or_checkboxMILLIPEDES, or_checkboxEARWIGS, or_checkboxPILL_BUGS,
			or_checkboxCRICKETS, or_checkboxWASPS, or_checkboxSUB_TERMITIES,
			or_checkboxDRYWOOD_TERMITIES, or_checkboxROOF_RATS,
			or_checkboxMICE, or_checkboxRACCOONS, or_checkboxOPOSSUMS,
			or_checkboxSKUNKS, or_checkboxSquirrels,
			or_checkboxANTS_Inspected_treated_for, or_checkboxARGENTINE_ANTS,
			or_checkboxOTHER_PESTS_INSPECTED_TREATED_FOR,
			or_checkboxEXTERIOR_PERIMRTER_OF_HOUSE,
			or_checkboxFRONTYARDAreasInspected,
			or_checkboxBackYardAreasInspected,
			or_checkboxExteriorPerimeterofGarage, or_checkboxOutBuildings,
			or_checkboxFenceLinesAreasInspected,
			or_checkboxInteriorHouseAreasInspected,
			or_checkboxExteriorHouseAreasInspected,
			or_checkboxAtticAreasinspected, or_checkboxBASEMENTAreasinspected,
			or_checkboxCRAWL_SPACEAreasinspected;
	EditText edtFenceLinesByZone, edtBackyardByZone, edtFrontYardByZone,
			edtExteriorParaByZone, edtRoofGutterByZone, edtGarageByZone,
			edtAtticPestByZone, edtInteriorHouseByZone,
			edt_Basement_PestActivityByZone,
			edtNUMBER_OF_BAITING_DEVICES_INSPECTED,
			edt_Basement_TermiteServices, edtAdditionalDeviceAdded,
			edtNumberDeviceRemoved, edtNO_LIVEANIMAL_TRAPS_INSTALLES,
			edtNumber_SNAPTRAPS, edtMosquitoService1, edtMosquitoService2,
			edttxtInspectedandTreatedFor2, edttxtInspectedandTreatedFor1,
			edtTechComment, edtServiceTech, edtEMP, edtCustomer, edtDate,
			edtIntValue, edtProductvalue, edtTaxvalue, edtTotal, edtAmount,
			edtCheckNo, edtDRIVING_LICENCE, edtEXPIRATION_DATE;

	TableRow tramount, trcheckno, trDrivingLicence, trExpirationDate,
			trExpirationDateValidation;

	LinearLayout LinearlayoutCHECK_IMAGE, LinearlayoutCHECK_FRONT_IMAGE,
			LinearlayoutCHECK_BACK_IMAGE;

	RadioGroup radioGroupInvoice, radioGroupFenceInspected,
			radioGroupBaitingDevices,
			radioGroupTERMITE_BAITING_DEVICES_REPLACED,
			radioGroupExteriorPerimeterofhouse, radioGroupAttic,
			radioGroupINTERIOROFHOUSE, radioGroupBUILDERS_CONTRUCTION_GAP,
			radioGroupSOFFIT_RETURNS, radioGroupBREEZYWAY_FROM_GARAGE,
			radioGroupROOF_VENTS, radioGroupGARAGE_DOOR,
			radioGroupSOFFIT_VENTS, radioGroupAC_DRYER_VENTS,
			radioGroupSIDEING_HOUSE, radioGroupCRAWL_SPACE_VENTS,
			radioGroupCRAWL_SPACE_DOOR;

	RadioButton radioCash, radioCheck, radioCreditcard, radioBillLater,
			radioPreBill, radioNocharge, radiobtnFenceInspected1,
			radiobtnFenceInspected2, radiobtnBaitingDevices1,
			radiobtnBaitingDevices2, radiobtnTERMITE_BAITING_DEVICES_REPLACED1,
			radiobtnTERMITE_BAITING_DEVICES_REPLACED2,
			radiobtnExteriorPerimeterofhouse1,
			radiobtnExteriorPerimeterofhouse2, radiobtnAttic1, radiobtnAttic2,
			radiobtnINTERIOROFHOUSE1, radiobtnINTERIOROFHOUSE2,
			radiobtnBUILDERS_CONTRUCTION_GAP1,
			radiobtnBUILDERS_CONTRUCTION_GAP2, radiobtnSOFFIT_RETURNS1,
			radiobtnSOFFIT_RETURNS2, radiobtnBREEZYWAY_FROM_GARAGE1,
			radiobtnBREEZYWAY_FROM_GARAGE2, radiobtnROOF_VENTS1,
			radiobtnROOF_VENTS2, radiobtnGARAGE_DOOR1, radiobtnGARAGE_DOOR2,
			radiobtnSOFFIT_VENTS1, radiobtnSOFFIT_VENTS2,
			radiobtnAC_DRYER_VENTSR1, radiobtnAC_DRYER_VENTSR2,
			radiobtnSIDEING_HOUSE1, radiobtnSIDEING_HOUSE2,
			radiobtnCRAWL_SPACE_VENTS1, radiobtnCRAWL_SPACE_VENTS2,
			radiobtnCRAWL_SPACE_DOOR1, radiobtnCRAWL_SPACE_DOOR2;

	ArrayList<CheckBox> serviceForCheckboxList = new ArrayList<CheckBox>();
	ArrayList<CheckBox> AreasInspectedCheckboxList = new ArrayList<CheckBox>();
	ArrayList<CheckBox> InspectedTreatedForCheckboxList = new ArrayList<CheckBox>();
	ArrayList<CheckBox> PestActivityZoneCheckboxList = new ArrayList<CheckBox>();
	ArrayList<CheckBox> pestControlServicesCheckboxList = new ArrayList<CheckBox>();
	ArrayList<CheckBox> treatmentOfSchoolCheckboxList = new ArrayList<CheckBox>();
	ArrayList<CheckBox> mosquitoCheckboxList = new ArrayList<CheckBox>();
	ArrayList<CheckBox> OutsidepestControlServicesCheckboxList = new ArrayList<CheckBox>();
	ArrayList<CheckBox> InsidepestControlServicesCheckboxList = new ArrayList<CheckBox>();

	ArrayList<ImgDto> listFrontImg = new ArrayList<ImgDto>();
	ArrayList<String> tempFrontImagList = new ArrayList<String>();
	ArrayList<String> tempStoreFrontImageName = new ArrayList<String>();
	ArrayList<ImgDto> listBackImg = new ArrayList<ImgDto>();
	ArrayList<String> tempBackImagList = new ArrayList<String>();
	ArrayList<String> tempStoreBackImageName = new ArrayList<String>();
	ArrayList<String> validationList=new ArrayList<String>();
	
	public static final int REQUEST_Camera_ACTIVITY_CODE_Front=101;
	public static final int REQUEST_Camera_ACTIVITY_CODE_Back=102;
	public static final int REQUEST_Gallery_ACTIVITY_CODE_Front=103;
	public static final int REQUEST_Gallery_ACTIVITY_CODE_Back=104;
	String CheckFrontImageName,CheckBackImageName,strTreatmentSchool;
	Dialog dialogFrontImage,dialogBackImage;
	public static Bitmap bitmapFront,bitmapBack;
	HorizontalListView imageFrontListView,imageBackListView;
	GalleryImageAdapter adapterFront,adapterBack;
	int checkChange = 0;
	Boolean isValidated = true;
	EditText objectFocus;
	long mLastClickTime = 0;
	OResidential_pest_DTO oresidential_pest_DTO;
	HoustonFlowFunctions houstonFlowFunctions;
	UserDTO userDTO;
	GeneralInfoDTO generalInfoDTO;
	String serviceID_new = "", strServiceFor, strAreasInspected,
			strpestActivityByZone, strInspectedadnTreatedFor,strInspectedadnTreatedFor1,strInspectedadnTreatedFor2,
			strpestControlServices, strOutsideService, strMosquito,strSchoolTreatment,strFloridaResidentialServiceId,
			// for pest activity by zone
			strEdttxtFenceLinesByZone,strEdttxtFrontyardByZone,strEdttxtBackYardByZone,strEdttxtExteriorPerimeterHouseByZone,
			strEdttxtRoofGutterLine,strEdttxtGarageBYZone,strEdttxtAtticByZone,strEdttxtBasement,strEdttxtInteriorOfHouseByZone,
			strEdttxtShedByZone,strEdttxtOtherByZone,strInsidepestControlServices,strOutsidepestControlServices,
			strCheckFenceInspected,
			strRadioFenceInspected,strBaitingDevicesInspected,strCheckNumberBaitingDevicesInspectedTermite
			,strCheckBaitingDevice,strRadioBaitingDevice,strCheckTermiteBaitingDevicereplaced,
			strCheckExteriorPerimeterofhouseTermite,strCheckATTICTermite,strCheckINTERIOROFHOUSETermite,strRadioINTERIOROFHOUSETermite
			,strCheckBasementTermite,strBasementTermite,strRadioATTICTermite,strRadioBaitingDevicereplaced
			,strCheckBUILDERS_CONTRUCTION_GAP,strRadioBUILDERS_CONTRUCTION_GAP,strCheckSOFFIT_RETURNS,strRadioSOFFIT_RETURNS
			,strRadioInvoice,strEdtExpirationDate,strEdttxtCheckNo,strEdttxtDrivingLicence,
			strEdttxtAmount,strEdttxtTotal,strEdttxtTax,strEdttxtProdValue,strEdttxtInvValue,
			strEdttxtdate,strEdttxtCustomer,strEdttxtEmp,strEdttxtServiceTech,
			strEdttxtTechComments,strCheckTreeBranches,strCheckFirewood,strCheckDebrisInCrawl,
			strCheckExcessiveplant,strCheckSoilAbove,strCheckWoodSoil,strCheckDebrisRoof,
			strCheckStandingWater,strCheckMoistureProblem,strCheckOpeningPlumbing,
			strCheckExcessiveGaps,strCheckLaekyPlumbing,strCheckGarbageCans,
			strCheckMoistureDamage,strCheckGrocerybags,strCheckPetFood,strCheckRecycledItems,
			strCheckExcessiveStorage,strEdttxtMosquitoServices1,strEdttxtMosquitoServices2,
			strCheckCRAWL_SPACE_DOOR,strRadioCRAWL_SPACE_DOOR,strCheckCRAWL_SPACE_VENTS,
			strRadioCRAWL_SPACE_VENTS,strCheckNumber_SNAPTRAPS,strNumber_SNAPTRAPS,
			strCheckSIDEING_HOUSE,strRadioSIDEING_HOUSE,strNO_LIVEANIMAL_TRAPS_INSTALLES,
			strCheckNO_LIVEANIMAL_TRAPS_INSTALLES,strRadioNO_LIVEANIMAL_TRAPS_INSTALLES,
			strRadioSOFFIT_VENTS,strCheckSOFFIT_VENTS,strCheckAC_DRYER_VENTS,
			strRadioAC_DRYER_VENTS,strCheckROOF_VENTS,strRadioROOF_VENTS,strCheckGARAGE_DOOR,strRadioGARAGE_DOOR,strCheckBREEZYWAY_FROM_GARAGE,strRadioBREEZYWAY_FROM_GARAGE,strRadioExteriorPerimeterofhouseTermite;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.or_servicereport);

		Thread.setDefaultUncaughtExceptionHandler(new TopExceptionHandler(this));
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setDisplayShowHomeEnabled(true);
		initialize();

		JSONObject jsGeneralInfo = houstonFlowFunctions
				.getSelectedService(serviceID_new);
		if (jsGeneralInfo != null) {
			generalInfoDTO = new Gson().fromJson(jsGeneralInfo.toString(),
					GeneralInfoDTO.class);
			LoggerUtil.e("","generalInfoDTO inside if..."+ generalInfoDTO.getCheckNo());
		}
		  addCheckBoxexToList();
		  //generateChemicaltable();
		  setPreviousFilledData();
		  checkChange = 1;
		  if (generalInfoDTO.getServiceStatus().equals("Complete")) {
		  setViewForComplete();
	 }
		super.onCreate(savedInstanceState);
	}

	private void setViewForComplete() {
		// TODO Auto-generated method stub
		btnCHECK_FRONT_IMAGE.setEnabled(false);
		btnCHECK_BACK_IMAGE.setEnabled(false);
		
		LinearlayoutCHECK_IMAGE.setEnabled(false);
		LinearlayoutCHECK_FRONT_IMAGE.setEnabled(false);
		LinearlayoutCHECK_BACK_IMAGE.setEnabled(false);

		edtFenceLinesByZone.setEnabled(false);
		edtBackyardByZone.setEnabled(false);
		edtFrontYardByZone.setEnabled(false);
		edtExteriorParaByZone.setEnabled(false);
		edtRoofGutterByZone.setEnabled(false);
		edtGarageByZone.setEnabled(false);
		edtAtticPestByZone.setEnabled(false);
		edtInteriorHouseByZone.setEnabled(false);
		edt_Basement_PestActivityByZone.setEnabled(false);
		edtNUMBER_OF_BAITING_DEVICES_INSPECTED.setEnabled(false);
		edt_Basement_TermiteServices.setEnabled(false);
		edtAdditionalDeviceAdded.setEnabled(false);
		edtNumberDeviceRemoved.setEnabled(false);
		edtNO_LIVEANIMAL_TRAPS_INSTALLES.setEnabled(false);
		edtNumber_SNAPTRAPS.setEnabled(false);
		edtMosquitoService1.setEnabled(false);
		edtMosquitoService2.setEnabled(false);
		edttxtInspectedandTreatedFor2.setEnabled(false);
		edttxtInspectedandTreatedFor1.setEnabled(false);
		edtTechComment.setEnabled(false);
		edtServiceTech.setEnabled(false);
		edtEMP.setEnabled(false);
		edtCustomer.setEnabled(false);
		edtDate.setEnabled(false);
		edtIntValue.setEnabled(false);
		edtProductvalue.setEnabled(false);
		edtTaxvalue.setEnabled(false);
		edtTotal.setEnabled(false);
		edtAmount.setEnabled(false);
		edtCheckNo.setEnabled(false);
		edtDRIVING_LICENCE.setEnabled(false);
		edtEXPIRATION_DATE.setEnabled(false);

		radioGroupInvoice.setEnabled(false);
		radioGroupFenceInspected.setEnabled(false);
		radioGroupBaitingDevices.setEnabled(false);
		radioGroupTERMITE_BAITING_DEVICES_REPLACED.setEnabled(false);
		radioGroupExteriorPerimeterofhouse.setEnabled(false);
		radioGroupAttic.setEnabled(false);
		radioGroupINTERIOROFHOUSE.setEnabled(false);
		radioGroupBUILDERS_CONTRUCTION_GAP.setEnabled(false);
		radioGroupSOFFIT_RETURNS.setEnabled(false);
		radioGroupBREEZYWAY_FROM_GARAGE.setEnabled(false);
		radioGroupROOF_VENTS.setEnabled(false);
		radioGroupGARAGE_DOOR.setEnabled(false);
		radioGroupSOFFIT_VENTS.setEnabled(false);
		radioGroupAC_DRYER_VENTS.setEnabled(false);
		radioGroupSIDEING_HOUSE.setEnabled(false);
		radioGroupCRAWL_SPACE_VENTS.setEnabled(false);
		radioGroupCRAWL_SPACE_DOOR.setEnabled(false);

		radioCash.setEnabled(false);
		radioCheck.setEnabled(false);
		radioCreditcard.setEnabled(false);
		radioBillLater.setEnabled(false);
		radioPreBill.setEnabled(false);
		radioNocharge.setEnabled(false);
		radiobtnFenceInspected1.setEnabled(false);
		radiobtnFenceInspected2.setEnabled(false);
		radiobtnBaitingDevices1.setEnabled(false);
		radiobtnBaitingDevices2.setEnabled(false);
		radiobtnTERMITE_BAITING_DEVICES_REPLACED1.setEnabled(false);
		radiobtnTERMITE_BAITING_DEVICES_REPLACED2.setEnabled(false);
		radiobtnExteriorPerimeterofhouse1.setEnabled(false);
		radiobtnExteriorPerimeterofhouse2.setEnabled(false);
		radiobtnAttic1.setEnabled(false);
		radiobtnAttic2.setEnabled(false);
		radiobtnINTERIOROFHOUSE1.setEnabled(false);
		radiobtnINTERIOROFHOUSE2.setEnabled(false);
		radiobtnBUILDERS_CONTRUCTION_GAP1.setEnabled(false);
		radiobtnBUILDERS_CONTRUCTION_GAP2.setEnabled(false);
		radiobtnSOFFIT_RETURNS1.setEnabled(false);
		radiobtnSOFFIT_RETURNS2.setEnabled(false);
		radiobtnBREEZYWAY_FROM_GARAGE1.setEnabled(false);
		radiobtnBREEZYWAY_FROM_GARAGE2.setEnabled(false);
		radiobtnROOF_VENTS1.setEnabled(false);
		radiobtnROOF_VENTS2.setEnabled(false);
		radiobtnGARAGE_DOOR1.setEnabled(false);
		radiobtnGARAGE_DOOR2.setEnabled(false);
		radiobtnSOFFIT_VENTS1.setEnabled(false);
		radiobtnSOFFIT_VENTS2.setEnabled(false);
		radiobtnAC_DRYER_VENTSR1.setEnabled(false);
		radiobtnAC_DRYER_VENTSR2.setEnabled(false);
		radiobtnSIDEING_HOUSE1.setEnabled(false);
		radiobtnSIDEING_HOUSE2.setEnabled(false);
		radiobtnCRAWL_SPACE_VENTS1.setEnabled(false);
		radiobtnCRAWL_SPACE_VENTS2.setEnabled(false);
		radiobtnCRAWL_SPACE_DOOR1.setEnabled(false);
		radiobtnCRAWL_SPACE_DOOR2.setEnabled(false);

		or_checkboxSignaturePestcontrol.setEnabled(false);
		or_checkboxPlusTermites.setEnabled(false);
		or_checkboxPlusMosquitomistingSystem.setEnabled(false);
		or_checkboxPlusFireAnts.setEnabled(false);
		or_checkboxPluSRodents.setEnabled(false);
		or_checkboxPlusFleas.setEnabled(false);
		or_checkboxTermiteTreatment.setEnabled(false);
		or_checkboxRodentExclusion.setEnabled(false);
		or_checkboxBedBugTreatment.setEnabled(false);
		or_checkboxOutSideService.setEnabled(false);
		or_checkboxFenceLines.setEnabled(false);
		or_checkboxBackYardByZone.setEnabled(false);
		or_checkboxFrontyardByZOne.setEnabled(false);
		or_checkboxExteriorParametresofhouseByZone.setEnabled(false);
		or_checkboxRofGutter.setEnabled(false);
		or_checkboxGarage.setEnabled(false);
		or_checkboxAtticByZone.setEnabled(false);
		or_checkboxInteriorHouseByZone.setEnabled(false);
		or_checkbox_Basement_PestActivityZone.setEnabled(false);
		or_checkboxINTERIOR.setEnabled(false);
		or_checkboxTreatedAcct.setEnabled(false);
		or_checkboxTreatedKitchen.setEnabled(false);
		or_checkboxTreatedCracks.setEnabled(false);
		or_checkboxBasements.setEnabled(false);
		or_checkboxTreatedliving.setEnabled(false);
		or_checkboxTreatedZoneMonitors.setEnabled(false);
		or_checkboxTreatedBedrooms.setEnabled(false);
		or_checkboxEXTERIOR.setEnabled(false);
		or_checkboxTreatedFront.setEnabled(false);
		or_checkboxTreatedback.setEnabled(false);
		or_checkboxTreatedLaundryRooms.setEnabled(false);
		or_checkboxTreatedLivingRooms.setEnabled(false);
		or_checkboxtreatedyards.setEnabled(false);
		or_checkboxTreatedfirenets.setEnabled(false);
		or_checkboxTreatedDoorways.setEnabled(false);
		or_checkboxTreatedCrawlSpace.setEnabled(false);
		or_checkboxFenceInspectedTermite.setEnabled(false);
		or_checkboxNumberBaitingDevicesInspectedTermite.setEnabled(false);
		or_checkboxBaitingDevicesTermite.setEnabled(false);
		or_checkboxTermiteBaitingDevicereplacedTermite.setEnabled(false);
		or_checkboxExteriorPerimeterofhouseTermite.setEnabled(false);
		or_checkboxATTICTermite.setEnabled(false);
		or_checkboxINTERIOROFHOUSETermite.setEnabled(false);
		or_checkbox_Basement_Termite.setEnabled(false);
		or_checkboxAdditionalDeviceAddedTermite.setEnabled(false);
		or_checkboxNumberDeviceRemovedTermite.setEnabled(false);
		or_checkboxBUILDERS_CONTRUCTION_GAP.setEnabled(false);
		or_checkboxSOFFIT_RETURNS.setEnabled(false);
		or_checkboxBREEZYWAY_FROM_GARAGE.setEnabled(false);
		or_checkboxROOF_VENTS.setEnabled(false);
		or_checkboxGARAGE_DOOR.setEnabled(false);
		or_checkboxSOFFIT_VENTS.setEnabled(false);
		or_checkboxAC_DRYER_VENTS.setEnabled(false);
		or_checkboxNO_LIVEANIMAL_TRAPS_INSTALLES.setEnabled(false);
		or_checkboxSIDEING_HOUSE.setEnabled(false);
		or_checkboxNumber_SNAPTRAPS.setEnabled(false);
		or_checkboxCRAWL_SPACE_VENTS.setEnabled(false);
		or_checkboxCRAWL_SPACE_DOOR.setEnabled(false);
		or_checkboxTopped_TANK.setEnabled(false);
		or_checkboxLEACKY_TANK.setEnabled(false);
		or_checkboxTUBING_LEACKY.setEnabled(false);
		or_checkboxINSPECTED_NOZZLES.setEnabled(false);
		or_checkboxRAN_SYATEMS.setEnabled(false);
		or_checkboxNOZZLES_LEAKING.setEnabled(false);
		or_checkboxCHECKED_RUN_DURACTION_TIME.setEnabled(false);
		or_checkboxCHECKED_TIME_SYSTEM_RUNS.setEnabled(false);
		or_checkboxBranchHouse.setEnabled(false);
		or_checkboxFirewood.setEnabled(false);
		or_checkboxDebrisCrawl.setEnabled(false);
		or_checkboxExcessiveplant.setEnabled(false);
		or_checkboxSoilAbove.setEnabled(false);
		or_checkboxWoodSoil.setEnabled(false);
		or_checkboxDebrisRoof.setEnabled(false);
		or_checkboxStandingwater.setEnabled(false);
		or_checkboxMoistureProblem.setEnabled(false);
		or_checkboxOpeningspumbing.setEnabled(false);
		or_checkboxExcessiveGaps.setEnabled(false);
		or_checkboxLeackPumbing.setEnabled(false);
		or_checkboxgarbageCans.setEnabled(false);
		or_checkboxMoisrtureDamage.setEnabled(false);
		or_checkboxGroceryBags.setEnabled(false);
		or_checkboxPetFood.setEnabled(false);
		or_checkboxRecycledItems.setEnabled(false);
		or_checkboxExcessiveStorage.setEnabled(false);
		or_checkboxSCHOOL_HOLIDAY.setEnabled(false);
		or_checkboxRE_ENTRY_SIGN.setEnabled(false);
		or_checkboxWARNING_SIGN.setEnabled(false);
		or_checkboxDOORS_LOCKED_AREA_SECURED.setEnabled(false);
		or_checkboxNO_CHILDREN_STUDENT_PRESENT.setEnabled(false);
		or_checkboxRoaches.setEnabled(false);
		or_checkboxSpiders.setEnabled(false);
		or_checkboxScorpions.setEnabled(false);
		or_checkboxSilverfish.setEnabled(false);
		or_checkboxPANTRY_PESTS.setEnabled(false);
		or_checkboxPHARAOH_ANTS.setEnabled(false);
		or_checkboxFIRE_ANTS.setEnabled(false);
		or_checkboxCARPENTER_ANTS.setEnabled(false);
		or_checkboxMILLIPEDES.setEnabled(false);
		or_checkboxEARWIGS.setEnabled(false);
		or_checkboxPILL_BUGS.setEnabled(false);
		or_checkboxCRICKETS.setEnabled(false);
		or_checkboxWASPS.setEnabled(false);
		or_checkboxSUB_TERMITIES.setEnabled(false);
		or_checkboxDRYWOOD_TERMITIES.setEnabled(false);
		or_checkboxROOF_RATS.setEnabled(false);
		or_checkboxMICE.setEnabled(false);
		or_checkboxRACCOONS.setEnabled(false);
		or_checkboxOPOSSUMS.setEnabled(false);
		or_checkboxSKUNKS.setEnabled(false);
		or_checkboxSquirrels.setEnabled(false);
		or_checkboxANTS_Inspected_treated_for.setEnabled(false);
		or_checkboxARGENTINE_ANTS.setEnabled(false);
		or_checkboxOTHER_PESTS_INSPECTED_TREATED_FOR.setEnabled(false);

	}

	private void setPreviousFilledData() {
		// TODO Auto-generated method stub
		// txtName.setText(generalInfoDTO.getFirst_Name());
		txtAccountNumber.setText(generalInfoDTO.getAccountNo());
		// txtTimeIn.setText(generalInfoDTO.getTimeIn());
		txtOrderNumber.setText(generalInfoDTO.getOrder_Number());

		JSONObject orlando_res_pest_string = HoustonFlowFunctions
				.getOrlandoResidentialReport(serviceID_new);
		if (!(orlando_res_pest_string == null)) {
			// Log.e("orlando string.. to string..",
			// "orlando string..."+orlando_res_pest_string.toString());
			oresidential_pest_DTO = new Gson().fromJson(
					orlando_res_pest_string.toString(),
					OResidential_pest_DTO.class);

			setPreviousServiceForData();
			setPreviousAreaseInspected();
			setPreviousInspectedTreatedFor();
			setPreviousOutsideOnly();
			setPreviousPestByZone();
			setPreviousPestControlServices();
			setPreviousTermiteServices();
			setPreviousRodentServices();
			setPreviousMosquitoServices();
			setPreviousTreatmentForSchool();
			setpreviousRecommendations();

		}
		setPreviousInvoice();
	}

	private void setPreviousInvoice() {

		ArrayList<RadioButton> listRadioButton = new ArrayList<RadioButton>();
		listRadioButton.add(radioCash);
		listRadioButton.add(radioCheck);
		listRadioButton.add(radioCreditcard);
		listRadioButton.add(radioNocharge);
		listRadioButton.add(radioBillLater);
		listRadioButton.add(radioPreBill);
		// Log.e("getPaymentType...",listRadioButton.size()+"");

		for (int i = 0; i < listRadioButton.size(); i++) {

			String selection = listRadioButton.get(i).getText().toString();
			// Log.v("","getPaymentType..."+selection+"..."+residential_pest_DTO.getPaymentType());
			if (selection.equals(oresidential_pest_DTO.getPaymentType())) {
				listRadioButton.get(i).setChecked(true);
				break;
			}
		}
		edtTechComment.setText(oresidential_pest_DTO.getTechnicianComments());
		edtServiceTech.setText(generalInfoDTO.getService_Tech_Num());
		edtEMP.setText(oresidential_pest_DTO.getEmpNo());
		edtDate.setText(generalInfoDTO.getServiceDate());

		if (CommonFunction.checkString(generalInfoDTO.getFirst_Name(), "").equals("")
				&& CommonFunction.checkString(generalInfoDTO.getLast_Name(), "").equals("")) {
			edtCustomer.setText(generalInfoDTO.getCompanyName());

		} else {
			edtCustomer.setText(generalInfoDTO.getFirst_Name() + " "+ generalInfoDTO.getLast_Name());

		}
		edtIntValue.setText(generalInfoDTO.getInv_value());
		edtProductvalue.setText(generalInfoDTO.getProd_value());

		String taxValue;
		double roundOff = 0;
		if (CommonFunction.checkString(generalInfoDTO.getInv_value(), "")
				.equals("")
				|| CommonFunction.checkString(generalInfoDTO.getTaxCode(), "")
						.equals("")) {
			taxValue = "0";
		} else {
			Float taxValueFloat = Float.valueOf(generalInfoDTO.getInv_value())
					* Float.valueOf(generalInfoDTO.getTaxCode());
			roundOff = Math.round(taxValueFloat * 100.0) / 100.0;
			taxValue = String.valueOf(roundOff);

		}

		edtTaxvalue.setText(taxValue);
		// Log.e("taxValue", "taxValue.."+taxValue);
		Double totlaD = roundOff
				+ Double.valueOf(generalInfoDTO.getInv_value());
		Double totalFinal = Math.round(totlaD * 100.0) / 100.0;
		edtTotal.setText(String.valueOf(totalFinal));
		edtCheckNo.setText(generalInfoDTO.getCheckNo());
		// Toast.makeText(getApplicationContext(),
		// generalInfoDTO.getLicenseNo()+"", Toast.LENGTH_LONG).show();

		edtDRIVING_LICENCE.setText(generalInfoDTO.getLicenseNo());
		if (!generalInfoDTO.getExpirationDate().equals("01/01/1900")
				&& !generalInfoDTO.getExpirationDate().equals("")) {
			edtEXPIRATION_DATE.setText(generalInfoDTO.getExpirationDate());
		}

		edtAmount.setText(oresidential_pest_DTO.getAmount());

		strFloridaResidentialServiceId = oresidential_pest_DTO.getFloridaResidentialServiceId();
		// Log.e("",
		// "strResidentialId.."+oresidential_pest_DTO.getResidentialId());
		if (CommonFunction.checkString(strFloridaResidentialServiceId, "").equals("")) {
			strFloridaResidentialServiceId = "0";
		}
		if (!CommonFunction.checkString(generalInfoDTO.getCheckFrontImage(), "").equals("")) {
			RefreshFrontImage();
			reloadPriviousCheckFront();
		}
		if (!CommonFunction.checkString(generalInfoDTO.getCheckBackImage(), "").equals("")) {
			RefreshBackImage();
			reloadPriviousCheckBack();
		}
	}

	private void reloadPriviousCheckBack() {
		// TODO Auto-generated method stub
		String imageNames=generalInfoDTO.getCheckBackImage();

		if(!CommonFunction.checkString(imageNames, "").equals(""))
		{
			String[] imageArray=imageNames.split(",");
			for(int i=0;i<imageArray.length;i++)
			{
				if(!imageArray[i].equals(""))
				{
					tempStoreBackImageName.add(imageArray[i]);
					RefreshBackImage();
				}
			}
		}
	}

	private void RefreshBackImage() {
		// TODO Auto-generated method stub
		try
		{ 
			listBackImg.clear();
			Bitmap bmp = null;
			for (int i = 0; i < tempStoreBackImageName.size(); i++) {
				bmp = null;
				ImgDto dto = new ImgDto();
				try 
				{
					LoggerUtil.e("tempStoreBackImageName in refresh",tempStoreBackImageName.get(i));
					/*	bmp = BitmapFactory.decodeStream(getContentResolver()
						.openInputStream(Uri.fromFile(CommanFunction.getFileLocation(IWitnessActivity.this,tempStoreFrontImageName.get(i)))));
					 */    
					bmp=decodeSmallUri(Uri.fromFile(CommonFunction.getFileLocation(OR_PestControlServiceReport.this,tempStoreBackImageName.get(i))));

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Bitmap bitmap = scaleBitmap(bmp, 100, 100);
				dto.setImageBitMap(bitmap);
				if (!tempBackImagList.contains(tempStoreBackImageName.get(i))) {

					if (bmp != null) 
					{

						//CompressImage(bmp);
					}
				}// dto.setImageUri(Uri.fromFile(CommanFunction.getFileLocation(IWitnessActivity.this,
				// tempStoreFrontImageName.get(i))));

				tempBackImagList.add(tempStoreBackImageName.get(i));
				listBackImg.add(dto);
			}

			//LoggerUtil.e("List Size", listImg.size() + "");

			/*if (listBackImg.size() == 0) {
				ImgDto dto = new ImgDto();
				dto.setImagedrawable(R.drawable.noimage);
				listBackImg.add(dto);
			}else
			{

			}*/
			adapterBack=new GalleryImageAdapter(getApplicationContext(), listBackImg) ;
			imageBackListView.setAdapter(adapterBack);
			//adapter.notifyDataSetChanged(); // TODO Auto-generated method stub
		}
		catch (Exception e) 
		{
			LoggerUtil.e("Exception in Refresh Image", e+"");
		}
	}

	private void reloadPriviousCheckFront() {
		// TODO Auto-generated method stub
		String imageNames=generalInfoDTO.getCheckFrontImage();

		//Log.e("", "imageNames.."+imageNames);

		if(!CommonFunction.checkString(imageNames, "").equals(""))
		{
			String[] imageArray=imageNames.split(",");
			for(int i=0;i<imageArray.length;i++)
			{
				if(!imageArray[i].equals(""))
				{
					tempStoreFrontImageName.add(imageArray[i]);
					RefreshFrontImage();
				}
			}
		}
	}

	private void RefreshFrontImage() {
		// TODO Auto-generated method stub
		try
		{ 
			listFrontImg.clear();
			Bitmap bmp = null;
			for (int i = 0; i < tempStoreFrontImageName.size(); i++) {
				bmp = null;
				ImgDto dto = new ImgDto();
				try {
					LoggerUtil.e("tempStoreFrontImageName in refresh",tempStoreFrontImageName.get(i));
					/*	bmp = BitmapFactory.decodeStream(getContentResolver()
						.openInputStream(Uri.fromFile(CommanFunction.getFileLocation(IWitnessActivity.this,tempStoreFrontImageName.get(i)))));
					 */    
					bmp=decodeSmallUri(Uri.fromFile(CommonFunction.getFileLocation(OR_PestControlServiceReport.this,tempStoreFrontImageName.get(i))));

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Bitmap bitmap = scaleBitmap(bmp, 100, 100);
				dto.setImageBitMap(bitmap);
				if (!tempFrontImagList.contains(tempStoreFrontImageName.get(i))) {

					if (bmp != null) {

						//CompressImage(bmp);
					}
				}// dto.setImageUri(Uri.fromFile(CommanFunction.getFileLocation(IWitnessActivity.this,
				// tempStoreFrontImageName.get(i))));

				tempFrontImagList.add(tempStoreFrontImageName.get(i));
				listFrontImg.add(dto);
			}

			//LoggerUtil.e("List Size", listImg.size() + "");

			/*if (listFrontImg.size() == 0)
			{
				ImgDto dto = new ImgDto();
				dto.setImagedrawable(R.drawable.noimage);
				listFrontImg.add(dto);
			}
			else
			{

			}*/
			adapterFront=new GalleryImageAdapter(getApplicationContext(), listFrontImg) ;
			imageFrontListView.setAdapter(adapterFront);
			//adapter.notifyDataSetChanged(); // TODO Auto-generated method stub
		}
		catch (Exception e) 
		{
			LoggerUtil.e("Exception in Refresh Image", e+"");
		}
	}
	
    private Bitmap decodeSmallUri(Uri selectedImage) throws FileNotFoundException 
	{
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(OR_PestControlServiceReport.this.getContentResolver().openInputStream(selectedImage), null, o);

		final int REQUIRED_SIZE = 100;

		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeStream(OR_PestControlServiceReport.this.getContentResolver().openInputStream(selectedImage), null, o2);
	}

	public void getfile(String fileName)
	{
		try
		{
			LoggerUtil.e("Get File Called", fileName);

			File imagefile=CommonFunction.getFileLocation(OR_PestControlServiceReport.this, fileName);
			if(imagefile.exists())
			{
				//Bitmap myBitmap = BitmapFactory.decodeFile(imagefile.getAbsolutePath());

				Bitmap myBitmap=decodeUri(Uri.fromFile(CommonFunction.getFileLocation(OR_PestControlServiceReport.this,fileName)));
				LoggerUtil.e("Input Bitmap width and heigth  ", myBitmap.getWidth()+" "+myBitmap.getWidth());
				//	edtDescription.setText(fileName+" "+myBitmap.getWidth()+" and "+myBitmap.getHeight());
				Bitmap output = Bitmap.createScaledBitmap(myBitmap, 800, 800, true);
				LoggerUtil.e("Out Put Bitmap width and heigth  ", output.getWidth()+" "+output.getWidth());
				imagefile.delete();
				CommonFunction.saveBitmap(output, OR_PestControlServiceReport.this, fileName);
			}}catch(Exception e)
			{

			}
	}

	private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException 
	{
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(OR_PestControlServiceReport.this.getContentResolver().openInputStream(selectedImage), null, o);

		final int REQUIRED_SIZE = 800;

		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeStream(OR_PestControlServiceReport.this.getContentResolver().openInputStream(selectedImage), null, o2);
	}
	public static Bitmap scaleBitmap(Bitmap bitmapToScale, float newWidth,float newHeight) 
	{
		if (bitmapToScale == null)
			return null;
		// get the original width and height
		int width = bitmapToScale.getWidth();
		int height = bitmapToScale.getHeight();
		// create a matrix for the manipulation
		Matrix matrix = new Matrix();

		// resize the bit map
		matrix.postScale(newWidth / width, newHeight / height);

		// recreate the new Bitmap and set it back
		return Bitmap.createBitmap(bitmapToScale, 0, 0, bitmapToScale
				.getWidth(), bitmapToScale.getHeight(), matrix, true);
	}

	private void setpreviousRecommendations() {
		// TODO Auto-generated method stub
		if (CommonFunction.checkString(oresidential_pest_DTO.getTreeBranches(),
				"").equals("")) {
			or_checkboxBranchHouse.setChecked(false);
		} else {
			or_checkboxBranchHouse.setChecked(true);
		}
		if (CommonFunction.checkString(oresidential_pest_DTO.getFirewood(), "")
				.equals("")) {
			or_checkboxFirewood.setChecked(false);
		} else {
			or_checkboxFirewood.setChecked(true);
		}
		if (CommonFunction.checkString(oresidential_pest_DTO.getDebrisCrawl(),
				"").equals("")) {
			or_checkboxDebrisCrawl.setChecked(false);
		} else {
			or_checkboxDebrisCrawl.setChecked(true);
		}
		if (CommonFunction.checkString(
				oresidential_pest_DTO.getExcessivePlant(), "").equals("")) {
			or_checkboxExcessiveplant.setChecked(false);
		} else {
			or_checkboxExcessiveplant.setChecked(true);
		}
		if (CommonFunction.checkString(oresidential_pest_DTO.getSoil(), "")
				.equals("")) {
			or_checkboxSoilAbove.setChecked(false);
		} else {
			or_checkboxSoilAbove.setChecked(true);
		}
		if (CommonFunction.checkString(oresidential_pest_DTO.getWoodSoil(), "")
				.equals("")) {
			or_checkboxWoodSoil.setChecked(false);
		} else {
			or_checkboxWoodSoil.setChecked(true);
		}
		if (CommonFunction.checkString(oresidential_pest_DTO.getDebrisRoof(),
				"").equals("")) {
			or_checkboxDebrisRoof.setChecked(false);
		} else {
			or_checkboxDebrisRoof.setChecked(true);
		}
		if (CommonFunction.checkString(
				oresidential_pest_DTO.getStandingWater(), "").equals("")) {
			or_checkboxStandingwater.setChecked(false);
		} else {
			or_checkboxStandingwater.setChecked(true);
		}
		if (CommonFunction.checkString(
				oresidential_pest_DTO.getMoistureProblem(), "").equals("")) {
			or_checkboxMoistureProblem.setChecked(false);
		} else {
			or_checkboxMoistureProblem.setChecked(true);
		}
		if (CommonFunction.checkString(oresidential_pest_DTO.getOpenings(), "")
				.equals("")) {
			or_checkboxOpeningspumbing.setChecked(false);
		} else {
			or_checkboxOpeningspumbing.setChecked(true);
		}
		if (CommonFunction.checkString(
				oresidential_pest_DTO.getExcessiveGaps(), "").equals("")) {
			or_checkboxExcessiveGaps.setChecked(false);
		} else {
			or_checkboxExcessiveGaps.setChecked(true);
		}
		if (CommonFunction.checkString(
				oresidential_pest_DTO.getLeakyPlumbing(), "").equals("")) {
			or_checkboxLeackPumbing.setChecked(false);
		} else {
			or_checkboxLeackPumbing.setChecked(true);
		}
		if (CommonFunction.checkString(oresidential_pest_DTO.getGarbageCans(),
				"").equals("")) {
			or_checkboxgarbageCans.setChecked(false);
		} else {
			or_checkboxgarbageCans.setChecked(true);
		}
		if (CommonFunction.checkString(
				oresidential_pest_DTO.getMoistureDamaged(), "").equals("")) {
			or_checkboxMoisrtureDamage.setChecked(false);
		} else {
			or_checkboxMoisrtureDamage.setChecked(true);
		}
		if (CommonFunction.checkString(oresidential_pest_DTO.getGroceryBags(),
				"").equals("")) {
			or_checkboxGroceryBags.setChecked(false);
		} else {
			or_checkboxGroceryBags.setChecked(true);
		}
		if (CommonFunction.checkString(oresidential_pest_DTO.getPetFood(), "")
				.equals("")) {
			or_checkboxPetFood.setChecked(false);
		} else {
			or_checkboxPetFood.setChecked(true);
		}
		if (CommonFunction.checkString(
				oresidential_pest_DTO.getRecycledItems(), "").equals("")) {
			or_checkboxRecycledItems.setChecked(false);
		} else {
			or_checkboxRecycledItems.setChecked(true);
		}
		if (CommonFunction.checkString(
				oresidential_pest_DTO.getExcessiveStorage(), "").equals("")) {
			or_checkboxExcessiveStorage.setChecked(false);
		} else {
			or_checkboxExcessiveStorage.setChecked(true);
		}
	}
	private void setPreviousTreatmentForSchool() {
		// TODO Auto-generated method stub
		strTreatmentSchool = oresidential_pest_DTO.getTreatmentOfSchool();
		// Log.e("", "service for records...."+strServiceFor);
		ArrayList<String> schoolTreatmentList = new ArrayList<String>(
				Arrays.asList(strTreatmentSchool.split(",")));
		for (int s = 0; s < schoolTreatmentList.size(); s++) {
			// Log.e("", "service for records...inside for 1."+strServiceFor);
			for (int c = 0; c < treatmentOfSchoolCheckboxList.size(); c++) {
				// Log.e("",
				// "service for records...inside for 2."+strServiceFor);
				if (schoolTreatmentList.get(s).equals(
						treatmentOfSchoolCheckboxList.get(c).getText())) {
					// Log.e("",
					// "service for records...inside if."+strServiceFor);
					treatmentOfSchoolCheckboxList.get(c).setChecked(true);
				}
			}
		}
	}

	private void setPreviousMosquitoServices() {
		// TODO Auto-generated method stub
		strMosquito = oresidential_pest_DTO.getPlusMosquitoDetail();
		ArrayList<String> mosquitoList = new ArrayList<String>(
				Arrays.asList(strMosquito.split(",")));
		for (int s = 0; s < mosquitoList.size(); s++) {
			for (int c = 0; c < mosquitoCheckboxList.size(); c++) {
				if (mosquitoList.get(s).equals(
						mosquitoCheckboxList.get(c).getText())) {
					mosquitoCheckboxList.get(c).setChecked(true);
				}
			}
		}
		edtMosquitoService1.setText(oresidential_pest_DTO.getDurationTime());
		edtMosquitoService2.setText(oresidential_pest_DTO.getSystemRuns());
	}

	private void setPreviousRodentServices() {

		if (oresidential_pest_DTO.getBuilderConstruction().equals(
				or_checkboxBUILDERS_CONTRUCTION_GAP.getText())) {
			or_checkboxBUILDERS_CONTRUCTION_GAP.setChecked(true);
			// Log.e("",
			// "ROOF PITCHES..."+oresidential_pest_DTO.getRoofNeeds());
			if (oresidential_pest_DTO.getBuilderConstructionActivity().equals(
					"Yes")) {
				radiobtnBUILDERS_CONTRUCTION_GAP1.setChecked(true);
			} else {
				radiobtnBUILDERS_CONTRUCTION_GAP2.setChecked(true);
			}
		}
		if (oresidential_pest_DTO.getFascia().equals(
				or_checkboxSOFFIT_RETURNS.getText())) {
			or_checkboxSOFFIT_RETURNS.setChecked(true);
			if (oresidential_pest_DTO.getFasciaNeeds().equals("Yes")) {
				radiobtnSOFFIT_RETURNS1.setChecked(true);
			} else {
				radiobtnSOFFIT_RETURNS2.setChecked(true);
			}
		}
		if (oresidential_pest_DTO.getBreezway().equals(
				or_checkboxBREEZYWAY_FROM_GARAGE.getText())) {
			or_checkboxBREEZYWAY_FROM_GARAGE.setChecked(true);
			if (oresidential_pest_DTO.getBreezwayNeeds().equals("Yes")) {
				radiobtnBREEZYWAY_FROM_GARAGE1.setChecked(true);
			} else {
				radiobtnBREEZYWAY_FROM_GARAGE2.setChecked(true);
			}
		}
		if (oresidential_pest_DTO.getRoofVentRodent().equals(
				or_checkboxROOF_VENTS.getText())) {
			or_checkboxROOF_VENTS.setChecked(true);
			if (oresidential_pest_DTO.getRoofVentNeeds().equals("Yes")) {
				radiobtnROOF_VENTS1.setChecked(true);
			} else {
				radiobtnROOF_VENTS2.setChecked(true);
			}
		}
		if (oresidential_pest_DTO.getGarageDoor().equals(
				or_checkboxGARAGE_DOOR.getText())) {
			or_checkboxGARAGE_DOOR.setChecked(true);
			if (oresidential_pest_DTO.getGarageDoorNeeds().equals("Yes")) {
				radiobtnGARAGE_DOOR1.setChecked(true);
			} else {
				radiobtnGARAGE_DOOR2.setChecked(true);
			}
		}
		if (oresidential_pest_DTO.getFlashing().equals(
				or_checkboxSOFFIT_VENTS.getText())) {
			or_checkboxSOFFIT_VENTS.setChecked(true);
			if (oresidential_pest_DTO.getFlashingNeeds().equals("Yes")) {
				radiobtnSOFFIT_VENTS1.setChecked(true);
			} else {
				radiobtnSOFFIT_VENTS2.setChecked(true);
			}
		}
		if (oresidential_pest_DTO.getDryerVent().equals(
				or_checkboxAC_DRYER_VENTS.getText())) {
			or_checkboxAC_DRYER_VENTS.setChecked(true);
			if (oresidential_pest_DTO.getDryerVentNeeds().equals("Yes")) {
				radiobtnAC_DRYER_VENTSR1.setChecked(true);
			} else {
				radiobtnAC_DRYER_VENTSR1.setChecked(true);
			}
		}
		if (oresidential_pest_DTO.getNumberLive().equals(
				or_checkboxNO_LIVEANIMAL_TRAPS_INSTALLES.getText())) {
			or_checkboxNO_LIVEANIMAL_TRAPS_INSTALLES.setChecked(true);
			edtNO_LIVEANIMAL_TRAPS_INSTALLES.setText(oresidential_pest_DTO
					.getNumberLiveNeeds());
		}
		if (oresidential_pest_DTO.getSiding().equals(
				or_checkboxSIDEING_HOUSE.getText())) {
			or_checkboxSIDEING_HOUSE.setChecked(true);
			if (oresidential_pest_DTO.getSidingNeeds().equals("Yes")) {
				radiobtnSIDEING_HOUSE1.setChecked(true);
			} else {
				radiobtnSIDEING_HOUSE1.setChecked(true);
			}
		}
		if (oresidential_pest_DTO.getNumberSnap().equals(
				or_checkboxNumber_SNAPTRAPS.getText())) {
			or_checkboxNumber_SNAPTRAPS.setChecked(true);
			edtNumber_SNAPTRAPS.setText(oresidential_pest_DTO
					.getNumberSnapNeeds());
		}
	}

	private void setPreviousTermiteServices() {
		if (oresidential_pest_DTO.getFencePest().equals(
				or_checkboxFenceInspectedTermite.getText())) {
			or_checkboxFenceInspectedTermite.setChecked(true);
			if (oresidential_pest_DTO.getFenceActivity().equals("Yes")) {
				radiobtnFenceInspected1.setChecked(true);
			} else {
				radiobtnFenceInspected2.setChecked(true);
			}
		}

		if (oresidential_pest_DTO.getNumberPest().equals(
				or_checkboxNumberBaitingDevicesInspectedTermite.getText())) {
			or_checkboxNumberBaitingDevicesInspectedTermite.setChecked(true);
			edtNUMBER_OF_BAITING_DEVICES_INSPECTED
					.setText(oresidential_pest_DTO.getNumberActivitytext());
		}

		if (oresidential_pest_DTO.getWoodPest().equals(
				or_checkboxBaitingDevicesTermite.getText())) {
			or_checkboxBaitingDevicesTermite.setChecked(true);
			// Log.e("oresidential_pest_DTO.getWoodOption()..",
			// "..."+oresidential_pest_DTO.getWoodOption());

			if (oresidential_pest_DTO.getWoodActivity().equals("Yes")) {
				radiobtnBaitingDevices1.setChecked(true);
			} else {
				radiobtnBaitingDevices1.setChecked(true);
			}
		}

		if (oresidential_pest_DTO.getNumberActivity().equals(
				or_checkboxTermiteBaitingDevicereplacedTermite.getText())) {
			or_checkboxTermiteBaitingDevicereplacedTermite.setChecked(true);
			if (oresidential_pest_DTO.getWoodOption().equals("Yes")) {
				radiobtnTERMITE_BAITING_DEVICES_REPLACED1.setChecked(true);
			} else {
				radiobtnTERMITE_BAITING_DEVICES_REPLACED2.setChecked(true);
			}
		}

		if (oresidential_pest_DTO.getExteriorPest().equals(
				or_checkboxExteriorPerimeterofhouseTermite.getText())) {
			or_checkboxExteriorPerimeterofhouseTermite.setChecked(true);
			if (oresidential_pest_DTO.getExteriorActivity().equals("Yes")) {
				radiobtnExteriorPerimeterofhouse1.setChecked(true);
			} else {
				radiobtnExteriorPerimeterofhouse2.setChecked(true);
			}
		}

		if (oresidential_pest_DTO.getAttic().equals(
				or_checkboxATTICTermite.getText())) {
			or_checkboxATTICTermite.setChecked(true);
			if (oresidential_pest_DTO.getAtticActivity().equals("Yes")) {
				radiobtnAttic1.setChecked(true);
			} else {
				radiobtnAttic2.setChecked(true);
			}
		}
		if (oresidential_pest_DTO.getInteriorPest().equals(
				or_checkboxINTERIOROFHOUSETermite.getText())) {
			or_checkboxINTERIOROFHOUSETermite.setChecked(true);
			if (oresidential_pest_DTO.getInteriorActivity().equals("Yes")) {
				radiobtnINTERIOROFHOUSE1.setChecked(true);
			} else {
				radiobtnINTERIOROFHOUSE2.setChecked(true);
			}
		}

	}

	private void setPreviousPestControlServices() {

		strpestControlServices = oresidential_pest_DTO.getPestControlServices();
		ArrayList<String> pestControlServicesList = new ArrayList<String>(
				Arrays.asList(strpestControlServices.split(",")));
		for (int s = 0; s < pestControlServicesList.size(); s++) {
			for (int c = 0; c < pestControlServicesCheckboxList.size(); c++) {
				if (pestControlServicesList.get(s).equals(
						pestControlServicesCheckboxList.get(c).getText())) {
					pestControlServicesCheckboxList.get(c).setChecked(true);
				}
			}
		}

	}

	private void setPreviousPestByZone() {
		strpestActivityByZone = oresidential_pest_DTO.getPestActivity();
		// Log.e("strpestActivityByZone", ""+strpestActivityByZone);
		ArrayList<String> pestActivityList = new ArrayList<String>(
				Arrays.asList(strpestActivityByZone.split(",")));
		// Log.e("pestActivityList.size()", ""+pestActivityList.size());
		for (int s = 0; s < pestActivityList.size(); s++) {
			for (int c = 0; c < PestActivityZoneCheckboxList.size(); c++) {
				if (pestActivityList.get(s).equals(
						PestActivityZoneCheckboxList.get(c).getText())) {
					// Log.e("PestActivityZoneCheckboxList.get(c).getText()",
					// ""+pestActivityList.get(s).equals(PestActivityZoneCheckboxList.get(c).getText()));
					PestActivityZoneCheckboxList.get(c).setChecked(true);
				}
			}
		}

		edtFenceLinesByZone.setText(oresidential_pest_DTO.getFence());
		edtBackyardByZone.setText(oresidential_pest_DTO.getBackYard());

		edtFrontYardByZone.setText(oresidential_pest_DTO.getFrontYard());
		edtExteriorParaByZone.setText(oresidential_pest_DTO
				.getExteriorPerimeter());

		edtRoofGutterByZone.setText(oresidential_pest_DTO.getRoofline());
		edtGarageByZone.setText(oresidential_pest_DTO.getGarage());

		edtAtticPestByZone.setText(oresidential_pest_DTO.getAttic());
		edtInteriorHouseByZone
				.setText(oresidential_pest_DTO.getInteriorHouse());

	}

	private void setPreviousOutsideOnly() {
		strOutsideService = oresidential_pest_DTO.getOutsideOnly();
		if (strOutsideService.equals("true")) {
			or_checkboxOutSideService.setChecked(true);
		}

	}

	private void setPreviousInspectedTreatedFor() {
		strInspectedadnTreatedFor = oresidential_pest_DTO.getInsectActivity();
		ArrayList<String> InspectedTreatedList = new ArrayList<String>(
				Arrays.asList(strInspectedadnTreatedFor.split(",")));
		for (int s = 0; s < InspectedTreatedList.size(); s++) {
			for (int c = 0; c < InspectedTreatedForCheckboxList.size(); c++) {
				if (InspectedTreatedList.get(s).equals(
						InspectedTreatedForCheckboxList.get(c).getText())) {
					InspectedTreatedForCheckboxList.get(c).setChecked(true);
				}
			}
		}
		if (!CommonFunction.checkString(oresidential_pest_DTO.getAnts(), "")
				.equals("")) {
			edttxtInspectedandTreatedFor1.setText(oresidential_pest_DTO
					.getAnts());
			or_checkboxANTS_Inspected_treated_for.setChecked(true);
		}

		if (!CommonFunction.checkString(oresidential_pest_DTO.getOtherPest(),
				"").equals("")) {
			edttxtInspectedandTreatedFor2.setText(oresidential_pest_DTO
					.getOtherPest());
			or_checkboxOTHER_PESTS_INSPECTED_TREATED_FOR.setChecked(true);
		}
	}

	private void setPreviousAreaseInspected() {
		// TODO Auto-generated method stub
		strAreasInspected = oresidential_pest_DTO.getAreasInspected();
		ArrayList<String> areasInspectedList = new ArrayList<String>(
				Arrays.asList(strAreasInspected.split(",")));
		for (int s = 0; s < areasInspectedList.size(); s++) {
			for (int c = 0; c < AreasInspectedCheckboxList.size(); c++) {
				if (areasInspectedList.get(s).equals(
						AreasInspectedCheckboxList.get(c).getText())) {
					AreasInspectedCheckboxList.get(c).setChecked(true);
				}
			}
		}
	}

	private void setPreviousServiceForData() {
		// TODO Auto-generated method stub
		strServiceFor = oresidential_pest_DTO.getServiceFor();
		// Log.e("", "service for records...."+strServiceFor);
		ArrayList<String> serviceForList = new ArrayList<String>(
				Arrays.asList(strServiceFor.split(",")));
		for (int s = 0; s < serviceForList.size(); s++) {
			// Log.e("", "service for records...inside for 1."+strServiceFor);
			for (int c = 0; c < serviceForCheckboxList.size(); c++) {
				// Log.e("",
				// "service for records...inside for 2."+strServiceFor);
				if (serviceForList.get(s).equals(
						serviceForCheckboxList.get(c).getText())) {
					// Log.e("",
					// "service for records...inside if."+strServiceFor);
					serviceForCheckboxList.get(c).setChecked(true);
				}
			}
		}
	}

	private void addCheckBoxexToList() {
		// TODO Auto-generated method stub
		serviceForCheckboxList.add(or_checkboxSignaturePestcontrol);
		serviceForCheckboxList.add(or_checkboxPlusTermites);
		serviceForCheckboxList.add(or_checkboxPlusMosquitomistingSystem);
		serviceForCheckboxList.add(or_checkboxPlusFireAnts);
		serviceForCheckboxList.add(or_checkboxPluSRodents);
		serviceForCheckboxList.add(or_checkboxPlusFleas);
		serviceForCheckboxList.add(or_checkboxTermiteTreatment);
		serviceForCheckboxList.add(or_checkboxRodentExclusion);
		serviceForCheckboxList.add(or_checkboxBedBugTreatment);

		PestActivityZoneCheckboxList.add(or_checkboxFenceLines);
		PestActivityZoneCheckboxList.add(or_checkboxFrontyardByZOne);
		PestActivityZoneCheckboxList.add(or_checkboxBackYardByZone);
		PestActivityZoneCheckboxList
				.add(or_checkboxExteriorParametresofhouseByZone);
		PestActivityZoneCheckboxList.add(or_checkboxRofGutter);
		PestActivityZoneCheckboxList.add(or_checkboxGarage);
		PestActivityZoneCheckboxList.add(or_checkboxAtticByZone);
		PestActivityZoneCheckboxList.add(or_checkboxInteriorHouseByZone);
		PestActivityZoneCheckboxList.add(or_checkbox_Basement_PestActivityZone);

		pestControlServicesCheckboxList.add(or_checkboxEXTERIOR);
		pestControlServicesCheckboxList.add(or_checkboxTreatedFront);
		pestControlServicesCheckboxList.add(or_checkboxTreatedback);
		pestControlServicesCheckboxList.add(or_checkboxTreatedLaundryRooms);
		pestControlServicesCheckboxList.add(or_checkboxTreatedLivingRooms);
		pestControlServicesCheckboxList.add(or_checkboxtreatedyards);
		pestControlServicesCheckboxList.add(or_checkboxTreatedfirenets);
		pestControlServicesCheckboxList.add(or_checkboxTreatedDoorways);
		pestControlServicesCheckboxList.add(or_checkboxTreatedCrawlSpace);

		pestControlServicesCheckboxList.add(or_checkboxINTERIOR);
		pestControlServicesCheckboxList.add(or_checkboxTreatedAcct);
		pestControlServicesCheckboxList.add(or_checkboxTreatedKitchen);
		pestControlServicesCheckboxList.add(or_checkboxTreatedCracks);
		pestControlServicesCheckboxList.add(or_checkboxBasements);
		pestControlServicesCheckboxList.add(or_checkboxTreatedliving);
		pestControlServicesCheckboxList.add(or_checkboxTreatedZoneMonitors);
		pestControlServicesCheckboxList.add(or_checkboxTreatedBedrooms);

		OutsidepestControlServicesCheckboxList.add(or_checkboxEXTERIOR);
		OutsidepestControlServicesCheckboxList.add(or_checkboxTreatedFront);
		OutsidepestControlServicesCheckboxList.add(or_checkboxTreatedback);
		OutsidepestControlServicesCheckboxList.add(or_checkboxTreatedLaundryRooms);
		OutsidepestControlServicesCheckboxList.add(or_checkboxTreatedLivingRooms);
		OutsidepestControlServicesCheckboxList.add(or_checkboxtreatedyards);
		OutsidepestControlServicesCheckboxList.add(or_checkboxTreatedfirenets);
		OutsidepestControlServicesCheckboxList.add(or_checkboxTreatedDoorways);
		OutsidepestControlServicesCheckboxList.add(or_checkboxTreatedCrawlSpace);

		InsidepestControlServicesCheckboxList.add(or_checkboxINTERIOR);
		InsidepestControlServicesCheckboxList.add(or_checkboxTreatedAcct);
		InsidepestControlServicesCheckboxList.add(or_checkboxTreatedKitchen);
		InsidepestControlServicesCheckboxList.add(or_checkboxTreatedCracks);
		InsidepestControlServicesCheckboxList.add(or_checkboxBasements);
		InsidepestControlServicesCheckboxList.add(or_checkboxTreatedZoneMonitors);
		InsidepestControlServicesCheckboxList.add(or_checkboxTreatedliving);
		InsidepestControlServicesCheckboxList.add(or_checkboxTreatedBedrooms);

		InspectedTreatedForCheckboxList.add(or_checkboxRoaches);
		InspectedTreatedForCheckboxList.add(or_checkboxSpiders);
		InspectedTreatedForCheckboxList.add(or_checkboxScorpions);
		InspectedTreatedForCheckboxList.add(or_checkboxSilverfish);
		InspectedTreatedForCheckboxList.add(or_checkboxPANTRY_PESTS);
		InspectedTreatedForCheckboxList.add(or_checkboxPHARAOH_ANTS);
		InspectedTreatedForCheckboxList.add(or_checkboxFIRE_ANTS);
		InspectedTreatedForCheckboxList.add(or_checkboxCARPENTER_ANTS);
		InspectedTreatedForCheckboxList.add(or_checkboxMILLIPEDES);

		InspectedTreatedForCheckboxList.add(or_checkboxEARWIGS);
		InspectedTreatedForCheckboxList.add(or_checkboxPILL_BUGS);
		InspectedTreatedForCheckboxList.add(or_checkboxCRICKETS);
		InspectedTreatedForCheckboxList.add(or_checkboxWASPS);
		InspectedTreatedForCheckboxList.add(or_checkboxSUB_TERMITIES);
		InspectedTreatedForCheckboxList.add(or_checkboxDRYWOOD_TERMITIES);
		InspectedTreatedForCheckboxList.add(or_checkboxROOF_RATS);
		InspectedTreatedForCheckboxList.add(or_checkboxMICE);
		InspectedTreatedForCheckboxList.add(or_checkboxRACCOONS);

		InspectedTreatedForCheckboxList.add(or_checkboxOPOSSUMS);
		InspectedTreatedForCheckboxList.add(or_checkboxSKUNKS);
		InspectedTreatedForCheckboxList.add(or_checkboxSquirrels);
		InspectedTreatedForCheckboxList
				.add(or_checkboxANTS_Inspected_treated_for);
		InspectedTreatedForCheckboxList.add(or_checkboxARGENTINE_ANTS);
		InspectedTreatedForCheckboxList
				.add(or_checkboxOTHER_PESTS_INSPECTED_TREATED_FOR);

		AreasInspectedCheckboxList.add(or_checkboxExteriorHouseAreasInspected);
		AreasInspectedCheckboxList.add(or_checkboxFRONTYARDAreasInspected);
		AreasInspectedCheckboxList.add(or_checkboxBackYardAreasInspected);
		AreasInspectedCheckboxList.add(or_checkboxExteriorPerimeterofGarage);
		AreasInspectedCheckboxList.add(or_checkboxOutBuildings);
		AreasInspectedCheckboxList.add(or_checkboxExteriorHouseAreasInspected);
		AreasInspectedCheckboxList.add(or_checkboxBackYardAreasInspected);
		AreasInspectedCheckboxList.add(or_checkboxFenceLinesAreasInspected);
		AreasInspectedCheckboxList.add(or_checkboxAtticAreasinspected);
		AreasInspectedCheckboxList.add(or_checkboxBASEMENTAreasinspected);
		AreasInspectedCheckboxList.add(or_checkboxCRAWL_SPACEAreasinspected);

		mosquitoCheckboxList.add(or_checkboxTopped_TANK);
		mosquitoCheckboxList.add(or_checkboxTUBING_LEACKY);
		mosquitoCheckboxList.add(or_checkboxRAN_SYATEMS);
		mosquitoCheckboxList.add(or_checkboxCHECKED_RUN_DURACTION_TIME);
		mosquitoCheckboxList.add(or_checkboxLEACKY_TANK);
		mosquitoCheckboxList.add(or_checkboxINSPECTED_NOZZLES);
		mosquitoCheckboxList.add(or_checkboxNOZZLES_LEAKING);
		mosquitoCheckboxList.add(or_checkboxCHECKED_TIME_SYSTEM_RUNS);
		
		treatmentOfSchoolCheckboxList.add(or_checkboxSCHOOL_HOLIDAY);
		treatmentOfSchoolCheckboxList.add(or_checkboxRE_ENTRY_SIGN);
		treatmentOfSchoolCheckboxList.add(or_checkboxWARNING_SIGN);
		treatmentOfSchoolCheckboxList.add(or_checkboxDOORS_LOCKED_AREA_SECURED);
		treatmentOfSchoolCheckboxList.add(or_checkboxNO_CHILDREN_STUDENT_PRESENT);

	}

	private void initialize() {
		// TODO Auto-generated method stub
		houstonFlowFunctions = new HoustonFlowFunctions(getApplicationContext());
		userDTO = (UserDTO) this.getIntent().getExtras().getSerializable("userDTO");
		generalInfoDTO = (GeneralInfoDTO) this.getIntent().getExtras().getSerializable("generalInfoDTO");

		// Log.e(userDTO.getEmailid(), userDTO.getEmailid());
		// Log.e(generalInfoDTO.getEmail_Id(), generalInfoDTO.getEmail_Id());

		// Change By Dinesh
		serviceID_new = this.getIntent().getExtras().getString(ParameterUtil.ServiceID_new);

		imageFrontListView=(HorizontalListView)findViewById(R.id.listImg_front);
		imageFrontListView.setOnItemLongClickListener(new OnGalleryClickFront());
		imageFrontListView.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view,int position, long id)
			{
				Bitmap bmp= listFrontImg.get(position).getImageBitMap();
				if(bmp!=null)
				{
					bitmapFront=bmp;
					Intent intent=new Intent(OR_PestControlServiceReport.this, FullScreenViewActivity.class);
					intent.putExtra("from","frontCheck");
					startActivity(intent);
				}
			}

		});
		
		imageBackListView=(HorizontalListView)findViewById(R.id.listImg_back);
		imageBackListView.setOnItemLongClickListener(new OnGalleryClickBack());
		imageBackListView.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view,int position, long id)
			{
				Bitmap bmp= listBackImg.get(position).getImageBitMap();
				if(bmp!=null)
				{
					bitmapBack=bmp;
					Intent intent=new Intent(OR_PestControlServiceReport.this, FullScreenViewActivity.class);
					intent.putExtra("from","backCheck");
					startActivity(intent);
				}
			}

		});
		
		tramount = (TableRow) findViewById(R.id.tramount);
		trcheckno = (TableRow) findViewById(R.id.trcheckno);
		trDrivingLicence = (TableRow) findViewById(R.id.trDrivingLicence);
		trExpirationDate = (TableRow) findViewById(R.id.trExpirationDate);
		trExpirationDateValidation = (TableRow) findViewById(R.id.trExpirationDateValidation);

		btnCHECK_FRONT_IMAGE = (Button) findViewById(R.id.btnCHECK_FRONT_IMAGE);
		btnCHECK_FRONT_IMAGE.setOnClickListener(new OnButtonClick());
		
		btnCHECK_BACK_IMAGE = (Button) findViewById(R.id.btnCHECK_BACK_IMAGE);
		btnCHECK_BACK_IMAGE.setOnClickListener(new OnButtonClick());
		
		btnsaveandcontinueINVOICE = (Button) findViewById(R.id.btnsaveandcontinueINVOICE);
		btnsaveandcontinueINVOICE.setOnClickListener(new OnButtonClick());
		
		btnCancel = (Button) findViewById(R.id.btnCancel);
		btnCancel.setOnClickListener(new OnButtonClick());
		
		textViewApppVer = (TextView) findViewById(R.id.textViewApppVer);
		txtAccountNumber = (TextView) findViewById(R.id.txtAccountNumber);
		txtOrderNumber = (TextView) findViewById(R.id.txtOrderNumber);

		LinearlayoutCHECK_IMAGE = (LinearLayout) findViewById(R.id.LinearlayoutCHECK_IMAGE);
		LinearlayoutCHECK_FRONT_IMAGE = (LinearLayout) findViewById(R.id.LinearlayoutCHECK_FRONT_IMAGE);
		LinearlayoutCHECK_BACK_IMAGE = (LinearLayout) findViewById(R.id.LinearlayoutCHECK_BACK_IMAGE);

		edtFenceLinesByZone = (EditText) findViewById(R.id.edtFenceLinesByZone);
		edtFenceLinesByZone.addTextChangedListener(new CustomTextWatcher(edtFenceLinesByZone));
		edtFenceLinesByZone.setFilters(CommonFunction.limitchars(50));

		edtBackyardByZone = (EditText) findViewById(R.id.edtBackyardByZone);
		edtBackyardByZone.addTextChangedListener(new CustomTextWatcher(edtBackyardByZone));
		edtBackyardByZone.setFilters(CommonFunction.limitchars(50));
		
		edtFrontYardByZone = (EditText) findViewById(R.id.edtFrontYardByZone);
		edtFrontYardByZone.addTextChangedListener(new CustomTextWatcher(edtFrontYardByZone));
		edtFrontYardByZone.setFilters(CommonFunction.limitchars(50));
		
		edtExteriorParaByZone = (EditText) findViewById(R.id.edtExteriorParaByZone);
		edtExteriorParaByZone.addTextChangedListener(new CustomTextWatcher(edtExteriorParaByZone));
		edtExteriorParaByZone.setFilters(CommonFunction.limitchars(50));
		
		edtRoofGutterByZone = (EditText) findViewById(R.id.edtRoofGutterByZone);
		edtRoofGutterByZone.addTextChangedListener(new CustomTextWatcher(edtRoofGutterByZone));
		edtRoofGutterByZone.setFilters(CommonFunction.limitchars(50));
		
		edtGarageByZone = (EditText) findViewById(R.id.edtGarageByZone);
		edtGarageByZone.addTextChangedListener(new CustomTextWatcher(edtGarageByZone));
		edtGarageByZone.setFilters(CommonFunction.limitchars(50));

		
		edtAtticPestByZone = (EditText) findViewById(R.id.edtAtticPestByZone);
		edtAtticPestByZone.addTextChangedListener(new CustomTextWatcher(edtAtticPestByZone));
		edtAtticPestByZone.setFilters(CommonFunction.limitchars(50));
		
		edtInteriorHouseByZone = (EditText) findViewById(R.id.edtInteriorHouseByZone);
		edtInteriorHouseByZone.addTextChangedListener(new CustomTextWatcher(edtInteriorHouseByZone));
		edtInteriorHouseByZone.setFilters(CommonFunction.limitchars(50));
		
		edt_Basement_PestActivityByZone = (EditText) findViewById(R.id.edt_Basement_PestActivityByZone);
		edt_Basement_PestActivityByZone.addTextChangedListener(new CustomTextWatcher(edt_Basement_PestActivityByZone));
		edt_Basement_PestActivityByZone.setFilters(CommonFunction.limitchars(50));
		
		edtNUMBER_OF_BAITING_DEVICES_INSPECTED = (EditText) findViewById(R.id.edtNUMBER_OF_BAITING_DEVICES_INSPECTED);
		edtNUMBER_OF_BAITING_DEVICES_INSPECTED.addTextChangedListener(new CustomTextWatcher(edtNUMBER_OF_BAITING_DEVICES_INSPECTED));
		
		edt_Basement_TermiteServices = (EditText) findViewById(R.id.edt_Basement_TermiteServices);
		edt_Basement_TermiteServices.addTextChangedListener(new CustomTextWatcher(edt_Basement_TermiteServices));
		
		edtAdditionalDeviceAdded = (EditText) findViewById(R.id.edtAdditionalDeviceAdded);
		edtAdditionalDeviceAdded.addTextChangedListener(new CustomTextWatcher(edtAdditionalDeviceAdded));
		
		edtNumberDeviceRemoved = (EditText) findViewById(R.id.edtNumberDeviceRemoved);
		edtNumberDeviceRemoved.addTextChangedListener(new CustomTextWatcher(edtNumberDeviceRemoved));
		
		edtNO_LIVEANIMAL_TRAPS_INSTALLES = (EditText) findViewById(R.id.edtNO_LIVEANIMAL_TRAPS_INSTALLES);
		edtNO_LIVEANIMAL_TRAPS_INSTALLES.addTextChangedListener(new CustomTextWatcher(edtNO_LIVEANIMAL_TRAPS_INSTALLES));
		
		edtNumber_SNAPTRAPS = (EditText) findViewById(R.id.edtNumber_SNAPTRAPS);
		edtNumber_SNAPTRAPS.addTextChangedListener(new CustomTextWatcher(edtNumber_SNAPTRAPS));
		
		edtMosquitoService1 = (EditText) findViewById(R.id.edtMosquitoService1);
		edtMosquitoService1.addTextChangedListener(new CustomTextWatcher(edtMosquitoService1));
		
		edtMosquitoService2 = (EditText) findViewById(R.id.edtMosquitoService2);
		edtMosquitoService2.addTextChangedListener(new CustomTextWatcher(edtMosquitoService2));
		
		edttxtInspectedandTreatedFor2 = (EditText) findViewById(R.id.edttxtInspectedandTreatedFor2);
		edttxtInspectedandTreatedFor2.addTextChangedListener(new CustomTextWatcher(edttxtInspectedandTreatedFor2));
		edttxtInspectedandTreatedFor2.setFilters(CommonFunction.limitchars(20));
		
		edttxtInspectedandTreatedFor1 = (EditText) findViewById(R.id.edttxtInspectedandTreatedFor1);
		edttxtInspectedandTreatedFor1.addTextChangedListener(new CustomTextWatcher(edttxtInspectedandTreatedFor1));
		edttxtInspectedandTreatedFor1.setFilters(CommonFunction.limitchars(20));
		
		edtTechComment = (EditText) findViewById(R.id.edtTechComment);
		edtTechComment.addTextChangedListener(new CustomTextWatcher(edtTechComment));
		edtTechComment.setFilters(CommonFunction.limitchars(400));
		
		
		edtServiceTech = (EditText) findViewById(R.id.edtServiceTech);
		edtServiceTech.setFilters(CommonFunction.limitchars(50));
		
		edtEMP = (EditText) findViewById(R.id.edtEMP);
		edtEMP.setFilters(CommonFunction.limitchars(50));
		
		edtCustomer = (EditText) findViewById(R.id.edtCustomer);
		edtCustomer.setFilters(CommonFunction.limitchars(50));
		edtCustomer.setEnabled(false);
		
		edtDate = (EditText) findViewById(R.id.edtDate);
		edtDate.setInputType(InputType.TYPE_NULL);
		edtDate.setOnClickListener(new OnButtonClick());		
		
		edtIntValue = (EditText) findViewById(R.id.edtIntValue);
		edtProductvalue = (EditText) findViewById(R.id.edtProductvalue);
		edtTaxvalue = (EditText) findViewById(R.id.edtTaxvalue);
		edtTotal = (EditText) findViewById(R.id.edtTotal);
		
		edtAmount = (EditText) findViewById(R.id.edtAmount);
		edtAmount.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(8,2)});
		
		edtCheckNo = (EditText) findViewById(R.id.edtCheckNo);
		edtCheckNo.setFilters(CommonFunction.limitchars(20));
		
		edtDRIVING_LICENCE = (EditText) findViewById(R.id.edtDRIVING_LICENCE);
		edtDRIVING_LICENCE.setFilters(CommonFunction.limitchars(50));
		
		edtEXPIRATION_DATE = (EditText) findViewById(R.id.edtEXPIRATION_DATE);
		edtEXPIRATION_DATE.setInputType(InputType.TYPE_NULL);
		edtEXPIRATION_DATE.setOnClickListener(new OnButtonClick());
		
		radioGroupInvoice = (RadioGroup) findViewById(R.id.radioGroupInvoice);
		radioGroupInvoice.setOnCheckedChangeListener(new OnRadioChange());
		
		radioGroupFenceInspected = (RadioGroup) findViewById(R.id.radioGroupFenceInspected);
		radioGroupFenceInspected.setOnCheckedChangeListener(new OnRadioChange());
		
		radioGroupBaitingDevices = (RadioGroup) findViewById(R.id.radioGroupBaitingDevices);
		radioGroupBaitingDevices.setOnCheckedChangeListener(new OnRadioChange());
		
		radioGroupTERMITE_BAITING_DEVICES_REPLACED = (RadioGroup) findViewById(R.id.radioGroupTERMITE_BAITING_DEVICES_REPLACED);
		radioGroupTERMITE_BAITING_DEVICES_REPLACED.setOnCheckedChangeListener(new OnRadioChange());
		
		radioGroupExteriorPerimeterofhouse = (RadioGroup) findViewById(R.id.radioGroupExteriorPerimeterofhouse);
		radioGroupExteriorPerimeterofhouse.setOnCheckedChangeListener(new OnRadioChange());
		
		radioGroupAttic = (RadioGroup) findViewById(R.id.radioGroupAttic);
		radioGroupAttic.setOnCheckedChangeListener(new OnRadioChange());
		
		radioGroupINTERIOROFHOUSE = (RadioGroup) findViewById(R.id.radioGroupINTERIOROFHOUSE);
		radioGroupINTERIOROFHOUSE.setOnCheckedChangeListener(new OnRadioChange());
		
		radioGroupBUILDERS_CONTRUCTION_GAP = (RadioGroup) findViewById(R.id.radioGroupBUILDERS_CONTRUCTION_GAP);
		radioGroupBUILDERS_CONTRUCTION_GAP.setOnCheckedChangeListener(new OnRadioChange());
		
		radioGroupSOFFIT_RETURNS = (RadioGroup) findViewById(R.id.radioGroupSOFFIT_RETURNS);
		radioGroupSOFFIT_RETURNS.setOnCheckedChangeListener(new OnRadioChange());
		
		radioGroupBREEZYWAY_FROM_GARAGE = (RadioGroup) findViewById(R.id.radioGroupBREEZYWAY_FROM_GARAGE);
		radioGroupBREEZYWAY_FROM_GARAGE.setOnCheckedChangeListener(new OnRadioChange());
		
		radioGroupROOF_VENTS = (RadioGroup) findViewById(R.id.radioGroupROOF_VENTS);
		radioGroupROOF_VENTS.setOnCheckedChangeListener(new OnRadioChange());
		
		radioGroupGARAGE_DOOR = (RadioGroup) findViewById(R.id.radioGroupGARAGE_DOOR);
		radioGroupGARAGE_DOOR.setOnCheckedChangeListener(new OnRadioChange());
		
		radioGroupSOFFIT_VENTS = (RadioGroup) findViewById(R.id.radioGroupSOFFIT_VENTS);
		radioGroupSOFFIT_VENTS.setOnCheckedChangeListener(new OnRadioChange());
		
		radioGroupAC_DRYER_VENTS = (RadioGroup) findViewById(R.id.radioGroupAC_DRYER_VENTS);
		radioGroupAC_DRYER_VENTS.setOnCheckedChangeListener(new OnRadioChange());
		
		radioGroupSIDEING_HOUSE = (RadioGroup) findViewById(R.id.radioGroupSIDEING_HOUSE);
		radioGroupSIDEING_HOUSE.setOnCheckedChangeListener(new OnRadioChange());
		
		radioGroupCRAWL_SPACE_VENTS = (RadioGroup) findViewById(R.id.radioGroupCRAWL_SPACE_VENTS);
		radioGroupCRAWL_SPACE_VENTS.setOnCheckedChangeListener(new OnRadioChange());
		
		radioGroupCRAWL_SPACE_DOOR = (RadioGroup) findViewById(R.id.radioGroupCRAWL_SPACE_DOOR);
		radioGroupCRAWL_SPACE_DOOR.setOnCheckedChangeListener(new OnRadioChange());
		
		radioCash = (RadioButton) findViewById(R.id.radioCash);
		radioCheck = (RadioButton) findViewById(R.id.radioCheck);
		radioCreditcard = (RadioButton) findViewById(R.id.radioCreditcard);
		radioBillLater = (RadioButton) findViewById(R.id.radioBillLater);
		radioPreBill = (RadioButton) findViewById(R.id.radioPreBill);
		radioNocharge = (RadioButton) findViewById(R.id.radioNocharge);
		radiobtnFenceInspected1 = (RadioButton) findViewById(R.id.radiobtnFenceInspected1);
		radiobtnFenceInspected2 = (RadioButton) findViewById(R.id.radiobtnFenceInspected2);
		radiobtnBaitingDevices1 = (RadioButton) findViewById(R.id.radiobtnBaitingDevices1);
		radiobtnBaitingDevices2 = (RadioButton) findViewById(R.id.radiobtnBaitingDevices2);
		radiobtnTERMITE_BAITING_DEVICES_REPLACED1 = (RadioButton) findViewById(R.id.radiobtnTERMITE_BAITING_DEVICES_REPLACED1);
		radiobtnTERMITE_BAITING_DEVICES_REPLACED2 = (RadioButton) findViewById(R.id.radiobtnTERMITE_BAITING_DEVICES_REPLACED2);
		radiobtnExteriorPerimeterofhouse1 = (RadioButton) findViewById(R.id.radiobtnExteriorPerimeterofhouse1);
		radiobtnExteriorPerimeterofhouse2 = (RadioButton) findViewById(R.id.radiobtnExteriorPerimeterofhouse2);
		radiobtnAttic1 = (RadioButton) findViewById(R.id.radiobtnAttic1);
		radiobtnAttic2 = (RadioButton) findViewById(R.id.radiobtnAttic2);
		radiobtnINTERIOROFHOUSE1 = (RadioButton) findViewById(R.id.radiobtnINTERIOROFHOUSE1);
		radiobtnINTERIOROFHOUSE2 = (RadioButton) findViewById(R.id.radiobtnINTERIOROFHOUSE2);
		radiobtnBUILDERS_CONTRUCTION_GAP1 = (RadioButton) findViewById(R.id.radiobtnBUILDERS_CONTRUCTION_GAP1);
		radiobtnBUILDERS_CONTRUCTION_GAP2 = (RadioButton) findViewById(R.id.radiobtnBUILDERS_CONTRUCTION_GAP2);
		radiobtnSOFFIT_RETURNS1 = (RadioButton) findViewById(R.id.radiobtnSOFFIT_RETURNS1);
		radiobtnSOFFIT_RETURNS2 = (RadioButton) findViewById(R.id.radiobtnSOFFIT_RETURNS2);
		radiobtnBREEZYWAY_FROM_GARAGE1 = (RadioButton) findViewById(R.id.radiobtnBREEZYWAY_FROM_GARAGE1);
		radiobtnBREEZYWAY_FROM_GARAGE2 = (RadioButton) findViewById(R.id.radiobtnBREEZYWAY_FROM_GARAGE2);
		radiobtnROOF_VENTS1 = (RadioButton) findViewById(R.id.radiobtnROOF_VENTS1);
		radiobtnROOF_VENTS2 = (RadioButton) findViewById(R.id.radiobtnROOF_VENTS2);
		radiobtnGARAGE_DOOR1 = (RadioButton) findViewById(R.id.radiobtnGARAGE_DOOR1);
		radiobtnGARAGE_DOOR2 = (RadioButton) findViewById(R.id.radiobtnGARAGE_DOOR2);
		radiobtnSOFFIT_VENTS1 = (RadioButton) findViewById(R.id.radiobtnSOFFIT_VENTS1);
		radiobtnSOFFIT_VENTS2 = (RadioButton) findViewById(R.id.radiobtnSOFFIT_VENTS2);
		radiobtnAC_DRYER_VENTSR1 = (RadioButton) findViewById(R.id.radiobtnAC_DRYER_VENTSR1);
		radiobtnAC_DRYER_VENTSR2 = (RadioButton) findViewById(R.id.radiobtnAC_DRYER_VENTSR2);
		radiobtnSIDEING_HOUSE1 = (RadioButton) findViewById(R.id.radiobtnSIDEING_HOUSE1);
		radiobtnSIDEING_HOUSE2 = (RadioButton) findViewById(R.id.radiobtnSIDEING_HOUSE2);
		radiobtnCRAWL_SPACE_VENTS1 = (RadioButton) findViewById(R.id.radiobtnCRAWL_SPACE_VENTS1);
		radiobtnCRAWL_SPACE_VENTS2 = (RadioButton) findViewById(R.id.radiobtnCRAWL_SPACE_VENTS2);
		radiobtnCRAWL_SPACE_DOOR1 = (RadioButton) findViewById(R.id.radiobtnCRAWL_SPACE_DOOR1);
		radiobtnCRAWL_SPACE_DOOR2 = (RadioButton) findViewById(R.id.radiobtnCRAWL_SPACE_DOOR2);

		or_checkboxSignaturePestcontrol = (CheckBox) findViewById(R.id.or_checkboxSignaturePestcontrol);
		or_checkboxSignaturePestcontrol.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
			
		or_checkboxPlusTermites = (CheckBox) findViewById(R.id.or_checkboxPlusTermites);
		or_checkboxPlusTermites.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxPlusMosquitomistingSystem = (CheckBox) findViewById(R.id.or_checkboxPlusMosquitomistingSystem);
		or_checkboxPlusMosquitomistingSystem.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxPlusFireAnts = (CheckBox) findViewById(R.id.or_checkboxPlusFireAnts);
		or_checkboxPlusFireAnts.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxPluSRodents = (CheckBox) findViewById(R.id.or_checkboxPluSRodents);
		or_checkboxPluSRodents.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxPlusFleas = (CheckBox) findViewById(R.id.or_checkboxPlusFleas);
		or_checkboxPlusFleas.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxTermiteTreatment = (CheckBox) findViewById(R.id.or_checkboxTermiteTreatment);
		or_checkboxTermiteTreatment.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxRodentExclusion = (CheckBox) findViewById(R.id.or_checkboxRodentExclusion);
		or_checkboxRodentExclusion.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxBedBugTreatment = (CheckBox) findViewById(R.id.or_checkboxBedBugTreatment);
		or_checkboxBedBugTreatment.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxOutSideService = (CheckBox) findViewById(R.id.or_checkboxOutSideService);
		or_checkboxOutSideService.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxFenceLines = (CheckBox) findViewById(R.id.or_checkboxFenceLines);
		or_checkboxFenceLines.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxBackYardByZone = (CheckBox) findViewById(R.id.or_checkboxBackYardByZone);
		or_checkboxBackYardByZone.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxFrontyardByZOne = (CheckBox) findViewById(R.id.or_checkboxFrontyardByZOne);
		or_checkboxFrontyardByZOne.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxExteriorParametresofhouseByZone = (CheckBox) findViewById(R.id.or_checkboxExteriorParametresofhouseByZone);
		or_checkboxExteriorParametresofhouseByZone.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxRofGutter = (CheckBox) findViewById(R.id.or_checkboxRofGutter);
		or_checkboxRofGutter.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxGarage = (CheckBox) findViewById(R.id.or_checkboxGarage);
		or_checkboxGarage.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxAtticByZone = (CheckBox) findViewById(R.id.or_checkboxAtticByZone);
		or_checkboxAtticByZone.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxInteriorHouseByZone = (CheckBox) findViewById(R.id.or_checkboxInteriorHouseByZone);
		or_checkboxInteriorHouseByZone.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkbox_Basement_PestActivityZone = (CheckBox) findViewById(R.id.or_checkbox_Basement_PestActivityZone);
		or_checkbox_Basement_PestActivityZone.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxINTERIOR = (CheckBox) findViewById(R.id.or_checkboxINTERIOR);
		or_checkboxINTERIOR.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxTreatedAcct = (CheckBox) findViewById(R.id.or_checkboxTreatedAcct);
		or_checkboxTreatedAcct.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxTreatedKitchen = (CheckBox) findViewById(R.id.or_checkboxTreatedKitchen);
		or_checkboxTreatedKitchen.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxTreatedCracks = (CheckBox) findViewById(R.id.or_checkboxTreatedCracks);
		or_checkboxTreatedCracks.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
				
		or_checkboxBasements = (CheckBox) findViewById(R.id.or_checkboxBasements);
		or_checkboxBasements.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxTreatedliving = (CheckBox) findViewById(R.id.or_checkboxTreatedliving);
		or_checkboxTreatedliving.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
	
		or_checkboxTreatedZoneMonitors = (CheckBox) findViewById(R.id.or_checkboxTreatedZoneMonitors);
		or_checkboxTreatedZoneMonitors.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxTreatedBedrooms = (CheckBox) findViewById(R.id.or_checkboxTreatedBedrooms);
		or_checkboxTreatedBedrooms.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxEXTERIOR = (CheckBox) findViewById(R.id.or_checkboxEXTERIOR);
		or_checkboxEXTERIOR.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxTreatedFront = (CheckBox) findViewById(R.id.or_checkboxTreatedFront);
		or_checkboxTreatedFront.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxTreatedback = (CheckBox) findViewById(R.id.or_checkboxTreatedback);
		or_checkboxTreatedback.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxTreatedLaundryRooms = (CheckBox) findViewById(R.id.or_checkboxTreatedLaundryRooms);
		or_checkboxTreatedLaundryRooms.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxTreatedLivingRooms = (CheckBox) findViewById(R.id.or_checkboxTreatedLivingRooms);
		or_checkboxTreatedLivingRooms.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxtreatedyards = (CheckBox) findViewById(R.id.or_checkboxtreatedyards);
		or_checkboxtreatedyards.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxTreatedfirenets = (CheckBox) findViewById(R.id.or_checkboxTreatedfirenets);
		or_checkboxTreatedfirenets.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxTreatedDoorways = (CheckBox) findViewById(R.id.or_checkboxTreatedDoorways);
		or_checkboxTreatedDoorways.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxTreatedCrawlSpace = (CheckBox) findViewById(R.id.or_checkboxTreatedCrawlSpace);
		or_checkboxTreatedCrawlSpace.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxFenceInspectedTermite = (CheckBox) findViewById(R.id.or_checkboxFenceInspectedTermite);
		or_checkboxFenceInspectedTermite.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxNumberBaitingDevicesInspectedTermite = (CheckBox) findViewById(R.id.or_checkboxNumberBaitingDevicesInspectedTermite);
		or_checkboxNumberBaitingDevicesInspectedTermite.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxBaitingDevicesTermite = (CheckBox) findViewById(R.id.or_checkboxBaitingDevicesTermite);
		or_checkboxBaitingDevicesTermite.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxTermiteBaitingDevicereplacedTermite = (CheckBox) findViewById(R.id.or_checkboxTermiteBaitingDevicereplacedTermite);
		or_checkboxTermiteBaitingDevicereplacedTermite.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxExteriorPerimeterofhouseTermite = (CheckBox) findViewById(R.id.or_checkboxExteriorPerimeterofhouseTermite);
		or_checkboxExteriorPerimeterofhouseTermite.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxATTICTermite = (CheckBox) findViewById(R.id.or_checkboxATTICTermite);
		or_checkboxATTICTermite.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxINTERIOROFHOUSETermite = (CheckBox) findViewById(R.id.or_checkboxINTERIOROFHOUSETermite);
		or_checkboxINTERIOROFHOUSETermite.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkbox_Basement_Termite = (CheckBox) findViewById(R.id.or_checkbox_Basement_Termite);
		or_checkbox_Basement_Termite.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxAdditionalDeviceAddedTermite = (CheckBox) findViewById(R.id.or_checkboxAdditionalDeviceAddedTermite);
		or_checkboxAdditionalDeviceAddedTermite.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxNumberDeviceRemovedTermite = (CheckBox) findViewById(R.id.or_checkboxNumberDeviceRemovedTermite);
		or_checkboxNumberDeviceRemovedTermite.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxBUILDERS_CONTRUCTION_GAP = (CheckBox) findViewById(R.id.or_checkboxBUILDERS_CONTRUCTION_GAP);
		or_checkboxBUILDERS_CONTRUCTION_GAP.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxSOFFIT_RETURNS = (CheckBox) findViewById(R.id.or_checkboxSOFFIT_RETURNS);
		or_checkboxSOFFIT_RETURNS.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxBREEZYWAY_FROM_GARAGE = (CheckBox) findViewById(R.id.or_checkboxBREEZYWAY_FROM_GARAGE);
		or_checkboxBREEZYWAY_FROM_GARAGE.setOnCheckedChangeListener(new OnCheckedboxChangeListener());or_checkboxROOF_VENTS = (CheckBox) findViewById(R.id.or_checkboxROOF_VENTS);
		
		or_checkboxGARAGE_DOOR = (CheckBox) findViewById(R.id.or_checkboxGARAGE_DOOR);
		or_checkboxGARAGE_DOOR.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxSOFFIT_VENTS = (CheckBox) findViewById(R.id.or_checkboxSOFFIT_VENTS);
		or_checkboxSOFFIT_VENTS.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxAC_DRYER_VENTS = (CheckBox) findViewById(R.id.or_checkboxAC_DRYER_VENTS);
		or_checkboxAC_DRYER_VENTS.setOnCheckedChangeListener(new OnCheckedboxChangeListener());or_checkboxNO_LIVEANIMAL_TRAPS_INSTALLES = (CheckBox) findViewById(R.id.or_checkboxNO_LIVEANIMAL_TRAPS_INSTALLES);
		
		or_checkboxSIDEING_HOUSE = (CheckBox) findViewById(R.id.or_checkboxSIDEING_HOUSE);
		or_checkboxSIDEING_HOUSE.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxNumber_SNAPTRAPS = (CheckBox) findViewById(R.id.or_checkboxNumber_SNAPTRAPS);
		or_checkboxNumber_SNAPTRAPS.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxCRAWL_SPACE_VENTS = (CheckBox) findViewById(R.id.or_checkboxCRAWL_SPACE_VENTS);
		or_checkboxCRAWL_SPACE_VENTS.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxCRAWL_SPACE_DOOR = (CheckBox) findViewById(R.id.or_checkboxCRAWL_SPACE_DOOR);
		or_checkboxCRAWL_SPACE_DOOR.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxTopped_TANK = (CheckBox) findViewById(R.id.or_checkboxTopped_TANK);
		or_checkboxTopped_TANK.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxLEACKY_TANK = (CheckBox) findViewById(R.id.or_checkboxLEACKY_TANK);
		or_checkboxLEACKY_TANK.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxTUBING_LEACKY = (CheckBox) findViewById(R.id.or_checkboxTUBING_LEACKY);
		or_checkboxTUBING_LEACKY.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxINSPECTED_NOZZLES = (CheckBox) findViewById(R.id.or_checkboxINSPECTED_NOZZLES);
		or_checkboxINSPECTED_NOZZLES.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxRAN_SYATEMS = (CheckBox) findViewById(R.id.or_checkboxRAN_SYATEMS);
		or_checkboxRAN_SYATEMS.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxNOZZLES_LEAKING = (CheckBox) findViewById(R.id.or_checkboxNOZZLES_LEAKING);
		or_checkboxNOZZLES_LEAKING.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxCHECKED_RUN_DURACTION_TIME = (CheckBox) findViewById(R.id.or_checkboxCHECKED_RUN_DURACTION_TIME);
		or_checkboxCHECKED_RUN_DURACTION_TIME.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxCHECKED_TIME_SYSTEM_RUNS = (CheckBox) findViewById(R.id.or_checkboxCHECKED_TIME_SYSTEM_RUNS);
		or_checkboxCHECKED_TIME_SYSTEM_RUNS.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxBranchHouse = (CheckBox) findViewById(R.id.or_checkboxBranchHouse);
		or_checkboxBranchHouse.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxFirewood = (CheckBox) findViewById(R.id.or_checkboxFirewood);
		or_checkboxFirewood.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxDebrisCrawl = (CheckBox) findViewById(R.id.or_checkboxDebrisCrawl);
		or_checkboxDebrisCrawl.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxExcessiveplant = (CheckBox) findViewById(R.id.or_checkboxExcessiveplant);
		or_checkboxExcessiveplant.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
	
		or_checkboxSoilAbove = (CheckBox) findViewById(R.id.or_checkboxSoilAbove);
		or_checkboxSoilAbove.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxWoodSoil = (CheckBox) findViewById(R.id.or_checkboxWoodSoil);
		or_checkboxWoodSoil.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxDebrisRoof = (CheckBox) findViewById(R.id.or_checkboxDebrisRoof);
		or_checkboxDebrisRoof.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxStandingwater = (CheckBox) findViewById(R.id.or_checkboxStandingwater);
		or_checkboxStandingwater.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxMoistureProblem = (CheckBox) findViewById(R.id.or_checkboxMoistureProblem);
		or_checkboxMoistureProblem.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxOpeningspumbing = (CheckBox) findViewById(R.id.or_checkboxOpeningspumbing);
		or_checkboxOpeningspumbing.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxExcessiveGaps = (CheckBox) findViewById(R.id.or_checkboxExcessiveGaps);
		or_checkboxExcessiveGaps.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxLeackPumbing = (CheckBox) findViewById(R.id.or_checkboxLeackPumbing);
		or_checkboxLeackPumbing.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxgarbageCans = (CheckBox) findViewById(R.id.or_checkboxgarbageCans);
		or_checkboxgarbageCans.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxMoisrtureDamage = (CheckBox) findViewById(R.id.or_checkboxMoisrtureDamage);
		or_checkboxMoisrtureDamage.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxGroceryBags = (CheckBox) findViewById(R.id.or_checkboxGroceryBags);
		or_checkboxGroceryBags.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxPetFood = (CheckBox) findViewById(R.id.or_checkboxPetFood);
		or_checkboxPetFood.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxRecycledItems = (CheckBox) findViewById(R.id.or_checkboxRecycledItems);
		or_checkboxRecycledItems.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxExcessiveStorage = (CheckBox) findViewById(R.id.or_checkboxExcessiveStorage);
		or_checkboxExcessiveStorage.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxSCHOOL_HOLIDAY = (CheckBox) findViewById(R.id.or_checkboxSCHOOL_HOLIDAY);
		or_checkboxSCHOOL_HOLIDAY.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxRE_ENTRY_SIGN = (CheckBox) findViewById(R.id.or_checkboxRE_ENTRY_SIGN);
		or_checkboxRE_ENTRY_SIGN.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxWARNING_SIGN = (CheckBox) findViewById(R.id.or_checkboxWARNING_SIGN);
		or_checkboxWARNING_SIGN.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxDOORS_LOCKED_AREA_SECURED = (CheckBox) findViewById(R.id.or_checkboxDOORS_LOCKED_AREA_SECURED);
		or_checkboxDOORS_LOCKED_AREA_SECURED.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxNO_CHILDREN_STUDENT_PRESENT = (CheckBox) findViewById(R.id.or_checkboxNO_CHILDREN_STUDENT_PRESENT);
		or_checkboxNO_CHILDREN_STUDENT_PRESENT.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxRoaches = (CheckBox) findViewById(R.id.or_checkboxRoaches);
		or_checkboxRoaches.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxSpiders = (CheckBox) findViewById(R.id.or_checkboxSpiders);
		or_checkboxSpiders.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxScorpions = (CheckBox) findViewById(R.id.or_checkboxScorpions);
		or_checkboxScorpions.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxSilverfish = (CheckBox) findViewById(R.id.or_checkboxSilverfish);
		or_checkboxSilverfish.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxPANTRY_PESTS = (CheckBox) findViewById(R.id.or_checkboxPANTRY_PESTS);
		or_checkboxPANTRY_PESTS.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxPHARAOH_ANTS = (CheckBox) findViewById(R.id.or_checkboxPHARAOH_ANTS);
		or_checkboxPHARAOH_ANTS.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxFIRE_ANTS = (CheckBox) findViewById(R.id.or_checkboxFIRE_ANTS);
		or_checkboxFIRE_ANTS.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxCARPENTER_ANTS = (CheckBox) findViewById(R.id.or_checkboxCARPENTER_ANTS);
		or_checkboxCARPENTER_ANTS.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxMILLIPEDES = (CheckBox) findViewById(R.id.or_checkboxMILLIPEDES);
		or_checkboxMILLIPEDES.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxEARWIGS = (CheckBox) findViewById(R.id.or_checkboxEARWIGS);
		or_checkboxEARWIGS.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxPILL_BUGS = (CheckBox) findViewById(R.id.or_checkboxPILL_BUGS);
		or_checkboxPILL_BUGS.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
				
		or_checkboxCRICKETS = (CheckBox) findViewById(R.id.or_checkboxCRICKETS);
		or_checkboxCRICKETS.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxWASPS = (CheckBox) findViewById(R.id.or_checkboxWASPS);
		or_checkboxWASPS.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxSUB_TERMITIES = (CheckBox) findViewById(R.id.or_checkboxSUB_TERMITIES);
		or_checkboxSUB_TERMITIES.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxDRYWOOD_TERMITIES = (CheckBox) findViewById(R.id.or_checkboxDRYWOOD_TERMITIES);
		or_checkboxDRYWOOD_TERMITIES.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxROOF_RATS = (CheckBox) findViewById(R.id.or_checkboxROOF_RATS);
		or_checkboxROOF_RATS.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxMICE = (CheckBox) findViewById(R.id.or_checkboxMICE);
		or_checkboxMICE.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxRACCOONS = (CheckBox) findViewById(R.id.or_checkboxRACCOONS);
		or_checkboxRACCOONS.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxOPOSSUMS = (CheckBox) findViewById(R.id.or_checkboxOPOSSUMS);
		or_checkboxOPOSSUMS.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxSKUNKS = (CheckBox) findViewById(R.id.or_checkboxSKUNKS);
		or_checkboxSKUNKS.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxSquirrels = (CheckBox) findViewById(R.id.or_checkboxSquirrels);
		or_checkboxSquirrels.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxANTS_Inspected_treated_for = (CheckBox) findViewById(R.id.or_checkboxANTS_Inspected_treated_for);
		or_checkboxANTS_Inspected_treated_for.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxARGENTINE_ANTS = (CheckBox) findViewById(R.id.or_checkboxARGENTINE_ANTS);
		or_checkboxARGENTINE_ANTS.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		or_checkboxOTHER_PESTS_INSPECTED_TREATED_FOR = (CheckBox) findViewById(R.id.or_checkboxOTHER_PESTS_INSPECTED_TREATED_FOR);
		or_checkboxOTHER_PESTS_INSPECTED_TREATED_FOR.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
	}
	
	public class OnCheckedboxChangeListener implements android.widget.CompoundButton.OnCheckedChangeListener
	{
		@Override
		public void onCheckedChanged(CompoundButton buttonView,boolean isChecked)
		{
			// TODO Auto-generated method stub
			switch (buttonView.getId()) 
			{
			case R.id.or_checkboxANTS_Inspected_treated_for:
				if(!or_checkboxANTS_Inspected_treated_for.isChecked())
				{
					edttxtInspectedandTreatedFor1.getText().clear();
				}
				else
				{
					if(checkChange==1)
					{
						edttxtInspectedandTreatedFor1.requestFocus();
					}
				}
				break;
			case R.id.or_checkboxOTHER_PESTS_INSPECTED_TREATED_FOR:
				if(!or_checkboxOTHER_PESTS_INSPECTED_TREATED_FOR.isChecked())
				{
					edttxtInspectedandTreatedFor2.getText().clear();
				}
				else
				{
					if(checkChange==1)
					{
						edttxtInspectedandTreatedFor2.requestFocus();
					}
				}
				break;
			case R.id.or_checkboxCHECKED_RUN_DURACTION_TIME:
				if(!or_checkboxCHECKED_RUN_DURACTION_TIME.isChecked())
				{
					edtMosquitoService1.getText().clear();
				}
				else
				{
					if(checkChange==1)
					{
						edtMosquitoService1.requestFocus();
					}
				}
				break;
			case R.id.or_checkboxCHECKED_TIME_SYSTEM_RUNS:
				if(!or_checkboxCHECKED_TIME_SYSTEM_RUNS.isChecked())
				{
					edtMosquitoService1.getText().clear();
				}
				else
				{
					if(checkChange==1)
					{
						edtMosquitoService1.requestFocus();
					}
				}
				break;
			case R.id.or_checkboxFenceLines:
				if(!or_checkboxFenceLines.isChecked())
				{
					edtFenceLinesByZone.getText().clear();
				}
				else
				{
					if(checkChange==1)
					{
						edtFenceLinesByZone.requestFocus();
					}
				}
				break;
			case R.id.or_checkboxBackYardByZone:
				if(!or_checkboxBackYardByZone.isChecked())
				{
					edtBackyardByZone.getText().clear();
				}
				else
				{
					if(checkChange==1)
					{
						edtBackyardByZone.requestFocus();
					}
				}
				break;
			case R.id.or_checkboxFrontyardByZOne:
				if(!or_checkboxFrontyardByZOne.isChecked())
				{
					edtFrontYardByZone.getText().clear();
				}
				else
				{
					if(checkChange==1)
					{
						edtFrontYardByZone.requestFocus();
					}
				}
				break;
			case R.id.or_checkboxExteriorParametresofhouseByZone:
				if(!or_checkboxExteriorParametresofhouseByZone.isChecked())
				{
					edtExteriorParaByZone.getText().clear();
				}
				else
				{
					if(checkChange==1)
					{
						edtExteriorParaByZone.requestFocus();
					}
				}
				break;
			case R.id.or_checkboxRofGutter:
				if(!or_checkboxRofGutter.isChecked())
				{
					edtRoofGutterByZone.getText().clear();
				}
				else
				{
					if(checkChange==1)
					{
						edtRoofGutterByZone.requestFocus();
					}
				}
				break;
			case R.id.or_checkboxGarage:
				if(!or_checkboxGarage.isChecked())
				{
					edtGarageByZone.getText().clear();
				}
				else
				{
					if(checkChange==1)
					{
						edtGarageByZone.requestFocus();
					}
				}
				break;
			case R.id.or_checkboxAtticByZone:
				if(!or_checkboxAtticByZone.isChecked())
				{
					edtAtticPestByZone.getText().clear();
				}
				else
				{
					if(checkChange==1)
					{
						edtAtticPestByZone.requestFocus();
					}
				}
				break;
			case R.id.or_checkboxInteriorHouseByZone:
				if(!or_checkboxInteriorHouseByZone.isChecked())
				{
					edtInteriorHouseByZone.getText().clear();
				}
				else
				{
					if(checkChange==1)
					{
						edtInteriorHouseByZone.requestFocus();
					}
				}
				break;
			case R.id.or_checkbox_Basement_PestActivityZone:
				if(!or_checkbox_Basement_PestActivityZone.isChecked())
				{
					edt_Basement_PestActivityByZone.getText().clear();
				}
				else
				{
					if(checkChange==1)
					{
						edt_Basement_PestActivityByZone.requestFocus();
					}
				}
				break;
            default:
				break;
			}
		}
	}
	class OnRadioChange implements RadioGroup.OnCheckedChangeListener
	{
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId)
		{
			//Log.e("", "inside radio click");
			// TODO Auto-generated method stub
			switch (checkedId) 
			{
			case R.id.radioCash:
				//edttxtAmount.getText().clear();
				edtDRIVING_LICENCE.getText().clear();
				edtEXPIRATION_DATE.getText().clear();
				tramount.setVisibility(View.VISIBLE);
				trcheckno.setVisibility(View.GONE);
				trDrivingLicence.setVisibility(View.GONE);
				trExpirationDate.setVisibility(View.GONE);
				LinearlayoutCHECK_IMAGE.setVisibility(View.GONE);
				break;
			case R.id.radioCheck:

				//edttxtAmount.getText().clear();
				edtDRIVING_LICENCE.setText(generalInfoDTO.getLicenseNo());
				if(!generalInfoDTO.getExpirationDate().equals("01/01/1900") && !generalInfoDTO.getExpirationDate().equals(""))
				{
					edtEXPIRATION_DATE.setText(generalInfoDTO.getExpirationDate());
				}
				tramount.setVisibility(View.VISIBLE);
				trcheckno.setVisibility(View.VISIBLE);
				trDrivingLicence.setVisibility(View.VISIBLE);
				trExpirationDate.setVisibility(View.VISIBLE);
				LinearlayoutCHECK_IMAGE.setVisibility(View.VISIBLE);
				break;
			case R.id.radioCreditcard:
				//edttxtAmount.getText().clear();
				edtDRIVING_LICENCE.getText().clear();
				edtEXPIRATION_DATE.getText().clear();
				tramount.setVisibility(View.VISIBLE);
				trcheckno.setVisibility(View.GONE);
				trDrivingLicence.setVisibility(View.GONE);
				trExpirationDate.setVisibility(View.GONE);
				LinearlayoutCHECK_IMAGE.setVisibility(View.GONE);
				break;
			case R.id.radioBillLater:
				//edttxtAmount.getText().clear();
				edtDRIVING_LICENCE.getText().clear();
				edtEXPIRATION_DATE.getText().clear();
				tramount.setVisibility(View.GONE);
				trcheckno.setVisibility(View.GONE);
				trDrivingLicence.setVisibility(View.GONE);
				trExpirationDate.setVisibility(View.GONE);
				LinearlayoutCHECK_IMAGE.setVisibility(View.GONE);
				break;
			case R.id.radioPreBill:
				//edttxtAmount.getText().clear();
				edtDRIVING_LICENCE.getText().clear();
				edtEXPIRATION_DATE.getText().clear();
				tramount.setVisibility(View.GONE);
				trcheckno.setVisibility(View.GONE);
				trDrivingLicence.setVisibility(View.GONE);
				trExpirationDate.setVisibility(View.GONE);
				LinearlayoutCHECK_IMAGE.setVisibility(View.GONE);
				break;
			case R.id.radioNocharge:
				//edttxtAmount.getText().clear();
				edtDRIVING_LICENCE.getText().clear();
				edtEXPIRATION_DATE.getText().clear();
				tramount.setVisibility(View.GONE);
				trcheckno.setVisibility(View.GONE);
				trDrivingLicence.setVisibility(View.GONE);
				trExpirationDate.setVisibility(View.GONE);
				LinearlayoutCHECK_IMAGE.setVisibility(View.GONE);
				break;
			case R.id.radiobtnFenceInspected1:
				if(radiobtnFenceInspected1.isChecked())
				{
					or_checkboxFenceInspectedTermite.setChecked(true);
				}
				else
				{
					or_checkboxFenceInspectedTermite.setChecked(false);
				}
				break;
			case R.id.radiobtnFenceInspected2:
				if(radiobtnFenceInspected2.isChecked())
				{
					or_checkboxFenceInspectedTermite.setChecked(true);
				}
				else
				{
					or_checkboxFenceInspectedTermite.setChecked(false);
				}
				break;
			case R.id.radiobtnBaitingDevices1:
				if(radiobtnBaitingDevices1.isChecked())
				{
					or_checkboxBaitingDevicesTermite.setChecked(true);
				}
				else
				{
					or_checkboxBaitingDevicesTermite.setChecked(false);
				}
				break;
			case R.id.radiobtnBaitingDevices2:
				if(radiobtnBaitingDevices2.isChecked())
				{
					or_checkboxBaitingDevicesTermite.setChecked(true);
				}
				else
				{
					or_checkboxBaitingDevicesTermite.setChecked(false);
				}
				break;
			case R.id.radiobtnTERMITE_BAITING_DEVICES_REPLACED1:
				if(radiobtnTERMITE_BAITING_DEVICES_REPLACED1.isChecked())
				{
					or_checkboxTermiteBaitingDevicereplacedTermite.setChecked(true);
				}
				else
				{
					or_checkboxTermiteBaitingDevicereplacedTermite.setChecked(false);
				}
				break;
			case R.id.radiobtnTERMITE_BAITING_DEVICES_REPLACED2:
				if(radiobtnTERMITE_BAITING_DEVICES_REPLACED2.isChecked())
				{
					or_checkboxTermiteBaitingDevicereplacedTermite.setChecked(true);
				}
				else
				{
					or_checkboxTermiteBaitingDevicereplacedTermite.setChecked(false);
				}
				break;
			case R.id.radiobtnExteriorPerimeterofhouse1:
				if(radiobtnExteriorPerimeterofhouse1.isChecked())
				{
					or_checkboxExteriorPerimeterofhouseTermite.setChecked(true);
				}
				else
				{
					or_checkboxExteriorPerimeterofhouseTermite.setChecked(false);
				}
				break;
			case R.id.radiobtnExteriorPerimeterofhouse2:
				if(radiobtnExteriorPerimeterofhouse2.isChecked())
				{
					or_checkboxExteriorPerimeterofhouseTermite.setChecked(true);
				}
				else
				{
					or_checkboxExteriorPerimeterofhouseTermite.setChecked(false);
				}
				break;
			case R.id.radiobtnAttic1:
				if(radiobtnAttic1.isChecked())
				{
					or_checkboxATTICTermite.setChecked(true);
				}
				else
				{
					or_checkboxATTICTermite.setChecked(false);
				}
				break;
			case R.id.radiobtnAttic2:
				if(radiobtnAttic2.isChecked())
				{
					or_checkboxATTICTermite.setChecked(true);
				}
				else
				{
					or_checkboxATTICTermite.setChecked(false);
				}
				break;
			case R.id.radiobtnINTERIOROFHOUSE1:
				if(radiobtnINTERIOROFHOUSE1.isChecked())
				{
					or_checkboxINTERIOROFHOUSETermite.setChecked(true);
				}
				else
				{
					or_checkboxINTERIOROFHOUSETermite.setChecked(false);
				}
				break;
			case R.id.radiobtnINTERIOROFHOUSE2:
				if(radiobtnINTERIOROFHOUSE2.isChecked())
				{
					or_checkboxINTERIOROFHOUSETermite.setChecked(true);
				}
				else
				{
					or_checkboxINTERIOROFHOUSETermite.setChecked(false);
				}
				break;
			case R.id.radiobtnBUILDERS_CONTRUCTION_GAP1:
				if(radiobtnBUILDERS_CONTRUCTION_GAP1.isChecked())
				{
					or_checkboxBUILDERS_CONTRUCTION_GAP.setChecked(true);
				}
				else
				{
					or_checkboxBUILDERS_CONTRUCTION_GAP.setChecked(false);
				}
				break;
			case R.id.radiobtnBUILDERS_CONTRUCTION_GAP2:
				if(radiobtnBUILDERS_CONTRUCTION_GAP2.isChecked())
				{
					or_checkboxBUILDERS_CONTRUCTION_GAP.setChecked(true);
				}
				else
				{
					or_checkboxBUILDERS_CONTRUCTION_GAP.setChecked(false);
				}
				break;
			case R.id.radiobtnSOFFIT_RETURNS1:
				if(radiobtnSOFFIT_RETURNS1.isChecked())
				{
					or_checkboxSOFFIT_RETURNS.setChecked(true);
				}
				else
				{
					or_checkboxSOFFIT_RETURNS.setChecked(false);
				}
				break;
			case R.id.radiobtnSOFFIT_RETURNS2:
				if(radiobtnSOFFIT_RETURNS2.isChecked())
				{
					or_checkboxSOFFIT_RETURNS.setChecked(true);
				}
				else
				{
					or_checkboxSOFFIT_RETURNS.setChecked(false);
				}
				break;
			case R.id.radiobtnBREEZYWAY_FROM_GARAGE1:
				if(radiobtnBREEZYWAY_FROM_GARAGE1.isChecked())
				{
					or_checkboxBREEZYWAY_FROM_GARAGE.setChecked(true);
				}
				else
				{
					or_checkboxBREEZYWAY_FROM_GARAGE.setChecked(false);
				}
				break;
			case R.id.radiobtnBREEZYWAY_FROM_GARAGE2:
				if(radiobtnBREEZYWAY_FROM_GARAGE2.isChecked())
				{
					or_checkboxBREEZYWAY_FROM_GARAGE.setChecked(true);
				}
				else
				{
					or_checkboxBREEZYWAY_FROM_GARAGE.setChecked(false);
				}
				break;
			case R.id.radiobtnROOF_VENTS1:
				if(radiobtnROOF_VENTS1.isChecked())
				{
					or_checkboxROOF_VENTS.setChecked(true);
				}
				else
				{
					or_checkboxROOF_VENTS.setChecked(false);
				}
				break;
			case R.id.radiobtnROOF_VENTS2:
				if(radiobtnROOF_VENTS2.isChecked())
				{
					or_checkboxROOF_VENTS.setChecked(true);
				}
				else
				{
					or_checkboxROOF_VENTS.setChecked(false);
				}
				break;
			case R.id.radiobtnGARAGE_DOOR1:
				if(radiobtnGARAGE_DOOR1.isChecked())
				{
					or_checkboxGARAGE_DOOR.setChecked(true);
				}
				else
				{
					or_checkboxGARAGE_DOOR.setChecked(false);
				}
				break;
			case R.id.radiobtnGARAGE_DOOR2:
				if(radiobtnGARAGE_DOOR2.isChecked())
				{
					or_checkboxGARAGE_DOOR.setChecked(true);
				}
				else
				{
					or_checkboxGARAGE_DOOR.setChecked(false);
				}
				break;
			case R.id.radiobtnSOFFIT_VENTS1:
				if(radiobtnSOFFIT_VENTS1.isChecked())
				{
					or_checkboxSOFFIT_VENTS.setChecked(true);
				}
				else
				{
					or_checkboxSOFFIT_VENTS.setChecked(false);
				}
				break;
			case R.id.radiobtnSOFFIT_VENTS2:
				if(radiobtnSOFFIT_VENTS2.isChecked())
				{
					or_checkboxSOFFIT_VENTS.setChecked(true);
				}
				else
				{
					or_checkboxSOFFIT_VENTS.setChecked(false);
				}
				break;
			case R.id.radiobtnAC_DRYER_VENTSR1:
				if(radiobtnAC_DRYER_VENTSR1.isChecked())
				{
					or_checkboxAC_DRYER_VENTS.setChecked(true);
				}
				else
				{
					or_checkboxAC_DRYER_VENTS.setChecked(false);
				}
				break;
			case R.id.radiobtnAC_DRYER_VENTSR2:
				if(radiobtnAC_DRYER_VENTSR2.isChecked())
				{
					or_checkboxAC_DRYER_VENTS.setChecked(true);
				}
				else
				{
					or_checkboxAC_DRYER_VENTS.setChecked(false);
				}
				break;
			case R.id.radiobtnSIDEING_HOUSE1:
				if(radiobtnSIDEING_HOUSE1.isChecked())
				{
					or_checkboxSIDEING_HOUSE.setChecked(true);
				}
				else
				{
					or_checkboxSIDEING_HOUSE.setChecked(false);
				}
				break;
			case R.id.radiobtnSIDEING_HOUSE2:
				if(radiobtnSIDEING_HOUSE2.isChecked())
				{
					or_checkboxSIDEING_HOUSE.setChecked(true);
				}
				else
				{
					or_checkboxSIDEING_HOUSE.setChecked(false);
				}
				break;
			case R.id.radiobtnCRAWL_SPACE_VENTS1:
				if(radiobtnCRAWL_SPACE_VENTS1.isChecked())
				{
					or_checkboxCRAWL_SPACE_VENTS.setChecked(true);
				}
				else
				{
					or_checkboxCRAWL_SPACE_VENTS.setChecked(false);
				}
				break;
			case R.id.radiobtnCRAWL_SPACE_VENTS2:
				if(radiobtnCRAWL_SPACE_VENTS2.isChecked())
				{
					or_checkboxCRAWL_SPACE_VENTS.setChecked(true);
				}
				else
				{
					or_checkboxCRAWL_SPACE_VENTS.setChecked(false);
				}
				break;
			case R.id.radiobtnCRAWL_SPACE_DOOR1:
				if(radiobtnCRAWL_SPACE_DOOR1.isChecked())
				{
					or_checkboxCRAWL_SPACE_DOOR.setChecked(true);
				}
				else
				{
					or_checkboxCRAWL_SPACE_DOOR.setChecked(false);
				}
				break;
			case R.id.radiobtnCRAWL_SPACE_DOOR2:
				if(radiobtnCRAWL_SPACE_DOOR2.isChecked())
				{
					or_checkboxCRAWL_SPACE_DOOR.setChecked(true);
				}
				else
				{
					or_checkboxCRAWL_SPACE_DOOR.setChecked(false);
				}
				break;

			default:
				break;
			}
		}
	}
	private class CustomTextWatcher implements TextWatcher 
	{
		private View view;
		private CustomTextWatcher(View view) 
		{
			this.view = view;
		}
		@Override
		public void afterTextChanged(Editable s) 
		{
			// TODO Auto-generated method stub
			String newString = s.toString();

			switch(view.getId())
			{
			
			case R.id.edtFenceLinesByZone:
				if(newString.equals(""))
				{
					//	LoggerUtil.e("checked newString","false");
					or_checkboxFenceLines.setChecked(false);

				}
				else
				{
					//	LoggerUtil.e("checked newString","true");
					or_checkboxFenceLines.setChecked(true);
				}
				break;
			case R.id.edtFrontYard:
				if(newString.equals(""))
				{
					//	LoggerUtil.e("checked newString","false");
					or_checkboxFrontyardByZOne.setChecked(false);

				}
				else
				{
					//	LoggerUtil.e("checked newString","true");
					or_checkboxFrontyardByZOne.setChecked(true);
				}
				break;
			case R.id.edtBackyard:
				if(newString.equals(""))
				{
					//	LoggerUtil.e("checked newString","false");
					or_checkboxBackYardByZone.setChecked(false);

				}
				else
				{
					//	LoggerUtil.e("checked newString","true");
					or_checkboxBackYardByZone.setChecked(true);
				}
				break;
			case R.id.edtExteriorParaByZone:
				if(newString.equals(""))
				{
					//	LoggerUtil.e("checked newString","false");
					or_checkboxExteriorParametresofhouseByZone.setChecked(false);

				}
				else
				{
					//	LoggerUtil.e("checked newString","true");
					or_checkboxExteriorParametresofhouseByZone.setChecked(true);
				}
				break;
			case R.id.edtRoofGutterByZone:
				if(newString.equals(""))
				{
					//	LoggerUtil.e("checked newString","false");
					or_checkboxRofGutter.setChecked(false);

				}
				else
				{
					//	LoggerUtil.e("checked newString","true");
					or_checkboxRofGutter.setChecked(true);
				}
				break;
			case R.id.edtGarageByZone:
				if(newString.equals(""))
				{
					//	LoggerUtil.e("checked newString","false");
					or_checkboxGarage.setChecked(false);

				}
				else
				{
					//	LoggerUtil.e("checked newString","true");
					or_checkboxGarage.setChecked(true);
				}
				break;
			case R.id.edtAtticPestByZone:
				if(newString.equals(""))
				{
					//	LoggerUtil.e("checked newString","false");
					or_checkboxAtticByZone.setChecked(false);

				}
				else
				{
					//	LoggerUtil.e("checked newString","true");
					or_checkboxAtticByZone.setChecked(true);
				}
				break;
			case R.id.edtInteriorHouseByZone:
				if(newString.equals(""))
				{
					//	LoggerUtil.e("checked newString","false");
					or_checkboxInteriorHouseByZone.setChecked(false);
				}
				else
				{
					//	LoggerUtil.e("checked newString","true");
					or_checkboxInteriorHouseByZone.setChecked(true);
				}
				break;
			case R.id.edt_Basement_PestActivityByZone:
				if(newString.equals(""))
				{
					//	LoggerUtil.e("checked newString","false");
					or_checkbox_Basement_PestActivityZone.setChecked(false);
				}
				else
				{
					//	LoggerUtil.e("checked newString","true");
					or_checkbox_Basement_PestActivityZone.setChecked(true);
				}
				break;
			case R.id.edtNUMBER_OF_BAITING_DEVICES_INSPECTED:
				if(newString.equals(""))
				{
					//	LoggerUtil.e("checked newString","false");
					or_checkboxNumberBaitingDevicesInspectedTermite.setChecked(false);
				}
				else
				{
					//	LoggerUtil.e("checked newString","true");
					or_checkboxNumberBaitingDevicesInspectedTermite.setChecked(true);
				}
				break;
			case R.id.edt_Basement_TermiteServices:
				if(newString.equals(""))
				{
					//	LoggerUtil.e("checked newString","false");
					or_checkbox_Basement_Termite.setChecked(false);
				}
				else
				{
					//	LoggerUtil.e("checked newString","true");
					or_checkbox_Basement_Termite.setChecked(true);
				}
				break;
			case R.id.edtAdditionalDeviceAdded:
				if(newString.equals(""))
				{
					//LoggerUtil.e("checked newString","false");
					or_checkboxAdditionalDeviceAddedTermite.setChecked(false);
				}
				else
				{
					//LoggerUtil.e("checked newString","true");
					or_checkboxAdditionalDeviceAddedTermite.setChecked(true);
				}
				break;
			case R.id.edtNumberDeviceRemoved:
				if(newString.equals(""))
				{
					//LoggerUtil.e("checked newString","false");
					or_checkboxNumberDeviceRemovedTermite.setChecked(false);
				}
				else
				{
					//LoggerUtil.e("checked newString","true");
					or_checkboxNumberDeviceRemovedTermite.setChecked(true);
				}
				break;
			case R.id.edtNO_LIVEANIMAL_TRAPS_INSTALLES:
				if(newString.equals(""))
				{
					//LoggerUtil.e("checked newString","false");
					or_checkboxNO_LIVEANIMAL_TRAPS_INSTALLES.setChecked(false);
				}
				else
				{
					//LoggerUtil.e("checked newString","true");
					or_checkboxNO_LIVEANIMAL_TRAPS_INSTALLES.setChecked(true);
				}
				break;
				
			case R.id.edtNumber_SNAPTRAPS:
				if(newString.equals(""))
				{
					//LoggerUtil.e("checked newString","false");
					or_checkboxNumber_SNAPTRAPS.setChecked(false);
				}
				else
				{
					//LoggerUtil.e("checked newString","true");
					or_checkboxNumber_SNAPTRAPS.setChecked(true);
				}
				break;
			case R.id.edtMosquitoService1:
				if(newString.equals(""))
				{
					//LoggerUtil.e("checked newString","false");
					or_checkboxCHECKED_RUN_DURACTION_TIME.setChecked(false);
				}
				else
				{
					//LoggerUtil.e("checked newString","true");
					or_checkboxCHECKED_RUN_DURACTION_TIME.setChecked(true);
				}
				break;
			case R.id.edtMosquitoService2:
				if(newString.equals(""))
				{
					//LoggerUtil.e("checked newString","false");
					or_checkboxCHECKED_TIME_SYSTEM_RUNS.setChecked(false);
				}
				else
				{
					//LoggerUtil.e("checked newString","true");
					or_checkboxCHECKED_TIME_SYSTEM_RUNS.setChecked(true);
				}
				break;
			case R.id.edttxtInspectedandTreatedFor1:
				if(newString.equals(""))
				{
					//LoggerUtil.e("checked newString","false");
					or_checkboxANTS_Inspected_treated_for.setChecked(false);
				}
				else
				{
					//LoggerUtil.e("checked newString","true");
					or_checkboxANTS_Inspected_treated_for.setChecked(true);
				}
				break;
			case R.id.edttxtInspectedandTreatedFor2:
				if(newString.equals(""))
				{
					//LoggerUtil.e("checked newString","false");
					or_checkboxOTHER_PESTS_INSPECTED_TREATED_FOR.setChecked(false);
				}
				else
				{
					//LoggerUtil.e("checked newString","true");
					or_checkboxOTHER_PESTS_INSPECTED_TREATED_FOR.setChecked(true);
				}
				break;
			default:
				break;
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			/*System.out.println("1wf"+s);
			if(s.equals(" "))
			{
				checkBoxFrontyard.setChecked(false);
			}
			if(!s.equals(" "))
			{
			checkBoxFrontyard.setChecked(true);
			}*/
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			/*System.out.println("1wf..."+s);
			if(s.equals(" "))
			{
				checkBoxFrontyard.setChecked(false);
			}
			if(!s.equals(" "))
			{
			checkBoxFrontyard.setChecked(true);
			}*/
		}
	}

	CharSequence[] charSequenceItems;
	class OnButtonClick implements OnClickListener
	{
		@Override
		public void onClick(View v)
		{
			switch (v.getId()) {
			case R.id.btnCHECK_BACK_IMAGE:
				if(listBackImg.size()>0)
				{
					new AlertDialog.Builder(OR_PestControlServiceReport.this)
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setTitle("Change Check Back Image")
					.setMessage("Are you sure want to replace it?")
					.setCancelable(false)
					.setPositiveButton("Yes", new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int which) 
						{
             				final CharSequence[] items = {" Take New Photo "," Choose from Gallery "};

							// Creating and Building the Dialog 
							AlertDialog.Builder builder = new AlertDialog.Builder(OR_PestControlServiceReport.this);
							builder.setTitle("Upload Image");
							builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int item) {

									switch(item)
									{
									case 0:

										if (CommonFunction.isSdPresent())
										{
											takeBackPicture();
										} else
										{
											Toast.makeText(OR_PestControlServiceReport.this,"SdCard Not Present", 1000).show();
										}


										break;
									case 1:

										if (CommonFunction.isSdPresent()) 
										{
											pickImage("CHECK_BACK_IMAGE");
										} else 
										{
											Toast.makeText(OR_PestControlServiceReport.this,"SdCard Not Present", 1000).show();
										}

										break;
									}
									dialogBackImage.dismiss();    
								}
							});
							dialogBackImage = builder.create();
							dialogBackImage.show();

						}

					})
					.setNegativeButton("No", new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int which) 
						{
							dialog.dismiss();
						}
					})
					.show();	
				}
				else
				{
					final CharSequence[] items = {" Take New Photo "," Choose from Gallery "};

					// Creating and Building the Dialog 
					AlertDialog.Builder builder = new AlertDialog.Builder(OR_PestControlServiceReport.this);
					builder.setTitle("Upload Image");
					builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int item) {

							switch(item)
							{
							case 0:

								if (CommonFunction.isSdPresent())
								{
									takeBackPicture();
								} else
								{
									Toast.makeText(OR_PestControlServiceReport.this,"SdCard Not Present", 1000).show();
								}

								break;
							case 1:

								if (CommonFunction.isSdPresent()) 
								{
									pickImage("CHECK_BACK_IMAGE");
								} 
								else 
								{
									Toast.makeText(OR_PestControlServiceReport.this,"SdCard Not Present", 1000).show();
								}

								break;
							}
							dialogBackImage.dismiss();    
						}
					});
					dialogBackImage = builder.create();
					dialogBackImage.show();
				}

				break;
			case R.id.btnCHECK_FRONT_IMAGE:
			//	Log.e("listFrontImg.size()", ""+listFrontImg.size());
				if(listFrontImg.size()>0)
				{
					new AlertDialog.Builder(OR_PestControlServiceReport.this)
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setTitle("Change Check Front Image")
					.setMessage("Are you sure want to replace it?")
					.setCancelable(false)
					.setPositiveButton("Yes", new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int which) 
						{


							final CharSequence[] items = {" Take New Photo "," Choose from Gallery "};

							// Creating and Building the Dialog 
							AlertDialog.Builder builder = new AlertDialog.Builder(OR_PestControlServiceReport.this);
							builder.setTitle("Upload Image");
							builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int item) {

									switch(item)
									{

									case 0:
										if (CommonFunction.isSdPresent())
										{
											takeFrontPicture();
										} else
										{
											Toast.makeText(OR_PestControlServiceReport.this,"SdCard Not Present", 1000).show();
										}

										break;
									case 1:
										if (CommonFunction.isSdPresent()) 
										{
											pickImage("CHECK_FRONT_IMAGE");
										} else 
										{
											Toast.makeText(OR_PestControlServiceReport.this,"SdCard Not Present", 1000).show();
										}

										break;
									}
									dialogFrontImage.dismiss();    
								}
							});
							dialogFrontImage = builder.create();
							dialogFrontImage.show();
						}
					})
					.setNegativeButton("No", new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int which) 
						{
							dialog.dismiss();
						}
					})
					.show();	
				}
				else
				{
					final CharSequence[] items = {" Take New Photo "," Choose from Gallery "};

					// Creating and Building the Dialog 
					AlertDialog.Builder builder = new AlertDialog.Builder(OR_PestControlServiceReport.this);
					builder.setTitle("Upload Image");
					builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int item) {

							switch(item)
							{
							case 0:
								if (CommonFunction.isSdPresent())
								{
									takeFrontPicture();
								} else
								{
									Toast.makeText(OR_PestControlServiceReport.this,"SdCard Not Present", 1000).show();
								}
								break;
							case 1:

								if (CommonFunction.isSdPresent()) 
								{
									pickImage("CHECK_FRONT_IMAGE");
								} else 
								{
									Toast.makeText(OR_PestControlServiceReport.this,"SdCard Not Present", 1000).show();
								}

								break;
							}
							dialogFrontImage.dismiss();    
						}
					});
					dialogFrontImage = builder.create();
					dialogFrontImage.show();
				}

				break;
			case R.id.edtEXPIRATION_DATE:
				// String strFromDate=edtFromdate.getText().toString();
				//  String strToDate=edtTodate.getText().toString();
				DialogFragment DatePickerFragment = new DatePickerFragment(edtEXPIRATION_DATE);
				DatePickerFragment.show(getFragmentManager(), "Date Picker");
				DatePickerFragment.setCancelable(false);
				LoggerUtil.e("edttxtExpirationDate.getText().toString()", ""+edtEXPIRATION_DATE.getText().toString());

				break;
			case R.id.edtDate:
				// String strFromDate=edtFromdate.getText().toString();
				//  String strToDate=edtTodate.getText().toString();
				DialogFragment TodateFragment = new DatePickerFragment(edtDate,edtDate,2);
				TodateFragment.show(getFragmentManager(), "Date Picker");
				TodateFragment.setCancelable(false);
				break;
			case R.id.btnsaveandcontinueINVOICE:
				getValuesFromComponents();
				setValuesToResidentialDTO();
				validationList.clear();
				isValidated=true;
				objectFocus=null;
				validateComponents();
				if(isValidated)
				{    
					checkChange=0;
					/*insertUpadteResidentialTable();
					insertUpdateLog();*/
					Intent i= new Intent(OR_PestControlServiceReport.this,OR_TodayServiceInvoice.class);
					i.putExtra("userDTO",userDTO);
					i.putExtra("generalInfoDTO", generalInfoDTO);
					i.putExtra("strOutsideService",strOutsidepestControlServices);
					i.putExtra("strInsideService", strInsidepestControlServices);
					i.putExtra(ParameterUtil.ServiceID_new, generalInfoDTO.getServiceID_new());
					startActivity(i);
				}
				else
				{
					mLastClickTime = 0;
					charSequenceItems = validationList.toArray(new CharSequence[validationList.size()]);
					createMessageList(charSequenceItems,objectFocus);
				}
				break;
			
			case R.id.btnCancel:
				finish();
				break;

			default:
				break;
			}
		}
	}
	private void validateComponents() 
	{
	   // TODO Auto-generated method stub
	   if(radioCreditcard.isChecked())
	   {
		if(edtAmount.getText().toString().equals("") || edtAmount.getText().toString().equals("0"))
		{
			validationList.add("Enter Amount.");
			objectFocus=edtAmount;
			isValidated=false;
		}
		else if(edtAmount.getText().toString().contains(".") && edtAmount.getText().toString().length()==1)
		{
			validationList.add("Amount format should be: 00.00");
			objectFocus=edtAmount;
			isValidated=false;
		}
	    }
	   if(radioCash.isChecked())
	    {
		if(edtAmount.getText().toString().equals("") || edtAmount.getText().toString().equals("0"))
		{
			validationList.add("Enter Amount.");
			objectFocus=edtAmount;
			isValidated=false;
		}
		else if(edtAmount.getText().toString().contains(".") && edtAmount.getText().toString().length()==1)
		{
			validationList.add("Amount format should be: 00:00");
			objectFocus=edtAmount;
			isValidated=false;
		}
	  }
	  if(radioCheck.isChecked())
	  {
		if(edtAmount.getText().toString().equals("") || edtAmount.getText().toString().equals("0"))
		{
			validationList.add("Enter Amount.");
			objectFocus=edtAmount;
			isValidated=false;
		}
		if(edtAmount.getText().toString().contains(".") && edtAmount.getText().toString().length()==1)
		{
			validationList.add("Amount format should be: 00:00");
			objectFocus=edtAmount;
			isValidated=false;
		}
		if(edtCheckNo.getText().toString().equals(""))
		{
			validationList.add("Enter Check#.");
			objectFocus=edtCheckNo;
			isValidated=false;
		}
		if(strEdttxtDrivingLicence.equals(""))
		{
			validationList.add("Enter Driving  License#.");
			objectFocus=edtDRIVING_LICENCE;
			isValidated=false;
		}
		if(strEdtExpirationDate.equals(""))
		{
			validationList.add("Enter Expiration Date.");
			objectFocus=edtEXPIRATION_DATE;
			isValidated=false;
		}
		if(!edtEXPIRATION_DATE.getText().toString().equals(""))
		{
			try 
			{
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

				Date dateCurrent = sdf.parse(CommonFunction.getCurrentDate());
				Date dateExpiry = sdf.parse(edtEXPIRATION_DATE.getText().toString());
				if(dateExpiry.before(dateCurrent))
				{
					validationList.add("Expiration Date should not be smaller then Today.");
					objectFocus=edtEXPIRATION_DATE;
					isValidated=false;
				}
			} 
			catch (Exception e) 
			{
			}
		}

		//Log.e("", "generalInfoDTO.getCheckFrontImage().inside validate.."+generalInfoDTO.getCheckFrontImage());
		if(CommonFunction.checkString(generalInfoDTO.getCheckFrontImage(), "").equals(""))
		{
			//Log.e("", "ImageNameFront..."+ImageNameFront);
			validationList.add("Upload Check Front Image.");
			isValidated=false;
		}
		if(CommonFunction.checkString(generalInfoDTO.getCheckBackImage(), "").equals(""))
		{
			//Log.e("", "ImageNameBack..."+ImageNameBack);
			validationList.add("Upload Check Back Image.");
			isValidated=false;
		}
	}

	if(or_checkboxCHECKED_TIME_SYSTEM_RUNS.isChecked())
	{
		if(edtMosquitoService2.getText().toString().equals(""))
		{
			validationList.add("Enter Checked Time System Runs Mosquito Services Tab.");
			objectFocus=edtMosquitoService2;
			isValidated=false;
		}
	}
	if(or_checkboxCHECKED_RUN_DURACTION_TIME.isChecked())
	{
		if(edtMosquitoService1.getText().toString().equals(""))
		{
			validationList.add("Enter Checked Run Duration Time Under Mosquito Services Tab.");
			objectFocus=edtMosquitoService1;
			isValidated=false;
		}
	}

	if(or_checkboxNumber_SNAPTRAPS.isChecked())
	{
		if(edtNumber_SNAPTRAPS.getText().toString().equals(""))
		{
			validationList.add("Enter Number of Snap traps placed in Rodent Control Tab.");
			isValidated=false;
		}
	}
	if(or_checkboxNO_LIVEANIMAL_TRAPS_INSTALLES.isChecked())
	{
		if(edtNO_LIVEANIMAL_TRAPS_INSTALLES.getText().toString().equals(""))
		{
			validationList.add("Enter Number of Live animal traps installed in Rodent Control Tab.");
			isValidated=false;
		}
	}

	if(or_checkboxNumberBaitingDevicesInspectedTermite.isChecked())
	{
		if(edtNUMBER_OF_BAITING_DEVICES_INSPECTED.getText().toString().equals(""))
		{
			validationList.add("Enter Number of Baiting Devices Inspected in Termite Control Tab.");
			isValidated=false;
		}
	}
	if(or_checkboxOTHER_PESTS_INSPECTED_TREATED_FOR.isChecked() && CommonFunction.checkString(edttxtInspectedandTreatedFor2.getText().toString().trim(), "").equals(""))
	{
		//Log.e("", "ImageNameFront..."+ImageNameFront);
		validationList.add("Enter Number of Other Pests under Inspected and Treated For Tab");
		objectFocus=edttxtInspectedandTreatedFor2;
		isValidated=false;
	}
	if(or_checkboxANTS_Inspected_treated_for.isChecked() && CommonFunction.checkString(edttxtInspectedandTreatedFor1.getText().toString().trim(), "").equals(""))
	{
		//Log.e("", "ImageNameFront..."+ImageNameFront);
		validationList.add("Enter Number of Ants under Inspected and Treated For Tab");
		objectFocus=edttxtInspectedandTreatedFor1;
		isValidated=false;
	}

	/*if(radioCreditcard.isChecked())
	{
		if(edttxtAmount.getText().toString().equals("") || edttxtAmount.getText().toString().equals("0"))
		{
			validationList.add("Enter Amount.");
			isValidated=false;
		}
	}*/
	validateUnderTermite();
	validateUnderRodent();
	//validateUnderChemicalTable(); doubt
  }
	private void validateUnderRodent() {
		// TODO Auto-generated method stub
		if(or_checkboxBUILDERS_CONTRUCTION_GAP.isChecked())
		{

			int idRoofPitches = radioGroupBUILDERS_CONTRUCTION_GAP.getCheckedRadioButtonId();
			if(idRoofPitches == -1)
			{

				validationList.add("Select Builders construction In Rodent Services");
				isValidated=false;
			}
		}

		if(or_checkboxSOFFIT_RETURNS.isChecked())
		{
			int idRoofVents = radioGroupSOFFIT_RETURNS.getCheckedRadioButtonId();
			if(idRoofVents == -1)
			{

				validationList.add("Select Sealed for Soffit Returns In Rodent Services");
				isValidated=false;
			}
		}
		if(or_checkboxBREEZYWAY_FROM_GARAGE.isChecked())
		{
			int idBreezewayGarage = radioGroupBREEZYWAY_FROM_GARAGE.getCheckedRadioButtonId();

			if(idBreezewayGarage == -1)
			{
				validationList.add("Select Sealed for Breezy Way In Rodent Services");
				isValidated=false;
			}
		}
		if(or_checkboxROOF_VENTS.isChecked())
		{
			int idSoffitVents = radioGroupROOF_VENTS.getCheckedRadioButtonId();
			if(idSoffitVents == -1)
			{

				validationList.add("Select Sealed for Roof Vents In Rodent Services");
				isValidated=false;
			}
		}

		if(or_checkboxGARAGE_DOOR.isChecked())
		{

			int idGarageDoor = radioGroupGARAGE_DOOR.getCheckedRadioButtonId();
			if(idGarageDoor == -1)
			{

				validationList.add("Select Sealed for Garage Door In Rodent Services");
				isValidated=false;
			}
		}

		if(or_checkboxSOFFIT_VENTS.isChecked())
		{
			int idSoffitVents = radioGroupSOFFIT_VENTS.getCheckedRadioButtonId();
			if(idSoffitVents == -1)
			{

				validationList.add("Select Sealed for Soffit Vents In Rodent Services");
				isValidated=false;
			}
		}
		if(or_checkboxAC_DRYER_VENTS.isChecked())
		{

			int idACDRYER = radioGroupAC_DRYER_VENTS.getCheckedRadioButtonId();
			if(idACDRYER == -1)
			{

				validationList.add("Select Sealed for AC Dryer In Rodent Services");
				isValidated=false;
			}
		}

		if(or_checkboxSIDEING_HOUSE.isChecked())
		{

			int idSidingofHouse = radioGroupSIDEING_HOUSE.getCheckedRadioButtonId();
			if(idSidingofHouse == -1)
			{

				validationList.add("Select Sealed for Siding of House In Rodent Services");
				isValidated=false;
			}
		}


		if(or_checkboxCRAWL_SPACE_VENTS.isChecked())
		{

			int idSidingofHouse = radioGroupCRAWL_SPACE_VENTS.getCheckedRadioButtonId();
			if(idSidingofHouse == -1)
			{

				validationList.add("Select Sealed crawl space In Rodent Services");
				isValidated=false;
			}
		}	
		
	}
	private void validateUnderTermite() {
		// TODO Auto-generated method stub
		if(or_checkboxFenceInspectedTermite.isChecked())
		{

			int idFenceInspected = radioGroupFenceInspected.getCheckedRadioButtonId();
			if(idFenceInspected == -1)
			{
				validationList.add("Select Visible Termite Activity for Fence Inspected in Service Termite Tab");
				isValidated=false;
			}
		}


		if(or_checkboxBaitingDevicesTermite.isChecked())
		{

			int idMoniterDevices = radioGroupBaitingDevices.getCheckedRadioButtonId();
			if(idMoniterDevices == -1)
			{
				validationList.add("Select Visible Baiting Activity for Monitoring Devices in Service Termite Tab");
				isValidated=false;
			}
		}

		if(or_checkboxTermiteBaitingDevicereplacedTermite.isChecked())
		{

			int idTermiteMoniter = radioGroupTERMITE_BAITING_DEVICES_REPLACED.getCheckedRadioButtonId();
			if(idTermiteMoniter == -1)
			{
				validationList.add("Select Visible Baiting Activity for Termite Monitoring Devices Replaced in Service Termite Tab");
				isValidated=false;
			}
		}

		if(or_checkboxExteriorPerimeterofhouseTermite.isChecked())
		{

			int idExteriorPerimeter = radioGroupExteriorPerimeterofhouse.getCheckedRadioButtonId();
			if(idExteriorPerimeter == -1)
			{
				validationList.add("Select Visible Termite Activity for Exterior Perimeter in Service Termite Tab");
				isValidated=false;
			}
		}

		if(or_checkboxATTICTermite.isChecked())
		{
			int idAttic = radioGroupAttic.getCheckedRadioButtonId();
			if(idAttic == -1)
			{
				validationList.add("Select Visible Termite Activity for Attic Devices in Service Termite Tab");
				isValidated=false;
			}
		}


		if(or_checkboxINTERIOROFHOUSETermite.isChecked())
		{

			int idInteriorHouse = radioGroupINTERIOROFHOUSE.getCheckedRadioButtonId();
			if(idInteriorHouse == -1)
			{
				validationList.add("Select Visible Termite Activity for Interior of House in Service Termite Tab");
				isValidated=false;
			}
		}

	}

	public void pickImage(String CHECK_FRONT_IMAGE) 
	{
		//Log.e("CHECK_FRONT_IMAGE", ""+CHECK_FRONT_IMAGE);
		if(CHECK_FRONT_IMAGE.equals("CHECK_FRONT_IMAGE"))
		{
			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			intent.addCategory(Intent.CATEGORY_OPENABLE);
			startActivityForResult(intent, REQUEST_Gallery_ACTIVITY_CODE_Front);
		}
		else
		{
			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			intent.addCategory(Intent.CATEGORY_OPENABLE);
			startActivityForResult(intent, REQUEST_Gallery_ACTIVITY_CODE_Back);
		}
	}
	
    public void takeBackPicture() 
	{
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		CheckBackImageName =generalInfoDTO.getServiceID_new()+"_CheckBack.jpg"; //CommonFunction.CreateImageName();
		Uri imageUri = Uri.fromFile(CommonFunction.getFileLocation(OR_PestControlServiceReport.this, CheckBackImageName));
		intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
		intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
		intent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, Integer.toString(1024*1024));
		startActivityForResult(intent, REQUEST_Camera_ACTIVITY_CODE_Back);
		//LoggerUtil.e("Image Url", imageUri + "");
	}
	public void takeFrontPicture() 
	{
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		CheckFrontImageName =generalInfoDTO.getServiceID_new()+"_CheckFront.jpg"; //CommonFunction.CreateImageName();
		Uri imageUri = Uri.fromFile(CommonFunction.getFileLocation(OR_PestControlServiceReport.this, CheckFrontImageName));
		intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
		intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
		intent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, Integer.toString(1024*1024));
		startActivityForResult(intent, REQUEST_Camera_ACTIVITY_CODE_Front);
		//LoggerUtil.e("Image Url", imageUri + "");
	}
	private final Dialog createMessageList(final CharSequence[] messageList,final EditText object)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(OR_PestControlServiceReport.this);
		builder.setTitle(getResources().getString(R.string.REQUIRED_INFORMATION));
		builder.setItems(charSequenceItems,new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton)
			{
				//  LoggerUtil.e("","E' stato premuto il pulsante: "+messageList[whichButton]);   
			}
		});
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton)
			{
				if(object!=null)
				{
					object.requestFocus();
				}
			}
		});
		return builder.show();
	}
	class OnGalleryClickFront implements OnItemLongClickListener 
	{
		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
				int position, long arg3) 
		{
			if(generalInfoDTO.getServiceStatus().equals("InComplete"))
			{
				if(listFrontImg.size()>0)
				{
					listFrontImg.remove(position);
					if(tempFrontImagList.size()>0)
					{
						tempFrontImagList.remove(position);
					}
					if(tempStoreFrontImageName.size()>0)
					{
						tempStoreFrontImageName.remove(position);
					}
					RefreshFrontImage();
					listFrontImg.clear();
					generalInfoDTO.setCheckFrontImage("");
				}
			}
			return false;
		}
	}

	class OnGalleryClickBack implements OnItemLongClickListener 
	{
		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
				int position, long arg3) 
		{
			if(generalInfoDTO.getServiceStatus().equals("InComplete"))
			{
				if(listBackImg.size()>0)
				{
					listBackImg.remove(position);
					if(tempBackImagList.size()>0)
					{
						tempBackImagList.remove(position);

					}
					if(tempStoreBackImageName.size()>0)
					{
						tempStoreBackImageName.remove(position);
					}
					RefreshBackImage();
					listBackImg.clear();
					generalInfoDTO.setCheckBackImage("");
				}
			}
			return false;
		}
	}
	public void setValuesToResidentialDTO() 
	{
		oresidential_pest_DTO.setFloridaResidentialServiceId(strFloridaResidentialServiceId);
		oresidential_pest_DTO.setServiceID_new(serviceID_new);
				
		//set service for
		oresidential_pest_DTO.setServiceFor(strServiceFor);

		//set Areas inspected
		oresidential_pest_DTO.setAreasInspected(strAreasInspected);

		//set inspected treated for
		oresidential_pest_DTO.setInsectActivity(strInspectedadnTreatedFor);
		oresidential_pest_DTO.setAnts(strInspectedadnTreatedFor1);
		oresidential_pest_DTO.setOtherPest(strInspectedadnTreatedFor2);
				
		//set outside service performed
		oresidential_pest_DTO.setOutsideOnly(strOutsideService);

		//for pest activity by zone
		oresidential_pest_DTO.setPestActivity(strpestActivityByZone);
		oresidential_pest_DTO.setFence(strEdttxtFenceLinesByZone);
		oresidential_pest_DTO.setBackYard(strEdttxtBackYardByZone);
		oresidential_pest_DTO.setFrontYard(strEdttxtFrontyardByZone);
		oresidential_pest_DTO.setExteriorPerimeter(strEdttxtExteriorPerimeterHouseByZone);
		oresidential_pest_DTO.setRoofline(strEdttxtRoofGutterLine);
		oresidential_pest_DTO.setGarage(strEdttxtGarageBYZone);
		oresidential_pest_DTO.setAttic(strEdttxtAtticByZone);
		oresidential_pest_DTO.setInteriorHouse(strEdttxtInteriorOfHouseByZone);
		oresidential_pest_DTO.setBasement(strEdttxtBasement);

		//for pest control services
		oresidential_pest_DTO.setPestControlServices(strpestControlServices);

		//for termite services
		oresidential_pest_DTO.setFencePest(strCheckFenceInspected);
		oresidential_pest_DTO.setNumberPest(strCheckNumberBaitingDevicesInspectedTermite);
		oresidential_pest_DTO.setWoodPest(strCheckBaitingDevice);
		oresidential_pest_DTO.setNumberActivity(strCheckTermiteBaitingDevicereplaced);
		oresidential_pest_DTO.setExteriorPest(strCheckExteriorPerimeterofhouseTermite);
		oresidential_pest_DTO.setAtticPest(strCheckATTICTermite);
		oresidential_pest_DTO.setInteriorPest(strCheckINTERIOROFHOUSETermite);
		oresidential_pest_DTO.setBasementTermite(strCheckBasementTermite);

		//edit text
		oresidential_pest_DTO.setFenceActivity(strRadioFenceInspected);
		oresidential_pest_DTO.setNumberActivitytext(strBaitingDevicesInspected);
		oresidential_pest_DTO.setWoodActivity(strRadioBaitingDevice);
		oresidential_pest_DTO.setWoodOption(strRadioBaitingDevicereplaced);
		oresidential_pest_DTO.setExteriorActivity(strRadioExteriorPerimeterofhouseTermite);
		oresidential_pest_DTO.setAtticActivity(strRadioATTICTermite);
		oresidential_pest_DTO.setInteriorActivity(strRadioINTERIOROFHOUSETermite);

		// for rodent services
		oresidential_pest_DTO.setBuilderConstruction(strCheckBUILDERS_CONTRUCTION_GAP);
		oresidential_pest_DTO.setBuilderConstructionActivity(strRadioBUILDERS_CONTRUCTION_GAP);
		
		//soffit returns doubt ?
		
		oresidential_pest_DTO.setBreezway(strCheckBREEZYWAY_FROM_GARAGE);
		oresidential_pest_DTO.setBreezwayNeeds(strRadioBREEZYWAY_FROM_GARAGE);
		
		oresidential_pest_DTO.setRoofVentRodent(strCheckROOF_VENTS);
		oresidential_pest_DTO.setRoofVentNeeds(strRadioROOF_VENTS);
				
		oresidential_pest_DTO.setGarageDoor(strCheckGARAGE_DOOR);
		oresidential_pest_DTO.setGarageDoorNeeds(strRadioGARAGE_DOOR);
		
		oresidential_pest_DTO.setFlashing(strCheckSOFFIT_VENTS);
		oresidential_pest_DTO.setFlashingNeeds(strRadioSOFFIT_VENTS);
		
		oresidential_pest_DTO.setDryerVent(strCheckAC_DRYER_VENTS);
		oresidential_pest_DTO.setDryerVentNeeds(strRadioAC_DRYER_VENTS);
		
		oresidential_pest_DTO.setNumberLive(strCheckNO_LIVEANIMAL_TRAPS_INSTALLES);
		oresidential_pest_DTO.setNumberLiveNeeds(strRadioNO_LIVEANIMAL_TRAPS_INSTALLES);

		oresidential_pest_DTO.setSiding(strCheckSIDEING_HOUSE);
		oresidential_pest_DTO.setSidingNeeds(strRadioSIDEING_HOUSE);
		
		oresidential_pest_DTO.setNumberSnap(strCheckNumber_SNAPTRAPS);
		oresidential_pest_DTO.setNumberSnapNeeds(strNumber_SNAPTRAPS);
		
		//crawl space vents doubt ?
		//crawl space door doubt ?
						
		//for mosquito
		oresidential_pest_DTO.setPlusMosquitoDetail(strMosquito);
		oresidential_pest_DTO.setDurationTime(strEdttxtMosquitoServices1);
		oresidential_pest_DTO.setSystemRuns(strEdttxtMosquitoServices2);
				
		// for recommendations
		oresidential_pest_DTO.setTreeBranches(strCheckTreeBranches);
		oresidential_pest_DTO.setFirewood(strCheckFirewood);
		oresidential_pest_DTO.setDebrisCrawl(strCheckDebrisInCrawl);
		oresidential_pest_DTO.setExcessivePlant(strCheckExcessiveplant);
		oresidential_pest_DTO.setSoil(strCheckSoilAbove);
		oresidential_pest_DTO.setWoodSoil(strCheckWoodSoil);
		oresidential_pest_DTO.setDebrisRoof(strCheckDebrisRoof);
		oresidential_pest_DTO.setStandingWater(strCheckStandingWater);
		oresidential_pest_DTO.setMoistureProblem(strCheckMoistureProblem);
		oresidential_pest_DTO.setOpenings(strCheckOpeningPlumbing);
		oresidential_pest_DTO.setExcessiveGaps(strCheckExcessiveGaps);
		oresidential_pest_DTO.setLeakyPlumbing(strCheckLaekyPlumbing);
		oresidential_pest_DTO.setGarbageCans(strCheckGarbageCans);
		oresidential_pest_DTO.setMoistureDamaged(strCheckMoistureDamage);
		oresidential_pest_DTO.setGroceryBags(strCheckGrocerybags);
		oresidential_pest_DTO.setPetFood(strCheckPetFood);
		oresidential_pest_DTO.setRecycledItems(strCheckRecycledItems);
		oresidential_pest_DTO.setExcessiveStorage(strCheckExcessiveStorage);

		//set values for invoice
		oresidential_pest_DTO.setTechnicianComments(strEdttxtTechComments);
		oresidential_pest_DTO.setTechnician(strEdttxtServiceTech);
		oresidential_pest_DTO.setEmpNo(strEdttxtEmp);
		oresidential_pest_DTO.setCustomer(strEdttxtCustomer);
		oresidential_pest_DTO.setDate(strEdttxtdate);
		oresidential_pest_DTO.setAmount(strEdttxtAmount);
		
		generalInfoDTO.setServiceID_new(serviceID_new);
		generalInfoDTO.setInv_value(strEdttxtInvValue);
		generalInfoDTO.setProd_value(strEdttxtProdValue);
		generalInfoDTO.setTax_value(strEdttxtTax);
		generalInfoDTO.setTotal(generalInfoDTO.getInv_value());
		generalInfoDTO.setCheckNo(strEdttxtCheckNo);
		generalInfoDTO.setLicenseNo(strEdttxtDrivingLicence);

		oresidential_pest_DTO.setCheckNo(strEdttxtCheckNo);
		oresidential_pest_DTO.setLicenseNo(strEdttxtDrivingLicence);


		if(!CommonFunction.checkString(strEdtExpirationDate,"").equals(""))
		{
			generalInfoDTO.setExpirationDate(strEdtExpirationDate);
		}
		else
		{
			generalInfoDTO.setExpirationDate("01/01/1900");
		}
		if(tempStoreBackImageName.size() != 0)
		{
			generalInfoDTO.setCheckBackImage(tempStoreBackImageName.get(0));
		}
		if(tempStoreFrontImageName.size() != 0)
		{
			generalInfoDTO.setCheckFrontImage(tempStoreFrontImageName.get(0));
		}

		oresidential_pest_DTO.setIsCompleted("true");
		//Log.e("strRadioInvoice...", "strRadioInvoice..."+strRadioInvoice);
		oresidential_pest_DTO.setPaymentType(strRadioInvoice);
		//residential_pest_DTO.setCheckNo(strEdttxtCheckNo);
		//residential_pest_DTO.setLicenseNo(strEdttxtDrivingLicence);
		oresidential_pest_DTO.setCreate_By(userDTO.getPestPackId());
		oresidential_pest_DTO.setUpdate_Date(CommonFunction.getCurrentDateTime());
		oresidential_pest_DTO.setCreate_date(CommonFunction.getCurrentDateTime());

		oresidential_pest_DTO.setTimeout(CommonFunction.getCurrentTime());
		oresidential_pest_DTO.setTimeOutDateTime(CommonFunction.getCurrentDateTime());
		//"010101"
		oresidential_pest_DTO.setTimeIn(generalInfoDTO.getTimeIn());
		oresidential_pest_DTO.setTimeInDateTime(generalInfoDTO.getTimeInDateTime());

		
	}
	public void getValuesFromComponents() 
	{
		// TODO Auto-generated method stub
		getValuesFromServiceForHeader();
		getValuesformAreasInspected();
		grtValuesforInspectedTreatedFor();
		getValueForoutsideOnlyServiceFor();
		getValuesPestActivitybyZone();
		getValuesPestControlServices();
		getValuesTermiteServices();
		getValuesRodentServices();
		getValuesMosquitoServices();
		getValuesForSchoolTreatment();
		getValuesForRecommendations();
		getValuesForInvoice();

	}
	private void getValuesForInvoice() 
	{
		// TODO Auto-generated method stub.getC
		strEdttxtTechComments=edtTechComment.getText().toString().trim();
		strEdttxtServiceTech=edtServiceTech.getText().toString().trim();
		strEdttxtEmp=edtEMP.getText().toString().trim();
		strEdttxtCustomer=edtCustomer.getText().toString().trim();
		//strEdttxtdate=edttxtdate.getText().toString();
		strEdttxtdate=generalInfoDTO.getServiceDate().trim();
		strEdttxtInvValue=edtIntValue.getText().toString().trim();
		strEdttxtProdValue=edtProductvalue.getText().toString().trim();
		strEdttxtTax=edtTaxvalue.getText().toString().trim();
		strEdttxtTotal=edtTotal.getText().toString().trim();

		//strEdttxtAmount=edttxtAmount.getText().toString().trim();
		if(!edtAmount.getText().toString().trim().equals(""))
		{
			strEdttxtAmount=edtAmount.getText().toString().trim();
		}
		else
		{
			strEdttxtAmount="0";
		}
		strEdttxtDrivingLicence=edtDRIVING_LICENCE.getText().toString().trim();
		strEdttxtCheckNo=edtCheckNo.getText().toString().trim();
		strEdtExpirationDate=edtEXPIRATION_DATE.getText().toString().trim();

		int idInvoiceRadio = radioGroupInvoice.getCheckedRadioButtonId();
		RadioButton radioButtonInvoice = (RadioButton) findViewById(idInvoiceRadio);
		strRadioInvoice= radioButtonInvoice.getText().toString().trim();
		//Log.e("strRadioInvoice..get value.",strRadioInvoice+"");
	}

	private void getValuesForRecommendations() 
	{
		// TODO Auto-generated method stub
		if(or_checkboxBranchHouse.isChecked())
		{
			strCheckTreeBranches = or_checkboxBranchHouse.getText().toString().trim();
			//Log.e("", "strCheckTreeBranches inside if"+strCheckTreeBranches);
		}
		else
		{
			strCheckTreeBranches="".trim();
		}
		if(or_checkboxFirewood.isChecked())
		{
			strCheckFirewood = or_checkboxFirewood.getText().toString().trim();
		}
		else
		{
			strCheckFirewood="".trim();
		}
		if(or_checkboxDebrisCrawl.isChecked())
		{
			strCheckDebrisInCrawl = or_checkboxDebrisCrawl.getText().toString().trim();
		}
		else
		{
			strCheckDebrisInCrawl="".trim();
		}
		if(or_checkboxExcessiveplant.isChecked())
		{
			strCheckExcessiveplant = or_checkboxExcessiveplant.getText().toString().trim();

		}
		else
		{
			strCheckExcessiveplant="".trim();
		}
		if(or_checkboxSoilAbove.isChecked())
		{
			strCheckSoilAbove = or_checkboxSoilAbove.getText().toString().trim();
		}
		else
		{
			strCheckSoilAbove="".trim();
		}
		if(or_checkboxWoodSoil.isChecked())
		{
			strCheckWoodSoil = or_checkboxWoodSoil.getText().toString().trim();

		}
		else
		{
			strCheckWoodSoil="".trim();
		}
		if(or_checkboxDebrisRoof.isChecked())
		{
			strCheckDebrisRoof = or_checkboxDebrisRoof.getText().toString().trim();
		}
		else
		{
			strCheckDebrisRoof="".trim();
		}
		if(or_checkboxStandingwater.isChecked())
		{
			strCheckStandingWater = or_checkboxStandingwater.getText().toString().trim();

		}
		else
		{
			strCheckStandingWater="".trim();
		}
		if(or_checkboxMoistureProblem.isChecked())
		{
			strCheckMoistureProblem = or_checkboxMoistureProblem.getText().toString().trim();

		}
		else
		{
			strCheckMoistureProblem="".trim();
		}
		if(or_checkboxOpeningspumbing.isChecked())
		{
			strCheckOpeningPlumbing = or_checkboxOpeningspumbing.getText().toString().trim();

		}
		else
		{
			strCheckOpeningPlumbing="".trim();
		}
		if(or_checkboxExcessiveGaps.isChecked())
		{
			strCheckExcessiveGaps = or_checkboxExcessiveGaps.getText().toString().trim();

		}
		else
		{
			strCheckExcessiveGaps="".trim();
		}
		if(or_checkboxLeackPumbing.isChecked())
		{
			strCheckLaekyPlumbing = or_checkboxLeackPumbing.getText().toString().trim();

		}
		else
		{
			strCheckLaekyPlumbing="".trim();
		}
		if(or_checkboxgarbageCans.isChecked())
		{
			strCheckGarbageCans = or_checkboxgarbageCans.getText().toString().trim();

		}
		else
		{
			strCheckGarbageCans="".trim();
		}
		if(or_checkboxMoisrtureDamage.isChecked())
		{
			strCheckMoistureDamage = or_checkboxMoisrtureDamage.getText().toString().trim();

		}
		else
		{
			strCheckMoistureDamage="".trim();
		}
		if(or_checkboxGroceryBags.isChecked())
		{
			strCheckGrocerybags = or_checkboxGroceryBags.getText().toString().trim();

		}
		else
		{
			strCheckGrocerybags="".trim();
		}
		if(or_checkboxPetFood.isChecked())
		{
			strCheckPetFood = or_checkboxPetFood.getText().toString().trim();

		}
		else
		{
			strCheckPetFood="".trim();
		}
		if(or_checkboxRecycledItems.isChecked())
		{
			strCheckRecycledItems = or_checkboxRecycledItems.getText().toString().trim();

		}
		else
		{
			strCheckRecycledItems="".trim();
		}
		if(or_checkboxExcessiveStorage.isChecked())
		{
			strCheckExcessiveStorage = or_checkboxExcessiveStorage.getText().toString().trim();

		}
		else
		{
			strCheckExcessiveStorage="".trim();
		}
	}
	private void getValuesForSchoolTreatment() 
	{
		strSchoolTreatment = "";
		for (int i = 0; i < treatmentOfSchoolCheckboxList.size(); i++) {
			// LoggerUtil.e("inside for loop 1..", "inside for loop 1..");
			if (treatmentOfSchoolCheckboxList.get(i).isChecked()) {

			strSchoolTreatment = strSchoolTreatment+ ","+ treatmentOfSchoolCheckboxList.get(i).getText().toString()
								.trim();
			}
		}
		strSchoolTreatment = CommonFunction.removeFirstCharIF(strSchoolTreatment, ",").trim();

	}
	private void getValuesMosquitoServices() 
	{
		// TODO Auto-generated method stub
		strMosquito = "";
		for (int i = 0; i < mosquitoCheckboxList.size(); i++) {
			// LoggerUtil.e("inside for loop 1..", "inside for loop 1..");
			if (mosquitoCheckboxList.get(i).isChecked()) {

				strMosquito = strMosquito
						+ ","
						+ mosquitoCheckboxList.get(i).getText().toString()
								.trim();

			}
		}
		strMosquito = CommonFunction.removeFirstCharIF(strMosquito, ",").trim();
		if (mosquitoCheckboxList.get(3).isChecked()) {
			strEdttxtMosquitoServices1 = edtMosquitoService1.getText()
					.toString().trim();
		} else {
			strEdttxtMosquitoServices1 = "";
		}
		if (mosquitoCheckboxList.get(7).isChecked()) {
			strEdttxtMosquitoServices2 = edtMosquitoService2.getText()
					.toString().trim();
		} else {
			strEdttxtMosquitoServices2 = "";
		}

	}
	private void getValuesRodentServices() 
	{
		if(or_checkboxBUILDERS_CONTRUCTION_GAP.isChecked())
		{
			strCheckBUILDERS_CONTRUCTION_GAP = or_checkboxBUILDERS_CONTRUCTION_GAP.getText().toString().trim();

			int idBUILDERS_CONTRUCTION_GAP = radioGroupFenceInspected.getCheckedRadioButtonId();
			if(idBUILDERS_CONTRUCTION_GAP != -1)
			{
				//Log.e("idFenceInspected", ""+idFenceInspected);
				RadioButton radioButton = (RadioButton) findViewById(idBUILDERS_CONTRUCTION_GAP);
				strRadioBUILDERS_CONTRUCTION_GAP = radioButton.getText().toString().trim();
			}
		}
		else
		{
			strCheckBUILDERS_CONTRUCTION_GAP="".trim();
		}
		
		if(or_checkboxSOFFIT_RETURNS.isChecked())
		{
			strCheckSOFFIT_RETURNS = or_checkboxSOFFIT_RETURNS.getText().toString().trim();

			int idSOFFITRETURNS = radioGroupFenceInspected.getCheckedRadioButtonId();
			if(idSOFFITRETURNS != -1)
			{
				//Log.e("idFenceInspected", ""+idFenceInspected);
				RadioButton radioButton = (RadioButton) findViewById(idSOFFITRETURNS);
				strRadioSOFFIT_RETURNS = radioButton.getText().toString().trim();
			}
		}
		else
		{
			strCheckSOFFIT_RETURNS="".trim();
		}
		
		if(or_checkboxBREEZYWAY_FROM_GARAGE.isChecked())
		{
			strCheckBREEZYWAY_FROM_GARAGE = or_checkboxBREEZYWAY_FROM_GARAGE.getText().toString().trim();

			int id = radioGroupBREEZYWAY_FROM_GARAGE.getCheckedRadioButtonId();
			if(id != -1)
			{
				//Log.e("idFenceInspected", ""+idFenceInspected);
				RadioButton radioButton = (RadioButton) findViewById(id);
				strRadioBREEZYWAY_FROM_GARAGE = radioButton.getText().toString().trim();
			}
		}
		else
		{
			strCheckBREEZYWAY_FROM_GARAGE="".trim();
		}
		
		if(or_checkboxROOF_VENTS.isChecked())
		{
			strCheckROOF_VENTS = or_checkboxROOF_VENTS.getText().toString().trim();

			int id = radioGroupROOF_VENTS.getCheckedRadioButtonId();
			if(id != -1)
			{
				//Log.e("idFenceInspected", ""+idFenceInspected);
				RadioButton radioButton = (RadioButton) findViewById(id);
				strRadioROOF_VENTS = radioButton.getText().toString().trim();
			}
		}
		else
		{
			strCheckROOF_VENTS="".trim();
		}
		
		if(or_checkboxGARAGE_DOOR.isChecked())
		{
			strCheckGARAGE_DOOR = or_checkboxGARAGE_DOOR.getText().toString().trim();

			int id = radioGroupGARAGE_DOOR.getCheckedRadioButtonId();
			if(id != -1)
			{
				//Log.e("idFenceInspected", ""+idFenceInspected);
				RadioButton radioButton = (RadioButton) findViewById(id);
				strRadioGARAGE_DOOR = radioButton.getText().toString().trim();
			}
		}
		else
		{
			strCheckGARAGE_DOOR="".trim();
		}
		if(or_checkboxSOFFIT_VENTS.isChecked())
		{
			strCheckSOFFIT_VENTS = or_checkboxSOFFIT_VENTS.getText().toString().trim();

			int id = radioGroupSOFFIT_VENTS.getCheckedRadioButtonId();
			if(id != -1)
			{
				//Log.e("idFenceInspected", ""+idFenceInspected);
				RadioButton radioButton = (RadioButton) findViewById(id);
				strRadioSOFFIT_VENTS = radioButton.getText().toString().trim();
			}
		}
		else
		{
			strCheckSOFFIT_VENTS ="".trim();
		}
		if(or_checkboxAC_DRYER_VENTS.isChecked())
		{
			strCheckAC_DRYER_VENTS = or_checkboxAC_DRYER_VENTS.getText().toString().trim();

			int id = radioGroupAC_DRYER_VENTS.getCheckedRadioButtonId();
			if(id != -1)
			{
				//Log.e("idFenceInspected", ""+idFenceInspected);
				RadioButton radioButton = (RadioButton) findViewById(id);
				strRadioAC_DRYER_VENTS = radioButton.getText().toString().trim();
			}
		}
		else
		{
			strCheckAC_DRYER_VENTS ="".trim();
		}
		
		if(or_checkboxNO_LIVEANIMAL_TRAPS_INSTALLES.isChecked())
		{
			strCheckNO_LIVEANIMAL_TRAPS_INSTALLES = or_checkboxNO_LIVEANIMAL_TRAPS_INSTALLES.getText().toString().trim();

			strNO_LIVEANIMAL_TRAPS_INSTALLES = edtNO_LIVEANIMAL_TRAPS_INSTALLES.getText().toString().trim();
		}
		else
		{
			strCheckNO_LIVEANIMAL_TRAPS_INSTALLES ="".trim();
		}
		if(or_checkboxSIDEING_HOUSE.isChecked())
		{
			strCheckSIDEING_HOUSE = or_checkboxSIDEING_HOUSE.getText().toString().trim();

			int id = radioGroupSIDEING_HOUSE.getCheckedRadioButtonId();
			if(id != -1)
			{
				//Log.e("idFenceInspected", ""+idFenceInspected);
				RadioButton radioButton = (RadioButton) findViewById(id);
				strRadioSIDEING_HOUSE = radioButton.getText().toString().trim();
			}
		}
		else
		{
			strCheckSIDEING_HOUSE ="".trim();
		}
		
		if(or_checkboxNumber_SNAPTRAPS.isChecked())
		{
			strCheckNumber_SNAPTRAPS = or_checkboxNumber_SNAPTRAPS.getText().toString().trim();

			strNumber_SNAPTRAPS = edtNumber_SNAPTRAPS.getText().toString().trim();
		}
		else
		{
			strCheckNumber_SNAPTRAPS ="".trim();
		}
		if(or_checkboxCRAWL_SPACE_VENTS.isChecked())
		{
			strCheckCRAWL_SPACE_VENTS = or_checkboxCRAWL_SPACE_VENTS.getText().toString().trim();

			int id = radioGroupCRAWL_SPACE_VENTS.getCheckedRadioButtonId();
			if(id != -1)
			{
				//Log.e("idFenceInspected", ""+idFenceInspected);
				RadioButton radioButton = (RadioButton) findViewById(id);
				strRadioCRAWL_SPACE_VENTS = radioButton.getText().toString().trim();
			}
		}
		else
		{
			strCheckCRAWL_SPACE_VENTS ="".trim();
		}
		
		if(or_checkboxCRAWL_SPACE_DOOR.isChecked())
		{
			strCheckCRAWL_SPACE_DOOR = or_checkboxCRAWL_SPACE_DOOR.getText().toString().trim();

			int id = radioGroupCRAWL_SPACE_DOOR.getCheckedRadioButtonId();
			if(id != -1)
			{
				//Log.e("idFenceInspected", ""+idFenceInspected);
				RadioButton radioButton = (RadioButton) findViewById(id);
				strRadioCRAWL_SPACE_DOOR = radioButton.getText().toString().trim();
			}
		}
		else
		{
			strCheckCRAWL_SPACE_DOOR ="".trim();
		}
	}
	private void getValuesTermiteServices() 
	{
		// TODO Auto-generated method stub
		if(or_checkboxFenceInspectedTermite.isChecked())
		{
			strCheckFenceInspected = or_checkboxFenceInspectedTermite.getText().toString().trim();

			int idFenceInspected = radioGroupFenceInspected.getCheckedRadioButtonId();
			if(idFenceInspected != -1)
			{
				//Log.e("idFenceInspected", ""+idFenceInspected);
				RadioButton radioButtonFence = (RadioButton) findViewById(idFenceInspected);
				strRadioFenceInspected = radioButtonFence.getText().toString().trim();
			}
		}
		else
		{
			strCheckFenceInspected="".trim();
		}
		
		if(or_checkboxNumberBaitingDevicesInspectedTermite.isChecked())
		{
			strCheckNumberBaitingDevicesInspectedTermite = or_checkboxNumberBaitingDevicesInspectedTermite.getText().toString().trim();

			strBaitingDevicesInspected = edtNUMBER_OF_BAITING_DEVICES_INSPECTED.getText().toString().trim();
		}
		else
		{
			strCheckNumberBaitingDevicesInspectedTermite="".trim();
		}
		if(or_checkboxBaitingDevicesTermite.isChecked())
		{
			strCheckBaitingDevice = or_checkboxBaitingDevicesTermite.getText().toString().trim();

			int idBaitingDevice = radioGroupBaitingDevices.getCheckedRadioButtonId();
			if(idBaitingDevice != -1)
			{
				//Log.e("idFenceInspected", ""+idFenceInspected);

				RadioButton radioButton = (RadioButton) findViewById(idBaitingDevice);
				strRadioBaitingDevice = radioButton.getText().toString().trim();
			}
		}
		else
		{
			strCheckBaitingDevice="".trim();
		}
		if(or_checkboxTermiteBaitingDevicereplacedTermite.isChecked())
		{
			strCheckTermiteBaitingDevicereplaced = or_checkboxTermiteBaitingDevicereplacedTermite.getText().toString().trim();

			int idBaitingDevice = radioGroupTERMITE_BAITING_DEVICES_REPLACED.getCheckedRadioButtonId();
			if(idBaitingDevice != -1)
			{
				//Log.e("idFenceInspected", ""+idFenceInspected);
				RadioButton radioButton = (RadioButton) findViewById(idBaitingDevice);
				strRadioBaitingDevicereplaced = radioButton.getText().toString().trim();
			}
		}
		else
		{
			strCheckTermiteBaitingDevicereplaced="".trim();
		}
		
		if(or_checkboxExteriorPerimeterofhouseTermite.isChecked())
		{
			strCheckExteriorPerimeterofhouseTermite = or_checkboxExteriorPerimeterofhouseTermite.getText().toString().trim();

			int idExterior = radioGroupExteriorPerimeterofhouse.getCheckedRadioButtonId();
			if(idExterior != -1)
			{
				//Log.e("idFenceInspected", ""+idFenceInspected);
				RadioButton radioButton = (RadioButton) findViewById(idExterior);
				strRadioExteriorPerimeterofhouseTermite = radioButton.getText().toString().trim();
			}
		}
		else
		{
			strCheckExteriorPerimeterofhouseTermite="".trim();
		}
		if(or_checkboxATTICTermite.isChecked())
		{
			strCheckATTICTermite = or_checkboxATTICTermite.getText().toString().trim();

			int idATTICT = radioGroupAttic.getCheckedRadioButtonId();
			if(idATTICT != -1)
			{
				//Log.e("idFenceInspected", ""+idFenceInspected);
				RadioButton radioButton = (RadioButton) findViewById(idATTICT);
				strRadioATTICTermite = radioButton.getText().toString().trim();
			}
		}
		else
		{
			strCheckATTICTermite="".trim();
		}
		if(or_checkboxINTERIOROFHOUSETermite.isChecked())
		{
			strCheckINTERIOROFHOUSETermite = or_checkboxINTERIOROFHOUSETermite.getText().toString().trim();

			int idINTERIOROFHOUSETermite = radioGroupINTERIOROFHOUSE.getCheckedRadioButtonId();
			if(idINTERIOROFHOUSETermite != -1)
			{
				//Log.e("idFenceInspected", ""+idFenceInspected);
				RadioButton radioButton = (RadioButton) findViewById(idINTERIOROFHOUSETermite);
				strRadioINTERIOROFHOUSETermite = radioButton.getText().toString().trim();
			}
		}
		else
		{
			strCheckINTERIOROFHOUSETermite ="".trim();
		}
		if(or_checkbox_Basement_Termite.isChecked())
		{
			strCheckBasementTermite = or_checkbox_Basement_Termite.getText().toString().trim();

			strBasementTermite = edt_Basement_TermiteServices.getText().toString().trim();
		}
		else
		{
			strBasementTermite="".trim();
		}
	}
	private void getValuesPestControlServices() 
	{
		// TODO Auto-generated method stub
		strpestControlServices="";
		strOutsidepestControlServices="";
		strInsidepestControlServices="";

		for(int i=0;i < pestControlServicesCheckboxList.size();i++)
		{
			//LoggerUtil.e("inside for loop 1..", "inside for loop 1..");
			if(pestControlServicesCheckboxList.get(i).isChecked())
			{
				strpestControlServices=strpestControlServices+","+pestControlServicesCheckboxList.get(i).getText().toString();	
			}
		}
		strpestControlServices=CommonFunction.removeFirstCharIF(strpestControlServices, ",").trim();
		
		for(int i=0;i < OutsidepestControlServicesCheckboxList.size();i++)
		{
			//LoggerUtil.e("inside for loop 1..", "inside for loop 1..");
			if(OutsidepestControlServicesCheckboxList.get(i).isChecked())
			{
				strOutsidepestControlServices=strOutsidepestControlServices+","+OutsidepestControlServicesCheckboxList.get(i).getText().toString();	
			}
		}
		strOutsidepestControlServices=CommonFunction.removeFirstCharIF(strOutsidepestControlServices, ",").trim();
	
		for(int i=0;i < InsidepestControlServicesCheckboxList.size();i++)
		{
			//LoggerUtil.e("inside for loop 1..", "inside for loop 1..");
			if(InsidepestControlServicesCheckboxList.get(i).isChecked())
			{
				strInsidepestControlServices=strInsidepestControlServices+","+InsidepestControlServicesCheckboxList.get(i).getText().toString();	
			}
		}
		strInsidepestControlServices=CommonFunction.removeFirstCharIF(strInsidepestControlServices, ",").trim();
	}
	private void getValuesPestActivitybyZone() 
	{
		// TODO Auto-generated method stub
		strpestActivityByZone="";
		for(int i=0;i < PestActivityZoneCheckboxList.size();i++)
		{
			//LoggerUtil.e("inside for loop 1..", "inside for loop 1..");
			//strpestActivityByZone="";
			if(PestActivityZoneCheckboxList.get(i).isChecked())
			{
				strpestActivityByZone=strpestActivityByZone+","+PestActivityZoneCheckboxList.get(i).getText().toString();	
			}
		}
		strpestActivityByZone=CommonFunction.removeFirstCharIF(strpestActivityByZone, ",").trim();
		//	Log.e("", "strpestActivityByZone..get."+strpestActivityByZone);
		strEdttxtFenceLinesByZone = edtFenceLinesByZone.getText().toString().trim();
		strEdttxtFrontyardByZone= edtFrontYardByZone.getText().toString().trim();
		strEdttxtBackYardByZone = edtBackyardByZone.getText().toString().trim();
		strEdttxtExteriorPerimeterHouseByZone = edtExteriorParaByZone.getText().toString().trim();
		strEdttxtRoofGutterLine = edtRoofGutterByZone.getText().toString().trim();
		strEdttxtGarageBYZone = edtGarageByZone.getText().toString().trim();
		strEdttxtAtticByZone = edtAtticPestByZone.getText().toString().trim();
		strEdttxtInteriorOfHouseByZone = edtInteriorHouseByZone.getText().toString().trim();
		strEdttxtBasement = edt_Basement_PestActivityByZone.getText().toString().trim();
	}
	private void getValueForoutsideOnlyServiceFor() 
	{
		// TODO Auto-generated method stub
		strOutsideService="";
		if(or_checkboxOutSideService.isChecked())
		{
			strOutsideService = "true";
		}
	}
	private void grtValuesforInspectedTreatedFor() 
	{
		// TODO Auto-generated method stub
		strInspectedadnTreatedFor="";
		for(int i=0;i < InspectedTreatedForCheckboxList.size();i++)
		{
			if(InspectedTreatedForCheckboxList.get(i).isChecked())
			{
				strInspectedadnTreatedFor=strInspectedadnTreatedFor+","+InspectedTreatedForCheckboxList.get(i).getText().toString();	
			}
		}
		strInspectedadnTreatedFor=CommonFunction.removeFirstCharIF(strInspectedadnTreatedFor, ",").trim();
		//	LoggerUtil.e("inside for loop 1..", "strInspectedadnTreatedFor...."+strInspectedadnTreatedFor);
		//strAnts=checkBoxAnts.getText().toString();
		//strOtherPest=checkBoxOtherPests.getText().toString();
		strInspectedadnTreatedFor1=edttxtInspectedandTreatedFor1.getText().toString().trim();
		strInspectedadnTreatedFor2=edttxtInspectedandTreatedFor2.getText().toString().trim();
		//	Log.e("", "strEdttxtInspectedandTreatedFor1...."+strEdttxtInspectedandTreatedFor1+"..."+strEdttxtInspectedandTreatedFor2);
	}
	
	private void getValuesformAreasInspected() 
	{
		// TODO Auto-generated method stub
		strAreasInspected="";
		for(int i=0;i < AreasInspectedCheckboxList.size();i++)
		{
			//LoggerUtil.e("inside for loop 1..", "inside for loop 1..");
			if(AreasInspectedCheckboxList.get(i).isChecked())
			{
				strAreasInspected=strAreasInspected+","+AreasInspectedCheckboxList.get(i).getText().toString();	
			}
		}
		strAreasInspected=CommonFunction.removeFirstCharIF(strAreasInspected, ",").trim();
	}
	private void getValuesFromServiceForHeader() 
	{
		// TODO Auto-generated method stub
		strServiceFor="";
		for(int i=0;i < serviceForCheckboxList.size();i++)
		{
			//	LoggerUtil.e("inside for loop 1..", "inside for loop 1..");
			if(serviceForCheckboxList.get(i).isChecked())
			{

				strServiceFor=strServiceFor+","+serviceForCheckboxList.get(i).getText().toString();	
			}
		}
		strServiceFor=CommonFunction.removeFirstCharIF(strServiceFor, ",").trim();
	}
}
