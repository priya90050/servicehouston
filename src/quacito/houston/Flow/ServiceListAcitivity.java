package quacito.houston.Flow;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import quacito.houston.CommonUtilities.CommonFunction;
import quacito.houston.CommonUtilities.LoggerUtil;
import quacito.houston.CommonUtilities.ParameterUtil;
import quacito.houston.CommonUtilities.SharedPreference;
import quacito.houston.DBhelper.DatabaseHelper;
import quacito.houston.DBhelper.HoustonFlowFunctions;
import quacito.houston.Flow.servicehouston.R;
import quacito.houston.adapter.ManageServiceListAdapter;
import quacito.houston.model.AddressDTO;
import quacito.houston.model.AgreementMailIdDTO;
import quacito.houston.model.ChemicalDto;
import quacito.houston.model.Chemical_WhenNoRecordDTO;
import quacito.houston.model.GetAllRouteDTO;
import quacito.houston.model.GetAssignCrewMemberDTO;
import quacito.houston.model.GetAssignedLeadDTO;
import quacito.houston.model.GetRouteCrewDTO;
import quacito.houston.model.HCommercial_pest_DTO;
import quacito.houston.model.CrewMemberDTO;
import quacito.houston.model.EmployeeDTO;
import quacito.houston.model.GeneralInfoDTO;
import quacito.houston.model.LogDTO;
import quacito.houston.model.OResidential_pest_DTO;
import quacito.houston.model.PreventionDTO;
import quacito.houston.model.HResidential_pest_DTO;
import quacito.houston.model.Selected_crewDTO;
import quacito.houston.model.UserDTO;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.splunk.mint.ExceptionHandler;
import com.splunk.mint.Mint;

public class ServiceListAcitivity extends Activity {
	Button buttonGo;
	long mLastClickTime = 0;
	ImageButton btnClose;
	EditText edtFromdate/* , edtTodate */;
	EditText edtfirstName, edtlastName, edtemailAddress, edtaccountNo,
			edtaddress, edtcity, edtstate, edtworkOrder;
	Spinner spnserviceStatus, spnserviceBranch, spnToDate;
	static UserDTO userDTO;
	LinearLayout filter_criteria_layout;
	ArrayList<GeneralInfoDTO> serviceList;
	String userId = "361", firstName = "", lastName = "", emailAddress = "",
			accountNo = "", address = "", city = "", state = "",
			serviceStatus = "", strserviceBranch = "", techName = "",
			techId = "", fromDate = "", toDate = "", workOrder = "";
	// LinearLayout layouytAdapter;
	ListView listView;
	String[] spinnerDateArray = new String[2];
	HoustonFlowFunctions houstonFlowFunctions;
	private final String NAMESPACE = CommonFunction.IPAD_ABC;
	private final String URL = CommonFunction.IPAD_ABC
			+ "Houston_Service.asmx?op";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.activity_servicelist);
		super.onCreate(savedInstanceState);
		
		Mint.initAndStartSession(ServiceListAcitivity.this, "e53b080c");
		// Mint.setLogging(100);
		// Mint.enableLogging(true);
		Thread.setDefaultUncaughtExceptionHandler(new TopExceptionHandler(this));
		initialize();
		if(savedInstanceState != null)
		{
			listView.setEnabled(true);
		}
		else
		{
			listView.setEnabled(true);
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		edtFromdate.setText(dateFormat.format(new Date()));
		getToDate();
		ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
				this, android.R.layout.simple_spinner_item, spinnerDateArray);
		spinnerArrayAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spnToDate.setAdapter(spinnerArrayAdapter);
		fromDate = edtFromdate.getText().toString().trim();
		toDate = spnToDate.getSelectedItem().toString();
		houstonFlowFunctions = new HoustonFlowFunctions(getApplicationContext());

		// Log.e("getTommorrowsDate", "...."+getTommorrowsDate());

		if (CommonFunction.haveInternet(ServiceListAcitivity.this)) {
			SyncAllData();
		} else {
			clearAllFilterCriteria();
			setRecordtoServiceList();
		}
		registerReceiver();
		
	}

	private void getToDate() {
		// TODO Auto-generated method stub
		// get a calendar instance, which defaults to "now"
		Calendar calendar = Calendar.getInstance();

		// get a date to represent "today"
		Date today = calendar.getTime();
		// add one day to the date/calendar
		calendar.add(Calendar.DAY_OF_YEAR, 1);

		// now get "tomorrow"
		Date tomorrow = calendar.getTime();

		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

		String todayAsString = dateFormat.format(today);
		String tomorrowAsString = dateFormat.format(tomorrow);
		// print out tomorrow's date
		spinnerDateArray[0] = todayAsString;
		spinnerDateArray[1] = tomorrowAsString;
	}

	IntentFilter intentFilter;
	BroadcastReceiver intentReceiver;

	public void registerReceiver() {
		intentFilter = new IntentFilter();
		intentFilter.addAction("REFRESH_DATA_ACTION");
		intentReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				serviceList.clear();
				clearAllFilterCriteria();
				JSONArray searchArray = houstonFlowFunctions
						.getAllServicelist(spnserviceStatus.getSelectedItem()
								.toString());
				serviceList = new Gson().fromJson(searchArray.toString(),
						new TypeToken<List<GeneralInfoDTO>>() {
						}.getType());
				ManageServiceListAdapter manageServiceListAdapter = new ManageServiceListAdapter(
						serviceList, ServiceListAcitivity.this, userDTO);
				listView.setAdapter(manageServiceListAdapter);
				if (spnserviceStatus != null) {
					spnserviceStatus.setSelection(0);
				}

			}
		};
		registerReceiver(intentReceiver, intentFilter);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onPause()
	 */
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		if (intentReceiver != null) {
			unregisterReceiver(intentReceiver);
			intentReceiver = null;
		}
		super.onPause();
	}


	  @Override protected void onResume() {
	  //Toast.makeText(getApplicationContext(), "On Resume",
	    //Toast.LENGTH_LONG).show();
	  listView.setEnabled(true);
	  super.onResume(); }
	 

	private void SyncAllData() {
		// TODO Auto-generated method stub
		if (CommonFunction.haveInternet(ServiceListAcitivity.this)) {

			POSTAll_Data_ServiceNewID post_data_ServiceNewID = new POSTAll_Data_ServiceNewID();
			post_data_ServiceNewID.execute("");

			/*
			 * .PostLogData postLogData = new PostLogData();
			 * postLogData.execute("");
			 */
		} else {
			CommonFunction.AboutBox(ServiceListAcitivity.this.getResources()
					.getString(R.string.no_internet_connection),
					ServiceListAcitivity.this);
			setRecordtoServiceList();
		}
	}

	private void getAllRecordFromServer() {

		DownloadAll_General_Information downloadAll_General_Information = new DownloadAll_General_Information();
		downloadAll_General_Information
				.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		// Residential Async task is calling in general info onPost execute.
		// commercial Async task is calling in Residential info onPost execute.
		// orlando Async task is calling in Residential info onPost execute.

		DownloadAll_Employee_Detail downloadAll_Employee_Detail = new DownloadAll_Employee_Detail();
		downloadAll_Employee_Detail
				.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

		DownloadAll_Chemical downloadAll_DownloadAll_Chemical = new DownloadAll_Chemical();
		downloadAll_DownloadAll_Chemical
				.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

		DownloadAll_Chemical_WhenNoRecord downloadAll_Download_Chemical_WhenNoRecord = new DownloadAll_Chemical_WhenNoRecord();
		downloadAll_Download_Chemical_WhenNoRecord
				.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

		DownloadAll_Crew_Members downloadAll_Crew_Members = new DownloadAll_Crew_Members();
		downloadAll_Crew_Members
				.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	public void setRecordtoServiceList() {
		// Log.e("setRecordtoServiceList",
		// "spnserviceStatus.getSelectedItem().toString()...."+spnserviceStatus.getSelectedItem().toString());
		/*
		 * JSONArray searchArray =
		 * houstonFlowFunctions.getAllServicelist(spnserviceStatus
		 * .getSelectedItem().toString());
		 * 
		 * serviceList = new Gson().fromJson(searchArray.toString(), new
		 * TypeToken<List<GeneralInfoDTO>>() { }.getType());
		 */
		// LoggerUtil.e("setRecordtoServiceList",
		// "serviceList..."+serviceList.size());

		setData();
		// setRecordtoServiceList();
		// Log.e("workOrder", workOrder);
		JSONArray searchArray = houstonFlowFunctions.getSelectedServicelist(
				firstName, lastName, emailAddress, accountNo, address, city,
				state, techName, techId, fromDate, toDate, workOrder,
				serviceStatus, strserviceBranch);
		serviceList = new Gson().fromJson(searchArray.toString(),
				new TypeToken<List<GeneralInfoDTO>>() {
				}.getType());

		if (serviceList.size() == 0) {
			noRecordFound();
		} else {
			recordFound();
		}
		ManageServiceListAdapter manageServiceListAdapter = new ManageServiceListAdapter(
				serviceList, ServiceListAcitivity.this, userDTO);
		listView.setAdapter(manageServiceListAdapter);
	}

	private final String SOAP_ACTION_Get_Employee_Detail = CommonFunction.IPAD_ABC
			+ "All_Employee_List";
	private final String METHOD_NAME_Get_Employee_Detail = "All_Employee_List";
	ProgressDialog progressDialogGet_Employee_Detail;

	class DownloadAll_Employee_Detail extends AsyncTask<String, String, String> {
		@Override
		protected String doInBackground(String... params) {
			return downloadAll_Employee_Detail();
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			String downloading = getResources().getString(
					R.string.loading_employee_details);
			String pleasewait = getResources().getString(R.string.please_wait);
			progressDialogGet_Employee_Detail = ProgressDialog.show(
					ServiceListAcitivity.this, downloading, pleasewait, true); // TODO

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if ((progressDialogGet_Employee_Detail != null)
					&& progressDialogGet_Employee_Detail.isShowing()) {
				progressDialogGet_Employee_Detail.dismiss();
				progressDialogGet_Employee_Detail = null;
			}
			// ./setRecordtoServiceList();

		}
	}

	public String downloadAll_Employee_Detail() {
		SoapObject request = new SoapObject(NAMESPACE,
				METHOD_NAME_Get_Employee_Detail);

		PropertyInfo UserName = new PropertyInfo();
		UserName.setName(ParameterUtil.Username);
		UserName.setValue("rahul");
		request.addProperty(UserName);

		PropertyInfo FirstName = new PropertyInfo();
		FirstName.setName(ParameterUtil.FirstName);
		FirstName.setValue("");
		request.addProperty(FirstName);

		PropertyInfo LastName = new PropertyInfo();
		LastName.setName(ParameterUtil.LastName);
		LastName.setValue("");
		request.addProperty(LastName);

		PropertyInfo Email = new PropertyInfo();
		Email.setName(ParameterUtil.Email);
		Email.setValue("");
		request.addProperty(Email);

		PropertyInfo Role = new PropertyInfo();
		Role.setName(ParameterUtil.Role);
		Role.setValue("");
		request.addProperty(Role);

		PropertyInfo Status = new PropertyInfo();
		Status.setName(ParameterUtil.Status);
		Status.setValue("");
		request.addProperty(Status);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport
					.call(SOAP_ACTION_Get_Employee_Detail, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			String res = response.toString();
			//LoggerUtil.e("resp from all employees", "employees..." + res);
			try {
				JSONArray jsServiceDataByServiceNewID = new JSONArray(res);
				if (jsServiceDataByServiceNewID.length() > 0) {
					houstonFlowFunctions
							.deleteAllTableData(DatabaseHelper.TABLE_EMPLOYEE_INFO);
				}
				ArrayList<EmployeeDTO> serviceDataList = new Gson().fromJson(
						jsServiceDataByServiceNewID.toString(),
						new TypeToken<List<EmployeeDTO>>() {
						}.getType());
				jsServiceDataByServiceNewID = new JSONArray(
						new Gson().toJson(serviceDataList));
				houstonFlowFunctions.insertMultipleRecords(
						jsServiceDataByServiceNewID,
						DatabaseHelper.TABLE_EMPLOYEE_INFO);

			} catch (Exception e) {
				LoggerUtil.e("Exception", e.toString());
			}
			return res;
		} catch (IOException e) {
			LoggerUtil.e("IOException", e.toString());
		} catch (XmlPullParserException e) {
			LoggerUtil.e("Get_Service_Data_By_ServiceNewID", e.toString());
		}
		return "[{\"result\":\"No Record Found\"}]";
	}

	private final String SOAP_ACTION_Get_Crew_Member = CommonFunction.IPAD_ABC
			+ "Mobile_Get_CrewMember";
	private final String METHOD_NAME_Get_Crew_Member = "Mobile_Get_CrewMember";
	ProgressDialog progressDialogGet_Crew_Member;

	class DownloadAll_Crew_Members extends AsyncTask<String, String, String> {
		@Override
		protected String doInBackground(String... params) {
			return downloadAll_Crew_Member();
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			String downloading = getResources().getString(
					R.string.loading_crew_membwers);
			String pleasewait = getResources().getString(R.string.please_wait);
			progressDialogGet_Crew_Member = ProgressDialog.show(
					ServiceListAcitivity.this, downloading, pleasewait, true); // TODO

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if ((progressDialogGet_Crew_Member != null)
					&& progressDialogGet_Crew_Member.isShowing()) {
				progressDialogGet_Crew_Member.dismiss();
				progressDialogGet_Crew_Member = null;
			}
			// ./setRecordtoServiceList();

		}
	}

	public String downloadAll_Crew_Member() {
		SoapObject request = new SoapObject(NAMESPACE,
				METHOD_NAME_Get_Crew_Member);

		PropertyInfo EmpId = new PropertyInfo();
		EmpId.setName(ParameterUtil.EmpId);
		EmpId.setValue(userDTO.getEmployeeId());
		request.addProperty(EmpId);

		PropertyInfo SDate = new PropertyInfo();
		SDate.setName(ParameterUtil.SDate);
		// SDate.setValue(getCurrentDate());
		SDate.setValue(getCurrentDate());
		request.addProperty(SDate);

		PropertyInfo Edate = new PropertyInfo();
		Edate.setName(ParameterUtil.Edate);
		// Edate.setValue(getCurrentDate());
		Edate.setValue(getTommorrowsDate());
		request.addProperty(Edate);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		try {
			androidHttpTransport.call(SOAP_ACTION_Get_Crew_Member, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			String res = response.toString();
			//LoggerUtil.e("resp from all crew", "crew..." + res);
			try {
				JSONArray jsServiceDataByServiceNewID = new JSONArray(res);
				if (jsServiceDataByServiceNewID.length() > 0) {
					houstonFlowFunctions
							.deleteAllTableData(DatabaseHelper.TABLE_CREW_MEMBER);
				}
				ArrayList<CrewMemberDTO> serviceDataList = new Gson().fromJson(
						jsServiceDataByServiceNewID.toString(),
						new TypeToken<List<CrewMemberDTO>>() {
						}.getType());
				jsServiceDataByServiceNewID = new JSONArray(
						new Gson().toJson(serviceDataList));
				houstonFlowFunctions.insertMultipleRecords(
						jsServiceDataByServiceNewID,
						DatabaseHelper.TABLE_CREW_MEMBER);
			} catch (Exception e) {
				LoggerUtil.e("Exception in crew", e.toString());
			}
			return res;
		} catch (IOException e) {
			LoggerUtil.e("IOException", e.toString());
		} catch (XmlPullParserException e) {
			LoggerUtil.e("Get_Service_Data_By_ServiceNewID", e.toString());
		}
		return "[{\"result\":\"No Record Found\"}]";
	}

	private final String SOAP_ACTION_Get_PreventionData = "http://myanteater.com/Get_ResidentialServiceDetail";
	private final String METHOD_NAME_Get_PreventionData = "Get_ResidentialServiceDetail";
	ProgressDialog progressDialogGet_PreventionData;

	class DownloadAll_Prevention_Data extends AsyncTask<String, String, String> {
		@Override
		protected String doInBackground(String... params) {
			return downloadAll_Prevention_Data();
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			String downloading = getResources().getString(
					R.string.loading_prevention_information);
			String pleasewait = getResources().getString(R.string.please_wait);
			progressDialogGet_PreventionData = ProgressDialog.show(
					ServiceListAcitivity.this, downloading, pleasewait, true); // TODO

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				progressDialogGet_PreventionData.dismiss();
				progressDialogGet_PreventionData = null;
			} catch (Exception e) {
				Log.e("", "Exception" + e);
			}

		}
	}

	public String downloadAll_Prevention_Data() {
		SoapObject request = new SoapObject(NAMESPACE,
				METHOD_NAME_Get_PreventionData);

		PropertyInfo FDate = new PropertyInfo();
		FDate.setName("");
		FDate.setValue(fromDate);
		request.addProperty(FDate);

		PropertyInfo EDate = new PropertyInfo();
		EDate.setName("");
		EDate.setValue(toDate);
		request.addProperty(EDate);

		PropertyInfo UserID = new PropertyInfo();
		UserID.setName("");
		UserID.setValue(userId);
		request.addProperty(UserID);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(SOAP_ACTION_Get_PreventionData, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			String res = response.toString();
			LoggerUtil.e("ServiceDataByServiceNewID1", res + "");
			try {
				JSONArray jsServiceDataByServiceNewID = new JSONArray(res);
				if (jsServiceDataByServiceNewID.length() > 0) {
					houstonFlowFunctions
							.deleteAllTableData(DatabaseHelper.TABLE_PREVENTION_INFO);
				}
				ArrayList<PreventionDTO> serviceDataList = new Gson().fromJson(
						jsServiceDataByServiceNewID.toString(),
						new TypeToken<List<PreventionDTO>>() {
						}.getType());
				jsServiceDataByServiceNewID = new JSONArray(
						new Gson().toJson(serviceDataList));
				houstonFlowFunctions.insertMultipleRecords(
						jsServiceDataByServiceNewID,
						DatabaseHelper.TABLE_PREVENTION_INFO);

			} catch (Exception e) {
				LoggerUtil.e("Exception", e.toString());
			}
			return res;
		} catch (IOException e) {
			LoggerUtil.e("IOException", e.toString());
		} catch (XmlPullParserException e) {
			LoggerUtil.e("Get_Service_Data_By_ServiceNewID", e.toString());
		}
		return "[{\"result\":\"No Record Found\"}]";
	}

	//private final String SOAP_ACTION_Get_Chemical_WhenNoRecord = CommonFunction.IPAD_ABC
	//		+ "Get_All_Chemical_Product";
	//private final String METHOD_NAME_Get_Chemical_WhenNoRecord = "Get_All_Chemical_Product";
	 private final String SOAP_ACTION_Get_Chemical_WhenNoRecord =
	 CommonFunction.IPAD_ABC
	 + "Get_All_Commercial_Chemical_Product";
	 private final String METHOD_NAME_Get_Chemical_WhenNoRecord =
	 "Get_All_Commercial_Chemical_Product";

	ProgressDialog progressDialogGet_Chemical_WhenNoRecord;

	class DownloadAll_Chemical_WhenNoRecord extends
			AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			String downloading = getResources().getString(
					R.string.loading_chemical_information);

			String pleasewait = getResources().getString(R.string.please_wait);
			progressDialogGet_Chemical_WhenNoRecord = ProgressDialog.show(
					ServiceListAcitivity.this, downloading, pleasewait, true); // TODO

		}

		@Override
		protected String doInBackground(String... params) {
			return downloadAll_Chemicals_WhenNoRecord();
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if ((progressDialogGet_Chemical_WhenNoRecord != null)
					&& progressDialogGet_Chemical_WhenNoRecord.isShowing()) {
				progressDialogGet_Chemical_WhenNoRecord.dismiss();
				progressDialogGet_Chemical_WhenNoRecord = null;
			}

		}
	}

	public String downloadAll_Chemicals_WhenNoRecord() {
		SoapObject request = new SoapObject(NAMESPACE,
				METHOD_NAME_Get_Chemical_WhenNoRecord);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(SOAP_ACTION_Get_Chemical_WhenNoRecord,
					envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			String res = response.toString();
			//LoggerUtil.e("resp from data chemical when no record..", res + "");
			try {
				JSONArray jsServiceDataByServiceNewID = new JSONArray(res);
				if (jsServiceDataByServiceNewID.length() > 0) {
					houstonFlowFunctions
							.deleteAllTableData(DatabaseHelper.TABLE_CHEMICALS_WHEN_NO_RECORD);
				}
				ArrayList<Chemical_WhenNoRecordDTO> serviceDataList = new Gson()
						.fromJson(
								jsServiceDataByServiceNewID.toString(),
								new TypeToken<List<Chemical_WhenNoRecordDTO>>() {
								}.getType());
				jsServiceDataByServiceNewID = new JSONArray(
						new Gson().toJson(serviceDataList));
				houstonFlowFunctions.insertMultipleRecords(
						jsServiceDataByServiceNewID,
						DatabaseHelper.TABLE_CHEMICALS_WHEN_NO_RECORD);

			} catch (Exception e) {
				LoggerUtil.e("Exception", e.toString());
			}

			return res;
		} catch (IOException e) {
			LoggerUtil.e("IOException", e.toString());
		} catch (XmlPullParserException e) {
			LoggerUtil.e("Get_Service_Data_By_ServiceNewID..Chemicals..",
					e.toString());
		}
		return "[{\"result\":\"No Record Found\"}]";
	}

	//private final String SOAP_ACTION_Get_Chemical = CommonFunction.IPAD_ABC
	//		+ "Get_Mobile_usp_Get_All_Chemicals";
	//private final String METHOD_NAME_Get_Chemical = "Get_Mobile_usp_Get_All_Chemicals";
	 private final String SOAP_ACTION_Get_Chemical = CommonFunction.IPAD_ABC
	 + "Get_Mobile_Commercial_usp_Get_All_Chemicals";
	 private final String METHOD_NAME_Get_Chemical =
	 "Get_Mobile_Commercial_usp_Get_All_Chemicals";
	ProgressDialog progressDialogGet_Chemical;

	class DownloadAll_Chemical extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			String downloading = getResources().getString(
					R.string.loading_chemical_information);
			String pleasewait = getResources().getString(R.string.please_wait);
			progressDialogGet_Chemical = ProgressDialog.show(
					ServiceListAcitivity.this, downloading, pleasewait, true); // TODO

		}

		@Override
		protected String doInBackground(String... params) {
			return downloadAll_Chemicals();
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if ((progressDialogGet_Chemical != null)
					&& progressDialogGet_Chemical.isShowing()) {
				progressDialogGet_Chemical.dismiss();
				progressDialogGet_Chemical = null;
			}
		}
	}

	public String downloadAll_Chemicals() {
		SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_Get_Chemical);

		PropertyInfo EmpId = new PropertyInfo();
		EmpId.setName(ParameterUtil.EmpId);
		EmpId.setValue(userDTO.getEmployeeId());
		request.addProperty(EmpId);

		PropertyInfo SDate = new PropertyInfo();
		SDate.setName(ParameterUtil.SDate);
		SDate.setValue(getCurrentDate());
		request.addProperty(SDate);

		PropertyInfo Edate = new PropertyInfo();
		Edate.setName(ParameterUtil.Edate);
		Edate.setValue(getTommorrowsDate());
		request.addProperty(Edate);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(SOAP_ACTION_Get_Chemical, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			String res = response.toString();
			//LoggerUtil.e("resp from chemical ..", res + "");
			try {
				JSONArray jsServiceDataByServiceNewID = new JSONArray(res);
				if (jsServiceDataByServiceNewID.length() > 0) {
					 houstonFlowFunctions
					.deleteAllTableData(DatabaseHelper.TABLE_CHEMICALS);
					houstonFlowFunctions.deleteSelectedTableData(
							DatabaseHelper.TABLE_CHEMICALS,
							ParameterUtil.isCompleted, "");
					houstonFlowFunctions.deleteSelectedTableData(
							DatabaseHelper.TABLE_CHEMICALS,
							ParameterUtil.isCompleted, "0");

					ArrayList<ChemicalDto> serviceDataList = new Gson()
							.fromJson(jsServiceDataByServiceNewID.toString(),
									new TypeToken<List<ChemicalDto>>() {
									}.getType());
					jsServiceDataByServiceNewID = new JSONArray(
							new Gson().toJson(serviceDataList));

					for (int i = 0; i < serviceDataList.size(); i++) {
						ChemicalDto chemicalDto = serviceDataList.get(i);
						boolean isAlreadyAdded = houstonFlowFunctions
								.ischemicalAlreadyAdded(
										chemicalDto.getServiceID_new(),
										chemicalDto.getPreventationID(),
										DatabaseHelper.TABLE_CHEMICALS);
						if (!isAlreadyAdded) {
							long ch = houstonFlowFunctions
									.addChemicals(jsServiceDataByServiceNewID
											.getJSONObject(i));
							// Log.e("Chemical Added", ch + "");
						} else {
							// Log.e("Aready existsChemical Added","Exists");
						}
					}

					// houstonFlowFunctions.insertMultipleRecords(jsServiceDataByServiceNewID,
					// DatabaseHelper.TABLE_CHEMICALS);
				}
			} catch (Exception e) {
				LoggerUtil.e("Exception", e.toString());
			}

			return res;
		} catch (IOException e) {
			LoggerUtil.e("IOException", e.toString());
		} catch (XmlPullParserException e) {
			LoggerUtil.e("Get_Service_Data_By_ServiceNewID..Chemicals..",
					e.toString());
		}
		return "[{\"result\":\"No Record Found\"}]";
	}

	private final String SOAP_ACTION_Get_GeneralInformation = CommonFunction.IPAD_ABC
			+ "Get_Todays_Service_Gen_Info";
	private final String METHOD_NAME_Get_GeneralAddressInformation = "Get_Todays_Service_Gen_Info";
	ProgressDialog progressDialogGet_GeneralInfoDetail;

	class DownloadAll_General_Information extends
			AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			String downloading = getResources().getString(
					R.string.loading_general_information);
			String pleasewait = getResources().getString(R.string.please_wait);
			progressDialogGet_GeneralInfoDetail = ProgressDialog.show(
					ServiceListAcitivity.this, downloading, pleasewait, true); // TODO

		}

		@Override
		protected String doInBackground(String... params) {
			return downloadAll_General_Information();
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if ((progressDialogGet_GeneralInfoDetail != null)
					&& progressDialogGet_GeneralInfoDetail.isShowing()) {
				progressDialogGet_GeneralInfoDetail.dismiss();
				progressDialogGet_GeneralInfoDetail = null;
			}
			setRecordtoServiceList();
			DownloadAll_Residential_Pest downloadAll_Residential_Pest = new DownloadAll_Residential_Pest();
			downloadAll_Residential_Pest
					.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		}
	}

	public String downloadAll_General_Information() {
		SoapObject request = new SoapObject(NAMESPACE,
				METHOD_NAME_Get_GeneralAddressInformation);

		PropertyInfo EmpId = new PropertyInfo();
		EmpId.setName(ParameterUtil.EmpId);
		EmpId.setValue(userDTO.getEmployeeId());
		request.addProperty(EmpId);

		PropertyInfo SDate = new PropertyInfo();
		SDate.setName(ParameterUtil.SDate);
		SDate.setValue(getCurrentDate());
		request.addProperty(SDate);

		PropertyInfo Edate = new PropertyInfo();
		Edate.setName(ParameterUtil.Edate);
		Edate.setValue(getTommorrowsDate());
		request.addProperty(Edate);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(SOAP_ACTION_Get_GeneralInformation,
					envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			String res = response.toString();

			// parseServiceListGenralInformation(res);
			//LoggerUtil.e("resp from gene info..", res + "");
			try {
				// Change....
				JSONArray jsServiceDataByServiceNewID = new JSONArray(res);
				if (jsServiceDataByServiceNewID.length() > 0) {
					// houstonFlowFunctions.deleteAllTableData(DatabaseHelper.TABLE_GENERAL_INFO);
				}
				// Delete Old Record

				ArrayList<GeneralInfoDTO> serviceDataList = new Gson()
						.fromJson(jsServiceDataByServiceNewID.toString(),
								new TypeToken<List<GeneralInfoDTO>>() {
								}.getType());

				if (serviceDataList.size() > 0) {
					int oldDelete = houstonFlowFunctions
							.deleteOldServiceData(CommonFunction
									.getCurrentDate());
					// Log.e("Old Delete", oldDelete+"");
				}
				jsServiceDataByServiceNewID = new JSONArray(
						new Gson().toJson(serviceDataList));
				// jsServiceList=new JSONArray(new Gson().toJson(serviceList));
				for (int i = 0; i < jsServiceDataByServiceNewID.length(); i++) {
					GeneralInfoDTO serviceDTO = serviceDataList.get(i);
					boolean isExists = houstonFlowFunctions.isAlreadyAdded(
							serviceDTO.getServiceID_new(),
							DatabaseHelper.TABLE_GENERAL_INFO);
					if (!isExists) {
						// Log.e("serviceDTO.getIsCompleted not is exist",
						// serviceDTO.getIsCompleted()+"new added");
						houstonFlowFunctions
								.addServicesGeneralSRDetail(new JSONObject(
										new Gson().toJson(serviceDTO)));

						// houstonFlowFunctions.addServicesGeneralSRDetail(jsServiceDataByServiceNewID.getJSONObject(i));
					} else {
						GeneralInfoDTO serviceDTOLocally = houstonFlowFunctions
								.getServiceDTODetial(serviceDTO
										.getServiceID_new());
						String timeInLocally = serviceDTOLocally.getTimeIn()
								.trim();
						String serviceStatusLocally = serviceDTOLocally
								.getServiceStatus();
						String isCompleted = serviceDTOLocally.getIsCompleted();
						// Log.e("isCompleted",
						// isCompleted+" "+serviceDTOLocally.getServiceID_new());

						String timeInServer = serviceDTO.getTimeIn().trim();
						if (CommonFunction.checkString(timeInLocally, "")
								.equals("")
								|| isCompleted.equals("")
								|| isCompleted.equals("0")) {
							houstonFlowFunctions.deleteSelectedTableData(
									DatabaseHelper.TABLE_GENERAL_INFO,
									ParameterUtil.ServiceID_new,
									serviceDTOLocally.getServiceID_new());
							houstonFlowFunctions
									.addServicesGeneralSRDetail(new JSONObject(
											new Gson().toJson(serviceDTO)));
						} else {
							if (!serviceStatusLocally.equals("Complete")) {
								if (serviceStatusLocally.equals("InComplete")) {
									if (isCompleted.equals("true")) {

									} else {
										houstonFlowFunctions
												.deleteSelectedTableData(
														DatabaseHelper.TABLE_GENERAL_INFO,
														ParameterUtil.ServiceID_new,
														serviceDTOLocally
																.getServiceID_new());
										houstonFlowFunctions
												.addServicesGeneralSRDetail(new JSONObject(
														new Gson()
																.toJson(serviceDTO)));

									}
								}

								if (serviceStatusLocally.equals("Reset")
										&& (isCompleted.equals("") || isCompleted
												.equals("0"))) {
									houstonFlowFunctions
											.deleteSelectedTableData(
													DatabaseHelper.TABLE_GENERAL_INFO,
													ParameterUtil.ServiceID_new,
													serviceDTOLocally
															.getServiceID_new());
									houstonFlowFunctions
											.addServicesGeneralSRDetail(new JSONObject(
													new Gson()
															.toJson(serviceDTO)));
								}

							} else {
								// Log.e("Complete",
								// ""+serviceDTO.getAccountNo()+"");
							}
						}
					}
				}
				/*
				 * if(jsServiceDataByServiceNewID.length()>0) {
				 * //serviceFlowFunctions
				 * .deleteAllTableData(DatabaseHelper.TABLE_RESIDENTIAL_PEST);
				 * houstonFlowFunctions
				 * .deleteSelectedTableData(DatabaseHelper.TABLE_GENERAL_INFO
				 * ,ParameterUtil.isCompleted,"");
				 * houstonFlowFunctions.deleteSelectedTableData
				 * (DatabaseHelper.TABLE_GENERAL_INFO
				 * ,ParameterUtil.isCompleted,"0"); }
				 */
				for (int i = 0; i < serviceDataList.size(); i++) {
					LoggerUtil.e("downloadImageByName inside general",
							"downloadImageByName");
					downloadImageByName(
							serviceDataList.get(i).getBeforeImage(),
							CommonFunction.DOWNLOAD_IMAGE_URL);
					downloadImageByName(serviceDataList.get(i).getAfterImage(),
							CommonFunction.DOWNLOAD_IMAGE_URL);
					donwloadAudioByName(serviceDataList.get(i).getAudioFile());
					downloadImageByName(serviceDataList.get(i)
							.getCheckFrontImage(),
							CommonFunction.DOWNLOAD_CHECK_IMAGE_URL);
					downloadImageByName(serviceDataList.get(i)
							.getCheckBackImage(),
							CommonFunction.DOWNLOAD_CHECK_IMAGE_URL);
				}
				// houstonFlowFunctions.insertMultipleRecords(jsServiceDataByServiceNewID,
				// DatabaseHelper.TABLE_GENERAL_INFO);

			} catch (Exception e) {
				LoggerUtil.e("Exception", e.toString());
			}

			return res;
		} catch (IOException e) {
			LoggerUtil.e("IOException", e.toString());
		} catch (XmlPullParserException e) {
			LoggerUtil.e("Get_Service_Data_By_ServiceNewID", e.toString());
		}
		return "[{\"result\":\"No Record Found\"}]";
	}

	public void downloadImageByName(String imageNames, String url) {

		String[] imagesArray = imageNames.split(",");

		for (int b = 0; b < imagesArray.length; b++) {
			String imageName = imagesArray[b];
			if (!CommonFunction.checkString(imageName, "").equals("")) {
				boolean isFileExists = isFileExistsByName(imageName);

				if (isFileExists) {

				} else {
					downloadFile(url, imageName);
				}
			}
		}

	}

	public void donwloadAudioByName(String fileName) {
		if (!CommonFunction.checkString(fileName, "").equals("")) {
			boolean isFileExists = isFileExistsByName(fileName);

			if (isFileExists) {

			} else {
				downloadFile(CommonFunction.DOWNLOAD_IMAGE_URL, fileName);
			}
		}
	}

	public boolean isFileExistsByName(String ImageName) {
		if (ImageName == null || ImageName.equals("")) {
			return false;
		}

		try {
			// Log.e("Image Name in refresh",ImageName);
			File profileImage = CommonFunction.getFileLocation(
					ServiceListAcitivity.this, ImageName);
			if (profileImage.exists()) {
				return true;
				// bmp = scaleBitmap(bmp, 100, 100);
			} else {
				return false;
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// bmp=getResources().getDrawable(R.drawable.noimage);
			e.printStackTrace();
		}

		return false;
	}

	public void downloadFile(String fileUrl, String fileName) {
		URL myFileUrl = null;
		// Bitmap bmImg = null;
		try {
			myFileUrl = new URL(fileUrl + "" + fileName);
			// Log.e("myFileUrl download file",""+myFileUrl);

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			// Log.e("MalformedURLException download file",""+e);
		}
		try {
			HttpURLConnection conn = (HttpURLConnection) myFileUrl
					.openConnection();
			conn.setDoInput(true);
			conn.connect();
			File file = CommonFunction.getFileLocation(
					ServiceListAcitivity.this, fileName);
			FileOutputStream fileOutput = new FileOutputStream(file);
			InputStream is = conn.getInputStream();
			// bmImg = BitmapFactory.decodeStream(is);
			byte[] buffer = new byte[1024];
			int bufferLength = 0;
			while ((bufferLength = is.read(buffer)) > 0) {
				fileOutput.write(buffer, 0, bufferLength);
			}
			// close the output stream when done
			fileOutput.close();
			try {/*
				 * bmImg=getBimapByName(fileName); if(bmImg!=null) {
				 * //imageview.setImageBitmap(bmImg);
				 * 
				 * }else { //imageview.setImageResource(R.drawable.noimage);
				 * //pb.setVisibility(View.GONE); }
				 */
			} catch (Exception e) {
				Log.e("Exception in download file", e.toString());

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e("Exception in download file", e.toString());
		}

	}

	private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(getApplicationContext().getContentResolver()
				.openInputStream(selectedImage), null, o);

		final int REQUIRED_SIZE = 800;

		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeStream(getApplicationContext()
				.getContentResolver().openInputStream(selectedImage), null, o2);
	}

	private final String SOAP_ACTION_Get_ResidentialInformation = CommonFunction.IPAD_ABC
			+ "Get_ResidentialServiceDetail";
	private final String METHOD_NAME_Get_ResidentialInformation = "Get_ResidentialServiceDetail";
	ProgressDialog progressDialogGet_ResidentialInfoDetail;

	class DownloadAll_Residential_Pest extends
			AsyncTask<String, String, String> {
		@Override
		protected String doInBackground(String... params) {
			return downloadAll_Residential_Information();
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			String downloading = getResources().getString(
					R.string.Loading_Residential_Detail);
			String pleasewait = getResources().getString(R.string.please_wait);
			progressDialogGet_ResidentialInfoDetail = ProgressDialog.show(
					ServiceListAcitivity.this, downloading, pleasewait, true); // TODO
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if ((progressDialogGet_ResidentialInfoDetail != null)
					&& progressDialogGet_ResidentialInfoDetail.isShowing()) {
				progressDialogGet_ResidentialInfoDetail.dismiss();
				progressDialogGet_ResidentialInfoDetail = null;
			}
			DownloadAll_Commercial_Pest downloadAll_Commercial_Pest = new DownloadAll_Commercial_Pest();
			downloadAll_Commercial_Pest
					.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

		}
	}

	public String downloadAll_Residential_Information() {
		SoapObject request = new SoapObject(NAMESPACE,
				METHOD_NAME_Get_ResidentialInformation);

		PropertyInfo EmpId = new PropertyInfo();
		EmpId.setName(ParameterUtil.EmpId);
		EmpId.setValue(userDTO.getEmployeeId());
		request.addProperty(EmpId);

		PropertyInfo SDate = new PropertyInfo();
		SDate.setName(ParameterUtil.SDate);
		SDate.setValue(getCurrentDate());
		request.addProperty(SDate);

		PropertyInfo Edate = new PropertyInfo();
		Edate.setName(ParameterUtil.Edate);
		Edate.setValue(getTommorrowsDate());
		request.addProperty(Edate);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(SOAP_ACTION_Get_ResidentialInformation,
					envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			String res = response.toString();
			//LoggerUtil.e("resp from resi info..", res + "");
			try {
				JSONArray jsServiceDataByServiceNewID = new JSONArray(res);
				if (jsServiceDataByServiceNewID.length() > 0) {
					// String
					// currentServiceId=SharedPreference.getSharedPrefer(getApplicationContext(),
					// SharedPreference.INPROGRESS_SERVICE,Context.MODE_MULTI_PROCESS);
					// houstonFlowFunctions.deleteAllTableData(DatabaseHelper.TABLE_HR_PEST_INFO);
				}
				ArrayList<HResidential_pest_DTO> serviceDataList = new Gson()
						.fromJson(jsServiceDataByServiceNewID.toString(),
								new TypeToken<List<HResidential_pest_DTO>>() {
								}.getType());
				jsServiceDataByServiceNewID = new JSONArray(
						new Gson().toJson(serviceDataList));
				// houstonFlowFunctions.insertMultipleRecords(jsServiceDataByServiceNewID,
				// DatabaseHelper.TABLE_HR_PEST_INFO);

				for (int i = 0; i < serviceDataList.size(); i++) {
					HResidential_pest_DTO residential_PestDTO = serviceDataList
							.get(i);
					boolean isExists = houstonFlowFunctions.isAlreadyAdded(
							residential_PestDTO.getServiceID_new(),
							DatabaseHelper.TABLE_HR_PEST_INFO);
					if (!isExists) {
						residential_PestDTO.setIsCompleted("false");
						houstonFlowFunctions.addServicesForResidential(new JSONObject(
								new Gson().toJson(residential_PestDTO)));
					} else {
						GeneralInfoDTO serviceDTOLocally = houstonFlowFunctions
								.getServiceDTODetial(residential_PestDTO
										.getServiceID_new());
						// Log.e("ServiceDTO", serviceDTOLocally+"ServiceDTO");
						String serviceStatusLocally = serviceDTOLocally
								.getServiceStatus();
						if (serviceStatusLocally.equals("InComplete")) {
							HResidential_pest_DTO residential_pest_DTOLocalley = houstonFlowFunctions
									.getResidential_Pest_DTO(serviceDTOLocally
											.getServiceID_new());
							if (residential_pest_DTOLocalley.getIsCompleted()
									.equals("true")) {

							} else {
								// Log.e("serviceStatusLocally",
								// serviceStatusLocally);
								houstonFlowFunctions
										.deleteSelectedTableData(
												DatabaseHelper.TABLE_HR_PEST_INFO,
												ParameterUtil.ServiceID_new,
												serviceDTOLocally
														.getServiceID_new());
								residential_PestDTO.setIsCompleted("false");
								houstonFlowFunctions
										.addServicesForResidential(new JSONObject(
												new Gson()
														.toJson(residential_PestDTO)));
							}
						}
						if (serviceStatusLocally.equals("Reset")) {
							// Log.e("serviceStatusLocally",
							// serviceStatusLocally);
							houstonFlowFunctions.deleteSelectedTableData(
									DatabaseHelper.TABLE_HR_PEST_INFO,
									ParameterUtil.ServiceID_new,
									serviceDTOLocally.getServiceID_new());
							residential_PestDTO.setIsCompleted("false");
							houstonFlowFunctions.addServicesForResidential(new JSONObject(
									new Gson().toJson(residential_PestDTO)));
						}
						if (serviceStatusLocally.equals("Complete")) {
							HResidential_pest_DTO residential_pest_DTOLocalley = houstonFlowFunctions
									.getResidential_Pest_DTO(serviceDTOLocally
											.getServiceID_new());
							if (residential_pest_DTOLocalley.getIsCompleted()
									.equals("true")) {

							} else {
								// Log.e("serviceStatusLocally",
								// serviceStatusLocally);
								houstonFlowFunctions
										.deleteSelectedTableData(
												DatabaseHelper.TABLE_HR_PEST_INFO,
												ParameterUtil.ServiceID_new,
												serviceDTOLocally
														.getServiceID_new());
								residential_PestDTO.setIsCompleted("false");
								houstonFlowFunctions
										.addServicesForResidential(new JSONObject(
												new Gson()
														.toJson(residential_PestDTO)));
							}
						}
					}
				}
				if (jsServiceDataByServiceNewID.length() > 0) {
					// serviceFlowFunctions.deleteAllTableData(DatabaseHelper.TABLE_RESIDENTIAL_PEST);
					houstonFlowFunctions.deleteSelectedTableData(
							DatabaseHelper.TABLE_HR_PEST_INFO,
							ParameterUtil.isCompleted, "");
					houstonFlowFunctions.deleteSelectedTableData(
							DatabaseHelper.TABLE_HR_PEST_INFO,
							ParameterUtil.isCompleted, "0");
				}

				for (int i = 0; i < serviceDataList.size(); i++) {
					downloadImageByName(serviceDataList.get(i)
							.getServiceSignature(),
							CommonFunction.DOWNLOAD_RESIDENTIAL_SIGNATURE);
					downloadImageByName(serviceDataList.get(i).getSignature(),
							CommonFunction.DOWNLOAD_RESIDENTIAL_SIGNATURE);

				}
			} catch (Exception e) {
				LoggerUtil.e("Exception", e.toString());
			}
			return res;
		} catch (IOException e) {
			LoggerUtil.e("IOException..residential..", e.toString());
		} catch (XmlPullParserException e) {
			LoggerUtil.e("Get_Service_Data_By_ServiceNewID...residential..S",
					e.toString());
		}
		return "[{\"result\":\"No Record Found\"}]";
	}

	private final String SOAP_ACTION_Get_CommercialInformation = CommonFunction.IPAD_ABC
			+ "Get_CommercialServiceDetail";
	private final String METHOD_NAME_Get_CommercialInformation = "Get_CommercialServiceDetail";
	ProgressDialog progressDialogGet_CommercialInfoDetail;

	class DownloadAll_Commercial_Pest extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			String downloading = getResources().getString(
					R.string.Loading_Commercial_Detail);
			String pleasewait = getResources().getString(R.string.please_wait);
			progressDialogGet_CommercialInfoDetail = ProgressDialog.show(
					ServiceListAcitivity.this, downloading, pleasewait, true); // TODO
		}

		@Override
		protected String doInBackground(String... params) {
			return downloadAll_Commercial_Information();
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if ((progressDialogGet_CommercialInfoDetail != null)
					&& progressDialogGet_CommercialInfoDetail.isShowing()) {
				progressDialogGet_CommercialInfoDetail.dismiss();
				progressDialogGet_CommercialInfoDetail = null;
			}
			DownloadAll_Get_All_Route get_all_route = new DownloadAll_Get_All_Route();
			get_all_route
					.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	
			//DownloadAll_Orlando_Res_Pest all_Orlando_Res_Pest = new DownloadAll_Orlando_Res_Pest();
			//all_Orlando_Res_Pest
				//	.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	
		}
	}

	public String downloadAll_Commercial_Information() {
		SoapObject request = new SoapObject(NAMESPACE,
				METHOD_NAME_Get_CommercialInformation);

		PropertyInfo EmpId = new PropertyInfo();
		EmpId.setName(ParameterUtil.EmpId);
		EmpId.setValue(userDTO.getEmployeeId());
		request.addProperty(EmpId);

		PropertyInfo SDate = new PropertyInfo();
		SDate.setName(ParameterUtil.SDate);
		// SDate.setValue("2015/10/24");
		SDate.setValue(getCurrentDate());
		request.addProperty(SDate);

		PropertyInfo Edate = new PropertyInfo();
		Edate.setName(ParameterUtil.Edate);
		// Edate.setValue("2015/11/24");
		Edate.setValue(getTommorrowsDate());
		request.addProperty(Edate);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(SOAP_ACTION_Get_CommercialInformation,
					envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			String res = response.toString();
			LoggerUtil.e("resp from commercial info..", res + "");
			try {
				JSONArray jsServiceDataByServiceNewID = new JSONArray(res);
				
                ArrayList<HCommercial_pest_DTO> serviceDataList 
                = new Gson().fromJson(jsServiceDataByServiceNewID.toString(),
						new TypeToken<List<HCommercial_pest_DTO>>(){}.getType());
				
               	
				for (int i = 0; i < serviceDataList.size(); i++) {
					
					HCommercial_pest_DTO hCommercial_pest_DTO = serviceDataList.get(i);
					boolean isExists = houstonFlowFunctions.isAlreadyAdded(
							hCommercial_pest_DTO.getServiceID_new(),
							DatabaseHelper.TABLE_HC_PEST_INFO);
					
					LoggerUtil.e("commercial isExists", "" + isExists);
					
					if (!isExists) 
					{
						
						hCommercial_pest_DTO.setIsCompleted("false");
						houstonFlowFunctions
								.addServicesForCommercial(new JSONObject(
										new Gson().toJson(hCommercial_pest_DTO)));
					} 
					else
                    {
						GeneralInfoDTO serviceDTOLocally = houstonFlowFunctions
								.getServiceDTODetial(hCommercial_pest_DTO
										.getServiceID_new());
						
						String serviceStatusLocally = serviceDTOLocally
								.getServiceStatus();
						if (serviceStatusLocally.equals("InComplete")) {
							
							Log.e("InComplete","adding commercial data");
							HCommercial_pest_DTO commercial_pest_DTOLocalley = houstonFlowFunctions
									.getCommercial_Pest_DTO(serviceDTOLocally
											.getServiceID_new());
							
							if (commercial_pest_DTOLocalley.getIsCompleted()
									.equals("true")) {
								Log.e("InComplete isComplete","adding commercial data");
							} 
							else 
							{
								Log.e("InComplete not isComplete","adding commercial data");
								houstonFlowFunctions
										.deleteSelectedTableData(
												DatabaseHelper.TABLE_HC_PEST_INFO,
												ParameterUtil.ServiceID_new,
												serviceDTOLocally
														.getServiceID_new());
								
								hCommercial_pest_DTO.setIsCompleted("false");
								houstonFlowFunctions
										.addServicesForCommercial(new JSONObject(
												new Gson()
														.toJson(hCommercial_pest_DTO)));
							}
						}
						if (serviceStatusLocally.equals("Reset")) {
							// Log.e("serviceStatusLocally",
							// serviceStatusLocally);
							houstonFlowFunctions.deleteSelectedTableData(
									DatabaseHelper.TABLE_HC_PEST_INFO,
									ParameterUtil.ServiceID_new,
									serviceDTOLocally.getServiceID_new());
							hCommercial_pest_DTO.setIsCompleted("false");
							houstonFlowFunctions.addServicesForCommercial(new JSONObject(
									new Gson().toJson(hCommercial_pest_DTO)));
						}
						if (serviceStatusLocally.equals("Complete")) 
						{
							
							Log.e("Complete","adding commercial data");
							
							HCommercial_pest_DTO commertial_pest_DTOLocalley = houstonFlowFunctions
									.getCommercial_Pest_DTO(serviceDTOLocally
											.getServiceID_new());
							if (commertial_pest_DTOLocalley.getIsCompleted()
									.equals("true")) {
								Log.e("Complete IsCompleted","adding commercial data");
							} else 
							{
								Log.e("Complete not IsCompleted","adding commercial data");
								houstonFlowFunctions
										.deleteSelectedTableData(
												DatabaseHelper.TABLE_HC_PEST_INFO,
												ParameterUtil.ServiceID_new,
												serviceDTOLocally
														.getServiceID_new());
								hCommercial_pest_DTO.setIsCompleted("false");
								houstonFlowFunctions
										.addServicesForCommercial(new JSONObject(
												new Gson()
														.toJson(hCommercial_pest_DTO)));
							}
						}
					}
				}
				if (jsServiceDataByServiceNewID.length() > 0) {
					// serviceFlowFunctions.deleteAllTableData(DatabaseHelper.TABLE_RESIDENTIAL_PEST);
					houstonFlowFunctions.deleteSelectedTableData(
							DatabaseHelper.TABLE_HC_PEST_INFO,
							ParameterUtil.isCompleted, "");
					houstonFlowFunctions.deleteSelectedTableData(
							DatabaseHelper.TABLE_HC_PEST_INFO,
							ParameterUtil.isCompleted, "0");
				}

				for (int i = 0; i < serviceDataList.size(); i++) {
					downloadImageByName(serviceDataList.get(i)
							.getServiceSignature(),
							CommonFunction.DOWNLOAD_COMMERCIAL_SIGNATURE);
					downloadImageByName(serviceDataList.get(i).getSignature(),
							CommonFunction.DOWNLOAD_COMMERCIAL_SIGNATURE);

				}
			} catch (Exception e) {
				LoggerUtil.e("Exception", e.toString());
			}
			return res;
		} catch (IOException e) {
			LoggerUtil.e("IOException..commertial..", e.toString());
		} catch (XmlPullParserException e) {
			LoggerUtil.e("Get_Service_Data_By_ServiceNewID...commertial..S",
					e.toString());
		}
		return "[{\"result\":\"No Record Found\"}]";
	}
	
	private final String SOAP_ACTION_Get_All_AssignCrewForTheDay = CommonFunction.IPAD_ABC
			+ "Get_All_AssignCrewMemberForTheDay";
	private final String METHOD_NAME_Get_All_AssignCrewForTheDay = "Get_All_AssignCrewMemberForTheDay";
	ProgressDialog progressDialogGet_All_AssignCrewForTheDay;

	class DownloadAll_Get_All_AssignCrewForTheDay extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			String downloading = getResources().getString(
					R.string.loading_Assigned_Crew);
			String pleasewait = getResources().getString(R.string.please_wait);
			progressDialogGet_All_AssignCrewForTheDay = ProgressDialog.show(
					ServiceListAcitivity.this, downloading, pleasewait, true); // TODO
		}

		@Override
		protected String doInBackground(String... params) {
			return download_All_AssignCrewForTheDay();
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if ((progressDialogGet_All_AssignCrewForTheDay != null)
					&& progressDialogGet_All_AssignCrewForTheDay.isShowing()) {
				progressDialogGet_All_AssignCrewForTheDay.dismiss();
				progressDialogGet_All_AssignCrewForTheDay = null;
			}
			
		}
	}
	public String download_All_AssignCrewForTheDay() {
		SoapObject request = new SoapObject(NAMESPACE,
				METHOD_NAME_Get_All_AssignCrewForTheDay);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		try {
			androidHttpTransport.call(SOAP_ACTION_Get_All_AssignCrewForTheDay, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			String res = response.toString();
			//LoggerUtil.e("resp from all crew", "crew..." + res);
			try {
				JSONArray jsServiceDataByServiceNewID = new JSONArray(res);
				if (jsServiceDataByServiceNewID.length() > 0) {
					houstonFlowFunctions.deleteAllTableData(DatabaseHelper.TABLE_GET_ASSIGN_CREW_FOR_DAY);
				}
				ArrayList<GetAssignCrewMemberDTO> serviceDataList = new Gson().fromJson(
						jsServiceDataByServiceNewID.toString(),
						new TypeToken<List<GetAssignCrewMemberDTO>>() {
						}.getType());
				jsServiceDataByServiceNewID = new JSONArray(
						new Gson().toJson(serviceDataList));
				houstonFlowFunctions.insertMultipleRecords(
						jsServiceDataByServiceNewID,
						DatabaseHelper.TABLE_GET_ASSIGN_CREW_FOR_DAY);
			} catch (Exception e) {
				LoggerUtil.e("Exception in get all assigned crew", e.toString());
			}
			return res;
		} catch (IOException e) {
			LoggerUtil.e("IOException", e.toString());
		} catch (XmlPullParserException e) {
			LoggerUtil.e("Get_Service_Data_By_ServiceNewID", e.toString());
		}
		return "[{\"result\":\"No Record Found\"}]";
	}

	
	
	private final String SOAP_ACTION_Get_All_AssignLeadForTheDay = CommonFunction.IPAD_ABC
			+ "Get_All_AssignLeadForTheDay";
	private final String METHOD_NAME_Get_All_AssignLeadForTheDay = "Get_All_AssignLeadForTheDay";
	ProgressDialog progressDialogGet_All_AssignLeadForTheDay;

	class DownloadAll_Get_All_AssignLeadForTheDay extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			String downloading = getResources().getString(
					R.string.loading_Assigned_Lead);
			String pleasewait = getResources().getString(R.string.please_wait);
			progressDialogGet_All_AssignLeadForTheDay = ProgressDialog.show(
					ServiceListAcitivity.this, downloading, pleasewait, true); // TODO
		}

		@Override
		protected String doInBackground(String... params) {
			return download_All_AssignLeadForTheDay();
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if ((progressDialogGet_All_AssignLeadForTheDay != null)
					&& progressDialogGet_All_AssignLeadForTheDay.isShowing()) {
				progressDialogGet_All_AssignLeadForTheDay.dismiss();
				progressDialogGet_All_AssignLeadForTheDay = null;
			}
			DownloadAll_Get_All_AssignCrewForTheDay get_all_AssignCrewForTheDay = new DownloadAll_Get_All_AssignCrewForTheDay();
			get_all_AssignCrewForTheDay
					.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		}
	}
	
	public String download_All_AssignLeadForTheDay() {
		SoapObject request = new SoapObject(NAMESPACE,
				METHOD_NAME_Get_All_AssignLeadForTheDay);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		try {
			androidHttpTransport.call(SOAP_ACTION_Get_All_AssignLeadForTheDay, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			String res = response.toString();
			//LoggerUtil.e("resp from all crew", "crew..." + res);
			try {
				JSONArray jsServiceDataByServiceNewID = new JSONArray(res);
				if (jsServiceDataByServiceNewID.length() > 0) {
					houstonFlowFunctions.deleteAllTableData(DatabaseHelper.TABLE_GET_ASSIGN_LEAD_FOR_DAY);
				}
				ArrayList<GetAssignedLeadDTO> serviceDataList = new Gson().fromJson(
						jsServiceDataByServiceNewID.toString(),
						new TypeToken<List<GetAssignedLeadDTO>>() {
						}.getType());
				jsServiceDataByServiceNewID = new JSONArray(
						new Gson().toJson(serviceDataList));
				houstonFlowFunctions.insertMultipleRecords(
						jsServiceDataByServiceNewID,
						DatabaseHelper.TABLE_GET_ASSIGN_LEAD_FOR_DAY);
			} catch (Exception e) {
				LoggerUtil.e("Exception in get all assigned lead", e.toString());
			}
			return res;
		} catch (IOException e) {
			LoggerUtil.e("IOException", e.toString());
		} catch (XmlPullParserException e) {
			LoggerUtil.e("Get_Service_Data_By_ServiceNewID", e.toString());
		}
		return "[{\"result\":\"No Record Found\"}]";
	}

	

	
	
	private final String SOAP_ACTION_Get_All_Route_Crew = CommonFunction.IPAD_ABC
			+ "Get_All_Route_Crew";
	private final String METHOD_NAME_Get_All_Route_Crew = "Get_All_Route_Crew";
	ProgressDialog progressDialogGet_Get_All_Route_Crew;

	class DownloadAll_Get_All_Route_Crew extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			String downloading = getResources().getString(
					R.string.loading_All_Route_Crew);
			String pleasewait = getResources().getString(R.string.please_wait);
			progressDialogGet_Get_All_Route_Crew = ProgressDialog.show(
					ServiceListAcitivity.this, downloading, pleasewait, true); // TODO
		}

		@Override
		protected String doInBackground(String... params) {
			return download_All_RouteCrew_Information();
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if ((progressDialogGet_Get_All_Route_Crew != null)
					&& progressDialogGet_Get_All_Route_Crew.isShowing()) {
				progressDialogGet_Get_All_Route_Crew.dismiss();
				progressDialogGet_Get_All_Route_Crew = null;
			}
			DownloadAll_Get_All_AssignLeadForTheDay get_all_AssignLeadForTheDay = new DownloadAll_Get_All_AssignLeadForTheDay();
			get_all_AssignLeadForTheDay
					.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	
		}
	}
	public String download_All_RouteCrew_Information() {
		SoapObject request = new SoapObject(NAMESPACE,
				METHOD_NAME_Get_All_Route_Crew);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		try {
			androidHttpTransport.call(SOAP_ACTION_Get_All_Route_Crew, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			String res = response.toString();
			//LoggerUtil.e("resp from all crew", "crew..." + res);
			try {
				JSONArray jsServiceDataByServiceNewID = new JSONArray(res);
				if (jsServiceDataByServiceNewID.length() > 0) {
					houstonFlowFunctions.deleteAllTableData(DatabaseHelper.TABLE_GET_ALL_ROUTE_CREW);
				}
				ArrayList<GetRouteCrewDTO> serviceDataList = new Gson().fromJson(
						jsServiceDataByServiceNewID.toString(),
						new TypeToken<List<GetRouteCrewDTO>>() {
						}.getType());
				jsServiceDataByServiceNewID = new JSONArray(
						new Gson().toJson(serviceDataList));
				houstonFlowFunctions.insertMultipleRecords(
						jsServiceDataByServiceNewID,
						DatabaseHelper.TABLE_GET_ALL_ROUTE_CREW);
			} catch (Exception e) {
				LoggerUtil.e("Exception in get all route crew", e.toString());
			}
			return res;
		} catch (IOException e) {
			LoggerUtil.e("IOException", e.toString());
		} catch (XmlPullParserException e) {
			LoggerUtil.e("Get_Service_Data_By_ServiceNewID", e.toString());
		}
		return "[{\"result\":\"No Record Found\"}]";
	}

	
	
	private final String SOAP_ACTION_Get_All_Route = CommonFunction.IPAD_ABC
			+ "Get_All_Route";
	private final String METHOD_NAME_Get_All_Route = "Get_All_Route";
	ProgressDialog progressDialogGet_Get_All_Route;

	class DownloadAll_Get_All_Route extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			String downloading = getResources().getString(
					R.string.Loading_all_route);
			String pleasewait = getResources().getString(R.string.please_wait);
			progressDialogGet_Get_All_Route = ProgressDialog.show(
					ServiceListAcitivity.this, downloading, pleasewait, true); // TODO
		}

		@Override
		protected String doInBackground(String... params) {
			return download_All_Route_Information();
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if ((progressDialogGet_Get_All_Route != null)
					&& progressDialogGet_Get_All_Route.isShowing()) {
				progressDialogGet_Get_All_Route.dismiss();
				progressDialogGet_Get_All_Route = null;
			}
			DownloadAll_Get_All_Route_Crew get_all_route_crew = new DownloadAll_Get_All_Route_Crew();
			get_all_route_crew
					.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	
		}
	}
	public String download_All_Route_Information() {
		SoapObject request = new SoapObject(NAMESPACE,
				METHOD_NAME_Get_All_Route);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		try {
			androidHttpTransport.call(SOAP_ACTION_Get_All_Route, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			String res = response.toString();
			//LoggerUtil.e("resp from all crew", "crew..." + res);
			try {
				JSONArray jsServiceDataByServiceNewID = new JSONArray(res);
				if (jsServiceDataByServiceNewID.length() > 0) {
					houstonFlowFunctions.deleteAllTableData(DatabaseHelper.TABLE_GET_ALL_ROUTE);
				}
				ArrayList<GetAllRouteDTO> serviceDataList = new Gson().fromJson(
						jsServiceDataByServiceNewID.toString(),
						new TypeToken<List<GetAllRouteDTO>>() {
						}.getType());
				jsServiceDataByServiceNewID = new JSONArray(
						new Gson().toJson(serviceDataList));
				houstonFlowFunctions.insertMultipleRecords(
						jsServiceDataByServiceNewID,
						DatabaseHelper.TABLE_GET_ALL_ROUTE);
			} catch (Exception e) {
				LoggerUtil.e("Exception in get all route", e.toString());
			}
			return res;
		} catch (IOException e) {
			LoggerUtil.e("IOException", e.toString());
		} catch (XmlPullParserException e) {
			LoggerUtil.e("Get_Service_Data_By_ServiceNewID", e.toString());
		}
		return "[{\"result\":\"No Record Found\"}]";
	}

	private final String SOAP_ACTION_Get_OrlandoResidential = CommonFunction.IPAD_ABC
			+ "Get_CommercialServiceDetail";
	private final String METHOD_NAME_Get_OrlandoResidential = "Get_CommercialServiceDetail";
	ProgressDialog progressDialogGet_OrlandoResidential;

	class DownloadAll_Orlando_Res_Pest extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			String downloading = getResources().getString(
					R.string.Loading_Orlando_Res_Detail);
			String pleasewait = getResources().getString(R.string.please_wait);
			progressDialogGet_OrlandoResidential = ProgressDialog.show(
					ServiceListAcitivity.this, downloading, pleasewait, true); // TODO
		}

		@Override
		protected String doInBackground(String... params) {
			return downloadAll_Orlando_Res_Information();
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if ((progressDialogGet_OrlandoResidential != null)
					&& progressDialogGet_OrlandoResidential.isShowing()) {
				progressDialogGet_OrlandoResidential.dismiss();
				progressDialogGet_OrlandoResidential = null;
			}
		}
	}
	public String downloadAll_Orlando_Res_Information() {
		// TODO Auto-generated method stub
		SoapObject request = new SoapObject(NAMESPACE,
				METHOD_NAME_Get_OrlandoResidential);

		PropertyInfo EmpId = new PropertyInfo();
		EmpId.setName(ParameterUtil.EmpId);
		EmpId.setValue(userDTO.getEmployeeId());
		request.addProperty(EmpId);

		PropertyInfo SDate = new PropertyInfo();
		SDate.setName(ParameterUtil.SDate);
		// SDate.setValue("2015/10/24");
		SDate.setValue(getCurrentDate());
		request.addProperty(SDate);

		PropertyInfo Edate = new PropertyInfo();
		Edate.setName(ParameterUtil.Edate);
		// Edate.setValue("2015/11/24");
		Edate.setValue(getTommorrowsDate());
		request.addProperty(Edate);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(SOAP_ACTION_Get_OrlandoResidential,envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			String res = response.toString();
			LoggerUtil.e("resp from orlando resi info..", res + "");
			try {
				JSONArray jsServiceDataByServiceNewID = new JSONArray(res);
				
                ArrayList<OResidential_pest_DTO> serviceDataList 
                = new Gson().fromJson(jsServiceDataByServiceNewID.toString(),
						new TypeToken<List<OResidential_pest_DTO>>(){}.getType());
				
               	
				for (int i = 0; i < serviceDataList.size(); i++) {
					
					OResidential_pest_DTO oResidential_pest_DTO = serviceDataList.get(i);
					boolean isExists = houstonFlowFunctions.isAlreadyAdded(
							oResidential_pest_DTO.getServiceID_new(),
							DatabaseHelper.TABLE_OR_PEST_INFO);
					
					LoggerUtil.e("Orlando resi isExists", "" + isExists);
					
					if (!isExists) 
					{
						oResidential_pest_DTO.setIsCompleted("false");
						houstonFlowFunctions.addServicesForOrlandoResi(new JSONObject(
										new Gson().toJson(oResidential_pest_DTO)));
					} 
					else
                    {
						GeneralInfoDTO serviceDTOLocally = houstonFlowFunctions
								.getServiceDTODetial(oResidential_pest_DTO
										.getServiceID_new());
						
						String serviceStatusLocally = serviceDTOLocally
								.getServiceStatus();
						if (serviceStatusLocally.equals("InComplete")) {
							
							Log.e("InComplete","adding orlando res data");
							OResidential_pest_DTO orlandoRes_pest_DTOLocalley = houstonFlowFunctions
									.getOrlandoRes_Pest_DTO(serviceDTOLocally
											.getServiceID_new());
							
							if (orlandoRes_pest_DTOLocalley.getIsCompleted()
									.equals("true")) {
								Log.e("InComplete isComplete","adding orlando res data");
							} 
							else 
							{
								Log.e("InComplete not isComplete","adding orlando res data");
								houstonFlowFunctions
										.deleteSelectedTableData(
												DatabaseHelper.TABLE_OR_PEST_INFO,
												ParameterUtil.ServiceID_new,
												serviceDTOLocally
														.getServiceID_new());
								
								oResidential_pest_DTO.setIsCompleted("false");
								houstonFlowFunctions
										.addServicesForOrlandoResi(new JSONObject(
												new Gson()
														.toJson(oResidential_pest_DTO)));
							}
						}
						if (serviceStatusLocally.equals("Reset")) {
							// Log.e("serviceStatusLocally",
							// serviceStatusLocally);
							houstonFlowFunctions.deleteSelectedTableData(
									DatabaseHelper.TABLE_OR_PEST_INFO,
									ParameterUtil.ServiceID_new,
									serviceDTOLocally.getServiceID_new());
							oResidential_pest_DTO.setIsCompleted("false");
							houstonFlowFunctions.addServicesForOrlandoResi(new JSONObject(
									new Gson().toJson(oResidential_pest_DTO)));
						}
						if (serviceStatusLocally.equals("Complete")) 
						{
							Log.e("Complete","adding orlando res data");
							
							OResidential_pest_DTO OResidential_pest_DTOLocalley = houstonFlowFunctions
									.getOrlandoRes_Pest_DTO(serviceDTOLocally.getServiceID_new());
							if (OResidential_pest_DTOLocalley.getIsCompleted().equals("true")) {
								Log.e("Complete IsCompleted","adding orlando res data");
							} else 
							{
								Log.e("Complete not IsCompleted","adding orlando res data");
								houstonFlowFunctions.deleteSelectedTableData(
												DatabaseHelper.TABLE_OR_PEST_INFO,
												ParameterUtil.ServiceID_new,
												serviceDTOLocally
														.getServiceID_new());
								oResidential_pest_DTO.setIsCompleted("false");
								houstonFlowFunctions.addServicesForOrlandoResi(new JSONObject(new Gson()
														.toJson(oResidential_pest_DTO)));
							}
						}
					}
				}
				if (jsServiceDataByServiceNewID.length() > 0) {
					// serviceFlowFunctions.deleteAllTableData(DatabaseHelper.TABLE_RESIDENTIAL_PEST);
					houstonFlowFunctions.deleteSelectedTableData(
							DatabaseHelper.TABLE_OR_PEST_INFO,
							ParameterUtil.isCompleted, "");
					houstonFlowFunctions.deleteSelectedTableData(
							DatabaseHelper.TABLE_OR_PEST_INFO,
							ParameterUtil.isCompleted, "0");
				}

				for (int i = 0; i < serviceDataList.size(); i++) {
					downloadImageByName(serviceDataList.get(i)
							.getServiceSignature(),
							CommonFunction.DOWNLOAD_ORLANDO_SIGNATURE);
					downloadImageByName(serviceDataList.get(i).getSignature(),
							CommonFunction.DOWNLOAD_ORLANDO_SIGNATURE);

				}
			} catch (Exception e) {
				LoggerUtil.e("Exception", e.toString());
			}
			return res;
		} catch (IOException e) {
			LoggerUtil.e("IOException..orlando..", e.toString());
		} catch (XmlPullParserException e) {
			LoggerUtil.e("Get_Service_Data_By_ServiceNewID...orlando..S",
					e.toString());
		}
		return "[{\"result\":\"No Record Found\"}]";
	}

	private void initialize() {
		// TODO Auto-generated method stub
		userDTO = (UserDTO) getIntent().getExtras().getSerializable("userDTO");
		// userDTO = null;
		if (userDTO == null) {
			// Log.e("", "dto is null.." + userDTO);
			String result = SharedPreference.getSharedPrefer(
					getApplicationContext(), SharedPreference.USER_DETAIL);
			// result = null;
			if (result == null) {
				Toast.makeText(getApplicationContext(),
						"Please login again...", Toast.LENGTH_LONG).show();
				Intent i = new Intent(ServiceListAcitivity.this,
						LoginActivity.class);
				startActivity(i);
				finish();
				// Log.e("", "result is null.." + result);
			} else {
				userDTO = parseLoginResponse(result);
			}
		}
		serviceList = new ArrayList<GeneralInfoDTO>();
		houstonFlowFunctions = new HoustonFlowFunctions(getApplicationContext());
		buttonGo = (Button) findViewById(R.id.btnGO);
		buttonGo.setOnClickListener(new OnButtonClick());
		btnClose = (ImageButton) findViewById(R.id.btnClose);
		btnClose.setOnClickListener(new OnButtonClick());
		listView = (ListView) findViewById(R.id.listView1);
		listView.setOnItemClickListener((OnItemClickListener) new OnItemClickListeners());

		mLastClickTime = SystemClock.elapsedRealtime();
		// layouytAdapter=(LinearLayout)findViewById(R.id.layouytAdapter);
		// layouytAdapter.setOnClickListener(new OnButtonClick());

		edtFromdate = (EditText) findViewById(R.id.edtFromDate);
		edtFromdate.setInputType(InputType.TYPE_NULL);

		// edtTodate =(EditText)findViewById(R.id.edtToDate);
		// edtTodate.setInputType(InputType.TYPE_NULL);

		edtFromdate.setOnClickListener(new OnButtonClick());
		// edtTodate.setOnClickListener(new OnButtonClick());
		edtfirstName = (EditText) findViewById(R.id.edtfirstName);

		edtlastName = (EditText) findViewById(R.id.edtlastName);
		edtemailAddress = (EditText) findViewById(R.id.edtemailAddress);
		edtaccountNo = (EditText) findViewById(R.id.edtAccountNo);
		edtaddress = (EditText) findViewById(R.id.edtAddres);
		edtcity = (EditText) findViewById(R.id.edtCity);
		edtstate = (EditText) findViewById(R.id.edtState);
		// edttechName=(EditText)findViewById(R.id.edttechName);
		// edttechId=(EditText)findViewById(R.id.edttechId);
		edtworkOrder = (EditText) findViewById(R.id.edtworkOrder);
		spnserviceStatus = (Spinner) findViewById(R.id.spnserviceStatus);
		spnserviceBranch = (Spinner) findViewById(R.id.spnserviceBranch);
		spnToDate = (Spinner) findViewById(R.id.spnToDate);
		// linear layout initialixze..
		filter_criteria_layout = (LinearLayout) findViewById(R.id.filter_criteria_layout);
	}

	

	public UserDTO parseLoginResponse(String resp) {
		try {
			JSONArray jsBeanList = new JSONArray(resp);
			// Log.e("","jsBeanList"+jsBeanList.length());
			if (jsBeanList.length() > 0) {
				JSONObject jsUserDTO = jsBeanList.getJSONObject(0);
				try {
					String result = jsUserDTO.getString("Result");
					return null;
				} catch (Exception e) {
					// TODO: handle exception

				}
				UserDTO dto = new Gson().fromJson(jsUserDTO.toString(),
						UserDTO.class);
				// Log.e("","jsBeanList..UserDTO.."+dto);
				return dto;
			}
			return null;
		} catch (Exception e) {
			Log.e("Exception", e.toString());
		}
		return null;
	}

	public void setData() {
		firstName = edtfirstName.getText().toString().trim();
		lastName = edtlastName.getText().toString().trim();
		emailAddress = edtemailAddress.getText().toString().trim();
		accountNo = edtaccountNo.getText().toString().trim();
		address = edtaddress.getText().toString().trim();
		city = edtcity.getText().toString().trim();
		state = edtstate.getText().toString().trim();
		serviceStatus = spnserviceStatus.getSelectedItem().toString();
		strserviceBranch = spnserviceBranch.getSelectedItem().toString();
		// techName=edttechName.getText().toString().trim();
		// techId=edttechId.getText().toString().trim();
		fromDate = edtFromdate.getText().toString().trim();
		toDate = spnToDate.getSelectedItem().toString();
		workOrder = edtworkOrder.getText().toString().trim();
		// "11/10/2014",toDate="11/10/2014",workOrder;
	}

	class OnButtonClick implements OnClickListener {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.edtFromDate:
				// System.out.println("Date1 is before Date2...5");
				DialogFragment FromdateFragment = new DatePickerFragment(
						edtFromdate, edtFromdate, 1);
				FromdateFragment.show(getFragmentManager(), "Date Picker");
				break;
			case R.id.btnClose:

				if (filter_criteria_layout.getVisibility() == View.VISIBLE) {
					filter_criteria_layout.setVisibility(View.GONE);
				} else {
					// Log.e("", "inside visible22");
					// load the animation
					/*
					 * Animation animSideDown =
					 * AnimationUtils.loadAnimation(getActivity(),
					 * R.anim.slide_down);
					 */// filter_criteria_layout.startAnimation(animSideDown);
					filter_criteria_layout.setVisibility(View.VISIBLE);
				}
				break;
			case R.id.btnGO:
				setData();
				// setRecordtoServiceList();
				// Log.e("workOrder", workOrder);
				JSONArray searchArray = houstonFlowFunctions
						.getSelectedServicelist(firstName, lastName,
								emailAddress, accountNo, address, city, state,
								techName, techId, fromDate, toDate, workOrder,
								serviceStatus, strserviceBranch);
				serviceList = new Gson().fromJson(searchArray.toString(),
						new TypeToken<List<GeneralInfoDTO>>() {
						}.getType());
				ManageServiceListAdapter manageServiceListAdapter = new ManageServiceListAdapter(
						serviceList, ServiceListAcitivity.this, userDTO);
				listView.setAdapter(manageServiceListAdapter);
				// DownloadAll_Todays_Service downloadAll_Todays_Service=new
				// DownloadAll_Todays_Service();
				// downloadAll_Todays_Service.execute("");

				if (serviceList.size() == 0) {
					noRecordFound();
				} else {
					recordFound();
				}
				filter_criteria_layout.setVisibility(View.GONE);
				try {
					InputMethodManager inputManager = (InputMethodManager) getSystemService(HR_TodayServiceInvoice.INPUT_METHOD_SERVICE);
					inputManager.hideSoftInputFromWindow(getCurrentFocus()
							.getWindowToken(),
							InputMethodManager.HIDE_NOT_ALWAYS);
				} catch (Exception e) {
					Log.e("e exception: ", "" + e);
				}
				break;
			default:
				break;
			}
		}
	}

	public void noRecordFound() {
		TextView txtnorecord = (TextView) findViewById(R.id.txtnorecord);
		txtnorecord.setVisibility(View.VISIBLE);
		listView.setVisibility(View.GONE);
	}

	public void recordFound() {
		TextView txtnorecord = (TextView) findViewById(R.id.txtnorecord);
		txtnorecord.setVisibility(View.GONE);
		listView.setVisibility(View.VISIBLE);
	}

	public String getCurrentDate() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
		Calendar calendar1 = Calendar.getInstance();
		calendar1.setTimeInMillis(System.currentTimeMillis());
		String date = "" + formatter.format(calendar1.getTime());
		// fromDate=date;
		// toDate=date;
		return date;
	}

	public String getTommorrowsDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_YEAR, 1);
		// Use SimpleDateFormat to format the Date as a String:
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
		String date = "" + formatter.format(calendar.getTime());
		return date;
	}

	private class OnItemClickListeners implements OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			String serviceNew_Id = serviceList.get(arg2).getServiceID_new();

			boolean isExists = houstonFlowFunctions.isAlreadyAdded(
					serviceNew_Id, DatabaseHelper.TABLE_HR_PEST_INFO);
			LoggerUtil.e("selectQuerty isExists", "" + isExists);
			if (isExists) {
				listView.setEnabled(false);
				Intent intent = new Intent(ServiceListAcitivity.this,
						GeneralInformationActivity.class);
				intent.putExtra("serviceIdNew", serviceList.get(arg2)
						.getServiceID_new());
				SharedPreference.setSharedPrefer(getApplicationContext(),
						SharedPreference.INPROGRESS_SERVICE, serviceNew_Id,
						Context.MODE_MULTI_PROCESS);
				intent.putExtra("userDTO", userDTO);
				startActivity(intent);
			} else {
				new AlertDialog.Builder(ServiceListAcitivity.this)
						.setTitle("Error!!")
						.setMessage(
								" This Work Order is not loaded properly please try again..")
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										dialog.dismiss();
										dialog = null;
										// getAllRecordFromServer();
										SyncAllData();
									}
								}).show();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getMenuInflater().inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			// create intent to perform web search for this planet
			filter_criteria_layout.setVisibility(View.GONE);
			SyncAllData();

			return true;
		case R.id.action_logout:
			createLogOutAlert();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void createLogOutAlert() {
		// TODO Auto-generated method stub
		new AlertDialog.Builder(this)
				.setIcon(android.R.drawable.ic_dialog_alert)
				.setTitle("Exit")
				.setMessage("Are you sure want to Logout?")
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								SharedPreference.setSharedPrefer(
										getApplicationContext(),
										SharedPreference.LOGOUT_PREF, "logout");
								finish();
								System.exit(0);
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						SharedPreference.setSharedPrefer(
								getApplicationContext(),
								SharedPreference.LOGOUT_PREF, "notlogout");
						dialog.dismiss();
					}
				}).show();
	}

	class PostLogData extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			postServiceDetailData();
			return "";
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			try {
				if ((progressDialogPost != null)
						&& progressDialogPost.isShowing()) {
					progressDialogPost.dismiss();
					progressDialogPost = null;
				}

			} catch (Exception e) {
				// TODO: handle exception
			}
			try {

				JSONArray responseArray = new JSONArray(result);
				for (int i = 0; i < responseArray.length(); i++) {
					JSONObject jsResponse = responseArray.getJSONObject(i);
					String Response = jsResponse.getString("Response").trim();
					String From = jsResponse.getString("From").trim();
					String ServiceID_new = jsResponse
							.getString("ServiceID_new").trim();
					if (Response.equals("OK")) {
						int delete1 = houstonFlowFunctions
								.deleteSelectedTableData(
										DatabaseHelper.TABLE_LOG,
										ParameterUtil.ServiceID_new,
										ServiceID_new);
						LoggerUtil
								.e("Log Data Delete", String.valueOf(delete1));
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			LoggerUtil.e("getting data from server====",
					"==============================================");
			clearAllFilterCriteria();
			if (userDTO != null) {
				getAllRecordFromServer();
			}
		}

	}

	private final String SOAP_ACTION_POST_SERVICE_DETAIL = CommonFunction.IPAD_ABC
			+ "Save_One_WO";
	private final String METHOD_NAME_POST_SERVICE_DETAIL = "Save_One_WO";

	public void postServiceDetailData() {
		try {
			JSONArray jsServiceDetail = houstonFlowFunctions
					.getServiceDetailDTOList();
			LoggerUtil.e("jsServiceDetail", jsServiceDetail + "");
			ArrayList<LogDTO> jsServiceDetailToSynch = new Gson().fromJson(
					jsServiceDetail.toString(), new TypeToken<List<LogDTO>>() {
					}.getType());
			for (int i = 0; i < jsServiceDetailToSynch.size(); i++) {
				LogDTO detailsDto = jsServiceDetailToSynch.get(i);
				SoapObject request = new SoapObject(NAMESPACE,
						METHOD_NAME_POST_SERVICE_DETAIL);
				PropertyInfo SID = new PropertyInfo();
				SID.setName("SID");
				SID.setValue(detailsDto.getSID() + "");
				request.addProperty(SID);

				PropertyInfo GeneralSR = new PropertyInfo();
				GeneralSR.setName("GeneralSR");
				GeneralSR.setValue(detailsDto.getGeneralSR() + "");
				request.addProperty(GeneralSR);

				PropertyInfo Residetial_Insp = new PropertyInfo();
				Residetial_Insp.setName("Residetial_Insp");
				Residetial_Insp.setValue(detailsDto.getResidetial_Insp() + "");
				request.addProperty(Residetial_Insp);

				PropertyInfo Chemical = new PropertyInfo();
				Chemical.setName("Chemical");
				Chemical.setValue(detailsDto.getChemical() + "");
				request.addProperty(Chemical);

				PropertyInfo AgreeMentMailID = new PropertyInfo();
				AgreeMentMailID.setName("AgreeMentMailID");
				AgreeMentMailID.setValue(detailsDto.getAgreeMentMailID() + "");
				request.addProperty(AgreeMentMailID);

				PropertyInfo CrewMember = new PropertyInfo();
				CrewMember.setName("CrewMember");
				CrewMember.setValue(detailsDto.getCrew_Member() + "");
				request.addProperty(CrewMember);

				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
						SoapEnvelope.VER11);
				envelope.dotNet = true;
				envelope.setOutputSoapObject(request);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				try {
					androidHttpTransport.call(SOAP_ACTION_POST_SERVICE_DETAIL,
							envelope);
					SoapPrimitive response = (SoapPrimitive) envelope
							.getResponse();
					String res = response.toString();
					LoggerUtil.e("postServiceDetailData", res + "");
					if (res.contains("Ok")) {
						houstonFlowFunctions.deleteSelectedTableData(
								DatabaseHelper.TABLE_LOG, ParameterUtil.SID,
								detailsDto.getSID());
					}
					// Log.e("Response", res.toLowerCase());
				} catch (IOException e) {
					LoggerUtil.e("IOException", e.toString());
				}
			}
		} catch (XmlPullParserException e) {
			LoggerUtil.e("Exception", e.toString());
		}
	}

	//private final String SOAP_ACTION_POST_SERVICE = CommonFunction.IPAD_ABC
	//		+ "Save_All_WO";
	//private final String METHOD_NAME_POST_SERVICE = "Save_All_WO";
	 private final String SOAP_ACTION_POST_SERVICE = CommonFunction.IPAD_ABC+
	 "Save_All_Commercial_WO";
	 private final String METHOD_NAME_POST_SERVICE = "Save_All_Commercial_WO";

	ProgressDialog progressDialogPost;

	class POSTAll_Data_ServiceNewID extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			String downloading = getResources().getString(R.string.SYNC);
			String pleasewait = getResources().getString(R.string.please_wait);
			progressDialogPost = ProgressDialog.show(ServiceListAcitivity.this,
					downloading, pleasewait, true); // TODO
		}

		@Override
		protected String doInBackground(String... params) {
			return Post_Service_Data();
		}

		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				JSONArray responseArray = new JSONArray(result);
				for (int i = 0; i < responseArray.length(); i++) {
					JSONObject jsResponse = responseArray.getJSONObject(i);
					String Response = jsResponse.getString("Response").trim();
					String From = jsResponse.getString("From").trim();
					String ServiceID_new = jsResponse
							.getString("ServiceID_new").trim();
					LoggerUtil.e("RESPONSE SYNC", String.valueOf(Response)
							+ " " + ServiceID_new + " " + From);
					// No Change.....
					if (From.equals("GeneralSR") && Response.equals("deleted")) {
						int deleteGenSr = houstonFlowFunctions
								.deleteSelectedTableData(
										DatabaseHelper.TABLE_GENERAL_INFO,
										ParameterUtil.ServiceID_new,
										ServiceID_new);
						int deleteResiSr = houstonFlowFunctions
								.deleteSelectedTableData(
										DatabaseHelper.TABLE_HR_PEST_INFO,
										ParameterUtil.ServiceID_new,
										ServiceID_new);
						int deleteCheSr = houstonFlowFunctions
								.deleteSelectedTableData(
										DatabaseHelper.TABLE_SELECTED_CHEMICALS,
										ParameterUtil.ServiceID_new,
										ServiceID_new);
						int deleteCrew = houstonFlowFunctions
								.deleteSelectedTableData(
										DatabaseHelper.TABLE_SELECTED_CREW_MEMBER,
										ParameterUtil.ServiceID_new,
										ServiceID_new);
						int deleteLogData = houstonFlowFunctions
								.deleteSelectedTableData(
										DatabaseHelper.TABLE_LOG,
										ParameterUtil.ServiceID_new,
										ServiceID_new);
						LoggerUtil.e("ALL Delete", String.valueOf(deleteGenSr));
					}
					// Change
					if (From.equals("GeneralSR") && Response.equals("OK")) {

						houstonFlowFunctions.updateServiceUploadedToServer(
								DatabaseHelper.TABLE_GENERAL_INFO,
								ServiceID_new, "0");
						houstonFlowFunctions
								.deleteAllTableData(DatabaseHelper.TABLE_AGREEMENTMAILID);
						LoggerUtil.e("GeneralSR Response",
								String.valueOf(Response));
					}

					// Change
					if (From.equals("Residential") && Response.equals("OK")) {

						houstonFlowFunctions.updateServiceUploadedToServer(
								DatabaseHelper.TABLE_HR_PEST_INFO,
								ServiceID_new, "0");

						LoggerUtil.e("Residential Response",
								String.valueOf(Response));
					}

					// Change
					if (From.equals("Commercial") && Response.equals("OK")) {

						houstonFlowFunctions.updateServiceUploadedToServer(
								DatabaseHelper.TABLE_HC_PEST_INFO,
								ServiceID_new, "0");

					}

					if (From.equals("Chemical") && Response.equals("OK")) {

						houstonFlowFunctions.updateServiceUploadedToServer(
								DatabaseHelper.TABLE_SELECTED_CHEMICALS,
								ServiceID_new, "0");

						LoggerUtil.e("Chemical Response",
								String.valueOf(Response));
					}

					if (From.equals("CrewMember") && Response.equals("OK")) {

						int delete1 = houstonFlowFunctions
								.deleteSelectedTableData(
										DatabaseHelper.TABLE_SELECTED_CREW_MEMBER,
										ParameterUtil.ServiceID_new,
										ServiceID_new);
					}
				}
			} catch (Exception e) {

			}
			PostLogData postLogData = new PostLogData();
			postLogData.execute("");
			/*
			 * .clearAllFilterCriteria(); getAllRecordFromServer();
			 */
		}
	}

	private void clearAllFilterCriteria() {
		// TODO Auto-generated method stub
		edtfirstName.getText().clear();
		edtlastName.getText().clear();
		edtemailAddress.getText().clear();
		edtaccountNo.getText().clear();
		edtaddress.getText().clear();
		edtcity.getText().clear();
		edtstate.getText().clear();
		edtworkOrder.getText().clear();
		spnserviceStatus.setSelection(0);
		// spnserviceBranch.setSelection(0);
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		// edtTodate.setText(dateFormat.format(new Date()));
		edtFromdate.setText(dateFormat.format(new Date()));

	}

	public String Post_Service_Data() {
		// return METHOD_NAME_Get_AddressInformation;
		Gson gson = new Gson();

		// Upload After and Before Image
		ArrayList<GeneralInfoDTO> serviceListToSynch = new Gson().fromJson(
				houstonFlowFunctions.getAllServiceChangeLocaly().toString(),
				new TypeToken<List<GeneralInfoDTO>>() {
				}.getType());
		LoggerUtil.e("serviceListToSynch","serviceListToSynch..."+ serviceListToSynch.size());

		for (int i = 0; i < serviceListToSynch.size(); i++) {
			serviceListToSynch.get(i).setMobile_Info(String.valueOf(android.os.Build.MODEL + " " + android.os.Build.VERSION.RELEASE));
			serviceListToSynch.get(i).setApp_Info(String.valueOf("VERSION " + CommonFunction.getAppVersion(getApplicationContext())));
			serviceListToSynch.get(i).setServiceDate(serviceListToSynch.get(i).getServiceDate() + " "+ serviceListToSynch.get(i).getServiceTime());
				
			String afterImage = serviceListToSynch.get(i).getAfterImage();
			String beforeImage = serviceListToSynch.get(i).getBeforeImage();
			String audioFileName = serviceListToSynch.get(i).getAudioFile();
			String checkFrontImage = serviceListToSynch.get(i).getCheckFrontImage();
			String checkBackImage = serviceListToSynch.get(i).getCheckBackImage();

			if (!CommonFunction.checkString(afterImage, "").equals("")) {
				String afterImages[] = afterImage.split(",");
				for (int a = 0; a < afterImages.length; a++) {
					String filePath = CommonFunction.getFileLocation(
							ServiceListAcitivity.this, afterImages[a])
							.toString();
					String res = uploadFile(filePath,
							CommonFunction.UPLOAD_IMAGE_URL);
					// LoggerUtil.e("afterImage Upload Res", res);
				}
			}
			if (!CommonFunction.checkString(beforeImage, "").equals("")) {
				String beforeImages[] = beforeImage.split(",");
				for (int b = 0; b < beforeImages.length; b++) {
					String filePath = CommonFunction.getFileLocation(
							ServiceListAcitivity.this, beforeImages[b])
							.toString();
					String res = uploadFile(filePath,
							CommonFunction.UPLOAD_IMAGE_URL);
					// LoggerUtil.e("beforeImage Upload Res", res);
				}
			}
			if (!CommonFunction.checkString(audioFileName, "").equals("")) {
				String filePath = CommonFunction
						.getAudioFilePath(audioFileName).toString();
				String res = uploadFile(filePath,
						CommonFunction.UPLOAD_IMAGE_URL);
				// LoggerUtil.e("audioFileName Upload Res", res);
			}
			if (!CommonFunction.checkString(checkFrontImage, "").equals("")) {
				String filePath = CommonFunction.getFileLocation(
						ServiceListAcitivity.this, checkFrontImage).toString();
				String res = uploadFile(filePath,
						CommonFunction.UPLOAD_CHECK_FRONT_IMAGE_URL);
				// LoggerUtil.e("chechFrontImage Upload Res", res);
			}
			if (!CommonFunction.checkString(checkBackImage, "").equals("")) {
				String filePath = CommonFunction.getFileLocation(
						ServiceListAcitivity.this, checkBackImage).toString();
				String res = uploadFile(filePath,
						CommonFunction.UPLOAD_CHECK_FRONT_IMAGE_URL);
				LoggerUtil.e("chechBackImage Upload Res", res);
			}
		}

		SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_POST_SERVICE);
		PropertyInfo genralInformationDATA = new PropertyInfo();
		genralInformationDATA.setName("GeneralSR");
		
		
		
		genralInformationDATA.setValue(gson.toJson(serviceListToSynch));
		request.addProperty(genralInformationDATA);
		
		LoggerUtil.e("GeneralSR insp", gson.toJson(serviceListToSynch));

		//-------------------------------------------------------------------
		
		ArrayList<HResidential_pest_DTO> residentailDataToSynch = new Gson()
				.fromJson(
						houstonFlowFunctions
								.getCompleteResidential_Commercial_pestService(
										DatabaseHelper.TABLE_HR_PEST_INFO)
								.toString(),
						new TypeToken<List<HResidential_pest_DTO>>() {
						}.getType());
		PropertyInfo residential_pesttData = new PropertyInfo();
		residential_pesttData.setName("Residetial_Insp");
		// Special Character...
		// Upload Customer And Technician Signature Singnature
		for (int i = 0; i < residentailDataToSynch.size(); i++) {
			String technicianSignImage = residentailDataToSynch.get(i)
					.getServiceSignature();
			String customerSignImage = residentailDataToSynch.get(i)
					.getSignature();
			// Technician Sign Image Upload

			if (!CommonFunction.checkString(technicianSignImage, "").equals("")) {

				String filePath = CommonFunction.getFileLocation(
						ServiceListAcitivity.this, technicianSignImage)
						.toString();
				String res = uploadFile(filePath,
						CommonFunction.UPLOAD_RESIDENTIAL_SIGNATURE);
				// LoggerUtil.e("technicianSignImage Upload Res", res);
			}
			// Customer Sign Image Upload
			if (!CommonFunction.checkString(customerSignImage, "").equals("")) {
				String filePath = CommonFunction.getFileLocation(
						ServiceListAcitivity.this, customerSignImage)
						.toString();
				String res = uploadFile(filePath,
						CommonFunction.UPLOAD_RESIDENTIAL_SIGNATURE);
				// LoggerUtil.e("customerSignImage Upload Res", res);
			}
		}

		residential_pesttData.setValue(gson.toJson(residentailDataToSynch));
		request.addProperty(residential_pesttData);

		LoggerUtil.e("Residetial_Insp", gson.toJson(residentailDataToSynch));

		//-------------------------------------------------------------------------
		
		ArrayList<HCommercial_pest_DTO> commercialDataToSynch = new Gson().fromJson(
				houstonFlowFunctions
						.getCompleteResidential_Commercial_pestService(
								DatabaseHelper.TABLE_HC_PEST_INFO)
						.toString(), new TypeToken<List<HCommercial_pest_DTO>>() {
				}.getType());

		PropertyInfo commercial_pesttData = new PropertyInfo();
		commercial_pesttData.setName("Commercial_Insp");

		// Upload Customer And Technician Signature Singnature
		for (int i = 0; i < commercialDataToSynch.size(); i++) {
			String technicianSignImage = commercialDataToSynch.get(i)
					.getServiceSignature();
			String customerSignImage = commercialDataToSynch.get(i)
					.getSignature();
			// Technician Sign Image Upload

			if (!CommonFunction.checkString(technicianSignImage, "").equals("")) {

				String filePath = CommonFunction.getFileLocation(
						ServiceListAcitivity.this, technicianSignImage)
						.toString();
				String res = uploadFile(filePath,
						CommonFunction.UPLOAD_COMMERCIAL_SIGNATURE);
				// LoggerUtil.e("technicianSignImage Upload Res", res);
			}
			// Customer Sign Image Upload
			if (!CommonFunction.checkString(customerSignImage, "").equals("")) {
				String filePath = CommonFunction.getFileLocation(
						ServiceListAcitivity.this, customerSignImage)
						.toString();
				String res = uploadFile(filePath,
						CommonFunction.UPLOAD_COMMERCIAL_SIGNATURE);
				// LoggerUtil.e("customerSignImage Upload Res", res);
			}
		}

		commercial_pesttData.setValue(gson.toJson(commercialDataToSynch));
		request.addProperty(commercial_pesttData);

		LoggerUtil.e("Commercial_Insp", gson.toJson(commercialDataToSynch));

		//-----------------------------------------------------------------
		
		/*ArrayList<OResidential_pest_DTO> orlandoResDataToSynch = new Gson().fromJson(
				houstonFlowFunctions
						.getCompleteResidential_Commercial_pestService(
								DatabaseHelper.TABLE_OR_PEST_INFO)
						.toString(), new TypeToken<List<OResidential_pest_DTO>>() {
				}.getType());

		PropertyInfo OResidential_pesttData = new PropertyInfo();
		OResidential_pesttData.setName("Orlando_Insp");

		// Upload Customer And Technician Signature Singnature
		for (int i = 0; i < orlandoResDataToSynch.size(); i++) {
			String technicianSignImage = orlandoResDataToSynch.get(i)
					.getServiceSignature();
			String customerSignImage = orlandoResDataToSynch.get(i)
					.getSignature();
			// Technician Sign Image Upload

			if (!CommonFunction.checkString(technicianSignImage, "").equals("")) {

				String filePath = CommonFunction.getFileLocation(
						ServiceListAcitivity.this, technicianSignImage)
						.toString();
				String res = uploadFile(filePath,
						CommonFunction.UPLOAD_ORLANDO_SIGNATURE);
				// LoggerUtil.e("technicianSignImage Upload Res", res);
			}
			// Customer Sign Image Upload
			if (!CommonFunction.checkString(customerSignImage, "").equals("")) {
				String filePath = CommonFunction.getFileLocation(
						ServiceListAcitivity.this, customerSignImage)
						.toString();
				String res = uploadFile(filePath,
						CommonFunction.UPLOAD_ORLANDO_SIGNATURE);
				// LoggerUtil.e("customerSignImage Upload Res", res);
			}
		}

		OResidential_pesttData.setValue(gson.toJson(orlandoResDataToSynch));
		request.addProperty(OResidential_pesttData);

		LoggerUtil.e("Orlando res_Insp", gson.toJson(orlandoResDataToSynch));
*/
	//-------------------------------------------------------------------
		// sync chemicals
		ArrayList<ChemicalDto> chemicalDataToSynch = new Gson().fromJson(
				houstonFlowFunctions.getAllServiceSelectedChemicalListToUpload(
						DatabaseHelper.TABLE_SELECTED_CHEMICALS).toString(),
				new TypeToken<List<ChemicalDto>>() {
				}.getType());
		/*
		 * for (int i = 0; i < chemicalDataToSynch.size(); i++) {
		 * if(CommonFunction
		 * .checkString(chemicalDataToSynch.get(i).getPestPrevention_ID(),
		 * "").equals("")) { chemicalDataToSynch. } else {
		 * chemicalDto.setPestPrevention_ID
		 * (listChemicals.get(i).getPestPrevention_ID()); } }
		 */

		// JSONArray chemicalJSONArray =
		// houstonFlowFunctions.getAllServiceSelectedChemicalListToUpload(DatabaseHelper.TABLE_SELECTED_CHEMICALS);
		PropertyInfo chemicalstData = new PropertyInfo();
		chemicalstData.setName("Chemical");
		// if (chemicalDataToSynch.length() < 0)
		// {
		// chemicalstData.setValue("");
		// }
		// else
		// {
		chemicalstData.setValue(gson.toJson(chemicalDataToSynch));
		// }
		request.addProperty(chemicalstData);
		LoggerUtil.e("Chemical_Insp", gson.toJson(chemicalDataToSynch));

		JSONArray crewListArray = houstonFlowFunctions.getSelectedCrewMember();
		ArrayList<Selected_crewDTO> crewList = new Gson().fromJson(
				crewListArray.toString(),
				new TypeToken<List<Selected_crewDTO>>() {
				}.getType());

		PropertyInfo crewMemberData = new PropertyInfo();
		crewMemberData.setName("CrewMember");
		// LoggerUtil.e("CrewMembers", gson.toJson(crewList));
		crewMemberData.setValue(gson.toJson(crewList));
		request.addProperty(crewMemberData);

		//-------------------------------------------------------------------
		
		JSONArray emailIDListArray = houstonFlowFunctions
				.getAllSelectedEmaiLID();
		ArrayList<AgreementMailIdDTO> emailList = new Gson().fromJson(
				emailIDListArray.toString(),
				new TypeToken<List<AgreementMailIdDTO>>() {
				}.getType());
		;
		PropertyInfo emailAddressData = new PropertyInfo();
		emailAddressData.setName("AgreeMentMailID");
		// LoggerUtil.e("AGREEMENT MAIL SYNCHRONIZATION",
		// gson.toJson(emailList));
		emailAddressData.setValue(gson.toJson(emailList));
		request.addProperty(emailAddressData);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(SOAP_ACTION_POST_SERVICE, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			String res = response.toString();
			LoggerUtil.e("Post_Service_Data", res + "");
			try {
			} catch (Exception e) {
				LoggerUtil.e("Exception", e.toString());

			}
			return res;
		} catch (IOException e) {
			LoggerUtil.e("IOException", e.toString());
		} catch (XmlPullParserException e) {
			LoggerUtil.e("Get_Service_Data_By_ServiceNewID", e.toString());
		}
		return "[{\"result\":\"No Record Found\"}]";
	}

	public String uploadFile(String filePath, String serverUrl) {
		String fileUploadResponse = CommonFunction.uploadFile(filePath,
				serverUrl);
		return fileUploadResponse;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onRestart()
	 */
	/*
	 * @Override protected void onRestart() { // TODO Auto-generated method stub
	 * if(CommonFunction.haveInternet(getApplicationContext())) { SyncAllData();
	 * } super.onRestart(); }
	 */

}
