package quacito.houston.Flow;

import quacito.houston.CommonUtilities.ParameterUtil;
import quacito.houston.CommonUtilities.SharedPreference;
import quacito.houston.DBhelper.DatabaseHelper;
import quacito.houston.DBhelper.HoustonFlowFunctions;
import quacito.houston.Flow.servicehouston.R;
import quacito.houston.model.GeneralInfoDTO;
import quacito.houston.model.LogDTO;
import quacito.houston.model.UserDTO;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SelectServiceAcitvity extends Activity {
	TextView txtResidential, txtCommercial;
	LinearLayout layoutResidential, layoutCommercial;
	static String serviceID_new;
	UserDTO userDTO;
	TextView txtAcctNo, txtOrderNumber;
	GeneralInfoDTO generalInfoDTO;
	HoustonFlowFunctions houstonFlowFunctions;
	// String strViewDetail =
	// "<u>"+getResources().getString(R.string.VIEW_DETAIL)+"</u>";
	// String strFillDetail =
	// "<u>"+getResources().getString(R.string.FILL_DETAIL)+"</u>";

	String strViewDetail = "<u>View Work Order Detail</u>";
	String strFillDetail = "<u>Fill Work Order Detail</u>";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.activity_select_service);

		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setDisplayShowHomeEnabled(true);

		initialize();
		checkServiceStatus();

		super.onCreate(savedInstanceState);
	}

	private void checkServiceStatus() {
		// TODO Auto-generated method stub
		//&& houstonFlowFunctions.isAlreadyAdded(serviceID_new,
		//		DatabaseHelper.TABLE_COMMERCIAL_PEST_INFO)
		if (generalInfoDTO.getServiceStatus().equals("Complete")
			&& generalInfoDTO.getServiceNameType().equals("HoustonCommercial"))
		{
			txtCommercial.setText(Html.fromHtml(strViewDetail));

			layoutResidential.setEnabled(false);
			txtResidential.setEnabled(false);
			txtResidential.setTextColor(new Color().parseColor("#6E6E6E"));
			txtResidential.setText(Html.fromHtml(strViewDetail));
		}

		//&& houstonFlowFunctions.isAlreadyAdded(serviceID_new,
			//	DatabaseHelper.TABLE_RESIDENTIAL_PEST_INFO)
		else if (generalInfoDTO.getServiceStatus().equals("Complete")
				&& generalInfoDTO.getServiceNameType().equals("HoustonResidential")) 
		{
			txtResidential.setText(Html.fromHtml(strViewDetail));

			layoutCommercial.setEnabled(false);
			txtCommercial.setEnabled(false);
			txtCommercial.setTextColor(new Color().parseColor("#6E6E6E"));
			txtCommercial.setText(Html.fromHtml(strViewDetail));

		} else {
			txtResidential.setText(Html.fromHtml(strFillDetail));
			txtCommercial.setText(Html.fromHtml(strFillDetail));
		}
	}

	private void initialize() {
		// TODO Auto-generated method stub
		houstonFlowFunctions = new HoustonFlowFunctions(getApplicationContext());
		userDTO = (UserDTO) this.getIntent().getExtras()
				.getSerializable("userDTO");
		generalInfoDTO = (GeneralInfoDTO) this.getIntent().getExtras()
				.getSerializable("generalInfoDTO");
		serviceID_new = this.getIntent().getExtras()
				.getString(ParameterUtil.ServiceID_new);

		layoutCommercial = (LinearLayout) findViewById(R.id.layoutCommercial);
		layoutResidential = (LinearLayout) findViewById(R.id.layoutResidential);

		txtAcctNo = (TextView) findViewById(R.id.txtAcctNo);
		txtOrderNumber = (TextView) findViewById(R.id.txtOrderNumber);
		txtAcctNo.setText(generalInfoDTO.getAccountNo());
		// txtTimeIn.setText(generalInfoDTO.getTimeIn());
		txtOrderNumber.setText(generalInfoDTO.getOrder_Number());

		txtCommercial = (TextView) findViewById(R.id.txtCommercial);
		layoutCommercial.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(SelectServiceAcitvity.this,
						HC_PestControlServiceReport.class);
				i.putExtra("userDTO", userDTO);
				i.putExtra("generalInfoDTO", generalInfoDTO);
				i.putExtra(ParameterUtil.ServiceID_new, serviceID_new);

				startActivity(i);
			}
		});

		txtResidential = (TextView) findViewById(R.id.txtResidential);
		layoutResidential.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(SelectServiceAcitvity.this,
						HR_PestControlServiceReport.class);
				i.putExtra("userDTO", userDTO);
				i.putExtra("generalInfoDTO", generalInfoDTO);
				i.putExtra(ParameterUtil.ServiceID_new, serviceID_new);
				startActivity(i);
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();

		if (id == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
}
