package quacito.houston.Flow;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import quacito.houston.CommonUtilities.CommonFunction;
import quacito.houston.CommonUtilities.DecimalDigitsInputFilter;
import quacito.houston.CommonUtilities.LoggerUtil;
import quacito.houston.CommonUtilities.ParameterUtil;
import quacito.houston.DBhelper.DatabaseHelper;
import quacito.houston.DBhelper.HoustonFlowFunctions;
import quacito.houston.Flow.HR_PestControlServiceReport.OnButtonClick;
import quacito.houston.Flow.HR_PestControlServiceReport.OnCheckedboxChangeListener;
import quacito.houston.Flow.HR_PestControlServiceReport.OnGalleryClickBack;
import quacito.houston.Flow.HR_PestControlServiceReport.OnGalleryClickFront;
import quacito.houston.Flow.HR_PestControlServiceReport.OnRadioChange;

import quacito.houston.Flow.servicehouston.R;
import quacito.houston.adapter.GalleryImageAdapter;
import quacito.houston.horizontallistview.HorizontalListView;
import quacito.houston.model.ChemicalDto;
import quacito.houston.model.ChemicalsComponentDTO;
import quacito.houston.model.HCommercial_pest_DTO;
import quacito.houston.model.GeneralInfoDTO;
import quacito.houston.model.ImgDto;
import quacito.houston.model.LogDTO;
//import quacito.houston.model.Residential_pest_DTO;
import quacito.houston.model.UserDTO;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class HC_PestControlServiceReport extends Activity {
	long mLastClickTime = 0;
	int checkChange = 0;
	HCommercial_pest_DTO hCommercial_pest_DTO;
	LogDTO logDTO;
	UserDTO userDTO;

	GeneralInfoDTO generalInfoDTO;
	Uri imageUri;
	Dialog dialogFrontImage, dialogBackImage;
	public static Bitmap bitmapFront, bitmapBack;
	GalleryImageAdapter adapterFront, adapterBack;
	int imageCountFront = 1, imageCountBack = 1;
	HorizontalListView imageFrontListView, imageBackListView;
	public static final int REQUEST_Camera_ACTIVITY_CODE_Front = 101;
	public static final int REQUEST_Camera_ACTIVITY_CODE_Back = 102;
	public static final int REQUEST_Gallery_ACTIVITY_CODE_Front = 103;
	public static final int REQUEST_Gallery_ACTIVITY_CODE_Back = 104;
	HoustonFlowFunctions houstonFlowFunctions;
	LinearLayout mainLayout, LinearlayoutCHECK_IMAGE,
			LinearlayoutCHECK_FRONT_IMAGE, LinearlayoutCHECK_BACK_IMAGE,
			trExpirationDateValidation;
	Button btnCHECK_FRONT_IMAGE, btnCHECK_BACK_IMAGE,
			btnsaveandcontinueINVOICE, btnCancel;
	HorizontalListView listImg_front, listImg_back;

	TableRow tramount, trcheckno, trDrivingLicence, trExpirationDate,
			tablerowCharactersTyped, tableRowTREATED_CONDUSIVE_CONDITIONS,
			tableRowTREATED_SLAB_PENETRATIONS,
			tableRowTREATED_PERIMETER_STRUCTURE,
			tableRowREPLACED_MONITORING_STATIONS2,
			tableRowREPLACED_MONITORING_STATIONS1,
			tableRowVISIBLE_SIGN_INTERIOR_STRUCTURE,
			tableRowVISIBLE_SIGN_PERIMETER_STRUCTURE,
			tableRowINSTALLED_MONITORING_STATIONS,
			tableRowVISIBLE_SIGN_TERMITE_MONITORING_STATIONS2,
			tableRowVISIBLE_SIGN_TERMITE_MONITORING_STATIONS1,
			tableRow_INSPECTED_RODENT_STATIONS,
			tableRow_CLEANES_RODENT_STATIONS,
			tableRow_INSTALLED_RODENT_BAIT_STATIONS1,
			tableRow_INSTALLED_RODENT_BAIT_STATIONS2, tableRow_REPLACED_BAIT1,
			tableRow_REPLACED_BAIT2, tableRow_REPLACED_BAIT_STATIONS1,
			tableRow_REPLACED_BAIT_STATIONS2, tableRow_REPLACED_BAIT_STATIONS3,
			tableRow_INSPECTED_RODENT_TRAPS, tableRow_CLEANED_RODENT_TRAPS,
			tableRow_INSTALLED_RODENT_TRAPS1, tableRow_INSTALLED_RODENT_TRAPS2,
			tableRow_REPLACED_RODENT_TRAPS1, tableRow_REPLACED_RODENT_TRAPS2,
			tableRow_REPLACED_RODENT_TRAPS3, tableRow_SEALED_ENTRY_POINTS,
			tableRow_INSPECTED_SNAP_TRAPS, tableRow_INSPECTED_LIVE_CAGES,
			tableRow_REMOVED_SNAP_TRAPS, tableRow_REMOVED_LIVE_CAGES,
			tableRow_REMOVED_RODENT_BAIT_STATIONS,
			tableRow_REMOVED_RODENT_TRAPS, tableRow_SET_SNAP_TRAPS1,
			tableRow_SET_SNAP_TRAPS2, tableRow_SET_SNAP_TRAPS3,
			tableRow_SET_LIVE_CAGES1, tableRow_SET_LIVE_CAGES2,
			tableRow_SET_LIVE_CAGES3;

	String CheckFrontImageName, CheckBackImageName, serviceID_new,
			strServiceFor, strAreasInspectedInterior,
			strAreasInspectedExterior, strInspectedadnTreatedFor,
			strpestActivityByZone, strPestControlServices, strMosquito,
			strSnapTraps, strLivecages, strLiveCages, strRecommendations,
			strCommercialId;

	TextView txtAccountNumber, txtOrderNumber, product, PercentHead,
			AmountHead, UnitHead, TargetHead, textviewCharactersTyped;

	String strEdttxtInspectedandTreatedFor1, strEdttxtInspectedandTreatedFor2,
			strOutsidepestControlServices, strCheckSignOfTermiteMS,
			strSignOfTermiteMS, strStationSignOfTermiteMS,
			strINSTALLED_MONITORING_STATIONS, strInsidepestControlServices,
			strCheckINSTALLED_MS, strInterior, strOutsidef_Perimeter,
			strDumpster, strDining_Room, strCommon_Areas, strKitchen,
			strDry_Storage, strDish_Washing, strRoof_Tops, strWait_Stations,
			strDrop_Ceiling, strPlanters, strWarehouse_Storage,
			strSignOfTermitePS, strCheckSignOfTermitePS, strSignOfTermiteIS,
			strCheckSignOfTermiteIS, strCheckREPLACED_MS, strTreatedPS,
			strCheckTreatedPS, strStationREPLACED_MS, strREPLACED_MS,
			strTreatedSP, strCheckTreatedSP, strTreatedCC, strCheckTreatedCC,
			strMosquitoService1, strMosquitoService2, strCheckDEBRIS_IN_DRAINS,
			strCheckINACCSISIBLE_AREAS, strCheckMOISTURE_WALLS,
			strCheckDOCK_DOOR_LEFT_OPEN, strCheckDEBRIS_UNDER_COOKLINE,
			strCheckDOOR_PROPPED_OPEN, strCheckTRASH_CANTS_NOT_EMPTIED,
			strCheckWEATHER_STRIPPING, strCheckSYRUP_SODA, strCheckAIR_CURTAIN,
			strCheckDIRTY_DINING_TABLE, strCheckFLY_LIGHTS,
			strCheckFILM_BAR_TaPS, strEdttxtInvValue, strEdttxtProdValue,
			strEdttxtTax, strEdttxtTotal, strEdttxtTechComments,
			strEdttxtServiceTech, strEdttxtEmp, strEdttxtCustomer,
			strEdttxtdate, stredtxtTimeIn, strHowMany_INSPECTED_SNAP_TRAPS,
			strCheckINSPECTED_SNAP_TRAPS, strEdttxtAmount, strEdttxtCheckNo,
			strEdttxtDrivingLicence, strEdtExpirationDate, strRadioInvoice,
			strCheckINSPECTED_RODENT_STATIONS,
			strRadioINSPECTED_RODENT_STATIONS,
			strHowManyINSPECTED_RODENT_STATIONS,
			strCheckCLEANES_RODENT_STATIONS,
			strCheckINSTALLED_RODENT_BAIT_STATIONS,
			strHowManyINSTALLED_RODENT_BAIT_STATIONS,
			strHowManyREPLACED_BAIT_STATIONS, strCheckREPLACED_BAIT_STATIONS,
			strRadioREPLACED_BAIT_STATIONS, strStationREPLACED_BAIT_STATIONS,
			strRadioINSTALLED_RODENT_BAIT_STATIONS, strCheckREPLACED_BAIT,
			strStationREPLACED_BAIT, strRadioREPLACED_BAIT,
			strCheckREPLACED_RODENT_TRAPS, strHowManyREPLACED_RODENT_TRAPS,
			strStationREPLACED_RODENT_TRAPS, strCheckINSTALLED_RODENT_TRAPS,
			strHowManyINSTALLED_RODENT_TRAPS, strRadioINSTALLED_RODENT_TRAPS,
			strCheckCLEANED_RODENT_TRAPS, strRadioCLEANED_RODENT_TRAPS,
			strCheckINSPECTED_RODENT_TRAPS, strRadioINSPECTED_RODENT_TRAPS,
			strRadioCLEANES_RODENT_STATIONS, strCheckSEALED_ENTRY_POINTS,
			strRadioSEALED_ENTRY_POINTS, strRadioREPLACED_RODENT_TRAPS,
			strCheckINSPECTED_LIVE_CAGES, strHowManyINSPECTED_LIVE_CAGES,
			strCheckREMOVED_RODENT_TRAPS, strHowManyREMOVED_RODENT_TRAPS,
			strCheckREMOVED_RODENT_BAIT_STATIONS,
			strHowManyREMOVED_RODENT_BAIT_STATIONS, strCheckREMOVED_LIVE_CAGES,
			strHowManyREMOVED_LIVE_CAGES, strCheckREMOVED_SNAP_TRAPS,
			strHowManyREMOVED_SNAP_TRAPS, strCheckSET_LIVE_CAGES,
			strCheckSET_SNAP_TRAPS, strSET_SNAP_TRAPS,
			strCheckChildSET_SNAP_TRAPS, strSET_LIVE_CAGES,
			strCheckChildSET_LIVE_CAGES;

	boolean isValidated = true;
	boolean isFirstTime = true;
	ArrayList<String> validationList = new ArrayList<String>();
	ArrayList<CheckBox> serviceForCheckboxList = new ArrayList<CheckBox>();
	ArrayList<CheckBox> AreasInspectedInteriorCheckboxList = new ArrayList<CheckBox>();
	ArrayList<CheckBox> AreasInspectedExteriorCheckboxList = new ArrayList<CheckBox>();
	ArrayList<CheckBox> InspectedTreatedForCheckboxList = new ArrayList<CheckBox>();
	ArrayList<CheckBox> PestActivityZoneCheckboxList = new ArrayList<CheckBox>();
	ArrayList<CheckBox> pestControlServicesCheckboxList = new ArrayList<CheckBox>();
	ArrayList<CheckBox> mosquitoCheckboxList = new ArrayList<CheckBox>();
	ArrayList<CheckBox> recommendationCheckboxList = new ArrayList<CheckBox>();
	ArrayList<CheckBox> OutsidepestControlServicesCheckboxList = new ArrayList<CheckBox>();
	ArrayList<CheckBox> InsidepestControlServicesCheckboxList = new ArrayList<CheckBox>();
	ArrayList<CheckBox> liveCagesCheckboxList = new ArrayList<CheckBox>();
	ArrayList<CheckBox> snapTrapsCheckboxList = new ArrayList<CheckBox>();
	ArrayList<ChemicalDto> listChemicals;
	ArrayList<ChemicalDto> listSelectedChemicals = new ArrayList<ChemicalDto>();
	ArrayList<ChemicalDto> listPreviousSelectedChemicals = new ArrayList<ChemicalDto>();
	ArrayList<String> listRemoveSelectedChemicals = new ArrayList<String>();
	ArrayList<String> listPreviousPestId = new ArrayList<String>();
	ArrayList<ImgDto> listFrontImg = new ArrayList<ImgDto>();
	ArrayList<String> tempFrontImagList = new ArrayList<String>();
	ArrayList<String> tempStoreFrontImageName = new ArrayList<String>();
	ArrayList<ImgDto> listBackImg = new ArrayList<ImgDto>();
	ArrayList<String> tempBackImagList = new ArrayList<String>();
	ArrayList<String> tempStoreBackImageName = new ArrayList<String>();
	ArrayList<CheckBox> checkList = new ArrayList<CheckBox>();
	ArrayList<ChemicalsComponentDTO> chemicalsComponentList = new ArrayList<ChemicalsComponentDTO>();
	CheckBox checkBoxPLANTERS_PestActivtyByZone,
			checkboxPLANTERS_AreasInspected, checkBoxDRY_STORAGE_Pest_Activity,
			chemicalcheckBox, checkboxCommercialPcService, checkboxCommercialX,
			checkboxCommercialPcInitial, checkboxCommercialTermite,
			checkboxOFFICES, checkboxBREAKROOMS, checkboxRODENT_BAIT_STATIONS,
			checkboxLOG_BOOK, checkboxFOOD_CHARTS, checkboxPERIMETER,
			checkboxCOFFEE_BARS, checkBoxKITCHEN_Pest_Activity,
			checkboxLANDSCAPE_BEDS, checkboxLAUNDRY, checkboxHOUSE_KEEPING,
			checkboxMANHOLE_COVERS, checkBoxWAIT_STATION_PEST_ACTIVTY,
			checkboxWAIT_STATIONS_AREAS_INSPECTED,
			checkboxFLY_LIHGTS_AREAS_INSPECTED, checkboxGREASE_TRAPS,
			checkboxELEVATOR_PITS, checkboxMECHANICAL_ROOMS,
			checkboxROOF_TOPS_pestActivity_by_zone, checkboxPATIENT_ROOMS,
			checkboxNURSES_STATIONS, checkboxREST_ROOMS, checkboxDOCK,
			checkboxDISH_WSHING_AREAS_INSPECTED,
			checkBoxDISH_WSHING_PEST_ACTIVTY, checkboxDRY_STORAGE,
			checkboxENTRY_POINTS, checkboxBAR, checkboxLOBBY,
			checkBoxDINING_ROOM_PestActivityByZone,
			checkboxWarehouseStorage_AreasInspected,
			checkboxDiningRoom_AreasInspected, checkboxRoaches,
			checkboxAMERICAN_COCKROACHES, checkboxGERMAN_COCKROACHES,
			checkboxSPIDERS, checkboxSCORPIONS, checkboxSILVER_FISH,
			checkboxSTORED_PRODUCT_PESTS, checkboxPHARAOH_ANTS,
			checkboxFIRE_ANTS, checkboxCARPENTER_ANTS, checkboxMILLIPEDES,
			checkboxEAR_WIGS, checkboxPILL_BUGS, checkboxCRICKETS,
			checkboxWASPS, checkboxSUB_TERMITES, checkboxDRYWOOD_TERMITIES,
			checkboxROOF_RATS, checkboxMICE, checkboxRACOONS, checkboxOPOSSUMS,
			checkboxSKUNKS, checkboxSQUIRRELS, checkBoxTICKS, checkBoxBEDBUGS,
			checkBoxTAWNY_CRAZY_ANTS, checkBoxANTS, checkBoxOTHER_PESTS,
			checkBoxINTERIOR_PEST_ACTIVITY_ZONE, checkBoxOUTSIDE_PERIMETER,
			checkBoxDUMPSTER, checkBoxDUMPSTER_pest_activitybyzone,
			checkBoxCOMMON_AREAS, checkBoxROOF_TOPS, checkBoxDROP_CEILINGS,
			checkboxKITCHEN_AreasInspected,
			checkBoxWAREHOUSE_STORAGE_PestActivtyByZone,
			checkBoxINTERIOR_PEST_CONTROL_SERVICES,
			checkBoxTREATED_INSPECT_DROP_CEILINGS, checkBoxTREATED_KITCHEN,
			checkBoxTREATED_LAUNDRY, checkBoxTREATED_INSPECT_OFFICES,
			checkBoxTREATED_INSPECT_ROOF_TOPS, checkBoxTREATED_ZONE_MONITORS,
			checkBoxTREATED_INSPECT_COMMON_AREAS,
			checkBoxEXTERIOR_PEST_CONTROL_SERVICES,
			checkBoxTREATED_fRONT_PERIMETRE, checkBoxTREATED_BACK_PERIMETRE,
			checkBoxTREaTED_WHEPHOLES, checkBoxTREATED_CRACKS,
			checkBoxTREATED_HARBOURAGE_BREEDING_SITES,
			checkBoxTREATED_FIREANTS_LANDSCAPE,
			checkBoxTREATED_DOORWAYS_windows,
			checkBoxVISIBLE_SIGN_TERMITE_MONITORING_STATIONS,
			checkBoxINSTALLED_MONITORING_STATIONS,
			checkBoxVISIBLE_SIGN_PERIMETER_STRUCTURE,
			checkBoxVISIBLE_SIGN_INTERIOR_STRUCTURE,
			checkBoxREPLACED_MONITORING_STATIONS,
			checkBoxTREATED_PERIMETER_STRUCTURE,
			checkBoxTREATED_SLAB_PENETRATIONS,
			checkBoxTREATED_CONDUSIVE_CONDITIONS,

			checkBoxINSPECTED_RODENT_STATIONS, checkBoxCLEANES_RODENT_STATIONS,
			checkBoxINSTALLED_RODENT_BAIT_STATIONS, checkBoxREPLACED_BAIT,
			checkBoxREPLACED_BAIT_STATIONS, checkBoxINSPECTED_RODENT_TRAPS,
			checkBoxCLEANED_RODENT_TRAPS, checkBoxINSTALLED_RODENT_TRAPS,
			checkBoxREPLACED_RODENT_TRAPS, checkBoxSEALED_ENTRY_POINTS,
			checkBoxINSPECTED_SNAP_TRAPS, checkBoxINSPECTED_LIVE_CAGES,
			checkBoxREMOVED_SNAP_TRAPS, checkBoxREMOVED_LIVE_CAGES,
			checkBoxREMOVED_RODENT_BAIT_STATIONS,

			checkBoxREMOVED_RODENT_TRAPS, checkBoxSET_SNAP_TRAPS,
			checkBoxSET_LIVE_CAGES, checkBoxTopped_TANK, checkBoxLEACKY_TANK,
			checkBoxTUBING_LEACKY, checkBoxINSPECTED_NOZZLES,
			checkBoxRAN_SYATEMS, checkBoxNOZZLES_LEAKING,
			checkBoxCHECKED_RUN_DURACTION_TIME,
			checkBoxCHECKED_TIME_SYSTEM_RUNS, checkBoxDEBRIS_IN_DRAINS,
			checkBoxINACCSISIBLE_AREAS, checkBoxMOISTURE_WALLS,
			checkBoxDOCK_DOOR_LEFT_OPEN, checkBoxDEBRIS_UNDER_COOKLINE,
			checkBoxDOOR_PROPPED_OPEN, checkBoxTRASH_CANTS_NOT_EMPTIED,
			checkBoxWEATHER_STRIPPING_MISSING, checkBoxSYRUP_SODA,
			checkBoxAIR_CURTAIN, checkBoxDIRTY_DINING_TABLE,
			checkboxSET_SNAP_TRAPS1, checkboxSET_SNAP_TRAPS2,
			checkboxSET_SNAP_TRAPS3, checkboxSET_SNAP_TRAPS4,
			checkBoxFLY_LIGHTS, checkBoxFILM_BAR_TaPS, checkboxSET_LIVE_CAGES1,
			checkboxSET_LIVE_CAGES2, checkboxSET_LIVE_CAGES3,
			checkboxSET_LIVE_CAGES4;

	EditText objectFocus, edtiNSPECTEDANDtREATEDFOR1,
			edtiNSPECTEDANDtREATEDFOR2, edtINTERIOR, edtOUTSIDE_PERIMETER,
			edtDUMPSTER, edtDINING_ROOM, edtCOMMON_AREAS,
			edtKITCHEN_PestActivityByZone, edtDRY_STORAGE, edtDISH_WSHING,
			edtROOF_TOPS, edtWAIT_STATION, edtDROP_CEILINGS, edtPLANTERS,
			edtWAREHOUSE_STORAGE,

			edt_station_VISIBLE_SIGN_TERMITE_MONITORING_STATIONS,
			edtINSTALLED_MONITORING_STATIONS, edtREPLACED_MONITORING_STATIONS,
			edt_station_REPLACED_MONITORING_STATIONS,

			edt_how_many_INSPECTED_RODENT_STATIONS,
			edt_how_many_INSTALLED_RODENT_BAIT_STATIONS,
			edt_station_REPLACED_BAIT, edt_how_many_REPLACED_BAIT_STATIONS,
			edt_station_REPLACED_BAIT_STATIONS,
			edt_how_many_REPLACED_RODENT_TRAPS,
			edt_station_REPLACED_RODENT_TRAPS,
			edt_how_many_INSPECTED_SNAP_TRAPS,
			edt_how_many_INSPECTED_LIVE_CAGES, edt_how_many_REMOVED_SNAP_TRAPS,
			edt_how_many_REMOVED_LIVE_CAGES,
			edt_how_many_REMOVED_RODENT_BAIT_STATIONS,
			edt_how_many_INSTALLED_RODENT_TRAPS,
			edt_how_many_REMOVED_RODENT_TRAPS,

			edt_SET_SNAP_TRAPS, edt_SET_LIVE_CAGES, edtMosquitoService1,
			edtMosquitoService2,

			edtTechComment, edtServiceTech, edtEMP, edtCustomer, edtDate,
			edtIntValue, edtProductvalue, edtTaxvalue, edtTotal,

			edtAmount, edtCheckNo, edtDRIVING_LICENCE, edtEXPIRATION_DATE,
			edtOtherText, percent, amount;

	RadioGroup radioGroupVISIBLE_SIGN_TERMITE_MONITORING_STATIONS,
			radioGroupVISIBLE_SIGN_PERIMETER_STRUCTURE,
			radioGroupVISIBLE_SIGN_INTERIOR_STRUCTURE,
			radioGroupTREATED_PERIMETER_STRUCTURE,
			radioGroupTREATED_SLAB_PENETRATIONS,
			radioGroupTREATED_CONDUSIVE_CONDITIONS,

			radioGroupCLEANES_RODENT_STATIONS,
			radioGroupINSTALLED_RODENT_BAIT_STATIONS, radioGroupREPLACED_BAIT,
			radioGroupREPLACED_BAIT_STATIONS, radioGroupINSPECTED_RODENT_TRAPS,
			radioGroupCLEANED_RODENT_TRAPS, radioGroupINSTALLED_RODENT_TRAPS,
			radioGroupSEALED_ENTRY_POINTS, radioGroupInvoice,
			radioGroupREPLACED_RODENT_TRAPS;
	RadioButton radiobtnVISIBLE_SIGN_TERMITE_MONITORING_STATIONS1,
			radiobtnVISIBLE_SIGN_TERMITE_MONITORING_STATIONS2,
			radiobtnVISIBLE_SIGN_PERIMETER_STRUCTURE1,
			radiobtnVISIBLE_SIGN_PERIMETER_STRUCTURE2,
			radiobtnVISIBLE_SIGN_INTERIOR_STRUCTURE1,
			radiobtnVISIBLE_SIGN_INTERIOR_STRUCTURE2,
			radiobtnTREATED_PERIMETER_STRUCTURE1,
			radiobtnTREATED_PERIMETER_STRUCTURE2,
			radiobtnTREATED_SLAB_PENETRATIONS1,
			radiobtnTREATED_SLAB_PENETRATIONS2,
			radiobtnTREATED_CONDUSIVE_CONDITIONS1,
			radiobtnTREATED_CONDUSIVE_CONDITIONS2,

			radiobtnREPLACED_RODENT_TRAPS1, radiobtnREPLACED_RODENT_TRAPS2,
			radiobtnCLEANES_RODENT_STATIONS1, radiobtnCLEANES_RODENT_STATIONS2,
			radiobtnINSTALLED_RODENT_BAIT_STATIONS1,
			radiobtnINSTALLED_RODENT_BAIT_STATIONS2, radiobtnREPLACED_BAIT1,
			radiobtnREPLACED_BAIT2, radiobtnREPLACED_BAIT_STATIONS1,
			radiobtnREPLACED_BAIT_STATIONS2, radiobtnINSPECTED_RODENT_TRAPS1,
			radiobtnINSPECTED_RODENT_TRAPS2, radiobtnCLEANED_RODENT_TRAPS1,
			radiobtnCLEANED_RODENT_TRAPS2, radiobtnINSTALLED_RODENT_TRAPS1,
			radiobtnINSTALLED_RODENT_TRAPS2, radiobtnSEALED_ENTRY_POINTS1,
			radiobtnSEALED_ENTRY_POINTS2,

			radiobtnSET_LIVE_CAGES1, radiobtnSET_LIVE_CAGES2,
			radiobtnSET_LIVE_CAGES3, radiobtnSET_LIVE_CAGES4, radioCash,
			radioCheck, radioCreditcard, radioBillLater, radioPreBill,
			radioNocharge;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.hc_servicereport);
		Thread.setDefaultUncaughtExceptionHandler(new TopExceptionHandler(this));
		
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setDisplayShowHomeEnabled(true);

		initialize();
		generateChemicaltable();
		setPreviousFilledData();
		checkChange = 1;
		if (generalInfoDTO.getServiceStatus().equals("Complete")) {
			setViewForComplete();
		}
		super.onCreate(savedInstanceState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.residential_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		/*
		 * if (id == R.id.action_back) {
		 * 
		 * 
		 * if (SystemClock.elapsedRealtime() - mLastClickTime < 4000) { return
		 * true; } mLastClickTime = SystemClock.elapsedRealtime(); Intent
		 * intent=new Intent(ResidentialPestControlServiceReport.this,
		 * GeneralInformationActivity.class); //Log.e("",
		 * ""+serviceList.get(arg2));
		 * intent.putExtra("serviceIdNew",serviceID_new);
		 * intent.putExtra("userDTO", userDTO); startActivity(intent); finish();
		 * return true; }
		 */
		if (id == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}

	private void setViewForComplete() {
		// TODO Auto-generated method stub
		for (int i = 0; i < chemicalsComponentList.size(); i++) {
			ChemicalsComponentDTO chemicalsComponentDTO = chemicalsComponentList
					.get(i);
			chemicalsComponentDTO.getAmountEditText().setEnabled(false);
			chemicalsComponentDTO.getChemicalsCheckBox().setEnabled(false);
			chemicalsComponentDTO.getPersentSpinner().setEnabled(false);
			chemicalsComponentDTO.getTargetSpinner().setEnabled(false);
			chemicalsComponentDTO.getUnitSpinner().setEnabled(false);
		}

		checkboxCommercialPcService.setEnabled(false);
		checkboxCommercialX.setEnabled(false);
		checkboxCommercialPcInitial.setEnabled(false);
		checkboxCommercialTermite.setEnabled(false);
		checkboxOFFICES.setEnabled(false);
		checkboxBREAKROOMS.setEnabled(false);
		checkboxRODENT_BAIT_STATIONS.setEnabled(false);
		checkboxLOG_BOOK.setEnabled(false);
		checkboxFOOD_CHARTS.setEnabled(false);
		checkboxPERIMETER.setEnabled(false);
		checkboxCOFFEE_BARS.setEnabled(false);
		checkboxDiningRoom_AreasInspected.setEnabled(false);
		checkboxLANDSCAPE_BEDS.setEnabled(false);
		checkboxLAUNDRY.setEnabled(false);
		checkboxHOUSE_KEEPING.setEnabled(false);
		checkboxMANHOLE_COVERS.setEnabled(false);
		checkboxWAIT_STATIONS_AREAS_INSPECTED.setEnabled(false);
		checkBoxWAIT_STATION_PEST_ACTIVTY.setEnabled(false);
		checkboxFLY_LIHGTS_AREAS_INSPECTED.setEnabled(false);
		checkboxGREASE_TRAPS.setEnabled(false);
		checkboxELEVATOR_PITS.setEnabled(false);
		checkboxMECHANICAL_ROOMS.setEnabled(false);

		checkboxROOF_TOPS_pestActivity_by_zone.setEnabled(false);
		checkboxPATIENT_ROOMS.setEnabled(false);
		checkboxNURSES_STATIONS.setEnabled(false);
		checkboxREST_ROOMS.setEnabled(false);
		checkboxPLANTERS_AreasInspected.setEnabled(false);
		checkboxDOCK.setEnabled(false);
		checkBoxKITCHEN_Pest_Activity.setEnabled(false);
		checkBoxDRY_STORAGE_Pest_Activity.setEnabled(false);
		checkboxDISH_WSHING_AREAS_INSPECTED.setEnabled(false);
		checkBoxDUMPSTER_pest_activitybyzone.setEnabled(false);
		checkboxENTRY_POINTS.setEnabled(false);
		checkboxBAR.setEnabled(false);
		checkboxLOBBY.setEnabled(false);
		checkBoxDINING_ROOM_PestActivityByZone.setEnabled(false);
		checkBoxWAREHOUSE_STORAGE_PestActivtyByZone.setEnabled(false);
		checkboxWarehouseStorage_AreasInspected.setEnabled(false);
		checkboxRoaches.setEnabled(false);
		checkboxAMERICAN_COCKROACHES.setEnabled(false);
		checkboxGERMAN_COCKROACHES.setEnabled(false);
		checkboxSPIDERS.setEnabled(false);
		checkboxSCORPIONS.setEnabled(false);
		checkboxSILVER_FISH.setEnabled(false);
		checkboxSTORED_PRODUCT_PESTS.setEnabled(false);
		checkboxPHARAOH_ANTS.setEnabled(false);
		checkboxFIRE_ANTS.setEnabled(false);
		checkboxCARPENTER_ANTS.setEnabled(false);
		checkboxMILLIPEDES.setEnabled(false);
		checkboxEAR_WIGS.setEnabled(false);
		checkboxPILL_BUGS.setEnabled(false);
		checkboxCRICKETS.setEnabled(false);
		checkboxWASPS.setEnabled(false);
		checkboxSUB_TERMITES.setEnabled(false);
		checkboxDRYWOOD_TERMITIES.setEnabled(false);
		checkboxROOF_RATS.setEnabled(false);
		checkboxMICE.setEnabled(false);
		checkboxRACOONS.setEnabled(false);
		checkboxOPOSSUMS.setEnabled(false);
		checkboxSKUNKS.setEnabled(false);
		checkboxSQUIRRELS.setEnabled(false);
		checkBoxTICKS.setEnabled(false);
		checkBoxBEDBUGS.setEnabled(false);
		checkBoxTAWNY_CRAZY_ANTS.setEnabled(false);
		checkBoxANTS.setEnabled(false);
		checkBoxOTHER_PESTS.setEnabled(false);
		checkBoxINTERIOR_PEST_ACTIVITY_ZONE.setEnabled(false);
		checkBoxOUTSIDE_PERIMETER.setEnabled(false);
		checkBoxDUMPSTER.setEnabled(false);

		checkBoxCOMMON_AREAS.setEnabled(false);
		checkboxKITCHEN_AreasInspected.setEnabled(false);
		checkboxDRY_STORAGE.setEnabled(false);
		checkBoxDISH_WSHING_PEST_ACTIVTY.setEnabled(false);
		checkBoxROOF_TOPS.setEnabled(false);

		checkBoxDROP_CEILINGS.setEnabled(false);
		checkBoxPLANTERS_PestActivtyByZone.setEnabled(false);
		checkBoxWAREHOUSE_STORAGE_PestActivtyByZone.setEnabled(false);
		checkBoxINTERIOR_PEST_CONTROL_SERVICES.setEnabled(false);
		checkBoxTREATED_INSPECT_DROP_CEILINGS.setEnabled(false);
		checkBoxTREATED_KITCHEN.setEnabled(false);
		checkBoxTREATED_LAUNDRY.setEnabled(false);

		checkBoxTREATED_INSPECT_OFFICES.setEnabled(false);
		checkBoxTREATED_INSPECT_ROOF_TOPS.setEnabled(false);
		checkBoxTREATED_ZONE_MONITORS.setEnabled(false);
		checkBoxTREATED_INSPECT_COMMON_AREAS.setEnabled(false);
		checkBoxEXTERIOR_PEST_CONTROL_SERVICES.setEnabled(false);
		checkBoxTREATED_fRONT_PERIMETRE.setEnabled(false);
		checkBoxTREATED_BACK_PERIMETRE.setEnabled(false);
		checkBoxTREaTED_WHEPHOLES.setEnabled(false);
		checkBoxTREATED_CRACKS.setEnabled(false);
		checkBoxTREATED_HARBOURAGE_BREEDING_SITES.setEnabled(false);
		checkBoxTREATED_FIREANTS_LANDSCAPE.setEnabled(false);
		checkBoxTREATED_DOORWAYS_windows.setEnabled(false);
		checkBoxVISIBLE_SIGN_TERMITE_MONITORING_STATIONS.setEnabled(false);
		checkBoxINSTALLED_MONITORING_STATIONS.setEnabled(false);
		checkBoxVISIBLE_SIGN_PERIMETER_STRUCTURE.setEnabled(false);
		checkBoxVISIBLE_SIGN_INTERIOR_STRUCTURE.setEnabled(false);
		checkBoxREPLACED_MONITORING_STATIONS.setEnabled(false);
		checkBoxTREATED_PERIMETER_STRUCTURE.setEnabled(false);
		checkBoxTREATED_SLAB_PENETRATIONS.setEnabled(false);
		checkBoxTREATED_CONDUSIVE_CONDITIONS.setEnabled(false);

		checkBoxINSPECTED_RODENT_STATIONS.setEnabled(false);
		checkBoxCLEANES_RODENT_STATIONS.setEnabled(false);
		checkBoxINSTALLED_RODENT_BAIT_STATIONS.setEnabled(false);
		checkBoxREPLACED_BAIT.setEnabled(false);
		checkBoxREPLACED_BAIT_STATIONS.setEnabled(false);
		checkBoxINSPECTED_RODENT_TRAPS.setEnabled(false);
		checkBoxCLEANED_RODENT_TRAPS.setEnabled(false);
		checkBoxINSTALLED_RODENT_TRAPS.setEnabled(false);
		checkBoxREPLACED_RODENT_TRAPS.setEnabled(false);
		checkBoxSEALED_ENTRY_POINTS.setEnabled(false);
		checkBoxINSPECTED_SNAP_TRAPS.setEnabled(false);
		checkBoxINSPECTED_LIVE_CAGES.setEnabled(false);
		checkBoxREMOVED_SNAP_TRAPS.setEnabled(false);
		checkBoxREMOVED_LIVE_CAGES.setEnabled(false);
		checkBoxREMOVED_RODENT_BAIT_STATIONS.setEnabled(false);

		checkBoxREMOVED_RODENT_TRAPS.setEnabled(false);
		checkBoxSET_SNAP_TRAPS.setEnabled(false);
		checkBoxSET_LIVE_CAGES.setEnabled(false);
		checkBoxTopped_TANK.setEnabled(false);
		checkBoxLEACKY_TANK.setEnabled(false);
		checkBoxTUBING_LEACKY.setEnabled(false);
		checkBoxINSPECTED_NOZZLES.setEnabled(false);
		checkBoxRAN_SYATEMS.setEnabled(false);
		checkBoxNOZZLES_LEAKING.setEnabled(false);
		checkBoxCHECKED_RUN_DURACTION_TIME.setEnabled(false);
		checkBoxCHECKED_TIME_SYSTEM_RUNS.setEnabled(false);
		checkBoxDEBRIS_IN_DRAINS.setEnabled(false);
		checkBoxINACCSISIBLE_AREAS.setEnabled(false);
		checkBoxMOISTURE_WALLS.setEnabled(false);
		checkBoxDOCK_DOOR_LEFT_OPEN.setEnabled(false);
		checkBoxDEBRIS_UNDER_COOKLINE.setEnabled(false);
		checkBoxDOOR_PROPPED_OPEN.setEnabled(false);
		checkBoxTRASH_CANTS_NOT_EMPTIED.setEnabled(false);
		checkBoxWEATHER_STRIPPING_MISSING.setEnabled(false);
		checkBoxSYRUP_SODA.setEnabled(false);
		checkBoxAIR_CURTAIN.setEnabled(false);
		checkBoxDIRTY_DINING_TABLE.setEnabled(false);
		checkboxSET_SNAP_TRAPS1.setEnabled(false);
		checkboxSET_SNAP_TRAPS2.setEnabled(false);
		checkboxSET_SNAP_TRAPS3.setEnabled(false);
		checkboxSET_SNAP_TRAPS4.setEnabled(false);
		checkBoxFLY_LIGHTS.setEnabled(false);
		checkBoxFILM_BAR_TaPS.setEnabled(false);
		checkboxSET_LIVE_CAGES1.setEnabled(false);
		checkboxSET_LIVE_CAGES2.setEnabled(false);
		checkboxSET_LIVE_CAGES3.setEnabled(false);
		checkboxSET_LIVE_CAGES4.setEnabled(false);

		edtiNSPECTEDANDtREATEDFOR1.setEnabled(false);
		edtiNSPECTEDANDtREATEDFOR2.setEnabled(false);
		edtINTERIOR.setEnabled(false);
		edtOUTSIDE_PERIMETER.setEnabled(false);
		edtDUMPSTER.setEnabled(false);
		edtDINING_ROOM.setEnabled(false);
		edtCOMMON_AREAS.setEnabled(false);
		edtKITCHEN_PestActivityByZone.setEnabled(false);
		edtDRY_STORAGE.setEnabled(false);
		edtDISH_WSHING.setEnabled(false);
		edtROOF_TOPS.setEnabled(false);
		edtWAIT_STATION.setEnabled(false);
		edtDROP_CEILINGS.setEnabled(false);
		edtPLANTERS.setEnabled(false);
		edtWAREHOUSE_STORAGE.setEnabled(false);
		edt_station_VISIBLE_SIGN_TERMITE_MONITORING_STATIONS.setEnabled(false);
		edtINSTALLED_MONITORING_STATIONS.setEnabled(false);
		edtREPLACED_MONITORING_STATIONS.setEnabled(false);
		edt_station_REPLACED_MONITORING_STATIONS.setEnabled(false);
		edt_how_many_INSPECTED_RODENT_STATIONS.setEnabled(false);
		edt_how_many_INSTALLED_RODENT_BAIT_STATIONS.setEnabled(false);
		edt_station_REPLACED_BAIT.setEnabled(false);
		edt_how_many_REPLACED_BAIT_STATIONS.setEnabled(false);
		edt_station_REPLACED_BAIT_STATIONS.setEnabled(false);
		edt_how_many_REPLACED_RODENT_TRAPS.setEnabled(false);
		edt_station_REPLACED_RODENT_TRAPS.setEnabled(false);
		edt_how_many_INSPECTED_SNAP_TRAPS.setEnabled(false);
		edt_how_many_INSPECTED_LIVE_CAGES.setEnabled(false);
		edt_how_many_REMOVED_SNAP_TRAPS.setEnabled(false);
		edt_how_many_REMOVED_LIVE_CAGES.setEnabled(false);
		edt_how_many_REMOVED_RODENT_BAIT_STATIONS.setEnabled(false);
		edt_how_many_INSTALLED_RODENT_TRAPS.setEnabled(false);
		edt_SET_SNAP_TRAPS.setEnabled(false);
		edt_SET_LIVE_CAGES.setEnabled(false);
		edtMosquitoService1.setEnabled(false);
		edtMosquitoService2.setEnabled(false);
		edtTechComment.setEnabled(false);
		edtServiceTech.setEnabled(false);
		edtEMP.setEnabled(false);
		edtCustomer.setEnabled(false);
		edtDate.setEnabled(false);
		edtIntValue.setEnabled(false);
		edtProductvalue.setEnabled(false);
		edtTaxvalue.setEnabled(false);
		edtTotal.setEnabled(false);
		edtAmount.setEnabled(false);
		edtCheckNo.setEnabled(false);
		edtDRIVING_LICENCE.setEnabled(false);
		edtEXPIRATION_DATE.setEnabled(false);

		btnCHECK_FRONT_IMAGE.setEnabled(false);
		btnCHECK_BACK_IMAGE.setEnabled(false);
		// btnsaveandcontinueINVOICE.setEnabled(false);
		// btnCancel.setEnabled(false);

		radioCash.setEnabled(false);
		radioCheck.setEnabled(false);
		radioCreditcard.setEnabled(false);
		radioBillLater.setEnabled(false);
		radioPreBill.setEnabled(false);
		radioNocharge.setEnabled(false);

		radiobtnVISIBLE_SIGN_TERMITE_MONITORING_STATIONS1.setEnabled(false);
		radiobtnVISIBLE_SIGN_TERMITE_MONITORING_STATIONS2.setEnabled(false);

		radiobtnVISIBLE_SIGN_PERIMETER_STRUCTURE1.setEnabled(false);
		radiobtnVISIBLE_SIGN_PERIMETER_STRUCTURE2.setEnabled(false);

		radiobtnVISIBLE_SIGN_INTERIOR_STRUCTURE1.setEnabled(false);
		radiobtnVISIBLE_SIGN_INTERIOR_STRUCTURE2.setEnabled(false);

		radiobtnTREATED_PERIMETER_STRUCTURE1.setEnabled(false);
		radiobtnTREATED_PERIMETER_STRUCTURE2.setEnabled(false);

		radiobtnTREATED_SLAB_PENETRATIONS1.setEnabled(false);
		radiobtnTREATED_SLAB_PENETRATIONS2.setEnabled(false);

		radiobtnTREATED_CONDUSIVE_CONDITIONS1.setEnabled(false);
		radiobtnTREATED_CONDUSIVE_CONDITIONS2.setEnabled(false);

		radiobtnCLEANES_RODENT_STATIONS1.setEnabled(false);
		radiobtnCLEANES_RODENT_STATIONS2.setEnabled(false);

		radiobtnINSTALLED_RODENT_BAIT_STATIONS1.setEnabled(false);
		radiobtnINSTALLED_RODENT_BAIT_STATIONS2.setEnabled(false);

		radiobtnREPLACED_BAIT1.setEnabled(false);
		radiobtnREPLACED_BAIT2.setEnabled(false);

		radiobtnREPLACED_BAIT_STATIONS1.setEnabled(false);
		radiobtnREPLACED_BAIT_STATIONS2.setEnabled(false);

		radiobtnINSPECTED_RODENT_TRAPS1.setEnabled(false);
		radiobtnINSPECTED_RODENT_TRAPS2.setEnabled(false);

		radiobtnCLEANED_RODENT_TRAPS1.setEnabled(false);
		radiobtnCLEANED_RODENT_TRAPS2.setEnabled(false);

		radiobtnINSTALLED_RODENT_TRAPS1.setEnabled(false);
		radiobtnINSTALLED_RODENT_TRAPS2.setEnabled(false);

		radiobtnREPLACED_RODENT_TRAPS1.setEnabled(false);
		radiobtnREPLACED_RODENT_TRAPS2.setEnabled(false);

		radiobtnSEALED_ENTRY_POINTS1.setEnabled(false);
		radiobtnSEALED_ENTRY_POINTS2.setEnabled(false);
	}

	private void generateChemicaltable() {
		// TODO Auto-generated method stub

		JSONArray arrayRecords = houstonFlowFunctions.getChemicalsWhenNoRecord(
				DatabaseHelper.TABLE_CHEMICALS_WHEN_NO_RECORD, "Commercial");
		 LoggerUtil.e("TABLE_CHEMICALS_WHEN_NO_RECORD", "" + arrayRecords);

		listChemicals = new Gson().fromJson(arrayRecords.toString(),
				new TypeToken<List<ChemicalDto>>() {
				}.getType());

		ArrayList<ChemicalDto> chemicalListSelected = new ArrayList<ChemicalDto>();

		for (int i = 0; i < listChemicals.size(); i++) {
			ChemicalDto chemicalMasterDTO = listChemicals.get(i);
			ChemicalDto chemicalSelectedDTO = houstonFlowFunctions
					.getchemicalAlreadyAdded("Commercial", serviceID_new,
							chemicalMasterDTO.getPreventationID(),
							DatabaseHelper.TABLE_CHEMICALS);
			if (chemicalSelectedDTO != null) {

				 LoggerUtil.e("chemicalSelectedDTO", "" +
				 chemicalSelectedDTO.toString());
				chemicalListSelected.add(chemicalSelectedDTO);
			} else {

				 LoggerUtil.e("chemicalMasterDTO", "" +
				 chemicalMasterDTO.toString());
				chemicalListSelected.add(chemicalMasterDTO);
			}

		}
		listChemicals.clear();
		listChemicals.addAll(chemicalListSelected);
		for (int i = 0; i < listChemicals.size(); i++) {
			AddChemicals(i);
		}
	}

	int i = 0;
	TableLayout tb;
	TableRow deleteTableRow;
	int numberofaudit;
	TableRow tr, tr1;
	ArrayList<TableRow> trlist = new ArrayList<TableRow>();

	public void AddChemicals(final int listindex) {
		numberofaudit++;
		// LoggerUtil.e("serviceID_new", serviceID_new);
		if (i == 0) {
			tb = new TableLayout(HC_PestControlServiceReport.this);
			// addHeader();
			mainLayout.addView(tb);
		}
		i = i + 1;
		tr = new TableRow(HC_PestControlServiceReport.this);
		trlist.add(tr);
		tr1 = new TableRow(HC_PestControlServiceReport.this);
		trlist.add(tr1);
		// if(i%2!=0)
		tr.setBackgroundColor(Color.parseColor("#EDEDEF"));
		tr.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT));
		tr1.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT));

		LayoutParams params1 = new TableRow.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.FILL_PARENT, 1f);
		final LinearLayout MainLayout = new LinearLayout(
				getApplicationContext());
		// LayoutParams params1 = new
		// TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
		// LayoutParams.FILL_PARENT,1f);
		MainLayout.setOrientation(LinearLayout.HORIZONTAL);
		MainLayout.setLayoutParams(params1);
		MainLayout.setGravity(Gravity.CENTER_VERTICAL);
		// MainLayout.setVisibility(View.GONE);

		final LinearLayout belowLayout = new LinearLayout(
				getApplicationContext());
		// LayoutParams params1 = new
		// TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
		// LayoutParams.FILL_PARENT,1f);
		// ((MarginLayoutParams) params1).setMargins(5, 0, 5, 0);
		belowLayout.setOrientation(LinearLayout.VERTICAL);
		belowLayout.setLayoutParams(params1);
		belowLayout.setGravity(Gravity.CENTER_VERTICAL);
		belowLayout.setVisibility(View.GONE);

		chemicalcheckBox = new CheckBox(HC_PestControlServiceReport.this);
		checkList.add(listindex, chemicalcheckBox);
		chemicalcheckBox.setTag(Integer.valueOf(i));
		chemicalcheckBox.setText("");
		chemicalcheckBox.setId(i + 10000);
		// chemicalcheckBox.setBackgroundResource(R.drawable.checked);
		chemicalcheckBox.setGravity(Gravity.CENTER_VERTICAL);

		// LoggerUtil.e("con",
		// "contains or not"+listPreviousSelectedChemicals.contains(listChemicals.get(listindex).getPestPrevention_ID()+".."+listChemicals.get(listindex).getPestPrevention_ID()));

		if (listChemicals.get(listindex).getProductCheck().equals("true")) {
			belowLayout.setVisibility(View.VISIBLE);
			checkList.get(listindex).setChecked(true);
		} else {
			checkList.get(listindex).setChecked(false);
		}

		LayoutParams params0 = new TableRow.LayoutParams(
				android.widget.LinearLayout.LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		final ChemicalDto chemicalDto = new ChemicalDto();
		chemicalcheckBox.setLayoutParams(params0);
		MainLayout.addView(chemicalcheckBox);

		chemicalcheckBox
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// TODO Auto-generated method stub
						if (isChecked) {
							belowLayout.setVisibility(View.VISIBLE);

						} else {
							belowLayout.setVisibility(View.GONE);
						}
					}
				});

		LinearLayout productLayout = new LinearLayout(getApplicationContext());
		productLayout.setOrientation(LinearLayout.HORIZONTAL);
		productLayout.setLayoutParams(params1);
		productLayout.setGravity(Gravity.CENTER_VERTICAL);
		String productName = listChemicals.get(listindex).getProduct();
		product = new TextView(getApplicationContext());
		product.setText(productName);
		product.setTextColor(Color.BLACK);

		LayoutParams params2 = new TableRow.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT, .3f);

		productLayout.addView(product);
		if (productName.equals("Other")) {
			// LoggerUtil.e("edtOtherText", "edtOtherText");
			edtOtherText = new EditText(HC_PestControlServiceReport.this);
			edtOtherText.setId(i + 3000);
			edtOtherText.setVisibility(View.VISIBLE);
			edtOtherText.setSingleLine();
			edtOtherText.setCursorVisible(true);
			edtOtherText.setFilters(CommonFunction.specialchars());

			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.FILL_PARENT,
					LinearLayout.LayoutParams.FILL_PARENT);
			lp.setMargins(3, 0, 3, 0);
			edtOtherText.setLayoutParams(lp);

			edtOtherText.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
			edtOtherText
					.setBackgroundResource(android.R.drawable.editbox_background_normal);
			final float scale = this.getResources().getDisplayMetrics().density;
			edtOtherText.setPadding(edtOtherText.getPaddingLeft()
					+ (int) (10.0f * scale + 0.5f), 5, 5, 5);
			edtOtherText.setText(listChemicals.get(listindex).getOtherText());
			if (listChemicals.get(listindex).getProductCheck().equals("true")) {
				edtOtherText.setText(listChemicals.get(listindex)
						.getOtherText());
				listChemicals.get(listindex).setOtherText(
						edtOtherText.getText().toString());
			} else {
				edtOtherText.setText(listChemicals.get(listindex)
						.getOtherText());
				listChemicals.get(listindex).setOtherText(
						edtOtherText.getText().toString());
			}
			edtOtherText.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					listChemicals.get(listindex).setOtherText(
							s.toString().trim());
					chemicalDto.setOtherText(listChemicals.get(listindex)
							.getOtherText());
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

					// listChemicals.get(listindex).setOtherText(s.toString().trim());
				}
			});
			productLayout.addView(edtOtherText);
		}

		MainLayout.addView(productLayout);
		tr.addView(MainLayout);

		final LinearLayout PercentLayout = new LinearLayout(
				getApplicationContext());
		// LayoutParams params1 = new
		// TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
		// LayoutParams.FILL_PARENT,1f);
		PercentLayout.setOrientation(LinearLayout.HORIZONTAL);
		PercentLayout.setLayoutParams(params1);
		PercentLayout.setGravity(Gravity.CENTER_VERTICAL);

		PercentHead = new TextView(getApplicationContext());
		PercentHead.setText("Percent");
		PercentHead.setTextColor(Color.BLACK);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		lp.setMargins(5, 0, 5, 0);
		PercentHead.setLayoutParams(lp);
		PercentLayout.addView(PercentHead);

		percent = new EditText(HC_PestControlServiceReport.this);
		percent.setId(i + 3000);
		percent.setMaxWidth(60);
		percent.setSingleLine();
		percent.setGravity(Gravity.LEFT);
		percent.setCursorVisible(true);
		percent.setFilters(CommonFunction.limitchars(6));
		percent.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
		percent.setInputType(InputType.TYPE_CLASS_NUMBER
				| InputType.TYPE_NUMBER_FLAG_DECIMAL);
		percent.setBackgroundResource(android.R.drawable.editbox_background_normal);
		if (listChemicals.get(listindex).getProductCheck().equals("true")) {

			percent.setText(listChemicals.get(listindex).getPercentPes());
			listChemicals.get(listindex).setPercentPes(
					percent.getText().toString());

		} else {
			percent.setText(listChemicals.get(listindex).getPercentPes());
			listChemicals.get(listindex).setPercentPes(
					percent.getText().toString());

		}
		percent.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				listChemicals.get(listindex).setPercentPes(s.toString().trim());
				chemicalDto.setPercentPes(listChemicals.get(listindex)
						.getPercentPes());
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

				listChemicals.get(listindex).setPercentPes(s.toString().trim());
			}
		});
		// amount.requestFocus();
		// availroom.setPadding(5, 0, 5, 0);
		percent.setTextColor(Color.BLACK);
		percent.setLayoutParams(params2);
		PercentLayout.addView(percent);

		belowLayout.addView(PercentLayout);

		final LinearLayout AmountLayout = new LinearLayout(
				getApplicationContext());
		// LayoutParams params1 = new
		// TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
		// LayoutParams.FILL_PARENT,1f);
		AmountLayout.setOrientation(LinearLayout.HORIZONTAL);
		AmountLayout.setLayoutParams(params1);
		AmountLayout.setGravity(Gravity.CENTER_VERTICAL);

		AmountHead = new TextView(getApplicationContext());
		AmountHead.setText("Amount");
		AmountHead.setTextColor(Color.BLACK);
		LinearLayout.LayoutParams lpAmt = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		lpAmt.setMargins(5, 0, 3, 0);
		AmountHead.setLayoutParams(lpAmt);
		AmountLayout.addView(AmountHead);

		amount = new EditText(HC_PestControlServiceReport.this);
		amount.setId(i + 3000);
		amount.setMaxWidth(60);
		amount.setSingleLine();
		amount.setGravity(Gravity.LEFT);
		amount.setCursorVisible(true);
		amount.setFilters(CommonFunction.limitchars(8));
		amount.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
		amount.setInputType(InputType.TYPE_CLASS_NUMBER
				| InputType.TYPE_NUMBER_FLAG_DECIMAL);
		amount.setBackgroundResource(android.R.drawable.editbox_background_normal);

		// availroom.setPadding(5, 0, 5, 0);
		if (listChemicals.get(listindex).getProductCheck().equals("true")) {
			amount.setText(listChemicals.get(listindex).getAmount());
			listChemicals.get(listindex).setAmount(amount.getText().toString());
		} else {
			amount.setText(listChemicals.get(listindex).getAmount());
			listChemicals.get(listindex).setAmount(amount.getText().toString());
		}
		amount.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

				listChemicals.get(listindex).setAmount(s.toString().trim());
				chemicalDto.setAmount(listChemicals.get(listindex).getAmount());
			}
		});
		amount.setTextColor(Color.BLACK);
		// amount.setText(preventionDTO.getAmount());
		amount.setLayoutParams(params2);
		AmountLayout.addView(amount);
		belowLayout.addView(AmountLayout);

		final LinearLayout UnitLayout = new LinearLayout(
				getApplicationContext());
		// LayoutParams params1 = new
		// TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
		// LayoutParams.FILL_PARENT,1f);
		UnitLayout.setOrientation(LinearLayout.HORIZONTAL);
		UnitLayout.setLayoutParams(params1);
		UnitLayout.setGravity(Gravity.CENTER_VERTICAL);
		UnitLayout.setVisibility(View.GONE);

		UnitHead = new TextView(getApplicationContext());
		UnitHead.setText("Unit   ");
		UnitHead.setTextColor(Color.BLACK);
		LinearLayout.LayoutParams lpUnit = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		lpUnit.setMargins(5, 0, 10, 0);
		UnitHead.setLayoutParams(lpUnit);
		PercentLayout.addView(UnitHead);
		String[] units = getResources().getStringArray(R.array.units);

		final ArrayList<String> UNIT = new ArrayList<String>(
				Arrays.asList(units));
		ArrayAdapter<String> unitAdapter = new ArrayAdapter<String>(
				HC_PestControlServiceReport.this,
				android.R.layout.simple_spinner_item, UNIT);
		final Spinner spnUNIT = new Spinner(
				HC_PestControlServiceReport.this);
		spnUNIT.setAdapter(unitAdapter);
		spnUNIT.setPrompt("Select ");
		spnUNIT.setLayoutParams(params2);
		spnUNIT.setEnabled(true);
		// LoggerUtil.e("listPreviousPestId.contains...","listPreviousPestId.contains..."+listPreviousPestId.contains(listChemicals.get(listindex).getPestPrevention_ID()));
		// LoggerUtil.e("listPreviousPestId.indexOf...","listPreviousPestId.contains..."+listPreviousPestId.indexOf(listChemicals.get(listindex).getPestPrevention_ID()));

		if (listChemicals.get(listindex).getProductCheck().equals("true")) {
			int index = Arrays.asList(units).indexOf(
					listChemicals.get(listindex).getUnit());
			spnUNIT.setSelection(index);
			listChemicals.get(listindex).setUnit(
					spnUNIT.getSelectedItem().toString());
		}

		spnUNIT.setOnItemSelectedListener(new OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				listChemicals.get(listindex).setUnit(
						spnUNIT.getSelectedItem().toString());
			}

			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		PercentLayout.addView(spnUNIT);
		belowLayout.addView(UnitLayout);

		final LinearLayout TargetLayout = new LinearLayout(
				getApplicationContext());
		// LayoutParams params1 = new
		// TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
		// LayoutParams.FILL_PARENT,1f);
		TargetLayout.setOrientation(LinearLayout.HORIZONTAL);
		TargetLayout.setLayoutParams(params1);
		TargetLayout.setGravity(Gravity.CENTER_VERTICAL);

		TargetHead = new TextView(getApplicationContext());
		TargetHead.setText("Target ");
		TargetHead.setTextColor(Color.BLACK);
		LinearLayout.LayoutParams lpTarget = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		lpTarget.setMargins(5, 0, 8, 0);
		TargetHead.setLayoutParams(lpTarget);
		TargetLayout.addView(TargetHead);

		final EditText target = new EditText(
				HC_PestControlServiceReport.this);
		target.setId(i + 3000);
		target.setSingleLine(false);
		target.setMaxWidth(60);
		// target.setKeyListener(DigitsKeyListener.getInstance("a bcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"));
		target.setInputType(InputType.TYPE_CLASS_TEXT);
		target.setGravity(Gravity.LEFT);
		target.setCursorVisible(true);
		target.setFilters(CommonFunction.specialcharsForTarget());

		// target.setFilters(CommonFunction.limitchars(20));
		target.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
		if (listChemicals.get(listindex).getProductCheck().equals("true")) {
			target.setText(listChemicals.get(listindex).getTarget());
			listChemicals.get(listindex).setTarget(target.getText().toString());
		} else {
			target.setText(listChemicals.get(listindex).getTarget());
			listChemicals.get(listindex).setTarget(target.getText().toString());
		}
		target.setBackgroundResource(android.R.drawable.editbox_background_normal);
		target.setTextColor(Color.BLACK);

		target.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.length() >= 55) {
					s = s.subSequence(0, s.length() - 1);
					target.setText(s.toString());
					target.setSelection(s.length(), s.length());
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

				listChemicals.get(listindex).setTarget(s.toString().trim());
				listChemicals.get(listindex).setTarget(
						target.getText().toString());
			}
		});
		target.setLayoutParams(params2);
		TargetLayout.addView(target);
		belowLayout.addView(TargetLayout);

		tr1.addView(belowLayout);

		tb.addView(tr);
		tb.addView(tr1);

		ChemicalsComponentDTO chemicalsComponentDTO = new ChemicalsComponentDTO();
		chemicalsComponentDTO.setAmountEditText(amount);
		chemicalsComponentDTO.setChemicalsCheckBox(chemicalcheckBox);
		chemicalsComponentDTO.setPersentSpinner(percent);
		chemicalsComponentDTO.setProductTextView(product);
		chemicalsComponentDTO.setTargetSpinner(target);
		chemicalsComponentDTO.setUnitSpinner(spnUNIT);
		chemicalsComponentDTO.setOtherEditText(edtOtherText);

		chemicalsComponentList.add(chemicalsComponentDTO);
	}

	public void addHeader() {
		TableRow tr = new TableRow(getApplicationContext());
		if (i % 2 != 0)

			tr.setBackgroundColor(Color.parseColor("#2F80B7"));
		// tr.setOrientation(LinearLayout.HORIZONTAL);
		tr.setBackgroundColor(Color.parseColor("#2F80B7"));
		tr.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.FILL_PARENT));
		// tr.setWeightSum(1);
		tr.setPadding(0, 10, 0, 10);
		LayoutParams params1 = new TableRow.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.FILL_PARENT, 1f);
		LayoutParams params2 = new TableRow.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT, .3f);
		TextView products = new TextView(getApplicationContext());
		/*
		 * products.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL)
		 * ; //products.setVisibility(View.INVISIBLE);
		 * products.setText("Product"); products.setTextColor(Color.WHITE);
		 * LayoutParams paramss1 = new
		 * TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
		 * LayoutParams.FILL_PARENT,1f); products.setLayoutParams(params2);
		 * tr.addView(products);
		 */

		TextView product = new TextView(getApplicationContext());
		product.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
		product.setText(getApplicationContext().getResources().getString(
				R.string.PRODUCT));
		product.setTextColor(Color.WHITE);
		product.setLayoutParams(params2);
		tr.addView(product);

		TextView percent = new TextView(getApplicationContext());
		percent.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
		percent.setText(getApplicationContext().getResources().getString(
				R.string.percent));
		percent.setLayoutParams(params2);
		percent.setTextColor(Color.WHITE);
		tr.addView(percent);

		TextView AMT = new TextView(getApplicationContext());
		AMT.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
		AMT.setText(getApplicationContext().getResources().getString(
				R.string.AMT));
		AMT.setPadding(0, 0, 5, 0);
		AMT.setLayoutParams(params2);
		AMT.setTextColor(Color.WHITE);
		tr.addView(AMT);

		TextView UNIT = new TextView(getApplicationContext());
		UNIT.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
		UNIT.setText(getApplicationContext().getResources().getString(
				R.string.UNIT));
		UNIT.setPadding(0, 0, 5, 0);
		UNIT.setLayoutParams(params2);
		UNIT.setTextColor(Color.WHITE);
		tr.addView(UNIT);

		TextView TARGET = new TextView(getApplicationContext());
		TARGET.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
		TARGET.setText(getApplicationContext().getResources().getString(
				R.string.TARGET));
		TARGET.setPadding(0, 0, 5, 0);
		TARGET.setLayoutParams(params2);
		TARGET.setTextColor(Color.WHITE);
		tr.addView(TARGET);

		tb.addView(tr, new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT));

	}

	private void setPreviousFilledData() {
		// TODO Auto-generated method stub
		addChekboxesToList();
		// txtName.setText(generalInfoDTO.getFirst_Name());
		txtAccountNumber.setText(generalInfoDTO.getAccountNo());
		// txtTimeIn.setText(generalInfoDTO.getTimeIn());
		txtOrderNumber.setText(generalInfoDTO.getOrder_Number());

		JSONObject commercial_pest_string = HoustonFlowFunctions
				.getCommercialReport(serviceID_new);
		Log.e("commercial_pest_string string.. to string 1..",
				"commercial_pest_string string..." + commercial_pest_string);

		if (!(commercial_pest_string == null)) {
			// Log.e("residentil string.. to string..",
			// "residentil string..."+residential_pest_string.toString());
			hCommercial_pest_DTO = new Gson().fromJson(
					commercial_pest_string.toString(), HCommercial_pest_DTO.class);
			strCommercialId = hCommercial_pest_DTO.getCommercialId();
			if (CommonFunction.checkString(strCommercialId, "").equals("")) {
				strCommercialId = "0";
			}
			setPreviousServiceProvided();
			setPreviousAreaseInspected();
			setPreviousInspectedTreatedFor();

			setPreviousPestByZone();
			setPreviousPestControlServices();
			setPreviousTermiteServices();
			setPreviousRodentServices();
			setPreviousMosquitoServices();
			setpreviousRecommendations();

		}

		setpreviousInvoiceDetail();
	}

	private void setpreviousInvoiceDetail() {
		// TODO Auto-generated method stub
		ArrayList<RadioButton> listRadioButton = new ArrayList<RadioButton>();
		listRadioButton.add(radioCash);
		listRadioButton.add(radioCheck);
		listRadioButton.add(radioCreditcard);
		listRadioButton.add(radioNocharge);
		listRadioButton.add(radioBillLater);
		listRadioButton.add(radioPreBill);
		// Log.e("getPaymentType...",listRadioButton.size()+"");

		for (int i = 0; i < listRadioButton.size(); i++) {

			String selection = listRadioButton.get(i).getText().toString();

			if (selection.equals(hCommercial_pest_DTO.getPaymentType())) {
				listRadioButton.get(i).setChecked(true);
				break;
			}
		}
		edtTechComment.setText(hCommercial_pest_DTO.getTechnicianComments());
		edtServiceTech.setText(generalInfoDTO.getService_Tech_Num());
		edtEMP.setText(hCommercial_pest_DTO.getEmpNo());
		edtDate.setText(generalInfoDTO.getServiceDate());

		if (CommonFunction.checkString(generalInfoDTO.getFirst_Name(), "")
				.equals("")
				&& CommonFunction
						.checkString(generalInfoDTO.getLast_Name(), "").equals(
								"")) {
			edtCustomer.setText(generalInfoDTO.getCompanyName());

		} else {
			edtCustomer.setText(generalInfoDTO.getFirst_Name() + " "
					+ generalInfoDTO.getLast_Name());

		}
		edtIntValue.setText(generalInfoDTO.getInv_value());
		edtProductvalue.setText(generalInfoDTO.getProd_value());

		String taxValue;
		double roundOff = 0;
		if (CommonFunction.checkString(generalInfoDTO.getInv_value(), "")
				.equals("")
				|| CommonFunction.checkString(generalInfoDTO.getTaxCode(), "")
						.equals("")) {
			taxValue = "0";
		} else {
			Float taxValueFloat = Float.valueOf(generalInfoDTO.getInv_value())
					* Float.valueOf(generalInfoDTO.getTaxCode());
			roundOff = Math.round(taxValueFloat * 100.0) / 100.0;
			taxValue = String.valueOf(roundOff);
		}

		edtTaxvalue.setText(taxValue);
		// Log.e("taxValue", "taxValue.."+taxValue);
		Double totlaD = roundOff
				+ Double.valueOf(generalInfoDTO.getInv_value());
		Double totalFinal = Math.round(totlaD * 100.0) / 100.0;
		edtTotal.setText(String.valueOf(totalFinal));
		edtCheckNo.setText(generalInfoDTO.getCheckNo());
		// Toast.makeText(getApplicationContext(),
		// generalInfoDTO.getLicenseNo()+"", Toast.LENGTH_LONG).show();

		edtDRIVING_LICENCE.setText(generalInfoDTO.getLicenseNo());
		if (!generalInfoDTO.getExpirationDate().equals("01/01/1900")
				&& !generalInfoDTO.getExpirationDate().equals("")) {
			edtEXPIRATION_DATE.setText(generalInfoDTO.getExpirationDate());
		}

		edtAmount.setText(hCommercial_pest_DTO.getAmount());

		strCommercialId = hCommercial_pest_DTO.getCommercialId();
		// Log.e("",
		// "strResidentialId.."+residential_pest_DTO.getResidentialId());
		if (CommonFunction.checkString(strCommercialId, "").equals("")) {
			strCommercialId = "0";
		}
		// edttxtDrivingLicence.setText(generalInfoDTO.getDr);
		// edttxtTotal.setText(generalInfoDTO.getTotal());
		// Log.e("",
		// "generalInfoDTO.getCheckFrontImage()..."+generalInfoDTO.getCheckFrontImage());
		if (!CommonFunction
				.checkString(generalInfoDTO.getCheckFrontImage(), "")
				.equals("")) {
			RefreshFrontImage();
			reloadPriviousCheckFront();
		}
		if (!CommonFunction.checkString(generalInfoDTO.getCheckBackImage(), "")
				.equals("")) {
			RefreshBackImage();
			reloadPriviousCheckBack();
		}

	}

	public void reloadPriviousCheckFront() {
		String imageNames = generalInfoDTO.getCheckFrontImage();

		// Log.e("", "imageNames.."+imageNames);

		if (!CommonFunction.checkString(imageNames, "").equals("")) {
			String[] imageArray = imageNames.split(",");
			for (int i = 0; i < imageArray.length; i++) {
				if (!imageArray[i].equals("")) {
					tempStoreFrontImageName.add(imageArray[i]);
					RefreshFrontImage();
				}
			}
		}
	}

	public void reloadPriviousCheckBack() {
		String imageNames = generalInfoDTO.getCheckBackImage();

		// Log.e("", "imageNames.."+imageNames);

		if (!CommonFunction.checkString(imageNames, "").equals("")) {
			String[] imageArray = imageNames.split(",");
			for (int i = 0; i < imageArray.length; i++) {
				if (!imageArray[i].equals("")) {
					tempStoreBackImageName.add(imageArray[i]);
					RefreshBackImage();
				}
			}
		}
	}

	private void RefreshBackImage() {
		try {
			listBackImg.clear();
			Bitmap bmp = null;
			for (int i = 0; i < tempStoreBackImageName.size(); i++) {
				bmp = null;
				ImgDto dto = new ImgDto();
				try {
					LoggerUtil.e("tempStoreBackImageName in refresh",
							tempStoreBackImageName.get(i));
					/*
					 * bmp = BitmapFactory.decodeStream(getContentResolver()
					 * .openInputStream
					 * (Uri.fromFile(CommanFunction.getFileLocation
					 * (IWitnessActivity
					 * .this,tempStoreFrontImageName.get(i)))));
					 */
					bmp = decodeSmallUri(Uri.fromFile(CommonFunction
							.getFileLocation(
									HC_PestControlServiceReport.this,
									tempStoreBackImageName.get(i))));

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Bitmap bitmap = scaleBitmap(bmp, 100, 100);
				dto.setImageBitMap(bitmap);
				if (!tempBackImagList.contains(tempStoreBackImageName.get(i))) {

					if (bmp != null) {

						// CompressImage(bmp);
					}
				}// dto.setImageUri(Uri.fromFile(CommanFunction.getFileLocation(IWitnessActivity.this,
					// tempStoreFrontImageName.get(i))));

				tempBackImagList.add(tempStoreBackImageName.get(i));
				listBackImg.add(dto);
			}

			// LoggerUtil.e("List Size", listImg.size() + "");

			/*
			 * if (listBackImg.size() == 0) { ImgDto dto = new ImgDto();
			 * dto.setImagedrawable(R.drawable.noimage); listBackImg.add(dto);
			 * }else {
			 * 
			 * }
			 */
			adapterBack = new GalleryImageAdapter(getApplicationContext(),
					listBackImg);
			imageBackListView.setAdapter(adapterBack);
			// adapter.notifyDataSetChanged(); // TODO Auto-generated method
			// stub
		} catch (Exception e) {
			LoggerUtil.e("Exception in Refresh Image", e + "");
		}
	}

	public static Bitmap scaleBitmap(Bitmap bitmapToScale, float newWidth,
			float newHeight) {
		if (bitmapToScale == null)
			return null;
		// get the original width and height
		int width = bitmapToScale.getWidth();
		int height = bitmapToScale.getHeight();
		// create a matrix for the manipulation
		Matrix matrix = new Matrix();

		// resize the bit map
		matrix.postScale(newWidth / width, newHeight / height);

		// recreate the new Bitmap and set it back
		return Bitmap.createBitmap(bitmapToScale, 0, 0,
				bitmapToScale.getWidth(), bitmapToScale.getHeight(), matrix,
				true);
	}

	private void RefreshFrontImage() {
		// TODO Auto-generated method stub
		try {
			listFrontImg.clear();
			Bitmap bmp = null;
			for (int i = 0; i < tempStoreFrontImageName.size(); i++) {
				bmp = null;
				ImgDto dto = new ImgDto();
				try {
					LoggerUtil.e("tempStoreFrontImageName in refresh",
							tempStoreFrontImageName.get(i));
					/*
					 * bmp = BitmapFactory.decodeStream(getContentResolver()
					 * .openInputStream
					 * (Uri.fromFile(CommanFunction.getFileLocation
					 * (IWitnessActivity
					 * .this,tempStoreFrontImageName.get(i)))));
					 */
					bmp = decodeSmallUri(Uri.fromFile(CommonFunction
							.getFileLocation(
									HC_PestControlServiceReport.this,
									tempStoreFrontImageName.get(i))));

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Bitmap bitmap = scaleBitmap(bmp, 100, 100);
				dto.setImageBitMap(bitmap);
				if (!tempFrontImagList.contains(tempStoreFrontImageName.get(i))) {

					if (bmp != null) {

						// CompressImage(bmp);
					}
				}// dto.setImageUri(Uri.fromFile(CommanFunction.getFileLocation(IWitnessActivity.this,
					// tempStoreFrontImageName.get(i))));

				tempFrontImagList.add(tempStoreFrontImageName.get(i));
				listFrontImg.add(dto);
			}

			// LoggerUtil.e("List Size", listImg.size() + "");

			/*
			 * if (listFrontImg.size() == 0) { ImgDto dto = new ImgDto();
			 * dto.setImagedrawable(R.drawable.noimage); listFrontImg.add(dto);
			 * } else {
			 * 
			 * }
			 */
			adapterFront = new GalleryImageAdapter(getApplicationContext(),
					listFrontImg);
			imageFrontListView.setAdapter(adapterFront);
			// adapter.notifyDataSetChanged(); // TODO Auto-generated method
			// stub
		} catch (Exception e) {
			LoggerUtil.e("Exception in Refresh Image", e + "");
		}
	}

	private Bitmap decodeSmallUri(Uri selectedImage)
			throws FileNotFoundException {
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(HC_PestControlServiceReport.this
				.getContentResolver().openInputStream(selectedImage), null, o);

		final int REQUIRED_SIZE = 100;

		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeStream(
				HC_PestControlServiceReport.this.getContentResolver()
						.openInputStream(selectedImage), null, o2);
	}

	private void setpreviousRecommendations() {
		strRecommendations = hCommercial_pest_DTO.getRecommendations();
		// Log.e("", "service for records...."+strServiceFor);
		ArrayList<String> recommendationsList = new ArrayList<String>(
				Arrays.asList(strRecommendations.split(",")));
		for (int s = 0; s < recommendationsList.size(); s++) {
			// Log.e("", "service for records...inside for 1."+strServiceFor);
			for (int c = 0; c < recommendationCheckboxList.size(); c++) {
				// Log.e("",
				// "service for records...inside for 2."+strServiceFor);
				if (recommendationsList.get(s).equals(
						recommendationCheckboxList.get(c).getText())) {
					// Log.e("",
					// "service for records...inside if."+strServiceFor);
					recommendationCheckboxList.get(c).setChecked(true);
				}
			}
		}
	}

	private void setPreviousMosquitoServices() {

		// TODO Auto-generated method stub
		strMosquito = hCommercial_pest_DTO.getPlusMosquitoDetail();
		ArrayList<String> mosquitoList = new ArrayList<String>(
				Arrays.asList(strMosquito.split(",")));
		for (int s = 0; s < mosquitoList.size(); s++) {
			for (int c = 0; c < mosquitoCheckboxList.size(); c++) {
				if (mosquitoList.get(s).equals(
						mosquitoCheckboxList.get(c).getText())) {
					mosquitoCheckboxList.get(c).setChecked(true);
				}
			}
		}
		edtMosquitoService1.setText(hCommercial_pest_DTO.getDurationTime());
		edtMosquitoService2.setText(hCommercial_pest_DTO.getSystemRuns());

	}

	private void setPreviousRodentServices() {
		// TODO Auto-generated method stub
		if (hCommercial_pest_DTO.getInspectedRodentStation().equals(
				checkBoxINSPECTED_RODENT_STATIONS.getText())) {
			checkBoxINSPECTED_RODENT_STATIONS.setChecked(true);
		
			if (!CommonFunction.checkString(hCommercial_pest_DTO.getTxtInspectedRodentStation(), "").equals(""))
			{
			edt_how_many_INSPECTED_RODENT_STATIONS.setText(hCommercial_pest_DTO
					.getTxtInspectedRodentStation());
			}
		}

		if (hCommercial_pest_DTO.getCleanedRodentStation().equals(
				checkBoxCLEANES_RODENT_STATIONS.getText())) {
			checkBoxCLEANES_RODENT_STATIONS.setChecked(true);
			if (hCommercial_pest_DTO.getIsCleanedRodentStation().equals("Yes")) {
				radiobtnCLEANES_RODENT_STATIONS1.setChecked(true);
			} else if(hCommercial_pest_DTO.getIsCleanedRodentStation().equals("No")){
				radiobtnCLEANES_RODENT_STATIONS1.setChecked(true);
			}
			else
			{
				
			}
		}

		if (hCommercial_pest_DTO.getInstallRodentBaitStation().equals(
				checkBoxINSTALLED_RODENT_BAIT_STATIONS.getText())) {
			checkBoxINSTALLED_RODENT_BAIT_STATIONS.setChecked(true);
			if (hCommercial_pest_DTO.getIsInstallRodentBaitStation().equals("Yes")) {
				radiobtnINSTALLED_RODENT_BAIT_STATIONS1.setChecked(true);
			} else if(hCommercial_pest_DTO.getIsInstallRodentBaitStation().equals("No")){
				radiobtnINSTALLED_RODENT_BAIT_STATIONS2.setChecked(true);
			}
			else
			{
				
			}
			if (!CommonFunction.checkString(hCommercial_pest_DTO.getTxtInstallRodentBaitStation(), "").equals(""))
			{
			edt_how_many_INSTALLED_RODENT_BAIT_STATIONS.setText(hCommercial_pest_DTO
					.getTxtInstallRodentBaitStation());
			}
		}

		if (hCommercial_pest_DTO.getReplacedBait().equals(
				checkBoxREPLACED_BAIT.getText())) {
			checkBoxREPLACED_BAIT.setChecked(true);
			if (hCommercial_pest_DTO.getIsReplacedBait().equals("Yes")) {
				radiobtnREPLACED_BAIT1.setChecked(true);
			} else if(hCommercial_pest_DTO.getIsReplacedBait().equals("No")){
				radiobtnREPLACED_BAIT2.setChecked(true);
			}
			else
			{
				
			}
			if (!CommonFunction.checkString(hCommercial_pest_DTO.getSNoReplacedBait(), "").equals(""))
			{
			edt_station_REPLACED_BAIT.setText(hCommercial_pest_DTO
					.getSNoReplacedBait());
			}
		}

		if (hCommercial_pest_DTO.getReplacedBaitStation().equals(
				checkBoxREPLACED_BAIT_STATIONS.getText())) {
			checkBoxREPLACED_BAIT_STATIONS.setChecked(true);
			if (hCommercial_pest_DTO.getIsReplacedBaitStation().equals("Yes")) {
				radiobtnREPLACED_BAIT_STATIONS1.setChecked(true);
			} else if(hCommercial_pest_DTO.getIsReplacedBaitStation().equals("No")) {
				radiobtnREPLACED_BAIT_STATIONS2.setChecked(true);
			}
			else
			{
				
			}
			if (!CommonFunction.checkString(hCommercial_pest_DTO.getTxtReplacedBaitStation(), "").equals(""))
			{
			edt_how_many_REPLACED_BAIT_STATIONS.setText(hCommercial_pest_DTO
					.getTxtReplacedBaitStation());
			}
			if (!CommonFunction.checkString(hCommercial_pest_DTO.getSNoReplacedBaitStation(), "").equals(""))
			{
			edt_station_REPLACED_BAIT_STATIONS.setText(hCommercial_pest_DTO
					.getSNoReplacedBaitStation());
			}
		}

		if (hCommercial_pest_DTO.getInspectedRodentTraps().equals(
				checkBoxINSPECTED_RODENT_TRAPS.getText())) {
			checkBoxINSPECTED_RODENT_TRAPS.setChecked(true);
			if (hCommercial_pest_DTO.getIsInspectedRodentTraps().equals("Yes")) {
				radiobtnINSPECTED_RODENT_TRAPS1.setChecked(true);
			} else if(hCommercial_pest_DTO.getIsInspectedRodentTraps().equals("No")) {
				radiobtnINSPECTED_RODENT_TRAPS2.setChecked(true);
			}
			else
			{
				
			}

		}

		if (hCommercial_pest_DTO.getCleanedRodentTrap().equals(
				checkBoxCLEANED_RODENT_TRAPS.getText())) {
			checkBoxCLEANED_RODENT_TRAPS.setChecked(true);
			if (hCommercial_pest_DTO.getIsCleanedRodentTrap().equals("Yes")) {
				radiobtnCLEANED_RODENT_TRAPS1.setChecked(true);
			} else if(hCommercial_pest_DTO.getIsCleanedRodentTrap().equals("No")) {
				radiobtnCLEANED_RODENT_TRAPS2.setChecked(true);
			}
			else
			{
				
			}

		}

		if (hCommercial_pest_DTO.getInstallRodentTrap().equals(
				checkBoxINSTALLED_RODENT_TRAPS.getText())) {
			checkBoxINSTALLED_RODENT_TRAPS.setChecked(true);
			if (hCommercial_pest_DTO.getIsInstallRodentTrap().equals("Yes")) {
				radiobtnINSTALLED_RODENT_TRAPS1.setChecked(true);
			} else if(hCommercial_pest_DTO.getIsInstallRodentTrap().equals("No")) {
				radiobtnINSTALLED_RODENT_TRAPS2.setChecked(true);
			}
			else
			{
				
			}
			if (!CommonFunction.checkString(hCommercial_pest_DTO.getTxtInstallRodentTrap(), "").equals(""))
			{
			edt_how_many_INSTALLED_RODENT_TRAPS.setText(hCommercial_pest_DTO
					.getTxtInstallRodentTrap());
			}
		}

		if (hCommercial_pest_DTO.getReplacedRodentTrap().equals(
				checkBoxREPLACED_RODENT_TRAPS.getText())) {
			checkBoxREPLACED_RODENT_TRAPS.setChecked(true);
			if (hCommercial_pest_DTO.getIsReplacedRodentTrap().equals("Yes")) {
				radiobtnREPLACED_RODENT_TRAPS1.setChecked(true);
			} else if (hCommercial_pest_DTO.getIsReplacedRodentTrap().equals("No")) {
				radiobtnREPLACED_RODENT_TRAPS2.setChecked(true);
			}
			else
			{
				
			}
			if (!CommonFunction.checkString(hCommercial_pest_DTO.getTxtReplacedRodentTrap(), "").equals(""))
			{
			edt_how_many_REPLACED_RODENT_TRAPS.setText(hCommercial_pest_DTO
					.getTxtReplacedRodentTrap());
			}
			if (!CommonFunction.checkString(hCommercial_pest_DTO.getSNoReplacedRodentTrap(), "").equals(""))
			{
			edt_station_REPLACED_RODENT_TRAPS.setText(hCommercial_pest_DTO
					.getSNoReplacedRodentTrap());
			}
		}

		if (hCommercial_pest_DTO.getSealedEntryPoint().equals(
				checkBoxSEALED_ENTRY_POINTS.getText())) {
			checkBoxSEALED_ENTRY_POINTS.setChecked(true);
			if (hCommercial_pest_DTO.getIsSealedEntryPoint().equals("Yes")) {
				radiobtnSEALED_ENTRY_POINTS1.setChecked(true);
			} else if (hCommercial_pest_DTO.getIsSealedEntryPoint().equals("No")){
				radiobtnSEALED_ENTRY_POINTS2.setChecked(true);
			}
			else
			{
				
			}
		}

		// Log.e("InspectedSnapTraps...",
		// ""+commercialDTO.getInspectedSnapTrap()+checkBoxINSPECTED_SNAP_TRAPS.getText()+commercialDTO.getTxtInspectedSnapTrap());
		if (hCommercial_pest_DTO.getInspectedSnapTrap().equals(
				checkBoxINSPECTED_SNAP_TRAPS.getText())) {
			checkBoxINSPECTED_SNAP_TRAPS.setChecked(true);
			if (!CommonFunction.checkString(hCommercial_pest_DTO.getTxtInspectedSnapTrap(), "").equals(""))
			{
			edt_how_many_INSPECTED_SNAP_TRAPS.setText(hCommercial_pest_DTO
					.getTxtInspectedSnapTrap());
			}
		}

		if (hCommercial_pest_DTO.getInspectedLiveCages().equals(
				checkBoxINSPECTED_LIVE_CAGES.getText())) {
			checkBoxINSPECTED_LIVE_CAGES.setChecked(true);
			if (!CommonFunction.checkString(hCommercial_pest_DTO.getTxtInspectedLiveCages(), "").equals(""))
			{
			edt_how_many_INSPECTED_LIVE_CAGES.setText(hCommercial_pest_DTO
					.getTxtInspectedLiveCages());
			}
		}

		if (hCommercial_pest_DTO.getRemovedSnapTraps().equals(
				checkBoxREMOVED_SNAP_TRAPS.getText())) {
			checkBoxREMOVED_SNAP_TRAPS.setChecked(true);
			if (!CommonFunction.checkString(hCommercial_pest_DTO.getTxtRemovedSnapTraps(), "").equals(""))
			{
			edt_how_many_REMOVED_SNAP_TRAPS.setText(hCommercial_pest_DTO
					.getTxtRemovedSnapTraps());
			}
		}

		if (hCommercial_pest_DTO.getRemovedLiveCages().equals(
				checkBoxREMOVED_LIVE_CAGES.getText())) {
			checkBoxREMOVED_LIVE_CAGES.setChecked(true);
			if (!CommonFunction.checkString(hCommercial_pest_DTO.getTxtRemovedLiveCages(), "").equals(""))
			{
			edt_how_many_REMOVED_LIVE_CAGES.setText(hCommercial_pest_DTO
					.getTxtRemovedLiveCages());
			}
		}

		if (hCommercial_pest_DTO.getRemovedRodentBaitStation().equals(
				checkBoxREMOVED_RODENT_BAIT_STATIONS.getText())) {
			checkBoxREMOVED_RODENT_BAIT_STATIONS.setChecked(true);
			if (!CommonFunction.checkString(hCommercial_pest_DTO.getTxtRemovedRodentBaitStation(), "").equals(""))
			{
			edt_how_many_REMOVED_RODENT_BAIT_STATIONS.setText(hCommercial_pest_DTO
					.getTxtRemovedRodentBaitStation());
			}
		}

		if (hCommercial_pest_DTO.getRemovedRodentTrap().equals(
				checkBoxREMOVED_RODENT_TRAPS.getText())) {
			checkBoxREMOVED_RODENT_TRAPS.setChecked(true);
			if (!CommonFunction.checkString(hCommercial_pest_DTO.getTxtRemovedRodentTrap(), "").equals(""))
			{
			edt_how_many_REMOVED_RODENT_TRAPS.setText(hCommercial_pest_DTO
					.getTxtRemovedRodentTrap());
			}
		}

		if (hCommercial_pest_DTO.getSetSnapTrap().equals(
				checkBoxSET_SNAP_TRAPS.getText())) {
			checkBoxSET_SNAP_TRAPS.setChecked(true);
			if (!CommonFunction.checkString(hCommercial_pest_DTO.getTxtSetSnapTrap(), "").equals(""))
			{
			edt_SET_SNAP_TRAPS.setText(hCommercial_pest_DTO.getTxtSetSnapTrap());
			}
		}

		if (hCommercial_pest_DTO.getSetLiveCages().equals(
				checkBoxSET_LIVE_CAGES.getText())) {
			checkBoxSET_LIVE_CAGES.setChecked(true);
			if (!CommonFunction.checkString(hCommercial_pest_DTO.getTxtSetLiveCages(), "").equals(""))
			{
			edt_SET_LIVE_CAGES.setText(hCommercial_pest_DTO.getTxtSetLiveCages());
			}
		}

		strSnapTraps = hCommercial_pest_DTO.getLstSetSnapTrap();
		// Log.e("", "service for records...."+strServiceFor);
		ArrayList<String> snaptrapsList = new ArrayList<String>(
				Arrays.asList(strSnapTraps.split(",")));
		for (int s = 0; s < snaptrapsList.size(); s++) {
			// Log.e("", "service for records...inside for 1."+strServiceFor);
			for (int c = 0; c < snapTrapsCheckboxList.size(); c++) {
				// Log.e("",
				// "service for records...inside for 2."+strServiceFor);
				if (snaptrapsList.get(s).equals(
						snapTrapsCheckboxList.get(c).getText())) {
					// Log.e("",
					// "service for records...inside if."+strServiceFor);
					snapTrapsCheckboxList.get(c).setChecked(true);
				}
			}
		}

		strLiveCages = hCommercial_pest_DTO.getLstSetLiveCages();
		// Log.e("", "service for records...."+strServiceFor);
		ArrayList<String> liveTrapsList = new ArrayList<String>(
				Arrays.asList(strLiveCages.split(",")));
		for (int s = 0; s < liveTrapsList.size(); s++) {
			// Log.e("", "service for records...inside for 1."+strServiceFor);
			for (int c = 0; c < liveCagesCheckboxList.size(); c++) {
				// Log.e("",
				// "service for records...inside for 2."+strServiceFor);
				if (liveTrapsList.get(s).equals(
						liveCagesCheckboxList.get(c).getText())) {
					// Log.e("",
					// "service for records...inside if."+strServiceFor);
					liveCagesCheckboxList.get(c).setChecked(true);
				}
			}
		}
	}

	private void setPreviousTermiteServices() {
		// TODO Auto-generated method stub
		if (hCommercial_pest_DTO.getSignOfTermiteMS().equals(
				checkBoxVISIBLE_SIGN_TERMITE_MONITORING_STATIONS.getText())) {
			checkBoxVISIBLE_SIGN_TERMITE_MONITORING_STATIONS.setChecked(true);
			if (hCommercial_pest_DTO.getSignOfTermiteActivityMS().equals("Yes")) {
				radiobtnVISIBLE_SIGN_TERMITE_MONITORING_STATIONS1
						.setChecked(true);
			} else if(hCommercial_pest_DTO.getSignOfTermiteActivityMS().equals("No")) {
				radiobtnVISIBLE_SIGN_TERMITE_MONITORING_STATIONS2
						.setChecked(true);
			}
			else
			{
				
			}
			if (!CommonFunction.checkString(hCommercial_pest_DTO.getSignOfTermiteText(), "").equals(""))
			{
			edt_station_VISIBLE_SIGN_TERMITE_MONITORING_STATIONS
					.setText(hCommercial_pest_DTO.getSignOfTermiteText());
			}
		}
		
		if (hCommercial_pest_DTO.getInstallMS().equals(
				checkBoxINSTALLED_MONITORING_STATIONS.getText())) {

			
			checkBoxINSTALLED_MONITORING_STATIONS.setChecked(true);
			
			if (!CommonFunction.checkString(hCommercial_pest_DTO.getInstallMSText(), "").equals(""))
			{
			
			edtINSTALLED_MONITORING_STATIONS.setText(hCommercial_pest_DTO
					.getInstallMSText());
			}
		}

		if (hCommercial_pest_DTO.getSignOfTermitePS().equals(
				checkBoxVISIBLE_SIGN_PERIMETER_STRUCTURE.getText())) {
			checkBoxVISIBLE_SIGN_PERIMETER_STRUCTURE.setChecked(true);
			if (hCommercial_pest_DTO.getSignOfTermiteActivityPS().equals("Yes")) {
				radiobtnVISIBLE_SIGN_PERIMETER_STRUCTURE1.setChecked(true);
			} else if(hCommercial_pest_DTO.getSignOfTermiteActivityPS().equals("No")) {
				radiobtnVISIBLE_SIGN_PERIMETER_STRUCTURE2.setChecked(true);
			}
			else
			{
				
			}

		}

		if (hCommercial_pest_DTO.getSignOfTermiteIS().equals(
				checkBoxVISIBLE_SIGN_INTERIOR_STRUCTURE.getText())) {
			checkBoxVISIBLE_SIGN_INTERIOR_STRUCTURE.setChecked(true);
			if (hCommercial_pest_DTO.getSignOfTermiteActivityIS().equals("Yes")) {
				radiobtnVISIBLE_SIGN_INTERIOR_STRUCTURE1.setChecked(true);
			} else if(hCommercial_pest_DTO.getSignOfTermiteActivityIS().equals("No")){
				radiobtnVISIBLE_SIGN_INTERIOR_STRUCTURE2.setChecked(true);
			}
			else{
				
			}

		}
		
		if (hCommercial_pest_DTO.getReplacedMS().equals(
				checkBoxREPLACED_MONITORING_STATIONS.getText())) {
			checkBoxREPLACED_MONITORING_STATIONS.setChecked(true);
			if (!CommonFunction.checkString(hCommercial_pest_DTO.getReplacedActivityTextMS(), "").equals(""))
			{
			edtREPLACED_MONITORING_STATIONS.setText(hCommercial_pest_DTO
					.getReplacedActivityTextMS());
			}
			if (!CommonFunction.checkString(hCommercial_pest_DTO.getReplacedTextMS(), "").equals(""))
			{
			edt_station_REPLACED_MONITORING_STATIONS.setText(hCommercial_pest_DTO
					.getReplacedTextMS());
			}
		}

		if (hCommercial_pest_DTO.getTreatedPS().equals(
				checkBoxTREATED_PERIMETER_STRUCTURE.getText())) {
			checkBoxTREATED_PERIMETER_STRUCTURE.setChecked(true);
			if (hCommercial_pest_DTO.getTreatedActivityPS().equals("Yes")) {
				radiobtnTREATED_PERIMETER_STRUCTURE1.setChecked(true);
			} else if(hCommercial_pest_DTO.getTreatedActivityPS().equals("No"))  {
				radiobtnTREATED_PERIMETER_STRUCTURE2.setChecked(true);
			}
			else
			{
				
			}

		}

		if (hCommercial_pest_DTO.getTreatedSP().equals(
				checkBoxTREATED_SLAB_PENETRATIONS.getText())) {
			checkBoxTREATED_SLAB_PENETRATIONS.setChecked(true);
			if (hCommercial_pest_DTO.getTreatedActivitySP().equals("Yes")) {
				radiobtnTREATED_SLAB_PENETRATIONS1.setChecked(true);
			} else if(hCommercial_pest_DTO.getTreatedActivitySP().equals("No")){
				radiobtnTREATED_SLAB_PENETRATIONS2.setChecked(true);
			}
			else
			{
				
			}

		}

		if (hCommercial_pest_DTO.getTreatedCC().equals(
				checkBoxTREATED_CONDUSIVE_CONDITIONS.getText())) {
			checkBoxTREATED_CONDUSIVE_CONDITIONS.setChecked(true);
			if (hCommercial_pest_DTO.getTreatedActivityCC().equals("Yes")) {
				radiobtnTREATED_CONDUSIVE_CONDITIONS1.setChecked(true);
			} else if(hCommercial_pest_DTO.getTreatedActivityCC().equals("No")) {
				radiobtnTREATED_CONDUSIVE_CONDITIONS2.setChecked(true);
			}
			else
			{
				
			}

		}
	}

	private void setPreviousPestControlServices() {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		strPestControlServices = hCommercial_pest_DTO.getPestControlServices();
		ArrayList<String> pestControlServicesList = new ArrayList<String>(
				Arrays.asList(strPestControlServices.split(",")));
		for (int s = 0; s < pestControlServicesList.size(); s++) {
			for (int c = 0; c < pestControlServicesCheckboxList.size(); c++) {
				if (pestControlServicesList.get(s).equals(
						pestControlServicesCheckboxList.get(c).getText())) {
					pestControlServicesCheckboxList.get(c).setChecked(true);
				}
			}
		}
	}

	private void setPreviousPestByZone() {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		strpestActivityByZone = hCommercial_pest_DTO.getPestActivity();
		// Log.e("strpestActivityByZone", ""+strpestActivityByZone);

		ArrayList<String> pestActivityList = new ArrayList<String>(
				Arrays.asList(strpestActivityByZone.split(",")));
		// Log.e("pestActivityList.size()", ""+pestActivityList.size());

		for (int s = 0; s < pestActivityList.size(); s++) {
			for (int c = 0; c < PestActivityZoneCheckboxList.size(); c++) {
				if (pestActivityList.get(s).equals(
						PestActivityZoneCheckboxList.get(c).getText())) {
					// Log.e("PestActivityZoneCheckboxList.get(c).getText()",
					// ""+pestActivityList.get(s).equals(PestActivityZoneCheckboxList.get(c).getText()));

					PestActivityZoneCheckboxList.get(c).setChecked(true);
				}
			}
		}

		edtINTERIOR.setText(hCommercial_pest_DTO.getInterior());
		edtCOMMON_AREAS.setText(hCommercial_pest_DTO.getCommon_Areas());
		edtOUTSIDE_PERIMETER.setText(hCommercial_pest_DTO.getOutsidef_Perimeter());
		edtKITCHEN_PestActivityByZone.setText(hCommercial_pest_DTO.getKitchen());
		edtDUMPSTER.setText(hCommercial_pest_DTO.getDumpster());
		edtDRY_STORAGE.setText(hCommercial_pest_DTO.getDry_Storage());
		edtDISH_WSHING.setText(hCommercial_pest_DTO.getDish_Washing());
		edtDINING_ROOM.setText(hCommercial_pest_DTO.getDining_Room());
		edtROOF_TOPS.setText(hCommercial_pest_DTO.getRoof_Tops());
		edtWAIT_STATION.setText(hCommercial_pest_DTO.getWait_Stations());
		edtDROP_CEILINGS.setText(hCommercial_pest_DTO.getDrop_Ceiling());
		edtPLANTERS.setText(hCommercial_pest_DTO.getPlanters());
		edtWAREHOUSE_STORAGE.setText(hCommercial_pest_DTO.getWarehouse_Storage());
	}

	private void setPreviousInspectedTreatedFor() {
		// TODO Auto-generated method stub

		strInspectedadnTreatedFor = hCommercial_pest_DTO.getInsectActivity();
		ArrayList<String> InspectedTreatedList = new ArrayList<String>(
				Arrays.asList(strInspectedadnTreatedFor.split(",")));
		for (int s = 0; s < InspectedTreatedList.size(); s++) {
			for (int c = 0; c < InspectedTreatedForCheckboxList.size(); c++) {
				if (InspectedTreatedList.get(s).equals(
						InspectedTreatedForCheckboxList.get(c).getText())) {
					InspectedTreatedForCheckboxList.get(c).setChecked(true);
				}
			}
		}
		if (!CommonFunction.checkString(hCommercial_pest_DTO.getAnts(), "").equals("")) {
			edtiNSPECTEDANDtREATEDFOR1.setText(hCommercial_pest_DTO.getAnts());
			checkBoxANTS.setChecked(true);
		}

		if (!CommonFunction.checkString(hCommercial_pest_DTO.getOtherPest(), "")
				.equals("")) {
			edtiNSPECTEDANDtREATEDFOR2.setText(hCommercial_pest_DTO.getOtherPest());
			checkBoxOTHER_PESTS.setChecked(true);
		}
	}

	private void setPreviousAreaseInspected() {
		// TODO Auto-generated method stub
		strAreasInspectedExterior = "";
		strAreasInspectedInterior = "";
		strAreasInspectedInterior = hCommercial_pest_DTO.getAreasInspectedInterior();
		strAreasInspectedExterior = hCommercial_pest_DTO.getAreasInspectedExterior();

		ArrayList<String> areasInspectedInteriorList = new ArrayList<String>(
				Arrays.asList(strAreasInspectedInterior.split(",")));
		for (int s = 0; s < areasInspectedInteriorList.size(); s++) {
			for (int c = 0; c < AreasInspectedInteriorCheckboxList.size(); c++) {

				// Log.e("strAreasInspectedExterior c", "" + c);

				if (areasInspectedInteriorList.get(s).equals(
						AreasInspectedInteriorCheckboxList.get(c).getText())) {
					AreasInspectedInteriorCheckboxList.get(c).setChecked(true);
				}
			}
		}

		ArrayList<String> areasInspectedExteriorList = new ArrayList<String>(
				Arrays.asList(strAreasInspectedExterior.split(",")));
		for (int s = 0; s < areasInspectedExteriorList.size(); s++) {
			for (int c = 0; c < AreasInspectedExteriorCheckboxList.size(); c++) {
				if (areasInspectedExteriorList.get(s).equals(
						AreasInspectedExteriorCheckboxList.get(c).getText())) {
					AreasInspectedExteriorCheckboxList.get(c).setChecked(true);
				}
			}
		}
	}

	private void addChekboxesToList() {
		// TODO Auto-generated method stub
		serviceForCheckboxList.add(checkboxCommercialPcService);
		serviceForCheckboxList.add(checkboxCommercialX);
		serviceForCheckboxList.add(checkboxCommercialPcInitial);
		serviceForCheckboxList.add(checkboxCommercialTermite);

		AreasInspectedInteriorCheckboxList.add(checkboxOFFICES);
		AreasInspectedInteriorCheckboxList.add(checkboxBREAKROOMS);
		AreasInspectedInteriorCheckboxList.add(checkboxLOG_BOOK);
		AreasInspectedInteriorCheckboxList.add(checkboxFOOD_CHARTS);
		AreasInspectedInteriorCheckboxList.add(checkboxCOFFEE_BARS);
		AreasInspectedInteriorCheckboxList.add(checkboxKITCHEN_AreasInspected);
		AreasInspectedInteriorCheckboxList.add(checkboxLAUNDRY);
		AreasInspectedInteriorCheckboxList.add(checkboxHOUSE_KEEPING);
		AreasInspectedInteriorCheckboxList
				.add(checkboxWAIT_STATIONS_AREAS_INSPECTED);
		AreasInspectedInteriorCheckboxList
				.add(checkboxFLY_LIHGTS_AREAS_INSPECTED);
		AreasInspectedInteriorCheckboxList.add(checkboxELEVATOR_PITS);
		AreasInspectedInteriorCheckboxList.add(checkboxMECHANICAL_ROOMS);
		AreasInspectedInteriorCheckboxList.add(checkboxPATIENT_ROOMS);
		AreasInspectedInteriorCheckboxList.add(checkboxNURSES_STATIONS);
		AreasInspectedInteriorCheckboxList.add(checkboxBAR);
		AreasInspectedInteriorCheckboxList.add(checkboxLOBBY);
		AreasInspectedInteriorCheckboxList
				.add(checkboxDiningRoom_AreasInspected);
		AreasInspectedInteriorCheckboxList
				.add(checkboxWarehouseStorage_AreasInspected);
		AreasInspectedInteriorCheckboxList.add(checkboxREST_ROOMS);
		AreasInspectedInteriorCheckboxList.add(checkboxPLANTERS_AreasInspected);
		AreasInspectedInteriorCheckboxList
				.add(checkboxDISH_WSHING_AREAS_INSPECTED);
		AreasInspectedInteriorCheckboxList.add(checkboxDRY_STORAGE);

		AreasInspectedExteriorCheckboxList.add(checkboxRODENT_BAIT_STATIONS);
		AreasInspectedExteriorCheckboxList.add(checkboxPERIMETER);
		AreasInspectedExteriorCheckboxList.add(checkboxLANDSCAPE_BEDS);
		AreasInspectedExteriorCheckboxList.add(checkboxMANHOLE_COVERS);
		AreasInspectedExteriorCheckboxList.add(checkboxGREASE_TRAPS);
		AreasInspectedExteriorCheckboxList.add(checkBoxROOF_TOPS);
		AreasInspectedExteriorCheckboxList.add(checkBoxDUMPSTER);
		AreasInspectedExteriorCheckboxList.add(checkboxDOCK);
		AreasInspectedExteriorCheckboxList.add(checkboxENTRY_POINTS);

		InspectedTreatedForCheckboxList.add(checkboxRoaches);
		InspectedTreatedForCheckboxList.add(checkboxAMERICAN_COCKROACHES);
		InspectedTreatedForCheckboxList.add(checkboxGERMAN_COCKROACHES);
		InspectedTreatedForCheckboxList.add(checkboxSPIDERS);
		InspectedTreatedForCheckboxList.add(checkboxSCORPIONS);
		InspectedTreatedForCheckboxList.add(checkboxSILVER_FISH);
		InspectedTreatedForCheckboxList.add(checkboxSTORED_PRODUCT_PESTS);
		InspectedTreatedForCheckboxList.add(checkboxPHARAOH_ANTS);
		InspectedTreatedForCheckboxList.add(checkboxFIRE_ANTS);
		InspectedTreatedForCheckboxList.add(checkboxCARPENTER_ANTS);
		InspectedTreatedForCheckboxList.add(checkboxMILLIPEDES);
		InspectedTreatedForCheckboxList.add(checkboxEAR_WIGS);
		InspectedTreatedForCheckboxList.add(checkboxPILL_BUGS);
		InspectedTreatedForCheckboxList.add(checkboxCRICKETS);
		InspectedTreatedForCheckboxList.add(checkboxWASPS);
		InspectedTreatedForCheckboxList.add(checkboxSUB_TERMITES);
		InspectedTreatedForCheckboxList.add(checkboxDRYWOOD_TERMITIES);
		InspectedTreatedForCheckboxList.add(checkboxROOF_RATS);
		InspectedTreatedForCheckboxList.add(checkboxMICE);
		InspectedTreatedForCheckboxList.add(checkboxRACOONS);
		InspectedTreatedForCheckboxList.add(checkboxOPOSSUMS);
		InspectedTreatedForCheckboxList.add(checkboxSKUNKS);
		InspectedTreatedForCheckboxList.add(checkboxSQUIRRELS);
		InspectedTreatedForCheckboxList.add(checkBoxTICKS);
		InspectedTreatedForCheckboxList.add(checkBoxBEDBUGS);
		InspectedTreatedForCheckboxList.add(checkBoxTAWNY_CRAZY_ANTS);
		//InspectedTreatedForCheckboxList.add(checkboxROOF_RATS);
		InspectedTreatedForCheckboxList.add(checkBoxANTS);
		InspectedTreatedForCheckboxList.add(checkBoxOTHER_PESTS);

		PestActivityZoneCheckboxList.add(checkBoxINTERIOR_PEST_ACTIVITY_ZONE);
		PestActivityZoneCheckboxList.add(checkBoxOUTSIDE_PERIMETER);
		PestActivityZoneCheckboxList.add(checkBoxDUMPSTER_pest_activitybyzone);
		PestActivityZoneCheckboxList
				.add(checkBoxDINING_ROOM_PestActivityByZone);
		PestActivityZoneCheckboxList.add(checkBoxCOMMON_AREAS);
		PestActivityZoneCheckboxList.add(checkBoxKITCHEN_Pest_Activity);
		PestActivityZoneCheckboxList.add(checkBoxDRY_STORAGE_Pest_Activity);
		PestActivityZoneCheckboxList.add(checkBoxDISH_WSHING_PEST_ACTIVTY);
		PestActivityZoneCheckboxList
				.add(checkboxROOF_TOPS_pestActivity_by_zone);
		PestActivityZoneCheckboxList.add(checkBoxWAIT_STATION_PEST_ACTIVTY);
		PestActivityZoneCheckboxList.add(checkBoxDROP_CEILINGS);
		PestActivityZoneCheckboxList.add(checkBoxPLANTERS_PestActivtyByZone);
		PestActivityZoneCheckboxList
				.add(checkBoxWAREHOUSE_STORAGE_PestActivtyByZone);

		pestControlServicesCheckboxList
				.add(checkBoxEXTERIOR_PEST_CONTROL_SERVICES);
		pestControlServicesCheckboxList.add(checkBoxTREATED_fRONT_PERIMETRE);
		pestControlServicesCheckboxList.add(checkBoxTREATED_BACK_PERIMETRE);
		pestControlServicesCheckboxList.add(checkBoxTREaTED_WHEPHOLES);
		pestControlServicesCheckboxList.add(checkBoxTREATED_CRACKS);
		pestControlServicesCheckboxList
				.add(checkBoxTREATED_HARBOURAGE_BREEDING_SITES);
		pestControlServicesCheckboxList.add(checkBoxTREATED_FIREANTS_LANDSCAPE);
		pestControlServicesCheckboxList.add(checkBoxTREATED_DOORWAYS_windows);

		pestControlServicesCheckboxList
				.add(checkBoxINTERIOR_PEST_CONTROL_SERVICES);
		pestControlServicesCheckboxList
				.add(checkBoxTREATED_INSPECT_DROP_CEILINGS);
		pestControlServicesCheckboxList.add(checkBoxTREATED_KITCHEN);
		pestControlServicesCheckboxList.add(checkBoxTREATED_LAUNDRY);
		pestControlServicesCheckboxList.add(checkBoxTREATED_INSPECT_OFFICES);
		pestControlServicesCheckboxList.add(checkBoxTREATED_INSPECT_ROOF_TOPS);
		pestControlServicesCheckboxList.add(checkBoxTREATED_ZONE_MONITORS);
		pestControlServicesCheckboxList
				.add(checkBoxTREATED_INSPECT_COMMON_AREAS);

		OutsidepestControlServicesCheckboxList
				.add(checkBoxEXTERIOR_PEST_CONTROL_SERVICES);
		OutsidepestControlServicesCheckboxList
				.add(checkBoxTREATED_fRONT_PERIMETRE);
		OutsidepestControlServicesCheckboxList
				.add(checkBoxTREATED_BACK_PERIMETRE);
		OutsidepestControlServicesCheckboxList.add(checkBoxTREaTED_WHEPHOLES);
		OutsidepestControlServicesCheckboxList.add(checkBoxTREATED_CRACKS);
		OutsidepestControlServicesCheckboxList
				.add(checkBoxTREATED_HARBOURAGE_BREEDING_SITES);
		OutsidepestControlServicesCheckboxList
				.add(checkBoxTREATED_FIREANTS_LANDSCAPE);
		OutsidepestControlServicesCheckboxList
				.add(checkBoxTREATED_DOORWAYS_windows);

		InsidepestControlServicesCheckboxList
				.add(checkBoxINTERIOR_PEST_CONTROL_SERVICES);
		InsidepestControlServicesCheckboxList
				.add(checkBoxTREATED_INSPECT_DROP_CEILINGS);
		InsidepestControlServicesCheckboxList.add(checkBoxTREATED_KITCHEN);
		InsidepestControlServicesCheckboxList.add(checkBoxTREATED_LAUNDRY);
		InsidepestControlServicesCheckboxList
				.add(checkBoxTREATED_INSPECT_OFFICES);
		InsidepestControlServicesCheckboxList
				.add(checkBoxTREATED_INSPECT_ROOF_TOPS);
		InsidepestControlServicesCheckboxList
				.add(checkBoxTREATED_ZONE_MONITORS);
		InsidepestControlServicesCheckboxList
				.add(checkBoxTREATED_INSPECT_COMMON_AREAS);

		recommendationCheckboxList.add(checkBoxDEBRIS_IN_DRAINS);
		recommendationCheckboxList.add(checkBoxINACCSISIBLE_AREAS);
		recommendationCheckboxList.add(checkBoxMOISTURE_WALLS);
		recommendationCheckboxList.add(checkBoxDOCK_DOOR_LEFT_OPEN);
		recommendationCheckboxList.add(checkBoxDEBRIS_UNDER_COOKLINE);
		recommendationCheckboxList.add(checkBoxDOOR_PROPPED_OPEN);
		recommendationCheckboxList.add(checkBoxTRASH_CANTS_NOT_EMPTIED);
		recommendationCheckboxList.add(checkBoxWEATHER_STRIPPING_MISSING);
		recommendationCheckboxList.add(checkBoxSYRUP_SODA);
		recommendationCheckboxList.add(checkBoxAIR_CURTAIN);
		recommendationCheckboxList.add(checkBoxDIRTY_DINING_TABLE);
		recommendationCheckboxList.add(checkBoxFLY_LIGHTS);
		recommendationCheckboxList.add(checkBoxFILM_BAR_TaPS);

		snapTrapsCheckboxList.add(checkboxSET_SNAP_TRAPS1);
		snapTrapsCheckboxList.add(checkboxSET_SNAP_TRAPS2);
		snapTrapsCheckboxList.add(checkboxSET_SNAP_TRAPS3);
		snapTrapsCheckboxList.add(checkboxSET_SNAP_TRAPS4);

		liveCagesCheckboxList.add(checkboxSET_LIVE_CAGES1);
		liveCagesCheckboxList.add(checkboxSET_LIVE_CAGES2);
		liveCagesCheckboxList.add(checkboxSET_LIVE_CAGES3);
		liveCagesCheckboxList.add(checkboxSET_LIVE_CAGES4);

		mosquitoCheckboxList.add(checkBoxTopped_TANK);
		mosquitoCheckboxList.add(checkBoxLEACKY_TANK);
		mosquitoCheckboxList.add(checkBoxTUBING_LEACKY);
		mosquitoCheckboxList.add(checkBoxINSPECTED_NOZZLES);
		mosquitoCheckboxList.add(checkBoxRAN_SYATEMS);
		mosquitoCheckboxList.add(checkBoxNOZZLES_LEAKING);
		mosquitoCheckboxList.add(checkBoxCHECKED_RUN_DURACTION_TIME);
		mosquitoCheckboxList.add(checkBoxCHECKED_TIME_SYSTEM_RUNS);

	}

	private void setPreviousServiceProvided() {
		// TODO Auto-generated method stub
		strServiceFor = hCommercial_pest_DTO.getServiceFor();
		// Log.e("", "service for records...."+strServiceFor);
		ArrayList<String> serviceForList = new ArrayList<String>(
				Arrays.asList(strServiceFor.split(",")));
		for (int s = 0; s < serviceForList.size(); s++) {
			// Log.e("", "service for records...inside for 1."+strServiceFor);
			for (int c = 0; c < serviceForCheckboxList.size(); c++) {
				// Log.e("",
				// "service for records...inside for 2."+strServiceFor);
				if (serviceForList.get(s).equals(
						serviceForCheckboxList.get(c).getText())) {
					// Log.e("",
					// "service for records...inside if."+strServiceFor);
					serviceForCheckboxList.get(c).setChecked(true);
				}
			}
		}
	}

	private void initialize() {

		houstonFlowFunctions = new HoustonFlowFunctions(getApplicationContext());
		hCommercial_pest_DTO = new HCommercial_pest_DTO();
		logDTO = new LogDTO();
		userDTO = (UserDTO) this.getIntent().getExtras()
				.getSerializable("userDTO");
		generalInfoDTO = (GeneralInfoDTO) this.getIntent().getExtras()
				.getSerializable("generalInfoDTO");
		serviceID_new = this.getIntent().getExtras()
				.getString(ParameterUtil.ServiceID_new);
		JSONObject jsGeneralInfo = houstonFlowFunctions
				.getSelectedService(serviceID_new);
		if (jsGeneralInfo != null) {
			generalInfoDTO = new Gson().fromJson(jsGeneralInfo.toString(),
					GeneralInfoDTO.class);
			LoggerUtil
					.e("",
							"generalInfoDTO inside if..."
									+ generalInfoDTO.getCheckNo());
		}

		txtAccountNumber = (TextView) findViewById(R.id.txtAccountNumber);
		textviewCharactersTyped = (TextView) findViewById(R.id.textviewCharactersTyped);
		txtOrderNumber = (TextView) findViewById(R.id.txtOrderNumber);

		btnCHECK_BACK_IMAGE = (Button) findViewById(R.id.btnCHECK_BACK_IMAGE);
		btnCHECK_BACK_IMAGE.setOnClickListener(new OnButtonClick());
		btnCHECK_FRONT_IMAGE = (Button) findViewById(R.id.btnCHECK_FRONT_IMAGE);
		btnCHECK_FRONT_IMAGE.setOnClickListener(new OnButtonClick());
		btnCancel = (Button) findViewById(R.id.btnCancel);
		btnCancel.setOnClickListener(new OnButtonClick());
		btnsaveandcontinueINVOICE = (Button) findViewById(R.id.btnsaveandcontinueINVOICE);
		// btnTermiteDisclosure=(Button)findViewById(R.id.btnTermiteDisclosure);
		btnsaveandcontinueINVOICE.setOnClickListener(new OnButtonClick());
		imageFrontListView = (HorizontalListView) findViewById(R.id.listImg_front);

		imageFrontListView
				.setOnItemLongClickListener(new OnGalleryClickFront());
		imageFrontListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Bitmap bmp = listFrontImg.get(position).getImageBitMap();
				if (bmp != null) {
					bitmapFront = bmp;
					Intent intent = new Intent(
							HC_PestControlServiceReport.this,
							FullImageCommercial.class);
					intent.putExtra("from", "frontCheck");
					startActivity(intent);
				}
			}

		});
		imageBackListView = (HorizontalListView) findViewById(R.id.listImg_back);
		imageBackListView.setOnItemLongClickListener(new OnGalleryClickBack());
		imageBackListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Bitmap bmp = listBackImg.get(position).getImageBitMap();
				if (bmp != null) {
					bitmapBack = bmp;
					Intent intent = new Intent(
							HC_PestControlServiceReport.this,
							FullImageCommercial.class);
					intent.putExtra("from", "backCheck");
					startActivity(intent);
				}
			}

		});

		// all checkbox
		checkboxCommercialPcService = (CheckBox) findViewById(R.id.checkboxCommercialPcService);
		checkboxCommercialPcService
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxCommercialX = (CheckBox) findViewById(R.id.checkboxCommercialX);
		checkboxCommercialX
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxCommercialPcInitial = (CheckBox) findViewById(R.id.checkboxCommercialPcInitial);
		checkboxCommercialPcInitial
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxCommercialTermite = (CheckBox) findViewById(R.id.checkboxCommercialTermite);
		checkboxCommercialTermite
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxOFFICES = (CheckBox) findViewById(R.id.checkboxOFFICES);
		checkboxOFFICES
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxBREAKROOMS = (CheckBox) findViewById(R.id.checkboxBREAKROOMS);
		checkboxBREAKROOMS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxRODENT_BAIT_STATIONS = (CheckBox) findViewById(R.id.checkboxRODENT_BAIT_STATIONS);
		checkboxRODENT_BAIT_STATIONS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxLOG_BOOK = (CheckBox) findViewById(R.id.checkboxLOG_BOOK);
		checkboxLOG_BOOK
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxFOOD_CHARTS = (CheckBox) findViewById(R.id.checkboxFOOD_CHARTS);
		checkboxFOOD_CHARTS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxPERIMETER = (CheckBox) findViewById(R.id.checkboxPERIMETER);
		checkboxPERIMETER
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxCOFFEE_BARS = (CheckBox) findViewById(R.id.checkboxCOFFEE_BARS);
		checkboxCOFFEE_BARS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxKITCHEN_Pest_Activity = (CheckBox) findViewById(R.id.checkBoxKITCHEN_Pest_Activity);
		checkBoxKITCHEN_Pest_Activity
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxLANDSCAPE_BEDS = (CheckBox) findViewById(R.id.checkboxLANDSCAPE_BEDS);
		checkboxLANDSCAPE_BEDS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxLAUNDRY = (CheckBox) findViewById(R.id.checkboxLAUNDRY);
		checkboxLAUNDRY
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxHOUSE_KEEPING = (CheckBox) findViewById(R.id.checkboxHOUSE_KEEPING);
		checkboxHOUSE_KEEPING
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxDiningRoom_AreasInspected = (CheckBox) findViewById(R.id.checkboxDiningRoom_AreasInspected);
		checkboxDiningRoom_AreasInspected
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxWarehouseStorage_AreasInspected = (CheckBox) findViewById(R.id.checkboxWarehouseStorage_AreasInspected);
		checkboxWarehouseStorage_AreasInspected
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxMANHOLE_COVERS = (CheckBox) findViewById(R.id.checkboxMANHOLE_COVERS);
		checkboxMANHOLE_COVERS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxWAIT_STATIONS_AREAS_INSPECTED = (CheckBox) findViewById(R.id.checkboxWAIT_STATIONS_AREAS_INSPECTED);
		checkboxWAIT_STATIONS_AREAS_INSPECTED
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxFLY_LIHGTS_AREAS_INSPECTED = (CheckBox) findViewById(R.id.checkboxFLY_LIHGTS_AREAS_INSPECTED);
		checkboxFLY_LIHGTS_AREAS_INSPECTED
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxGREASE_TRAPS = (CheckBox) findViewById(R.id.checkboxGREASE_TRAPS);
		checkboxGREASE_TRAPS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxELEVATOR_PITS = (CheckBox) findViewById(R.id.checkboxELEVATOR_PITS);
		checkboxELEVATOR_PITS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxMECHANICAL_ROOMS = (CheckBox) findViewById(R.id.checkboxMECHANICAL_ROOMS);
		checkboxMECHANICAL_ROOMS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxROOF_TOPS_pestActivity_by_zone = (CheckBox) findViewById(R.id.checkboxROOF_TOPS_pestActivity_by_zone);
		checkboxROOF_TOPS_pestActivity_by_zone
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxPATIENT_ROOMS = (CheckBox) findViewById(R.id.checkboxPATIENT_ROOMS);
		checkboxPATIENT_ROOMS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxNURSES_STATIONS = (CheckBox) findViewById(R.id.checkboxNURSES_STATIONS);
		checkboxNURSES_STATIONS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxDUMPSTER = (CheckBox) findViewById(R.id.checkBoxDUMPSTER);
		checkBoxDUMPSTER
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxREST_ROOMS = (CheckBox) findViewById(R.id.checkboxREST_ROOMS);
		checkboxREST_ROOMS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxPLANTERS_AreasInspected = (CheckBox) findViewById(R.id.checkboxPLANTERS_AreasInspected);
		checkboxPLANTERS_AreasInspected
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxDOCK = (CheckBox) findViewById(R.id.checkboxDOCK);
		checkboxDOCK
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxDISH_WSHING_AREAS_INSPECTED = (CheckBox) findViewById(R.id.checkboxDISH_WSHING_AREAS_INSPECTED);
		checkboxDISH_WSHING_AREAS_INSPECTED
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxDRY_STORAGE = (CheckBox) findViewById(R.id.checkboxDRY_STORAGE);
		checkboxDRY_STORAGE
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxDRY_STORAGE_Pest_Activity = (CheckBox) findViewById(R.id.checkBoxDRY_STORAGE_Pest_Activity);
		checkBoxDRY_STORAGE_Pest_Activity
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxENTRY_POINTS = (CheckBox) findViewById(R.id.checkboxENTRY_POINTS);
		checkboxENTRY_POINTS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxBAR = (CheckBox) findViewById(R.id.checkboxBAR);
		checkboxBAR
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxLOBBY = (CheckBox) findViewById(R.id.checkboxLOBBY);
		checkboxLOBBY
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxWAREHOUSE_STORAGE_PestActivtyByZone = (CheckBox) findViewById(R.id.checkBoxWAREHOUSE_STORAGE_PestActivtyByZone);
		checkBoxWAREHOUSE_STORAGE_PestActivtyByZone
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxRoaches = (CheckBox) findViewById(R.id.checkboxRoaches);
		checkboxRoaches
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxAMERICAN_COCKROACHES = (CheckBox) findViewById(R.id.checkboxAMERICAN_COCKROACHES);
		checkboxAMERICAN_COCKROACHES
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxGERMAN_COCKROACHES = (CheckBox) findViewById(R.id.checkboxGERMAN_COCKROACHES);
		checkboxGERMAN_COCKROACHES
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxSPIDERS = (CheckBox) findViewById(R.id.checkboxSPIDERS);
		checkboxSPIDERS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxSCORPIONS = (CheckBox) findViewById(R.id.checkboxSCORPIONS);
		checkboxSCORPIONS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxSILVER_FISH = (CheckBox) findViewById(R.id.checkboxSILVER_FISH);
		checkboxSILVER_FISH
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxSTORED_PRODUCT_PESTS = (CheckBox) findViewById(R.id.checkboxSTORED_PRODUCT_PESTS);
		checkboxSTORED_PRODUCT_PESTS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxPHARAOH_ANTS = (CheckBox) findViewById(R.id.checkboxPHARAOH_ANTS);
		checkboxPHARAOH_ANTS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxFIRE_ANTS = (CheckBox) findViewById(R.id.checkboxFIRE_ANTS);
		checkboxFIRE_ANTS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxCARPENTER_ANTS = (CheckBox) findViewById(R.id.checkboxCARPENTER_ANTS);
		checkboxCARPENTER_ANTS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxMILLIPEDES = (CheckBox) findViewById(R.id.checkboxMILLIPEDES);
		checkboxMILLIPEDES
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxEAR_WIGS = (CheckBox) findViewById(R.id.checkboxEAR_WIGS);
		checkboxEAR_WIGS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxPILL_BUGS = (CheckBox) findViewById(R.id.checkboxPILL_BUGS);
		checkboxPILL_BUGS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxCRICKETS = (CheckBox) findViewById(R.id.checkboxCRICKETS);
		checkboxCRICKETS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxWASPS = (CheckBox) findViewById(R.id.checkboxWASPS);
		checkboxWASPS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxSUB_TERMITES = (CheckBox) findViewById(R.id.checkboxSUB_TERMITES);
		checkboxSUB_TERMITES
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxDRYWOOD_TERMITIES = (CheckBox) findViewById(R.id.checkboxDRYWOOD_TERMITIES);
		checkboxDRYWOOD_TERMITIES
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxROOF_RATS = (CheckBox) findViewById(R.id.checkboxROOF_RATS);
		checkboxROOF_RATS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxMICE = (CheckBox) findViewById(R.id.checkboxMICE);
		checkboxMICE
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxRACOONS = (CheckBox) findViewById(R.id.checkboxRACOONS);
		checkboxRACOONS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxOPOSSUMS = (CheckBox) findViewById(R.id.checkboxOPOSSUMS);
		checkboxOPOSSUMS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxSKUNKS = (CheckBox) findViewById(R.id.checkboxSKUNKS);
		checkboxSKUNKS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxSQUIRRELS = (CheckBox) findViewById(R.id.checkboxSQUIRRELS);
		checkboxSQUIRRELS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTICKS = (CheckBox) findViewById(R.id.checkBoxTICKS);
		checkBoxTICKS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxBEDBUGS = (CheckBox) findViewById(R.id.checkBoxBEDBUGS);
		checkBoxBEDBUGS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTAWNY_CRAZY_ANTS = (CheckBox) findViewById(R.id.checkBoxTAWNY_CRAZY_ANTS);
		checkBoxTAWNY_CRAZY_ANTS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxANTS = (CheckBox) findViewById(R.id.checkBoxANTS);
		checkBoxANTS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxOTHER_PESTS = (CheckBox) findViewById(R.id.checkBoxOTHER_PESTS);
		checkBoxOTHER_PESTS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxINTERIOR_PEST_ACTIVITY_ZONE = (CheckBox) findViewById(R.id.checkBoxINTERIOR_PEST_ACTIVITY_ZONE);
		checkBoxINTERIOR_PEST_ACTIVITY_ZONE
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxOUTSIDE_PERIMETER = (CheckBox) findViewById(R.id.checkBoxOUTSIDE_PERIMETER);
		checkBoxOUTSIDE_PERIMETER
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxDUMPSTER_pest_activitybyzone = (CheckBox) findViewById(R.id.checkBoxDUMPSTER_pest_activitybyzone);
		checkBoxDUMPSTER_pest_activitybyzone
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxDINING_ROOM_PestActivityByZone = (CheckBox) findViewById(R.id.checkBoxDINING_ROOM_PestActivityByZone);
		checkBoxDINING_ROOM_PestActivityByZone
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxCOMMON_AREAS = (CheckBox) findViewById(R.id.checkBoxCOMMON_AREAS);
		checkBoxCOMMON_AREAS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxKITCHEN_AreasInspected = (CheckBox) findViewById(R.id.checkboxKITCHEN_AreasInspected);
		checkboxKITCHEN_AreasInspected
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		/*
		 * checkBoxDRY_STORAGE = (CheckBox)
		 * findViewById(R.id.checkboxDRY_STORAGE); checkBoxDRY_STORAGE
		 * .setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		 */

		checkBoxDISH_WSHING_PEST_ACTIVTY = (CheckBox) findViewById(R.id.checkBoxDISH_WSHING_PEST_ACTIVTY);
		checkBoxDISH_WSHING_PEST_ACTIVTY
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxROOF_TOPS = (CheckBox) findViewById(R.id.checkBoxROOF_TOPS);
		checkBoxROOF_TOPS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxWAIT_STATION_PEST_ACTIVTY = (CheckBox) findViewById(R.id.checkBoxWAIT_STATION_PEST_ACTIVTY);
		checkBoxWAIT_STATION_PEST_ACTIVTY
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxDROP_CEILINGS = (CheckBox) findViewById(R.id.checkBoxDROP_CEILINGS);
		checkBoxDROP_CEILINGS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxPLANTERS_PestActivtyByZone = (CheckBox) findViewById(R.id.checkBoxPLANTERS_PestActivtyByZone);
		checkBoxPLANTERS_PestActivtyByZone
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxINTERIOR_PEST_CONTROL_SERVICES = (CheckBox) findViewById(R.id.checkBoxINTERIOR_PEST_CONTROL_SERVICES);
		checkBoxINTERIOR_PEST_CONTROL_SERVICES
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTREATED_INSPECT_DROP_CEILINGS = (CheckBox) findViewById(R.id.checkBoxTREATED_INSPECT_DROP_CEILINGS);
		checkBoxTREATED_INSPECT_DROP_CEILINGS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTREATED_KITCHEN = (CheckBox) findViewById(R.id.checkBoxTREATED_KITCHEN);
		checkBoxTREATED_KITCHEN
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTREATED_LAUNDRY = (CheckBox) findViewById(R.id.checkBoxTREATED_LAUNDRY);
		checkBoxTREATED_LAUNDRY
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTREATED_INSPECT_OFFICES = (CheckBox) findViewById(R.id.checkBoxTREATED_INSPECT_OFFICES);
		checkBoxTREATED_INSPECT_OFFICES
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTREATED_INSPECT_ROOF_TOPS = (CheckBox) findViewById(R.id.checkBoxTREATED_INSPECT_ROOF_TOPS);
		checkBoxTREATED_INSPECT_ROOF_TOPS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTREATED_ZONE_MONITORS = (CheckBox) findViewById(R.id.checkBoxTREATED_ZONE_MONITORS);
		checkBoxTREATED_ZONE_MONITORS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTREATED_INSPECT_COMMON_AREAS = (CheckBox) findViewById(R.id.checkBoxTREATED_INSPECT_COMMON_AREAS);
		checkBoxTREATED_INSPECT_COMMON_AREAS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxEXTERIOR_PEST_CONTROL_SERVICES = (CheckBox) findViewById(R.id.checkBoxEXTERIOR_PEST_CONTROL_SERVICES);
		checkBoxEXTERIOR_PEST_CONTROL_SERVICES
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTREATED_fRONT_PERIMETRE = (CheckBox) findViewById(R.id.checkBoxTREATED_fRONT_PERIMETRE);
		checkBoxTREATED_fRONT_PERIMETRE
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTREATED_BACK_PERIMETRE = (CheckBox) findViewById(R.id.checkBoxTREATED_BACK_PERIMETRE);
		checkBoxTREATED_BACK_PERIMETRE
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTREaTED_WHEPHOLES = (CheckBox) findViewById(R.id.checkBoxTREaTED_WHEPHOLES);
		checkBoxTREaTED_WHEPHOLES
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTREATED_CRACKS = (CheckBox) findViewById(R.id.checkBoxTREATED_CRACKS);
		checkBoxTREATED_CRACKS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTREATED_HARBOURAGE_BREEDING_SITES = (CheckBox) findViewById(R.id.checkBoxTREATED_HARBOURAGE_BREEDING_SITES);
		checkBoxTREATED_HARBOURAGE_BREEDING_SITES
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTREATED_FIREANTS_LANDSCAPE = (CheckBox) findViewById(R.id.checkBoxTREATED_FIREANTS_LANDSCAPE);
		checkBoxTREATED_FIREANTS_LANDSCAPE
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTREATED_DOORWAYS_windows = (CheckBox) findViewById(R.id.checkBoxTREATED_DOORWAYS_windows);
		checkBoxTREATED_DOORWAYS_windows
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxVISIBLE_SIGN_TERMITE_MONITORING_STATIONS = (CheckBox) findViewById(R.id.checkBoxVISIBLE_SIGN_TERMITE_MONITORING_STATIONS);
		checkBoxVISIBLE_SIGN_TERMITE_MONITORING_STATIONS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxINSTALLED_MONITORING_STATIONS = (CheckBox) findViewById(R.id.checkBoxINSTALLED_MONITORING_STATIONS);
		checkBoxINSTALLED_MONITORING_STATIONS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxVISIBLE_SIGN_PERIMETER_STRUCTURE = (CheckBox) findViewById(R.id.checkBoxVISIBLE_SIGN_PERIMETER_STRUCTURE);
		checkBoxVISIBLE_SIGN_PERIMETER_STRUCTURE
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxVISIBLE_SIGN_INTERIOR_STRUCTURE = (CheckBox) findViewById(R.id.checkBoxVISIBLE_SIGN_INTERIOR_STRUCTURE);
		checkBoxVISIBLE_SIGN_INTERIOR_STRUCTURE
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxREPLACED_MONITORING_STATIONS = (CheckBox) findViewById(R.id.checkBoxREPLACED_MONITORING_STATIONS);
		checkBoxREPLACED_MONITORING_STATIONS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTREATED_PERIMETER_STRUCTURE = (CheckBox) findViewById(R.id.checkBoxTREATED_PERIMETER_STRUCTURE);
		checkBoxTREATED_PERIMETER_STRUCTURE
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTREATED_SLAB_PENETRATIONS = (CheckBox) findViewById(R.id.checkBoxTREATED_SLAB_PENETRATIONS);
		checkBoxTREATED_SLAB_PENETRATIONS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTREATED_CONDUSIVE_CONDITIONS = (CheckBox) findViewById(R.id.checkBoxTREATED_CONDUSIVE_CONDITIONS);
		checkBoxTREATED_CONDUSIVE_CONDITIONS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxINSPECTED_RODENT_STATIONS = (CheckBox) findViewById(R.id.checkBoxINSPECTED_RODENT_STATIONS);
		checkBoxINSPECTED_RODENT_STATIONS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxCLEANES_RODENT_STATIONS = (CheckBox) findViewById(R.id.checkBoxCLEANES_RODENT_STATIONS);
		checkBoxCLEANES_RODENT_STATIONS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxINSTALLED_RODENT_BAIT_STATIONS = (CheckBox) findViewById(R.id.checkBoxINSTALLED_RODENT_BAIT_STATIONS);
		checkBoxINSTALLED_RODENT_BAIT_STATIONS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxREPLACED_BAIT = (CheckBox) findViewById(R.id.checkBoxREPLACED_BAIT);
		checkBoxREPLACED_BAIT
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxREPLACED_BAIT_STATIONS = (CheckBox) findViewById(R.id.checkBoxREPLACED_BAIT_STATIONS);
		checkBoxREPLACED_BAIT_STATIONS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxINSPECTED_RODENT_TRAPS = (CheckBox) findViewById(R.id.checkBoxINSPECTED_RODENT_TRAPS);
		checkBoxINSPECTED_RODENT_TRAPS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxCLEANED_RODENT_TRAPS = (CheckBox) findViewById(R.id.checkBoxCLEANED_RODENT_TRAPS);
		checkBoxCLEANED_RODENT_TRAPS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxINSTALLED_RODENT_TRAPS = (CheckBox) findViewById(R.id.checkBoxINSTALLED_RODENT_TRAPS);
		checkBoxINSTALLED_RODENT_TRAPS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxREPLACED_RODENT_TRAPS = (CheckBox) findViewById(R.id.checkBoxREPLACED_RODENT_TRAPS);
		checkBoxREPLACED_RODENT_TRAPS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxSEALED_ENTRY_POINTS = (CheckBox) findViewById(R.id.checkBoxSEALED_ENTRY_POINTS);
		checkBoxSEALED_ENTRY_POINTS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxINSPECTED_SNAP_TRAPS = (CheckBox) findViewById(R.id.checkBoxINSPECTED_SNAP_TRAPS);
		checkBoxINSPECTED_SNAP_TRAPS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxINSPECTED_LIVE_CAGES = (CheckBox) findViewById(R.id.checkBoxINSPECTED_LIVE_CAGES);
		checkBoxINSPECTED_LIVE_CAGES
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxREMOVED_SNAP_TRAPS = (CheckBox) findViewById(R.id.checkBoxREMOVED_SNAP_TRAPS);
		checkBoxREMOVED_SNAP_TRAPS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxREMOVED_LIVE_CAGES = (CheckBox) findViewById(R.id.checkBoxREMOVED_LIVE_CAGES);
		checkBoxREMOVED_LIVE_CAGES
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxREMOVED_RODENT_BAIT_STATIONS = (CheckBox) findViewById(R.id.checkBoxREMOVED_RODENT_BAIT_STATIONS);
		checkBoxREMOVED_RODENT_BAIT_STATIONS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxREMOVED_RODENT_TRAPS = (CheckBox) findViewById(R.id.checkBoxREMOVED_RODENT_TRAPS);
		checkBoxREMOVED_RODENT_TRAPS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxSET_SNAP_TRAPS = (CheckBox) findViewById(R.id.checkBoxSET_SNAP_TRAPS);
		checkBoxSET_SNAP_TRAPS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxSET_LIVE_CAGES = (CheckBox) findViewById(R.id.checkBoxSET_LIVE_CAGES);
		checkBoxSET_LIVE_CAGES
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTopped_TANK = (CheckBox) findViewById(R.id.checkBoxTopped_TANK);
		checkBoxTopped_TANK
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxLEACKY_TANK = (CheckBox) findViewById(R.id.checkBoxLEACKY_TANK);
		checkBoxLEACKY_TANK
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTUBING_LEACKY = (CheckBox) findViewById(R.id.checkBoxTUBING_LEACKY);
		checkBoxTUBING_LEACKY
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxINSPECTED_NOZZLES = (CheckBox) findViewById(R.id.checkBoxINSPECTED_NOZZLES);
		checkBoxINSPECTED_NOZZLES
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxRAN_SYATEMS = (CheckBox) findViewById(R.id.checkBoxRAN_SYATEMS);
		checkBoxRAN_SYATEMS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxNOZZLES_LEAKING = (CheckBox) findViewById(R.id.checkBoxNOZZLES_LEAKING);
		checkBoxNOZZLES_LEAKING
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxCHECKED_RUN_DURACTION_TIME = (CheckBox) findViewById(R.id.checkBoxCHECKED_RUN_DURACTION_TIME);
		checkBoxCHECKED_RUN_DURACTION_TIME
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxCHECKED_TIME_SYSTEM_RUNS = (CheckBox) findViewById(R.id.checkBoxCHECKED_TIME_SYSTEM_RUNS);
		checkBoxCHECKED_TIME_SYSTEM_RUNS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxDEBRIS_IN_DRAINS = (CheckBox) findViewById(R.id.checkBoxDEBRIS_IN_DRAINS);
		checkBoxDEBRIS_IN_DRAINS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxINACCSISIBLE_AREAS = (CheckBox) findViewById(R.id.checkBoxINACCSISIBLE_AREAS);
		checkBoxINACCSISIBLE_AREAS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxMOISTURE_WALLS = (CheckBox) findViewById(R.id.checkBoxMOISTURE_WALLS);
		checkBoxMOISTURE_WALLS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxDOCK_DOOR_LEFT_OPEN = (CheckBox) findViewById(R.id.checkBoxDOCK_DOOR_LEFT_OPEN);
		checkboxCommercialPcService
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxDEBRIS_UNDER_COOKLINE = (CheckBox) findViewById(R.id.checkBoxDEBRIS_UNDER_COOKLINE);
		checkBoxDEBRIS_UNDER_COOKLINE
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxDOOR_PROPPED_OPEN = (CheckBox) findViewById(R.id.checkBoxDOOR_PROPPED_OPEN);
		checkBoxDOOR_PROPPED_OPEN
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxTRASH_CANTS_NOT_EMPTIED = (CheckBox) findViewById(R.id.checkBoxTRASH_CANTS_NOT_EMPTIED);
		checkBoxTRASH_CANTS_NOT_EMPTIED
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxWEATHER_STRIPPING_MISSING = (CheckBox) findViewById(R.id.checkBoxWEATHER_STRIPPING_MISSING);
		checkBoxWEATHER_STRIPPING_MISSING
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxSYRUP_SODA = (CheckBox) findViewById(R.id.checkBoxSYRUP_SODA);
		checkBoxSYRUP_SODA
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxAIR_CURTAIN = (CheckBox) findViewById(R.id.checkBoxAIR_CURTAIN);
		checkBoxAIR_CURTAIN
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxDIRTY_DINING_TABLE = (CheckBox) findViewById(R.id.checkBoxDIRTY_DINING_TABLE);
		checkBoxDIRTY_DINING_TABLE
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxFLY_LIGHTS = (CheckBox) findViewById(R.id.checkBoxFLY_LIGHTS);
		checkBoxFLY_LIGHTS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkBoxFILM_BAR_TaPS = (CheckBox) findViewById(R.id.checkBoxFILM_BAR_TaPS);
		checkBoxFILM_BAR_TaPS
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxSET_SNAP_TRAPS1 = (CheckBox) findViewById(R.id.checkboxSET_SNAP_TRAPS1);
		checkboxSET_SNAP_TRAPS1
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxSET_SNAP_TRAPS2 = (CheckBox) findViewById(R.id.checkboxSET_SNAP_TRAPS2);
		checkboxSET_SNAP_TRAPS2
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxSET_SNAP_TRAPS3 = (CheckBox) findViewById(R.id.checkboxSET_SNAP_TRAPS3);
		checkboxSET_SNAP_TRAPS3
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxSET_SNAP_TRAPS4 = (CheckBox) findViewById(R.id.checkboxSET_SNAP_TRAPS4);
		checkboxSET_SNAP_TRAPS4
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxSET_LIVE_CAGES1 = (CheckBox) findViewById(R.id.checkboxSET_LIVE_CAGES1);
		checkboxSET_LIVE_CAGES1
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxSET_LIVE_CAGES2 = (CheckBox) findViewById(R.id.checkboxSET_LIVE_CAGES2);
		checkboxSET_LIVE_CAGES2
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxSET_LIVE_CAGES3 = (CheckBox) findViewById(R.id.checkboxSET_LIVE_CAGES3);
		checkboxSET_LIVE_CAGES3
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		checkboxSET_LIVE_CAGES4 = (CheckBox) findViewById(R.id.checkboxSET_LIVE_CAGES4);
		checkboxSET_LIVE_CAGES4
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());

		// all edit text
		edtiNSPECTEDANDtREATEDFOR1 = (EditText) findViewById(R.id.edtiNSPECTEDANDtREATEDFOR1);
		edtiNSPECTEDANDtREATEDFOR1
				.addTextChangedListener(new CustomTextWatcher(
						edtiNSPECTEDANDtREATEDFOR1));
		edtiNSPECTEDANDtREATEDFOR1.setFilters(CommonFunction.limitchars(20));

		edtiNSPECTEDANDtREATEDFOR2 = (EditText) findViewById(R.id.edtiNSPECTEDANDtREATEDFOR2);
		edtiNSPECTEDANDtREATEDFOR2
				.addTextChangedListener(new CustomTextWatcher(
						edtiNSPECTEDANDtREATEDFOR2));
		edtiNSPECTEDANDtREATEDFOR2.setFilters(CommonFunction.limitchars(20));

		edtINTERIOR = (EditText) findViewById(R.id.edtINTERIOR);
		edtINTERIOR.addTextChangedListener(new CustomTextWatcher(edtINTERIOR));
		edtINTERIOR.setFilters(CommonFunction.limitchars(50));

		edtOUTSIDE_PERIMETER = (EditText) findViewById(R.id.edtOUTSIDE_PERIMETER);
		edtOUTSIDE_PERIMETER.addTextChangedListener(new CustomTextWatcher(
				edtOUTSIDE_PERIMETER));
		edtOUTSIDE_PERIMETER.setFilters(CommonFunction.limitchars(50));

		edtDUMPSTER = (EditText) findViewById(R.id.edtDUMPSTER);
		edtDUMPSTER.addTextChangedListener(new CustomTextWatcher(edtDUMPSTER));
		edtDUMPSTER.setFilters(CommonFunction.limitchars(50));

		edtDINING_ROOM = (EditText) findViewById(R.id.edtDINING_ROOM);
		edtDINING_ROOM.addTextChangedListener(new CustomTextWatcher(
				edtDINING_ROOM));
		edtDINING_ROOM.setFilters(CommonFunction.limitchars(50));

		edtCOMMON_AREAS = (EditText) findViewById(R.id.edtCOMMON_AREAS);
		edtCOMMON_AREAS.addTextChangedListener(new CustomTextWatcher(
				edtCOMMON_AREAS));
		edtCOMMON_AREAS.setFilters(CommonFunction.limitchars(50));

		edtKITCHEN_PestActivityByZone = (EditText) findViewById(R.id.edtKITCHEN_PestActivityByZone);
		edtKITCHEN_PestActivityByZone
				.addTextChangedListener(new CustomTextWatcher(
						edtKITCHEN_PestActivityByZone));
		edtKITCHEN_PestActivityByZone.setFilters(CommonFunction.limitchars(50));

		edtDRY_STORAGE = (EditText) findViewById(R.id.edtDRY_STORAGE);
		edtDRY_STORAGE.addTextChangedListener(new CustomTextWatcher(
				edtDRY_STORAGE));
		edtDRY_STORAGE.setFilters(CommonFunction.limitchars(50));

		edtDISH_WSHING = (EditText) findViewById(R.id.edtDISH_WSHING);
		edtDISH_WSHING.addTextChangedListener(new CustomTextWatcher(
				edtDISH_WSHING));
		edtDISH_WSHING.setFilters(CommonFunction.limitchars(50));

		edtROOF_TOPS = (EditText) findViewById(R.id.edtROOF_TOPS);
		edtROOF_TOPS
				.addTextChangedListener(new CustomTextWatcher(edtROOF_TOPS));
		edtROOF_TOPS.setFilters(CommonFunction.limitchars(50));

		edtWAIT_STATION = (EditText) findViewById(R.id.edtWAIT_STATION);
		edtWAIT_STATION.addTextChangedListener(new CustomTextWatcher(
				edtWAIT_STATION));
		edtWAIT_STATION.setFilters(CommonFunction.limitchars(50));

		edtDROP_CEILINGS = (EditText) findViewById(R.id.edtDROP_CEILINGS);
		edtDROP_CEILINGS.addTextChangedListener(new CustomTextWatcher(
				edtDROP_CEILINGS));
		edtDROP_CEILINGS.setFilters(CommonFunction.limitchars(50));

		edtPLANTERS = (EditText) findViewById(R.id.edtPLANTERS);
		edtPLANTERS.addTextChangedListener(new CustomTextWatcher(edtPLANTERS));
		edtPLANTERS.setFilters(CommonFunction.limitchars(50));

		edtWAREHOUSE_STORAGE = (EditText) findViewById(R.id.edtWAREHOUSE_STORAGE);
		edtWAREHOUSE_STORAGE.addTextChangedListener(new CustomTextWatcher(
				edtWAREHOUSE_STORAGE));
		edtWAREHOUSE_STORAGE.setFilters(CommonFunction.limitchars(50));

		edt_station_VISIBLE_SIGN_TERMITE_MONITORING_STATIONS = (EditText) findViewById(R.id.edt_station_VISIBLE_SIGN_TERMITE_MONITORING_STATIONS);
		edt_station_VISIBLE_SIGN_TERMITE_MONITORING_STATIONS
				.addTextChangedListener(new CustomTextWatcher(
						edt_station_VISIBLE_SIGN_TERMITE_MONITORING_STATIONS));
		edt_station_VISIBLE_SIGN_TERMITE_MONITORING_STATIONS
				.setFilters(CommonFunction.limitchars(50));

		edtINSTALLED_MONITORING_STATIONS = (EditText) findViewById(R.id.edtINSTALLED_MONITORING_STATIONS);
		edtINSTALLED_MONITORING_STATIONS
				.addTextChangedListener(new CustomTextWatcher(
						edtINSTALLED_MONITORING_STATIONS));
		edtINSTALLED_MONITORING_STATIONS.setFilters(CommonFunction
				.limitchars(50));

		edt_how_many_REMOVED_RODENT_TRAPS = (EditText) findViewById(R.id.edt_how_many_REMOVED_RODENT_TRAPS);
		edt_how_many_REMOVED_RODENT_TRAPS
				.addTextChangedListener(new CustomTextWatcher(
						edt_how_many_REMOVED_RODENT_TRAPS));
		edt_how_many_REMOVED_RODENT_TRAPS.setFilters(CommonFunction
				.limitchars(50));

		edtREPLACED_MONITORING_STATIONS = (EditText) findViewById(R.id.edtREPLACED_MONITORING_STATIONS);
		edtREPLACED_MONITORING_STATIONS
				.addTextChangedListener(new CustomTextWatcher(
						edtREPLACED_MONITORING_STATIONS));
		edtREPLACED_MONITORING_STATIONS.setFilters(CommonFunction
				.limitchars(50));

		edt_station_REPLACED_MONITORING_STATIONS = (EditText) findViewById(R.id.edt_station_REPLACED_MONITORING_STATIONS);
		edt_station_REPLACED_MONITORING_STATIONS
				.addTextChangedListener(new CustomTextWatcher(
						edt_station_REPLACED_MONITORING_STATIONS));
		edt_station_REPLACED_MONITORING_STATIONS.setFilters(CommonFunction
				.limitchars(50));

		edt_how_many_INSPECTED_RODENT_STATIONS = (EditText) findViewById(R.id.edt_how_many_INSPECTED_RODENT_STATIONS);
		edt_how_many_INSPECTED_RODENT_STATIONS
				.addTextChangedListener(new CustomTextWatcher(
						edt_how_many_INSPECTED_RODENT_STATIONS));
		edt_how_many_INSPECTED_RODENT_STATIONS.setFilters(CommonFunction
				.limitchars(50));

		edt_how_many_INSTALLED_RODENT_BAIT_STATIONS = (EditText) findViewById(R.id.edt_how_many_INSTALLED_RODENT_BAIT_STATIONS);
		edt_how_many_INSTALLED_RODENT_BAIT_STATIONS
				.addTextChangedListener(new CustomTextWatcher(
						edt_how_many_INSTALLED_RODENT_BAIT_STATIONS));
		edt_how_many_INSTALLED_RODENT_BAIT_STATIONS.setFilters(CommonFunction
				.limitchars(50));

		edt_station_REPLACED_BAIT = (EditText) findViewById(R.id.edt_station_REPLACED_BAIT);
		edt_station_REPLACED_BAIT.addTextChangedListener(new CustomTextWatcher(
				edt_station_REPLACED_BAIT));
		edt_station_REPLACED_BAIT.setFilters(CommonFunction.limitchars(50));

		edt_how_many_REPLACED_BAIT_STATIONS = (EditText) findViewById(R.id.edt_how_many_REPLACED_BAIT_STATIONS);
		edt_how_many_REPLACED_BAIT_STATIONS
				.addTextChangedListener(new CustomTextWatcher(
						edt_how_many_REPLACED_BAIT_STATIONS));
		edt_how_many_REPLACED_BAIT_STATIONS.setFilters(CommonFunction
				.limitchars(50));

		edt_station_REPLACED_BAIT_STATIONS = (EditText) findViewById(R.id.edt_station_REPLACED_BAIT_STATIONS);
		edt_station_REPLACED_BAIT_STATIONS
				.addTextChangedListener(new CustomTextWatcher(
						edt_station_REPLACED_BAIT_STATIONS));
		edt_station_REPLACED_BAIT_STATIONS.setFilters(CommonFunction
				.limitchars(50));

		edt_how_many_REPLACED_RODENT_TRAPS = (EditText) findViewById(R.id.edt_how_many_REPLACED_RODENT_TRAPS);
		edt_how_many_REPLACED_RODENT_TRAPS
				.addTextChangedListener(new CustomTextWatcher(
						edt_how_many_REPLACED_RODENT_TRAPS));
		edt_how_many_REPLACED_RODENT_TRAPS.setFilters(CommonFunction
				.limitchars(50));

		edt_station_REPLACED_RODENT_TRAPS = (EditText) findViewById(R.id.edt_station_REPLACED_RODENT_TRAPS);
		edt_station_REPLACED_RODENT_TRAPS
				.addTextChangedListener(new CustomTextWatcher(
						edt_station_REPLACED_RODENT_TRAPS));
		edt_station_REPLACED_RODENT_TRAPS.setFilters(CommonFunction
				.limitchars(50));

		edt_how_many_INSPECTED_SNAP_TRAPS = (EditText) findViewById(R.id.edt_how_many_INSPECTED_SNAP_TRAPS);
		edt_how_many_INSPECTED_SNAP_TRAPS
				.addTextChangedListener(new CustomTextWatcher(
						edt_how_many_INSPECTED_SNAP_TRAPS));
		edt_how_many_INSPECTED_SNAP_TRAPS.setFilters(CommonFunction
				.limitchars(50));

		edt_how_many_INSPECTED_LIVE_CAGES = (EditText) findViewById(R.id.edt_how_many_INSPECTED_LIVE_CAGES);
		edt_how_many_INSPECTED_LIVE_CAGES
				.addTextChangedListener(new CustomTextWatcher(
						edt_how_many_INSPECTED_LIVE_CAGES));
		edt_how_many_INSPECTED_LIVE_CAGES.setFilters(CommonFunction
				.limitchars(50));

		edt_how_many_REMOVED_SNAP_TRAPS = (EditText) findViewById(R.id.edt_how_many_REMOVED_SNAP_TRAPS);
		edt_how_many_REMOVED_SNAP_TRAPS
				.addTextChangedListener(new CustomTextWatcher(
						edt_how_many_REMOVED_SNAP_TRAPS));
		edt_how_many_REMOVED_SNAP_TRAPS.setFilters(CommonFunction
				.limitchars(50));

		edt_how_many_REMOVED_LIVE_CAGES = (EditText) findViewById(R.id.edt_how_many_REMOVED_LIVE_CAGES);
		edt_how_many_REMOVED_LIVE_CAGES
				.addTextChangedListener(new CustomTextWatcher(
						edt_how_many_REMOVED_LIVE_CAGES));
		edt_how_many_REMOVED_LIVE_CAGES.setFilters(CommonFunction
				.limitchars(50));

		edt_how_many_REMOVED_RODENT_BAIT_STATIONS = (EditText) findViewById(R.id.edt_how_many_REMOVED_RODENT_BAIT_STATIONS);
		edt_how_many_REMOVED_RODENT_BAIT_STATIONS
				.addTextChangedListener(new CustomTextWatcher(
						edt_how_many_REMOVED_RODENT_BAIT_STATIONS));
		edt_how_many_REMOVED_RODENT_BAIT_STATIONS.setFilters(CommonFunction
				.limitchars(50));

		edt_how_many_INSTALLED_RODENT_TRAPS = (EditText) findViewById(R.id.edt_how_many_INSTALLED_RODENT_TRAPS);
		edt_how_many_INSTALLED_RODENT_TRAPS
				.addTextChangedListener(new CustomTextWatcher(
						edt_how_many_INSTALLED_RODENT_TRAPS));
		edt_how_many_INSTALLED_RODENT_TRAPS.setFilters(CommonFunction
				.limitchars(50));

		edt_SET_SNAP_TRAPS = (EditText) findViewById(R.id.edt_SET_SNAP_TRAPS);
		edt_SET_SNAP_TRAPS.addTextChangedListener(new CustomTextWatcher(
				edt_SET_SNAP_TRAPS));
		edt_SET_SNAP_TRAPS.setFilters(CommonFunction.limitchars(50));

		edt_SET_LIVE_CAGES = (EditText) findViewById(R.id.edt_SET_LIVE_CAGES);
		edt_SET_LIVE_CAGES.addTextChangedListener(new CustomTextWatcher(
				edt_SET_LIVE_CAGES));
		edt_SET_LIVE_CAGES.setFilters(CommonFunction.limitchars(50));

		edtMosquitoService1 = (EditText) findViewById(R.id.edtMosquitoService1);
		edtMosquitoService1.addTextChangedListener(new CustomTextWatcher(
				edtMosquitoService1));
		edtMosquitoService1.setFilters(CommonFunction.limitchars(50));

		edtMosquitoService2 = (EditText) findViewById(R.id.edtMosquitoService2);
		edtMosquitoService2.addTextChangedListener(new CustomTextWatcher(
				edtMosquitoService2));
		edtMosquitoService2.setFilters(CommonFunction.limitchars(50));

		edtTechComment = (EditText) findViewById(R.id.edtTechComment);
		edtTechComment.addTextChangedListener(new CustomTextWatcher(
				edtTechComment));
		edtTechComment.setFilters(CommonFunction.limitchars(400));

		edtServiceTech = (EditText) findViewById(R.id.edtServiceTech);
		edtServiceTech.setFilters(CommonFunction.limitchars(50));

		edtEMP = (EditText) findViewById(R.id.edtEMP);
		edtEMP.setFilters(CommonFunction.limitchars(50));

		edtCustomer = (EditText) findViewById(R.id.edtCustomer);
		edtCustomer.setEnabled(false);
		edtCustomer.setFilters(CommonFunction.limitchars(50));

		edtDate = (EditText) findViewById(R.id.edtDate);
		edtDate.setInputType(InputType.TYPE_NULL);
		edtDate.setOnClickListener(new OnButtonClick());

		edtIntValue = (EditText) findViewById(R.id.edtIntValue);
		edtProductvalue = (EditText) findViewById(R.id.edtProductvalue);
		edtTaxvalue = (EditText) findViewById(R.id.edtTaxvalue);
		edtTotal = (EditText) findViewById(R.id.edtTotal);

		edtAmount = (EditText) findViewById(R.id.edtAmount);
		edtAmount.setFilters(new InputFilter[] { new DecimalDigitsInputFilter(
				8, 2) });

		edtCheckNo = (EditText) findViewById(R.id.edtCheckNo);
		edtCheckNo.setFilters(CommonFunction.limitchars(20));

		edtDRIVING_LICENCE = (EditText) findViewById(R.id.edtDRIVING_LICENCE);
		edtDRIVING_LICENCE.setFilters(CommonFunction.limitchars(50));

		edtEXPIRATION_DATE = (EditText) findViewById(R.id.edtEXPIRATION_DATE);
		edtEXPIRATION_DATE.setInputType(InputType.TYPE_NULL);
		edtEXPIRATION_DATE.setOnClickListener(new OnButtonClick());
		// radio group
		radioGroupVISIBLE_SIGN_TERMITE_MONITORING_STATIONS = (RadioGroup) findViewById(R.id.radioGroupVISIBLE_SIGN_TERMITE_MONITORING_STATIONS);
		radioGroupVISIBLE_SIGN_TERMITE_MONITORING_STATIONS
				.setOnCheckedChangeListener(new OnRadioChange());

		radioGroupVISIBLE_SIGN_PERIMETER_STRUCTURE = (RadioGroup) findViewById(R.id.radioGroupVISIBLE_SIGN_PERIMETER_STRUCTURE);
		radioGroupVISIBLE_SIGN_PERIMETER_STRUCTURE
				.setOnCheckedChangeListener(new OnRadioChange());

		radioGroupVISIBLE_SIGN_INTERIOR_STRUCTURE = (RadioGroup) findViewById(R.id.radioGroupVISIBLE_SIGN_INTERIOR_STRUCTURE);
		radioGroupVISIBLE_SIGN_INTERIOR_STRUCTURE
				.setOnCheckedChangeListener(new OnRadioChange());

		radioGroupTREATED_PERIMETER_STRUCTURE = (RadioGroup) findViewById(R.id.radioGroupTREATED_PERIMETER_STRUCTURE);
		radioGroupTREATED_PERIMETER_STRUCTURE
				.setOnCheckedChangeListener(new OnRadioChange());

		radioGroupTREATED_SLAB_PENETRATIONS = (RadioGroup) findViewById(R.id.radioGroupTREATED_SLAB_PENETRATIONS);
		radioGroupTREATED_SLAB_PENETRATIONS
				.setOnCheckedChangeListener(new OnRadioChange());

		radioGroupTREATED_CONDUSIVE_CONDITIONS = (RadioGroup) findViewById(R.id.radioGroupTREATED_CONDUSIVE_CONDITIONS);
		radioGroupTREATED_CONDUSIVE_CONDITIONS
				.setOnCheckedChangeListener(new OnRadioChange());

		// radioGroupINSPECTED_RODENT_STATIONS = (RadioGroup)
		// findViewById(R.id.radioGroupINSPECTED_RODENT_STATIONS);
		// radioGroupINSPECTED_RODENT_STATIONS
		// .setOnCheckedChangeListener(new OnRadioChange());

		radioGroupCLEANES_RODENT_STATIONS = (RadioGroup) findViewById(R.id.radioGroupCLEANES_RODENT_STATIONS);
		radioGroupCLEANES_RODENT_STATIONS
				.setOnCheckedChangeListener(new OnRadioChange());

		radioGroupINSTALLED_RODENT_BAIT_STATIONS = (RadioGroup) findViewById(R.id.radioGroupINSTALLED_RODENT_BAIT_STATIONS);
		radioGroupINSTALLED_RODENT_BAIT_STATIONS
				.setOnCheckedChangeListener(new OnRadioChange());

		radioGroupREPLACED_BAIT = (RadioGroup) findViewById(R.id.radioGroupREPLACED_BAIT);
		radioGroupREPLACED_BAIT.setOnCheckedChangeListener(new OnRadioChange());

		radioGroupREPLACED_BAIT_STATIONS = (RadioGroup) findViewById(R.id.radioGroupREPLACED_BAIT_STATIONS);
		radioGroupREPLACED_BAIT_STATIONS
				.setOnCheckedChangeListener(new OnRadioChange());

		radioGroupINSPECTED_RODENT_TRAPS = (RadioGroup) findViewById(R.id.radioGroupINSPECTED_RODENT_TRAPS);
		radioGroupINSPECTED_RODENT_TRAPS
				.setOnCheckedChangeListener(new OnRadioChange());

		radioGroupCLEANED_RODENT_TRAPS = (RadioGroup) findViewById(R.id.radioGroupCLEANED_RODENT_TRAPS);
		radioGroupCLEANED_RODENT_TRAPS
				.setOnCheckedChangeListener(new OnRadioChange());

		radioGroupINSTALLED_RODENT_TRAPS = (RadioGroup) findViewById(R.id.radioGroupINSTALLED_RODENT_TRAPS);
		radioGroupINSTALLED_RODENT_TRAPS
				.setOnCheckedChangeListener(new OnRadioChange());

		radioGroupSEALED_ENTRY_POINTS = (RadioGroup) findViewById(R.id.radioGroupSEALED_ENTRY_POINTS);
		radioGroupSEALED_ENTRY_POINTS
				.setOnCheckedChangeListener(new OnRadioChange());

		radioGroupInvoice = (RadioGroup) findViewById(R.id.radioGroupInvoice);
		radioGroupInvoice.setOnCheckedChangeListener(new OnRadioChange());

		radioGroupREPLACED_RODENT_TRAPS = (RadioGroup) findViewById(R.id.radioGroupREPLACED_RODENT_TRAPS);
		radioGroupREPLACED_RODENT_TRAPS
				.setOnCheckedChangeListener(new OnRadioChange());

		// radio buttons
		radiobtnVISIBLE_SIGN_TERMITE_MONITORING_STATIONS1 = (RadioButton) findViewById(R.id.radiobtnVISIBLE_SIGN_TERMITE_MONITORING_STATIONS1);
		radiobtnVISIBLE_SIGN_TERMITE_MONITORING_STATIONS2 = (RadioButton) findViewById(R.id.radiobtnVISIBLE_SIGN_TERMITE_MONITORING_STATIONS2);
		radiobtnVISIBLE_SIGN_PERIMETER_STRUCTURE1 = (RadioButton) findViewById(R.id.radiobtnVISIBLE_SIGN_PERIMETER_STRUCTURE1);
		radiobtnVISIBLE_SIGN_PERIMETER_STRUCTURE2 = (RadioButton) findViewById(R.id.radiobtnVISIBLE_SIGN_PERIMETER_STRUCTURE2);
		radiobtnVISIBLE_SIGN_INTERIOR_STRUCTURE1 = (RadioButton) findViewById(R.id.radiobtnVISIBLE_SIGN_INTERIOR_STRUCTURE1);
		radiobtnVISIBLE_SIGN_INTERIOR_STRUCTURE2 = (RadioButton) findViewById(R.id.radiobtnVISIBLE_SIGN_INTERIOR_STRUCTURE2);
		radiobtnTREATED_PERIMETER_STRUCTURE1 = (RadioButton) findViewById(R.id.radiobtnTREATED_PERIMETER_STRUCTURE1);
		radiobtnTREATED_PERIMETER_STRUCTURE2 = (RadioButton) findViewById(R.id.radiobtnTREATED_PERIMETER_STRUCTURE2);
		radiobtnTREATED_SLAB_PENETRATIONS1 = (RadioButton) findViewById(R.id.radiobtnTREATED_SLAB_PENETRATIONS1);
		radiobtnTREATED_SLAB_PENETRATIONS2 = (RadioButton) findViewById(R.id.radiobtnTREATED_SLAB_PENETRATIONS2);
		radiobtnTREATED_CONDUSIVE_CONDITIONS1 = (RadioButton) findViewById(R.id.radiobtnTREATED_CONDUSIVE_CONDITIONS1);
		radiobtnTREATED_CONDUSIVE_CONDITIONS2 = (RadioButton) findViewById(R.id.radiobtnTREATED_CONDUSIVE_CONDITIONS2);
		radiobtnREPLACED_RODENT_TRAPS1 = (RadioButton) findViewById(R.id.radiobtnREPLACED_RODENT_TRAPS1);
		radiobtnREPLACED_RODENT_TRAPS2 = (RadioButton) findViewById(R.id.radiobtnREPLACED_RODENT_TRAPS2);
		// radiobtnINSPECTED_RODENT_STATIONS1 = (RadioButton)
		// findViewById(R.id.radiobtnINSPECTED_RODENT_STATIONS1);
		// radiobtnINSPECTED_RODENT_STATIONS2 = (RadioButton)
		// findViewById(R.id.radiobtnINSPECTED_RODENT_STATIONS2);
		radiobtnCLEANES_RODENT_STATIONS1 = (RadioButton) findViewById(R.id.radiobtnCLEANES_RODENT_STATIONS1);
		radiobtnCLEANES_RODENT_STATIONS2 = (RadioButton) findViewById(R.id.radiobtnCLEANES_RODENT_STATIONS2);
		radiobtnINSTALLED_RODENT_BAIT_STATIONS1 = (RadioButton) findViewById(R.id.radiobtnINSTALLED_RODENT_BAIT_STATIONS1);
		radiobtnINSTALLED_RODENT_BAIT_STATIONS2 = (RadioButton) findViewById(R.id.radiobtnINSTALLED_RODENT_BAIT_STATIONS2);
		radiobtnREPLACED_BAIT1 = (RadioButton) findViewById(R.id.radiobtnREPLACED_BAIT1);
		radiobtnREPLACED_BAIT2 = (RadioButton) findViewById(R.id.radiobtnREPLACED_BAIT2);
		radiobtnREPLACED_BAIT_STATIONS1 = (RadioButton) findViewById(R.id.radiobtnREPLACED_BAIT_STATIONS1);
		radiobtnREPLACED_BAIT_STATIONS2 = (RadioButton) findViewById(R.id.radiobtnREPLACED_BAIT_STATIONS2);
		radiobtnINSPECTED_RODENT_TRAPS1 = (RadioButton) findViewById(R.id.radiobtnINSPECTED_RODENT_TRAPS1);
		radiobtnINSPECTED_RODENT_TRAPS2 = (RadioButton) findViewById(R.id.radiobtnINSPECTED_RODENT_TRAPS2);
		radiobtnCLEANED_RODENT_TRAPS1 = (RadioButton) findViewById(R.id.radiobtnCLEANED_RODENT_TRAPS1);
		radiobtnCLEANED_RODENT_TRAPS2 = (RadioButton) findViewById(R.id.radiobtnCLEANED_RODENT_TRAPS2);
		radiobtnINSTALLED_RODENT_TRAPS1 = (RadioButton) findViewById(R.id.radiobtnINSTALLED_RODENT_TRAPS1);
		radiobtnINSTALLED_RODENT_TRAPS2 = (RadioButton) findViewById(R.id.radiobtnINSTALLED_RODENT_TRAPS2);
		radiobtnSEALED_ENTRY_POINTS1 = (RadioButton) findViewById(R.id.radiobtnSEALED_ENTRY_POINTS1);
		radiobtnSEALED_ENTRY_POINTS2 = (RadioButton) findViewById(R.id.radiobtnSEALED_ENTRY_POINTS2);

		radioCash = (RadioButton) findViewById(R.id.radioCash);
		radioCheck = (RadioButton) findViewById(R.id.radioCheck);
		radioCreditcard = (RadioButton) findViewById(R.id.radioCreditcard);
		radioBillLater = (RadioButton) findViewById(R.id.radioBillLater);
		radioPreBill = (RadioButton) findViewById(R.id.radioPreBill);
		radioNocharge = (RadioButton) findViewById(R.id.radioNocharge);

		// all linear layouts
		mainLayout = (LinearLayout) findViewById(R.id.mainLayout);
		LinearlayoutCHECK_IMAGE = (LinearLayout) findViewById(R.id.LinearlayoutCHECK_IMAGE);
		LinearlayoutCHECK_FRONT_IMAGE = (LinearLayout) findViewById(R.id.LinearlayoutCHECK_FRONT_IMAGE);
		LinearlayoutCHECK_BACK_IMAGE = (LinearLayout) findViewById(R.id.LinearlayoutCHECK_BACK_IMAGE);
		trExpirationDateValidation = (LinearLayout) findViewById(R.id.trExpirationDateValidation);

		// buttons
		btnCHECK_FRONT_IMAGE = (Button) findViewById(R.id.btnCHECK_FRONT_IMAGE);
		btnCHECK_FRONT_IMAGE.setOnClickListener(new OnButtonClick());

		btnCHECK_BACK_IMAGE = (Button) findViewById(R.id.btnCHECK_BACK_IMAGE);
		btnCHECK_BACK_IMAGE.setOnClickListener(new OnButtonClick());

		btnsaveandcontinueINVOICE = (Button) findViewById(R.id.btnsaveandcontinueINVOICE);
		btnsaveandcontinueINVOICE.setOnClickListener(new OnButtonClick());

		// Table row
		tramount = (TableRow) findViewById(R.id.tramount);
		trcheckno = (TableRow) findViewById(R.id.trcheckno);
		trDrivingLicence = (TableRow) findViewById(R.id.trDrivingLicence);
		trExpirationDate = (TableRow) findViewById(R.id.trExpirationDate);
		tablerowCharactersTyped = (TableRow) findViewById(R.id.tablerowCharactersTyped);

		tableRowTREATED_CONDUSIVE_CONDITIONS = (TableRow) findViewById(R.id.tableRowTREATED_CONDUSIVE_CONDITIONS);
		tableRowTREATED_SLAB_PENETRATIONS = (TableRow) findViewById(R.id.tableRowTREATED_SLAB_PENETRATIONS);
		tableRowTREATED_PERIMETER_STRUCTURE = (TableRow) findViewById(R.id.tableRowTREATED_PERIMETER_STRUCTURE);
		tableRowREPLACED_MONITORING_STATIONS2 = (TableRow) findViewById(R.id.tableRowREPLACED_MONITORING_STATIONS2);
		tableRowREPLACED_MONITORING_STATIONS1 = (TableRow) findViewById(R.id.tableRowREPLACED_MONITORING_STATIONS1);
		tableRowVISIBLE_SIGN_INTERIOR_STRUCTURE = (TableRow) findViewById(R.id.tableRowVISIBLE_SIGN_INTERIOR_STRUCTURE);
		tableRowVISIBLE_SIGN_PERIMETER_STRUCTURE = (TableRow) findViewById(R.id.tableRowVISIBLE_SIGN_PERIMETER_STRUCTURE);
		tableRowINSTALLED_MONITORING_STATIONS = (TableRow) findViewById(R.id.tableRowINSTALLED_MONITORING_STATIONS);
		tableRowVISIBLE_SIGN_TERMITE_MONITORING_STATIONS2 = (TableRow) findViewById(R.id.tableRowVISIBLE_SIGN_TERMITE_MONITORING_STATIONS2);
		tableRowVISIBLE_SIGN_TERMITE_MONITORING_STATIONS1 = (TableRow) findViewById(R.id.tableRowVISIBLE_SIGN_TERMITE_MONITORING_STATIONS1);

		tableRow_INSPECTED_RODENT_STATIONS = (TableRow) findViewById(R.id.tableRow_INSPECTED_RODENT_STATIONS);
		tableRow_CLEANES_RODENT_STATIONS = (TableRow) findViewById(R.id.tableRow_CLEANES_RODENT_STATIONS);
		tableRow_INSTALLED_RODENT_BAIT_STATIONS1 = (TableRow) findViewById(R.id.tableRow_INSTALLED_RODENT_BAIT_STATIONS1);
		tableRow_INSTALLED_RODENT_BAIT_STATIONS2 = (TableRow) findViewById(R.id.tableRow_INSTALLED_RODENT_BAIT_STATIONS2);
		tableRow_REPLACED_BAIT1 = (TableRow) findViewById(R.id.tableRow_REPLACED_BAIT1);
		tableRow_REPLACED_BAIT2 = (TableRow) findViewById(R.id.tableRow_REPLACED_BAIT2);
		tableRow_REPLACED_BAIT_STATIONS1 = (TableRow) findViewById(R.id.tableRow_REPLACED_BAIT_STATIONS1);
		tableRow_REPLACED_BAIT_STATIONS2 = (TableRow) findViewById(R.id.tableRow_REPLACED_BAIT_STATIONS2);
		tableRow_REPLACED_BAIT_STATIONS3 = (TableRow) findViewById(R.id.tableRow_REPLACED_BAIT_STATIONS3);
		tableRow_INSPECTED_RODENT_TRAPS = (TableRow) findViewById(R.id.tableRow_INSPECTED_RODENT_TRAPS);
		tableRow_CLEANED_RODENT_TRAPS = (TableRow) findViewById(R.id.tableRow_CLEANED_RODENT_TRAPS);
		tableRow_INSTALLED_RODENT_TRAPS1 = (TableRow) findViewById(R.id.tableRow_INSTALLED_RODENT_TRAPS1);
		tableRow_INSTALLED_RODENT_TRAPS2 = (TableRow) findViewById(R.id.tableRow_INSTALLED_RODENT_TRAPS2);
		tableRow_REPLACED_RODENT_TRAPS1 = (TableRow) findViewById(R.id.tableRow_REPLACED_RODENT_TRAPS1);
		tableRow_REPLACED_RODENT_TRAPS2 = (TableRow) findViewById(R.id.tableRow_REPLACED_RODENT_TRAPS2);
		tableRow_REPLACED_RODENT_TRAPS3 = (TableRow) findViewById(R.id.tableRow_REPLACED_RODENT_TRAPS3);
		tableRow_SEALED_ENTRY_POINTS = (TableRow) findViewById(R.id.tableRow_SEALED_ENTRY_POINTS);
		tableRow_INSPECTED_SNAP_TRAPS = (TableRow) findViewById(R.id.tableRow_INSPECTED_SNAP_TRAPS);
		tableRow_INSPECTED_LIVE_CAGES = (TableRow) findViewById(R.id.tableRow_INSPECTED_LIVE_CAGES);
		tableRow_REMOVED_SNAP_TRAPS = (TableRow) findViewById(R.id.tableRow_REMOVED_SNAP_TRAPS);
		tableRow_REMOVED_LIVE_CAGES = (TableRow) findViewById(R.id.tableRow_REMOVED_LIVE_CAGES);
		tableRow_REMOVED_RODENT_BAIT_STATIONS = (TableRow) findViewById(R.id.tableRow_REMOVED_RODENT_BAIT_STATIONS);
		tableRow_REMOVED_RODENT_TRAPS = (TableRow) findViewById(R.id.tableRow_REMOVED_RODENT_TRAPS);
		tableRow_SET_SNAP_TRAPS1 = (TableRow) findViewById(R.id.tableRow_SET_SNAP_TRAPS1);
		tableRow_SET_SNAP_TRAPS2 = (TableRow) findViewById(R.id.tableRow_SET_SNAP_TRAPS2);
		tableRow_SET_SNAP_TRAPS3 = (TableRow) findViewById(R.id.tableRow_SET_SNAP_TRAPS3);
		// tableRow_SET_SNAP_TRAPS4= (TableRow)
		// findViewById(R.id.tableRow_SET_SNAP_TRAPS4);
		tableRow_SET_LIVE_CAGES1 = (TableRow) findViewById(R.id.tableRow_SET_LIVE_CAGES1);
		tableRow_SET_LIVE_CAGES2 = (TableRow) findViewById(R.id.tableRow_SET_LIVE_CAGES2);
		tableRow_SET_LIVE_CAGES3 = (TableRow) findViewById(R.id.tableRow_SET_LIVE_CAGES3);
		// tableRow_SET_LIVE_CAGES4= (TableRow)
		// findViewById(R.id.tableRow_SET_LIVE_CAGES4);
		// horizontal list view
		listImg_front = (HorizontalListView) findViewById(R.id.listImg_front);
		listImg_back = (HorizontalListView) findViewById(R.id.listImg_back);

	}

	private class CustomTextWatcher implements TextWatcher {
		private View view;

		private CustomTextWatcher(View view) {
			this.view = view;
		}

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			String newString = s.toString();

			switch (view.getId()) {

			case R.id.edtTechComment:
				LoggerUtil.e("edtTechComment", "");
				if (newString.length() > 0) {
					LoggerUtil.e("newString.length()", ""
							+ edtTechComment.getText().toString().length());
					textviewCharactersTyped.setText(edtTechComment.getText()
							.toString().length()
							+ " " + "Characters typed");
				}
				/*
				 * if (newString.equals("")) { //
				 * LoggerUtil.e("checked newString","false");
				 * checkBoxREMOVED_RODENT_TRAPS.setChecked(false);
				 * 
				 * } else { // LoggerUtil.e("checked newString","true");
				 * checkBoxREMOVED_RODENT_TRAPS.setChecked(true); }
				 */
				break;

			case R.id.edt_how_many_REMOVED_RODENT_TRAPS:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					checkBoxREMOVED_RODENT_TRAPS.setChecked(false);

				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxREMOVED_RODENT_TRAPS.setChecked(true);
				}
				break;

			case R.id.edtMosquitoService1:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					checkBoxCHECKED_RUN_DURACTION_TIME.setChecked(false);

				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxCHECKED_RUN_DURACTION_TIME.setChecked(true);
				}
				break;
			case R.id.edtMosquitoService2:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					checkBoxCHECKED_TIME_SYSTEM_RUNS.setChecked(false);

				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxCHECKED_TIME_SYSTEM_RUNS.setChecked(true);
				}
				break;
			case R.id.edtiNSPECTEDANDtREATEDFOR1:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					checkBoxANTS.setChecked(false);

				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxANTS.setChecked(true);
				}
				break;
			case R.id.edtiNSPECTEDANDtREATEDFOR2:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					checkBoxOTHER_PESTS.setChecked(false);

				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxOTHER_PESTS.setChecked(true);
				}
				break;
			case R.id.edtINTERIOR:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					checkBoxINTERIOR_PEST_ACTIVITY_ZONE.setChecked(false);

				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxINTERIOR_PEST_ACTIVITY_ZONE.setChecked(true);
				}
				break;
			case R.id.edtOUTSIDE_PERIMETER:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					checkBoxOUTSIDE_PERIMETER.setChecked(false);

				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxOUTSIDE_PERIMETER.setChecked(true);
				}
				break;
			case R.id.edtDUMPSTER:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					checkBoxDUMPSTER_pest_activitybyzone.setChecked(false);

				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxDUMPSTER_pest_activitybyzone.setChecked(true);
				}
				break;
			case R.id.edtDINING_ROOM:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					checkBoxDINING_ROOM_PestActivityByZone.setChecked(false);

				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxDINING_ROOM_PestActivityByZone.setChecked(true);
				}
				break;
			case R.id.edtCOMMON_AREAS:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					checkBoxCOMMON_AREAS.setChecked(false);

				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxCOMMON_AREAS.setChecked(true);
				}
				break;
			case R.id.edtKITCHEN_PestActivityByZone:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					checkBoxKITCHEN_Pest_Activity.setChecked(false);

				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxKITCHEN_Pest_Activity.setChecked(true);
				}
				break;

			case R.id.edtDRY_STORAGE:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					checkBoxDRY_STORAGE_Pest_Activity.setChecked(false);

				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxDRY_STORAGE_Pest_Activity.setChecked(true);
				}
				break;
			case R.id.edtDISH_WSHING:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					checkBoxDISH_WSHING_PEST_ACTIVTY.setChecked(false);

				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxDISH_WSHING_PEST_ACTIVTY.setChecked(true);
				}
				break;
			case R.id.edtROOF_TOPS:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					checkboxROOF_TOPS_pestActivity_by_zone.setChecked(false);

				} else {
					// LoggerUtil.e("checked newString","true");
					checkboxROOF_TOPS_pestActivity_by_zone.setChecked(true);
				}
				break;
			case R.id.edtWAIT_STATION:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					checkBoxWAIT_STATION_PEST_ACTIVTY.setChecked(false);

				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxWAIT_STATION_PEST_ACTIVTY.setChecked(true);
				}
				break;
			case R.id.edtDROP_CEILINGS:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					checkBoxDROP_CEILINGS.setChecked(false);

				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxDROP_CEILINGS.setChecked(true);
				}
				break;
			case R.id.edtPLANTERS:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					checkBoxPLANTERS_PestActivtyByZone.setChecked(false);

				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxPLANTERS_PestActivtyByZone.setChecked(true);
				}
				break;
			case R.id.edtWAREHOUSE_STORAGE:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					checkBoxWAREHOUSE_STORAGE_PestActivtyByZone
							.setChecked(false);

				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxWAREHOUSE_STORAGE_PestActivtyByZone
							.setChecked(true);
				}
				break;
			case R.id.edt_station_VISIBLE_SIGN_TERMITE_MONITORING_STATIONS:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					if (radioGroupVISIBLE_SIGN_TERMITE_MONITORING_STATIONS
							.getCheckedRadioButtonId() == -1) {
						// no radio buttons are checked
						checkBoxVISIBLE_SIGN_TERMITE_MONITORING_STATIONS
								.setChecked(false);
					}
				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxVISIBLE_SIGN_TERMITE_MONITORING_STATIONS
							.setChecked(true);
				}
				break;

			case R.id.edtINSTALLED_MONITORING_STATIONS:

				if (newString.equals("")) {
					LoggerUtil.e("checked newString","false");
					checkBoxINSTALLED_MONITORING_STATIONS.setChecked(false);

				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxINSTALLED_MONITORING_STATIONS.setChecked(true);
				}
				break;

			case R.id.edt_station_REPLACED_MONITORING_STATIONS:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					if (CommonFunction.checkString(
							edtREPLACED_MONITORING_STATIONS.getText()
									.toString(), "").equals("")) {
						checkBoxREPLACED_MONITORING_STATIONS.setChecked(false);
					}
				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxREPLACED_MONITORING_STATIONS.setChecked(true);
				}
				break;
			case R.id.edtREPLACED_MONITORING_STATIONS:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					if (CommonFunction.checkString(
							edt_station_REPLACED_MONITORING_STATIONS.getText()
									.toString(), "").equals("")) {
						checkBoxREPLACED_MONITORING_STATIONS.setChecked(false);
					}
				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxREPLACED_MONITORING_STATIONS.setChecked(true);
				}
				break;

			case R.id.edt_how_many_INSPECTED_RODENT_STATIONS:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");

					checkBoxINSPECTED_RODENT_STATIONS.setChecked(false);

				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxINSPECTED_RODENT_STATIONS.setChecked(true);
				}
				break;
			case R.id.edt_how_many_INSTALLED_RODENT_BAIT_STATIONS:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					if (radioGroupINSTALLED_RODENT_BAIT_STATIONS
							.getCheckedRadioButtonId() == -1) {
						checkBoxINSTALLED_RODENT_BAIT_STATIONS
								.setChecked(false);
					}
				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxINSTALLED_RODENT_BAIT_STATIONS.setChecked(true);
				}
				break;
			case R.id.edt_station_REPLACED_BAIT:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					if (radioGroupREPLACED_BAIT.getCheckedRadioButtonId() == -1) {
						checkBoxREPLACED_BAIT.setChecked(false);
					}
				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxREPLACED_BAIT.setChecked(true);
				}
				break;
			case R.id.edt_station_REPLACED_BAIT_STATIONS:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					if (radioGroupREPLACED_BAIT_STATIONS
							.getCheckedRadioButtonId() == -1
							&& edt_how_many_REPLACED_BAIT_STATIONS.equals("")) {
						checkBoxREPLACED_BAIT_STATIONS.setChecked(false);
					}
				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxREPLACED_BAIT_STATIONS.setChecked(true);
				}
				break;

			case R.id.edt_how_many_REPLACED_BAIT_STATIONS:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					if (radioGroupREPLACED_BAIT_STATIONS
							.getCheckedRadioButtonId() == -1
							&& edt_station_REPLACED_BAIT_STATIONS.equals("")) {
						checkBoxREPLACED_BAIT_STATIONS.setChecked(false);
					}
				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxREPLACED_BAIT_STATIONS.setChecked(true);
				}
				break;

			case R.id.edt_how_many_REPLACED_RODENT_TRAPS:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					if (radioGroupREPLACED_RODENT_TRAPS
							.getCheckedRadioButtonId() == -1
							&& edt_station_REPLACED_RODENT_TRAPS.equals("")) {
						checkBoxREPLACED_RODENT_TRAPS.setChecked(false);
					}
				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxREPLACED_RODENT_TRAPS.setChecked(true);
				}
				break;
			case R.id.edt_station_REPLACED_RODENT_TRAPS:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					if (radioGroupREPLACED_RODENT_TRAPS
							.getCheckedRadioButtonId() == -1
							&& edt_how_many_REPLACED_RODENT_TRAPS.equals("")) {
						checkBoxREPLACED_RODENT_TRAPS.setChecked(false);
					}
				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxREPLACED_RODENT_TRAPS.setChecked(true);
				}
				break;
			case R.id.edt_how_many_INSTALLED_RODENT_TRAPS:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					if (radioGroupINSTALLED_RODENT_TRAPS
							.getCheckedRadioButtonId() == -1) {
						checkBoxINSTALLED_RODENT_TRAPS.setChecked(false);
					}
				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxINSTALLED_RODENT_TRAPS.setChecked(true);
				}
				break;
			case R.id.edt_how_many_INSPECTED_SNAP_TRAPS:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					{
						checkBoxINSPECTED_SNAP_TRAPS.setChecked(false);
					}
				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxINSPECTED_SNAP_TRAPS.setChecked(true);
				}
				break;
			case R.id.edt_how_many_INSPECTED_LIVE_CAGES:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					{
						checkBoxINSPECTED_LIVE_CAGES.setChecked(false);
					}
				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxINSPECTED_LIVE_CAGES.setChecked(true);
				}
				break;
			case R.id.edt_how_many_REMOVED_SNAP_TRAPS:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					{
						checkBoxREMOVED_SNAP_TRAPS.setChecked(false);
					}
				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxREMOVED_SNAP_TRAPS.setChecked(true);
				}
				break;
			case R.id.edt_how_many_REMOVED_LIVE_CAGES:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					{
						checkBoxREMOVED_LIVE_CAGES.setChecked(false);
					}
				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxREMOVED_LIVE_CAGES.setChecked(true);
				}
				break;
			case R.id.edt_how_many_REMOVED_RODENT_BAIT_STATIONS:

				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");
					{
						checkBoxREMOVED_RODENT_BAIT_STATIONS.setChecked(false);
					}
				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxREMOVED_RODENT_BAIT_STATIONS.setChecked(true);
				}
				break;

			case R.id.edt_SET_SNAP_TRAPS:
				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");

					if (!checkboxSET_SNAP_TRAPS1.isChecked()
							&& !checkboxSET_SNAP_TRAPS2.isChecked()
							&& !checkboxSET_SNAP_TRAPS3.isChecked()
							&& !checkboxSET_SNAP_TRAPS4.isChecked()) {
						checkBoxSET_SNAP_TRAPS.setChecked(false);
					}
				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxSET_SNAP_TRAPS.setChecked(true);
				}
				break;

			case R.id.edt_SET_LIVE_CAGES:
				if (newString.equals("")) {
					// LoggerUtil.e("checked newString","false");

					if (!checkboxSET_LIVE_CAGES1.isChecked()
							&& !checkboxSET_LIVE_CAGES2.isChecked()
							&& !checkboxSET_LIVE_CAGES3.isChecked()
							&& !checkboxSET_LIVE_CAGES4.isChecked()) {
						checkBoxSET_LIVE_CAGES.setChecked(false);
					}
				} else {
					// LoggerUtil.e("checked newString","true");
					checkBoxSET_LIVE_CAGES.setChecked(true);
				}
				break;
			default:
				break;
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			/*
			 * System.out.println("1wf"+s); if(s.equals(" ")) {
			 * checkBoxFrontyard.setChecked(false); } if(!s.equals(" ")) {
			 * checkBoxFrontyard.setChecked(true); }
			 */
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			/*
			 * System.out.println("1wf..."+s); if(s.equals(" ")) {
			 * checkBoxFrontyard.setChecked(false); } if(!s.equals(" ")) {
			 * checkBoxFrontyard.setChecked(true); }
			 */
		}
	}

	class OnRadioChange implements RadioGroup.OnCheckedChangeListener {
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId)

		{
			// Log.e("", "inside radio click");
			// TODO Auto-generated method stub
			switch (checkedId) {

			case R.id.radiobtnVISIBLE_SIGN_TERMITE_MONITORING_STATIONS1:
				if (radiobtnVISIBLE_SIGN_TERMITE_MONITORING_STATIONS1
						.isChecked()) {
					checkBoxVISIBLE_SIGN_TERMITE_MONITORING_STATIONS
							.setChecked(true);
				} else {
					if (CommonFunction.checkString(
							edt_station_VISIBLE_SIGN_TERMITE_MONITORING_STATIONS
									.getText().toString(), "").equals("")) {
						checkBoxVISIBLE_SIGN_TERMITE_MONITORING_STATIONS
								.setChecked(false);
					}
				}

				break;
			case R.id.radiobtnVISIBLE_SIGN_TERMITE_MONITORING_STATIONS2:
				if (radiobtnVISIBLE_SIGN_TERMITE_MONITORING_STATIONS2
						.isChecked()) {
					checkBoxVISIBLE_SIGN_TERMITE_MONITORING_STATIONS
							.setChecked(true);
				} else {
					if (CommonFunction.checkString(
							edt_station_VISIBLE_SIGN_TERMITE_MONITORING_STATIONS
									.getText().toString(), "").equals("")) {
						checkBoxVISIBLE_SIGN_TERMITE_MONITORING_STATIONS
								.setChecked(false);
					}
				}

				break;

			case R.id.radiobtnVISIBLE_SIGN_PERIMETER_STRUCTURE1:
				if (radiobtnVISIBLE_SIGN_PERIMETER_STRUCTURE1.isChecked()) {
					checkBoxVISIBLE_SIGN_PERIMETER_STRUCTURE.setChecked(true);
				} else {
					checkBoxVISIBLE_SIGN_PERIMETER_STRUCTURE.setChecked(false);
				}

				break;
			case R.id.radiobtnVISIBLE_SIGN_PERIMETER_STRUCTURE2:
				if (radiobtnVISIBLE_SIGN_PERIMETER_STRUCTURE2.isChecked()) {
					checkBoxVISIBLE_SIGN_PERIMETER_STRUCTURE.setChecked(true);
				} else {
					checkBoxVISIBLE_SIGN_PERIMETER_STRUCTURE.setChecked(false);
				}
			case R.id.radiobtnVISIBLE_SIGN_INTERIOR_STRUCTURE1:
				if (radiobtnVISIBLE_SIGN_INTERIOR_STRUCTURE1.isChecked()) {
					checkBoxVISIBLE_SIGN_INTERIOR_STRUCTURE.setChecked(true);
				} else {
					checkBoxVISIBLE_SIGN_INTERIOR_STRUCTURE.setChecked(false);
				}
				break;
			case R.id.radiobtnVISIBLE_SIGN_INTERIOR_STRUCTURE2:
				if (radiobtnVISIBLE_SIGN_INTERIOR_STRUCTURE2.isChecked()) {
					checkBoxVISIBLE_SIGN_INTERIOR_STRUCTURE.setChecked(true);
				} else {
					checkBoxVISIBLE_SIGN_INTERIOR_STRUCTURE.setChecked(false);
				}
				break;
			case R.id.radiobtnTREATED_PERIMETER_STRUCTURE1:
				if (radiobtnTREATED_PERIMETER_STRUCTURE1.isChecked()) {
					checkBoxTREATED_PERIMETER_STRUCTURE.setChecked(true);
				} else {
					checkBoxTREATED_PERIMETER_STRUCTURE.setChecked(false);
				}
				break;
			case R.id.radiobtnTREATED_PERIMETER_STRUCTURE2:
				if (radiobtnTREATED_PERIMETER_STRUCTURE2.isChecked()) {
					checkBoxTREATED_PERIMETER_STRUCTURE.setChecked(true);
				} else {
					checkBoxTREATED_PERIMETER_STRUCTURE.setChecked(false);
				}
				break;
			case R.id.radiobtnTREATED_SLAB_PENETRATIONS1:
				if (radiobtnTREATED_SLAB_PENETRATIONS1.isChecked()) {
					checkBoxTREATED_SLAB_PENETRATIONS.setChecked(true);
				} else {
					checkBoxTREATED_SLAB_PENETRATIONS.setChecked(false);
				}
				break;
			case R.id.radiobtnTREATED_SLAB_PENETRATIONS2:
				if (radiobtnTREATED_SLAB_PENETRATIONS2.isChecked()) {
					checkBoxTREATED_SLAB_PENETRATIONS.setChecked(true);
				} else {
					checkBoxTREATED_SLAB_PENETRATIONS.setChecked(false);
				}
				break;
			case R.id.radiobtnTREATED_CONDUSIVE_CONDITIONS1:
				if (radiobtnTREATED_CONDUSIVE_CONDITIONS1.isChecked()) {
					checkBoxTREATED_CONDUSIVE_CONDITIONS.setChecked(true);
				} else {
					checkBoxTREATED_CONDUSIVE_CONDITIONS.setChecked(false);
				}
				break;

			case R.id.radiobtnTREATED_CONDUSIVE_CONDITIONS2:
				if (radiobtnTREATED_CONDUSIVE_CONDITIONS2.isChecked()) {
					checkBoxTREATED_CONDUSIVE_CONDITIONS.setChecked(true);
				} else {
					checkBoxTREATED_CONDUSIVE_CONDITIONS.setChecked(false);
				}
				break;
			case R.id.radiobtnCLEANES_RODENT_STATIONS1:

				// radiobtnCLEANES_RODENT_STATIONS1.setChecked(true);
				// checkBoxCLEANES_RODENT_STATIONS.setChecked(true);
				if (radiobtnCLEANES_RODENT_STATIONS1.isChecked()) {
					checkBoxCLEANES_RODENT_STATIONS.setChecked(true);
				} else {
					checkBoxCLEANES_RODENT_STATIONS.setChecked(false);
				}
				break;
			case R.id.radiobtnCLEANES_RODENT_STATIONS2:

				// radiobtnCLEANES_RODENT_STATIONS1.setChecked(true);
				// checkBoxCLEANES_RODENT_STATIONS.setChecked(true);

				if (radiobtnCLEANES_RODENT_STATIONS2.isChecked()) {
					checkBoxCLEANES_RODENT_STATIONS.setChecked(true);
				} else {
					checkBoxCLEANES_RODENT_STATIONS.setChecked(false);
				}
				break;

			case R.id.radiobtnINSTALLED_RODENT_BAIT_STATIONS1:
				if (radiobtnINSTALLED_RODENT_BAIT_STATIONS1.isChecked()) {
					checkBoxINSTALLED_RODENT_BAIT_STATIONS.setChecked(true);
				} else {
					if (CommonFunction.checkString(
							edt_how_many_INSTALLED_RODENT_BAIT_STATIONS
									.getText().toString(), "").equals("")) {
						checkBoxINSTALLED_RODENT_BAIT_STATIONS
								.setChecked(false);
					}
				}

				break;
			case R.id.radiobtnINSTALLED_RODENT_BAIT_STATIONS2:
				if (radiobtnINSTALLED_RODENT_BAIT_STATIONS2.isChecked()) {
					checkBoxINSTALLED_RODENT_BAIT_STATIONS.setChecked(true);
				} else {

					if (CommonFunction.checkString(
							edt_how_many_INSTALLED_RODENT_BAIT_STATIONS
									.getText().toString(), "").equals("")) {
						checkBoxINSTALLED_RODENT_BAIT_STATIONS
								.setChecked(false);
					}
				}

				break;
			case R.id.radiobtnREPLACED_BAIT1:
				if (radiobtnREPLACED_BAIT1.isChecked()) {
					checkBoxREPLACED_BAIT.setChecked(true);
				} else {
					if (CommonFunction.checkString(
							edt_station_REPLACED_BAIT.getText().toString(), "")
							.equals("")) {
						checkBoxREPLACED_BAIT.setChecked(false);
					}
				}

				break;
			case R.id.radiobtnREPLACED_BAIT2:
				if (radiobtnREPLACED_BAIT2.isChecked()) {
					checkBoxREPLACED_BAIT.setChecked(true);
				} else {
					if (CommonFunction.checkString(
							edt_station_REPLACED_BAIT.getText().toString(), "")
							.equals("")) {
						checkBoxREPLACED_BAIT.setChecked(false);
					}
				}

				break;

			case R.id.radiobtnREPLACED_BAIT_STATIONS1:
				if (radiobtnREPLACED_BAIT_STATIONS1.isChecked()) {
					checkBoxREPLACED_BAIT_STATIONS.setChecked(true);
				} else {
					if (CommonFunction.checkString(
							edt_station_REPLACED_BAIT_STATIONS.getText()
									.toString(), "").equals("")
							&& CommonFunction.checkString(
									edt_how_many_REPLACED_BAIT_STATIONS
											.getText().toString(), "").equals(
									"")) {
						checkBoxREPLACED_BAIT_STATIONS.setChecked(false);
					}
				}

				break;
			case R.id.radiobtnREPLACED_BAIT_STATIONS2:
				if (radiobtnREPLACED_BAIT_STATIONS2.isChecked()) {
					checkBoxREPLACED_BAIT_STATIONS.setChecked(true);
				} else {
					if (CommonFunction.checkString(
							edt_station_REPLACED_BAIT_STATIONS.getText()
									.toString(), "").equals("")
							&& CommonFunction.checkString(
									edt_how_many_REPLACED_BAIT_STATIONS
											.getText().toString(), "").equals(
									"")) {
						checkBoxREPLACED_BAIT_STATIONS.setChecked(false);
					}
				}

				break;
			case R.id.radiobtnINSPECTED_RODENT_TRAPS1:
				if (radiobtnINSPECTED_RODENT_TRAPS1.isChecked()) {
					checkBoxINSPECTED_RODENT_TRAPS.setChecked(true);
				} else {

					checkBoxINSPECTED_RODENT_TRAPS.setChecked(false);

				}

				break;
			case R.id.radiobtnINSPECTED_RODENT_TRAPS2:
				if (radiobtnINSPECTED_RODENT_TRAPS2.isChecked()) {
					checkBoxINSPECTED_RODENT_TRAPS.setChecked(true);
				} else {

					checkBoxINSPECTED_RODENT_TRAPS.setChecked(false);

				}

				break;

			case R.id.radiobtnCLEANED_RODENT_TRAPS1:
				if (radiobtnCLEANED_RODENT_TRAPS1.isChecked()) {
					checkBoxCLEANED_RODENT_TRAPS.setChecked(true);
				} else {

					checkBoxCLEANED_RODENT_TRAPS.setChecked(false);

				}

				break;

			case R.id.radiobtnCLEANED_RODENT_TRAPS2:
				if (radiobtnCLEANED_RODENT_TRAPS2.isChecked()) {
					checkBoxCLEANED_RODENT_TRAPS.setChecked(true);
				} else {

					checkBoxCLEANED_RODENT_TRAPS.setChecked(false);

				}

				break;

			case R.id.radiobtnINSTALLED_RODENT_TRAPS1:
				if (radiobtnINSTALLED_RODENT_TRAPS1.isChecked()) {
					checkBoxINSTALLED_RODENT_TRAPS.setChecked(true);
				} else {
					if (CommonFunction.checkString(
							edt_how_many_INSTALLED_RODENT_TRAPS.getText()
									.toString(), "").equals("")) {
						checkBoxINSTALLED_RODENT_TRAPS.setChecked(false);
					}
				}

				break;

			case R.id.radiobtnREPLACED_RODENT_TRAPS1:
				if (radiobtnREPLACED_RODENT_TRAPS1.isChecked()) {
					checkBoxREPLACED_RODENT_TRAPS.setChecked(true);
				} else {
					if (CommonFunction.checkString(
							edt_station_REPLACED_RODENT_TRAPS.getText()
									.toString(), "").equals("")
							&& CommonFunction.checkString(
									edt_how_many_REPLACED_RODENT_TRAPS
											.getText().toString(), "").equals(
									"")) {
						checkBoxREPLACED_RODENT_TRAPS.setChecked(false);
					}
				}

				break;
			case R.id.radiobtnREPLACED_RODENT_TRAPS2:
				if (radiobtnREPLACED_RODENT_TRAPS2.isChecked()) {
					checkBoxREPLACED_RODENT_TRAPS.setChecked(true);
				} else {
					if (CommonFunction.checkString(
							edt_station_REPLACED_RODENT_TRAPS.getText()
									.toString(), "").equals("")
							&& CommonFunction.checkString(
									edt_how_many_REPLACED_RODENT_TRAPS
											.getText().toString(), "").equals(
									"")) {
						checkBoxREPLACED_RODENT_TRAPS.setChecked(false);
					}
				}

				break;

			case R.id.radiobtnSEALED_ENTRY_POINTS1:
				if (radiobtnSEALED_ENTRY_POINTS1.isChecked()) {
					checkBoxSEALED_ENTRY_POINTS.setChecked(true);
				} else {

					checkBoxSEALED_ENTRY_POINTS.setChecked(false);

				}

				break;

			case R.id.radiobtnSEALED_ENTRY_POINTS2:
				if (radiobtnSEALED_ENTRY_POINTS2.isChecked()) {
					checkBoxSEALED_ENTRY_POINTS.setChecked(true);
				} else {

					checkBoxSEALED_ENTRY_POINTS.setChecked(false);

				}

				break;
			case R.id.radioCash:
				// edttxtAmount.getText().clear();
				edtDRIVING_LICENCE.getText().clear();
				edtEXPIRATION_DATE.getText().clear();
				tramount.setVisibility(View.VISIBLE);
				trcheckno.setVisibility(View.GONE);
				trDrivingLicence.setVisibility(View.GONE);
				trExpirationDate.setVisibility(View.GONE);
				LinearlayoutCHECK_IMAGE.setVisibility(View.GONE);
				break;
			case R.id.radioCheck:

				// edttxtAmount.getText().clear();
				edtDRIVING_LICENCE.setText(generalInfoDTO.getLicenseNo());
				if (!generalInfoDTO.getExpirationDate().equals("01/01/1900")
						&& !generalInfoDTO.getExpirationDate().equals("")) {
					edtEXPIRATION_DATE.setText(generalInfoDTO
							.getExpirationDate());
				}
				tramount.setVisibility(View.VISIBLE);
				trcheckno.setVisibility(View.VISIBLE);
				trDrivingLicence.setVisibility(View.VISIBLE);
				trExpirationDate.setVisibility(View.VISIBLE);
				LinearlayoutCHECK_IMAGE.setVisibility(View.VISIBLE);
				break;
			case R.id.radioCreditcard:
				// edttxtAmount.getText().clear();
				edtDRIVING_LICENCE.getText().clear();
				edtEXPIRATION_DATE.getText().clear();
				tramount.setVisibility(View.VISIBLE);
				trcheckno.setVisibility(View.GONE);
				trDrivingLicence.setVisibility(View.GONE);
				trExpirationDate.setVisibility(View.GONE);
				LinearlayoutCHECK_IMAGE.setVisibility(View.GONE);
				break;
			case R.id.radioBillLater:
				// edttxtAmount.getText().clear();
				edtDRIVING_LICENCE.getText().clear();
				edtEXPIRATION_DATE.getText().clear();
				tramount.setVisibility(View.GONE);
				trcheckno.setVisibility(View.GONE);
				trDrivingLicence.setVisibility(View.GONE);
				trExpirationDate.setVisibility(View.GONE);
				LinearlayoutCHECK_IMAGE.setVisibility(View.GONE);
				break;
			case R.id.radioPreBill:
				// edttxtAmount.getText().clear();
				edtDRIVING_LICENCE.getText().clear();
				edtEXPIRATION_DATE.getText().clear();
				tramount.setVisibility(View.GONE);
				trcheckno.setVisibility(View.GONE);
				trDrivingLicence.setVisibility(View.GONE);
				trExpirationDate.setVisibility(View.GONE);
				LinearlayoutCHECK_IMAGE.setVisibility(View.GONE);
				break;
			case R.id.radioNocharge:
				// edttxtAmount.getText().clear();
				edtDRIVING_LICENCE.getText().clear();
				edtEXPIRATION_DATE.getText().clear();
				tramount.setVisibility(View.GONE);
				trcheckno.setVisibility(View.GONE);
				trDrivingLicence.setVisibility(View.GONE);
				trExpirationDate.setVisibility(View.GONE);
				LinearlayoutCHECK_IMAGE.setVisibility(View.GONE);
				break;
			default:
				break;
			}
		}
	}

	CharSequence[] charSequenceItems;

	class OnButtonClick implements OnClickListener {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btnsaveandcontinueINVOICE:

				getValuesFromComponents();
				setValuesToCommercialDTO();
				validationList.clear();
				isValidated = true;
				objectFocus = null;
				validateComponents();
				if (isValidated) {
					checkChange = 0;
					insertUpadteCommercialTable();
					insertUpdateLog();
					Intent i = new Intent(
							HC_PestControlServiceReport.this,
							HC_ChemicalPreview.class);
					i.putExtra("userDTO", userDTO);
					Log.e("check gene info", generalInfoDTO.getTax_value()
							+ " " + generalInfoDTO.getInv_value() + " "
							+ generalInfoDTO.getServiceID_new());
					i.putExtra("generalInfoDTO", generalInfoDTO);
					i.putExtra("strOutsideService",
							strOutsidepestControlServices);
					i.putExtra("strInsideService", strInsidepestControlServices);
					/*
					 * i.putExtra("strAreasInspectedExterior",
					 * strAreasInspectedExterior);
					 * i.putExtra("strAreasInspectedInterior",
					 * strAreasInspectedInterior);
					 */

					i.putExtra(ParameterUtil.ServiceID_new,
							generalInfoDTO.getServiceID_new());
					startActivity(i);
				} else {
					mLastClickTime = 0;
					charSequenceItems = validationList
							.toArray(new CharSequence[validationList.size()]);
					createMessageList(charSequenceItems, objectFocus);
				}
				break;

			case R.id.edtEXPIRATION_DATE:
				// String strFromDate=edtFromdate.getText().toString();
				// String strToDate=edtTodate.getText().toString();
				DialogFragment DatePickerFragment = new DatePickerFragment(
						edtEXPIRATION_DATE);
				DatePickerFragment.show(getFragmentManager(), "Date Picker");
				DatePickerFragment.setCancelable(false);
				LoggerUtil.e("edttxtExpirationDate.getText().toString()", ""
						+ edtEXPIRATION_DATE.getText().toString());

				break;
			case R.id.btnCancel:
				finish();
				break;

			case R.id.edtDate:
				// String strFromDate=edtFromdate.getText().toString();
				// String strToDate=edtTodate.getText().toString();
				DialogFragment TodateFragment = new DatePickerFragment(edtDate,
						edtDate, 2);
				TodateFragment.show(getFragmentManager(), "Date Picker");
				TodateFragment.setCancelable(false);
				break;

			case R.id.btnCHECK_BACK_IMAGE:
				if (listBackImg.size() > 0) {
					new AlertDialog.Builder(
							HC_PestControlServiceReport.this)
							.setIcon(android.R.drawable.ic_dialog_alert)
							.setTitle("Change Check Back Image")
							.setMessage("Are you sure want to replace it?")
							.setCancelable(false)
							.setPositiveButton("Yes",
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											final CharSequence[] items = {
													" Take New Photo ",
													" Choose from Gallery " };

											// Creating and Building the Dialog
											AlertDialog.Builder builder = new AlertDialog.Builder(
													HC_PestControlServiceReport.this);
											builder.setTitle("Upload Image");
											builder.setSingleChoiceItems(
													items,
													-1,
													new DialogInterface.OnClickListener() {
														public void onClick(
																DialogInterface dialog,
																int item) {

															switch (item) {
															case 0:

																if (CommonFunction
																		.isSdPresent()) {
																	takeBackPicture();
																} else {
																	Toast.makeText(
																			HC_PestControlServiceReport.this,
																			"SdCard Not Present",
																			1000)
																			.show();
																}

																break;
															case 1:

																if (CommonFunction
																		.isSdPresent()) {
																	pickImage("CHECK_BACK_IMAGE");
																} else {
																	Toast.makeText(
																			HC_PestControlServiceReport.this,
																			"SdCard Not Present",
																			1000)
																			.show();
																}

																break;
															}
															dialogBackImage
																	.dismiss();
														}
													});
											dialogBackImage = builder.create();
											dialogBackImage.show();

										}

									})
							.setNegativeButton("No",
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											dialog.dismiss();
										}
									}).show();
				} else {
					final CharSequence[] items = { " Take New Photo ",
							" Choose from Gallery " };

					// Creating and Building the Dialog
					AlertDialog.Builder builder = new AlertDialog.Builder(
							HC_PestControlServiceReport.this);
					builder.setTitle("Upload Image");
					builder.setSingleChoiceItems(items, -1,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int item) {

									switch (item) {
									case 0:

										if (CommonFunction.isSdPresent()) {
											takeBackPicture();
										} else {
											Toast.makeText(
													HC_PestControlServiceReport.this,
													"SdCard Not Present", 1000)
													.show();
										}

										break;
									case 1:

										if (CommonFunction.isSdPresent()) {
											pickImage("CHECK_BACK_IMAGE");
										} else {
											Toast.makeText(
													HC_PestControlServiceReport.this,
													"SdCard Not Present", 1000)
													.show();
										}

										break;
									}
									dialogBackImage.dismiss();
								}
							});
					dialogBackImage = builder.create();
					dialogBackImage.show();
				}

				break;
			case R.id.btnCHECK_FRONT_IMAGE:
				// Log.e("listFrontImg.size()", ""+listFrontImg.size());
				if (listFrontImg.size() > 0) {
					new AlertDialog.Builder(
							HC_PestControlServiceReport.this)
							.setIcon(android.R.drawable.ic_dialog_alert)
							.setTitle("Change Check Front Image")
							.setMessage("Are you sure want to replace it?")
							.setCancelable(false)
							.setPositiveButton("Yes",
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {

											final CharSequence[] items = {
													" Take New Photo ",
													" Choose from Gallery " };

											// Creating and Building the Dialog
											AlertDialog.Builder builder = new AlertDialog.Builder(
													HC_PestControlServiceReport.this);
											builder.setTitle("Upload Image");
											builder.setSingleChoiceItems(
													items,
													-1,
													new DialogInterface.OnClickListener() {
														public void onClick(
																DialogInterface dialog,
																int item) {

															switch (item) {

															case 0:

																if (CommonFunction
																		.isSdPresent()) {
																	takeFrontPicture();
																} else {
																	Toast.makeText(
																			HC_PestControlServiceReport.this,
																			"SdCard Not Present",
																			1000)
																			.show();
																}

																break;
															case 1:

																if (CommonFunction
																		.isSdPresent()) {
																	pickImage("CHECK_FRONT_IMAGE");
																} else {
																	Toast.makeText(
																			HC_PestControlServiceReport.this,
																			"SdCard Not Present",
																			1000)
																			.show();
																}

																break;
															}
															dialogFrontImage
																	.dismiss();
														}
													});
											dialogFrontImage = builder.create();
											dialogFrontImage.show();

										}

									})
							.setNegativeButton("No",
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											dialog.dismiss();
										}
									}).show();
				} else {
					final CharSequence[] items = { " Take New Photo ",
							" Choose from Gallery " };

					// Creating and Building the Dialog
					AlertDialog.Builder builder = new AlertDialog.Builder(
							HC_PestControlServiceReport.this);
					builder.setTitle("Upload Image");
					builder.setSingleChoiceItems(items, -1,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int item) {

									switch (item) {
									case 0:
										if (CommonFunction.isSdPresent()) {
											takeFrontPicture();
										} else {
											Toast.makeText(
													HC_PestControlServiceReport.this,
													"SdCard Not Present", 1000)
													.show();
										}
										break;
									case 1:

										if (CommonFunction.isSdPresent()) {
											pickImage("CHECK_FRONT_IMAGE");
										} else {
											Toast.makeText(
													HC_PestControlServiceReport.this,
													"SdCard Not Present", 1000)
													.show();
										}

										break;
									}
									dialogFrontImage.dismiss();
								}
							});
					dialogFrontImage = builder.create();
					dialogFrontImage.show();
				}

				break;
			}
		}
	}

	public void insertUpdateLog() {
		Gson gson = new Gson();
		// TODO Auto-generated method stub

		logDTO.setCommercial_Insp("True");
		logDTO.setSID(serviceID_new);
		// String logString = gson.toJson(logDTO);
		try {
			// if(logArray.length()>0)
			// {
			int u = HoustonFlowFunctions.updateLog(logDTO);
			LoggerUtil.e("log updated. values", "" + u);

			JSONArray logArray = houstonFlowFunctions
					.getAllSelectedLogRow(serviceID_new);
			// Log.e("", "logArray...."+logArray);

			// }
			// else
			// {
			// long a = HoustonFlowFunctions.addLog(new JSONObject(logString));
			// LoggerUtil.e("log added1. values", ""+a);
			// }
		} catch (Exception e) {
			Log.e("logArray", "logArray...." + e);
		}
	}

	public void insertUpadteCommercialTable() {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		Gson gson = new Gson();

		for (int i = 0; i < checkList.size(); i++) {
			// LoggerUtil.e("checkList.size()..","checkList.size()..."+checkList.size());

			ChemicalDto chemicalDto = new ChemicalDto();
			// LoggerUtil.e("listChemicals.get(i).getPestPrevention_ID()..","listChemicals.get(i).getPestPrevention_ID()..."+listChemicals.get(i).getPestPrevention_ID());
			if (checkList.get(i).isChecked()) {
				logDTO.setChemical("True");
				chemicalDto.setIsCompleted("true");

				if (CommonFunction.checkString(
						listChemicals.get(i).getPestPrevention_ID(), "")
						.equals("")) {
					chemicalDto.setPestPrevention_ID("0");
				} else {
					chemicalDto.setPestPrevention_ID(listChemicals.get(i)
							.getPestPrevention_ID());
				}
				chemicalDto.setPreventationID(listChemicals.get(i)
						.getPreventationID());
				chemicalDto.setProduct(listChemicals.get(i).getProduct());

				if (CommonFunction.checkString(
						listChemicals.get(i).getPercentPes(), "").equals("")) {
					chemicalDto.setPercentPes("0.00");
				} else {
					chemicalDto.setPercentPes(listChemicals.get(i)
							.getPercentPes());
				}
				if (CommonFunction.checkString(
						listChemicals.get(i).getAmount(), "").equals("")) {
					chemicalDto.setAmount("0.00");
				} else {
					chemicalDto.setAmount(listChemicals.get(i).getAmount());
				}
				chemicalDto.setStatus("Commercial");
				chemicalDto.setUnit(listChemicals.get(i).getUnit());
				chemicalDto.setTarget(listChemicals.get(i).getTarget());
				chemicalDto.setCreate_By(userDTO.getPestPackId());
				chemicalDto.setServiceID_new(serviceID_new);
				// LoggerUtil.e("listChemicals.get(i).getOtherText()",listChemicals.get(i).getOtherText());

				chemicalDto.setOtherText(listChemicals.get(i).getOtherText());
				chemicalDto.setCreate_Date(CommonFunction.getCurrentDateTime());
				listChemicals.get(i).setProductCheck("true");
				chemicalDto.setProductCheck(listChemicals.get(i).getProductCheck());
			} 
			else {

				chemicalDto.setIsCompleted("true");
				chemicalDto.setPestPrevention_ID(listChemicals.get(i).getPestPrevention_ID());
				chemicalDto.setPreventationID(listChemicals.get(i).getPreventationID());
				chemicalDto.setProduct(listChemicals.get(i).getProduct());
				chemicalDto.setPercentPes(listChemicals.get(i).getPercentPes());
				chemicalDto.setAmount(listChemicals.get(i).getAmount());
				chemicalDto.setUnit(listChemicals.get(i).getUnit());
				chemicalDto.setTarget(listChemicals.get(i).getTarget());
				chemicalDto.setCreate_By(userDTO.getPestPackId());
				chemicalDto.setServiceID_new(serviceID_new);

				listChemicals.get(i).setOtherText("");
				chemicalDto.setOtherText(listChemicals.get(i).getOtherText());
				chemicalDto.setCreate_Date(CommonFunction.getCurrentDateTime());
				listChemicals.get(i).setProductCheck("false");
				chemicalDto.setProductCheck(listChemicals.get(i)
						.getProductCheck());

			}
			listSelectedChemicals.add(chemicalDto);
		}
		try {
			houstonFlowFunctions.deleteSelectedChemicalData(
					DatabaseHelper.TABLE_SELECTED_CHEMICALS,ParameterUtil.ServiceID_new,ParameterUtil.Status, serviceID_new, "Commercial");
			houstonFlowFunctions.deleteSelectedChemicalData(
					DatabaseHelper.TABLE_CHEMICALS,
					ParameterUtil.ServiceID_new,ParameterUtil.Status, serviceID_new, "Commercial");

			JSONArray jsSelectedChemical = new JSONArray(
					gson.toJson(listSelectedChemicals));

			houstonFlowFunctions.insertMultipleRecords(jsSelectedChemical,
					DatabaseHelper.TABLE_SELECTED_CHEMICALS);
			houstonFlowFunctions.insertMultipleRecords(jsSelectedChemical,
					DatabaseHelper.TABLE_CHEMICALS);

			listSelectedChemicals.clear();

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		hCommercial_pest_DTO.setServiceID_new(serviceID_new);
		String commercialstring = gson.toJson(hCommercial_pest_DTO);
		Log.e("commercialstring", commercialstring + "");
		try {

			int in = houstonFlowFunctions.update(generalInfoDTO);
			if (houstonFlowFunctions.isAlreadyAdded(serviceID_new,
					DatabaseHelper.TABLE_HC_PEST_INFO)) {

				int u = houstonFlowFunctions
						.updateCommercial_Pest(hCommercial_pest_DTO);

				LoggerUtil.e("Updated", u + " ...Update Commercial");
			} else {

				long a = houstonFlowFunctions
						.addServicesForCommercial(new JSONObject(
								commercialstring));
				Log.e("New commercial Record Added", a + "");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private final Dialog createMessageList(final CharSequence[] messageList,
			final EditText object) {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				HC_PestControlServiceReport.this);
		builder.setTitle(getResources()
				.getString(R.string.REQUIRED_INFORMATION));
		builder.setItems(charSequenceItems,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// LoggerUtil.e("","E' stato premuto il pulsante: "+messageList[whichButton]);
					}
				});
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				if (object != null) {
					object.requestFocus();
				}
			}
		});
		return builder.show();
	}

	public void setValuesToCommercialDTO() {

		hCommercial_pest_DTO.setCommercialId(strCommercialId);
		hCommercial_pest_DTO.setServiceID_new(serviceID_new);
		// set service provided
		hCommercial_pest_DTO.setServiceFor(strServiceFor);

		// set areas inspected
		hCommercial_pest_DTO.setAreasInspectedExterior(strAreasInspectedExterior);
		hCommercial_pest_DTO.setAreasInspectedInterior(strAreasInspectedInterior);

		// set inspected and treated for
		hCommercial_pest_DTO.setInsectActivity(strInspectedadnTreatedFor);
		hCommercial_pest_DTO.setAnts(strEdttxtInspectedandTreatedFor1);
		hCommercial_pest_DTO.setOtherPest(strEdttxtInspectedandTreatedFor2);

		// set pest activity by zone
		// Log.e("strpestActivityByZone", ""+strpestActivityByZone);

		hCommercial_pest_DTO.setPestActivity(strpestActivityByZone);
		hCommercial_pest_DTO.setInterior(strInterior);
		hCommercial_pest_DTO.setOutsidef_Perimeter(strOutsidef_Perimeter);
		hCommercial_pest_DTO.setDumpster(strDumpster);
		hCommercial_pest_DTO.setDining_Room(strDining_Room);
		hCommercial_pest_DTO.setCommon_Areas(strCommon_Areas);
		hCommercial_pest_DTO.setKitchen(strKitchen);
		hCommercial_pest_DTO.setDry_Storage(strDry_Storage);
		hCommercial_pest_DTO.setDish_Washing(strDish_Washing);
		hCommercial_pest_DTO.setRoof_Tops(strRoof_Tops);
		hCommercial_pest_DTO.setWait_Stations(strWait_Stations);
		hCommercial_pest_DTO.setDrop_Ceiling(strDrop_Ceiling);
		hCommercial_pest_DTO.setPlanters(strPlanters);
		hCommercial_pest_DTO.setWarehouse_Storage(strWarehouse_Storage);

		// set pest control services
		hCommercial_pest_DTO.setPestControlServices(strPestControlServices);

		// set termities
		hCommercial_pest_DTO.setSignOfTermiteMS(strCheckSignOfTermiteMS);
		hCommercial_pest_DTO.setSignOfTermiteActivityMS(strSignOfTermiteMS);
		hCommercial_pest_DTO.setSignOfTermiteText(strStationSignOfTermiteMS);

		hCommercial_pest_DTO.setInstallMS(strCheckINSTALLED_MS);
		hCommercial_pest_DTO.setInstallMSText(strINSTALLED_MONITORING_STATIONS);

		hCommercial_pest_DTO.setSignOfTermitePS(strCheckSignOfTermitePS);
		hCommercial_pest_DTO.setSignOfTermiteActivityPS(strSignOfTermitePS);

		hCommercial_pest_DTO.setSignOfTermiteIS(strCheckSignOfTermiteIS);
		hCommercial_pest_DTO.setSignOfTermiteActivityIS(strSignOfTermiteIS);

		hCommercial_pest_DTO.setReplacedMS(strCheckREPLACED_MS);
		hCommercial_pest_DTO.setReplacedActivityTextMS(strREPLACED_MS);
		hCommercial_pest_DTO.setReplacedTextMS(strStationREPLACED_MS);

		hCommercial_pest_DTO.setTreatedPS(strCheckTreatedPS);
		hCommercial_pest_DTO.setTreatedActivityPS(strTreatedPS);

		hCommercial_pest_DTO.setTreatedSP(strCheckTreatedSP);
		hCommercial_pest_DTO.setTreatedActivitySP(strTreatedSP);

		hCommercial_pest_DTO.setTreatedCC(strCheckTreatedCC);
		hCommercial_pest_DTO.setTreatedActivityCC(strTreatedCC);

		// set values for rodent
		hCommercial_pest_DTO
				.setInspectedRodentStation(strCheckINSPECTED_RODENT_STATIONS);
		hCommercial_pest_DTO
				.setTxtInspectedRodentStation(strHowManyINSPECTED_RODENT_STATIONS);

		hCommercial_pest_DTO.setCleanedRodentStation(strCheckCLEANES_RODENT_STATIONS);
		hCommercial_pest_DTO
				.setIsCleanedRodentStation(strRadioCLEANES_RODENT_STATIONS);

		hCommercial_pest_DTO
				.setInstallRodentBaitStation(strCheckINSTALLED_RODENT_BAIT_STATIONS);
		hCommercial_pest_DTO
				.setIsInstallRodentBaitStation(strRadioINSTALLED_RODENT_BAIT_STATIONS);
		hCommercial_pest_DTO
				.setTxtInstallRodentBaitStation(strHowManyINSTALLED_RODENT_BAIT_STATIONS);

		hCommercial_pest_DTO.setReplacedBait(strCheckREPLACED_BAIT);
		hCommercial_pest_DTO.setIsReplacedBait(strRadioREPLACED_BAIT);
		hCommercial_pest_DTO.setSNoReplacedBait(strStationREPLACED_BAIT);

		hCommercial_pest_DTO.setReplacedBaitStation(strCheckREPLACED_BAIT_STATIONS);
		hCommercial_pest_DTO.setIsReplacedBaitStation(strRadioREPLACED_BAIT_STATIONS);
		hCommercial_pest_DTO
				.setSNoReplacedBaitStation(strStationREPLACED_BAIT_STATIONS);
		hCommercial_pest_DTO
				.setTxtReplacedBaitStation(strHowManyREPLACED_BAIT_STATIONS);

		hCommercial_pest_DTO.setInspectedRodentTraps(strCheckINSPECTED_RODENT_TRAPS);
		hCommercial_pest_DTO.setIsInspectedRodentTraps(strRadioINSPECTED_RODENT_TRAPS);

		hCommercial_pest_DTO.setCleanedRodentTrap(strCheckCLEANED_RODENT_TRAPS);
		hCommercial_pest_DTO.setIsCleanedRodentTrap(strRadioCLEANED_RODENT_TRAPS);

		hCommercial_pest_DTO.setInstallRodentTrap(strCheckINSTALLED_RODENT_TRAPS);
		hCommercial_pest_DTO.setIsInstallRodentTrap(strRadioINSTALLED_RODENT_TRAPS);
		hCommercial_pest_DTO.setTxtInstallRodentTrap(strHowManyINSTALLED_RODENT_TRAPS);

		hCommercial_pest_DTO.setReplacedRodentTrap(strCheckREPLACED_RODENT_TRAPS);
		hCommercial_pest_DTO.setIsReplacedRodentTrap(strRadioREPLACED_RODENT_TRAPS);
		hCommercial_pest_DTO.setTxtReplacedRodentTrap(strHowManyREPLACED_RODENT_TRAPS);
		hCommercial_pest_DTO.setSNoReplacedRodentTrap(strStationREPLACED_RODENT_TRAPS);

		hCommercial_pest_DTO.setSealedEntryPoint(strCheckSEALED_ENTRY_POINTS);
		hCommercial_pest_DTO.setIsSealedEntryPoint(strRadioSEALED_ENTRY_POINTS);

		// Log.e(" set InspectedSnapTraps...",
		// strCheckINSPECTED_SNAP_TRAPS+" "+strHowMany_INSPECTED_SNAP_TRAPS);
		hCommercial_pest_DTO.setInspectedSnapTrap(strCheckINSPECTED_SNAP_TRAPS);
		hCommercial_pest_DTO.setTxtInspectedSnapTrap(strHowMany_INSPECTED_SNAP_TRAPS);

		hCommercial_pest_DTO.setInspectedLiveCages(strCheckINSPECTED_LIVE_CAGES);
		hCommercial_pest_DTO.setTxtInspectedLiveCages(strHowManyINSPECTED_LIVE_CAGES);

		hCommercial_pest_DTO.setRemovedSnapTraps(strCheckREMOVED_SNAP_TRAPS);
		hCommercial_pest_DTO.setTxtRemovedSnapTraps(strHowManyREMOVED_SNAP_TRAPS);

		hCommercial_pest_DTO.setRemovedLiveCages(strCheckREMOVED_LIVE_CAGES);
		hCommercial_pest_DTO.setTxtRemovedLiveCages(strHowManyREMOVED_LIVE_CAGES);

		hCommercial_pest_DTO
				.setRemovedRodentBaitStation(strCheckREMOVED_RODENT_BAIT_STATIONS);
		hCommercial_pest_DTO
				.setTxtRemovedRodentBaitStation(strHowManyREMOVED_RODENT_BAIT_STATIONS);

		hCommercial_pest_DTO.setRemovedRodentTrap(strCheckREMOVED_RODENT_TRAPS);
		hCommercial_pest_DTO.setTxtRemovedRodentTrap(strHowManyREMOVED_RODENT_TRAPS);

		hCommercial_pest_DTO.setSetSnapTrap(strCheckSET_SNAP_TRAPS);
		hCommercial_pest_DTO.setTxtSetSnapTrap(strSET_SNAP_TRAPS);
		hCommercial_pest_DTO.setLstSetSnapTrap(strCheckChildSET_SNAP_TRAPS);

		hCommercial_pest_DTO.setSetLiveCages(strCheckSET_LIVE_CAGES);
		hCommercial_pest_DTO.setTxtSetLiveCages(strSET_LIVE_CAGES);
		hCommercial_pest_DTO.setLstSetLiveCages(strCheckChildSET_LIVE_CAGES);

		// set mosquito services
		hCommercial_pest_DTO.setPlusMosquitoDetail(strMosquito);
		hCommercial_pest_DTO.setDurationTime(strMosquitoService1);
		hCommercial_pest_DTO.setSystemRuns(strMosquitoService2);

		// set Recommendations
		hCommercial_pest_DTO.setRecommendations(strRecommendations);

		// set invoice
		hCommercial_pest_DTO.setTechnicianComments(strEdttxtTechComments);
		hCommercial_pest_DTO.setTechnician(strEdttxtServiceTech);
		hCommercial_pest_DTO.setEmpNo(strEdttxtEmp);
		hCommercial_pest_DTO.setCustomer(strEdttxtCustomer);
		hCommercial_pest_DTO.setDate(strEdttxtdate);

		// set values for invoice
		hCommercial_pest_DTO.setTechnicianComments(strEdttxtTechComments);
		hCommercial_pest_DTO.setTechnician(strEdttxtServiceTech);
		hCommercial_pest_DTO.setEmpNo(strEdttxtEmp);
		hCommercial_pest_DTO.setCustomer(strEdttxtCustomer);
		hCommercial_pest_DTO.setDate(strEdttxtdate);
		hCommercial_pest_DTO.setAmount(strEdttxtAmount);

		generalInfoDTO.setServiceID_new(serviceID_new);
		generalInfoDTO.setInv_value(strEdttxtInvValue);
		generalInfoDTO.setProd_value(strEdttxtProdValue);
		generalInfoDTO.setTax_value(strEdttxtTax);
		generalInfoDTO.setTotal(generalInfoDTO.getInv_value());

		LoggerUtil.e("lic no", "start" + strEdttxtDrivingLicence + "end");

		generalInfoDTO.setCheckNo(strEdttxtCheckNo);
		generalInfoDTO.setLicenseNo(strEdttxtDrivingLicence);

		if (!CommonFunction.checkString(strEdtExpirationDate, "").equals("")) {
			generalInfoDTO.setExpirationDate(strEdtExpirationDate);
		} else {
			generalInfoDTO.setExpirationDate("01/01/1900");
		}
		if (tempStoreBackImageName.size() != 0) {
			generalInfoDTO.setCheckBackImage(tempStoreBackImageName.get(0));
		}
		if (tempStoreFrontImageName.size() != 0) {
			generalInfoDTO.setCheckFrontImage(tempStoreFrontImageName.get(0));
		}

		hCommercial_pest_DTO.setIsCompleted("true");
		// Log.e("strRadioInvoice...", "strRadioInvoice..."+strRadioInvoice);
		hCommercial_pest_DTO.setPaymentType(strRadioInvoice);
		// residential_pest_DTO.setCheckNo(strEdttxtCheckNo);
		// residential_pest_DTO.setLicenseNo(strEdttxtDrivingLicence);
		hCommercial_pest_DTO.setCreate_By(userDTO.getPestPackId());
		// commercialDTO.setUpdate_Date(CommonFunction.getCurrentDateTime());
		// commercialDTO.setCreate_Date(CommonFunction.getCurrentDateTime());

		hCommercial_pest_DTO.setTimeout(CommonFunction.getCurrentTime());
		hCommercial_pest_DTO.setTimeOutDateTime(CommonFunction.getCurrentDateTime());
		//"010101"
		hCommercial_pest_DTO.setTimeIn(generalInfoDTO.getTimeIn());
		hCommercial_pest_DTO.setTimeInDateTime(generalInfoDTO.getTimeInDateTime());

	}

	public void getValuesFromComponents() {
		// TODO Auto-generated method stub
		getValuesFromServiceForHeader();
		getValuesformAreasInspected();
		grtValuesforInspectedTreatedFor();
		getValuesPestActivitybyZone();
		getValuesPestControlServices();
		getValuesTermiteServices();
		getValuesRodentServices();
		getValuesMosquitoServices();
		getValuesForRecommendations();
		getValuesForInvoice();
	}

	private void getValuesRodentServices() {
		// TODO Auto-generated method stub
		if (checkBoxINSPECTED_RODENT_STATIONS.isChecked()) {
			strCheckINSPECTED_RODENT_STATIONS = checkBoxINSPECTED_RODENT_STATIONS
					.getText().toString();

			strHowManyINSPECTED_RODENT_STATIONS = edt_how_many_INSPECTED_RODENT_STATIONS
					.getText().toString();

		} else {
			strCheckINSPECTED_RODENT_STATIONS = "".trim();
		}

		if (checkBoxCLEANES_RODENT_STATIONS.isChecked()) {
			strCheckCLEANES_RODENT_STATIONS = checkBoxCLEANES_RODENT_STATIONS
					.getText().toString();
			int id = radioGroupCLEANES_RODENT_STATIONS
					.getCheckedRadioButtonId();
			// Log.e("", "idRoofPitches..."+idRoofPitches);

			if (id != -1) {

				RadioButton radio = (RadioButton) findViewById(id);

				strRadioCLEANES_RODENT_STATIONS = radio.getText().toString();

			}
		} else {
			strCheckCLEANES_RODENT_STATIONS = "".trim();
		}

		if (checkBoxINSTALLED_RODENT_BAIT_STATIONS.isChecked()) {
			strCheckINSTALLED_RODENT_BAIT_STATIONS = checkBoxINSTALLED_RODENT_BAIT_STATIONS
					.getText().toString();
			int id = radioGroupINSTALLED_RODENT_BAIT_STATIONS
					.getCheckedRadioButtonId();
			// Log.e("", "idRoofPitches..."+idRoofPitches);

			if (id != -1) {

				RadioButton radio = (RadioButton) findViewById(id);
				strRadioINSTALLED_RODENT_BAIT_STATIONS = radio.getText()
						.toString();
				strHowManyINSTALLED_RODENT_BAIT_STATIONS = edt_how_many_INSTALLED_RODENT_BAIT_STATIONS
						.getText().toString();
			}
		} else {
			strCheckINSTALLED_RODENT_BAIT_STATIONS = "".trim();
		}

		if (checkBoxREPLACED_BAIT.isChecked()) {
			strCheckREPLACED_BAIT = checkBoxREPLACED_BAIT.getText().toString();
			int id = radioGroupREPLACED_BAIT.getCheckedRadioButtonId();
			// Log.e("", "idRoofPitches..."+idRoofPitches);

			if (id != -1) {

				RadioButton radio = (RadioButton) findViewById(id);
				strRadioREPLACED_BAIT = radio.getText().toString();
				strStationREPLACED_BAIT = edt_station_REPLACED_BAIT.getText()
						.toString();
			}
		} else {
			strCheckREPLACED_BAIT = "".trim();
		}

		if (checkBoxREPLACED_BAIT_STATIONS.isChecked()) {
			strCheckREPLACED_BAIT_STATIONS = checkBoxREPLACED_BAIT_STATIONS
					.getText().toString();
			int id = radioGroupREPLACED_BAIT_STATIONS.getCheckedRadioButtonId();
			// Log.e("", "idRoofPitches..."+idRoofPitches);

			if (id != -1) {

				RadioButton radio = (RadioButton) findViewById(id);
				strRadioREPLACED_BAIT_STATIONS = radio.getText().toString();
				strStationREPLACED_BAIT_STATIONS = edt_station_REPLACED_BAIT_STATIONS
						.getText().toString();
				strHowManyREPLACED_BAIT_STATIONS = edt_how_many_REPLACED_BAIT_STATIONS
						.getText().toString();

			}
		} else {
			strCheckREPLACED_BAIT_STATIONS = "".trim();
		}

		if (checkBoxINSPECTED_RODENT_TRAPS.isChecked()) {
			strCheckINSPECTED_RODENT_TRAPS = checkBoxINSPECTED_RODENT_TRAPS
					.getText().toString();
			int id = radioGroupINSPECTED_RODENT_TRAPS.getCheckedRadioButtonId();
			// Log.e("", "idRoofPitches..."+idRoofPitches);

			if (id != -1) {

				RadioButton radio = (RadioButton) findViewById(id);

				strRadioINSPECTED_RODENT_TRAPS = radio.getText().toString();

			}
		} else {
			strCheckINSPECTED_RODENT_TRAPS = "".trim();
		}

		if (checkBoxCLEANED_RODENT_TRAPS.isChecked()) {
			strCheckCLEANED_RODENT_TRAPS = checkBoxCLEANED_RODENT_TRAPS
					.getText().toString();
			int id = radioGroupCLEANED_RODENT_TRAPS.getCheckedRadioButtonId();
			// Log.e("", "idRoofPitches..."+idRoofPitches);

			if (id != -1) {

				RadioButton radio = (RadioButton) findViewById(id);

				strRadioCLEANED_RODENT_TRAPS = radio.getText().toString();

			}
		} else {
			strCheckCLEANED_RODENT_TRAPS = "".trim();
		}

		if (checkBoxINSTALLED_RODENT_TRAPS.isChecked()) {
			strCheckINSTALLED_RODENT_TRAPS = checkBoxINSTALLED_RODENT_TRAPS
					.getText().toString();
			int id = radioGroupINSTALLED_RODENT_TRAPS.getCheckedRadioButtonId();
			// Log.e("", "idRoofPitches..."+idRoofPitches);

			if (id != -1) {

				RadioButton radio = (RadioButton) findViewById(id);
				strRadioINSTALLED_RODENT_TRAPS = radio.getText().toString()
						.trim();
				strHowManyINSTALLED_RODENT_TRAPS = edt_how_many_INSTALLED_RODENT_TRAPS
						.getText().toString();
			}
		} else {
			strCheckINSTALLED_RODENT_TRAPS = "".trim();
		}

		if (checkBoxREPLACED_RODENT_TRAPS.isChecked()) {
			strCheckREPLACED_RODENT_TRAPS = checkBoxREPLACED_RODENT_TRAPS
					.getText().toString();
			int id = radioGroupREPLACED_RODENT_TRAPS.getCheckedRadioButtonId();
			// Log.e("", "idRoofPitches..."+idRoofPitches);

			if (id != -1) {

				RadioButton radio = (RadioButton) findViewById(id);
				strRadioREPLACED_RODENT_TRAPS = radio.getText().toString();
				strStationREPLACED_RODENT_TRAPS = edt_station_REPLACED_RODENT_TRAPS
						.getText().toString();
				strHowManyREPLACED_RODENT_TRAPS = edt_how_many_REPLACED_RODENT_TRAPS
						.getText().toString();

			}
		} else {
			strCheckREPLACED_RODENT_TRAPS = "".trim();
		}
		if (checkBoxSEALED_ENTRY_POINTS.isChecked()) {
			strCheckSEALED_ENTRY_POINTS = checkBoxSEALED_ENTRY_POINTS.getText()
					.toString();
			int id = radioGroupSEALED_ENTRY_POINTS.getCheckedRadioButtonId();
			// Log.e("", "idRoofPitches..."+idRoofPitches);

			if (id != -1) {

				RadioButton radio = (RadioButton) findViewById(id);
				strRadioSEALED_ENTRY_POINTS = radio.getText().toString();

			}
		} else {
			strCheckSEALED_ENTRY_POINTS = "".trim();
		}
		if (checkBoxINSPECTED_SNAP_TRAPS.isChecked()) {
			strCheckINSPECTED_SNAP_TRAPS = checkBoxINSPECTED_SNAP_TRAPS
					.getText().toString();
			strHowMany_INSPECTED_SNAP_TRAPS = edt_how_many_INSPECTED_SNAP_TRAPS
					.getText().toString();
		} else {
			strCheckINSPECTED_SNAP_TRAPS = "".trim();
		}
		Log.e("get inspected snap traps", "..."
				+ strHowMany_INSPECTED_SNAP_TRAPS);
		if (checkBoxINSPECTED_LIVE_CAGES.isChecked()) {
			strCheckINSPECTED_LIVE_CAGES = checkBoxINSPECTED_LIVE_CAGES
					.getText().toString();
			strHowManyINSPECTED_LIVE_CAGES = edt_how_many_INSPECTED_LIVE_CAGES
					.getText().toString();
		} else {
			strCheckINSPECTED_LIVE_CAGES = "".trim();
		}

		if (checkBoxREMOVED_SNAP_TRAPS.isChecked()) {
			strCheckREMOVED_SNAP_TRAPS = checkBoxREMOVED_SNAP_TRAPS.getText()
					.toString();
			strHowManyREMOVED_SNAP_TRAPS = edt_how_many_REMOVED_SNAP_TRAPS
					.getText().toString();
		} else {
			strCheckREMOVED_SNAP_TRAPS = "".trim();
		}

		if (checkBoxREMOVED_LIVE_CAGES.isChecked()) {
			strCheckREMOVED_LIVE_CAGES = checkBoxREMOVED_LIVE_CAGES.getText()
					.toString();
			strHowManyREMOVED_LIVE_CAGES = edt_how_many_REMOVED_LIVE_CAGES
					.getText().toString();
		} else {
			strCheckREMOVED_LIVE_CAGES = "".trim();
		}

		if (checkBoxREMOVED_RODENT_BAIT_STATIONS.isChecked()) {
			strCheckREMOVED_RODENT_BAIT_STATIONS = checkBoxREMOVED_RODENT_BAIT_STATIONS
					.getText().toString();
			strHowManyREMOVED_RODENT_BAIT_STATIONS = edt_how_many_REMOVED_RODENT_BAIT_STATIONS
					.getText().toString();
		} else {
			strCheckREMOVED_RODENT_BAIT_STATIONS = "".trim();
		}

		if (checkBoxREMOVED_RODENT_TRAPS.isChecked()) {
			strCheckREMOVED_RODENT_TRAPS = checkBoxREMOVED_RODENT_TRAPS
					.getText().toString();
			strHowManyREMOVED_RODENT_TRAPS = edt_how_many_REMOVED_RODENT_TRAPS
					.getText().toString();
		} else {
			strCheckREMOVED_RODENT_TRAPS = "".trim();
		}
		if (checkBoxSET_SNAP_TRAPS.isChecked()) {
			strCheckSET_SNAP_TRAPS = checkBoxSET_SNAP_TRAPS.getText()
					.toString();
			strSET_SNAP_TRAPS = edt_SET_SNAP_TRAPS.getText().toString();

			strCheckChildSET_SNAP_TRAPS = "";

			for (int i = 0; i < snapTrapsCheckboxList.size(); i++) {
				// LoggerUtil.e("inside for loop 1..", "inside for loop 1..");
				if (snapTrapsCheckboxList.get(i).isChecked()) {
					strCheckChildSET_SNAP_TRAPS = strCheckChildSET_SNAP_TRAPS
							+ ","
							+ snapTrapsCheckboxList.get(i).getText().toString();
				}
			}
			strCheckChildSET_SNAP_TRAPS = CommonFunction.removeFirstCharIF(
					strCheckChildSET_SNAP_TRAPS, ",").trim();
		} else {
			strCheckSET_SNAP_TRAPS = "".trim();
		}
		if (checkBoxSET_LIVE_CAGES.isChecked()) {
			strCheckSET_LIVE_CAGES = checkBoxSET_LIVE_CAGES.getText()
					.toString();
			strSET_LIVE_CAGES = edt_SET_LIVE_CAGES.getText().toString().trim();

			strCheckChildSET_LIVE_CAGES = "";

			for (int i = 0; i < liveCagesCheckboxList.size(); i++) {
				// LoggerUtil.e("inside for loop 1..", "inside for loop 1..");
				if (liveCagesCheckboxList.get(i).isChecked()) {
					strCheckChildSET_LIVE_CAGES = strCheckChildSET_LIVE_CAGES
							+ ","
							+ liveCagesCheckboxList.get(i).getText().toString();
				}
			}
			strCheckChildSET_LIVE_CAGES = CommonFunction.removeFirstCharIF(
					strCheckChildSET_LIVE_CAGES, ",").trim();
		} else {
			strCheckSET_LIVE_CAGES = "".trim();
		}
	}

	private void getValuesForInvoice() {
		// TODO Auto-generated method stub.getC
		strEdttxtTechComments = edtTechComment.getText().toString().trim();
		strEdttxtServiceTech = edtServiceTech.getText().toString().trim();
		strEdttxtEmp = edtEMP.getText().toString().trim();
		strEdttxtCustomer = edtCustomer.getText().toString().trim();
		// strEdttxtdate=edttxtdate.getText().toString();
		strEdttxtdate = generalInfoDTO.getServiceDate().trim();
		strEdttxtInvValue = edtIntValue.getText().toString().trim();
		strEdttxtProdValue = edtProductvalue.getText().toString().trim();
		strEdttxtTax = edtTaxvalue.getText().toString().trim();
		strEdttxtTotal = edtTotal.getText().toString().trim();

		// strEdttxtAmount=edttxtAmount.getText().toString().trim();
		if (!edtAmount.getText().toString().trim().equals("")) {
			strEdttxtAmount = edtAmount.getText().toString().trim();
		} else {
			strEdttxtAmount = "0";
		}
		strEdttxtDrivingLicence = edtDRIVING_LICENCE.getText().toString()
				.trim();
		strEdttxtCheckNo = edtCheckNo.getText().toString().trim();
		strEdtExpirationDate = edtEXPIRATION_DATE.getText().toString().trim();

		int idInvoiceRadio = radioGroupInvoice.getCheckedRadioButtonId();
		RadioButton radioButtonInvoice = (RadioButton) findViewById(idInvoiceRadio);
		strRadioInvoice = radioButtonInvoice.getText().toString().trim();
		// Log.e("strRadioInvoice..get value.",strRadioInvoice+"");
	}

	private void getValuesForRecommendations() {
		// TODO Auto-generated method stub
		strRecommendations = "";
		for (int i = 0; i < recommendationCheckboxList.size(); i++) {
			if (recommendationCheckboxList.get(i).isChecked()) {

				strRecommendations = strRecommendations
						+ ","
						+ recommendationCheckboxList.get(i).getText()
								.toString().trim();

			}
		}
		strRecommendations = CommonFunction.removeFirstCharIF(
				strRecommendations, ",").trim();
		LoggerUtil.e("strRecommendations", "" + strRecommendations);
	}

	private void getValuesMosquitoServices() {
		// TODO Auto-generated method stub
		strMosquito = "";

		for (int i = 0; i < mosquitoCheckboxList.size(); i++) {
			// LoggerUtil.e("inside for loop 1..", "inside for loop 1..");
			if (mosquitoCheckboxList.get(i).isChecked()) {

				strMosquito = strMosquito
						+ ","
						+ mosquitoCheckboxList.get(i).getText().toString()
								.trim();

			}
		}
		strMosquito = CommonFunction.removeFirstCharIF(strMosquito, ",").trim();
		if (mosquitoCheckboxList.get(6).isChecked()) {
			strMosquitoService1 = edtMosquitoService1.getText().toString()
					.trim();
		} else {
			strMosquitoService1 = "";
		}
		if (mosquitoCheckboxList.get(7).isChecked()) {
			strMosquitoService2 = edtMosquitoService2.getText().toString()
					.trim();
		} else {
			strMosquitoService2 = "";
		}

	}

	private void getValuesTermiteServices() {
		// TODO Auto-generated method stub
		if (checkBoxVISIBLE_SIGN_TERMITE_MONITORING_STATIONS.isChecked()) {
			strCheckSignOfTermiteMS = checkBoxVISIBLE_SIGN_TERMITE_MONITORING_STATIONS
					.getText().toString();

			int idSignOfTermiteMS = radioGroupVISIBLE_SIGN_TERMITE_MONITORING_STATIONS
					.getCheckedRadioButtonId();
			if (idSignOfTermiteMS != -1) {
				// Log.e("idFenceInspected", ""+idFenceInspected);

				RadioButton radioSignOfTermiteMS = (RadioButton) findViewById(idSignOfTermiteMS);
				strSignOfTermiteMS = radioSignOfTermiteMS.getText().toString();
			}

			strStationSignOfTermiteMS = edt_station_VISIBLE_SIGN_TERMITE_MONITORING_STATIONS
					.getText().toString();
		} else {
			strCheckSignOfTermiteMS = "".trim();
		}

		if (checkBoxINSTALLED_MONITORING_STATIONS.isChecked()) {
			strCheckINSTALLED_MS = checkBoxINSTALLED_MONITORING_STATIONS
					.getText().toString();
			strINSTALLED_MONITORING_STATIONS = edtINSTALLED_MONITORING_STATIONS
					.getText().toString();
		} else {
			strCheckINSTALLED_MS = "".trim();
		}
		if (checkBoxVISIBLE_SIGN_PERIMETER_STRUCTURE.isChecked()) {
			strCheckSignOfTermitePS = checkBoxVISIBLE_SIGN_PERIMETER_STRUCTURE
					.getText().toString();

			int idSignOfTermitePS = radioGroupVISIBLE_SIGN_PERIMETER_STRUCTURE
					.getCheckedRadioButtonId();
			if (idSignOfTermitePS != -1) {
				// Log.e("idFenceInspected", ""+idFenceInspected);

				RadioButton radioSignOfTermitePS = (RadioButton) findViewById(idSignOfTermitePS);
				strSignOfTermitePS = radioSignOfTermitePS.getText().toString();
			}

		} else {
			strCheckSignOfTermitePS = "".trim();
		}
		if (checkBoxVISIBLE_SIGN_INTERIOR_STRUCTURE.isChecked()) {
			strCheckSignOfTermiteIS = checkBoxVISIBLE_SIGN_INTERIOR_STRUCTURE
					.getText().toString();

			int idSignOfTermiteIS = radioGroupVISIBLE_SIGN_INTERIOR_STRUCTURE
					.getCheckedRadioButtonId();
			if (idSignOfTermiteIS != -1) {
				// Log.e("idFenceInspected", ""+idFenceInspected);

				RadioButton radioSignOfTermiteIS = (RadioButton) findViewById(idSignOfTermiteIS);
				strSignOfTermiteIS = radioSignOfTermiteIS.getText().toString();
			}

		} else {
			strCheckSignOfTermiteIS = "".trim();
		}
		if (checkBoxREPLACED_MONITORING_STATIONS.isChecked()) {
			strCheckREPLACED_MS = checkBoxREPLACED_MONITORING_STATIONS
					.getText().toString();
			strStationREPLACED_MS = edt_station_REPLACED_MONITORING_STATIONS
					.getText().toString();
			strREPLACED_MS = edtREPLACED_MONITORING_STATIONS.getText()
					.toString();

		} else {
			strCheckREPLACED_MS = "".trim();
		}

		if (checkBoxTREATED_PERIMETER_STRUCTURE.isChecked()) {
			strCheckTreatedPS = checkBoxTREATED_PERIMETER_STRUCTURE.getText()
					.toString();

			int idTreatedPS = radioGroupTREATED_PERIMETER_STRUCTURE
					.getCheckedRadioButtonId();
			if (idTreatedPS != -1) {
				// Log.e("idFenceInspected", ""+idFenceInspected);

				RadioButton radioTreatedPS = (RadioButton) findViewById(idTreatedPS);
				strTreatedPS = radioTreatedPS.getText().toString();
			}

		} else {
			strCheckTreatedPS = "".trim();
		}

		if (checkBoxTREATED_SLAB_PENETRATIONS.isChecked()) {
			strCheckTreatedSP = checkBoxTREATED_SLAB_PENETRATIONS.getText()
					.toString();

			int idTreatedSP = radioGroupTREATED_SLAB_PENETRATIONS
					.getCheckedRadioButtonId();
			if (idTreatedSP != -1) {
				// Log.e("idFenceInspected", ""+idFenceInspected);

				RadioButton radioTreatedSP = (RadioButton) findViewById(idTreatedSP);
				strTreatedSP = radioTreatedSP.getText().toString();
			}

		} else {

			strCheckTreatedSP = "".trim();
		}

		if (checkBoxTREATED_CONDUSIVE_CONDITIONS.isChecked()) {
			strCheckTreatedCC = checkBoxTREATED_CONDUSIVE_CONDITIONS.getText()
					.toString();

			int idTreatedCC = radioGroupTREATED_CONDUSIVE_CONDITIONS
					.getCheckedRadioButtonId();
			if (idTreatedCC != -1) {
				// Log.e("idFenceInspected", ""+idFenceInspected);

				RadioButton radioTreatedCC = (RadioButton) findViewById(idTreatedCC);
				strTreatedCC = radioTreatedCC.getText().toString();
			}

		} else {

			strCheckTreatedCC = "".trim();
		}
	}

	private void getValuesPestControlServices() {
		// TODO Auto-generated method stub
		strPestControlServices = "";
		strOutsidepestControlServices = "";
		strInsidepestControlServices = "";

		for (int i = 0; i < pestControlServicesCheckboxList.size(); i++) {
			// LoggerUtil.e("inside for loop 1..", "inside for loop 1..");
			if (pestControlServicesCheckboxList.get(i).isChecked()) {
				strPestControlServices = strPestControlServices
						+ ","
						+ pestControlServicesCheckboxList.get(i).getText()
								.toString();
			}
		}
		strPestControlServices = CommonFunction.removeFirstCharIF(
				strPestControlServices, ",").trim();

		for (int i = 0; i < OutsidepestControlServicesCheckboxList.size(); i++) {
			// LoggerUtil.e("inside for loop 1..", "inside for loop 1..");
			if (OutsidepestControlServicesCheckboxList.get(i).isChecked()) {
				strOutsidepestControlServices = strOutsidepestControlServices
						+ ","
						+ OutsidepestControlServicesCheckboxList.get(i)
								.getText().toString();
			}
		}
		strOutsidepestControlServices = CommonFunction.removeFirstCharIF(
				strOutsidepestControlServices, ",").trim();

		for (int i = 0; i < InsidepestControlServicesCheckboxList.size(); i++) {
			// LoggerUtil.e("inside for loop 1..", "inside for loop 1..");
			if (InsidepestControlServicesCheckboxList.get(i).isChecked()) {
				strInsidepestControlServices = strInsidepestControlServices
						+ ","
						+ InsidepestControlServicesCheckboxList.get(i)
								.getText().toString();
			}
		}
		strInsidepestControlServices = CommonFunction.removeFirstCharIF(
				strInsidepestControlServices, ",").trim();

	}

	private void getValuesPestActivitybyZone() {
		// TODO Auto-generated method stub
		strpestActivityByZone = "";

		for (int i = 0; i < PestActivityZoneCheckboxList.size(); i++) {
			// LoggerUtil.e("inside for loop 1..", "inside for loop 1..");
			// strpestActivityByZone="";
			if (PestActivityZoneCheckboxList.get(i).isChecked()) {
				strpestActivityByZone = strpestActivityByZone
						+ ","
						+ PestActivityZoneCheckboxList.get(i).getText()
								.toString();
			}
		}
		strpestActivityByZone = CommonFunction.removeFirstCharIF(
				strpestActivityByZone, ",").trim();
		// Log.e("", "strpestActivityByZone..get."+strpestActivityByZone);

		strInterior = edtINTERIOR.getText().toString().trim();
		strOutsidef_Perimeter = edtOUTSIDE_PERIMETER.getText().toString()
				.trim();
		strDumpster = edtDUMPSTER.getText().toString().trim();
		strDining_Room = edtDINING_ROOM.getText().toString().trim();
		strCommon_Areas = edtCOMMON_AREAS.getText().toString().trim();
		strKitchen = edtKITCHEN_PestActivityByZone.getText().toString().trim();
		strDry_Storage = edtDRY_STORAGE.getText().toString().trim();
		strDish_Washing = edtDISH_WSHING.getText().toString().trim();
		strRoof_Tops = edtROOF_TOPS.getText().toString().trim();
		strWait_Stations = edtWAIT_STATION.getText().toString().trim();
		strDrop_Ceiling = edtDROP_CEILINGS.getText().toString().trim();
		strPlanters = edtPLANTERS.getText().toString().trim();
		strWarehouse_Storage = edtWAREHOUSE_STORAGE.getText().toString().trim();

	}

	private void grtValuesforInspectedTreatedFor() {
		// TODO Auto-generated method stub
		strInspectedadnTreatedFor = "";
		for (int i = 0; i < InspectedTreatedForCheckboxList.size(); i++) {

			if (InspectedTreatedForCheckboxList.get(i).isChecked()) {
				strInspectedadnTreatedFor = strInspectedadnTreatedFor
						+ ","
						+ InspectedTreatedForCheckboxList.get(i).getText()
								.toString();
			}
		}
		strInspectedadnTreatedFor = CommonFunction.removeFirstCharIF(
				strInspectedadnTreatedFor, ",").trim();
		// LoggerUtil.e("inside for loop 1..",
		// "strInspectedadnTreatedFor...."+strInspectedadnTreatedFor);
		// strAnts=checkBoxAnts.getText().toString();
		// strOtherPest=checkBoxOtherPests.getText().toString();
		strEdttxtInspectedandTreatedFor1 = edtiNSPECTEDANDtREATEDFOR1.getText()
				.toString().trim();
		strEdttxtInspectedandTreatedFor2 = edtiNSPECTEDANDtREATEDFOR2.getText()
				.toString().trim();
		// Log.e("",
		// "strEdttxtInspectedandTreatedFor1...."+strEdttxtInspectedandTreatedFor1+"..."+strEdttxtInspectedandTreatedFor2);
	}

	private void getValuesformAreasInspected() {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		strAreasInspectedExterior = "";
		strAreasInspectedInterior = "";

		for (int i = 0; i < AreasInspectedInteriorCheckboxList.size(); i++) {
			// LoggerUtil.e("inside for loop 1..", "inside for loop 1..");

			if (AreasInspectedInteriorCheckboxList.get(i).isChecked()) {

				strAreasInspectedInterior = strAreasInspectedInterior
						+ ","
						+ AreasInspectedInteriorCheckboxList.get(i).getText()
								.toString();
			}
		}
		strAreasInspectedInterior = CommonFunction.removeFirstCharIF(
				strAreasInspectedInterior, ",").trim();

		for (int i = 0; i < AreasInspectedExteriorCheckboxList.size(); i++) {
			// LoggerUtil.e("inside for loop 1..", "inside for loop 1..");
			if (AreasInspectedExteriorCheckboxList.get(i).isChecked()) {

				strAreasInspectedExterior = strAreasInspectedExterior
						+ ","
						+ AreasInspectedExteriorCheckboxList.get(i).getText()
								.toString();
			}
		}
		strAreasInspectedExterior = CommonFunction.removeFirstCharIF(
				strAreasInspectedExterior, ",").trim();
	}

	private void getValuesFromServiceForHeader() {
		// TODO Auto-generated method stub
		strServiceFor = "";

		for (int i = 0; i < serviceForCheckboxList.size(); i++) {
			// LoggerUtil.e("inside for loop 1..", "inside for loop 1..");
			if (serviceForCheckboxList.get(i).isChecked()) {
				strServiceFor = strServiceFor + ","
						+ serviceForCheckboxList.get(i).getText().toString();
			}
		}
		strServiceFor = CommonFunction.removeFirstCharIF(strServiceFor, ",")
				.trim();
	}

	private void validateComponents() {
		// TODO Auto-generated method stub
		if (radioCreditcard.isChecked()) {
			if (edtAmount.getText().toString().equals("")
					|| edtAmount.getText().toString().equals("0")) {
				validationList.add("Enter Amount.");
				objectFocus = edtAmount;
				isValidated = false;
			} else if (edtAmount.getText().toString().contains(".")
					&& edtAmount.getText().toString().length() == 1) {
				validationList.add("Amount format should be: 00.00");
				objectFocus = edtAmount;
				isValidated = false;
			}
		}
		if (radioCash.isChecked()) {
			if (edtAmount.getText().toString().equals("")
					|| edtAmount.getText().toString().equals("0")) {
				validationList.add("Enter Amount.");
				objectFocus = edtAmount;
				isValidated = false;
			} else if (edtAmount.getText().toString().contains(".")
					&& edtAmount.getText().toString().length() == 1) {
				validationList.add("Amount format should be: 00:00");
				objectFocus = edtAmount;
				isValidated = false;
			}
		}
		if (radioCheck.isChecked()) {
			if (edtAmount.getText().toString().equals("")
					|| edtAmount.getText().toString().equals("0")) {
				validationList.add("Enter Amount.");
				objectFocus = edtAmount;
				isValidated = false;
			}
			if (edtAmount.getText().toString().contains(".")
					&& edtAmount.getText().toString().length() == 1) {
				validationList.add("Amount format should be: 00:00");
				objectFocus = edtAmount;
				isValidated = false;
			}
			if (strEdttxtCheckNo.equals("")) {
				validationList.add("Enter Check#.");
				objectFocus = edtCheckNo;
				isValidated = false;
			}
			if (strEdttxtDrivingLicence.equals("")) {
				validationList.add("Enter Driving  License#.");
				objectFocus = edtDRIVING_LICENCE;
				isValidated = false;
			}
			if (edtEXPIRATION_DATE.getText().toString().equals("")) {
				validationList.add("Enter Expiration Date.");
				objectFocus = edtEXPIRATION_DATE;
				isValidated = false;
			}
			if (!edtEXPIRATION_DATE.getText().toString().equals("")) {
				try {
					SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

					Date dateCurrent = sdf.parse(CommonFunction
							.getCurrentDate());
					Date dateExpiry = sdf.parse(edtEXPIRATION_DATE.getText()
							.toString());
					if (dateExpiry.before(dateCurrent)) {
						validationList
								.add("Expiration Date should not be smaller then Today.");
						objectFocus = edtEXPIRATION_DATE;
						isValidated = false;
					}
				} catch (Exception e) {

				}
			}
			// Log.e("",
			// "generalInfoDTO.getCheckFrontImage().inside validate.."+generalInfoDTO.getCheckFrontImage());
			if (CommonFunction.checkString(generalInfoDTO.getCheckFrontImage(),
					"").equals("")) {
				// Log.e("", "ImageNameFront..."+ImageNameFront);
				validationList.add("Upload Check Front Image.");
				isValidated = false;
			}
			if (CommonFunction.checkString(generalInfoDTO.getCheckBackImage(),
					"").equals("")) {
				// Log.e("", "ImageNameBack..."+ImageNameBack);
				validationList.add("Upload Check Back Image.");
				isValidated = false;
			}
		}
		Log.e("edtMosquitoService1", ""
				+ edtMosquitoService1.getText().toString());
		if (checkBoxCHECKED_RUN_DURACTION_TIME.isChecked()) {
			if (edtMosquitoService1.getText().toString().equals("")) {
				validationList
						.add("Enter Checked Duration Time Under Mosquito Services Tab.");
				objectFocus = edtMosquitoService1;
				isValidated = false;
			}
		}
		if (checkBoxCHECKED_TIME_SYSTEM_RUNS.isChecked()) {
			if (edtMosquitoService2.getText().toString().equals("")) {
				validationList
						.add("Enter Checked Run Time System Runs Mosquito Services Tab.");
				objectFocus = edtMosquitoService2;
				isValidated = false;
			}
		}

		if (checkBoxOTHER_PESTS.isChecked()
				&& CommonFunction.checkString(
						edtiNSPECTEDANDtREATEDFOR2.getText().toString().trim(),
						"").equals("")) {
			// Log.e("", "ImageNameFront..."+ImageNameFront);
			validationList
					.add("Enter Number of Other Pests under Inspected and Treated For Tab");
			objectFocus = edtiNSPECTEDANDtREATEDFOR2;
			isValidated = false;
		}
		/*if (checkBoxANTS.isChecked()
				&& CommonFunction.checkString(
						edtiNSPECTEDANDtREATEDFOR1.getText().toString().trim(),
						"").equals("")) {
			// Log.e("", "ImageNameFront..."+ImageNameFront);
			validationList
					.add("Enter Number of Ants under Inspected and Treated For Tab");
			objectFocus = edtiNSPECTEDANDtREATEDFOR1;
			isValidated = false;
		}*/
		validateUnderChemicalTable();
	}

	public void validateUnderChemicalTable() {
		// TODO Auto-generated method stub
		for (int i = 0; i < checkList.size(); i++) {
			if (checkList.get(i).isChecked()) {
				if (listChemicals.get(i).getPercentPes().contains(".")
						&& listChemicals.get(i).getPercentPes().length() == 1) {
					validationList
							.add("Please enter some Percent under the chemical product: "
									+ listChemicals.get(i).getProduct());
					isValidated = false;
					// break;
				}
				if (listChemicals.get(i).getAmount().contains(".")
						&& listChemicals.get(i).getAmount().length() == 1) {
					validationList
							.add("Please enter some Amount under the chemical product: "
									+ listChemicals.get(i).getProduct());
					isValidated = false;
					// break;
				}
				if (!listChemicals.get(i).getPercentPes().equals("")
						&& Float.valueOf(listChemicals.get(i).getPercentPes()) > 100.00) {
					validationList
							.add("Percent value cannot be greater than 100 under the chemical product: "
									+ listChemicals.get(i).getProduct());
					isValidated = false;
				}
			}
		}
	}

	public class OnCheckedboxChangeListener implements
			android.widget.CompoundButton.OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
			switch (buttonView.getId()) {
			case R.id.checkBoxTREATED_INSPECT_DROP_CEILINGS:
				if (checkBoxTREATED_INSPECT_DROP_CEILINGS.isChecked()) {
					checkBoxINTERIOR_PEST_CONTROL_SERVICES.setChecked(true);
				}
				break;

			case R.id.checkBoxTREATED_KITCHEN:
				if (checkBoxTREATED_KITCHEN.isChecked()) {
					checkBoxINTERIOR_PEST_CONTROL_SERVICES.setChecked(true);
				}
				break;

			case R.id.checkBoxTREATED_LAUNDRY:
				if (checkBoxTREATED_LAUNDRY.isChecked()) {
					checkBoxINTERIOR_PEST_CONTROL_SERVICES.setChecked(true);
				}
				break;
			case R.id.checkBoxTREATED_INSPECT_OFFICES:
				if (checkBoxTREATED_INSPECT_OFFICES.isChecked()) {
					checkBoxINTERIOR_PEST_CONTROL_SERVICES.setChecked(true);
				}
				break;
			case R.id.checkBoxTREATED_INSPECT_ROOF_TOPS:
				if (checkBoxTREATED_INSPECT_ROOF_TOPS.isChecked()) {
					checkBoxINTERIOR_PEST_CONTROL_SERVICES.setChecked(true);
				}
				break;
			case R.id.checkBoxTREATED_ZONE_MONITORS:
				if (checkBoxTREATED_ZONE_MONITORS.isChecked()) {
					checkBoxINTERIOR_PEST_CONTROL_SERVICES.setChecked(true);
				}
				break;
			case R.id.checkBoxTREATED_INSPECT_COMMON_AREAS:
				if (checkBoxTREATED_INSPECT_COMMON_AREAS.isChecked()) {
					checkBoxINTERIOR_PEST_CONTROL_SERVICES.setChecked(true);
				}
				break;
			case R.id.checkBoxTREATED_fRONT_PERIMETRE:
				if (checkBoxTREATED_fRONT_PERIMETRE.isChecked()) {
					checkBoxEXTERIOR_PEST_CONTROL_SERVICES.setChecked(true);
				}
				break;

			case R.id.checkBoxTREATED_BACK_PERIMETRE:
				if (checkBoxTREATED_BACK_PERIMETRE.isChecked()) {
					checkBoxEXTERIOR_PEST_CONTROL_SERVICES.setChecked(true);
				}
				break;
			case R.id.checkBoxTREaTED_WHEPHOLES:
				if (checkBoxTREaTED_WHEPHOLES.isChecked()) {
					checkBoxEXTERIOR_PEST_CONTROL_SERVICES.setChecked(true);
				}
				break;
			case R.id.checkBoxTREATED_CRACKS:
				if (checkBoxTREATED_CRACKS.isChecked()) {
					checkBoxEXTERIOR_PEST_CONTROL_SERVICES.setChecked(true);
				}
				break;
			case R.id.checkBoxTREATED_HARBOURAGE_BREEDING_SITES:
				if (checkBoxTREATED_HARBOURAGE_BREEDING_SITES.isChecked()) {
					checkBoxEXTERIOR_PEST_CONTROL_SERVICES.setChecked(true);
				}
				break;
			case R.id.checkBoxTREATED_FIREANTS_LANDSCAPE:
				if (checkBoxTREATED_FIREANTS_LANDSCAPE.isChecked()) {
					checkBoxEXTERIOR_PEST_CONTROL_SERVICES.setChecked(true);
				}
				break;
			case R.id.checkBoxTREATED_DOORWAYS_windows:
				if (checkBoxTREATED_DOORWAYS_windows.isChecked()) {
					checkBoxEXTERIOR_PEST_CONTROL_SERVICES.setChecked(true);
				}
				break;

			case R.id.checkBoxINTERIOR_PEST_CONTROL_SERVICES:
				if (checkBoxINTERIOR_PEST_CONTROL_SERVICES.isChecked()) {
					// checkBoxExteriorPestControl.setChecked(true);
				} else {
					for (int c = 0; c < InsidepestControlServicesCheckboxList
							.size(); c++) {
						InsidepestControlServicesCheckboxList.get(c)
								.setChecked(false);
					}
				}

				break;
			case R.id.checkBoxEXTERIOR_PEST_CONTROL_SERVICES:
				if (checkBoxEXTERIOR_PEST_CONTROL_SERVICES.isChecked()) {
					// checkBoxExteriorPestControl.setChecked(true);
				} else {
					for (int c = 0; c < OutsidepestControlServicesCheckboxList
							.size(); c++) {
						OutsidepestControlServicesCheckboxList.get(c)
								.setChecked(false);
					}
				}

				break;
			case R.id.checkBoxINSPECTED_SNAP_TRAPS:
				if (!checkBoxINSPECTED_SNAP_TRAPS.isChecked()) {

					tableRow_INSPECTED_SNAP_TRAPS.setVisibility(View.GONE);
					edt_how_many_INSPECTED_SNAP_TRAPS.getText().clear();

				} else {

					tableRow_INSPECTED_SNAP_TRAPS.setVisibility(View.VISIBLE);
					if (checkChange == 1) {
						edt_how_many_INSPECTED_SNAP_TRAPS.requestFocus();
					}
				}

				break;

			case R.id.checkBoxINSPECTED_LIVE_CAGES:
				if (!checkBoxINSPECTED_LIVE_CAGES.isChecked()) {

					tableRow_INSPECTED_LIVE_CAGES.setVisibility(View.GONE);
					edt_how_many_INSPECTED_LIVE_CAGES.getText().clear();

				} else {

					tableRow_INSPECTED_LIVE_CAGES.setVisibility(View.VISIBLE);
					if (checkChange == 1) {
						edt_how_many_INSPECTED_LIVE_CAGES.requestFocus();
					}
				}

				break;

			case R.id.checkBoxREMOVED_SNAP_TRAPS:
				if (!checkBoxREMOVED_SNAP_TRAPS.isChecked()) {

					tableRow_REMOVED_SNAP_TRAPS.setVisibility(View.GONE);
					edt_how_many_REMOVED_SNAP_TRAPS.getText().clear();

				} else {
					tableRow_REMOVED_SNAP_TRAPS.setVisibility(View.VISIBLE);
					if (checkChange == 1) {
						edt_how_many_REMOVED_SNAP_TRAPS.requestFocus();
					}
				}

				break;

			case R.id.checkBoxREMOVED_LIVE_CAGES:
				if (!checkBoxREMOVED_LIVE_CAGES.isChecked()) {
					tableRow_REMOVED_LIVE_CAGES.setVisibility(View.GONE);
					edt_how_many_REMOVED_LIVE_CAGES.getText().clear();

				} else {

					tableRow_REMOVED_LIVE_CAGES.setVisibility(View.VISIBLE);
					if (checkChange == 1) {
						edt_how_many_REMOVED_LIVE_CAGES.requestFocus();
					}
				}

				break;
			case R.id.checkBoxREMOVED_RODENT_BAIT_STATIONS:
				if (!checkBoxREMOVED_RODENT_BAIT_STATIONS.isChecked()) {

					tableRow_REMOVED_RODENT_BAIT_STATIONS
							.setVisibility(View.GONE);
					edt_how_many_REMOVED_RODENT_BAIT_STATIONS.getText().clear();

				} else {

					tableRow_REMOVED_RODENT_BAIT_STATIONS
							.setVisibility(View.VISIBLE);
					if (checkChange == 1) {
						edt_how_many_REMOVED_RODENT_BAIT_STATIONS
								.requestFocus();
					}
				}

				break;

			case R.id.checkBoxREMOVED_RODENT_TRAPS:
				if (!checkBoxREMOVED_RODENT_TRAPS.isChecked()) {

					tableRow_REMOVED_RODENT_TRAPS.setVisibility(View.GONE);
					edt_how_many_REMOVED_RODENT_TRAPS.getText().clear();

				} else {

					tableRow_REMOVED_RODENT_TRAPS.setVisibility(View.VISIBLE);
					if (checkChange == 1) {
						edt_how_many_REMOVED_RODENT_TRAPS.requestFocus();
					}
				}

				break;

			case R.id.checkBoxSET_SNAP_TRAPS:
				if (!checkBoxSET_SNAP_TRAPS.isChecked()) {

					tableRow_SET_SNAP_TRAPS1.setVisibility(View.GONE);
					tableRow_SET_SNAP_TRAPS2.setVisibility(View.GONE);
					tableRow_SET_SNAP_TRAPS3.setVisibility(View.GONE);

					edt_SET_SNAP_TRAPS.getText().clear();
					checkboxSET_SNAP_TRAPS1.setChecked(false);
					checkboxSET_SNAP_TRAPS2.setChecked(false);
					checkboxSET_SNAP_TRAPS3.setChecked(false);
					checkboxSET_SNAP_TRAPS4.setChecked(false);
				} else {

					tableRow_SET_SNAP_TRAPS1.setVisibility(View.VISIBLE);
					tableRow_SET_SNAP_TRAPS2.setVisibility(View.VISIBLE);
					tableRow_SET_SNAP_TRAPS3.setVisibility(View.VISIBLE);

					if (checkChange == 1) {
						edt_SET_SNAP_TRAPS.requestFocus();
					}
				}

				break;
			case R.id.checkboxSET_SNAP_TRAPS1:
				if (!checkboxSET_SNAP_TRAPS1.isChecked()) {
					if (edt_SET_LIVE_CAGES.getText().toString().equals("")
							&& !checkboxSET_SNAP_TRAPS2.isChecked()
							&& !checkboxSET_SNAP_TRAPS3.isChecked()
							&& !checkboxSET_SNAP_TRAPS4.isChecked()) {
						checkBoxSET_SNAP_TRAPS.setChecked(false);
					}
				} else {
					if (checkChange == 1) {
						edt_SET_SNAP_TRAPS.requestFocus();
					}
				}

				break;
			case R.id.checkboxSET_SNAP_TRAPS2:
				if (!checkboxSET_SNAP_TRAPS2.isChecked()) {
					if (edt_SET_LIVE_CAGES.getText().toString().equals("")
							&& !checkboxSET_SNAP_TRAPS1.isChecked()
							&& !checkboxSET_SNAP_TRAPS3.isChecked()
							&& !checkboxSET_SNAP_TRAPS4.isChecked()) {
						checkBoxSET_SNAP_TRAPS.setChecked(false);
					}
				} else {
					if (checkChange == 1) {
						edt_SET_SNAP_TRAPS.requestFocus();
					}
				}

				break;
			case R.id.checkboxSET_SNAP_TRAPS3:
				if (!checkboxSET_SNAP_TRAPS3.isChecked()) {
					if (edt_SET_LIVE_CAGES.getText().toString().equals("")
							&& !checkboxSET_SNAP_TRAPS2.isChecked()
							&& !checkboxSET_SNAP_TRAPS1.isChecked()
							&& !checkboxSET_SNAP_TRAPS4.isChecked()) {
						checkBoxSET_SNAP_TRAPS.setChecked(false);
					}
				} else {
					if (checkChange == 1) {
						edt_SET_SNAP_TRAPS.requestFocus();
					}
				}

				break;
			case R.id.checkboxSET_SNAP_TRAPS4:
				if (!checkboxSET_SNAP_TRAPS4.isChecked()) {
					if (edt_SET_LIVE_CAGES.getText().toString().equals("")
							&& !checkboxSET_SNAP_TRAPS2.isChecked()
							&& !checkboxSET_SNAP_TRAPS3.isChecked()
							&& !checkboxSET_SNAP_TRAPS1.isChecked()) {
						checkBoxSET_SNAP_TRAPS.setChecked(false);
					}
				} else {
					if (checkChange == 1) {
						edt_SET_SNAP_TRAPS.requestFocus();
					}
				}

				break;
			case R.id.checkBoxSET_LIVE_CAGES:
				if (!checkBoxSET_LIVE_CAGES.isChecked()) {

					tableRow_SET_LIVE_CAGES1.setVisibility(View.GONE);
					tableRow_SET_LIVE_CAGES2.setVisibility(View.GONE);
					tableRow_SET_LIVE_CAGES3.setVisibility(View.GONE);

					edt_SET_LIVE_CAGES.getText().clear();
					checkboxSET_LIVE_CAGES1.setChecked(false);
					checkboxSET_LIVE_CAGES2.setChecked(false);
					checkboxSET_LIVE_CAGES3.setChecked(false);
					checkboxSET_LIVE_CAGES4.setChecked(false);
				} else {

					tableRow_SET_LIVE_CAGES1.setVisibility(View.VISIBLE);
					tableRow_SET_LIVE_CAGES2.setVisibility(View.VISIBLE);
					tableRow_SET_LIVE_CAGES3.setVisibility(View.VISIBLE);
					if (checkChange == 1) {
						edt_SET_LIVE_CAGES.requestFocus();
					}
				}

				break;
			case R.id.checkboxSET_LIVE_CAGES1:
				if (!checkboxSET_LIVE_CAGES1.isChecked()) {
					if (edt_SET_LIVE_CAGES.getText().toString().equals("")
							&& !checkboxSET_LIVE_CAGES2.isChecked()
							&& !checkboxSET_LIVE_CAGES3.isChecked()
							&& !checkboxSET_LIVE_CAGES4.isChecked()) {
						checkBoxSET_LIVE_CAGES.setChecked(false);
					}
				} else {
					if (checkChange == 1) {
						edt_SET_LIVE_CAGES.requestFocus();
					}
				}

				break;

			case R.id.checkboxSET_LIVE_CAGES2:
				if (!checkboxSET_LIVE_CAGES2.isChecked()) {
					if (edt_SET_LIVE_CAGES.getText().toString().equals("")
							&& !checkboxSET_LIVE_CAGES1.isChecked()
							&& !checkboxSET_LIVE_CAGES3.isChecked()
							&& !checkboxSET_LIVE_CAGES4.isChecked()) {
						checkBoxSET_LIVE_CAGES.setChecked(false);
					}
				} else {
					if (checkChange == 1) {
						edt_SET_LIVE_CAGES.requestFocus();
					}
				}

				break;
			case R.id.checkboxSET_LIVE_CAGES3:
				if (!checkboxSET_LIVE_CAGES3.isChecked()) {
					if (edt_SET_LIVE_CAGES.getText().toString().equals("")
							&& !checkboxSET_LIVE_CAGES2.isChecked()
							&& !checkboxSET_LIVE_CAGES1.isChecked()
							&& !checkboxSET_LIVE_CAGES4.isChecked()) {
						checkBoxSET_LIVE_CAGES.setChecked(false);
					}
				} else {
					if (checkChange == 1) {
						edt_SET_LIVE_CAGES.requestFocus();
					}
				}

				break;
			case R.id.checkboxSET_LIVE_CAGES4:
				if (!checkboxSET_LIVE_CAGES4.isChecked()) {
					if (edt_SET_LIVE_CAGES.getText().toString().equals("")
							&& !checkboxSET_LIVE_CAGES2.isChecked()
							&& !checkboxSET_LIVE_CAGES3.isChecked()
							&& !checkboxSET_LIVE_CAGES1.isChecked()) {
						checkBoxSET_LIVE_CAGES.setChecked(false);
					}
				} else {
					if (checkChange == 1) {
						edt_SET_LIVE_CAGES.requestFocus();
					}
				}

				break;
			case R.id.checkBoxANTS:

				if (!checkBoxANTS.isChecked()) {
					edtiNSPECTEDANDtREATEDFOR1.getText().clear();
				} else {
					if (checkChange == 1) {
						edtiNSPECTEDANDtREATEDFOR1.requestFocus();
					}
				}

				break;
			case R.id.checkBoxOTHER_PESTS:
				if (!checkBoxOTHER_PESTS.isChecked()) {
					edtiNSPECTEDANDtREATEDFOR2.getText().clear();
				} else {
					if (checkChange == 1) {
						edtiNSPECTEDANDtREATEDFOR2.requestFocus();
					}
				}

				break;
			case R.id.checkBoxINTERIOR_PEST_ACTIVITY_ZONE:
				if (!checkBoxINTERIOR_PEST_ACTIVITY_ZONE.isChecked()) {
					edtINTERIOR.getText().clear();
				} else {
					if (checkChange == 1) {
						edtINTERIOR.requestFocus();
					}
				}

				break;
			case R.id.checkBoxOUTSIDE_PERIMETER:
				if (!checkBoxOUTSIDE_PERIMETER.isChecked()) {
					edtOUTSIDE_PERIMETER.getText().clear();
				} else {
					if (checkChange == 1) {
						edtOUTSIDE_PERIMETER.requestFocus();
					}
				}

				break;
			case R.id.checkBoxDUMPSTER_pest_activitybyzone:
				if (!checkBoxDUMPSTER_pest_activitybyzone.isChecked()) {
					edtDUMPSTER.getText().clear();
				} else {
					if (checkChange == 1) {
						edtDUMPSTER.requestFocus();
					}
				}

				break;
			case R.id.checkBoxDINING_ROOM_PestActivityByZone:
				if (!checkBoxDINING_ROOM_PestActivityByZone.isChecked()) {
					edtDINING_ROOM.getText().clear();
				} else {
					if (checkChange == 1) {
						edtDINING_ROOM.requestFocus();
					}
				}

				break;
			case R.id.checkBoxCOMMON_AREAS:
				if (!checkBoxCOMMON_AREAS.isChecked()) {
					edtCOMMON_AREAS.getText().clear();
				} else {
					if (checkChange == 1) {
						edtCOMMON_AREAS.requestFocus();
					}
				}

				break;
			case R.id.checkBoxKITCHEN_Pest_Activity:
				if (!checkBoxKITCHEN_Pest_Activity.isChecked()) {
					edtKITCHEN_PestActivityByZone.getText().clear();
				} else {
					if (checkChange == 1) {
						edtKITCHEN_PestActivityByZone.requestFocus();
					}
				}

				break;
			case R.id.checkBoxDRY_STORAGE_Pest_Activity:
				if (!checkBoxDRY_STORAGE_Pest_Activity.isChecked()) {
					edtDRY_STORAGE.getText().clear();
				} else {
					if (checkChange == 1) {
						edtDRY_STORAGE.requestFocus();
					}
				}

				break;
			case R.id.checkBoxDISH_WSHING_PEST_ACTIVTY:
				if (!checkBoxDISH_WSHING_PEST_ACTIVTY.isChecked()) {
					edtDISH_WSHING.getText().clear();
				} else {
					if (checkChange == 1) {
						edtDISH_WSHING.requestFocus();
					}
				}

				break;
			case R.id.checkboxROOF_TOPS_pestActivity_by_zone:
				if (!checkboxROOF_TOPS_pestActivity_by_zone.isChecked()) {
					edtROOF_TOPS.getText().clear();
				} else {
					if (checkChange == 1) {
						edtROOF_TOPS.requestFocus();
					}
				}

				break;

			case R.id.checkBoxWAIT_STATION_PEST_ACTIVTY:
				if (!checkBoxWAIT_STATION_PEST_ACTIVTY.isChecked()) {
					edtWAIT_STATION.getText().clear();
				} else {
					if (checkChange == 1) {
						edtWAIT_STATION.requestFocus();
					}
				}

				break;
			case R.id.checkBoxDROP_CEILINGS:
				if (!checkBoxDROP_CEILINGS.isChecked()) {
					edtDROP_CEILINGS.getText().clear();
				} else {
					if (checkChange == 1) {
						edtDROP_CEILINGS.requestFocus();
					}
				}

				break;
			case R.id.checkBoxPLANTERS_PestActivtyByZone:
				if (!checkBoxPLANTERS_PestActivtyByZone.isChecked()) {
					edtPLANTERS.getText().clear();
				} else {
					if (checkChange == 1) {
						edtPLANTERS.requestFocus();
					}
				}

				break;
			case R.id.checkBoxWAREHOUSE_STORAGE_PestActivtyByZone:
				if (!checkBoxWAREHOUSE_STORAGE_PestActivtyByZone.isChecked()) {
					edtWAREHOUSE_STORAGE.getText().clear();
				} else {
					if (checkChange == 1) {
						edtWAREHOUSE_STORAGE.requestFocus();
					}
				}

				break;

			case R.id.checkBoxVISIBLE_SIGN_TERMITE_MONITORING_STATIONS:
				if (!checkBoxVISIBLE_SIGN_TERMITE_MONITORING_STATIONS
						.isChecked()) {
					tableRowVISIBLE_SIGN_TERMITE_MONITORING_STATIONS1
							.setVisibility(View.GONE);
					tableRowVISIBLE_SIGN_TERMITE_MONITORING_STATIONS2
							.setVisibility(View.GONE);

					edt_station_VISIBLE_SIGN_TERMITE_MONITORING_STATIONS
							.getText().clear();
					radiobtnVISIBLE_SIGN_TERMITE_MONITORING_STATIONS1
							.setChecked(false);
					radiobtnVISIBLE_SIGN_TERMITE_MONITORING_STATIONS2
							.setChecked(false);
				} else {
					tableRowVISIBLE_SIGN_TERMITE_MONITORING_STATIONS1
							.setVisibility(View.VISIBLE);
					tableRowVISIBLE_SIGN_TERMITE_MONITORING_STATIONS2
							.setVisibility(View.VISIBLE);

					if (checkChange == 1) {
						edt_station_VISIBLE_SIGN_TERMITE_MONITORING_STATIONS
								.requestFocus();
					}
				}

				break;
			case R.id.checkBoxINSTALLED_MONITORING_STATIONS:
				if (!checkBoxINSTALLED_MONITORING_STATIONS.isChecked()) {

					tableRowINSTALLED_MONITORING_STATIONS
							.setVisibility(View.GONE);

					edtINSTALLED_MONITORING_STATIONS.getText().clear();
				} else {
					tableRowINSTALLED_MONITORING_STATIONS
							.setVisibility(View.VISIBLE);
					if (checkChange == 1) {
						edtINSTALLED_MONITORING_STATIONS.requestFocus();
					}
				}

				break;
			case R.id.checkBoxVISIBLE_SIGN_PERIMETER_STRUCTURE:
				if (!checkBoxVISIBLE_SIGN_PERIMETER_STRUCTURE.isChecked()) {

					tableRowVISIBLE_SIGN_PERIMETER_STRUCTURE
							.setVisibility(View.GONE);
					radioGroupVISIBLE_SIGN_PERIMETER_STRUCTURE.clearCheck();
					// radiobtnVISIBLE_SIGN_PERIMETER_STRUCTURE1.setChecked(false);
					// radiobtnVISIBLE_SIGN_PERIMETER_STRUCTURE2.setChecked(false);
				} else {
					tableRowVISIBLE_SIGN_PERIMETER_STRUCTURE
							.setVisibility(View.VISIBLE);
				}

				break;
			case R.id.checkBoxVISIBLE_SIGN_INTERIOR_STRUCTURE:
				if (!checkBoxVISIBLE_SIGN_INTERIOR_STRUCTURE.isChecked()) {

					tableRowVISIBLE_SIGN_INTERIOR_STRUCTURE
							.setVisibility(View.GONE);
					radioGroupVISIBLE_SIGN_INTERIOR_STRUCTURE.clearCheck();
					// radiobtnVISIBLE_SIGN_INTERIOR_STRUCTURE1.setChecked(false);
					// radiobtnVISIBLE_SIGN_INTERIOR_STRUCTURE2.setChecked(false);
				} else {
					tableRowVISIBLE_SIGN_INTERIOR_STRUCTURE
							.setVisibility(View.VISIBLE);
				}

				break;
			case R.id.checkBoxREPLACED_MONITORING_STATIONS:
				if (!checkBoxREPLACED_MONITORING_STATIONS.isChecked()) {
					tableRowREPLACED_MONITORING_STATIONS1
							.setVisibility(View.GONE);
					tableRowREPLACED_MONITORING_STATIONS2
							.setVisibility(View.GONE);

					edtREPLACED_MONITORING_STATIONS.getText().clear();
					edt_station_REPLACED_MONITORING_STATIONS.getText().clear();

				} else {
					tableRowREPLACED_MONITORING_STATIONS2
							.setVisibility(View.VISIBLE);
					tableRowREPLACED_MONITORING_STATIONS1
							.setVisibility(View.VISIBLE);
					if (checkChange == 1) {
						edtREPLACED_MONITORING_STATIONS.requestFocus();
					}
				}

				break;
			case R.id.checkBoxTREATED_PERIMETER_STRUCTURE:
				if (!checkBoxTREATED_PERIMETER_STRUCTURE.isChecked()) {

					tableRowTREATED_PERIMETER_STRUCTURE
							.setVisibility(View.GONE);
					radioGroupVISIBLE_SIGN_PERIMETER_STRUCTURE.clearCheck();
					// radiobtnTREATED_PERIMETER_STRUCTURE1.setChecked(false);
					// radiobtnTREATED_PERIMETER_STRUCTURE2.setChecked(false);
				} else {
					tableRowTREATED_PERIMETER_STRUCTURE
							.setVisibility(View.VISIBLE);
				}

				break;

			case R.id.checkBoxTREATED_SLAB_PENETRATIONS:
				if (!checkBoxTREATED_SLAB_PENETRATIONS.isChecked()) {

					tableRowTREATED_SLAB_PENETRATIONS.setVisibility(View.GONE);
					radioGroupTREATED_SLAB_PENETRATIONS.clearCheck();
					// radiobtnTREATED_SLAB_PENETRATIONS1.setChecked(false);
					// radiobtnTREATED_SLAB_PENETRATIONS2.setChecked(false);
				} else {
					tableRowTREATED_SLAB_PENETRATIONS
							.setVisibility(View.VISIBLE);
				}

				break;
			case R.id.checkBoxTREATED_CONDUSIVE_CONDITIONS:
				if (!checkBoxTREATED_CONDUSIVE_CONDITIONS.isChecked()) {

					tableRowTREATED_CONDUSIVE_CONDITIONS
							.setVisibility(View.GONE);
					radioGroupTREATED_CONDUSIVE_CONDITIONS.clearCheck();
					// radiobtnTREATED_CONDUSIVE_CONDITIONS1.setChecked(false);
					// radiobtnTREATED_CONDUSIVE_CONDITIONS2.setChecked(false);
				} else {
					tableRowTREATED_CONDUSIVE_CONDITIONS
							.setVisibility(View.VISIBLE);
				}
			case R.id.checkBoxINSPECTED_RODENT_STATIONS:
				if (!checkBoxINSPECTED_RODENT_STATIONS.isChecked()) {

					tableRow_INSPECTED_RODENT_STATIONS.setVisibility(View.GONE);
					edt_how_many_INSPECTED_RODENT_STATIONS.getText().clear();
				} else {
					tableRow_INSPECTED_RODENT_STATIONS
							.setVisibility(View.VISIBLE);
					if (checkChange == 1) {
						edt_how_many_INSPECTED_RODENT_STATIONS.requestFocus();
					}
				}
				break;
			case R.id.checkBoxCLEANES_RODENT_STATIONS:
				if (!checkBoxCLEANES_RODENT_STATIONS.isChecked()) {

					tableRow_CLEANES_RODENT_STATIONS.setVisibility(View.GONE);

					radioGroupCLEANES_RODENT_STATIONS.clearCheck();
				} else {
					tableRow_CLEANES_RODENT_STATIONS
							.setVisibility(View.VISIBLE);
				}

				break;
			case R.id.checkBoxINSTALLED_RODENT_BAIT_STATIONS:
				if (!checkBoxINSTALLED_RODENT_BAIT_STATIONS.isChecked()) {

					tableRow_INSTALLED_RODENT_BAIT_STATIONS1
							.setVisibility(View.GONE);
					tableRow_INSTALLED_RODENT_BAIT_STATIONS2
							.setVisibility(View.GONE);
					radioGroupINSTALLED_RODENT_BAIT_STATIONS.clearCheck();
					// radiobtnINSTALLED_RODENT_BAIT_STATIONS1.setChecked(false);
					// radiobtnINSTALLED_RODENT_BAIT_STATIONS2.setChecked(false);
					edt_how_many_INSTALLED_RODENT_BAIT_STATIONS.getText()
							.clear();
				} else {
					tableRow_INSTALLED_RODENT_BAIT_STATIONS1
							.setVisibility(View.VISIBLE);
					tableRow_INSTALLED_RODENT_BAIT_STATIONS2
							.setVisibility(View.VISIBLE);

					if (checkChange == 1) {
						edt_how_many_INSTALLED_RODENT_BAIT_STATIONS
								.requestFocus();
					}
				}
				break;
			case R.id.checkBoxREPLACED_BAIT:
				if (!checkBoxREPLACED_BAIT.isChecked()) {
					// radiobtnREPLACED_BAIT1.setChecked(false);
					// radiobtnREPLACED_BAIT2.setChecked(false);
					tableRow_REPLACED_BAIT1.setVisibility(View.GONE);
					tableRow_REPLACED_BAIT2.setVisibility(View.GONE);

					radioGroupREPLACED_BAIT.clearCheck();
					edt_station_REPLACED_BAIT.getText().clear();
				} else {
					tableRow_REPLACED_BAIT1.setVisibility(View.VISIBLE);
					tableRow_REPLACED_BAIT2.setVisibility(View.VISIBLE);

					if (checkChange == 1) {
						edt_station_REPLACED_BAIT.requestFocus();
					}
				}
				break;
			case R.id.checkBoxREPLACED_BAIT_STATIONS:
				if (!checkBoxREPLACED_BAIT_STATIONS.isChecked()) {
					// radiobtnREPLACED_BAIT_STATIONS1.setChecked(false);
					// radiobtnREPLACED_BAIT_STATIONS2.setChecked(false);
					tableRow_REPLACED_BAIT_STATIONS1.setVisibility(View.GONE);
					tableRow_REPLACED_BAIT_STATIONS2.setVisibility(View.GONE);
					tableRow_REPLACED_BAIT_STATIONS3.setVisibility(View.GONE);

					radioGroupREPLACED_BAIT_STATIONS.clearCheck();
					edt_station_REPLACED_BAIT_STATIONS.getText().clear();
					edt_how_many_REPLACED_BAIT_STATIONS.getText().clear();
				} else {
					tableRow_REPLACED_BAIT_STATIONS1
							.setVisibility(View.VISIBLE);
					tableRow_REPLACED_BAIT_STATIONS2
							.setVisibility(View.VISIBLE);
					tableRow_REPLACED_BAIT_STATIONS3
							.setVisibility(View.VISIBLE);
					if (checkChange == 1) {
						edt_station_REPLACED_BAIT_STATIONS.requestFocus();
					}
				}
				break;
			case R.id.checkBoxINSPECTED_RODENT_TRAPS:
				if (!checkBoxINSPECTED_RODENT_TRAPS.isChecked()) {

					tableRow_INSPECTED_RODENT_TRAPS.setVisibility(View.GONE);
					radioGroupINSPECTED_RODENT_TRAPS.clearCheck();
					// radiobtnINSPECTED_RODENT_TRAPS1.setChecked(false);
					// radiobtnINSPECTED_RODENT_TRAPS2.setChecked(false);
				} else {
					tableRow_INSPECTED_RODENT_TRAPS.setVisibility(View.VISIBLE);
				}
				break;
			case R.id.checkBoxCLEANED_RODENT_TRAPS:
				if (!checkBoxCLEANED_RODENT_TRAPS.isChecked()) {

					tableRow_CLEANED_RODENT_TRAPS.setVisibility(View.GONE);
					radioGroupCLEANED_RODENT_TRAPS.clearCheck();
					// radiobtnCLEANED_RODENT_TRAPS1.setChecked(false);
					// radiobtnCLEANED_RODENT_TRAPS2.setChecked(false);
				} else {
					tableRow_CLEANED_RODENT_TRAPS.setVisibility(View.VISIBLE);
				}
				break;

			case R.id.checkBoxINSTALLED_RODENT_TRAPS:
				if (!checkBoxINSTALLED_RODENT_TRAPS.isChecked()) {

					// radiobtnINSTALLED_RODENT_TRAPS1.setChecked(false);
					// radiobtnINSTALLED_RODENT_TRAPS2.setChecked(false);
					tableRow_INSTALLED_RODENT_TRAPS1.setVisibility(View.GONE);
					tableRow_INSTALLED_RODENT_TRAPS2.setVisibility(View.GONE);

					radioGroupINSTALLED_RODENT_TRAPS.clearCheck();
					edt_how_many_INSTALLED_RODENT_TRAPS.getText().clear();

				} else {
					tableRow_INSTALLED_RODENT_TRAPS1
							.setVisibility(View.VISIBLE);
					tableRow_INSTALLED_RODENT_TRAPS2
							.setVisibility(View.VISIBLE);

					if (checkChange == 1) {
						edt_how_many_INSTALLED_RODENT_TRAPS.requestFocus();
					}
				}
				break;
			case R.id.checkBoxREPLACED_RODENT_TRAPS:
				if (!checkBoxREPLACED_RODENT_TRAPS.isChecked()) {
					// radiobtnREPLACED_RODENT_TRAPS1.setChecked(false);
					// radiobtnREPLACED_RODENT_TRAPS2.setChecked(false);
					tableRow_REPLACED_RODENT_TRAPS1.setVisibility(View.GONE);
					tableRow_REPLACED_RODENT_TRAPS2.setVisibility(View.GONE);
					tableRow_REPLACED_RODENT_TRAPS3.setVisibility(View.GONE);

					radioGroupREPLACED_RODENT_TRAPS.clearCheck();
					edt_station_REPLACED_RODENT_TRAPS.getText().clear();
					edt_how_many_REPLACED_RODENT_TRAPS.getText().clear();
				} else {

					tableRow_REPLACED_RODENT_TRAPS1.setVisibility(View.VISIBLE);
					tableRow_REPLACED_RODENT_TRAPS2.setVisibility(View.VISIBLE);
					tableRow_REPLACED_RODENT_TRAPS3.setVisibility(View.VISIBLE);
					if (checkChange == 1) {
						edt_how_many_REPLACED_RODENT_TRAPS.requestFocus();
					}
				}
				break;
			case R.id.checkBoxCHECKED_RUN_DURACTION_TIME:
				if (!checkBoxCHECKED_RUN_DURACTION_TIME.isChecked()) {
					edtMosquitoService1.getText().clear();
				} else {
					edtMosquitoService1.requestFocus();
				}
				break;
			case R.id.checkBoxCHECKED_TIME_SYSTEM_RUNS:
				if (!checkBoxCHECKED_TIME_SYSTEM_RUNS.isChecked()) {
					edtMosquitoService2.getText().clear();
				} else {
					edtMosquitoService2.requestFocus();
				}
				break;
			case R.id.checkBoxSEALED_ENTRY_POINTS:
				if (!checkBoxSEALED_ENTRY_POINTS.isChecked()) {

					tableRow_SEALED_ENTRY_POINTS.setVisibility(View.GONE);

					radioGroupSEALED_ENTRY_POINTS.clearCheck();
					// radiobtnSEALED_ENTRY_POINTS1.setChecked(false);
					// radiobtnSEALED_ENTRY_POINTS2.setChecked(false);
				} else {
					tableRow_SEALED_ENTRY_POINTS.setVisibility(View.VISIBLE);
				}
				break;
			default:

				break;
			}
		}
	}

	class OnGalleryClickFront implements OnItemLongClickListener {
		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
				int position, long arg3) {
			if (generalInfoDTO.getServiceStatus().equals("InComplete")) {
				if (listFrontImg.size() > 0) {
					listFrontImg.remove(position);
					if (tempFrontImagList.size() > 0) {
						tempFrontImagList.remove(position);
					}
					if (tempStoreFrontImageName.size() > 0) {
						tempStoreFrontImageName.remove(position);
					}
					RefreshFrontImage();
					listFrontImg.clear();
					generalInfoDTO.setCheckFrontImage("");
				}
			}
			return false;
		}
	}

	class OnGalleryClickBack implements OnItemLongClickListener {
		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
				int position, long arg3) {
			if (generalInfoDTO.getServiceStatus().equals("InComplete")) {
				if (listBackImg.size() > 0) {
					listBackImg.remove(position);
					if (tempBackImagList.size() > 0) {
						tempBackImagList.remove(position);

					}
					if (tempStoreBackImageName.size() > 0) {
						tempStoreBackImageName.remove(position);
					}
					RefreshBackImage();
					listBackImg.clear();
					generalInfoDTO.setCheckBackImage("");
				}
			}
			return false;
		}
	}

	public void pickImage(String CHECK_FRONT_IMAGE) {

		// Log.e("CHECK_FRONT_IMAGE", ""+CHECK_FRONT_IMAGE);
		if (CHECK_FRONT_IMAGE.equals("CHECK_FRONT_IMAGE")) {
			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			intent.addCategory(Intent.CATEGORY_OPENABLE);
			startActivityForResult(intent, REQUEST_Gallery_ACTIVITY_CODE_Front);
		} else {
			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			intent.addCategory(Intent.CATEGORY_OPENABLE);
			startActivityForResult(intent, REQUEST_Gallery_ACTIVITY_CODE_Back);
		}
	}

	public void takeBackPicture() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		CheckBackImageName = generalInfoDTO.getServiceID_new()
				+ "_CheckBack.jpg"; // CommonFunction.CreateImageName();
		Uri imageUri = Uri.fromFile(CommonFunction.getFileLocation(
				HC_PestControlServiceReport.this, CheckBackImageName));
		intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
		intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
		intent.putExtra(MediaStore.EXTRA_SIZE_LIMIT,
				Integer.toString(1024 * 1024));
		startActivityForResult(intent, REQUEST_Camera_ACTIVITY_CODE_Back);
		// LoggerUtil.e("Image Url", imageUri + "");
	}

	public void takeFrontPicture() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		CheckFrontImageName = generalInfoDTO.getServiceID_new()
				+ "_CheckFront.jpg"; // CommonFunction.CreateImageName();
		Uri imageUri = Uri.fromFile(CommonFunction.getFileLocation(
				HC_PestControlServiceReport.this, CheckFrontImageName));
		intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
		intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
		intent.putExtra(MediaStore.EXTRA_SIZE_LIMIT,
				Integer.toString(1024 * 1024));
		startActivityForResult(intent, REQUEST_Camera_ACTIVITY_CODE_Front);
		// LoggerUtil.e("Image Url", imageUri + "");
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_Gallery_ACTIVITY_CODE_Front) {
			if (resultCode == GeneralInformationActivity.RESULT_OK) {
				if (listFrontImg.size() > 0) {
					listFrontImg.remove(0);

					if (tempFrontImagList.size() > 0) {
						tempFrontImagList.remove(0);

					}
					if (tempStoreFrontImageName.size() > 0) {
						tempStoreFrontImageName.remove(0);
					}
					RefreshFrontImage();
					listFrontImg.clear();
				}
				Toast.makeText(this, "Picture is taken from gallery",
						Toast.LENGTH_SHORT).show();
				imageUri = data.getData();
				try {
					// bitmapFromG =
					// BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
					Bitmap outputFront = decodeUri(imageUri);
					CheckFrontImageName = generalInfoDTO.getServiceID_new()
							+ "_CheckFront.jpg";
					CommonFunction.saveBitmap(outputFront,
							getApplicationContext(), CheckFrontImageName);
					tempStoreFrontImageName.add(CheckFrontImageName);
					RefreshFrontImage();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		if (requestCode == REQUEST_Gallery_ACTIVITY_CODE_Back) {
			if (resultCode == GeneralInformationActivity.RESULT_OK) {
				if (listBackImg.size() > 0) {
					listBackImg.remove(0);

					if (tempBackImagList.size() > 0) {
						tempBackImagList.remove(0);

					}
					if (tempStoreBackImageName.size() > 0) {
						tempStoreBackImageName.remove(0);
					}
					RefreshBackImage();
					listBackImg.clear();
				}
				Toast.makeText(this, "Picture is  taken from gallery",
						Toast.LENGTH_SHORT).show();
				imageUri = data.getData();
				try {
					// bitmapFromG =
					// BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
					Bitmap outputBack = decodeUri(imageUri);
					CheckBackImageName = generalInfoDTO.getServiceID_new()
							+ "_CheckBack.jpg";
					CommonFunction.saveBitmap(outputBack,
							getApplicationContext(), CheckBackImageName);
					tempStoreBackImageName.add(CheckBackImageName);
					RefreshBackImage();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
		if (requestCode == REQUEST_Camera_ACTIVITY_CODE_Front) {
			if (resultCode == GeneralInformationActivity.RESULT_OK) {
				if (listFrontImg.size() > 0) {
					listFrontImg.remove(0);

					if (tempFrontImagList.size() > 0) {
						tempFrontImagList.remove(0);

					}
					if (tempStoreFrontImageName.size() > 0) {
						tempStoreFrontImageName.remove(0);
					}
					RefreshFrontImage();
					listFrontImg.clear();
				}
				imageCountFront++;
				Toast.makeText(HC_PestControlServiceReport.this,
						"Picture is  taken", Toast.LENGTH_SHORT);
				tempStoreFrontImageName.add(CheckFrontImageName);
				RefreshFrontImage();
				getfile(CheckFrontImageName);
				// LoggerUtil.e("File Name on done camera", ImageName + "");
			} else if (resultCode == GeneralInformationActivity.RESULT_CANCELED) {
				RefreshFrontImage();
			}
		}
		if (requestCode == REQUEST_Camera_ACTIVITY_CODE_Back) {
			if (resultCode == GeneralInformationActivity.RESULT_OK) {
				if (listBackImg.size() > 0) {
					listBackImg.remove(0);

					if (tempBackImagList.size() > 0) {
						tempBackImagList.remove(0);

					}
					if (tempStoreBackImageName.size() > 0) {
						tempStoreBackImageName.remove(0);
					}
					RefreshBackImage();
					listBackImg.clear();
				}
				imageCountBack++;
				Toast.makeText(HC_PestControlServiceReport.this,
						"Picture is  taken", Toast.LENGTH_SHORT);
				tempStoreBackImageName.add(CheckBackImageName);
				RefreshBackImage();
				getfile(CheckBackImageName);
				// LoggerUtil.e("File Name on done camera", ImageName + "");
			} else if (resultCode == GeneralInformationActivity.RESULT_CANCELED) {
				RefreshBackImage();
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	/*
	 * @Override protected void onResume() { // TODO Auto-generated method stub
	 * JSONObject
	 * commercial_pest_string=HoustonFlowFunctions.getTodaysCommercialReport
	 * (serviceID_new); commercialDTO= new
	 * Gson().fromJson(commercial_pest_string.toString(), CommercialDTO.class);
	 * 
	 * //Log.e("onResume called",""); super.onResume(); }
	 */
	public void getfile(String fileName) {
		try {
			LoggerUtil.e("Get File Called", fileName);

			File imagefile = CommonFunction.getFileLocation(
					HC_PestControlServiceReport.this, fileName);
			if (imagefile.exists()) {
				// Bitmap myBitmap =
				// BitmapFactory.decodeFile(imagefile.getAbsolutePath());

				Bitmap myBitmap = decodeUri(Uri.fromFile(CommonFunction
						.getFileLocation(
								HC_PestControlServiceReport.this,
								fileName)));
				LoggerUtil.e("Input Bitmap width and heigth  ",
						myBitmap.getWidth() + " " + myBitmap.getWidth());
				// edtDescription.setText(fileName+" "+myBitmap.getWidth()+" and "+myBitmap.getHeight());
				Bitmap output = Bitmap.createScaledBitmap(myBitmap, 800, 800,
						true);
				LoggerUtil.e("Out Put Bitmap width and heigth  ",
						output.getWidth() + " " + output.getWidth());
				imagefile.delete();
				CommonFunction.saveBitmap(output,
						HC_PestControlServiceReport.this, fileName);
			}
		} catch (Exception e) {

		}
	}

	private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(HC_PestControlServiceReport.this
				.getContentResolver().openInputStream(selectedImage), null, o);

		final int REQUIRED_SIZE = 800;

		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeStream(
				HC_PestControlServiceReport.this.getContentResolver()
						.openInputStream(selectedImage), null, o2);
	}

}
