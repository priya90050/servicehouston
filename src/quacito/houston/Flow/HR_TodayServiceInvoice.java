package quacito.houston.Flow;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;

import quacito.houston.CommonUtilities.CheckableRelativeLayout;
import quacito.houston.CommonUtilities.CommonFunction;
import quacito.houston.CommonUtilities.LoggerUtil;
import quacito.houston.CommonUtilities.ParameterUtil;
import quacito.houston.CommonUtilities.SharedPreference;
import quacito.houston.DBhelper.DatabaseHelper;
import quacito.houston.DBhelper.HoustonFlowFunctions;
import quacito.houston.Flow.servicehouston.R;
import quacito.houston.adapter.EmailAdapter;
import quacito.houston.adapter.GalleryImageAdapter;
import quacito.houston.horizontallistview.HorizontalListView;
import quacito.houston.model.AgreementMailIdDTO;
import quacito.houston.model.ChemicalDto;
import quacito.houston.model.GeneralInfoDTO;
import quacito.houston.model.ImgDto;
import quacito.houston.model.LogDTO;
import quacito.houston.model.HResidential_pest_DTO;
import quacito.houston.model.UserDTO;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Paint.Style;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.splunk.mint.Mint;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class HR_TodayServiceInvoice extends Activity
{  
	
	public String current = null;
	private Bitmap mBitmap;
	View mView;
	File mypath;
	LinearLayout mainLayout;
	private TextView yourName;
	LinearLayout mContent;
	signature mSignature;
	Dialog signatureDialog,sendEmailDialog;
	Button  btnBack, mClear, mGetSign, mCancel,btnRecordAudio,btnAfterImage,btnSignature,btnIagreeSignature,btnsubmitToPestpac,btnSendEmail;
	private MediaPlayer myPlayer;
	private ImageView playBtn,stopPlayBtn;
	HorizontalListView imageListView;
	RadioButton radioCash,radioCheck,radioCreditCard,radioBillLater,radioPreBill,radioNoCharge;
	public static int RECORD_AUDIO_REQUEST_CODE=102;
	ArrayList<String> aList,aList1;
	ArrayList<String> tempStoreImageName = new ArrayList<String>();
	String strmultipleImageName;
	CharSequence[] charSequenceItems;
	List<String> MandatoryList=new ArrayList<String>();
	TextView textViewServicePageHeader,txtPestControlHeadOutside,txtPestControlHeadInside,txtServiceReportAddress,txtAcct,txtTimeIn,txtTimeInHeader,txtEmail,text1,txtOrderNumber,
	txtSERVICE_FOR,txtAREAS_INSPECTED,txtINSPECTED_TREATED,txtAnts,txtOtherPest,txtOutsideonly,
	txtFenceLines,txtBackYard,txtFrontYard,txtExteriorperimeterofhouse,txtRoofLine,txtGarage,
	txtAttic,txtInteriorofhouse,txtVisibleSigns,txtNumberofMonitoringDevices,
	txtVisibleTermiteMonitorDevices,txtTermiteMoniterDevicereplaced,txtVisibleTermiteActivity,
	txtVisibleTermiteAttic,txtVisibleTermiteInteriorHouse,txtMosquitoServices,txtCheckedRunDurationTime,
	txtCheckedTimeSystemRuns,txtPestControlServiesInside,txtPestControlServiesOutside,
	txtTechComments,txtServicetech,txtOrder,txtLicence,txtdate,txtEmp
	,txtROOF_PITCHE_SEALED,txtBREEZY_WAY_GAAREGE,txtTODAY_ROOF_VENTS,txtTODAY_GARAGE_DOOR_SEALED,
	txtTODAYS_SOFFIT_VENTS,txtTODAYS_AC_DRYER,txtTODAYS_WEEP_HOLES,txtTODAY_NUMBER_LIVEANIMAL_TRAPS_INSTALLES,
	txtTODAY_SIDING_HOUSES_SEALED,txtTODAY_NUMBER_SNAPTRAPS,textRecordingPoint;

	EditText edtCheckNo,edtDrivingLic,edtAmount,edtCustomer,edtAddress,edtBillingAddress,edtHomePhone,edtCellPhone,edtAcct,
	edtChargerForCurrentServices,edtBalanceDue,edtTax,edtEmp,edtTotalAmount,edtZip,edtOpenOfficeNote;

	LinearLayout layoutChemicalHeader,layout_todays_service_report,layoutOtherPest,layoutAnts,rowFenceLines,rowBackYard,rowFrontyard,
	rowExteriorperimeterofhouse,rowRoofLine,rowGarage,rowAttic,rowInteriorofhouse,rowVisibleSigns,
	rowNumberofMonitoringDevices,rowVisibleTermiteMonitorDevices,rowTermiteMoniterDevicereplaced,
	rowVisibleTermiteActivity,rowVisibleTermiteAttic,rowVisibleTermiteInteriorHouse,rowMosquitoServices,
	rowCheckedRunDurationTime,rowCheckedTimeSystemRuns,rowINSPECTED_TREATED,
	linearlayout_amount,linearlayout_CheckNo,linearlayout_DrivingLic,
	LAYOUT_SERVICEFOR,LAYOUT_AREASINSPECTED,LAYOUT_INSPECTED_TREATED,LAYOUT_OUTSIDE_SERVICE,
	LAYOUT_PEST_ZONE,layout_termiteserivces,layout_mosquito_services,layout_recommendations,layout_pestControlserivces
	,layoutRODENTSERVICES,layoutROOF_PITCHE_SEALED,layoutBREEZY_WAY_GAAREGE,layoutTODAY_ROOF_VENTS,
	layoutTODAY_GARAGE_DOOR_SEALED,layoutTODAYS_SOFFIT_VENTS,layoutTODAYS_AC_DRYER,layoutTODAYS_WEEP_HOLES,
	layoutTODAY_NUMBER_LIVEANIMAL_TRAPS_INSTALLES,layoutTODAY_SIDING_HOUSES_SEALED,layoutTODAY_NUMBER_SNAPTRAPS;

	LinearLayout rowBRANCHES_HOUSE_DESCP,rowBRANCHES_HOUSE,rowFIREWOOD_FOUNDATION_DESCP,rowFIREWOOD_FOUNDATION
	,rowDEBRIS_CRAWL_DESCP,rowDEBRIS_CRAWL,rowEXCESSIVE_PLANT_DESCP,rowEXCESSIVE_PLANT,rowSOIL_ABOVE_DESCP
	,rowSOIL_ABOVE,rowWOOD_SOIL,rowWOOD_SOIL_DESCP,rowDEBRIS_ON_ROOF_DESCP,rowDEBRIS_ON_ROOF,rowSTANDING_WATER_DESCP,
	rowSTANDING_WATER,rowMOISTURE_PROBLM_DESCP,rowMOISTURE_PROBLM,rowOPENINGS_PLUMBING_DESCP
	,rowOPENINGS_PLUMBING,rowEXCESSIVE_GAPS,rowEXCESSIVE_GAPS_DESCP,rowLEACKY_PLUMBING,rowLEACKY_PLUMBING_DESCP
	,rowGARBAGE_CANS,rowGARBAGE_CANS_DESCP,rowMOISTURE_DAMAGE_DESCP,rowMOISTURE_DAMAGE,rowGROCERY_BAGS_DESCP
	,rowGROCERY_BAGS,rowPET_FOOD_DESCP,rowPET_FOOD,rowRECYCLED_ITEMS_DESCP,rowRECYCLED_ITEMS,rowEXCESSIVE_STOREGE_DESCP
	,rowEXCESSIVE_STOREGE,Layout_TechComment,Layout_TechComment_desc;
	boolean issignature=false,isValidate=true;
	ArrayList<String> listSelectedChemicals=new ArrayList<String>();
	ArrayList<ChemicalDto> listChemicals=new ArrayList<ChemicalDto>();
	HoustonFlowFunctions houstonFlowFunctions;
	HResidential_pest_DTO residential_pest_DTO;
	GeneralInfoDTO generalInfoDTO;
	UserDTO userDTO;
	LogDTO logDTO;
	String strOutsidepestControlServices,strInsidepestControlServices,serviceID_new,straudioFile,cussignName,strTechnicianSignature,strCustomerSignature;
	ImageView imageViewTechnicianSign,imageViewCustomerSign;
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		setContentView(R.layout.hr_service_invoice_report);
		
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
	    getActionBar().setDisplayShowHomeEnabled(true);
		
		Thread.setDefaultUncaughtExceptionHandler(new TopExceptionHandler(this));
		Mint.initAndStartSession(HR_TodayServiceInvoice.this, "e53b080c");
		initialize();
		
		generalInfoDTO = (GeneralInfoDTO)this.getIntent().getExtras().getSerializable("generalInfoDTO");
		userDTO  = (UserDTO)this.getIntent().getExtras().getSerializable("userDTO");
		serviceID_new = getIntent().getExtras().getString(ParameterUtil.ServiceID_new);
		strInsidepestControlServices = getIntent().getExtras().getString("strInsideService");
		strOutsidepestControlServices = getIntent().getExtras().getString("strOutsideService");

		setValuesInComponents(); 
		if(generalInfoDTO.getServiceStatus().equals("Complete"))
		{
			btnAfterImage.setEnabled(false);
			btnRecordAudio.setEnabled(false);
			playBtn.setEnabled(false);
			btnSignature.setEnabled(false);
			btnIagreeSignature.setEnabled(false);
			btnsubmitToPestpac.setEnabled(false);
		}
		else
		{
			btnAfterImage.setEnabled(true);
			btnRecordAudio.setEnabled(true);
			playBtn.setEnabled(true);
			btnSignature.setEnabled(true);
			btnIagreeSignature.setEnabled(true);
			btnsubmitToPestpac.setEnabled(true);
		}
		super.onCreate(savedInstanceState);
	}

	private void setValuesInComponents()
	{
		// TODO Auto-generated method stub
		JSONObject jsServiceObject= houstonFlowFunctions.getTodaysReport(serviceID_new);

		if(jsServiceObject!=null)
		{
			residential_pest_DTO= new Gson().fromJson(jsServiceObject.toString(), HResidential_pest_DTO.class);
		}
		//generalInfoDTO= new Gson().fromJson(houstonFlowFunctions.getSelectedService(serviceID_new).toString(), GeneralInfoDTO.class);

		setValuesForHeader();
		setValuesForServiceFor();
		setValuesForAreasInspected();
		setValuesForInspectedTreatedFor();
		setValuesForOutsideOnly();
		setValuesForPestActivityByZone();
		setChemicalTable();
		setValuesForPestControlServices();
		setValuesForTermiteServices();
		setValuesForRodentServices();
		setValuesForMosquitoServices();
		setValuesForRecommendations();
		setValuesForInvoice();
		reloadPriviousBeforeImage();
		straudioFile=generalInfoDTO.getAudioFile();
        if(!CommonFunction.checkString(straudioFile, "").equals(""))
        {
        	textRecordingPoint.setText("Audio available"); 	
        }
        else
        {
        	textRecordingPoint.setText("Audio not available");
        }
		/*if(generalInfoDTO.getServiceStatus().equals("Complete"))
		{
			btnRecordAudio.setEnabled(true);
			btnAfterImage.setEnabled(false);
			btnIagreeSignature.setEnabled(false);
			btnSignature.setEnabled(false);
			txtLicence.setEnabled(false);
			edtOpenOfficeNote.setEnabled(false);
			//checkBoxCustomerNotPresent.setEnabled(false);
		}*/
	}
	public void reloadPriviousBeforeImage()
	{
		String imageNames=generalInfoDTO.getAfterImage();
		//	Toast.makeText(getApplicationContext(), imageNames+"",Toast.LENGTH_LONG).show();
		if(!CommonFunction.checkString(imageNames, "").equals(""))
		{
			String[] imageArray=imageNames.split(",");
			for(int i=0;i<imageArray.length;i++)
			{
				if(!imageArray[i].equals(""))
				{
					tempStoreImageName.add(imageArray[i]);
					RefreshImage();
				}
			}
		}
		strCustomerSignature=residential_pest_DTO.getSignature();
		strTechnicianSignature=residential_pest_DTO.getServiceSignature();
		//Toast.makeText(getApplicationContext(), strCustomerSignature,Toast.LENGTH_LONG).show();
		Bitmap customerSignBitmap=getBimapByName(strCustomerSignature);
		Bitmap technicianSignBitmap=getBimapByName(strTechnicianSignature);
		imageViewCustomerSign.setImageBitmap(customerSignBitmap);
		imageViewTechnicianSign.setImageBitmap(technicianSignBitmap);
	}
	private void setChemicalTable() 
	{
		//LinearLayout header=(LinearLayout)findViewById(R.id.layoutChemicalHeader);
		//header.setVisibility(View.GONE);
		//TODO Auto-generated method stub
		JSONArray jsChemical = houstonFlowFunctions.getAllSelectedChemicals(DatabaseHelper.TABLE_SELECTED_CHEMICALS,serviceID_new,"Residential");
		listChemicals.clear();
		listChemicals=new Gson().fromJson(jsChemical.toString(), new TypeToken<List<ChemicalDto>>(){}.getType());
		//Log.e("", "dfdf"+listChemicals.get(0).getProduct()+".."+listChemicals.get(1).getProduct());
		if(listChemicals.size()==0)
		{
			layoutChemicalHeader.setVisibility(View.GONE);
		}
		for(int i=0;i<listChemicals.size();i++)
		{
			AddChemicals(i);
		}
	}
	int i=0;
	TableLayout tb;
	TableRow deleteTableRow;
	int numberofaudit;
	TableRow tr;
	ArrayList<TableRow> trlist=new ArrayList<TableRow>();

	private void AddChemicals(final int listindex) 
	{
		// TODO Auto-generated method stub
		numberofaudit++;
		LoggerUtil.e("serviceID_new", serviceID_new);
		ChemicalDto dto=listChemicals.get(listindex);
		if(i==0)
		{
			tb=new TableLayout(HR_TodayServiceInvoice.this);
			//addHeader();
			mainLayout.addView(tb);
		}
		i=i+1;
		tr = new TableRow(getApplicationContext());
		trlist.add(tr);
		if(i%2!=0)

		tr.setBackgroundColor(Color.parseColor("#EDEDEF"));
		tr.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.FILL_PARENT));
		tr.setPadding(0, 10, 0, 10);

		LayoutParams params2 = new TableRow.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT,1.0f);
		
		LayoutParams paramsProd = new TableRow.LayoutParams(70, LayoutParams.FILL_PARENT,1.0f);

		TextView product = new TextView(getApplicationContext());
		product.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
		product.setTextColor(Color.BLACK);
		product.setLayoutParams(paramsProd);

		if(listChemicals.get(listindex).getProduct().equals("Other") && !CommonFunction.checkString(listChemicals.get(listindex).getOtherText(), "").equals(""))
		{
			LoggerUtil.e("OTHER", ""+listChemicals.get(listindex).getOtherText());
			product.setText(listChemicals.get(listindex).getOtherText());
		}
		else
		{
			product.setText(listChemicals.get(listindex).getProduct());
		}
		tr.addView(product);

		TextView percent = new TextView(getApplicationContext());
		percent.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
		percent.setText(listChemicals.get(listindex).getPercentPes());
		percent.setLayoutParams(params2);
		percent.setTextColor(Color.BLACK);
		tr.addView(percent);

		TextView AMT = new TextView(getApplicationContext());
		AMT.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
		AMT.setText(listChemicals.get(listindex).getAmount());
		//AMT.setPadding(0, 0,5, 0);
		AMT.setLayoutParams(params2);
		AMT.setTextColor(Color.BLACK);
		tr.addView(AMT);

		TextView UNIT = new TextView(getApplicationContext());
		UNIT.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
		UNIT.setText(listChemicals.get(listindex).getUnit());
		//UNIT.setPadding(0, 0,5, 0);
		UNIT.setLayoutParams(params2);
		UNIT.setTextColor(Color.BLACK);
		tr.addView(UNIT);

		LayoutParams paramstarget = new TableRow.LayoutParams(50, LayoutParams.FILL_PARENT,1.0f);

		TextView TARGET = new TextView(getApplicationContext());
		TARGET.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
		TARGET.setText(listChemicals.get(listindex).getTarget());
		//TARGET.setPadding(0, 0,5, 0);
    	TARGET.setLayoutParams(paramstarget);
		TARGET.setTextColor(Color.BLACK);
		tr.addView(TARGET);
		tb.addView(tr, new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
	}
	public void addHeader()
	{
		TableRow tr = new TableRow(getApplicationContext());
		if(i%2!=0)

			tr.setBackgroundColor(Color.parseColor("#2F80B7"));
		//tr.setOrientation(LinearLayout.HORIZONTAL);
		tr.setBackgroundColor(Color.parseColor("#2F80B7"));
		tr.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.FILL_PARENT));
		//tr.setWeightSum(1);
		tr.setPadding(0, 10, 0, 10);

		//LayoutParams params0 = new TableRow.LayoutParams(android.widget.LinearLayout.LayoutParams.WRAP_CONTENT, LayoutParams.FILL_PARENT);

		TextView product = new TextView(getApplicationContext());
		product.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
		product.setText(getApplicationContext().getResources().getString(R.string.PRODUCT));
		product.setTextColor(Color.WHITE);
		LayoutParams params1 = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.FILL_PARENT,1f);
		product.setLayoutParams(params1);
		tr.addView(product);
		LayoutParams params2 = new TableRow.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT,.3f);

		TextView percent = new TextView(getApplicationContext());
		percent.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
		percent.setText(getApplicationContext().getResources().getString(R.string.percent));
		percent.setLayoutParams(params2);
		percent.setTextColor(Color.WHITE);
		tr.addView(percent);

		TextView AMT = new TextView(getApplicationContext());
		AMT.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
		AMT.setText(getApplicationContext().getResources().getString(R.string.AMT));
		AMT.setPadding(0, 0,5, 0);
		AMT.setLayoutParams(params2);
		AMT.setTextColor(Color.WHITE);
		tr.addView(AMT);

		TextView UNIT = new TextView(getApplicationContext());
		UNIT.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
		UNIT.setText(getApplicationContext().getResources().getString(R.string.UNIT));
		UNIT.setPadding(0, 0,5, 0);
		UNIT.setLayoutParams(params2);
		UNIT.setTextColor(Color.WHITE);
		tr.addView(UNIT);

		TextView TARGET = new TextView(getApplicationContext());
		TARGET.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
		TARGET.setText(getApplicationContext().getResources().getString(R.string.TARGET));
		TARGET.setPadding(0, 0,5, 0);
		TARGET.setLayoutParams(params2);
		TARGET.setTextColor(Color.WHITE);
		tr.addView(TARGET);

		tb.addView(tr, new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
	}

	private void setValuesForInvoice()
	{
		// TODO Auto-generated method stub
		if(CommonFunction.checkString(residential_pest_DTO.getTechnicianComments(), "").equals(""))
		{
			Layout_TechComment.setVisibility(View.GONE);
			Layout_TechComment_desc.setVisibility(View.GONE);
		}
		else
		{
			Layout_TechComment.setVisibility(View.VISIBLE);
			Layout_TechComment_desc.setVisibility(View.VISIBLE);
			txtTechComments.setText(residential_pest_DTO.getTechnicianComments());
		}
		txtServicetech.setText(residential_pest_DTO.getTechnician());
		txtTimeIn.setText(residential_pest_DTO.getTimeIn());
		txtLicence.setText(userDTO.getLicenceNo());
		txtdate.setText(residential_pest_DTO.getDate());
		txtEmp.setText(residential_pest_DTO.getEmpNo());
		edtChargerForCurrentServices.setText("$ "+generalInfoDTO.getInv_value());
		//edtBalanceDue.setText("$ "+String.valueOf(Float.valueOf(generalInfoDTO.getTotal()) - Float.valueOf(generalInfoDTO.getInv_value())));
		edtBalanceDue.setText("$ "+generalInfoDTO.getLocationBalance());
		edtTax.setText("$ "+generalInfoDTO.getTax_value());
		
		Double totlaD = Double.valueOf(generalInfoDTO.getTax_value())+Double.valueOf(generalInfoDTO.getInv_value());
		Double totalFinal = Math.round(totlaD * 100.0) / 100.0;
		edtTotalAmount.setText("$ "+String.valueOf(totalFinal));
		//edtTotalAmount.setText("$ "+generalInfoDTO.getTotal());
		edtOpenOfficeNote.setText(generalInfoDTO.getOffice_Note_ByRep());
	}

	private void setValuesForRecommendations() 
	{
		// TODO Auto-generated method stub
		//Log.e("", "residential_pest_DTO.getTreeBranches()"+residential_pest_DTO.getTreeBranches().equals("Tree Branches on house"));
		if(residential_pest_DTO.getTreeBranches().equals("Tree Branches on house"))
		{
			layout_recommendations.setVisibility(View.VISIBLE);
			rowBRANCHES_HOUSE.setVisibility(View.VISIBLE);
			rowBRANCHES_HOUSE_DESCP.setVisibility(View.VISIBLE);
		}

		else
		{
			rowBRANCHES_HOUSE.setVisibility(View.GONE);
			rowBRANCHES_HOUSE_DESCP.setVisibility(View.GONE);
		}

		if(residential_pest_DTO.getFirewood().equals("Firewood next to foundation"))
		{
			layout_recommendations.setVisibility(View.VISIBLE);
			rowFIREWOOD_FOUNDATION.setVisibility(View.VISIBLE);
			rowFIREWOOD_FOUNDATION_DESCP.setVisibility(View.VISIBLE);
		}

		else
		{
			rowFIREWOOD_FOUNDATION.setVisibility(View.GONE);
			rowFIREWOOD_FOUNDATION_DESCP.setVisibility(View.GONE);
		}
		if(residential_pest_DTO.getDebrisCrawl().equals("Debris in crawl space/next to foundation"))
		{
			layout_recommendations.setVisibility(View.VISIBLE);
			rowDEBRIS_CRAWL.setVisibility(View.VISIBLE);
			rowDEBRIS_CRAWL_DESCP.setVisibility(View.VISIBLE);
		}

		else
		{
			rowDEBRIS_CRAWL.setVisibility(View.GONE);
			rowDEBRIS_CRAWL_DESCP.setVisibility(View.GONE);
		}
		if(residential_pest_DTO.getExcessivePlant().equals("Excessive plant cover, stumps etc . "))
		{
			layout_recommendations.setVisibility(View.VISIBLE);
			rowEXCESSIVE_PLANT.setVisibility(View.VISIBLE);
			rowEXCESSIVE_PLANT_DESCP.setVisibility(View.VISIBLE);
		}

		else
		{
			rowEXCESSIVE_PLANT.setVisibility(View.GONE);
			rowEXCESSIVE_PLANT_DESCP.setVisibility(View.GONE);
		}
		if(residential_pest_DTO.getSoil().equals("Soil above the foundation line"))
		{
			layout_recommendations.setVisibility(View.VISIBLE);
			rowSOIL_ABOVE.setVisibility(View.VISIBLE);
			rowSOIL_ABOVE_DESCP.setVisibility(View.VISIBLE);
		}

		else
		{
			rowSOIL_ABOVE.setVisibility(View.GONE);
			rowSOIL_ABOVE_DESCP.setVisibility(View.GONE);
		}
		if(residential_pest_DTO.getWoodSoil().equals("Wood to soil contact"))
		{
			layout_recommendations.setVisibility(View.VISIBLE);
			rowWOOD_SOIL.setVisibility(View.VISIBLE);
			rowWOOD_SOIL_DESCP.setVisibility(View.VISIBLE);
		}

		else
		{
			rowWOOD_SOIL.setVisibility(View.GONE);
			rowWOOD_SOIL_DESCP.setVisibility(View.GONE);
		}
		if(residential_pest_DTO.getDebrisRoof().equals("Debris on roof/full gutter"))
		{
			layout_recommendations.setVisibility(View.VISIBLE);
			rowDEBRIS_ON_ROOF.setVisibility(View.VISIBLE);
			rowDEBRIS_ON_ROOF_DESCP.setVisibility(View.VISIBLE);
		}

		else
		{
			rowDEBRIS_ON_ROOF.setVisibility(View.GONE);
			rowDEBRIS_ON_ROOF_DESCP.setVisibility(View.GONE);
		}
		if(residential_pest_DTO.getStandingWater().equals("Standing water near/under structure"))
		{
			layout_recommendations.setVisibility(View.VISIBLE);
			rowSTANDING_WATER.setVisibility(View.VISIBLE);
			rowSTANDING_WATER_DESCP.setVisibility(View.VISIBLE);
		}

		else
		{
			rowSTANDING_WATER.setVisibility(View.GONE);
			rowSTANDING_WATER_DESCP.setVisibility(View.GONE);
		}
		if(residential_pest_DTO.getMoistureProblem().equals("Moisture problem under structure"))
		{
			layout_recommendations.setVisibility(View.VISIBLE);
			rowMOISTURE_PROBLM.setVisibility(View.VISIBLE);
			rowMOISTURE_PROBLM_DESCP.setVisibility(View.VISIBLE);
		}

		else
		{
			rowMOISTURE_PROBLM.setVisibility(View.GONE);
			rowMOISTURE_PROBLM_DESCP.setVisibility(View.GONE);
		}
		if(residential_pest_DTO.getOpenings().equals("Openings at plumbing & electric"))
		{
			layout_recommendations.setVisibility(View.VISIBLE);
			rowOPENINGS_PLUMBING.setVisibility(View.VISIBLE);
			rowOPENINGS_PLUMBING_DESCP.setVisibility(View.VISIBLE);
		}

		else
		{
			rowOPENINGS_PLUMBING.setVisibility(View.GONE);
			rowOPENINGS_PLUMBING_DESCP.setVisibility(View.GONE);
		}
		if(residential_pest_DTO.getExcessiveGaps().equals("Excessive gaps at windows/doors"))
		{
			layout_recommendations.setVisibility(View.VISIBLE);
			rowEXCESSIVE_GAPS.setVisibility(View.VISIBLE);
			rowEXCESSIVE_GAPS_DESCP.setVisibility(View.VISIBLE);
		}

		else
		{
			rowEXCESSIVE_GAPS.setVisibility(View.GONE);
			rowEXCESSIVE_GAPS_DESCP.setVisibility(View.GONE);
		}
		if(residential_pest_DTO.getLeakyPlumbing().equals("Leaky plumbing fixtures"))
		{
			layout_recommendations.setVisibility(View.VISIBLE);
			rowLEACKY_PLUMBING.setVisibility(View.VISIBLE);
			rowLEACKY_PLUMBING_DESCP.setVisibility(View.VISIBLE);
		}

		else
		{
			rowLEACKY_PLUMBING.setVisibility(View.GONE);
			rowLEACKY_PLUMBING_DESCP.setVisibility(View.GONE);
		}
		if(residential_pest_DTO.getGarbageCans().equals("Garbage cans uncovered"))
		{
			layout_recommendations.setVisibility(View.VISIBLE);
			rowGARBAGE_CANS.setVisibility(View.VISIBLE);
			rowGARBAGE_CANS_DESCP.setVisibility(View.VISIBLE);
		}

		else
		{
			rowGARBAGE_CANS.setVisibility(View.GONE);
			rowGARBAGE_CANS_DESCP.setVisibility(View.GONE);
		}
		if(residential_pest_DTO.getMoistureProblem().equals("Moisture damaged wood"))
		{
			layout_recommendations.setVisibility(View.VISIBLE);
			rowMOISTURE_DAMAGE.setVisibility(View.VISIBLE);
			rowMOISTURE_DAMAGE_DESCP.setVisibility(View.VISIBLE);
		}

		else
		{
			rowMOISTURE_DAMAGE.setVisibility(View.GONE);
			rowMOISTURE_DAMAGE_DESCP.setVisibility(View.GONE);
		}
		if(residential_pest_DTO.getGroceryBags().equals("Grocery bags stored improperly"))
		{
			layout_recommendations.setVisibility(View.VISIBLE);
			rowGROCERY_BAGS.setVisibility(View.VISIBLE);
			rowGROCERY_BAGS_DESCP.setVisibility(View.VISIBLE);
		}

		else
		{
			rowGROCERY_BAGS.setVisibility(View.GONE);
			rowGROCERY_BAGS_DESCP.setVisibility(View.GONE);
		}
		if(residential_pest_DTO.getPetFood().equals("Pet food unsealed or left out"))
		{
			layout_recommendations.setVisibility(View.VISIBLE);
			rowPET_FOOD.setVisibility(View.VISIBLE);
			rowPET_FOOD_DESCP.setVisibility(View.VISIBLE);
		}

		else
		{
			rowPET_FOOD.setVisibility(View.GONE);
			rowPET_FOOD_DESCP.setVisibility(View.GONE);
		}
		if(residential_pest_DTO.getRecycledItems().equals("Recycled items stored improperly"))
		{
			layout_recommendations.setVisibility(View.VISIBLE);
			rowRECYCLED_ITEMS.setVisibility(View.VISIBLE);
			rowRECYCLED_ITEMS_DESCP.setVisibility(View.VISIBLE);
		}

		else
		{
			rowRECYCLED_ITEMS.setVisibility(View.GONE);
			rowRECYCLED_ITEMS_DESCP.setVisibility(View.GONE);
		}
		if(residential_pest_DTO.getExcessiveStorage().equals("Excessive storage conditions"))
		{
			layout_recommendations.setVisibility(View.VISIBLE);
			rowEXCESSIVE_STOREGE.setVisibility(View.VISIBLE);
			rowEXCESSIVE_STOREGE_DESCP.setVisibility(View.VISIBLE);
		}

		else
		{
			rowEXCESSIVE_STOREGE.setVisibility(View.GONE);
			rowEXCESSIVE_STOREGE_DESCP.setVisibility(View.GONE);
		}
	}

	private void setValuesForRodentServices()
	{
		// TODO Auto-generated method stub
		if(residential_pest_DTO.getRoofRodent().equals("Roof Pitches"))
		{
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutROOF_PITCHE_SEALED.setVisibility(View.VISIBLE);
			txtROOF_PITCHE_SEALED.setText(residential_pest_DTO.getRoofNeeds());
		}
		else
		{
			layoutROOF_PITCHE_SEALED.setVisibility(View.GONE);
		}
		//Log.e("", ".getRoofVentRodent.."+residential_pest_DTO.getRoofVentRodent().equals("Roof Vents"));
		if(residential_pest_DTO.getRoofVentRodent().equals("Roof Vents"))
		{
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutTODAY_ROOF_VENTS.setVisibility(View.VISIBLE);
			txtTODAY_ROOF_VENTS.setText(residential_pest_DTO.getRoofVentNeeds());
		}
		else
		{
			layoutTODAY_ROOF_VENTS.setVisibility(View.GONE);
		}


		if(residential_pest_DTO.getFlashing().equals("Soffit Vents"))
		{
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutTODAYS_SOFFIT_VENTS.setVisibility(View.VISIBLE);
			txtTODAYS_SOFFIT_VENTS.setText(residential_pest_DTO.getFlashingNeeds());
		}
		else
		{
			layoutTODAYS_SOFFIT_VENTS.setVisibility(View.GONE);
		}
		if(residential_pest_DTO.getFascia().equals("Weep Holes"))
		{
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutTODAYS_WEEP_HOLES.setVisibility(View.VISIBLE);
			txtTODAYS_WEEP_HOLES.setText(residential_pest_DTO.getFasciaNeeds());
		}
		else
		{
			layoutTODAYS_WEEP_HOLES.setVisibility(View.GONE);
		}
		if(residential_pest_DTO.getSiding().equals("Siding of House"))
		{
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutTODAY_SIDING_HOUSES_SEALED.setVisibility(View.VISIBLE);
			txtTODAY_SIDING_HOUSES_SEALED.setText(residential_pest_DTO.getSidingNeeds());
		}
		else
		{
			layoutTODAY_SIDING_HOUSES_SEALED.setVisibility(View.GONE);
		}
		if(residential_pest_DTO.getBreezway().equals("Breezeway from Garage"))
		{
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutBREEZY_WAY_GAAREGE.setVisibility(View.VISIBLE);
			txtBREEZY_WAY_GAAREGE.setText(residential_pest_DTO.getBreezwayNeeds());
		}
		else
		{
			layoutBREEZY_WAY_GAAREGE.setVisibility(View.GONE);
		}

		if(residential_pest_DTO.getGarageDoor().equals("Garage Door"))
		{
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutTODAY_GARAGE_DOOR_SEALED.setVisibility(View.VISIBLE);
			txtTODAY_GARAGE_DOOR_SEALED.setText(residential_pest_DTO.getGarageDoorNeeds());
		}
		else
		{
			layoutTODAY_GARAGE_DOOR_SEALED.setVisibility(View.GONE);
		}

		if(residential_pest_DTO.getDryerVent().equals("A/C, Dryer Vents & Phone line"))
		{
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutTODAYS_AC_DRYER.setVisibility(View.VISIBLE);
			txtTODAYS_AC_DRYER.setText(residential_pest_DTO.getDryerVentNeeds());
		}
		else
		{
			layoutTODAYS_AC_DRYER.setVisibility(View.GONE);
		}

		//	Log.e("", "getNumberLive.."+residential_pest_DTO.getNumberLive().equals("Number of Live Animal Traps installed"));

		if(residential_pest_DTO.getNumberLive().equals("Number of Live Animal Traps installed"))
		{
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutTODAY_NUMBER_LIVEANIMAL_TRAPS_INSTALLES.setVisibility(View.VISIBLE);
			txtTODAY_NUMBER_LIVEANIMAL_TRAPS_INSTALLES.setText(residential_pest_DTO.getNumberLiveNeeds() );
		}
		else
		{
			layoutTODAY_NUMBER_LIVEANIMAL_TRAPS_INSTALLES.setVisibility(View.GONE);
		}

		//	Log.e("", "getNumberSnap.."+residential_pest_DTO.getNumberSnap());

		if(residential_pest_DTO.getNumberSnap().equals("Number of Snap Traps Placed"))
		{
			layoutRODENTSERVICES.setVisibility(View.VISIBLE);
			layoutTODAY_NUMBER_SNAPTRAPS.setVisibility(View.VISIBLE);
			txtTODAY_NUMBER_SNAPTRAPS.setText(residential_pest_DTO.getNumberSnapNeeds());
		}
		else
		{
			layoutTODAY_NUMBER_SNAPTRAPS.setVisibility(View.GONE);
		}
	}

	private void setValuesForMosquitoServices() 
	{
		// TODO Auto-generated method stub
		String strMosquitoServices=residential_pest_DTO.getPlusMosquitoDetail();
		//straudioFile=serviceDTO.getAudioFile();
		if(strMosquitoServices.length()>=1 || 
				!CommonFunction.checkString(residential_pest_DTO.getDurationTime(), "").equals("")
				|| !CommonFunction.checkString(residential_pest_DTO.getSystemRuns(), "").equals(""))
		{
			layout_mosquito_services.setVisibility(View.VISIBLE);

			if(strMosquitoServices.length()>=1)
			{
				txtMosquitoServices.setVisibility(View.VISIBLE);
				txtMosquitoServices.setText(strMosquitoServices);
			}
			else
			{
				txtMosquitoServices.setVisibility(View.GONE);
			}
			//Log.e("residential_pest_DTO.getSystemRuns()...", ""+residential_pest_DTO.getDurationTime()+".."+residential_pest_DTO.getSystemRuns());

			if(!CommonFunction.checkString(residential_pest_DTO.getDurationTime(), "").equals(""))
			{
				rowCheckedRunDurationTime.setVisibility(View.VISIBLE);
				txtCheckedRunDurationTime.setText(residential_pest_DTO.getDurationTime());
			}
			else
			{
				rowCheckedRunDurationTime.setVisibility(View.GONE);
			}
			if(!CommonFunction.checkString(residential_pest_DTO.getSystemRuns(), "").equals(""))
			{
				rowCheckedTimeSystemRuns.setVisibility(View.VISIBLE);
				txtCheckedTimeSystemRuns.setText(residential_pest_DTO.getSystemRuns());

			}
			else
			{
				rowCheckedTimeSystemRuns.setVisibility(View.GONE);
			}
		}
		else
		{
			layout_mosquito_services.setVisibility(View.GONE);
		}
	}

	private void setValuesForPestControlServices() 
	{
		if(!CommonFunction.checkString(strInsidepestControlServices,"" ).equals("") && !CommonFunction.checkString(strInsidepestControlServices,"Inside" ).equals("Inside"))
		{
			String str =strInsidepestControlServices;
			String kept = str.substring( 0, 6);
			String remainder = str.substring(7, str.length()); 
			layout_pestControlserivces.setVisibility(View.VISIBLE);
			txtPestControlHeadInside.setText(kept);
			txtPestControlServiesInside.setText(remainder);
		}
		if(!CommonFunction.checkString(strOutsidepestControlServices,"" ).equals("") && !CommonFunction.checkString(strOutsidepestControlServices,"Outside" ).equals("Outside"))
		{
			
			String str =strOutsidepestControlServices;
			String kept = str.substring( 0, 7);
			String remainder = str.substring(8, str.length()); 
			layout_pestControlserivces.setVisibility(View.VISIBLE);
			txtPestControlHeadOutside.setText(kept);
			txtPestControlServiesOutside.setText(remainder);
		}
		/*if(CommonFunction.checkString(strInsidepestControlServices,"" ).equals("") && CommonFunction.checkString(strOutsidepestControlServices,"" ).equals(""))
		{
			layout_pestControlserivces.setVisibility(View.GONE);
		}*/
		
		// TODO Auto-generated method stub
		/*String a = null,b = null;
		String strPestControlServices=residential_pest_DTO.getPestControlServices();
		//straudioFile=serviceDTO.getAudioFile();
		if(strPestControlServices.contains("Inside") && strPestControlServices.length()!=10 && strPestControlServices.length()!=5)
		{
		 final String[] splitStringArray = strPestControlServices.split("Inside");
		 a = splitStringArray[0];
		 b = "Inside"+splitStringArray[1];
		 int last = a.length() - 1;
		 if (last > 0 && a.charAt(last) == ',') {
		     a = a.substring(0, last);
		 }
		}
		
		if(CommonFunction.checkString(a,"" ).equals(""))
		{
			if(CommonFunction.checkString(b,"" ).equals("") && !CommonFunction.checkString(strPestControlServices,"" ).equals("") && strPestControlServices.contains("Outside") )
			{
				layout_pestControlserivces.setVisibility(View.VISIBLE);
				txtPestControlServiesOutside.setText(strPestControlServices);
			}
			if(CommonFunction.checkString(b,"" ).equals("") && !CommonFunction.checkString(strPestControlServices,"" ).equals("") && !strPestControlServices.contains("Outside") )
			{
				layout_pestControlserivces.setVisibility(View.VISIBLE);
				txtPestControlServiesOutside.setText(strPestControlServices);
			}
			
		}
		else
		{
			txtPestControlServiesOutside.setText(a);
		}
		if(!CommonFunction.checkString(b,"" ).equals(""))
		{
			
			layout_pestControlserivces.setVisibility(View.VISIBLE);
			//Log.e("", "a....."+a);
			//Log.e("", "b....."+b);
			txtPestControlServiesInside.setText("Inside"+b);
		}
		*/
		/*if(CommonFunction.checkString(b,"" ).equals("") && CommonFunction.checkString(a,"" ).equals("") && !CommonFunction.checkString(strPestControlServices,"" ).equals("") )
		{
			txtPestControlServiesInside.setText(strPestControlServices);
		}*/
			
			/*else
		{
			layout_pestControlserivces.setVisibility(View.GONE);
		}*/
	}

	private void setValuesForTermiteServices()
	{
		// TODO Auto-generated method stub
		if(residential_pest_DTO.getFencePest().equals("Fence Inspected"))
		{
			layout_termiteserivces.setVisibility(View.VISIBLE);
			rowVisibleSigns.setVisibility(View.VISIBLE);
			txtVisibleSigns.setText(residential_pest_DTO.getFenceActivity());
		}
		else
		{
			rowVisibleSigns.setVisibility(View.GONE);
		}
		if(residential_pest_DTO.getNumberPest().equals("Number of Monitoring Devices Inspected"))
		{
			layout_termiteserivces.setVisibility(View.VISIBLE);
			rowNumberofMonitoringDevices.setVisibility(View.VISIBLE);
			txtNumberofMonitoringDevices.setText(residential_pest_DTO.getNumberActivitytext());
		}
		else
		{
			rowNumberofMonitoringDevices.setVisibility(View.GONE);
		}
		if(residential_pest_DTO.getWoodPest().equals("Monitoring Devices"))
		{
			layout_termiteserivces.setVisibility(View.VISIBLE);
			rowVisibleTermiteMonitorDevices.setVisibility(View.VISIBLE);
			txtVisibleTermiteMonitorDevices.setText(residential_pest_DTO.getWoodActivity());
		}
		else
		{
			rowVisibleTermiteMonitorDevices.setVisibility(View.GONE);
		}
		if(residential_pest_DTO.getNumberActivity().equals("Termite Monitoring Device replaced"))
		{
			layout_termiteserivces.setVisibility(View.VISIBLE);
			rowTermiteMoniterDevicereplaced.setVisibility(View.VISIBLE);
			txtTermiteMoniterDevicereplaced.setText(residential_pest_DTO.getWoodOption());
		}
		else
		{
			rowTermiteMoniterDevicereplaced.setVisibility(View.GONE);
		}
		//Log.e("","...getExteriorPest..."+residential_pest_DTO.getExteriorPest().equals("Exterior Perimeter of House"));
		if(residential_pest_DTO.getExteriorPest().equals("Exterior Perimeter of House"))
		{
			layout_termiteserivces.setVisibility(View.VISIBLE);
			rowVisibleTermiteActivity.setVisibility(View.VISIBLE);
			txtVisibleTermiteActivity.setText(residential_pest_DTO.getExteriorActivity());
		}
		else
		{
			rowVisibleTermiteActivity.setVisibility(View.GONE);
		}
		if(residential_pest_DTO.getAtticPest().equals("Attic"))
		{
			layout_termiteserivces.setVisibility(View.VISIBLE);
			rowVisibleTermiteAttic.setVisibility(View.VISIBLE);
			txtVisibleTermiteAttic.setText(residential_pest_DTO.getAtticActivity());
		}
		else
		{
			rowVisibleTermiteAttic.setVisibility(View.GONE);
		}
		if(residential_pest_DTO.getInteriorPest().equals("Interior of House"))
		{
			layout_termiteserivces.setVisibility(View.VISIBLE);
			rowVisibleTermiteInteriorHouse.setVisibility(View.VISIBLE);
			txtVisibleTermiteInteriorHouse.setText(residential_pest_DTO.getInteriorActivity());
		}
		else
		{
			rowVisibleTermiteInteriorHouse.setVisibility(View.GONE);
		}
	}

	private void setValuesForPestActivityByZone() 
	{
		// TODO Auto-generated method stub
		// set value for pest rodent observed today
		String strPestActivityZone=residential_pest_DTO.getPestActivity();
		//Log.e("", "strpestActivityByZone..in final."+strPestActivityZone);
		aList=null;
		aList= new ArrayList<String>(Arrays.asList(strPestActivityZone.split(",")));
		//Log.e("", "aList.size().."+aList.get(0));
		if(aList.size()>0 && !aList.get(0).equals(""))
		{
			LAYOUT_PEST_ZONE.setVisibility(View.VISIBLE);
			for(int i=0;i<aList.size();i++)
			{ 
				//Log.e("", "alist.geti.."+aList.get(i));
				if(aList.get(i).equals("Fence Line"))
				{
					rowFenceLines.setVisibility(View.VISIBLE);
					txtFenceLines.setText(residential_pest_DTO.getFence());
				}

				if(aList.get(i).equals("Back Yard"))
				{
					rowBackYard.setVisibility(View.VISIBLE);
					txtBackYard.setText(residential_pest_DTO.getBackYard());
				}

				if(aList.get(i).equals("Front Yard"))
				{
					rowFrontyard.setVisibility(View.VISIBLE);
					txtFrontYard.setText(residential_pest_DTO.getFrontYard());
				}

				if(aList.get(i).equals("Exterior Perimeter of House"))
				{
					rowExteriorperimeterofhouse.setVisibility(View.VISIBLE);
					txtExteriorperimeterofhouse.setText(residential_pest_DTO.getExteriorPerimeter());
				}

				if(aList.get(i).equals("Roof Line"))
				{
					rowRoofLine.setVisibility(View.VISIBLE);
					txtRoofLine.setText(residential_pest_DTO.getRoofline());
				}

				if(aList.get(i).equals("Garage"))
				{
					rowGarage.setVisibility(View.VISIBLE);
					txtGarage.setText(residential_pest_DTO.getGarage());
				}

				if(aList.get(i).equals("Attic"))
				{
					rowAttic.setVisibility(View.VISIBLE);
					txtAttic.setText(residential_pest_DTO.getAttic());
				}

				if(aList.get(i).equals("Interior of House"))
				{
					rowInteriorofhouse.setVisibility(View.VISIBLE);
					txtInteriorofhouse.setText(residential_pest_DTO.getInteriorHouse());
				}

			}
		}

		else
		{
			LAYOUT_PEST_ZONE.setVisibility(View.GONE);
		}
	}

	private void setValuesForOutsideOnly() 
	{
		// TODO Auto-generated method stub
		String	strCheckOutsideService = residential_pest_DTO.getOutsideOnly();
		if(strCheckOutsideService.equals("true"))
		{
			LAYOUT_OUTSIDE_SERVICE.setVisibility(View.VISIBLE); 
		}
		else
		{
			LAYOUT_OUTSIDE_SERVICE.setVisibility(View.GONE);

		}
	}

	private void setValuesForInspectedTreatedFor() 
	{
		// TODO Auto-generated method stub
		String strInspectedTreatedFor=residential_pest_DTO.getInsectActivity();
		//straudioFile=serviceDTO.getAudioFile();
		if(strInspectedTreatedFor.length()>=1)
		{
			LAYOUT_INSPECTED_TREATED.setVisibility(View.VISIBLE);
			txtINSPECTED_TREATED.setText(strInspectedTreatedFor);
		}
		else
		{
			LAYOUT_INSPECTED_TREATED.setVisibility(View.GONE);
		}
		if(!CommonFunction.checkString(residential_pest_DTO.getAnts(), "").equals(""))
		{	
			layoutAnts.setVisibility(View.VISIBLE);	
			txtAnts.setText(residential_pest_DTO.getAnts());
		}
		else
		{
			layoutAnts.setVisibility(View.GONE);
		}
		if(!CommonFunction.checkString(residential_pest_DTO.getOtherPest(), "").equals(""))
		{	
			layoutOtherPest.setVisibility(View.VISIBLE);
			txtOtherPest.setText(residential_pest_DTO.getOtherPest());
		}
		else
		{
			layoutOtherPest.setVisibility(View.GONE);
		}
	}

	private void setValuesForAreasInspected() 
	{
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		String strAreasInspected=residential_pest_DTO.getAreasInspected();
		//straudioFile=serviceDTO.getAudioFile();
		if(strAreasInspected.length()>=1)
		{
			LAYOUT_AREASINSPECTED.setVisibility(View.VISIBLE);
			txtAREAS_INSPECTED.setText(strAreasInspected);
		}
		else
		{
			LAYOUT_AREASINSPECTED.setVisibility(View.GONE);
		}
	}

	private void setValuesForServiceFor()
	{
		// TODO Auto-generated method stub
		String strServiceFor=residential_pest_DTO.getServiceFor();
		//straudioFile=serviceDTO.getAudioFile();
		if(strServiceFor.length()>=1)
		{
			LAYOUT_SERVICEFOR.setVisibility(View.VISIBLE);
			txtSERVICE_FOR.setText(strServiceFor);
		}
		else
		{
			LAYOUT_SERVICEFOR.setVisibility(View.GONE);
		}
	}

	private void setValuesForHeader() 
	{
		// TODO Auto-generated method stub
		//txtServiceReportAddress.setText(generalInfoDTO.getServiceAddress());
		edtAmount.setText("$ "+residential_pest_DTO.getAmount());
		edtCheckNo.setText(generalInfoDTO.getCheckNo());
		edtDrivingLic.setText(generalInfoDTO.getLicenseNo());
		txtTimeInHeader.setText(generalInfoDTO.getTimeIn());
		edtCustomer.setText(residential_pest_DTO.getCustomer());
		txtAcct.setText(generalInfoDTO.getAccountNo());
		
		String strServiceAddress=generalInfoDTO.getAddress_Line1()+", "+generalInfoDTO.getAddress_Line2()+" "+generalInfoDTO.getCity()+", "+generalInfoDTO.getState()+", "+generalInfoDTO.getZipCode();
		edtAddress.setText(strServiceAddress);

		String strBillingAddress=generalInfoDTO.getBAddress_Line1()+", "+generalInfoDTO.getBAddress_Line2()+" "+generalInfoDTO.getBCity()+", "+generalInfoDTO.getBState()+", "+generalInfoDTO.getBZipCode();
		edtBillingAddress.setText(strBillingAddress);
		
		edtHomePhone.setText(generalInfoDTO.getHome_Phone());
		edtCellPhone.setText(generalInfoDTO.getCell_Phone());
		edtZip.setText(generalInfoDTO.getZipCode());
		edtAcct.setText(generalInfoDTO.getAccountNo());
		txtEmail.setText(generalInfoDTO.getNewEmail());
		txtOrder.setText(generalInfoDTO.getOrder_Number());
		txtOrderNumber.setText(generalInfoDTO.getOrder_Number());
		
		ArrayList<RadioButton> listRadioButton = new ArrayList<RadioButton>();
		listRadioButton.add(radioCash);
		listRadioButton.add(radioCheck);
		listRadioButton.add(radioCreditCard);
		listRadioButton.add(radioNoCharge);
		listRadioButton.add(radioBillLater);
		listRadioButton.add(radioPreBill);
		//Log.e("getPaymentType...",listRadioButton.size()+"");

		for(int i=0; i<listRadioButton.size() ; i++)
		{

			String selection =	listRadioButton.get(i).getText().toString();
			//	Log.v("","getPaymentType..."+selection+"..."+residential_pest_DTO.getPaymentType());
			if(selection.equals(residential_pest_DTO.getPaymentType()))
			{
				listRadioButton.get(i).setChecked(true);
				setView(i);
			}
		}
	}
	public void setView(int checkedId)
	{
		switch (checkedId) 
		{
		case 0:
			linearlayout_DrivingLic.setVisibility(View.GONE);
			linearlayout_CheckNo.setVisibility(View.GONE);
			linearlayout_amount.setVisibility(View.VISIBLE);
			break;
		case 1:
			linearlayout_DrivingLic.setVisibility(View.VISIBLE);
			linearlayout_CheckNo.setVisibility(View.VISIBLE);
			linearlayout_amount.setVisibility(View.VISIBLE);

			break;
		case 2:
			linearlayout_DrivingLic.setVisibility(View.GONE);
			linearlayout_CheckNo.setVisibility(View.GONE);
			linearlayout_amount.setVisibility(View.VISIBLE);
			break;
		case 3:
			linearlayout_DrivingLic.setVisibility(View.GONE);
			linearlayout_CheckNo.setVisibility(View.GONE);
			linearlayout_amount.setVisibility(View.GONE);

			break;
		case 4:
			linearlayout_DrivingLic.setVisibility(View.GONE);
			linearlayout_CheckNo.setVisibility(View.GONE);
			linearlayout_amount.setVisibility(View.GONE);

			break;
		case 5:
			linearlayout_DrivingLic.setVisibility(View.GONE);
			linearlayout_CheckNo.setVisibility(View.GONE);
			linearlayout_amount.setVisibility(View.GONE);

			break;
		default:
			break;
		}
	}
	private void initialize() 
	{
		// TODO Auto-generated method stub
		logDTO=new LogDTO();
		imageViewTechnicianSign=(ImageView)findViewById(R.id.imageViewTechnicianSign);
		imageViewCustomerSign=(ImageView)findViewById(R.id.imageViewCustomerSign);
		btnBack=(Button)findViewById(R.id.btnBack);
		btnBack.setOnClickListener(new OnButtonClick()); 

		btnSendEmail=(Button)findViewById(R.id.btnSendEmail);
		btnSendEmail.setOnClickListener(new OnButtonClick());

		btnsubmitToPestpac = (Button)findViewById(R.id.btnsubmitToPestpac);
		btnsubmitToPestpac.setOnClickListener(new OnButtonClick());

		btnSignature=(Button)findViewById(R.id.btnServiceSpecialistSignature);
		btnSignature.setOnClickListener(new OnButtonClick());
		btnIagreeSignature=(Button)findViewById(R.id.btnIAgreetoPricing);
		btnIagreeSignature.setOnClickListener(new OnButtonClick());
		btnRecordAudio=(Button)findViewById(R.id.btnRecord);
		btnRecordAudio.setOnClickListener(new OnButtonClick());
		imageListView=(HorizontalListView)findViewById(R.id.listImg);
		btnAfterImage=(Button)findViewById(R.id.btnCamera);
		btnAfterImage.setOnClickListener(new OnButtonClick());
		playBtn = (ImageView)findViewById(R.id.play);
		stopPlayBtn = (ImageView)findViewById(R.id.stopPlay);
		playBtn.setOnClickListener(new OnButtonClick());
		stopPlayBtn.setOnClickListener(new OnButtonClick());

		generalInfoDTO=new GeneralInfoDTO();
		residential_pest_DTO=new HResidential_pest_DTO();
		houstonFlowFunctions=new HoustonFlowFunctions(getApplicationContext());

		imageListView.setOnItemLongClickListener(new OnGalleryClick());
		imageListView.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view,int position, long id)
			{
				Bitmap bmp= listImg.get(position).getImageBitMap();
				if(bmp!=null)
				{
					bitmap=bmp;
					Intent intent=new Intent(HR_TodayServiceInvoice.this, FullScreenViewActivity.class);
					intent.putExtra("from","report");
					startActivity(intent);
				}
			}

		});


		// radio button
		radioCash=(RadioButton)findViewById(R.id.radioCash);
		radioCheck=(RadioButton)findViewById(R.id.radioCheck);
		radioCreditCard=(RadioButton)findViewById(R.id.radioCreditCard);
		radioNoCharge=(RadioButton)findViewById(R.id.radioNoCharge);
		radioPreBill=(RadioButton)findViewById(R.id.radioPreBill);
		radioBillLater=(RadioButton)findViewById(R.id.radioBillLater);

		//text view
		textViewServicePageHeader=(TextView)findViewById(R.id.textViewServicePageHeader);
		textViewServicePageHeader.setText("Service For");
		txtPestControlHeadOutside=(TextView)findViewById(R.id.txtPestControlHeadOutside);
		txtPestControlHeadInside=(TextView)findViewById(R.id.txtPestControlHeadInside);
		txtAcct=(TextView)findViewById(R.id.txtAccountNumber);
		txtTODAY_NUMBER_SNAPTRAPS=(TextView)findViewById(R.id.txtTODAY_NUMBER_SNAPTRAPS);
		txtROOF_PITCHE_SEALED=(TextView)findViewById(R.id.txtROOF_PITCHE_SEALED);
		txtBREEZY_WAY_GAAREGE=(TextView)findViewById(R.id.txtBREEZY_WAY_GAAREGE);
		txtTODAY_ROOF_VENTS=(TextView)findViewById(R.id.txtTODAY_ROOF_VENTS);
		txtTODAY_GARAGE_DOOR_SEALED=(TextView)findViewById(R.id.txtTODAY_GARAGE_DOOR_SEALED);
		txtTODAYS_SOFFIT_VENTS=(TextView)findViewById(R.id.txtTODAYS_SOFFIT_VENTS);
		txtTODAYS_AC_DRYER=(TextView)findViewById(R.id.txtTODAYS_AC_DRYER);
		txtTODAYS_WEEP_HOLES=(TextView)findViewById(R.id.txtTODAYS_WEEP_HOLES);
		txtTODAY_NUMBER_LIVEANIMAL_TRAPS_INSTALLES =(TextView)findViewById(R.id.txtTODAY_NUMBER_LIVEANIMAL_TRAPS_INSTALLES);
		txtTODAY_SIDING_HOUSES_SEALED=(TextView)findViewById(R.id.txtTODAY_SIDING_HOUSES_SEALED);;

		txtServiceReportAddress=(TextView)findViewById(R.id.txtServiceReportAddress);
		txtTimeIn=(TextView)findViewById(R.id.txtTimeIn);
		txtTimeInHeader=(TextView)findViewById(R.id.txtTimeInHeader);
		txtEmail=(TextView)findViewById(R.id.txtEmail);
		txtSERVICE_FOR=(TextView)findViewById(R.id.txtSERVICE_FOR);
		txtAREAS_INSPECTED=(TextView)findViewById(R.id.txtAREAS_INSPECTED);
		txtINSPECTED_TREATED=(TextView)findViewById(R.id.txtINSPECTED_TREATED);
		txtOutsideonly=(TextView)findViewById(R.id.txtOutsideonly);
		txtAnts=(TextView)findViewById(R.id.txtAnts);
		txtOtherPest=(TextView)findViewById(R.id.txtOtherPest);
		txtFenceLines=(TextView)findViewById(R.id.txtFenceLines);
		txtBackYard=(TextView)findViewById(R.id.txtBackYard);
		txtFrontYard=(TextView)findViewById(R.id.txtFrontYard);
		txtExteriorperimeterofhouse=(TextView)findViewById(R.id.txtExteriorperimeterofhouse);
		txtRoofLine=(TextView)findViewById(R.id.txtRoofLine);
		txtGarage=(TextView)findViewById(R.id.txtGarage);
		txtAttic=(TextView)findViewById(R.id.txtAttic);
		txtInteriorofhouse=(TextView)findViewById(R.id.txtInteriorofhouse);
		txtVisibleSigns=(TextView)findViewById(R.id.txtVisibleSigns);
		txtNumberofMonitoringDevices=(TextView)findViewById(R.id.txtNumberofMonitoringDevices);
		txtVisibleTermiteMonitorDevices=(TextView)findViewById(R.id.txtVisibleTermiteMonitorDevices);
		txtTermiteMoniterDevicereplaced=(TextView)findViewById(R.id.txtTermiteMoniterDevicereplaced);
		txtVisibleTermiteActivity=(TextView)findViewById(R.id.txtVisibleTermiteActivity);
		txtVisibleTermiteAttic=(TextView)findViewById(R.id.txtVisibleTermiteAttic);
		txtVisibleTermiteInteriorHouse=(TextView)findViewById(R.id.txtVisibleTermiteInteriorHouse);
		txtMosquitoServices=(TextView)findViewById(R.id.txtMosquitoServices);
		txtCheckedRunDurationTime=(TextView)findViewById(R.id.txtCheckedRunDurationTime);
		txtCheckedTimeSystemRuns=(TextView)findViewById(R.id.txtCheckedTimeSystemRuns);
		txtTechComments=(TextView)findViewById(R.id.txtTechComment);
		txtServicetech=(TextView)findViewById(R.id.txtServiceTech);
		txtOrder=(TextView)findViewById(R.id.txtOrder);
		txtEmp=(TextView)findViewById(R.id.txtEmp);
		txtLicence=(TextView)findViewById(R.id.txtLIC);
		txtdate=(TextView)findViewById(R.id.txtDate);
		txtPestControlServiesInside=(TextView)findViewById(R.id.txtPestControlServiesInside);
		txtPestControlServiesOutside=(TextView)findViewById(R.id.txtPestControlServiesOutside);
		txtOrderNumber=(TextView)findViewById(R.id.txtOrderNumber);
		textRecordingPoint=(TextView)findViewById(R.id.textRecordingPoint);
		//edit text
		edtZip=(EditText)findViewById(R.id.edtZIP);
		edtAmount=(EditText)findViewById(R.id.edtAmount);
		edtCheckNo=(EditText)findViewById(R.id.edtCheckNo);
		edtDrivingLic=(EditText)findViewById(R.id.edtDrivingLic);
		edtCustomer=(EditText)findViewById(R.id.edtCustomer);
		edtAddress=(EditText)findViewById(R.id.edtADDRESS);
		edtBillingAddress=(EditText)findViewById(R.id.edtBILLING_ADRESS);
		edtHomePhone=(EditText)findViewById(R.id.edtHOME_PHONE);
		edtCellPhone=(EditText)findViewById(R.id.edtCELL_PHONE);
		edtAcct=(EditText)findViewById(R.id.edtACCT);
		edtChargerForCurrentServices=(EditText)findViewById(R.id.edtChargesCurentService);
		edtBalanceDue=(EditText)findViewById(R.id.edtBALANCE_DUE);
		edtTax=(EditText)findViewById(R.id.edtTax);
		edtEmp=(EditText)findViewById(R.id.edtEMP);
		edtTotalAmount=(EditText)findViewById(R.id.edtTotal);
		edtOpenOfficeNote=(EditText)findViewById(R.id.edtOpenOfficeNote);
		edtOpenOfficeNote.setFilters(CommonFunction.limitchars(500));
		layout_todays_service_report=(LinearLayout)findViewById(R.id.todays_service_report);
		layoutChemicalHeader=(LinearLayout)findViewById(R.id.layoutChemicalHeader);
		mainLayout=(LinearLayout)findViewById(R.id.mainLayout);
		layoutTODAY_NUMBER_SNAPTRAPS=(LinearLayout)findViewById(R.id.layoutTODAY_NUMBER_SNAPTRAPS);
		layoutRODENTSERVICES=(LinearLayout)findViewById(R.id.layoutRODENTSERVICES);
		layoutROOF_PITCHE_SEALED=(LinearLayout)findViewById(R.id.layoutROOF_PITCHE_SEALED);
		layoutBREEZY_WAY_GAAREGE=(LinearLayout)findViewById(R.id.layoutBREEZY_WAY_GAAREGE);
		layoutTODAY_ROOF_VENTS=(LinearLayout)findViewById(R.id.layoutTODAY_ROOF_VENTS);
		layoutTODAY_GARAGE_DOOR_SEALED=(LinearLayout)findViewById(R.id.layoutTODAY_GARAGE_DOOR_SEALED);
		layoutTODAYS_SOFFIT_VENTS=(LinearLayout)findViewById(R.id.layoutTODAYS_SOFFIT_VENTS);
		layoutTODAYS_AC_DRYER=(LinearLayout)findViewById(R.id.layoutTODAYS_AC_DRYER);
		layoutTODAYS_WEEP_HOLES=(LinearLayout)findViewById(R.id.layoutTODAYS_WEEP_HOLES);
		layoutTODAY_NUMBER_LIVEANIMAL_TRAPS_INSTALLES=(LinearLayout)findViewById(R.id.layoutTODAY_NUMBER_LIVEANIMAL_TRAPS_INSTALLES);
		layoutTODAY_SIDING_HOUSES_SEALED=(LinearLayout)findViewById(R.id.layoutTODAY_SIDING_HOUSES_SEALED);;
		layout_pestControlserivces=(LinearLayout)findViewById(R.id.layout_pestControlserivces);
		linearlayout_amount=(LinearLayout)findViewById(R.id.linearlayout_amount);
		linearlayout_CheckNo=(LinearLayout)findViewById(R.id.linearlayout_CheckNo);
		linearlayout_DrivingLic=(LinearLayout)findViewById(R.id.linearlayout_DrivingLic);
		Layout_TechComment_desc=(LinearLayout)findViewById(R.id.Layout_TechComment_desc);
		Layout_TechComment=(LinearLayout)findViewById(R.id.Layout_TechComment);
		layoutOtherPest=(LinearLayout)findViewById(R.id.layoutOtherPest);
		layoutAnts=(LinearLayout)findViewById(R.id.layoutAnts);
		rowFenceLines=(LinearLayout)findViewById(R.id.rowFenceLines);
		rowBackYard=(LinearLayout)findViewById(R.id.rowBackYard);
		rowFrontyard=(LinearLayout)findViewById(R.id.rowFrontyard);
		rowExteriorperimeterofhouse=(LinearLayout)findViewById(R.id.rowExteriorperimeterofhouse);
		rowRoofLine=(LinearLayout)findViewById(R.id.rowRoofLine);
		rowGarage=(LinearLayout)findViewById(R.id.rowGarage);
		rowAttic=(LinearLayout)findViewById(R.id.rowInAttic);
		rowInteriorofhouse=(LinearLayout)findViewById(R.id.rowInteriorofhouse);
		rowVisibleSigns=(LinearLayout)findViewById(R.id.rowVisibleSigns);
		rowNumberofMonitoringDevices=(LinearLayout)findViewById(R.id.rowNumberofMonitoringDevices);
		rowVisibleTermiteMonitorDevices=(LinearLayout)findViewById(R.id.rowVisibleTermiteMonitorDevices);
		rowTermiteMoniterDevicereplaced=(LinearLayout)findViewById(R.id.rowTermiteMoniterDevicereplaced);
		rowVisibleTermiteActivity=(LinearLayout)findViewById(R.id.rowVisibleTermiteActivity);
		rowVisibleTermiteAttic=(LinearLayout)findViewById(R.id.rowVisibleTermiteAttic);
		rowVisibleTermiteInteriorHouse=(LinearLayout)findViewById(R.id.rowVisibleTermiteInteriorHouse);
		rowMosquitoServices=(LinearLayout)findViewById(R.id.rowMosquitoServices);
		rowCheckedRunDurationTime=(LinearLayout)findViewById(R.id.rowCheckedRunDurationTime);
		rowCheckedTimeSystemRuns=(LinearLayout)findViewById(R.id.rowCheckedTimeSystemRuns);
		rowINSPECTED_TREATED=(LinearLayout)findViewById(R.id.rowINSPECTED_TREATED);
		LAYOUT_SERVICEFOR=(LinearLayout)findViewById(R.id.LAYOUT_SERVICEFOR);
		LAYOUT_AREASINSPECTED=(LinearLayout)findViewById(R.id.LAYOUT_AREASINSPECTED);
		LAYOUT_INSPECTED_TREATED=(LinearLayout)findViewById(R.id.LAYOUT_INSPECTED_TREATED);
		LAYOUT_OUTSIDE_SERVICE=(LinearLayout)findViewById(R.id.LAYOUT_OUTSIDE_SERVICE);
		LAYOUT_PEST_ZONE=(LinearLayout)findViewById(R.id.LAYOUT_PEST_ZONE);
		layout_termiteserivces=(LinearLayout)findViewById(R.id.layout_termiteserivces);
		layout_mosquito_services=(LinearLayout)findViewById(R.id.layout_mosquito_services);
		layout_recommendations=(LinearLayout)findViewById(R.id.layout_recommendations);
		//table roe
		rowBRANCHES_HOUSE_DESCP=(LinearLayout)findViewById(R.id.rowBRANCHES_HOUSE_DESCP);
		rowBRANCHES_HOUSE=(LinearLayout)findViewById(R.id.rowBRANCHES_HOUSE);
		rowFIREWOOD_FOUNDATION_DESCP=(LinearLayout)findViewById(R.id.rowFIREWOOD_FOUNDATION_DESCP);
		rowFIREWOOD_FOUNDATION=(LinearLayout)findViewById(R.id.rowFIREWOOD_FOUNDATION);
		rowDEBRIS_CRAWL_DESCP=(LinearLayout)findViewById(R.id.rowDEBRIS_CRAWL_DESCP);
		rowDEBRIS_CRAWL=(LinearLayout)findViewById(R.id.rowDEBRIS_CRAWL);
		rowEXCESSIVE_PLANT_DESCP=(LinearLayout)findViewById(R.id.rowEXCESSIVE_PLANT);
		rowEXCESSIVE_PLANT=(LinearLayout)findViewById(R.id.rowEXCESSIVE_PLANT_DESCP);
		rowSOIL_ABOVE_DESCP=(LinearLayout)findViewById(R.id.rowSOIL_ABOVE_DESCP);
		rowSOIL_ABOVE=(LinearLayout)findViewById(R.id.rowSOIL_ABOVE);
		rowWOOD_SOIL=(LinearLayout)findViewById(R.id.rowWOOD_SOIL);
		rowWOOD_SOIL_DESCP=(LinearLayout)findViewById(R.id.rowWOOD_SOIL_DESCP);
		rowDEBRIS_ON_ROOF_DESCP=(LinearLayout)findViewById(R.id.rowDEBRIS_ON_ROOF_DESCP);
		rowDEBRIS_ON_ROOF=(LinearLayout)findViewById(R.id.rowDEBRIS_ON_ROOF);
		rowSTANDING_WATER_DESCP=(LinearLayout)findViewById(R.id.rowSTANDING_WATER_DESCP);
		rowSTANDING_WATER=(LinearLayout)findViewById(R.id.rowSTANDING_WATER);
		rowMOISTURE_PROBLM_DESCP=(LinearLayout)findViewById(R.id.rowMOISTURE_PROBLM_DESCP);
		rowMOISTURE_PROBLM=(LinearLayout)findViewById(R.id.rowMOISTURE_PROBLM);
		rowOPENINGS_PLUMBING_DESCP=(LinearLayout)findViewById(R.id.rowOPENINGS_PLUMBING_DESCP);
		rowOPENINGS_PLUMBING=(LinearLayout)findViewById(R.id.rowOPENINGS_PLUMBING);
		rowEXCESSIVE_GAPS=(LinearLayout)findViewById(R.id.rowEXCESSIVE_GAPS);
		rowEXCESSIVE_GAPS_DESCP=(LinearLayout)findViewById(R.id.rowEXCESSIVE_GAPS_DESCP);
		rowLEACKY_PLUMBING=(LinearLayout)findViewById(R.id.rowLEACKY_PLUMBING_FIXTURES);
		rowLEACKY_PLUMBING_DESCP=(LinearLayout)findViewById(R.id.rowLEACKY_PLUMBING_DESCP);
		rowGARBAGE_CANS=(LinearLayout)findViewById(R.id.rowGARBAGE_CANS);
		rowGARBAGE_CANS_DESCP=(LinearLayout)findViewById(R.id.rowGARBAGE_CANS_DESCP);
		rowMOISTURE_DAMAGE_DESCP=(LinearLayout)findViewById(R.id.rowMOISTURE_DAMAGE_DESCP);
		rowMOISTURE_DAMAGE=(LinearLayout)findViewById(R.id.rowMOISTURE_DAMAGE);
		rowGROCERY_BAGS_DESCP=(LinearLayout)findViewById(R.id.rowGROCERY_BAGS_DESCP);
		rowGROCERY_BAGS=(LinearLayout)findViewById(R.id.rowGROCERY_BAGS);
		rowPET_FOOD_DESCP=(LinearLayout)findViewById(R.id.rowPET_FOOD_DESCP);
		rowPET_FOOD=(LinearLayout)findViewById(R.id.rowPET_FOOD);
		rowRECYCLED_ITEMS_DESCP=(LinearLayout)findViewById(R.id.rowRECYCLED_ITEMS_DESCP);
		rowRECYCLED_ITEMS=(LinearLayout)findViewById(R.id.rowRECYCLED_ITEMS);
		rowEXCESSIVE_STOREGE_DESCP=(LinearLayout)findViewById(R.id.rowEXCESSIVE_STOREGE_DESCP);
		rowEXCESSIVE_STOREGE=(LinearLayout)findViewById(R.id.rowEXCESSIVE_STOREGE);

	}
	class OnButtonClick implements OnClickListener 
	{
		@Override
		public void onClick(View v) 
		{
			// TODO Auto-generated method stub

			switch (v.getId()) 
			{
			case R.id.play:
				// TODO Auto-generated method stub
				//stopPlayBtn.setVisibility(v.VISIBLE);
				if(!CommonFunction.checkString(straudioFile, "").equals(""))
				{
					play(v);
				}
				else
				{
				//.	text.setText("Recording Point: Audio File Not Available");
				}
				break;
			case R.id.stopPlay:
				stopPlay();
				break;	
			case R.id.btnBack:
				finish();
				break;
			case R.id.btnsubmitToPestpac:
				//btnsubmitToPestpac.setVisibility(View.GONE);
				//btnSendEmail.setVisibility(View.VISIBLE);
				//submitService();
				isValidate = true;
				MandatoryList.clear();
				validate();
				if(isValidate)
				{
				btnsubmitToPestpac.setEnabled(false);	
				showSendEmailDailog();
				}
				else
				{
					charSequenceItems = MandatoryList.toArray(new CharSequence[MandatoryList.size()]);
					createMessageList(charSequenceItems,"");
				}
				break;

			case R.id.btnSendEmail:
				break;

			case R.id.btnServiceSpecialistSignature:
				ShowSignatureDailog("Service Specialist Signature",1);
				break;

			case R.id.btnIAgreetoPricing:
				ShowSignatureDailog("I Agree to Pricing and Terms & Condition",2);
				break;

			case R.id.btnRecord:
				stopPlay();
				Intent intent=new Intent(getApplicationContext(),AudioRecordingActivity.class);
				intent.putExtra(ParameterUtil.ServiceID_new,String.valueOf(serviceID_new));
				startActivityForResult(intent,RECORD_AUDIO_REQUEST_CODE);
				break;
			case R.id.btnCamera:
				if (tempStoreImageName.size() != 3)
				{
					if (CommonFunction.isSdPresent())
					{
						takePicture();
					}
					else
					{
						Toast.makeText(getApplicationContext(),"SdCard Not Present", Toast.LENGTH_SHORT).show();
					}
				}
				else
				{
					Toast.makeText(getApplicationContext(),"Max number of upload image exceeded.",Toast.LENGTH_SHORT).show();
				}
				break;
			default:
				break;
			}
		}
	}
	private final Dialog createMessageList(final CharSequence[] messageList, /*final EditText object*/ String string)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(HR_TodayServiceInvoice.this);
		builder.setTitle(getResources().getString(R.string.REQUIRED_INFORMATION));
		builder.setItems(charSequenceItems,new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton)
			{
				//  LoggerUtil.e("","E' stato premuto il pulsante: "+messageList[whichButton]);   
			}
		});
	     builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton)
			{
				
			}
		});
		return builder.show();
	}
	public void submitService()
	{
		residential_pest_DTO.setTimeout(CommonFunction.getCurrentTime());
		residential_pest_DTO.setTimeOutDateTime(CommonFunction.getCurrentDateTime());
		residential_pest_DTO.setIsCompleted("true");
		residential_pest_DTO.setTimeIn(generalInfoDTO.getTimeIn());
		residential_pest_DTO.setTimeInDateTime(generalInfoDTO.getTimeInDateTime());
		strmultipleImageName="";
		for (int i = 0; i < tempStoreImageName.size(); i++)
		{
			strmultipleImageName += tempStoreImageName.get(i).toString().trim()+ ",";
		}
		if(strmultipleImageName.length()>0)
		{
			strmultipleImageName = strmultipleImageName.substring(0,strmultipleImageName.length()-1);
		}
		LoggerUtil.e("Before ImageName", strmultipleImageName);
		LoggerUtil.e("Audio ImamgeName", straudioFile+"");
		generalInfoDTO.setAudioFile(straudioFile);
		generalInfoDTO.setAfterImage(strmultipleImageName);
		generalInfoDTO.setIsCompleted("true");
		generalInfoDTO.setIsElectronicSignatureAvailable("false");
		generalInfoDTO.setOpenWONotificationFlag("false");
		if(residential_pest_DTO.getPaymentType().equals("Cash")||residential_pest_DTO.getPaymentType().equals("Check"))
		{
			generalInfoDTO.setPayment_Collection_Status("Open");
		}
		else
		{
			generalInfoDTO.setPayment_Collection_Status("Closed");
		}
		generalInfoDTO.setNote_Status("Open");
		generalInfoDTO.setServiceNameType("HoustonResidential");
		generalInfoDTO.setServiceStatus("Complete");
		generalInfoDTO.setOffice_Note_ByRep(edtOpenOfficeNote.getText().toString());
		generalInfoDTO.setLocationBalance(generalInfoDTO.getLocationBalance());

		int i =	houstonFlowFunctions.update(generalInfoDTO);
		//Log.e("Update", i+"");
		//residential_pest_DTO.setLicenseNo(txtLicence.getText().toString());
		residential_pest_DTO.setSignature(strCustomerSignature);
		residential_pest_DTO.setServiceSignature(strTechnicianSignature);
		int j = HoustonFlowFunctions.updateResidential_Pest(residential_pest_DTO);

	    //	Log.e("Update", j+"");
		//startService(new Intent(getApplicationContext(), SynchServiceToServerServices.class));
		Toast.makeText(getApplicationContext(), "Record Saved", Toast.LENGTH_LONG).show();
	}

	public void validate() {
		// TODO Auto-generated method stub
		if(tempStoreImageName.size() == 0)
		{
			MandatoryList.add("Add atleast one After Image");
			isValidate=false;
		}
	}
	ArrayList<String> tempImagList = new ArrayList<String>();
	ArrayList<ImgDto> listImg = new ArrayList<ImgDto>();
	GalleryImageAdapter adapter;
	String ImageName;
	public static Bitmap bitmap;
	public static int REQUEST_Camera_ACTIVITY_CODE=101;
	int imageCount=1;
	public void takePicture() 
	{
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		ImageName =generalInfoDTO.getServiceID_new()+"_"+imageCount+"after.jpg"; //CommonFunction.CreateImageName();
		Uri imageUri = Uri.fromFile(CommonFunction.getFileLocation(getApplicationContext(), ImageName));
		intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
		intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
		intent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, Integer.toString(1024*1024));
		startActivityForResult(intent, REQUEST_Camera_ACTIVITY_CODE);

	}
	public void play(View view)
	{
		try
		{
			if (myPlayer != null)
			{
				myPlayer.stop();
				myPlayer.release();
				myPlayer = null;
			}
			myPlayer = new MediaPlayer();
			//Log.e("straudioFile", straudioFile);
			myPlayer.setDataSource(CommonFunction.getAudioFilePath(straudioFile).toString());
			myPlayer.prepare();
			myPlayer.start();
			playBtn.setEnabled(false);
			stopPlayBtn.setEnabled(true);
			//text.setText("Recording Point: Playing");
			stopPlayBtn.setVisibility(View.VISIBLE);
			playBtn.setVisibility(View.GONE);
			Toast.makeText(getApplicationContext(), "Start play the recording...",Toast.LENGTH_SHORT).show();
		}
		catch (Exception e)
		{
			//text.setText("Recording Point: Audio File Not Available");
			Log.e("Exception", e+"..");
		}
		
		myPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener(){
		    public void onCompletion(MediaPlayer mp){
		        // Perform your steps here, eg.
		        stopPlayBtn.setVisibility(View.GONE);
		        playBtn.setVisibility(View.VISIBLE);
		        playBtn.setEnabled(true);
				//stopPlayBtn.setEnabled(true);
		        // (Make sure you make v final before)
		    }
		});
	}
	public void stopPlay()
	{
		try 

		{
			if (myPlayer != null)
			{
				myPlayer.stop();
				myPlayer.release();
				myPlayer = null;
				playBtn.setEnabled(true);



				stopPlayBtn.setEnabled(false);
				stopPlayBtn.setVisibility(View.GONE);
				playBtn.setVisibility(View.VISIBLE);
				//text.setText("Recording Point: Stop playing");

				//Toast.makeText(getApplicationContext(), "Stop playing the recording...",Toast.LENGTH_SHORT).show();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (requestCode == REQUEST_Camera_ACTIVITY_CODE) 
		{
			if (resultCode == RESULT_OK) 
			{
				imageCount++;
				Toast.makeText(getApplicationContext(), "Picture is  taken", Toast.LENGTH_SHORT);
				tempStoreImageName.add(ImageName);
				RefreshSaveImage();
				//getfile(ImageName);
				//	Log.e("File Name on done camera", ImageName + "");
			}
			else if (resultCode == RESULT_CANCELED) 
			{
				
				RefreshImage();
			}

		}

		if(requestCode==RECORD_AUDIO_REQUEST_CODE)
		{
			if (resultCode ==RESULT_OK) 
			{
				straudioFile=data.getExtras().getString(ParameterUtil.AudioFile);
				textRecordingPoint.setText("Audio available");
				
				Toast.makeText(getApplicationContext(), "Audio Recorded "+straudioFile,Toast.LENGTH_LONG).show();
				stopPlay();
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	private void RefreshSaveImage() {
		try {
			listImg.clear();
			Bitmap mutableBitmap = null;
			for (int i = 0; i < tempStoreImageName.size(); i++) {
				mutableBitmap = null;
				ImgDto dto = new ImgDto();
				try {
					LoggerUtil.e("Image Name in refresh",
							tempStoreImageName.get(i));
					mutableBitmap = decodeSmallUri(Uri.fromFile(CommonFunction
							.getFileLocation(HR_TodayServiceInvoice.this,
									tempStoreImageName.get(i))));
					if(i == tempStoreImageName.size()-1)
					{
					mutableBitmap = mutableBitmap.copy(Bitmap.Config.ARGB_8888,
							true);
					mutableBitmap =  rotateImage(mutableBitmap, 90);
					Canvas cs = new Canvas(mutableBitmap);
					Paint tPaint = new Paint();
					tPaint.setTextSize(35);
					//#B43104
					tPaint.setColor(getResources().getColor(R.color.red_orange));
					tPaint.setStyle(Style.FILL);
					float height = tPaint.measureText("xX");
					cs.drawText(CommonFunction.getCurrentDateTimeForImage(),height+300f,height+1200f, tPaint);	 //Bitmap bitmap = scaleBitmap(mutableBitmap, 100, 100);
					}									
					dto.setImageBitMap(mutableBitmap);

					//get file
					File imagefile = CommonFunction.getFileLocation(
							HR_TodayServiceInvoice.this, tempStoreImageName.get(i));
		            imagefile.delete();
					CommonFunction.saveBitmap(mutableBitmap,
							HR_TodayServiceInvoice.this, tempStoreImageName.get(i));
                    
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if (!tempImagList.contains(tempStoreImageName.get(i))) {

					if (mutableBitmap != null) {

						// CompressImage(bmp);
					}
				}

				tempImagList.add(tempStoreImageName.get(i));
				listImg.add(dto);
			}
			// LoggerUtil.e("List Size", listImg.size() + "");

			if (listImg.size() == 0) {
				ImgDto dto = new ImgDto();
				dto.setImagedrawable(R.drawable.noimage);
				listImg.add(dto);
			} else {

			}
			adapter = new GalleryImageAdapter(getApplicationContext(), listImg);
			imageListView.setAdapter(adapter);
			// adapter.notifyDataSetChanged(); // TODO Auto-generated method
			// stub
		} catch (Exception e) {
			LoggerUtil.e("Exception in Refresh Image", e + "");
		}
	}
		public static Bitmap rotateImage(Bitmap source, float angle) {
		    Bitmap retVal;

		    Matrix matrix = new Matrix();
		    matrix.postRotate(angle);
		    retVal = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);

		    return retVal;
		}


	private void RefreshImage()
	{
		try
		{ 
			listImg.clear();
			Bitmap bmp = null;
			for (int i = 0; i < tempStoreImageName.size(); i++) {
				bmp = null;
				ImgDto dto = new ImgDto();
				try {
					//	Log.e("Image Name in refresh",tempStoreImageName.get(i));
					//	bmp = BitmapFactory.decodeStream(getContentResolver()
						//	.openInputStream(Uri.fromFile(CommanFunction.getFileLocation(IWitnessActivity.this,tempStoreImageName.get(i)))));
					     
					bmp = BitmapFactory.decodeFile(CommonFunction
						.getFileLocation(getApplicationContext(),
									tempStoreImageName.get(i)).toString());

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//Bitmap bitmap = scaleBitmap(bmp, 100, 100);
				dto.setImageBitMap(bmp);
				if (!tempImagList.contains(tempStoreImageName.get(i))) {

					if (bmp != null) {

						//CompressImage(bmp);
					}
				}// dto.setImageUri(Uri.fromFile(CommanFunction.getFileLocation(IWitnessActivity.this,
				// tempStoreImageName.get(i))));

				tempImagList.add(tempStoreImageName.get(i));
				listImg.add(dto);


			}

			//Log.e("List Size", listImg.size() + "");

			if (listImg.size() == 0) {
				ImgDto dto = new ImgDto();
				dto.setImagedrawable(R.drawable.noimage);
				listImg.add(dto);
			}else
			{

			}
			adapter=new GalleryImageAdapter(getApplicationContext(), listImg) ;
			imageListView.setAdapter(adapter);
			//adapter.notifyDataSetChanged(); // TODO Auto-generated method stub
		}catch (Exception e) {
			Log.e("Exception in Refresh Image", e+"");
		}
	}


	private Bitmap decodeSmallUri(Uri selectedImage) throws FileNotFoundException 
	{
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(getApplicationContext().getContentResolver().openInputStream(selectedImage), null, o);

		final int REQUIRED_SIZE = 500;

		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeStream(getApplicationContext().getContentResolver().openInputStream(selectedImage), null, o2);
	}




	public void getfile(String fileName)
	{
		try
		{
			//	Log.e("Get File Called", fileName);

			File imagefile=CommonFunction.getFileLocation(getApplicationContext(), fileName);
			if(imagefile.exists())
			{
				//Bitmap myBitmap = BitmapFactory.decodeFile(imagefile.getAbsolutePath());

				Bitmap myBitmap=decodeUri(Uri.fromFile(CommonFunction.getFileLocation(getApplicationContext(),fileName)));
				//Log.e("Input Bitmap width and heigth  ", myBitmap.getWidth()+" "+myBitmap.getWidth());
				//	edtDescription.setText(fileName+" "+myBitmap.getWidth()+" and "+myBitmap.getHeight());
				Bitmap output = Bitmap.createScaledBitmap(myBitmap, 800, 800, true);
				//	Log.e("Out Put Bitmap width and heigth  ", output.getWidth()+" "+output.getWidth());
				imagefile.delete();
				CommonFunction.saveBitmap(output, getApplicationContext(), fileName);
			}}catch(Exception e)
			{

			}
	}


	private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException 
	{
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(getApplicationContext().getContentResolver().openInputStream(selectedImage), null, o);

		final int REQUIRED_SIZE = 800;

		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeStream(getApplicationContext().getContentResolver().openInputStream(selectedImage), null, o2);
	}
	public static Bitmap scaleBitmap(Bitmap bitmapToScale, float newWidth,float newHeight) 
	{
		if (bitmapToScale == null)
			return null;
		// get the original width and height
		int width = bitmapToScale.getWidth();
		int height = bitmapToScale.getHeight();
		// create a matrix for the manipulation
		Matrix matrix = new Matrix();

		// resize the bit map
		matrix.postScale(newWidth / width, newHeight / height);

		// recreate the new Bitmap and set it back
		return Bitmap.createBitmap(bitmapToScale, 0, 0, bitmapToScale
				.getWidth(), bitmapToScale.getHeight(), matrix, true);
	}


	class OnGalleryClick implements OnItemLongClickListener 
	{

		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
				int position, long arg3) 
		{
			if(generalInfoDTO.getServiceStatus().equals("InComplete"))
			{
				//Toast.makeText(getApplicationContext(),IWitnessActivity.position+ "  "+position, Toast.LENGTH_LONG).show();
				if(listImg.size()>0)
				{
					listImg.remove(position);
					if(tempImagList.size()>0)
					{
						tempImagList.remove(position);

					}
					if(tempStoreImageName.size()>0)
					{
						tempStoreImageName.remove(position);
					}
					RefreshImage();
					//alertboxClearGalleary("Confirmation", "Are you want to remove selected image ?");
				}
			}
			return false;
		}

	}	


	public Bitmap getBimapByName(String ImageName)
	{
		if(ImageName==null || ImageName.equals(""))
		{
			return null;
		}

		Bitmap bmp = null;
		try {
			//Log.e("Image Name in refresh",ImageName);
			File profileImage=CommonFunction.getFileLocation(getApplicationContext(),ImageName);
			if(profileImage.exists())
			{
				bmp=decodeUri(Uri.fromFile(profileImage));
				//bmp = scaleBitmap(bmp, 100, 100);
			}else
			{
				return bmp;
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			//bmp=getResources().getDrawable(R.drawable.noimage);
			e.printStackTrace();
		}

		return bmp;
	}
	//alert dialog
	private void ShowSignatureDailog(String title,int which)
	{
		signatureDialog = new Dialog(HR_TodayServiceInvoice.this);
		signatureDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		signatureDialog.setContentView(R.layout.signature);
		signatureDialog.setCancelable(false);
		mContent = (LinearLayout)signatureDialog. findViewById(R.id.linearLayout);
		InitializeSignature(signatureDialog,which);

		signatureDialog.show();

	}
	public void InitializeSignature(final Dialog signatureDialog,final int which)
	{
		mContent = (LinearLayout)signatureDialog.findViewById(R.id.linearLayout);
		mSignature = new signature(this, null);
		mSignature.setBackgroundColor(Color.WHITE);
		mContent.addView(mSignature, LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		mClear = (Button)signatureDialog.findViewById(R.id.clear);
		mGetSign = (Button)signatureDialog.findViewById(R.id.getsign);
		mGetSign.setEnabled(false);
		mCancel = (Button)signatureDialog.findViewById(R.id.cancel);
		mView = mContent;
		yourName = (TextView) signatureDialog.findViewById(R.id.yourName);

		switch (which)
		{
		case 1:
			yourName.setText(generalInfoDTO.getFirst_Name()+" "+generalInfoDTO.getLast_Name());
			break;
		case 2:
			yourName.setText(generalInfoDTO.getFirst_Name()+" "+generalInfoDTO.getLast_Name());

			break;
		default:
			break;
		}

		mClear.setOnClickListener(new OnClickListener() 
		{        
			public void onClick(View v) 
			{
				//Log.v("log_tag", "Panel Cleared");
				mSignature.clear();
				issignature=false;
				mGetSign.setEnabled(false);
			}
		});
		mGetSign.setOnClickListener(new OnClickListener() 
		{        
			public void onClick(View v) 
			{
				//Log.v("log_tag", "Panel Saved");
				boolean error = captureSignature();
				if(!error)
				{
					String name=yourName.getText().toString();
					//int auditid=MainActivity.auditDto.getId();
					//	Log.e("audit id", auditid+"");
					switch (which)
					{
					case 1:

						current =serviceID_new+".jpg";
						//Log.e("iMAGE name", current+"");
						cussignName=current;
						mypath= CommonFunction.getFileLocation(getApplicationContext(), current);
						mView.setDrawingCacheEnabled(true);
						mSignature.save(mView);


						strTechnicianSignature=cussignName;
						Bitmap technicianSignBitmap=getBimapByName(cussignName);
						imageViewTechnicianSign.setImageBitmap(technicianSignBitmap);


						break;
					case 2:
						current =serviceID_new+"_sales.jpg";
						//Log.e("iMAGE name", current+"");
						cussignName=current;
						mypath= CommonFunction.getFileLocation(getApplicationContext(), current);
						mView.setDrawingCacheEnabled(true);
						mSignature.save(mView);

						strCustomerSignature=cussignName;
						residential_pest_DTO.setSignature(cussignName);
						Bitmap customerSignBitmap=	getBimapByName(cussignName);
						imageViewCustomerSign.setImageBitmap(customerSignBitmap);

						break;
					default:
						break;
					}
					signatureDialog.dismiss();
				}
			}
		});
		mCancel.setOnClickListener(new OnClickListener() 
		{        
			public void onClick(View v) 
			{
				signatureDialog.dismiss();
			}
		});
	}
	public class signature extends View 
	{
		private static final float STROKE_WIDTH = 5f;
		private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
		private Paint paint = new Paint();
		private Path path = new Path();

		private float lastTouchX;
		private float lastTouchY;
		private final RectF dirtyRect = new RectF();

		public signature(Context context, AttributeSet attrs) 
		{
			super(context, attrs);
			paint.setAntiAlias(true);
			paint.setColor(Color.BLACK);
			paint.setStyle(Paint.Style.STROKE);
			paint.setStrokeJoin(Paint.Join.ROUND);
			paint.setStrokeWidth(STROKE_WIDTH);
		}

		public void save(View v) 
		{
			//Log.v("log_tag", "Width: " + v.getWidth());
			//Log.v("log_tag", "Height: " + v.getHeight());
			if(mBitmap == null)
			{
				mBitmap =  Bitmap.createBitmap (mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);;
			}
			Canvas canvas = new Canvas(mBitmap);
			try
			{
				FileOutputStream mFileOutStream = new FileOutputStream(mypath);

				v.draw(canvas); 
				mBitmap.compress(Bitmap.CompressFormat.JPEG, 90, mFileOutStream); 
				mFileOutStream.flush();
				mFileOutStream.close();
				//String url = Images.Media.insertImage(getContentResolver(), mBitmap, "title", null);
				//Log.e("log_tag","url: " + url);
				//In case you want to delete the file
				//boolean deleted = mypath.delete();
				//Log.v("log_tag","deleted: " + mypath.toString() + deleted);
				//If you want to convert the image to string use base64 converter

			}
			catch(Exception e) 
			{ 
				Log.v("log_tag", e.toString()); 
			} 
		}
		public void clear() 
		{
			path.reset();
			invalidate();
		}

		@Override
		protected void onDraw(Canvas canvas) 
		{
			canvas.drawPath(path, paint);
		}

		@Override
		public boolean onTouchEvent(MotionEvent event) 
		{
			float eventX = event.getX();
			float eventY = event.getY();
			mGetSign.setEnabled(true);
			issignature=true;
			switch (event.getAction()) 
			{
			case MotionEvent.ACTION_DOWN:
				path.moveTo(eventX, eventY);
				lastTouchX = eventX;
				lastTouchY = eventY;
				return true;

			case MotionEvent.ACTION_MOVE:

			case MotionEvent.ACTION_UP:

				resetDirtyRect(eventX, eventY);
				int historySize = event.getHistorySize();
				for (int i = 0; i < historySize; i++) 
				{
					float historicalX = event.getHistoricalX(i);
					float historicalY = event.getHistoricalY(i);
					expandDirtyRect(historicalX, historicalY);
					path.lineTo(historicalX, historicalY);
				}
				path.lineTo(eventX, eventY);
				break;

			default:
				debug("Ignored touch event: " + event.toString());
				return false;
			}

			invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
					(int) (dirtyRect.top - HALF_STROKE_WIDTH),
					(int) (dirtyRect.right + HALF_STROKE_WIDTH),
					(int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

			lastTouchX = eventX;
			lastTouchY = eventY;

			return true;
		}

		private void debug(String string){
		}

		private void expandDirtyRect(float historicalX, float historicalY) 
		{
			if (historicalX < dirtyRect.left) 
			{
				dirtyRect.left = historicalX;
			} 
			else if (historicalX > dirtyRect.right) 
			{
				dirtyRect.right = historicalX;
			}

			if (historicalY < dirtyRect.top) 
			{
				dirtyRect.top = historicalY;
			} 
			else if (historicalY > dirtyRect.bottom) 
			{
				dirtyRect.bottom = historicalY;
			}
		}

		private void resetDirtyRect(float eventX, float eventY) 
		{
			dirtyRect.left = Math.min(lastTouchX, eventX);
			dirtyRect.right = Math.max(lastTouchX, eventX);
			dirtyRect.top = Math.min(lastTouchY, eventY);
			dirtyRect.bottom = Math.max(lastTouchY, eventY);
		}
	}



	private boolean captureSignature()
	{

		boolean error = false;
		String errorMessage = "";


		if(yourName.getText().toString().equalsIgnoreCase("")){
			errorMessage = errorMessage + "Enter your Name\n";
			error = true;
		}   

		if(error){
			Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.TOP, 105, 50);
			toast.show();
		}

		return error;
	}
	 EmailAdapter myAdapter;
	private void showSendEmailDailog()
	{
		Log.e("reloaded", "reloaded");
		Button btnSendMail,btnadd,btncancel;
		TextView txtFrom,txtmessage;
		final EditText edtEmailAddress;
		final	ListView lstemailIdList;
		sendEmailDialog = new Dialog(HR_TodayServiceInvoice.this);
		sendEmailDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		sendEmailDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
		sendEmailDialog.setContentView(R.layout.send_email_dialog);
		//InitializeSignature(sendEmailDialog,which);
		btnSendMail=(Button)sendEmailDialog.findViewById(R.id.btnSendEmail);
		btnadd=(Button)sendEmailDialog.findViewById(R.id.btnAdd);
		btncancel=(Button)sendEmailDialog.findViewById(R.id.btnCancel);
		txtFrom=(TextView)sendEmailDialog.findViewById(R.id.txtFrom);
		txtmessage=(TextView)sendEmailDialog.findViewById(R.id.txtMessage);
		edtEmailAddress=(EditText)sendEmailDialog.findViewById(R.id.edtemailAddress);
		edtEmailAddress.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) 
			{
				// TODO Auto-generated method stub
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) 
			{
				// TODO Auto-generated method stub
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) 
			{
				// TODO Auto-generated method stub
				String str = s.toString();
	            if(str.length() > 0 && str.startsWith(" ")){
	               // Log.v("","Cannot begin with space");
	                edtEmailAddress.setText("");
	            }else{
	                //Log.v("","Doesn't contain space, good to go!");
	            }
			}
     
	    });
		
		lstemailIdList=(ListView)sendEmailDialog.findViewById(R.id.lstEmailList);
		txtFrom.setText(userDTO.getEmailid());
		
		houstonFlowFunctions.deleteSelectedTableData(DatabaseHelper.TABLE_AGREEMENTMAILID,ParameterUtil.Member_Id,serviceID_new);
		String[] emailIDS=	generalInfoDTO.getNewEmail().split(",");
		for(int i=0;i<emailIDS.length;i++)
		{
			if(CommonFunction.checkString(emailIDS[i], "").equals(""))
			{
				addEmailAddress(userDTO.getEmailid(),true);
				addEmailAddress("mobile2@goanteater.com",true);
			}
			else
			{
				addEmailAddress(userDTO.getEmailid(),true);
				addEmailAddress(emailIDS[i],true);
				addEmailAddress("mobile2@goanteater.com",true);
			}
		}
		if(emailIDS.length==0)
		{
			addEmailAddress("noemail@goanteater.com",true);
		}
		//JSONArray emailJSONArray=  serviceFlowFunctions.getAllSelectedServiceEmaiLID(serviceID_new);
		ArrayList<AgreementMailIdDTO> emailAddressList = new Gson().fromJson(houstonFlowFunctions.getAllSelectedServiceEmaiLID(serviceID_new).toString(), new TypeToken<List<AgreementMailIdDTO>>(){}.getType());
		myAdapter = new EmailAdapter(HR_TodayServiceInvoice.this, R.layout.row_team_layout, emailAddressList ,lstemailIdList);
		//lstemailIdList.getChildAt(0).setEnabled(false);
		lstemailIdList.setAdapter(myAdapter);
		lstemailIdList.setItemsCanFocus(false);
    
		btnSendMail.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				SharedPreference
				.setSharedPrefer(getApplicationContext(),
						SharedPreference.COMPLETED_BY,
						ParameterUtil.Residential);
				btnsubmitToPestpac.setEnabled(true);
				ArrayList<AgreementMailIdDTO> selectedTeams = new ArrayList<AgreementMailIdDTO>();
				final SparseBooleanArray checkedItems = lstemailIdList.getCheckedItemPositions();
			    //Log.e("checkedItemsCount ",""+checkedItems.size());

				int checkedItemsCount = checkedItems.size();
				
				for (int i = 0; i < checkedItemsCount; ++i) {
					// Item position in adapter
					int position = checkedItems.keyAt(i);
					// Add team if item is checked == TRUE!
					if(checkedItems.valueAt(i))
					{
						//Log.e("checkedItemsCount ",""+checkedItems.size()+" "+position);
						selectedTeams.add(myAdapter.getItem(position));	
						
					}
				}
			//	Log.e("selectedTeams ",""+selectedTeams.size());
				if(selectedTeams.size() < 1)
				{
					/*--------Commented By Ankit on 16-April-2015 based on client comment------*/
					//CommonFunction.AboutBox("Need to select one or more emailId.",TodayServiceReportActivity.this);
					//houstonFlowFunctions.deleteSelectedTableData(DatabaseHelper.TABLE_AGREEMENTMAILID,ParameterUtil.Member_Id,serviceID_new);
					//submitService();
					//goToHome("Yes");
					Toast.makeText(getBaseContext(), "Need to select one or more Items.", Toast.LENGTH_SHORT).show();
				}
				else
				{
					// Just logging the output.
					houstonFlowFunctions.deleteSelectedTableData(DatabaseHelper.TABLE_AGREEMENTMAILID,ParameterUtil.Member_Id,serviceID_new);

					for(AgreementMailIdDTO t : selectedTeams)
					{
						addEmailAddress(t.getEmailId(),true);	
					
					}

					submitService();
					goToHome("Yes");
					sendEmailDialog.dismiss();
				}
				// TODO Auto-generated method stub

				//goToHome("Yes");
			}

		});

		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v)
			{
				btnsubmitToPestpac.setEnabled(true);
				//serviceFlowFunctions.deleteSelectedTableData(DatabaseHelper.TABLE_AGREEMENTMAILID,ParameterUtil.Member_Id,serviceID_new);
				sendEmailDialog.dismiss();	
			}
		});

		btnadd.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				String emailAddress=edtEmailAddress.getText().toString().trim();
				if(eMailValidation(emailAddress))
				{
					addEmailAddress(emailAddress,true);
					setEmailAddressList(lstemailIdList);
					edtEmailAddress.setText("");
				}
				else
				{
					CommonFunction.AboutBox("Invalid Email ID",HR_TodayServiceInvoice.this);
				}
				
			    Context context=HR_TodayServiceInvoice.this;
				InputMethodManager imm = (InputMethodManager)getSystemService(context.INPUT_METHOD_SERVICE);
		        imm.hideSoftInputFromWindow(edtEmailAddress.getWindowToken(), 0);	
			}
		});
		sendEmailDialog.show();
		sendEmailDialog.setCancelable(false);
	}

	public void goToHome(String isSync)
	{
		Intent intent=new Intent(getApplicationContext(), ServiceListAcitivity.class);
		intent.putExtra("userDTO", ServiceListAcitivity.userDTO);

		Bundle extras = new Bundle();
		extras.putString(ParameterUtil.isSync, isSync);

		// 4. add bundle to intent
		intent.putExtras(extras);
		//intent.putExtra(ParameterUtil.isSync,isSync);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
	}
	public  boolean eMailValidation(String emailstring)
	{
		/*Pattern emailPattern = Pattern.compile(".+@.+\\.[a-z]+");
		Matcher emailMatcher = emailPattern.matcher(emailstring);
		return emailMatcher.matches();*/
		//Pattern emailPattern = Pattern.compile(".+@.+\\.[a-z]+");
		String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern emailPattern = Pattern.compile(EMAIL_PATTERN);
		Matcher emailMatcher = emailPattern.matcher(emailstring);
		return emailMatcher.matches();
		
	}

	public EmailAdapter setEmailAddressList(final ListView lstEmailAdress)
	{
		JSONArray emailJSONArray=  houstonFlowFunctions.getAllSelectedServiceEmaiLID(serviceID_new);
		ArrayList<AgreementMailIdDTO> emailAddressList = new Gson().fromJson(houstonFlowFunctions.getAllSelectedServiceEmaiLID(serviceID_new).toString(), new TypeToken<List<AgreementMailIdDTO>>(){}.getType());
		myAdapter = new EmailAdapter(this, R.layout.row_team_layout, emailAddressList ,lstEmailAdress);
		lstEmailAdress.setAdapter(myAdapter);
		lstEmailAdress.setItemsCanFocus(false);
		return myAdapter;
	}
	public void addEmailAddress(String emailAddress,boolean isSelected)
	{
		//Log.e("emailAddress..",isSelected+"emailAddress..."+emailAddress);
		try
		{
			if(!houstonFlowFunctions.isEmailAdressAlreadyAdded(serviceID_new, emailAddress))
			{
				//Log.e("emailAddress..",isSelected+"emailAddress..if.."+emailAddress);
				AgreementMailIdDTO agreementMailIdDTO=new AgreementMailIdDTO();
				agreementMailIdDTO.setAgreementId("0");
				//agreementMailId.setCreate_By(String.valueOf(GeneralInformationActivity.userDTO.getEmployeeId()));
				agreementMailIdDTO.setCreate_By(String.valueOf(userDTO.getPestPackId()));
				agreementMailIdDTO.setCreate_date(CommonFunction.getCurrentDateTime());
				agreementMailIdDTO.setEmailId(emailAddress);
				agreementMailIdDTO.setMailType("Residential");
				agreementMailIdDTO.setId(houstonFlowFunctions.getMaxId()+1);
				agreementMailIdDTO.setMember_Id(serviceID_new);
				agreementMailIdDTO.setSubject("");
				agreementMailIdDTO.setSelected(isSelected);
				try
				{
					Gson gson=new Gson();
					JSONObject agreementMailJSON =new JSONObject(gson.toJson(agreementMailIdDTO));
					//Log.e("agreementMailJSON", agreementMailJSON.toString());
					long i=	houstonFlowFunctions.agreementMail(agreementMailJSON);
					//Log.e("addEmailAddress",i+"");
				}catch(Exception e)
				{
					LoggerUtil.e("Exception in AgreementMailId", e.toString());

				}
			}else
			{    
				//Log.e("inside alert addEmailAddress",i+"");
				//Toast.makeText(getApplicationContext(), "Email Address Already Added",Toast.LENGTH_LONG).show();
				CommonFunction.AboutBox("Email Address Already Added",HR_TodayServiceInvoice.this);
			}
		} 
		catch (Exception e) 
		{
			Toast.makeText(getApplicationContext(), "Invalid Email Address", Toast.LENGTH_LONG).show();
			//Log.e("EMAIL EXCEPTION",e+"");
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.today_service_invoice, menu);
		return true;
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		// TODO Auto-generated method stub
		int id = item.getItemId();
		if (id == R.id.action_home)
		{
			goToHome("No");
			return true;
		}
		/*if (id == R.id.action_back) {
			finish();
			return true;
		}*/
		if(id==android.R.id.home)
		{
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
	public void insertUpdateLog() 
	{
		Gson gson=new Gson();
		// TODO Auto-generated method stub
		JSONArray logArray=houstonFlowFunctions.getAllSelectedLogRow(serviceID_new);
		//Log.e("", "logArray...."+logArray);
		logDTO.setAgreeMentMailID("True");
		String logString =	gson.toJson(logDTO);
		try
		{
			if(logArray.length()>0)
			{
				int u =	HoustonFlowFunctions.updateLog(logDTO);
				LoggerUtil.e("log updated. values", ""+u);
			}
			else
			{
				long a = HoustonFlowFunctions.addLog(new JSONObject(logString));
				LoggerUtil.e("log added1. values", ""+a);
			}
		}
		catch(Exception e)
		{

		}
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onBackPressed()
	 */
	/*@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent i = new Intent(TodayServiceInvoice.this,ResidentialPestControlServiceReport.class);
		startActivity(i);
	}*/

}
