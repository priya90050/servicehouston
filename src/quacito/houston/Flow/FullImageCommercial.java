package quacito.houston.Flow;

import quacito.houston.Flow.servicehouston.R;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Window;
import android.widget.ImageView;

public class  FullImageCommercial extends Activity
{
	private ImageView imageview;
	Bitmap bmp;
	String from;
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_fullscreen_view);
		from=getIntent().getExtras().getString("from");
		imageview = (ImageView) findViewById(R.id.imglarge);
		if(from.equals("general"))
		{
			Bitmap bitmap = GeneralInformationActivity.bitmap;
			if(bitmap!=null)
			{
				imageview.setImageBitmap(bitmap);
			}
			else
			{

			}
		}
		if(from.equals("frontCheck"))
		{
			Bitmap bitmapFront = HC_PestControlServiceReport.bitmapFront;
			if(bitmapFront!=null)
			{
				imageview.setImageBitmap(bitmapFront);
			}
			else
			{

			}
		}
		if(from.equals("backCheck"))
		{
			Bitmap bitmapBack = HC_PestControlServiceReport.bitmapBack;
			if(bitmapBack!=null)
			{
				imageview.setImageBitmap(bitmapBack);
			}
			else
			{

			}
		}

		if(from.equals("report"))
		{
			Bitmap bitmap = HC_TodayServiceInvoice.bitmap;
			if(bitmap!=null)
			{
				imageview.setImageBitmap(bitmap);
			}else
			{


			}

			//(Bitmap) getIntent().getParcelableExtra("BitmapImage");


		}

	}
}
