package quacito.houston.Flow;


import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import quacito.houston.CommonUtilities.CommonFunction;
import quacito.houston.CommonUtilities.LoggerUtil;
import quacito.houston.CommonUtilities.ParameterUtil;
import quacito.houston.DBhelper.DatabaseHelper;
import quacito.houston.DBhelper.HoustonFlowFunctions;
import quacito.houston.Flow.HC_TodayServiceInvoice;
import quacito.houston.Flow.HR_PestControlServiceReport;
import quacito.houston.Flow.HR_TodayServiceInvoice;
import quacito.houston.Flow.servicehouston.R;
import quacito.houston.model.ChemicalDto;
import quacito.houston.model.GeneralInfoDTO;
import quacito.houston.model.UserDTO;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class HC_ChemicalPreview extends Activity {

	HoustonFlowFunctions houstonFlowFunctions;
	ArrayList<String> listSelectedChemicals=new ArrayList<String>();
	ArrayList<ChemicalDto> listChemicals=new ArrayList<ChemicalDto>();
	LinearLayout layoutChemicalHeader,mainLayout;
	Button btnCountinueChemical,btnCancelChemical;
	TextView textViewNoChemicals,txtAccountNumber,txtOrderNumber;
	String serviceID_new,strInsidepestControlServices,strOutsidepestControlServices;
	GeneralInfoDTO generalInfoDTO;
	UserDTO userDTO;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_hr__chemical__preview);
		
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setDisplayShowHomeEnabled(true);
		
		initialize();
		setChemicalTable();
	}
	private void initialize() {
		// TODO Auto-generated method stub
		houstonFlowFunctions = new HoustonFlowFunctions(HC_ChemicalPreview.this);
		
		serviceID_new = getIntent().getExtras().getString(ParameterUtil.ServiceID_new);
		generalInfoDTO = (GeneralInfoDTO)this.getIntent().getExtras().getSerializable("generalInfoDTO");
		userDTO  = (UserDTO)this.getIntent().getExtras().getSerializable("userDTO");
		strInsidepestControlServices = getIntent().getExtras().getString("strInsideService");
		strOutsidepestControlServices = getIntent().getExtras().getString("strOutsideService");

		layoutChemicalHeader=(LinearLayout)findViewById(R.id.layoutChemicalHeader);
		mainLayout=(LinearLayout)findViewById(R.id.mainLayout);
		textViewNoChemicals = (TextView)findViewById(R.id.textViewNoChemicals);
		
		btnCountinueChemical = (Button)findViewById(R.id.btnCountinueChemical);
		btnCountinueChemical.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i= new Intent(HC_ChemicalPreview.this,HC_TodayServiceInvoice.class);
				i.putExtra("userDTO",userDTO);
				i.putExtra("generalInfoDTO", generalInfoDTO);
				i.putExtra("strOutsideService",strOutsidepestControlServices);
				i.putExtra("strInsideService", strInsidepestControlServices);
				i.putExtra(ParameterUtil.ServiceID_new, generalInfoDTO.getServiceID_new());
				startActivity(i);
			}
		});
		btnCancelChemical = (Button)findViewById(R.id.btnCancelChemical);
		btnCancelChemical.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		txtAccountNumber=(TextView)findViewById(R.id.txtAccountNumber);
		txtAccountNumber.setText(generalInfoDTO.getAccountNo());
		
		txtOrderNumber=(TextView)findViewById(R.id.txtOrderNumber);
		txtOrderNumber.setText(generalInfoDTO.getOrder_Number());
	}
	private void setChemicalTable() 
	{
		
		JSONArray jsChemical = houstonFlowFunctions.getAllSelectedChemicals(DatabaseHelper.TABLE_SELECTED_CHEMICALS,serviceID_new,"Commercial");
		listChemicals.clear();
		listChemicals=new Gson().fromJson(jsChemical.toString(), new TypeToken<List<ChemicalDto>>(){}.getType());
		
		if(listChemicals.size()==0)
		{
			 textViewNoChemicals.setVisibility(View.VISIBLE);
			//layoutChemicalHeader.setVisibility(View.GONE);
		}
		else
		{	
		for(int i=0;i<listChemicals.size();i++)
		{
			 textViewNoChemicals.setVisibility(View.GONE);
			AddChemicals(i);
		}
		}
	}
	int i=0;
	TableLayout tb;
	TableRow deleteTableRow;
	int numberofaudit;
	TableRow tr;
	ArrayList<TableRow> trlist=new ArrayList<TableRow>();

	private void AddChemicals(final int listindex) 
	{
		// TODO Auto-generated method stub
		numberofaudit++;
		LoggerUtil.e("serviceID_new", serviceID_new);
		ChemicalDto dto=listChemicals.get(listindex);
		if(i==0)
		{
			tb=new TableLayout(HC_ChemicalPreview.this);
			//addHeader();
			mainLayout.addView(tb);
		}
		i=i+1;
		tr = new TableRow(getApplicationContext());
		trlist.add(tr);
				
		if(i%2==0)

		tr.setBackgroundColor(Color.parseColor("#CAC9C9"));
		tr.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.FILL_PARENT));
		tr.setPadding(0, 10, 0, 10);

		LayoutParams params2 = new TableRow.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT,1.0f);
		
		LayoutParams paramsProd = new TableRow.LayoutParams(70, LayoutParams.FILL_PARENT,1.0f);
		
		TextView product = new TextView(getApplicationContext());
		product.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
		product.setTextColor(Color.BLACK);
		product.setLayoutParams(paramsProd);

		if(listChemicals.get(listindex).getProduct().equals("Other") && !CommonFunction.checkString(listChemicals.get(listindex).getOtherText(), "").equals(""))
		{
			LoggerUtil.e("OTHER", ""+listChemicals.get(listindex).getOtherText());
			product.setText(listChemicals.get(listindex).getOtherText());
		}
		else
		{
			product.setText(listChemicals.get(listindex).getProduct());
		}
		tr.addView(product);

		TextView percent = new TextView(getApplicationContext());
		percent.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
		percent.setText(listChemicals.get(listindex).getPercentPes());
		percent.setLayoutParams(params2);
		percent.setTextColor(Color.BLACK);
		tr.addView(percent);

		TextView AMT = new TextView(getApplicationContext());
		AMT.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
		AMT.setText(listChemicals.get(listindex).getAmount());
		//AMT.setPadding(0, 0,5, 0);
		AMT.setLayoutParams(params2);
		AMT.setTextColor(Color.BLACK);
		tr.addView(AMT);

		TextView UNIT = new TextView(getApplicationContext());
		UNIT.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
		UNIT.setText(listChemicals.get(listindex).getUnit());
		//UNIT.setPadding(0, 0,5, 0);
		UNIT.setLayoutParams(params2);
		UNIT.setTextColor(Color.BLACK);
		tr.addView(UNIT);

		LayoutParams paramstarget = new TableRow.LayoutParams(50, LayoutParams.FILL_PARENT,1.0f);

		TextView TARGET = new TextView(getApplicationContext());
		TARGET.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
		TARGET.setText(listChemicals.get(listindex).getTarget());
		//TARGET.setPadding(0, 0,5, 0);
    	TARGET.setLayoutParams(paramstarget);
		TARGET.setTextColor(Color.BLACK);
		tr.addView(TARGET);
		tb.addView(tr, new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
	}
	public void addHeader()
	{
		TableRow tr = new TableRow(getApplicationContext());
		if(i%2!=0)

			tr.setBackgroundColor(Color.parseColor("#2F80B7"));
		//tr.setOrientation(LinearLayout.HORIZONTAL);
		tr.setBackgroundColor(Color.parseColor("#2F80B7"));
		tr.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.FILL_PARENT));
		//tr.setWeightSum(1);
		tr.setPadding(0, 10, 0, 10);

		//LayoutParams params0 = new TableRow.LayoutParams(android.widget.LinearLayout.LayoutParams.WRAP_CONTENT, LayoutParams.FILL_PARENT);

		TextView product = new TextView(getApplicationContext());
		product.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
		product.setText(getApplicationContext().getResources().getString(R.string.PRODUCT));
		product.setTextColor(Color.WHITE);
		LayoutParams params1 = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.FILL_PARENT,1f);
		product.setLayoutParams(params1);
		tr.addView(product);
		LayoutParams params2 = new TableRow.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT,.3f);

		TextView percent = new TextView(getApplicationContext());
		percent.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
		percent.setText(getApplicationContext().getResources().getString(R.string.percent));
		percent.setLayoutParams(params2);
		percent.setTextColor(Color.WHITE);
		tr.addView(percent);

		TextView AMT = new TextView(getApplicationContext());
		AMT.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
		AMT.setText(getApplicationContext().getResources().getString(R.string.AMT));
		AMT.setPadding(0, 0,5, 0);
		AMT.setLayoutParams(params2);
		AMT.setTextColor(Color.WHITE);
		tr.addView(AMT);

		TextView UNIT = new TextView(getApplicationContext());
		UNIT.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
		UNIT.setText(getApplicationContext().getResources().getString(R.string.UNIT));
		UNIT.setPadding(0, 0,5, 0);
		UNIT.setLayoutParams(params2);
		UNIT.setTextColor(Color.WHITE);
		tr.addView(UNIT);

		TextView TARGET = new TextView(getApplicationContext());
		TARGET.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
		TARGET.setText(getApplicationContext().getResources().getString(R.string.TARGET));
		TARGET.setPadding(0, 0,5, 0);
		TARGET.setLayoutParams(params2);
		TARGET.setTextColor(Color.WHITE);
		tr.addView(TARGET);

		tb.addView(tr, new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.general_information, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		if (id == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
}
