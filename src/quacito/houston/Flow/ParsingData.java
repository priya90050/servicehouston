package quacito.houston.Flow;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import quacito.houston.CommonUtilities.CommonFunction;
import quacito.houston.CommonUtilities.LoggerUtil;
import quacito.houston.CommonUtilities.ParameterUtil;
import quacito.houston.DBhelper.DatabaseHelper;
import quacito.houston.DBhelper.HoustonFlowFunctions;
import quacito.houston.model.ChemicalDto;
import quacito.houston.model.GeneralInfoDTO;
import quacito.houston.model.HResidential_pest_DTO;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import android.widget.MultiAutoCompleteTextView.CommaTokenizer;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class ParsingData
{
	Context context;


	public ParsingData(Context context) {
		super();
		this.context = context;
	}

	public void parseDataPostResponse(String result)
	{
		HoustonFlowFunctions houstonFlowFunctions=new HoustonFlowFunctions(context);
		try
		{   
		

			JSONArray responseArray=new JSONArray(result);
			for(int i=0;i<responseArray.length();i++)
			{
				JSONObject jsResponse=responseArray.getJSONObject(i);
				String Response=jsResponse.getString("Response").trim();
				String From=jsResponse.getString("From").trim();
				String ServiceID_new=jsResponse.getString("ServiceID_new").trim();

				if(From.equals("GeneralSR")&&Response.equals("OK"))
				{
					int delete=	houstonFlowFunctions.deleteSelectedTableData(DatabaseHelper.TABLE_GENERAL_INFO,ParameterUtil.ServiceID_new, ServiceID_new);
					LoggerUtil.e("GeneralSR Delete",String.valueOf(delete));
				}
				if(From.equals("Residetial_Insp")&&Response.equals("OK"))
				{
					int delete=	houstonFlowFunctions.deleteSelectedTableData(DatabaseHelper.TABLE_HR_PEST_INFO,ParameterUtil.ServiceID_new, ServiceID_new);
					LoggerUtil.e("Residetial_Insp Delete",String.valueOf(delete));
				}

				if(From.equals("Chemical")&&Response.equals("OK"))
				{
					int delete	= houstonFlowFunctions.deleteSelectedTableData(DatabaseHelper.TABLE_SELECTED_CHEMICALS,ParameterUtil.ServiceID_new, ServiceID_new);
					LoggerUtil.e("Chemical Delete",String.valueOf(delete));
				}
				houstonFlowFunctions.deleteAllTableData(DatabaseHelper.TABLE_AGREEMENTMAILID);


			}
		}



		catch(Exception e)
		{

		}
	}

	public void parseServiceListGenralInformation(String result)
	{
		ArrayList<GeneralInfoDTO> serviceList=new ArrayList<GeneralInfoDTO>();
		HoustonFlowFunctions houstonFlowFunctions=new HoustonFlowFunctions(context);
		try
		{
			JSONArray jsServiceList=new JSONArray(result);
			serviceList = new Gson().fromJson(jsServiceList.toString(), new TypeToken<List<GeneralInfoDTO>>(){}.getType());

			if(serviceList.size()>0)
			{
				int i=	houstonFlowFunctions.deleteAllTableData(DatabaseHelper.TABLE_GENERAL_INFO);
			}

			try
			{
				JSONObject jsobject=jsServiceList.getJSONObject(0);
				String resultString=jsobject.getString("result");
				if(resultString.trim().equals("No Record Found"))
				{
					Log.e("No record",resultString);
					return;
				}

			}catch(Exception e)
			{
				Log.e("Exception", e.toString());

			}

			jsServiceList=new JSONArray(new Gson().toJson(serviceList));
			houstonFlowFunctions.insertMultipleRecords(jsServiceList, DatabaseHelper.TABLE_GENERAL_INFO);

			//Download Before And After Images And Audio File...	

			for(int i=0;i<serviceList.size();i++)
			{
				downloadImageByName(serviceList.get(i).getBeforeImage(),CommonFunction.DOWNLOAD_IMAGE_URL);
				downloadImageByName(serviceList.get(i).getAfterImage(),CommonFunction.DOWNLOAD_IMAGE_URL);
				donwloadAudioByName(serviceList.get(i).getAudioFile());
				downloadImageByName(serviceList.get(i).getCheckFrontImage(),CommonFunction.DOWNLOAD_CHECK_IMAGE_URL);
				downloadImageByName(serviceList.get(i).getCheckBackImage(),CommonFunction.DOWNLOAD_CHECK_IMAGE_URL);

			}
		}
		catch(Exception e)
		{
			LoggerUtil.e("Exception", e.toString());
		}
	}
	public void parseResidentialPestData(String res) {
		// TODO Auto-generated method stub
		
		HoustonFlowFunctions houstonFlowFunctions=new HoustonFlowFunctions(context);
		try
		{
			JSONArray jsServiceDataByServiceNewID=new JSONArray(res);
			if(jsServiceDataByServiceNewID.length()>0)
			{
				houstonFlowFunctions.deleteAllTableData(DatabaseHelper.TABLE_HR_PEST_INFO);
			}
			ArrayList<HResidential_pest_DTO>  serviceDataList = new Gson().fromJson(jsServiceDataByServiceNewID.toString(), new TypeToken<List<HResidential_pest_DTO>>(){}.getType());
			jsServiceDataByServiceNewID=new JSONArray(new Gson().toJson(serviceDataList));
			houstonFlowFunctions.insertMultipleRecords(jsServiceDataByServiceNewID, DatabaseHelper.TABLE_HR_PEST_INFO);
			for(int i=0;i<serviceDataList.size();i++)
			{
				downloadImageByName(serviceDataList.get(i).getServiceSignature(),CommonFunction.DOWNLOAD_IMAGE_URL);
				downloadImageByName(serviceDataList.get(i).getSignature(),CommonFunction.DOWNLOAD_IMAGE_URL);

			}
		}
		catch(Exception e)
		{
			LoggerUtil.e("Exception", e.toString());
		}
	}

	public void donwloadAudioByName(String fileName)
	{
		if(!CommonFunction.checkString(fileName, "").equals(""))
		{
			boolean	isFileExists=isFileExistsByName(fileName);

			if(isFileExists)
			{

			}else
			{
				downloadFile(CommonFunction.DOWNLOAD_IMAGE_URL, fileName);
			}
		}
	}



	public boolean isFileExistsByName(String ImageName)
	{
		if(ImageName==null || ImageName.equals(""))
		{
			return false;
		}


		try {
			Log.e("Image Name in refresh",ImageName);
			File profileImage=CommonFunction.getFileLocation(context,ImageName);
			if(profileImage.exists())
			{
				return true;
				//bmp = scaleBitmap(bmp, 100, 100);
			}else
			{
				return false;
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			//bmp=getResources().getDrawable(R.drawable.noimage);
			e.printStackTrace();
		}

		return false;
	}



	public void downloadImageByName(String imageNames,String url)
	{

		String[] imagesArray=imageNames.split(",");

		for(int b=0;b<imagesArray.length;b++)
		{
			String imageName=imagesArray[b];
			if(!CommonFunction.checkString(imageName, "").equals(""))
			{
				boolean	isFileExists=isFileExistsByName(imageName);

				if(isFileExists)
				{

				}else
				{
					downloadFile(url, imageName);
				}
			}
		}



	}

	public void downloadFile(String fileUrl,String fileName)
	{
		URL myFileUrl = null;
		//	Bitmap bmImg = null;
		try 
		{
			myFileUrl = new URL(fileUrl+""+fileName);
		} catch (MalformedURLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			HttpURLConnection conn = (HttpURLConnection) myFileUrl.openConnection();
			conn.setDoInput(true);
			conn.connect();
			File file = CommonFunction.getFileLocation(context, fileName);
			FileOutputStream fileOutput = new FileOutputStream(file);
			InputStream is = conn.getInputStream();
			//bmImg = BitmapFactory.decodeStream(is);
			byte[] buffer = new byte[1024];
			int bufferLength = 0;
			while ( (bufferLength = is.read(buffer)) > 0 )
			{
				fileOutput.write(buffer, 0, bufferLength);
			}
			//close the output stream when done
			fileOutput.close();
			try
			{/*
				bmImg=getBimapByName(fileName);
				if(bmImg!=null)
				{
					//imageview.setImageBitmap(bmImg);

				}else
				{
					//imageview.setImageResource(R.drawable.noimage);
					//pb.setVisibility(View.GONE);
				}

			 */
			} catch (Exception e)
			{
				Log.e("Exception in download file", e.toString());

			}
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			Log.e("Exception in download file", e.toString());
		}

	}

	private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException
	{
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(
				context.getContentResolver().openInputStream(selectedImage), null, o);

		final int REQUIRED_SIZE = 800;

		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeStream(context.getContentResolver().openInputStream(selectedImage), null, o2);
	}

	public void parsingchemicals(String res) {
		// TODO Auto-generated method stub
		HoustonFlowFunctions houstonFlowFunctions=new HoustonFlowFunctions(context);
		try
		{
			JSONArray jsServiceDataByServiceNewID=new JSONArray(res);
			if(jsServiceDataByServiceNewID.length()>0)
			{
				houstonFlowFunctions.deleteAllTableData(DatabaseHelper.TABLE_CHEMICALS);
			}
			ArrayList<ChemicalDto>  serviceDataList = new Gson().fromJson(jsServiceDataByServiceNewID.toString(), new TypeToken<List<ChemicalDto>>(){}.getType());
			jsServiceDataByServiceNewID=new JSONArray(new Gson().toJson(serviceDataList));
			houstonFlowFunctions.insertMultipleRecords(jsServiceDataByServiceNewID, DatabaseHelper.TABLE_CHEMICALS);

		}
		catch(Exception e)
		{
			LoggerUtil.e("Exception", e.toString());
		}
	}

	
}

