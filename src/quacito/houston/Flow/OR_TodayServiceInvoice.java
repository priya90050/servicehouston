package quacito.houston.Flow;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.json.JSONObject;

import com.google.gson.Gson;
import com.splunk.mint.Mint;

import quacito.houston.CommonUtilities.CommonFunction;
import quacito.houston.CommonUtilities.ParameterUtil;
import quacito.houston.DBhelper.HoustonFlowFunctions;
import quacito.houston.Flow.HR_TodayServiceInvoice.OnButtonClick;
import quacito.houston.Flow.HR_TodayServiceInvoice.OnGalleryClick;
import quacito.houston.Flow.servicehouston.R;
import quacito.houston.adapter.GalleryImageAdapter;
import quacito.houston.horizontallistview.HorizontalListView;
import quacito.houston.model.GeneralInfoDTO;
import quacito.houston.model.HResidential_pest_DTO;
import quacito.houston.model.ImgDto;
import quacito.houston.model.LogDTO;
import quacito.houston.model.OResidential_pest_DTO;
import quacito.houston.model.UserDTO;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;

public class OR_TodayServiceInvoice  extends Activity {
	
	TextView txtAccountNumber,txtOrderNumber,txtTimeInHeader,txtEmail,
	or_txtSERVICE_FOR,or_txtAREAS_INSPECTED,or_txtINSPECTED_TREATED,or_txtAnts,or_txtOtherPest,
	txtOutsideonly,txtFenceLines,txtBackYard,txtFrontYard,txtExteriorperimeterofhouse,
	txtRoofLine,txtGarage,txtAttic,txtInteriorofhouse,txtBasement,txtPestControlHeadInside,
	txtPestControlServiesInside,txtPestControlHeadOutside,txtPestControlServiesOutside,
	text_view_ServicePageHeader,txtVisibleSigns,txtNumberofMonitoringDevices,
	txtVisibleTermiteMonitorDevices,txtTermiteMoniterDevicereplaced,
	txtVisibleTermiteActivity,txtVisibleTermiteAttic,txtVisibleTermiteInteriorHouse,
	txtTODAY_BASEMENT,txtTODAY_BAITING_REPLACED,txtTODAY_CRAWL_SPACE_DOOR,
	txtTODAY_CRAWL_SPACE_VENTS,txtTODAY_NUMBER_SNAPTRAPS,txtBUILDERS_CONTRUCTION_GAP,
	txtROOF_PITCHE_SEALED,txtBREEZY_WAY_GAAREGE,txtTODAY_ROOF_VENTS,
	txtTODAY_GARAGE_DOOR_SEALED,txtTODAYS_SOFFIT_VENTS,txtTODAYS_AC_DRYER,
	txtTODAY_NUMBER_LIVEANIMAL_TRAPS_INSTALLES,txtMosquitoServices,txtCheckedRunDurationTime
	,txtCheckedTimeSystemRuns,txtSchoolTreatment;
	
	RadioButton radioCash,radioCheck,radioCreditCard,radioBillLater,radioPreBill,radioNoCharge;
	
	LinearLayout linearlayout_CheckNo,linearlayout_DrivingLic,linearlayout_amount,LAYOUT_SERVICEFOR,LAYOUT_AREASINSPECTED,
	LAYOUT_INSPECTED_TREATED,layoutAnts,layoutOtherPest,LAYOUT_OUTSIDE_SERVICE,LAYOUT_PEST_ZONE
	,layout_pestControlserivces,layout_termiteserivces,layoutRODENTSERVICES,
	layoutBUILDERS_CONTRUCTION_GAP,layoutROOF_PITCHE_SEALED,layoutBREEZY_WAY_GAAREGE,
	layoutTODAY_ROOF_VENTS,layoutTODAY_GARAGE_DOOR_SEALED,layoutTODAYS_SOFFIT_VENTS,
	layoutTODAYS_AC_DRYER,layoutTODAY_NUMBER_LIVEANIMAL_TRAPS_INSTALLES,
	layoutTODAY_NUMBER_SNAPTRAPS,layoutTODAY_CRAWL_SPACE_VENTS,
	layoutTODAY_CRAWL_SPACE_DOOR,layout_mosquito_services,LAYOUT_SCHOOL_TREATMENT;
	
	TableRow rowHeadingPestControl,rowFenceLines,rowBackYard,rowFrontyard,
	rowExteriorperimeterofhouse,rowRoofLine,rowGarage,rowInAttic,rowInteriorofhouse,
	rowBasement,rowVisibleSigns,rowNumberofMonitoringDevices,
	rowVisibleTermiteMonitorDevices,rowTermiteMoniterDevicereplaced,rowVisibleTermiteActivity,
	rowVisibleTermiteAttic,rowTODAY_BASEMENT,rowTODAY_BAITING_REPLACED,
	rowVisibleTermiteInteriorHouse,rowMosquitoServices,rowCheckedRunDurationTime,rowCheckedTimeSystemRuns;
	
	EditText edtCheckNo,edtDrivingLic,edtAmount,edtCustomer,edtAddress,edtBillingAddress,edtHomePhone,edtCellPhone,edtAcct,
	         edtChargerForCurrentServices,edtBalanceDue,edtTax,edtEmp,edtTotalAmount,edtZip,edtOpenOfficeNote;
    String serviceID_new,strOutsidepestControlServices,strInsidepestControlServices;
    Button btnSendEmail,btnsubmitToPestpac,btnSignature,btnIagreeSignature,btnRecordAudio,btnAfterImage;
	
    ImageView imageViewTechnicianSign,imageViewCustomerSign,playBtn,stopPlayBtn;
    ArrayList<String> tempImagList = new ArrayList<String>();
	ArrayList<ImgDto> listImg = new ArrayList<ImgDto>();
	GalleryImageAdapter adapter;
	String ImageName;
	public static Bitmap bitmap;
	public static int REQUEST_Camera_ACTIVITY_CODE=101;
	int imageCount=1;
    private Bitmap mBitmap;
    GeneralInfoDTO generalInfoDTO;
	LogDTO logDTO;
	UserDTO userDTO;
	OResidential_pest_DTO oResidential_pest_DTO;
	HorizontalListView imageListView;
	HoustonFlowFunctions houstonFlowFunctions;
	ArrayList<String> tempStoreImageName = new ArrayList<String>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.or_service_invoice_report);
		
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
	    getActionBar().setDisplayShowHomeEnabled(true);
		
		Thread.setDefaultUncaughtExceptionHandler(new TopExceptionHandler(this));
		Mint.initAndStartSession(OR_TodayServiceInvoice.this, "e53b080c");
		initialize();
		
		generalInfoDTO = (GeneralInfoDTO)this.getIntent().getExtras().getSerializable("generalInfoDTO");
		userDTO  = (UserDTO)this.getIntent().getExtras().getSerializable("userDTO");
		serviceID_new = getIntent().getExtras().getString(ParameterUtil.ServiceID_new);
		strInsidepestControlServices = getIntent().getExtras().getString("strInsideService");
		strOutsidepestControlServices = getIntent().getExtras().getString("strOutsideService");
		setValuesInComponents(); 
		if(generalInfoDTO.getServiceStatus().equals("Complete"))
		{
			btnAfterImage.setEnabled(false);
			btnRecordAudio.setEnabled(false);
			playBtn.setEnabled(false);
			btnSignature.setEnabled(false);
			btnIagreeSignature.setEnabled(false);
			btnsubmitToPestpac.setEnabled(false);
		}
		else
		{
			btnAfterImage.setEnabled(true);
			btnRecordAudio.setEnabled(true);
			playBtn.setEnabled(true);
			btnSignature.setEnabled(true);
			btnIagreeSignature.setEnabled(true);
			btnsubmitToPestpac.setEnabled(true);
		}
		super.onCreate(savedInstanceState);
		
	}
	private void setValuesInComponents()
	{
		// TODO Auto-generated method stub
		JSONObject jsServiceObject= houstonFlowFunctions.getTodaysOrlandoReport(serviceID_new);

		if(jsServiceObject!=null)
		{
			oResidential_pest_DTO= new Gson().fromJson(jsServiceObject.toString(), OResidential_pest_DTO.class);
		}
		//generalInfoDTO= new Gson().fromJson(houstonFlowFunctions.getSelectedService(serviceID_new).toString(), GeneralInfoDTO.class);

		setValuesForHeader();
		setValuesForServiceFor();
		setValuesForAreasInspected();
		/*setValuesForInspectedTreatedFor();
		setValuesForOutsideOnly();
		setValuesForPestActivityByZone();
		setChemicalTable();
		setValuesForPestControlServices();
		setValuesForTermiteServices();
		setValuesForRodentServices();
		setValuesForMosquitoServices();
		setValuesForRecommendations();
		setValuesForInvoice();
		reloadPriviousBeforeImage();
		straudioFile=generalInfoDTO.getAudioFile();
        if(!CommonFunction.checkString(straudioFile, "").equals(""))
        {
        	textRecordingPoint.setText("Audio available"); 	
        }
        else
        {
        	textRecordingPoint.setText("Audio not available");
        }*/
		
	}
	private void setValuesForInspectedTreatedFor() 
	{
		// TODO Auto-generated method stub
		String strInspectedTreatedFor=oResidential_pest_DTO.getInsectActivity();
		//straudioFile=serviceDTO.getAudioFile();
		if(strInspectedTreatedFor.length()>=1)
		{
			LAYOUT_INSPECTED_TREATED.setVisibility(View.VISIBLE);
			or_txtINSPECTED_TREATED.setText(strInspectedTreatedFor);
		}
		else
		{
			LAYOUT_INSPECTED_TREATED.setVisibility(View.GONE);
		}
		if(!CommonFunction.checkString(oResidential_pest_DTO.getAnts(), "").equals(""))
		{	
			layoutAnts.setVisibility(View.VISIBLE);	
			or_txtAnts.setText(oResidential_pest_DTO.getAnts());
		}
		else
		{
			layoutAnts.setVisibility(View.GONE);
		}
		if(!CommonFunction.checkString(oResidential_pest_DTO.getOtherPest(), "").equals(""))
		{	
			layoutOtherPest.setVisibility(View.VISIBLE);
			or_txtOtherPest.setText(oResidential_pest_DTO.getOtherPest());
		}
		else
		{
			layoutOtherPest.setVisibility(View.GONE);
		}
	}
	private void setValuesForServiceFor()
	{
		// TODO Auto-generated method stub
		String strServiceFor=oResidential_pest_DTO.getServiceFor();
		//straudioFile=serviceDTO.getAudioFile();
		if(strServiceFor.length()>=1)
		{
			LAYOUT_SERVICEFOR.setVisibility(View.VISIBLE);
			or_txtSERVICE_FOR.setText(strServiceFor);
		}
		else
		{
			LAYOUT_SERVICEFOR.setVisibility(View.GONE);
		}
	}
	private void setValuesForAreasInspected() 
	{
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		String strAreasInspected=oResidential_pest_DTO.getAreasInspected();
		//straudioFile=serviceDTO.getAudioFile();
		if(strAreasInspected.length()>=1)
		{
			LAYOUT_AREASINSPECTED.setVisibility(View.VISIBLE);
			or_txtAREAS_INSPECTED.setText(strAreasInspected);
		}
		else
		{
			LAYOUT_AREASINSPECTED.setVisibility(View.GONE);
		}
	}

	private void setValuesForHeader() {
		// TODO Auto-generated method stub
				//txtServiceReportAddress.setText(generalInfoDTO.getServiceAddress());
				edtAmount.setText("$ "+oResidential_pest_DTO.getAmount());
				edtCheckNo.setText(generalInfoDTO.getCheckNo());
				edtDrivingLic.setText(generalInfoDTO.getLicenseNo());
				txtTimeInHeader.setText(generalInfoDTO.getTimeIn());
				edtCustomer.setText(oResidential_pest_DTO.getCustomer());
				txtAccountNumber.setText(generalInfoDTO.getAccountNo());
				
				String strServiceAddress=generalInfoDTO.getAddress_Line1()+", "+generalInfoDTO.getAddress_Line2()+" "+generalInfoDTO.getCity()+", "+generalInfoDTO.getState()+", "+generalInfoDTO.getZipCode();
				edtAddress.setText(strServiceAddress);

				String strBillingAddress=generalInfoDTO.getBAddress_Line1()+", "+generalInfoDTO.getBAddress_Line2()+" "+generalInfoDTO.getBCity()+", "+generalInfoDTO.getBState()+", "+generalInfoDTO.getBZipCode();
				edtBillingAddress.setText(strBillingAddress);
				
				edtHomePhone.setText(generalInfoDTO.getHome_Phone());
				edtCellPhone.setText(generalInfoDTO.getCell_Phone());
				edtZip.setText(generalInfoDTO.getZipCode());
				edtAcct.setText(generalInfoDTO.getAccountNo());
				txtEmail.setText(generalInfoDTO.getEmail_Id());
				//txtOrder.setText(generalInfoDTO.getOrder_Number());
				txtOrderNumber.setText(generalInfoDTO.getOrder_Number());
				
				ArrayList<RadioButton> listRadioButton = new ArrayList<RadioButton>();
				listRadioButton.add(radioCash);
				listRadioButton.add(radioCheck);
				listRadioButton.add(radioCreditCard);
				listRadioButton.add(radioNoCharge);
				listRadioButton.add(radioBillLater);
				listRadioButton.add(radioPreBill);
				//Log.e("getPaymentType...",listRadioButton.size()+"");

				for(int i=0; i<listRadioButton.size() ; i++)
				{

					String selection =	listRadioButton.get(i).getText().toString();
					//	Log.v("","getPaymentType..."+selection+"..."+residential_pest_DTO.getPaymentType());
					if(selection.equals(oResidential_pest_DTO.getPaymentType()))
					{
						listRadioButton.get(i).setChecked(true);
						setView(i);
					}
				}
		
	}
	public void setView(int checkedId)
	{
		switch (checkedId) 
		{
		case 0:
			linearlayout_DrivingLic.setVisibility(View.GONE);
			linearlayout_CheckNo.setVisibility(View.GONE);
			linearlayout_amount.setVisibility(View.VISIBLE);
			break;
		case 1:
			linearlayout_DrivingLic.setVisibility(View.VISIBLE);
			linearlayout_CheckNo.setVisibility(View.VISIBLE);
			linearlayout_amount.setVisibility(View.VISIBLE);

			break;
		case 2:
			linearlayout_DrivingLic.setVisibility(View.GONE);
			linearlayout_CheckNo.setVisibility(View.GONE);
			linearlayout_amount.setVisibility(View.VISIBLE);
			break;
		case 3:
			linearlayout_DrivingLic.setVisibility(View.GONE);
			linearlayout_CheckNo.setVisibility(View.GONE);
			linearlayout_amount.setVisibility(View.GONE);

			break;
		case 4:
			linearlayout_DrivingLic.setVisibility(View.GONE);
			linearlayout_CheckNo.setVisibility(View.GONE);
			linearlayout_amount.setVisibility(View.GONE);

			break;
		case 5:
			linearlayout_DrivingLic.setVisibility(View.GONE);
			linearlayout_CheckNo.setVisibility(View.GONE);
			linearlayout_amount.setVisibility(View.GONE);

			break;
		default:
			break;
		}
	}
	private void initialize() {
		houstonFlowFunctions=new HoustonFlowFunctions(getApplicationContext());
		txtAccountNumber = (TextView) findViewById(R.id.txtAccountNumber);
		txtOrderNumber = (TextView) findViewById(R.id.txtOrderNumber);
		txtTimeInHeader = (TextView) findViewById(R.id.txtTimeInHeader);
		txtEmail = (TextView) findViewById(R.id.txtEmail);
		or_txtSERVICE_FOR = (TextView) findViewById(R.id.txtSERVICE_FOR);
		or_txtAREAS_INSPECTED = (TextView) findViewById(R.id.txtAREAS_INSPECTED);
		or_txtINSPECTED_TREATED = (TextView) findViewById(R.id.txtINSPECTED_TREATED);
		or_txtAnts = (TextView) findViewById(R.id.txtAnts);
		or_txtOtherPest = (TextView) findViewById(R.id.txtOtherPest);
		txtOutsideonly = (TextView) findViewById(R.id.txtOutsideonly);
		txtFenceLines = (TextView) findViewById(R.id.txtFenceLines);
		txtBackYard = (TextView) findViewById(R.id.txtBackYard);
		txtFrontYard = (TextView) findViewById(R.id.txtFrontYard);
		txtExteriorperimeterofhouse = (TextView) findViewById(R.id.txtExteriorperimeterofhouse);
		txtRoofLine = (TextView) findViewById(R.id.txtRoofLine);
		txtGarage = (TextView) findViewById(R.id.txtGarage);
		txtAttic = (TextView) findViewById(R.id.txtAttic);
		txtInteriorofhouse = (TextView) findViewById(R.id.txtInteriorofhouse);
		txtBasement = (TextView) findViewById(R.id.txtBasement);
		txtPestControlHeadInside = (TextView) findViewById(R.id.txtPestControlHeadInside);
		txtPestControlServiesInside = (TextView) findViewById(R.id.txtPestControlServiesInside);
		txtPestControlHeadOutside = (TextView) findViewById(R.id.txtPestControlHeadOutside);
		txtPestControlServiesOutside = (TextView) findViewById(R.id.txtPestControlServiesOutside);
		txtVisibleSigns = (TextView) findViewById(R.id.txtVisibleSigns);
		txtNumberofMonitoringDevices = (TextView) findViewById(R.id.txtNumberofMonitoringDevices);
		txtVisibleTermiteMonitorDevices = (TextView) findViewById(R.id.txtVisibleTermiteMonitorDevices);
		txtTermiteMoniterDevicereplaced = (TextView) findViewById(R.id.txtTermiteMoniterDevicereplaced);
		txtVisibleTermiteActivity = (TextView) findViewById(R.id.txtVisibleTermiteActivity);
		txtVisibleTermiteAttic = (TextView) findViewById(R.id.txtVisibleTermiteAttic);
		txtVisibleTermiteInteriorHouse = (TextView) findViewById(R.id.txtVisibleTermiteInteriorHouse);
		txtTODAY_BASEMENT = (TextView) findViewById(R.id.txtTODAY_BASEMENT);
		txtTODAY_BAITING_REPLACED = (TextView) findViewById(R.id.txtTODAY_BAITINGDEVICE_REPLACED);
		txtTODAY_CRAWL_SPACE_DOOR = (TextView) findViewById(R.id.txtTODAY_CRAWL_SPACE_DOOR);
		txtTODAY_CRAWL_SPACE_VENTS = (TextView) findViewById(R.id.txtTODAY_CRAWL_SPACE_VENTS);
		txtTODAY_NUMBER_SNAPTRAPS = (TextView) findViewById(R.id.txtTODAY_NUMBER_SNAPTRAPS);
		txtBUILDERS_CONTRUCTION_GAP = (TextView) findViewById(R.id.txtTODAY_BUILDER_CONSTRUCTION_GAP);
		txtROOF_PITCHE_SEALED = (TextView) findViewById(R.id.txtROOF_PITCHE_SEALED);
		txtBREEZY_WAY_GAAREGE = (TextView) findViewById(R.id.txtBREEZY_WAY_GAAREGE);
		txtTODAY_ROOF_VENTS = (TextView) findViewById(R.id.txtTODAY_ROOF_VENTS);
		txtTODAY_GARAGE_DOOR_SEALED = (TextView) findViewById(R.id.txtTODAY_GARAGE_DOOR_SEALED);
		txtTODAYS_SOFFIT_VENTS = (TextView) findViewById(R.id.txtTODAYS_SOFFIT_VENTS);
		txtTODAYS_AC_DRYER = (TextView) findViewById(R.id.txtTODAYS_AC_DRYER);
		txtTODAY_NUMBER_LIVEANIMAL_TRAPS_INSTALLES = (TextView) findViewById(R.id.txtTODAY_NUMBER_LIVEANIMAL_TRAPS_INSTALLES);
		txtMosquitoServices = (TextView) findViewById(R.id.txtMosquitoServices);
		txtCheckedRunDurationTime = (TextView) findViewById(R.id.txtCheckedRunDurationTime);
		txtCheckedTimeSystemRuns = (TextView) findViewById(R.id.txtCheckedTimeSystemRuns);
		txtSchoolTreatment = (TextView) findViewById(R.id.txtSchoolTreatment);
		
		radioCash = (RadioButton) findViewById(R.id.radioCash);
		radioCheck = (RadioButton) findViewById(R.id.radioCheck);
		radioCreditCard = (RadioButton) findViewById(R.id.radioCreditCard);
		radioBillLater = (RadioButton) findViewById(R.id.radioBillLater);
		radioPreBill = (RadioButton) findViewById(R.id.radioPreBill);
		radioNoCharge = (RadioButton) findViewById(R.id.radioNoCharge);
		
		linearlayout_CheckNo=(LinearLayout)findViewById(R.id.linearlayout_CheckNo);
		linearlayout_DrivingLic=(LinearLayout)findViewById(R.id.linearlayout_DrivingLic);
		layoutAnts=(LinearLayout)findViewById(R.id.layoutAnts);
		layoutOtherPest=(LinearLayout)findViewById(R.id.layoutOtherPest);
		linearlayout_amount = (LinearLayout) findViewById(R.id.linearlayout_amount);
		LAYOUT_SERVICEFOR = (LinearLayout) findViewById(R.id.LAYOUT_SERVICEFOR);
		LAYOUT_AREASINSPECTED = (LinearLayout) findViewById(R.id.LAYOUT_AREASINSPECTED);
		LAYOUT_INSPECTED_TREATED = (LinearLayout) findViewById(R.id.LAYOUT_INSPECTED_TREATED);
		LAYOUT_OUTSIDE_SERVICE = (LinearLayout) findViewById(R.id.LAYOUT_OUTSIDE_SERVICE);
		LAYOUT_PEST_ZONE = (LinearLayout) findViewById(R.id.LAYOUT_PEST_ZONE);
		layout_pestControlserivces = (LinearLayout) findViewById(R.id.layout_pestControlserivces);
		layout_termiteserivces = (LinearLayout) findViewById(R.id.layout_termiteserivces);
		layoutRODENTSERVICES = (LinearLayout) findViewById(R.id.layoutRODENTSERVICES);
		layoutBUILDERS_CONTRUCTION_GAP = (LinearLayout) findViewById(R.id.layoutTODAY_BUILDER_CONSTRUCTION_GAP);
		layoutROOF_PITCHE_SEALED = (LinearLayout) findViewById(R.id.layoutROOF_PITCHE_SEALED);
		layoutBREEZY_WAY_GAAREGE = (LinearLayout) findViewById(R.id.layoutBREEZY_WAY_GAAREGE);
		layoutTODAY_ROOF_VENTS = (LinearLayout) findViewById(R.id.layoutTODAY_ROOF_VENTS);
		layoutTODAY_GARAGE_DOOR_SEALED = (LinearLayout) findViewById(R.id.layoutTODAY_GARAGE_DOOR_SEALED);
		layoutTODAYS_SOFFIT_VENTS = (LinearLayout) findViewById(R.id.layoutTODAYS_SOFFIT_VENTS);
		layoutTODAYS_AC_DRYER = (LinearLayout) findViewById(R.id.layoutTODAYS_AC_DRYER);
		layoutTODAY_NUMBER_LIVEANIMAL_TRAPS_INSTALLES = (LinearLayout) findViewById(R.id.layoutTODAY_NUMBER_LIVEANIMAL_TRAPS_INSTALLES);
		layoutTODAY_NUMBER_SNAPTRAPS = (LinearLayout) findViewById(R.id.layoutTODAY_NUMBER_SNAPTRAPS);
		layoutTODAY_CRAWL_SPACE_VENTS = (LinearLayout) findViewById(R.id.layoutTODAY_CRAWL_SPACE_VENTS);
		layoutTODAY_CRAWL_SPACE_DOOR = (LinearLayout) findViewById(R.id.layoutTODAY_CRAWL_SPACE_DOOR);
		layout_mosquito_services = (LinearLayout) findViewById(R.id.layout_mosquito_services);
		LAYOUT_SCHOOL_TREATMENT = (LinearLayout) findViewById(R.id.LAYOUT_SCHOOL_TREATMENT);
	
		rowHeadingPestControl = (TableRow) findViewById(R.id.rowHeadingPestControl);
		rowFenceLines = (TableRow) findViewById(R.id.rowFenceLines);
		rowBackYard = (TableRow) findViewById(R.id.rowBackYard);
		rowFrontyard = (TableRow) findViewById(R.id.rowFrontyard);
		rowExteriorperimeterofhouse = (TableRow) findViewById(R.id.rowExteriorperimeterofhouse);
		rowRoofLine = (TableRow) findViewById(R.id.rowRoofLine);
		rowGarage = (TableRow) findViewById(R.id.rowGarage);
		rowInAttic = (TableRow) findViewById(R.id.rowInAttic);
		rowInteriorofhouse = (TableRow) findViewById(R.id.rowInteriorofhouse);
		rowBasement = (TableRow) findViewById(R.id.rowBasement);
		rowVisibleSigns = (TableRow) findViewById(R.id.rowVisibleSigns);
		rowNumberofMonitoringDevices = (TableRow) findViewById(R.id.rowNumberofMonitoringDevices);
		rowVisibleTermiteMonitorDevices = (TableRow) findViewById(R.id.rowVisibleTermiteMonitorDevices);
		rowTermiteMoniterDevicereplaced = (TableRow) findViewById(R.id.rowTermiteMoniterDevicereplaced);
		rowVisibleTermiteActivity = (TableRow) findViewById(R.id.rowVisibleTermiteActivity);
		rowVisibleTermiteAttic = (TableRow) findViewById(R.id.rowVisibleTermiteAttic);
		rowTODAY_BASEMENT = (TableRow) findViewById(R.id.rowTODAY_BASEMENT);
		rowTODAY_BAITING_REPLACED = (TableRow) findViewById(R.id.rowTODAY_BAITINGDEVICE_REPLACED);
		rowVisibleTermiteInteriorHouse = (TableRow) findViewById(R.id.rowVisibleTermiteInteriorHouse);
		rowMosquitoServices = (TableRow) findViewById(R.id.rowMosquitoServices);
		rowCheckedRunDurationTime = (TableRow) findViewById(R.id.rowCheckedRunDurationTime);
		rowCheckedTimeSystemRuns = (TableRow) findViewById(R.id.rowCheckedTimeSystemRuns);	
	
		edtCheckNo = (EditText) findViewById(R.id.edtCheckNo);
		edtDrivingLic = (EditText) findViewById(R.id.edtDrivingLic);
		edtAmount = (EditText) findViewById(R.id.edtAmount);
		edtCustomer = (EditText) findViewById(R.id.edtCustomer);
		edtAddress = (EditText) findViewById(R.id.edtADDRESS);
		edtBillingAddress = (EditText) findViewById(R.id.edtBillingAddress);
		edtHomePhone = (EditText) findViewById(R.id.edtHOME_PHONE);
		edtCellPhone = (EditText) findViewById(R.id.edtCELL_PHONE);
		edtAcct = (EditText) findViewById(R.id.edtACCT);
		edtChargerForCurrentServices = (EditText) findViewById(R.id.edtChargesCurentService);
		edtBalanceDue = (EditText) findViewById(R.id.edtBALANCE_DUE);
		edtTax = (EditText) findViewById(R.id.edtTax);
		edtEmp = (EditText) findViewById(R.id.edtEMP);
		edtTotalAmount = (EditText) findViewById(R.id.edtTotal);
		edtZip = (EditText) findViewById(R.id.edtZIP);
		edtOpenOfficeNote = (EditText) findViewById(R.id.edtOpenOfficeNote);
		logDTO=new LogDTO();
		imageViewTechnicianSign=(ImageView)findViewById(R.id.imageViewTechnicianSign);
		imageViewCustomerSign=(ImageView)findViewById(R.id.imageViewCustomerSign);
		/*btnBack=(Button)findViewById(R.id.btnBack);
		btnBack.setOnClickListener(new OnButtonClick()); 
        */
		btnSendEmail=(Button)findViewById(R.id.btnSendEmail);
		btnSendEmail.setOnClickListener(new OnButtonClick());

		btnsubmitToPestpac = (Button)findViewById(R.id.btnsubmitToPestpac);
		btnsubmitToPestpac.setOnClickListener(new OnButtonClick());

		btnSignature=(Button)findViewById(R.id.btnServiceSpecialistSignature);
		btnSignature.setOnClickListener(new OnButtonClick());
		btnIagreeSignature=(Button)findViewById(R.id.btnIAgreetoPricing);
		btnIagreeSignature.setOnClickListener(new OnButtonClick());
		btnRecordAudio=(Button)findViewById(R.id.btnRecord);
		btnRecordAudio.setOnClickListener(new OnButtonClick());
		imageListView=(HorizontalListView)findViewById(R.id.listImg);
		btnAfterImage=(Button)findViewById(R.id.btnCamera);
		btnAfterImage.setOnClickListener(new OnButtonClick());
		playBtn = (ImageView)findViewById(R.id.play);
		stopPlayBtn = (ImageView)findViewById(R.id.stopPlay);
		playBtn.setOnClickListener(new OnButtonClick());
		stopPlayBtn.setOnClickListener(new OnButtonClick());

		generalInfoDTO=new GeneralInfoDTO();
		oResidential_pest_DTO=new OResidential_pest_DTO();
		imageListView.setOnItemLongClickListener(new OnGalleryClick());
		imageListView.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view,int position, long id)
			{
				Bitmap bmp= listImg.get(position).getImageBitMap();
				if(bmp!=null)
				{
					bitmap=bmp;
					Intent intent=new Intent(OR_TodayServiceInvoice.this, FullScreenViewActivity.class);
					intent.putExtra("from","report");
					startActivity(intent);
				}
			}

		});


	}
	class OnGalleryClick implements OnItemLongClickListener 
	{

		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
				int position, long arg3) 
		{
			if(generalInfoDTO.getServiceStatus().equals("InComplete"))
			{
				//Toast.makeText(getApplicationContext(),IWitnessActivity.position+ "  "+position, Toast.LENGTH_LONG).show();
				if(listImg.size()>0)
				{
					listImg.remove(position);
					if(tempImagList.size()>0)
					{
						tempImagList.remove(position);

					}
					if(tempStoreImageName.size()>0)
					{
						tempStoreImageName.remove(position);
					}
					RefreshImage();
					//alertboxClearGalleary("Confirmation", "Are you want to remove selected image ?");
				}
			}
			return false;
		}

	}
	private void RefreshImage()
	{
		try
		{ 
			listImg.clear();
			Bitmap bmp = null;
			for (int i = 0; i < tempStoreImageName.size(); i++) {
				bmp = null;
				ImgDto dto = new ImgDto();
				try {
					//	Log.e("Image Name in refresh",tempStoreImageName.get(i));
					/*	bmp = BitmapFactory.decodeStream(getContentResolver()
							.openInputStream(Uri.fromFile(CommanFunction.getFileLocation(IWitnessActivity.this,tempStoreImageName.get(i)))));
					 */    
					bmp=decodeSmallUri(Uri.fromFile(CommonFunction.getFileLocation(getApplicationContext(),tempStoreImageName.get(i))));

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//Bitmap bitmap = scaleBitmap(bmp, 100, 100);
				dto.setImageBitMap(bmp);
				if (!tempImagList.contains(tempStoreImageName.get(i))) {

					if (bmp != null) {

						//CompressImage(bmp);
					}
				}// dto.setImageUri(Uri.fromFile(CommanFunction.getFileLocation(IWitnessActivity.this,
				// tempStoreImageName.get(i))));

				tempImagList.add(tempStoreImageName.get(i));
				listImg.add(dto);


			}

			//Log.e("List Size", listImg.size() + "");

			if (listImg.size() == 0) {
				ImgDto dto = new ImgDto();
				dto.setImagedrawable(R.drawable.noimage);
				listImg.add(dto);
			}else
			{

			}
			adapter=new GalleryImageAdapter(getApplicationContext(), listImg) ;
			imageListView.setAdapter(adapter);
			//adapter.notifyDataSetChanged(); // TODO Auto-generated method stub
		}catch (Exception e) {
			Log.e("Exception in Refresh Image", e+"");
		}
	}
	private Bitmap decodeSmallUri(Uri selectedImage) throws FileNotFoundException 
	{
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(getApplicationContext().getContentResolver().openInputStream(selectedImage), null, o);

		final int REQUIRED_SIZE = 100;

		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeStream(getApplicationContext().getContentResolver().openInputStream(selectedImage), null, o2);
	}
	class OnButtonClick implements OnClickListener 
	{
		@Override
		public void onClick(View v) 
		{
			// TODO Auto-generated method stub

			switch (v.getId()) 
			{
			/*case R.id.play:
				// TODO Auto-generated method stub
				//stopPlayBtn.setVisibility(v.VISIBLE);
				if(!CommonFunction.checkString(straudioFile, "").equals(""))
				{
					play(v);
				}
				else
				{
				//.	text.setText("Recording Point: Audio File Not Available");
				}
				break;
			case R.id.stopPlay:
				stopPlay();
				break;	
			case R.id.btnBack:
				finish();
				break;
			case R.id.btnsubmitToPestpac:
				//btnsubmitToPestpac.setVisibility(View.GONE);
				//btnSendEmail.setVisibility(View.VISIBLE);
				//submitService();
				isValidate = true;
				MandatoryList.clear();
				validate();
				if(isValidate)
				{
				btnsubmitToPestpac.setEnabled(false);	
				showSendEmailDailog();
				}
				else
				{
					charSequenceItems = MandatoryList.toArray(new CharSequence[MandatoryList.size()]);
					createMessageList(charSequenceItems,"");
				}
				break;

			case R.id.btnSendEmail:
				break;

			case R.id.btnServiceSpecialistSignature:
				ShowSignatureDailog("Service Specialist Signature",1);
				break;

			case R.id.btnIAgreetoPricing:
				ShowSignatureDailog("I Agree to Pricing and Terms & Condition",2);
				break;

			case R.id.btnRecord:
				stopPlay();
				Intent intent=new Intent(getApplicationContext(),AudioRecordingActivity.class);
				intent.putExtra(ParameterUtil.ServiceID_new,String.valueOf(serviceID_new));
				startActivityForResult(intent,RECORD_AUDIO_REQUEST_CODE);
				break;
			case R.id.btnCamera:
				if (tempStoreImageName.size() != 3)
				{
					if (CommonFunction.isSdPresent())
					{
						takePicture();
					}
					else
					{
						Toast.makeText(getApplicationContext(),"SdCard Not Present", Toast.LENGTH_SHORT).show();
					}
				}
				else
				{
					Toast.makeText(getApplicationContext(),"Max number of upload image exceeded.",Toast.LENGTH_SHORT).show();
				}
				break;*/
			default:
				break;
			}
		}
	}
}
