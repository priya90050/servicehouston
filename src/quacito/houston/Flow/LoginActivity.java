package quacito.houston.Flow;

//import android.support.v7.app.ActionBarActivity;
//import quacito.austin.serviceflow.LoginActivity.OnButtonClick;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;
import quacito.houston.CommonUtilities.CommonFunction;
import quacito.houston.CommonUtilities.LoggerUtil;
import quacito.houston.CommonUtilities.ParameterUtil;
import quacito.houston.CommonUtilities.SharedPreference;
import quacito.houston.model.UserDTO;

import quacito.houston.Flow.servicehouston.R;
import com.google.gson.Gson;
//import com.google.gson.Gson;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity{

	EditText edtUsername, edtPassword;
	Button btnLogin, btnCancle;
	String strUsername="", strPassword="";
	CheckBox check_Save;
	private final String NAMESPACE = CommonFunction.IPAD_ABC;
	private final String URL=CommonFunction.IPAD_ABC+"Houston_Service.asmx?op";
	private final String SOAP_ACTION=CommonFunction.IPAD_ABC+"check_Login";
	private final String METHOD_NAME="check_Login";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		setContentView(R.layout.activity_login);
		Thread.setDefaultUncaughtExceptionHandler(new TopExceptionHandler(this));
		initialize();
		
		//Log.e("already running if is", ""+isMyServiceRunning("quacito.houston.Flow.SynchServiceToServerServices"));
		/*if(isMyServiceRunning("quacito.houston.Flow.SynchServiceToServerServices"))
		{
			Log.e("already running", "Service already running");
		}
		else
		{
			Log.e("already not running", "Service already not running");
			startService(new Intent(getApplicationContext(), SynchServiceToServerServices.class));
			
		}*/
	}
	private boolean isMyServiceRunning(String ServicePakage)
	{
		ActivityManager manager = (ActivityManager)getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) 
		{
			if (ServicePakage.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
	private void initialize() 
	{
		// TODO Auto-generated method stub
		edtPassword = (EditText) findViewById(R.id.edtPassword);
		edtUsername = (EditText) findViewById(R.id.edtUserName);
		btnLogin = (Button) findViewById(R.id.btnLogin);
		btnCancle = (Button) findViewById(R.id.btn_login_Cancel);
		btnLogin.setOnClickListener(new OnButtonClick());
		btnCancle.setOnClickListener(new OnButtonClick());
		check_Save=(CheckBox)findViewById(R.id.check_Save);
		
		if(CommonFunction.haveInternet(getApplicationContext()))
		{
			String check_login=SharedPreference.getSharedPrefer(getApplicationContext(), SharedPreference.LOGIN_PREF);
			String check_logout=SharedPreference.getSharedPrefer(getApplicationContext(), SharedPreference.LOGOUT_PREF);
			
			if(!check_login.equals("0") && !check_logout.equals("logout"))
			{
				strPassword=SharedPreference.getSharedPrefer(getApplicationContext(), SharedPreference.USER_PASSWORD);
				strUsername=SharedPreference.getSharedPrefer(getApplicationContext(), SharedPreference.USER_NAME);
				check_Save.setChecked(true);
				edtPassword.setText(strPassword);
				edtUsername.setText(strUsername);
				CheckLoginAsyncTask checkLoginAsyncTask = new CheckLoginAsyncTask();
				checkLoginAsyncTask.execute(strUsername, strPassword);
			}
			else if(!check_login.equals("0") && check_logout.equals("logout"))
			{
				strPassword=SharedPreference.getSharedPrefer(getApplicationContext(), SharedPreference.USER_PASSWORD);
				strUsername=SharedPreference.getSharedPrefer(getApplicationContext(), SharedPreference.USER_NAME);
				check_Save.setChecked(true);
				edtPassword.setText(strPassword);
				edtUsername.setText(strUsername);
			}
		}
		else
		{
			String check_login=SharedPreference.getSharedPrefer(getApplicationContext(), SharedPreference.LOGIN_PREF);
			String check_logout=SharedPreference.getSharedPrefer(getApplicationContext(), SharedPreference.LOGOUT_PREF);
		
			if(!check_login.equals("0") && !check_logout.equals("logout"))
			{
				String result=SharedPreference.getSharedPrefer(getApplicationContext(), SharedPreference.USER_DETAIL);
				//Log.e("in else...","Result..."+result);
				onLogin(result);
			}
		}
	}
	 public UserDTO parseLoginResponse(String resp)
		{
			try 
			{
	      	JSONArray jsBeanList = new JSONArray(resp);
				//Log.e("","jsBeanList"+jsBeanList.length());
				if(jsBeanList.length()>0)
				{
					JSONObject jsUserDTO=jsBeanList.getJSONObject(0);
					try 
					{
						String result=jsUserDTO.getString("Result");
						return null;
					} catch (Exception e)
					{
						// TODO: handle exception
						
					}
					UserDTO dto = new Gson().fromJson(jsUserDTO.toString(),UserDTO.class);
					//Log.e("","jsBeanList..UserDTO.."+dto);
					return dto;
				}
				return null;
			}catch (Exception e)
			{
				Log.e("Exception", e.toString());
			}
			return null;
		}
	 private void onLogin(String result) 
		{
			// TODO Auto-generated method stub
			UserDTO dto=parseLoginResponse(result);
			//Log.e("on login...","dto..."+dto);
			if(dto!=null && dto.getUsername()!=null && dto.getEmployeeId() > 0 )
			{
				if(check_Save.isChecked())
				{
				SharedPreference.setSharedPrefer(getApplicationContext(),SharedPreference.USER_NAME,strUsername);
				SharedPreference.setSharedPrefer(LoginActivity.this,SharedPreference.LOGIN_PREF,"1");
				SharedPreference.setSharedPrefer(getApplicationContext(),SharedPreference.USER_NAME,strUsername);
				SharedPreference.setSharedPrefer(getApplicationContext(),SharedPreference.USER_PASSWORD,strPassword);
	            }
				SharedPreference.setSharedPrefer(getApplicationContext(),SharedPreference.USER_DETAIL, result);
					
				SharedPreference.setSharedPrefer(getApplicationContext(),SharedPreference.USER_DETAIL, result);
				setEmployeeId(String.valueOf(dto.getEmployeeId()));
				setEmployeeUserName(dto.getUsername());
			
				
				Intent intent=new Intent(getApplicationContext(), ServiceListAcitivity.class);
				//Log.e("","jsBeanList..UserDTO.."+dto.getPestPackId());
				intent.putExtra("userDTO", dto);
				startActivity(intent);
				finish();
				}
			else
				{
					Toast.makeText(getApplicationContext(),getResources().getString(R.string.no_internet_connection),Toast.LENGTH_LONG).show();
					LoggerUtil.e("inelse", "inside else");
				}
			}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	public void getFormData()
	{
		strUsername=edtUsername.getText().toString().trim();
		strPassword=edtPassword.getText().toString().trim();
	}
	String msg="";
	public boolean checkValidation()
	{

		if(strUsername.toString().equalsIgnoreCase(""))
		{
			msg="Username Required!";
			return false;
		}
		if(strPassword.toString().equalsIgnoreCase(""))
		{
			msg="Password required!";
			return false;
		}
		return true;
	}
	class OnButtonClick implements OnClickListener
	{

		@Override
		public void onClick(View v)
		{
			switch (v.getId()) {
			case R.id.btnLogin:
				// have internet
				getFormData();
				if(CommonFunction.haveInternet(getApplicationContext()))
				{
					if(checkValidation())
					{
						CheckLoginAsyncTask checkLoginAsyncTask = new CheckLoginAsyncTask();
						checkLoginAsyncTask.execute(strUsername, strPassword);
					}
					else
					{
						CommonFunction.AboutBox(msg, LoginActivity.this);
					}
				}
				else
				{
					String check_login=SharedPreference.getSharedPrefer(getApplicationContext(), SharedPreference.LOGIN_PREF);
					String check_logout=SharedPreference.getSharedPrefer(getApplicationContext(), SharedPreference.LOGOUT_PREF);
				
					if(!check_login.equals("0") && !check_logout.equals("logout"))
					{
					String result=SharedPreference.getSharedPrefer(getApplicationContext(), SharedPreference.USER_DETAIL);
					//Log.e("in else...","Result..."+result);
					onLogin(result);
					}
					else
					{
					Toast.makeText(getApplicationContext(),getResources().getString(R.string.no_internet_connection),Toast.LENGTH_LONG).show();
					}
					}
				break;
				
			case R.id.btnCancel:
				finish();
				System.exit(0);
				break;
				
			default:
				break;
			}
		}
	}
	ProgressDialog pg;
	public class CheckLoginAsyncTask extends AsyncTask<String, String, String>
	{

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pg = new ProgressDialog(LoginActivity.this);
			pg.show();
			pg.setCancelable(false);
			pg.setMessage("Please Wait...");
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String unm = params[0];
			String pwd = params[1];
		//	Log.e("unm",unm);
			//Log.e("pwd",pwd);
			return checkLoginDetailFromServer(unm, pwd);
		}

		@Override
		protected void onPostExecute(String result) 
		{
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(pg!=null)
			{
				try {
					pg.dismiss();
				} catch (Exception e) {
					// TODO: handle exception
				}

			}
			//Log.e("login resp from server",result);
			if(!result.toString().trim().contains("No Record Found"))
			{
				UserDTO dto=parseLoginResponse(result);
				if(dto.getUsername()!=null)
				{
					if(check_Save.isChecked())
					{
						SharedPreference.setSharedPrefer(getApplicationContext(),SharedPreference.USER_NAME,strUsername);
						SharedPreference.setSharedPrefer(LoginActivity.this,SharedPreference.LOGIN_PREF,"1");
						SharedPreference.setSharedPrefer(getApplicationContext(),SharedPreference.USER_NAME,strUsername);
						SharedPreference.setSharedPrefer(getApplicationContext(),SharedPreference.USER_PASSWORD,strPassword);
            		}
					else
					{
						SharedPreference.setSharedPrefer(LoginActivity.this,SharedPreference.LOGIN_PREF,"0");
					}
					SharedPreference.setSharedPrefer(getApplicationContext(),SharedPreference.LOGOUT_PREF, "notlogout");
					SharedPreference.setSharedPrefer(getApplicationContext(),SharedPreference.USER_DETAIL, result);
					
					SharedPreference.setSharedPrefer(getApplicationContext(), SharedPreference.USER_ID,String.valueOf(dto.getEmployeeId()));
					//Log.e("","getEmployeeId..UserDTO.."+dto.getEmployeeId());
				
					Intent intent=new Intent(LoginActivity.this, ServiceListAcitivity.class);
					//Log.e("dto..id..","get id..."+dto.getPestPackId());
					intent.putExtra("userDTO", dto);
					intent.putExtra("back", "general");
					startActivity(intent);
					finish();
				}
			}
			else
			{
				/*Intent intent = new Intent(LoginActivity.this,HomeScreenActivity.class);
				   startActivity(intent);*/

				/*Intent intent = new Intent(LoginActivity.this,DarogaHomeScreenActivity.class);
				startActivity(intent);*/
				handler.sendEmptyMessage(0);
			}
		}
	}
	public String checkLoginDetailFromServer(String unm, String pwd)
	{

		SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
		
		PropertyInfo UserName = new PropertyInfo();
		UserName.setName(ParameterUtil.Username);
		UserName.setValue(unm);
		//UserName.setValue("22.71955,75.85773:22.71527,75.84183:22.71311,75.8407:22.70909,75.82976:22.69974,75.82729:22.69407,75.82839:22.69058,75.82604:22.68046,75.8208:22.66016,75.80746:22.6441,75.80739:22.63487,75.79965:22.62805,75.80623:22.61001,75.7927:22.61616,75.73742:22.59605,75.70212:22.59017,75.69232:22.59456,75.67429:22.59024,75.66041:22.58175,75.65224:22.57279,75.64687:22.5382,75.65439:22.49711,75.6661:22.47701,75.65855:22.45813,75.64702:22.43804,75.62373:22.42179,75.60163:22.41114,75.5821:22.40363,75.55238:22.39151,75.53826:22.38437,75.53227:22.37474,75.52613:22.33802,75.50419:22.32404,75.49634:22.31166,75.50282:22.28788,75.49471:22.25867,75.4829:22.24858,75.47176:22.23941,75.47059:22.21431,75.46084:22.19347,75.46412:22.18214,75.46063:22.15964,75.44652:22.14544,75.44855:22.11753,75.44584:22.08828,75.41921:22.06977,75.39141:22.06044,75.38862:22.05258,75.37783:22.04163,75.37853:22.03759,75.36539:22.03222,75.3537:22.01526,75.34163:21.99324,75.32391:21.98295,75.30026:21.95958,75.28434:21.89793,75.23804:21.87043,75.21371:21.84063,75.19387:21.81896,75.18082:21.78087,75.16815:21.75106,75.15161:21.73203,75.12674:21.72099,75.1212:21.69353,75.11469:21.67776,75.10766:21.66374,75.09082:21.65656,75.07945:21.64413,75.07712:21.61188,75.07034:21.5949,75.06183:21.5886,75.05692:21.57896,75.05866:21.57413,75.05251:21.56637,75.05215:21.5336,75.04877:21.52128,75.04067:21.50017,75.01567:21.48185,75.00198:21.46419,74.99583:21.44513,74.98914:21.4367,74.98771:21.426,74.98015:21.39437,74.95991:21.35849,74.95378:21.34565,74.94328:21.33561,74.92145:21.32928,74.89732:21.32078,74.89026:21.30285,74.87463:21.28711,74.85261:21.27809,74.84742:21.26501,74.84699:21.22699,74.83565:21.19178,74.82856:21.14487,74.80519:21.11818,74.79098:21.09224,74.792:21.06266,74.78565:20.98901,74.78427:20.97414,74.78642:20.95845,74.78129:20.94021,74.77511:20.93339,74.77877:20.92863,74.79328:20.91434,74.79997:20.89403,74.79703:20.88119,74.79057:20.87247,74.77437:20.86117,74.76695:20.82104,74.74748:20.81423,74.74446:20.78988,74.74253:20.78211,74.73426:20.77306,74.73053:20.75063,74.71801:20.73756,74.70551:20.7236,74.70058:20.68717,74.68555:20.65923,74.67319:20.6327,74.64934:20.59626,74.60228:20.57488,74.56467:20.55465,74.53895:20.55474,74.5293:20.51951,74.51619:20.5023,74.51434:20.48985,74.50898:20.45881,74.50418:20.40621,74.48658:20.35803,74.48121:20.34651,74.47754:20.31643,74.45321:20.28233,74.44006:20.26743,74.43989:20.20559,74.44047:20.1977,74.43282:20.19394,74.43327:20.17588,74.43952:20.10975,74.45463:20.05664,74.47849:20.03256,74.49138:20.01692,74.49458:19.99061,74.49264:19.9665,74.49372:19.94953,74.49013:19.907,74.48258:19.88272,74.48706:19.87302,74.48398:19.86041,74.47954:19.85909,74.46852:19.85506,74.45833:19.85003,74.44587:19.83536,74.42246:19.81669,74.39795:19.81071,74.38971:19.79917,74.38341:19.77663,74.36695:19.75406,74.35239:19.73311,74.32995:19.70588,74.30679:19.69532,74.29804:19.68594,74.29665:19.65018,74.28873:19.62674,74.28452:19.60579,74.27542:19.5903,74.26674:19.58778,74.26077:19.58742,74.24439:19.57149,74.21543:19.53864,74.20449:19.50926,74.20035:19.47828,74.20049:19.47311,74.2047:19.46129,74.20505:19.44524,74.20032:19.43953,74.19855:19.44088,74.20081:19.43546,74.2033:19.42964,74.2008:19.42516,74.20511:19.39839,74.21254:19.36674,74.20362:19.3589,74.20667:19.34823,74.20541:19.33906,74.20227:19.3268,74.18909:19.32213,74.18036:19.30879,74.1723:19.29425,74.16973:19.29391,74.16368:19.27128,74.15001:19.25296,74.14498:19.22162,74.1259:19.19473,74.10775:19.16221,74.06177:19.14591,74.02854:19.12953,73.98877:19.12476,73.97967:19.1155,73.97341:19.09661,73.97072:19.04925,73.95613:19.00027,73.94421:18.96806,73.94517:18.95342,73.93909:18.94496,73.94095:18.92739,73.92575:18.89988,73.91119:18.8986,73.90653:18.85849,73.88901:18.83914,73.88058:18.80296,73.8721:18.79303,73.86743:18.78274,73.87264:18.76008,73.85899:18.73087,73.85601:18.70187,73.8481:18.65207,73.84978:18.63475,73.84981:18.61611,73.84832:18.61903,73.82981:18.60721,73.82241:18.57377,73.83665:18.55222,73.84947:18.5378,73.85062:18.52908,73.8525:18.52066,73.85674");
		
		request.addProperty(UserName);

		PropertyInfo Password = new PropertyInfo();
		Password.setName(ParameterUtil.Password);
		Password.setValue(pwd);
		request.addProperty(Password);
	
		
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try 
		{
			androidHttpTransport.call(SOAP_ACTION, envelope);
		//	LoggerUtil.e("Response","IN TRY");
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			String	res = response.toString();
			//LoggerUtil.e("Response", res+"");
			return res;
		} 
		catch (IOException  e) 
		{
			LoggerUtil.e("IOException", e.toString());
	
		} catch (XmlPullParserException e)
		{
			// TODO Auto-generated catch block
			LoggerUtil.e("Exception", e.toString());
		}
		return "[{\"result\":\"No Record Found\"}]";
		}
	Handler handler = new Handler()
	{
		public void handleMessage(android.os.Message msg)
		{
		if (msg.what == 0) 
			{
				Toast.makeText(getApplicationContext(),"Invalid Username Password!", 1000).show();
			}
		};
	};
		
	public String getEmployeeId()
	{
		SharedPreferences userfile=getSharedPreferences(SharedPreference.SERVICE_FLOW_PREF_FILE,MODE_MULTI_PROCESS);
		String userId=userfile.getString(SharedPreference.USER_ID,"0");
		return userId;
	}

	public void setEmployeeId(String userId)
	{
		SharedPreferences userfile= getSharedPreferences(SharedPreference.SERVICE_FLOW_PREF_FILE,MODE_MULTI_PROCESS);
		userfile.edit().putString(SharedPreference.USER_ID, userId).commit();
	}
	public void setEmployeeUserName(String userId)
	{
		SharedPreferences userfile= getSharedPreferences(SharedPreference.SERVICE_FLOW_PREF_FILE,MODE_MULTI_PROCESS);
		userfile.edit().putString(SharedPreference.EMPLOYEE_USER_NAME, userId).commit();
	}
}

