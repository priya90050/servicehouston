package quacito.houston.Flow;

//import android.support.v7.app.ActionBarActivity;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;
import quacito.houston.CommonUtilities.CommonFunction;
import quacito.houston.CommonUtilities.LoggerUtil;
import quacito.houston.CommonUtilities.ParameterUtil;
import quacito.houston.CommonUtilities.SharedPreference;
import quacito.houston.DBhelper.DatabaseHelper;
import quacito.houston.DBhelper.HoustonFlowFunctions;
import quacito.houston.Flow.HR_PestControlServiceReport.OnButtonClick;
import quacito.houston.adapter.GalleryImageAdapter;
import quacito.houston.horizontallistview.HorizontalListView;
import quacito.houston.model.ChemicalDto;
import quacito.houston.model.CrewMemberDTO;
import quacito.houston.model.EmployeeDTO;
import quacito.houston.model.GeneralInfoDTO;
import quacito.houston.model.ImgDto;
import quacito.houston.model.LogDTO;
import quacito.houston.model.Selected_crewDTO;
import quacito.houston.model.UserDTO;
import quacito.houston.Flow.servicehouston.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.splunk.mint.Mint;

import android.R.anim;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

public class GeneralInformationActivity extends Activity {
	GeneralInfoDTO generalInfoDTO;
	int imageCount = 1;
	long mLastClickTime = 0;
	LogDTO logDTO = new LogDTO();
	CheckBox chemicalcrew;
	ArrayList<CheckBox> checkList = new ArrayList<CheckBox>();
	String status_array[], reset_reason_array[], RefSource_array[];
	HoustonFlowFunctions houstonFlowFunctions;
	LinearLayout mainLayout, mainLayout2;
	ArrayList<String> tempImagList = new ArrayList<String>();
	ArrayList<String> tempStoreImageName = new ArrayList<String>();
	static UserDTO userDTO;
	HorizontalListView imageListView;
	ArrayList<CrewMemberDTO> listCrew = new ArrayList<CrewMemberDTO>();
	int intRouteId=0;
	int intStartActivity = 0;
	Boolean isValidate = true;
	Button btnSaveAndContinue, btnCancel, btnStartTime, btnReOpen, btnCamera;
	CheckBox checkboxNoEmail, checkboxSameAddress;
	Spinner spnHowDidUHear, spnStatus, spnStatusReason, spnServicePerson;
	TextView txtLeadOnly, txtTechnicians, txtAcct, txtOrderNumber, txtName,
			 txtResetReason, txtCrewMember;
	EditText edtServiceAttribute, edtResetDetail, edtFirstName, edtLastName,
			edtEmailId, edtHomePhone, edtCellPhone, edtAccountNo,
			edtServiceTechNumber, edtdateofService, edttimeofService,
			edtRouteName, edtRouteNumber, edtOrderNo, edtKeymap,
			edtServiceCode, edtDepartment, edtCompanyName, edtNotes,
			edtServiceDescription, edtOtherInstruction, edtSpecialinstructions,
			edtTimein, edtServiceAddressLine1, edtServiceAddressLine2,
			edtServiceCity, edtServiceState, edtServiceZipCode,
			edtBillAddressLine1, edtBillAddressLine2, edtBillCity,
			edtBillState, edtBillZipCode, edtBillFirstName, edtBillLastName,
			edtServiceAddress, edtBillingAddress;
	// strings to get edit text
	String strServiceAttribute, strResetDetail, strCrewMemId, strServicestatus,
			strmultipleImageName, strTimeInDateTime = "", strFirstName,
			strLastName, strEmail, strHomePhone, strCellphone,
			strAccountnumber, strTimeIn, strServiceTechNumber,
			strdateofService, strtimeofService, strRouteName, strRouteNumber,
			strOrderNo, strKeymap, strServiceCode, strDepartment,
			strCompanyName, strNotes, strServiceDescription,
			strOtherInstruction, strSpecialinstructions, strServiceAddress,
	        strBillingAddress, strServiceStatusReason, strServiceAddress1,
			strServiceAdderss2, strServiceCity, strServiceState,
			strServiceZipCode, strBillAddress1, strBillAddress2, strBillCity,
			strBillState, strBillZipCode, strNewEmail;

	EditText objectFocus;
	// strings to get spinner value
	String strTaxValue, strtaxCode, strHowDidUHear, strStatus, strResetReason,
			strServicePerson, strPestPeckId, strServiceIdNew;
	TableRow resetRow, tblRow, resetDetailRow;
	List<String> listCrewIdByEdittext = new ArrayList<String>();
	
	int tableI = 0;

	TableLayout tb;
	TableRow tr1, tr2;
	ArrayList<TableRow> trlist = new ArrayList<TableRow>();

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setDisplayShowHomeEnabled(true);

		setContentView(R.layout.activity_general_information);
		Thread.setDefaultUncaughtExceptionHandler(new TopExceptionHandler(this));
		Mint.initAndStartSession(GeneralInformationActivity.this, "e53b080c");
		houstonFlowFunctions = new HoustonFlowFunctions(getApplicationContext());
		strServiceIdNew = getIntent().getStringExtra("serviceIdNew");

		generalInfoDTO = new Gson().fromJson(houstonFlowFunctions
				.getSelectedService(strServiceIdNew).toString(),
				GeneralInfoDTO.class);

		userDTO = (UserDTO) getIntent().getExtras().getSerializable("userDTO");
		initailize();
		if (generalInfoDTO != null) {
			JSONObject commercial_pest_string = HoustonFlowFunctions
					.getCommercialReport(generalInfoDTO.getServiceID_new());

			Log.e("commercial_pest_string string.. to string 1..",
				  "commercial_pest_string string..." + commercial_pest_string);
			getAllValuesFromObject();
			setAllvaluestoEditText();
			RefreshImage();
			reloadPriviousBeforeImage();
			generateCrewMembers();

			if (generalInfoDTO.getServiceStatus().equals("Complete")) {
				spnStatus.setEnabled(false);
				btnStartTime.setEnabled(false);
				spnHowDidUHear.setEnabled(false);
				btnCamera.setEnabled(false);
			}
			if (generalInfoDTO.getServiceStatus().equals("Reset")) {
				spnStatus.setEnabled(false);
				spnStatusReason.setEnabled(false);
				edtResetDetail.setEnabled(false);
				btnStartTime.setEnabled(false);
				spnHowDidUHear.setEnabled(false);
				btnCamera.setEnabled(false);
			}
		}
	}

	private void generateCrewMembers() {
		// TODO Auto-generated method stub
		intStartActivity = 1;
		JSONArray arrayRecords = houstonFlowFunctions.getCrewMembers(
				DatabaseHelper.TABLE_CREW_MEMBER, strServiceIdNew);
		listCrew.clear();
		listCrew = new Gson().fromJson(arrayRecords.toString(),
				new TypeToken<List<CrewMemberDTO>>() {
				}.getType());
		
		if (listCrew.size() > 0 && listCrew.get(0).getCrewMember_ID().equals("0")) {
			Log.e("listCrew.size().if.",
					  "" + listCrew.size());
			tblRow.setVisibility(View.VISIBLE);
			txtLeadOnly.setVisibility(View.VISIBLE);

		} else if ((listCrew.size() > 0 && !(listCrew.get(0).getCrewMember_ID() == "0"))) 
		{
			tblRow.setVisibility(View.VISIBLE);
			
			for (int i = 0; i < listCrew.size(); i++) {
				
				AddCrew(i);
			}
		} else {
			tblRow.setVisibility(View.GONE);
		}
	}

	
	private void AddCrew(final int listIndex) {
		// TODO Auto-generated method stub
				
		if (tableI == 0) {
			tb = new TableLayout(GeneralInformationActivity.this);
			tb.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT));

			Log.e("AddCrew...","" +tableI);
		    mainLayout.removeAllViews();
			mainLayout.addView(tb);
			
		}
		tableI = tableI + 1;
		tr1 = new TableRow(GeneralInformationActivity.this);
		trlist.add(tr1);
		tr1.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT));

		tr2 = new TableRow(GeneralInformationActivity.this);
		trlist.add(tr2);
		tr2.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT));

		txtCrewMember = new TextView(GeneralInformationActivity.this);
		txtCrewMember.setGravity(Gravity.LEFT);
		txtCrewMember.setText("Crew Member:");
		txtCrewMember.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
		// txtCrewMember.setT
		txtCrewMember.setTextColor(Color.BLACK);
		// txtCrewMember.setPadding(5, 5, 5, 5);
		txtCrewMember.setTextAppearance(getApplicationContext(),
				R.style.text_view_form);
		txtCrewMember.setGravity(Gravity.CENTER_HORIZONTAL
				| Gravity.CENTER_VERTICAL);

		tr1.addView(txtCrewMember);
		// mainLayout.addView(txtCrewMember);

		chemicalcrew = new CheckBox(GeneralInformationActivity.this);
		checkList.add(listIndex, chemicalcrew);
		chemicalcrew.setTag(Integer.valueOf(tableI));
		chemicalcrew.setText(listCrew.get(listIndex).getTech_Name());
		chemicalcrew.setId(tableI + 10000);
		// chemicalcheckBox.setBackgroundResource(R.drawable.checked);
		chemicalcrew.setGravity(Gravity.CENTER_HORIZONTAL
				| Gravity.CENTER_VERTICAL);
		// Log.e("", listCrew.get(listIndex).getIsChecked());
		if (CommonFunction.checkString(generalInfoDTO.getTimeIn(), "").equals(
				"")) {
			int i = houstonFlowFunctions.updateCrewTable("true",
					listCrew.get(listIndex).getServiceID_new());

			checkList.get(listIndex).setChecked(true);
		} else {
			// 0 matlab false
			// 1 matlab true
			if (listCrew.get(listIndex).getIsChecked().equals("false")) {
				checkList.get(listIndex).setChecked(false);
			} else {

				checkList.get(listIndex).setChecked(true);
			}
		}

		if (generalInfoDTO.getServiceStatus().equals("Complete")) {
			chemicalcrew.setEnabled(false);
		}
		chemicalcrew.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
		   public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked) {
					int i = houstonFlowFunctions.updateCrewTable("true",
							listCrew.get(listIndex).getServiceID_new());
					// Log.e("Update crew if", i+"");
				} else {
					int i = houstonFlowFunctions.updateCrewTable("false",
							listCrew.get(listIndex).getServiceID_new());
					// Log.e("Update crew else", i+"");
				}
			}

		});

		tr2.addView(chemicalcrew);
		// mainLayout2.addView(chemicalcrew);

		tb.addView(tr1);
		tb.addView(tr2);
	}

	/*
	 * public class OnCheckedboxChangeListener implements
	 * android.widget.CompoundButton.OnCheckedChangeListener {
	 * 
	 * @Override public void onCheckedChanged(CompoundButton buttonView,boolean
	 * isChecked) { // TODO Auto-generated method stub switch
	 * (buttonView.getId()) { case R.id.checkBoxNoConductive: break;
	 * 
	 * default: break; } }
	 */

	public void reloadPriviousBeforeImage() {
		String imageNames = generalInfoDTO.getBeforeImage();

		if (!CommonFunction.checkString(imageNames, "").equals("")) {
			String[] imageArray = imageNames.split(",");
			for (int i = 0; i < imageArray.length; i++) {
				if (!imageArray[i].equals("")) {
					tempStoreImageName.add(imageArray[i]);
					RefreshImage();
				}
			}
		}
	}

	private void initailize() {
		// TODO Auto-generated method stub
		// generalInfoDTO= new GeneralInfoDTO();
		// Log.e(userDTO.getEmailid(), userDTO.getEmailid());

		resetDetailRow = (TableRow) findViewById(R.id.resetDetailRow);
		resetRow = (TableRow) findViewById(R.id.resetRow);
		tblRow = (TableRow) findViewById(R.id.tblRow);
		
		mainLayout = (LinearLayout) findViewById(R.id.mainLayout1);
		mainLayout2 = (LinearLayout) findViewById(R.id.mainLayout2);
		btnSaveAndContinue = (Button) findViewById(R.id.btnsaveandcontinue);
		btnSaveAndContinue.setOnClickListener(new OnButtonClick());
		btnCancel = (Button) findViewById(R.id.btnCancel);
		btnCancel.setOnClickListener(new OnButtonClick());
		btnStartTime = (Button) findViewById(R.id.btnSratTime);
		btnStartTime.setOnClickListener(new OnButtonClick());
		btnCamera = (Button) findViewById(R.id.btnCamera);
		btnCamera.setOnClickListener(new OnButtonClick());

		// spinner initialize
		spnHowDidUHear = (Spinner) findViewById(R.id.spnHowDidYouHearAbout);
		spnStatusReason = (Spinner) findViewById(R.id.spnServiceStatusReason);
		spnStatusReason.setOnItemSelectedListener(new OnSpinerItemSelect());

		spnStatus = (Spinner) findViewById(R.id.spnStatus);
		spnStatus.setOnItemSelectedListener(new OnSpinerItemSelect());

		spnServicePerson = (Spinner) findViewById(R.id.spinnerServicePerson);
		spnServicePerson.getSelectedView();
		spnServicePerson.setEnabled(false);

		imageListView = (HorizontalListView) findViewById(R.id.listImg);
		// Log.e("initialize", "imageNames.."+imageNames);

		imageListView.setOnItemLongClickListener(new OnGalleryClick());
		imageListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Bitmap bmp = listImg.get(position).getImageBitMap();
				if (bmp != null) {
					bitmap = bmp;
					Intent intent = new Intent(GeneralInformationActivity.this,
							FullScreenViewActivity.class);
					intent.putExtra("from", "general");
					startActivity(intent);
				}
			}

		});

		// checkbox initialize
		checkboxNoEmail = (CheckBox) findViewById(R.id.checkboxNoEmail);
		checkboxNoEmail
		.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		
		checkboxSameAddress = (CheckBox) findViewById(R.id.checkBoxSameAddress);
		checkboxSameAddress
				.setOnCheckedChangeListener(new OnCheckedboxChangeListener());
		// textview initialize
		// txtTechnicians=(TextView)findViewById(R.id.txtTechnicians);
		txtAcct = (TextView) findViewById(R.id.txtAccountNumber);
		txtOrderNumber = (TextView) findViewById(R.id.txtOrderNumber);
		txtName = (TextView) findViewById(R.id.txttechName);
		txtLeadOnly = (TextView) findViewById(R.id.txtLeadOnly);
		// edit text initialize
		edtFirstName = (EditText) findViewById(R.id.edtFirstName);
		edtLastName = (EditText) findViewById(R.id.edtLastName);
		
		edtEmailId = (EditText) findViewById(R.id.edtEmailId);
		edtEmailId.addTextChangedListener(new TextWatcher() {
			  
			  @Override public void onTextChanged(CharSequence s, int start, int
			  before, int count) {
				  strNewEmail =  s.toString();
				  if(count<=0)
				  {
					  checkboxNoEmail.setChecked(true);
				  }
				  else
				  {
					  checkboxNoEmail.setChecked(false); 
				  }
			  }
			  
			  @Override public void beforeTextChanged(CharSequence s, int start,
			  int count, int after) { // TODO Auto-generated method stub
			  
			 }
			  
			  @Override public void afterTextChanged(Editable s) { // TODO
			  
			  }
		});
		
		
		edtHomePhone = (EditText) findViewById(R.id.edtHOME_PHONE);
		/*
		 edtHomePhone.addTextChangedListener(new TextWatcher() {
		  
		  @Override public void onTextChanged(CharSequence s, int start, int
		  before, int count) {
		  
		  }
		  
		  @Override public void beforeTextChanged(CharSequence s, int start,
		  int count, int after) { // TODO Auto-generated method stub
		  
		 }
		  
		  @Override public void afterTextChanged(Editable s) { // TODO
		  Auto-generated method stub if (s.length() == 3 || s.length() == 8) {
		  s.append('-');
		  
		  }
		  
		  } });
		 */
		edtCellPhone = (EditText) findViewById(R.id.edtCELL_PHONE);

		/*
		 * edtCellPhone.addTextChangedListener(new TextWatcher() {
		 * 
		 * @Override public void onTextChanged(CharSequence s, int start, int
		 * before, int count) {
		 * 
		 * }
		 * 
		 * @Override public void beforeTextChanged(CharSequence s, int start,
		 * int count, int after) { // TODO Auto-generated method stub
		 * 
		 * }
		 * 
		 * @Override public void afterTextChanged(Editable s) { // TODO
		 * Auto-generated method stub { if (s.length() == 3 || s.length() == 7)
		 * s.append('-'); } } }); edtCellPhone.setOnKeyListener(new
		 * OnKeyListener() { public boolean onKey(View v, int keyCode, KeyEvent
		 * event) {
		 * 
		 * if (event.getAction() == KeyEvent.ACTION_UP) {
		 * 
		 * Log.e("", "keyCode"+keyCode+"..."+KeyEvent.KEYCODE_DEL); //check if
		 * the right key was pressed if (keyCode == KeyEvent.KEYCODE_DEL) {
		 * edtCellPhone.getText().clear(); return true; } //} return false; }
		 * });
		 */
		edtServiceAttribute = (EditText) findViewById(R.id.edtServiceAttribute);
		edtResetDetail = (EditText) findViewById(R.id.edtResetDetail);
		edtResetDetail.setFilters(CommonFunction.limitchars(150));
		edtServiceAddress = (EditText) findViewById(R.id.edtServiceAddress);
		edtBillingAddress = (EditText) findViewById(R.id.edtBillingAddress);
		edtAccountNo = (EditText) findViewById(R.id.edtAccountNo);
		edtServiceTechNumber = (EditText) findViewById(R.id.edtServiceTech);
		edtdateofService = (EditText) findViewById(R.id.edtDateService);
		edtdateofService.setInputType(InputType.TYPE_NULL);
		edtdateofService.setOnClickListener(new OnButtonClick());
		edttimeofService = (EditText) findViewById(R.id.edtTimeofService);
		edttimeofService.setInputType(InputType.TYPE_NULL);
		edttimeofService.setOnClickListener(new OnButtonClick());
		edtRouteName = (EditText) findViewById(R.id.edtRouteName);
		edtRouteName.addTextChangedListener(new TextWatcher() {
			  
			  @Override public void onTextChanged(CharSequence s, int start, int
			  before, int count) {
			     edtRouteNumber.setText(s);
			     
			     if(intStartActivity > 0)
			     { 
			     if(count >= 4)
			     {
			    	 listCrewIdByEdittext.clear();
			        intRouteId = houstonFlowFunctions.getRouteId_Tbl_GetAllRoute(s.toString());	
			        if(intRouteId > 0)
			        {
			        int AssignForDayLead_ID =houstonFlowFunctions.getAssignedLeadForDay(String.valueOf(intRouteId), generalInfoDTO.getServiceDate(),generalInfoDTO.getSalesperson_ID());
			        if(AssignForDayLead_ID == 0)
			        {
			    	  int iCheckDefaultLead =houstonFlowFunctions.checkInRouteMaster(String.valueOf(intRouteId),generalInfoDTO.getSalesperson_ID());
			          if(iCheckDefaultLead != 0)
			          {
			        	//now techs are crew
			        	listCrewIdByEdittext = houstonFlowFunctions.getAllCrewByRouteId(String.valueOf(intRouteId));
			          }
			        
			        }
			        else
			        {
			        	 listCrewIdByEdittext = houstonFlowFunctions.getAssignedCrewByAssignForDayLead_ID(String.valueOf(AssignForDayLead_ID));
			        }
			        
			        }
			        
			     }
			     generateCrewByEdittext();
			     }
			  }
			  
			  @Override public void beforeTextChanged(CharSequence s, int start,
			  int count, int after) { // TODO Auto-generated method stub
			  
				  
				  
			  }
			  
			  @Override public void afterTextChanged(Editable s) { // TODO
			  
			  }
			  
			  
         });
		
		
		
		edtRouteNumber = (EditText) findViewById(R.id.edtRouteNumber);
		edtOrderNo = (EditText) findViewById(R.id.edtOrderNo);
		edtKeymap = (EditText) findViewById(R.id.edtKeyMap);
		edtServiceCode = (EditText) findViewById(R.id.edtServiceCode);
		edtDepartment = (EditText) findViewById(R.id.edtDepartment);
		edtTimein = (EditText) findViewById(R.id.edtTimeIn);
		edtNotes = (EditText) findViewById(R.id.edtNotes);
		edtCompanyName = (EditText) findViewById(R.id.edtCompanyName);
		edtSpecialinstructions = (EditText) findViewById(R.id.edtSpecialinstructions);
		edtOtherInstruction = (EditText) findViewById(R.id.edtOtherInstruction);
		edtServiceDescription = (EditText) findViewById(R.id.edtServiceDes);

		edtServiceAddressLine1 = (EditText) findViewById(R.id.edtServiceAddLine1);
		edtServiceAddressLine2 = (EditText) findViewById(R.id.edtServiceAddLine2);
		edtServiceCity = (EditText) findViewById(R.id.edtServiceCity);
		edtServiceState = (EditText) findViewById(R.id.edtServiceState);
		edtServiceZipCode = (EditText) findViewById(R.id.edtServiceZipCode);
		edtBillAddressLine1 = (EditText) findViewById(R.id.edtBillAddLine1);
		edtBillAddressLine2 = (EditText) findViewById(R.id.edtBillAddlines2);
		edtBillCity = (EditText) findViewById(R.id.edtBillCity);
		edtBillState = (EditText) findViewById(R.id.edtBillState);
		edtBillZipCode = (EditText) findViewById(R.id.edtBillZipCode);
		edtBillFirstName = (EditText) findViewById(R.id.edtBillfirstName);
		edtBillLastName = (EditText) findViewById(R.id.edtBilllastName);
		// edtServiceAddress=(EditText)findViewById(R.id.edtServiceAddress);
		// edtBillingAddress=(EditText)findViewById(R.id.edtBillingAddress);
	}

	protected void generateCrewByEdittext() 
	{
		
	  //.listCrewIdByEdittext // SELECT * FROM users where id in (1,2,3,4,5);
		String strCrewIds = "";
		if(listCrewIdByEdittext.size() > 0)
		{
		for (Iterator<String> it = listCrewIdByEdittext.iterator() ; it.hasNext() ; ){
			strCrewIds += it.next();
		  if (it.hasNext()) {
			  strCrewIds += ", "; 
		  }
		  
		}
				
		   List<String> listTechName = houstonFlowFunctions.getCrewNameFromEmpTbl(strCrewIds);
		   Log.e("listTechName", ""+listTechName.size());
		   Log.e("listCrewIdByEdittext", ""+listCrewIdByEdittext.size());
		   
		   houstonFlowFunctions.deleteAllTableData(DatabaseHelper.TABLE_CREW_MEMBER);
		   
		   for(int i=0; i< listTechName.size() ; i++)
		   {
		     CrewMemberDTO crewMemberDTO = new CrewMemberDTO();
		     crewMemberDTO.setCrewMember_ID(listCrewIdByEdittext.get(i));
		     crewMemberDTO.setServiceID_new(strServiceIdNew);
		     crewMemberDTO.setTech_Name(listTechName.get(i));
		     crewMemberDTO.setRoute_ID(String.valueOf(intRouteId));
		     
		     long crewInsert = houstonFlowFunctions.inserCrewDynamically(crewMemberDTO);
		     Log.e("crewInsert", ""+crewInsert);
		     // listCrew.add(crewMemberDTO);
		     
		   }
		   trlist.clear();
		   tableI=0;
		   generateCrewMembers();
		}
		else
		{
			 houstonFlowFunctions.deleteAllTableData(DatabaseHelper.TABLE_CREW_MEMBER);
			 trlist.clear();
			 tableI=0;
			 generateCrewMembers();	
		}
	}

	public void getAllValuesFromObject() {

		strServiceAttribute = generalInfoDTO.getService_Attribute();
		strResetDetail = generalInfoDTO.getResetComment();
		strFirstName = generalInfoDTO.getFirst_Name();
		strLastName = generalInfoDTO.getLast_Name();
		strEmail = generalInfoDTO.getEmail_Id();
		strHomePhone = generalInfoDTO.getHome_Phone();
		strCellphone = generalInfoDTO.getCell_Phone();
		strAccountnumber = generalInfoDTO.getAccountNo();
		strServiceTechNumber = generalInfoDTO.getService_Tech_Num();
		strdateofService = generalInfoDTO.getServiceDate();
		strtimeofService = generalInfoDTO.getServiceTime();
		strRouteName = generalInfoDTO.getRoute_Name();
		strRouteNumber = generalInfoDTO.getRoute_Number();
		strOrderNo = generalInfoDTO.getOrder_Number();
		strKeymap = generalInfoDTO.getKeyMap();
		strServiceCode = generalInfoDTO.getServiceCode();
		strDepartment = generalInfoDTO.getDepartment();
		strCompanyName = generalInfoDTO.getCompanyName();
		strNotes = generalInfoDTO.getNotes();
		strServiceDescription = generalInfoDTO.getServiceDescription();
		strOtherInstruction = generalInfoDTO.getOtherInstruction();
		strSpecialinstructions = generalInfoDTO.getSpecialInstruction();
		strStatus = generalInfoDTO.getServiceStatus();
		strServiceStatusReason = generalInfoDTO.getService_Status_Reason();
		// strServicePerson=generalInfoDTO.getServicePerson();
		strTimeIn = generalInfoDTO.getTimeIn();
		strTimeInDateTime = generalInfoDTO.getTimeInDateTime();
		strServicePerson = generalInfoDTO.getFullName();
		strDepartment = generalInfoDTO.getDepartment();
		// Log.e("", generalInfoDTO.getTaxCode()+"tax code..");

		strServiceAddress1 = generalInfoDTO.getAddress_Line1();
		strServiceAdderss2 = generalInfoDTO.getAddress_Line2();
		strServiceCity = generalInfoDTO.getCity();
		strServiceState = generalInfoDTO.getState();
		strServiceZipCode = generalInfoDTO.getZipCode();

		// strBillFirstName=generalInfoDTO.getBillingFName();
		// strBillLastName=generalInfoDTO.getBillingLName();
		strBillAddress1 = generalInfoDTO.getBAddress_Line1();
		strBillAddress2 = generalInfoDTO.getBAddress_Line2();
		strBillCity = generalInfoDTO.getBCity();
		strBillState = generalInfoDTO.getBState();
		strBillZipCode = generalInfoDTO.getBZipCode();
		if (!CommonFunction.checkString(generalInfoDTO.getAddress_Line1(), "")
				.equals("")) {
			strServiceAddress = generalInfoDTO.getAddress_Line1() + ", ";
		}
		if (!CommonFunction.checkString(generalInfoDTO.getAddress_Line2(), "")
				.equals("")) {
			strServiceAddress = strServiceAddress
					+ generalInfoDTO.getAddress_Line2() + ", ";
		}
		if (!CommonFunction.checkString(generalInfoDTO.getCity(), "")
				.equals("")) {
			strServiceAddress = strServiceAddress + generalInfoDTO.getCity()
					+ ", ";
		}
		if (!CommonFunction.checkString(generalInfoDTO.getState(), "").equals(
				"")) {
			strServiceAddress = strServiceAddress + generalInfoDTO.getState()
					+ " ";
		}
		if (!CommonFunction.checkString(generalInfoDTO.getZipCode(), "")
				.equals("")) {
			strServiceAddress = strServiceAddress + generalInfoDTO.getZipCode();
		}
		// strServiceAddress=generalInfoDTO.getAddress_Line1()+","+generalInfoDTO.getAddress_Line2()+","+generalInfoDTO.getCity()+","+generalInfoDTO.getState()+","+generalInfoDTO.getZipCode();
		if (!CommonFunction.checkString(generalInfoDTO.getBAddress_Line1(), "")
				.equals("")) {
			strBillingAddress = generalInfoDTO.getBAddress_Line1() + ", ";
		}
		if (!CommonFunction.checkString(generalInfoDTO.getBAddress_Line2(), "")
				.equals("")) {
			strBillingAddress = strBillingAddress
					+ generalInfoDTO.getBAddress_Line2() + ", ";
		}
		if (!CommonFunction.checkString(generalInfoDTO.getBCity(), "").equals(
				"")) {
			strBillingAddress = strBillingAddress + generalInfoDTO.getBCity()
					+ ", ";
		}
		if (!CommonFunction.checkString(generalInfoDTO.getBState(), "").equals(
				"")) {
			strBillingAddress = strBillingAddress + generalInfoDTO.getBState()
					+ " ";
		}
		if (!CommonFunction.checkString(generalInfoDTO.getBZipCode(), "")
				.equals("")) {
			strBillingAddress = strBillingAddress
					+ generalInfoDTO.getBZipCode();
		}
	}

	public void setAllvaluestoEditText() {
		edtServiceAttribute.setText(strServiceAttribute);
		edtResetDetail.setText(strResetDetail);
		edtFirstName.setText(strFirstName);
		edtLastName.setText(strLastName);
		if (strEmail.equals("")) {
			checkboxNoEmail.setChecked(true);
		} else {
			edtEmailId.setText(strEmail);
		}

		edtHomePhone.setText(strHomePhone);
		edtCellPhone.setText(strCellphone);
		edtAccountNo.setText(strAccountnumber);
		edtServiceTechNumber.setText(strServiceTechNumber);
		edtdateofService.setText(strdateofService);
		edttimeofService.setText(strtimeofService);
		edtRouteName.setText(strRouteName);
		edtRouteNumber.setText(strRouteNumber);
		edtOrderNo.setText(strOrderNo);
		edtKeymap.setText(strKeymap);
		edtServiceCode.setText(strServiceCode);
		edtDepartment.setText(strDepartment);
		edtCompanyName.setText(strCompanyName);
		edtNotes.setText(strNotes);
		edtServiceDescription.setText(strServiceDescription);
		edtOtherInstruction.setText(strOtherInstruction);
		edtSpecialinstructions.setText(strSpecialinstructions);
		edtServiceAddressLine1.setText(strServiceAddress1);
		edtServiceAddressLine2.setText(strServiceAdderss2);
		edtServiceCity.setText(strServiceCity);
		edtServiceState.setText(strServiceState);
		edtServiceZipCode.setText(strServiceZipCode);
		edtBillAddressLine1.setText(strBillAddress1);
		edtBillAddressLine2.setText(strBillAddress2);
		edtBillCity.setText(strBillCity);
		edtBillState.setText(strBillState);
		edtBillZipCode.setText(strBillZipCode);
		edtBillFirstName.setText(strFirstName);
		edtBillLastName.setText(strLastName);
		// edtTimein.setText(strtimeofService);
		edtTimein.setText(strTimeInDateTime);
		txtAcct.setText(strAccountnumber);
		txtOrderNumber.setText(strOrderNo);
		txtName.setText(strFirstName + strLastName);
		edtServiceAddress.setText(strServiceAddress);
		edtBillingAddress.setText(strBillingAddress);

		// LoggerUtil.e("generalInfoDTO.getRef_Source()",generalInfoDTO.getRef_Source());
		RefSource_array = getResources().getStringArray(
				R.array.hear_about_array);
		ArrayAdapter<String> RefSourceAdapter = new ArrayAdapter<String>(
				GeneralInformationActivity.this,
				android.R.layout.simple_spinner_dropdown_item, RefSource_array);
		spnHowDidUHear.setAdapter(RefSourceAdapter);
		List<String> RefSource_arrayList = Arrays.asList(RefSource_array);
		spnHowDidUHear.setSelection(RefSource_arrayList.indexOf(generalInfoDTO
				.getRef_Source()));

		if (strStatus.equals("Complete")) {
			status_array = getResources().getStringArray(
					R.array.status_array_complete);
		} else {
			status_array = getResources().getStringArray(
					R.array.status_array_incomplete);
		}

		ArrayAdapter<String> statusAdapter = new ArrayAdapter<String>(
				GeneralInformationActivity.this,
				android.R.layout.simple_spinner_dropdown_item, status_array);
		spnStatus.setAdapter(statusAdapter);
		List<String> status_arrayList = Arrays.asList(status_array);
		spnStatus.setSelection(status_arrayList.indexOf(strStatus));

		if (strStatus.equals("Reset")) {
			reset_reason_array = getResources().getStringArray(
					R.array.reset_reason);
			ArrayAdapter<String> statusReasonAdapter = new ArrayAdapter<String>(
					GeneralInformationActivity.this,
					android.R.layout.simple_spinner_dropdown_item,
					reset_reason_array);
			spnStatusReason.setAdapter(statusReasonAdapter);
			List<String> status_reset_arrayList = Arrays
					.asList(reset_reason_array);
			spnStatusReason.setSelection(status_reset_arrayList
					.indexOf(strServiceStatusReason));
		}
		EmployeeDTO selectedEmployeeDTO = houstonFlowFunctions
				.getSelectedEmploye(generalInfoDTO.getSalesperson_ID());
		// LoggerUtil.e("selectedEmployeeDTO","selectedEmployeeDTO.."+selectedEmployeeDTO);
		spnServicePerson.setAdapter(CommonFunction
				.setEmployenameList(GeneralInformationActivity.this));

		if (selectedEmployeeDTO != null) {
			strPestPeckId = selectedEmployeeDTO.getPestPackId();
			int indexOfPestPeck = CommonFunction.getemployeeNameList(
					GeneralInformationActivity.this).indexOf(
					selectedEmployeeDTO.getEmpName());
			// LoggerUtil.e("index of pest peck id",String.valueOf(indexOfPestPeck)+" "+selectedEmployeeDTO.getEmpName());
			spnServicePerson.setSelection(indexOfPestPeck);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.general_information, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		/*
		 * if (id == R.id.action_back) { //Intent intent=new
		 * Intent(GeneralInformationActivity.this, ServiceListAcitivity.class);
		 * //Log.e("", ""+serviceList.get(arg2)); //intent.putExtra("userDTO",
		 * ServiceListAcitivity.userDTO); //startActivity(intent); finish();
		 * return true; }
		 */

		if (id == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}

	private class OnCheckedboxChangeListener implements
			android.widget.CompoundButton.OnCheckedChangeListener {
		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			switch (buttonView.getId()) {
			case R.id.checkboxNoEmail:
				if (checkboxNoEmail.isChecked())
					edtEmailId.getText().clear();
				
			case R.id.checkBoxSameAddress:
				if (checkboxSameAddress.isChecked()) {
					edtBillFirstName.setEnabled(false);
					edtBillFirstName.setText(generalInfoDTO.getFirst_Name());

					edtBillLastName.setEnabled(false);
					edtBillLastName.setText(generalInfoDTO.getLast_Name());

					edtBillAddressLine1.setEnabled(false);
					edtBillAddressLine1.setText(generalInfoDTO
							.getAddress_Line1());

					edtBillAddressLine2.setEnabled(false);
					edtBillAddressLine2.setText(generalInfoDTO
							.getAddress_Line2());

					edtBillCity.setEnabled(false);
					edtBillCity.setText(generalInfoDTO.getCity());

					edtBillState.setEnabled(false);
					edtBillState.setText(generalInfoDTO.getState());

					edtBillZipCode.setEnabled(false);
					edtBillZipCode.setText(generalInfoDTO.getZipCode());
				} else {
					edtBillFirstName.setEnabled(true);
					edtBillFirstName.setText(generalInfoDTO.getFirst_Name());

					edtBillLastName.setEnabled(true);
					edtBillLastName.setText(generalInfoDTO.getLast_Name());

					edtBillAddressLine1.setEnabled(true);
					edtBillAddressLine1.setText(generalInfoDTO
							.getBAddress_Line1());

					edtBillAddressLine2.setEnabled(true);
					edtBillAddressLine2.setText(generalInfoDTO
							.getBAddress_Line2());

					edtBillCity.setEnabled(true);
					edtBillCity.setText(generalInfoDTO.getBCity());

					edtBillState.setEnabled(true);
					edtBillState.setText(generalInfoDTO.getBState());

					edtBillZipCode.setEnabled(true);
					edtBillZipCode.setText(generalInfoDTO.getBZipCode());
				}

			}
		}
	}

	String ImageName;
	public static Bitmap bitmap;
	GalleryImageAdapter adapter;
	ArrayList<ImgDto> listImg = new ArrayList<ImgDto>();
	public static int REQUEST_Camera_ACTIVITY_CODE = 101;

	public void takePicture() {
		if (tempStoreImageName.size() != 0) {
			Character character = tempStoreImageName.get(
					tempStoreImageName.size() - 1).charAt(0);
			LoggerUtil.e("character", "" + character);

			int firstChar = Character.getNumericValue(character);

			LoggerUtil.e("firstChar", "" + firstChar);
			int addOne = firstChar + 1;

			LoggerUtil.e("addOne", "" + addOne);
			imageCount = addOne;
		}
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		ImageName = imageCount + generalInfoDTO.getServiceID_new() + "_"
				+ "before.jpg"; // CommonFunction.CreateImageName();
		Uri imageUri = Uri.fromFile(CommonFunction.getFileLocation(
				GeneralInformationActivity.this, ImageName));
		intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
		//intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
		//intent.putExtra(MediaStore.EXTRA_SIZE_LIMIT,
		//	Integer.toString(1024 * 1024));
		GeneralInformationActivity.this.startActivityForResult(intent,
				REQUEST_Camera_ACTIVITY_CODE);
		Log.e("takePicture image name", ImageName + "");
	}

	class OnButtonClick implements OnClickListener {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.edtDateService:
				// String strFromDate=edtFromdate.getText().toString();
				// String strToDate=edtTodate.getText().toString();
				DialogFragment TodateFragment = new DatePickerFragment(
						edtdateofService, edtdateofService, 2);
				TodateFragment.show(getFragmentManager(), "Date Picker");
				break;

			case R.id.edtTimeofService:

				Calendar mcurrentTime = Calendar.getInstance();
				int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
				int minute = mcurrentTime.get(Calendar.MINUTE);
				TimePickerDialog mTimePicker;
				mTimePicker = new TimePickerDialog(
						GeneralInformationActivity.this,
						new TimePickerDialog.OnTimeSetListener() {
							@Override
							public void onTimeSet(TimePicker timePicker,
									int selectedHour, int selectedMinute) {
								edttimeofService.setText(selectedHour + ":"
										+ selectedMinute);
							}
						}, hour, minute, true);// Yes 24 hour time
				mTimePicker.setTitle("Select Time");
				mTimePicker.show();

				break;

			case R.id.btnCamera:

				if (tempStoreImageName.size() != 3) {
					if (CommonFunction.isSdPresent()) {
						takePicture();
					} else {
						Toast.makeText(GeneralInformationActivity.this,
								"SdCard Not Present", Toast.LENGTH_SHORT)
								.show();
					}
				} else {
					Toast.makeText(GeneralInformationActivity.this,
							"Max number of upload image exceeded.",
							Toast.LENGTH_SHORT).show();
				}

				break;
			case R.id.btnsaveandcontinue:
				// variable to track event time
				/*
				 * if (SystemClock.elapsedRealtime() - mLastClickTime < 4000) {
				 * break; } mLastClickTime = SystemClock.elapsedRealtime();
				 */
				if (!strStatus.equals("Complete")) {
					getValuesFromComponents();
					setValuestoDTO();
					MandatoryList.clear();
					checkValidations();
					insertUpdateCrew();
					insertUpdateLog();
					if (isValidate) {
						if (strStatus.equals("InComplete")) {
							LoggerUtil.e("generalInfoDTO in general info?",
									"generalInfoDTO.getcheckno"
											+ generalInfoDTO.getCheckNo());

							int in = houstonFlowFunctions
									.update(generalInfoDTO);
							LoggerUtil.e("updated? in gene info", "" + in);
							LoggerUtil.e("generalInfoDTO?",
									"generalInfoDTO.getcheckno"
											+ generalInfoDTO.getCheckNo());
							Intent i = new Intent(
									GeneralInformationActivity.this,
									SelectServiceAcitvity.class);
							i.putExtra("userDTO", userDTO);
							i.putExtra("generalInfoDTO", generalInfoDTO);
							i.putExtra(ParameterUtil.ServiceID_new,
									generalInfoDTO.getServiceID_new());
							startActivity(i);
							// finish();
						} else {
							int in = houstonFlowFunctions
									.update(generalInfoDTO);
							// LoggerUtil.e("updated? in gene info",""+in);

							Intent intent = new Intent(getApplicationContext(),
									ServiceListAcitivity.class);
							intent.putExtra("userDTO",
									ServiceListAcitivity.userDTO);

							Bundle extras = new Bundle();
							extras.putString(ParameterUtil.isSync, "Yes");

							// 4. add bundle to intent
							intent.putExtras(extras);
							// intent.putExtra(ParameterUtil.isSync,isSync);
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);
							finish();
						}
					} else {
						mLastClickTime = 0;
						charSequenceItems = MandatoryList
								.toArray(new CharSequence[MandatoryList.size()]);
						// Toast.makeText(getActivity(), "MandatoryList",
						// Toast.LENGTH_LONG).show();
						createMessageList(charSequenceItems, objectFocus);
					}
				} else {
					Intent i = new Intent(GeneralInformationActivity.this,
							SelectServiceAcitvity.class);
					i.putExtra("userDTO", userDTO);
					i.putExtra("generalInfoDTO", generalInfoDTO);
					i.putExtra(ParameterUtil.ServiceID_new,
							generalInfoDTO.getServiceID_new());
					startActivity(i);
				}
				break;
			case R.id.btnSratTime:
				strTimeInDateTime = CommonFunction.getCurrentDateTime();
				strTimeIn = CommonFunction.getCurrentTime();
				edtTimein.setText(CommonFunction.getCurrentDateTime());

				break;
			case R.id.btnCancel:
				finish();
				break;
			default:
				break;
			}
		}

		private void insertUpdateLog() {
			Gson gson = new Gson();
			// TODO Auto-generated method stub
			JSONArray logArray = houstonFlowFunctions
					.getAllSelectedLogRow(strServiceIdNew);
			// Log.e("", "logArray...."+logArray);

			logDTO.setGeneralSR("True");
			logDTO.setSID(strServiceIdNew);
			String logString = gson.toJson(logDTO);
			try {
				if (logArray.length() > 0) {
					int u = HoustonFlowFunctions.updateLog(logDTO);
					LoggerUtil.e("log updated. values", "" + u);
				} else {
					long a = HoustonFlowFunctions.addLog(new JSONObject(
							logString));
					LoggerUtil.e("log added1. values", "" + a);
				}
			} catch (Exception e) {

			}
		}
	}

	private void insertUpdateCrew() {
		// TODO Auto-generated method stub
		JSONArray crewListArray = houstonFlowFunctions
				.getAllSelectedCrewMember(strServiceIdNew);
		// Log.e("", "crewListArray...."+crewListArray);
		int a = houstonFlowFunctions.deleteSelectedTableData(
				DatabaseHelper.TABLE_SELECTED_CREW_MEMBER,
				ParameterUtil.ServiceID_new, strServiceIdNew);
		// int a =
		// houstonFlowFunctions.deleteSelectedTableData(DatabaseHelper.TABLE_CREW_MEMBER,
		// ParameterUtil.ServiceID_new, strServiceIdNew);

		if (crewListArray.length() > 0) {
			ArrayList<CrewMemberDTO> crewList = new Gson().fromJson(
					crewListArray.toString(),
					new TypeToken<List<CrewMemberDTO>>() {
					}.getType());
			;
			strCrewMemId = "";
			for (int i = 0; i < crewList.size(); i++) {
				String crewId = crewList.get(i).getCrewMember_ID();
				strCrewMemId = strCrewMemId + "," + crewId;
				// houstonFlowFunctions.updateSelectedCrewStatus(strServiceIdNew,
				// crewId,"true");
			}
			strCrewMemId = CommonFunction.removeFirstCharIF(strCrewMemId, ",");

			String empId = houstonFlowFunctions.getEmpId(spnServicePerson
					.getSelectedItem().toString());
			Selected_crewDTO selected_crewDTO = new Selected_crewDTO();
			selected_crewDTO.setCrewMembers(strCrewMemId);
			selected_crewDTO.setServiceID_new(strServiceIdNew);
			selected_crewDTO.setLead_ID(empId);

			long val = houstonFlowFunctions
					.addSelectedCrewMember(selected_crewDTO);

			// Log.e("", "val..get."+val);
			logDTO.setCrew_Member("True");
		} else {
			JSONArray allCrewListArray = houstonFlowFunctions.getCrewMembers(
					DatabaseHelper.TABLE_CREW_MEMBER, strServiceIdNew);
			// Log.e("", "crewListArray...."+crewListArray.length());
			if (allCrewListArray.length() > 0) {
				String empId = houstonFlowFunctions.getEmpId(spnServicePerson
						.getSelectedItem().toString());

				Selected_crewDTO selected_crewDTO = new Selected_crewDTO();
				selected_crewDTO.setCrewMembers(strCrewMemId);
				selected_crewDTO.setServiceID_new(strServiceIdNew);
				selected_crewDTO.setLead_ID(empId);

				long val = houstonFlowFunctions
						.addSelectedCrewMember(selected_crewDTO);
			}
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_Camera_ACTIVITY_CODE) {
			if (resultCode == GeneralInformationActivity.RESULT_OK) {
				// imageCount++;
				Toast.makeText(GeneralInformationActivity.this,
						"Picture is  taken", Toast.LENGTH_SHORT);
			
				 tempStoreImageName.add(ImageName);
				 RefreshSaveImage();
				// getfile(ImageName);
				// LoggerUtil.e("File Name on done camera", ImageName + "");
			} else if (resultCode == GeneralInformationActivity.RESULT_CANCELED) {
				RefreshImage();
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
		private void RefreshSaveImage() {
		try {
			listImg.clear();
			Bitmap mutableBitmap = null;
			for (int i = 0; i < tempStoreImageName.size(); i++) {
				mutableBitmap = null;
				ImgDto dto = new ImgDto();
				try {
					LoggerUtil.e("Image Name in refresh",
							tempStoreImageName.get(i));
					mutableBitmap = decodeSmallUri(Uri.fromFile(CommonFunction
							.getFileLocation(GeneralInformationActivity.this,
									tempStoreImageName.get(i))));
					if(i == tempStoreImageName.size()-1)
					{
					mutableBitmap = mutableBitmap.copy(Bitmap.Config.ARGB_8888,
							true);
					mutableBitmap =  rotateImage(mutableBitmap, 90);
					Canvas cs = new Canvas(mutableBitmap);
					Paint tPaint = new Paint();
					tPaint.setTextSize(35);
					//#B43104
					tPaint.setColor(getResources().getColor(R.color.red_orange));
					tPaint.setStyle(Style.FILL);
					float height = tPaint.measureText("xX");
					cs.drawText(CommonFunction.getCurrentDateTimeForImage(),height+300f,
							height+1200f, tPaint);	
				    //Bitmap bitmap = scaleBitmap(mutableBitmap, 100, 100);
					}									
					dto.setImageBitMap(mutableBitmap);

					//get file
					File imagefile = CommonFunction.getFileLocation(
							GeneralInformationActivity.this, tempStoreImageName.get(i));
		            imagefile.delete();
					CommonFunction.saveBitmap(mutableBitmap,
								GeneralInformationActivity.this, tempStoreImageName.get(i));
                    
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if (!tempImagList.contains(tempStoreImageName.get(i))) {

					if (mutableBitmap != null) {

						// CompressImage(bmp);
					}
				}

				tempImagList.add(tempStoreImageName.get(i));
				listImg.add(dto);
			}
			// LoggerUtil.e("List Size", listImg.size() + "");

			if (listImg.size() == 0) {
				ImgDto dto = new ImgDto();
				dto.setImagedrawable(R.drawable.noimage);
				listImg.add(dto);
			} else {

			}
			adapter = new GalleryImageAdapter(getApplicationContext(), listImg);
			imageListView.setAdapter(adapter);
			// adapter.notifyDataSetChanged(); // TODO Auto-generated method
			// stub
		} catch (Exception e) {
			LoggerUtil.e("Exception in Refresh Image", e + "");
		}
	}
		public static Bitmap rotateImage(Bitmap source, float angle) {
		    Bitmap retVal;

		    Matrix matrix = new Matrix();
		    matrix.postRotate(angle);
		    retVal = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);

		    return retVal;
		}

	private void RefreshImage() {
		try {
			listImg.clear();
			Bitmap bmp = null;
			for (int i = 0; i < tempStoreImageName.size(); i++) {
				bmp = null;
				ImgDto dto = new ImgDto();
				try {
					LoggerUtil.e("Image Name in refresh",
							tempStoreImageName.get(i));
					/*bmp = decodeSmallUri(Uri.fromFile(CommonFunction
							.getFileLocation(GeneralInformationActivity.this,
									tempStoreImageName.get(i))));
                    */
					bmp = BitmapFactory.decodeFile(CommonFunction
							.getFileLocation(GeneralInformationActivity.this,
									tempStoreImageName.get(i)).toString());
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//Bitmap bitmap = scaleBitmap(bmp, 100, 100);
				dto.setImageBitMap(bmp);
				if (!tempImagList.contains(tempStoreImageName.get(i))) {

					if (bmp != null) {

						// CompressImage(bmp);
					}
				}

				tempImagList.add(tempStoreImageName.get(i));
				listImg.add(dto);
			}

			// LoggerUtil.e("List Size", listImg.size() + "");

			if (listImg.size() == 0) {
				ImgDto dto = new ImgDto();
				dto.setImagedrawable(R.drawable.noimage);
				listImg.add(dto);
			} else {

			}
			adapter = new GalleryImageAdapter(getApplicationContext(), listImg);
			imageListView.setAdapter(adapter);
			// adapter.notifyDataSetChanged(); // TODO Auto-generated method
			// stub
		} catch (Exception e) {
			LoggerUtil.e("Exception in Refresh Image", e + "");
		}
	}

	private Bitmap decodeSmallUri(Uri selectedImage)
			throws FileNotFoundException {
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(GeneralInformationActivity.this.getContentResolver().openInputStream(selectedImage), null, o);

		final int REQUIRED_SIZE = 500;

		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeStream(GeneralInformationActivity.this
				.getContentResolver().openInputStream(selectedImage), null, o2);
	}

	/*public void getfile(String fileName) {
		try {
			LoggerUtil.e("Get File Called", fileName);

			File imagefile = CommonFunction.getFileLocation(
					GeneralInformationActivity.this, fileName);
			if (imagefile.exists()) {
				// Bitmap myBitmap =
				// BitmapFactory.decodeFile(imagefile.getAbsolutePath());

				Bitmap myBitmap = decodeUri(Uri.fromFile(CommonFunction
						.getFileLocation(GeneralInformationActivity.this,
								fileName)));
				Bitmap mutableBitmap = myBitmap.copy(Bitmap.Config.ARGB_8888,
						true);

				Canvas cs = new Canvas(mutableBitmap);
				Paint tPaint = new Paint();
				tPaint.setTextSize(20);
				tPaint.setColor(Color.BLACK);
				tPaint.setStyle(Style.FILL);
				float height = tPaint.measureText("yY");
				cs.drawText(CommonFunction.getCurrentDateTime(), 20f,
						height + 15f, tPaint);	
			   
				Bitmap output = Bitmap.createScaledBitmap(mutableBitmap,
						800, 800, true);
				LoggerUtil.e("Out Put Bitmap width and heigth  ",
						mutableBitmap.getWidth() + " " + mutableBitmap.getWidth());

				
				imagefile.delete();
				CommonFunction.saveBitmap(output,
						GeneralInformationActivity.this, fileName);

			}
		} catch (Exception e) {

		}
	}

	private Bitmap decodeUris(Uri selectedImage) throws FileNotFoundException {
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(GeneralInformationActivity.this
				.getContentResolver().openInputStream(selectedImage), null, o);

		final int REQUIRED_SIZE = 800;

		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeStream(GeneralInformationActivity.this
				.getContentResolver().openInputStream(selectedImage), null, o2);
	}

	public static Bitmap scaleBitmap(Bitmap bitmapToScale, float newWidth,
			float newHeight) {
		if (bitmapToScale == null)
			return null;
		// get the original width and height
		int width = bitmapToScale.getWidth();
		int height = bitmapToScale.getHeight();
		// create a matrix for the manipulation
		Matrix matrix = new Matrix();

		// resize the bit map
		matrix.postScale(newWidth / width, newHeight / height);

		// recreate the new Bitmap and set it back
		return Bitmap.createBitmap(bitmapToScale, 0, 0,
				bitmapToScale.getWidth(), bitmapToScale.getHeight(), matrix,
				true);
	}
*/
	class OnGalleryClick implements OnItemLongClickListener {

		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
				int position, long arg3) {

			// Toast.makeText(getApplicationContext(),IWitnessActivity.position+
			// "  "+position, Toast.LENGTH_LONG).show();

			if (generalInfoDTO.getServiceStatus().equals("InComplete")) {
				if (listImg.size() > 0) {
					listImg.remove(position);
					if (tempImagList.size() > 0) {
						tempImagList.remove(position);
					}
					if (tempStoreImageName.size() > 0) {
						tempStoreImageName.remove(position);
					}
					RefreshImage();
					// alertboxClearGalleary("Confirmation",
					// "Are you want to remove selected image ?");
				}
			}

			return false;
		}

	}

	CharSequence[] charSequenceItems;
	List<String> MandatoryList = new ArrayList<String>();

	public void getValuesFromComponents() {
		// TODO Auto-generated method stub
		strFirstName = edtFirstName.getText().toString();
		strLastName = edtLastName.getText().toString();
		if (checkboxNoEmail.isChecked()) {
			strEmail = "".trim();
		} else {
			strEmail = edtEmailId.getText().toString().trim();
		}
		strServiceAttribute = edtServiceAttribute.getText().toString();
		strResetDetail = edtResetDetail.getText().toString();
		strHomePhone = edtHomePhone.getText().toString();
		strCellphone = edtCellPhone.getText().toString();
		strAccountnumber = edtAccountNo.getText().toString();
		strServiceTechNumber = edtServiceTechNumber.getText().toString();
		strdateofService = edtdateofService.getText().toString();
		strtimeofService = edttimeofService.getText().toString();
		strRouteName = edtRouteName.getText().toString();
		strRouteNumber = edtRouteNumber.getText().toString();
		strOrderNo = edtOrderNo.getText().toString();
		strKeymap = edtKeymap.getText().toString();
		strServiceCode = edtServiceCode.getText().toString();
		strDepartment = edtDepartment.getText().toString();
		strCompanyName = edtCompanyName.getText().toString();
		strNotes = edtNotes.getText().toString();
		strServiceDescription = edtServiceDescription.getText().toString();
		strOtherInstruction = edtOtherInstruction.getText().toString();
		strSpecialinstructions = edtSpecialinstructions.getText().toString();
		// strServiceAddress=edtServiceAddress.getText().toString();
		// strBillingAddress=edtBillingAddress.getText().toString();
		strTimeInDateTime = edtTimein.getText().toString();
		strStatus = spnStatus.getSelectedItem().toString();
		// Log.e("", "after status..."+strStatus);
		strServiceStatusReason = spnStatusReason.getSelectedItem().toString();
		strTaxValue = generalInfoDTO.getTax_value();
		strtaxCode = generalInfoDTO.getTaxCode();

		strServiceAddress1 = edtServiceAddressLine1.getText().toString();
		strServiceAdderss2 = edtServiceAddressLine2.getText().toString();
		strServiceCity = edtServiceCity.getText().toString();
		strServiceState = edtServiceState.getText().toString();
		strServiceZipCode = edtServiceZipCode.getText().toString();

		// for Billing Address information
		strBillAddress1 = edtBillAddressLine1.getText().toString();
		strBillAddress2 = edtBillAddressLine2.getText().toString();
		strBillCity = edtBillCity.getText().toString();
		strBillState = edtBillState.getText().toString();
		strBillZipCode = edtBillZipCode.getText().toString();
	}

	class OnSpinerItemSelect implements OnItemSelectedListener {
		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int positon, long id) {
			switch (parent.getId()) {
			case R.id.spnStatus:
				strServicestatus = parent.getSelectedItem().toString();
				// iCurrentSelection=spnResetReason.getSelectedItemPosition();

				// Log.e("", "inside on options item select"+strServicestatus);

				if (strServicestatus.equals("Reset")) {
					resetRow.setVisibility(View.VISIBLE);
					resetDetailRow.setVisibility(View.VISIBLE);
					// btnReOpen.setVisibility(View.VISIBLE);
					edtResetDetail.requestFocus();
					strServiceStatusReason = "";
				} else {
					strResetReason = "";
					resetDetailRow.setVisibility(View.GONE);
					resetRow.setVisibility(View.GONE);
				}
				break;
			/*
			 * case R.id.spinnerServicePerson: String item =
			 * spinnerServicePerson.getSelectedItem().toString(); int
			 * indexOfEmploy
			 * =CommanUtilities.getemployeeNameList(getActivity()).
			 * indexOf(item); ArrayList<String> PesPEckIdList=new
			 * ArrayList<String>();
			 * PesPEckIdList=CommanUtilities.getemployeePestPeckList
			 * (getActivity()); String
			 * strPestpeckid=PesPEckIdList.get(indexOfEmploy);
			 * edtServiceTech.setText(strPestpeckid); break;
			 */
			case R.id.spnServiceStatusReason:
				/*
				 * if (iCurrentSelection != positon) {
				 * if(!spnResetReason.getSelectedItem
				 * ().toString().equals("Select")) {
				 * showTechnicianCommentBoxDialog(); // Your code here } }
				 * iCurrentSelection = positon;
				 * strResetReason=spnResetReason.getSelectedItem().toString();
				 */
				break;
			default:
				break;
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			return;
		}
	}

	private final Dialog createMessageList(final CharSequence[] messageList,
			final EditText object) {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				GeneralInformationActivity.this);
		builder.setTitle(getResources()
				.getString(R.string.REQUIRED_INFORMATION));
		builder.setItems(charSequenceItems,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// LoggerUtil.e("","E' stato premuto il pulsante: "+messageList[whichButton]);
					}
				});
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				if (object == edtTimein) {
					object.requestFocus();
				} else {
					// spnStatusReason.requestFocus();
					// spnStatusReason.performClick();
				}
			}
		});
		return builder.show();
	}

	public Boolean checkValidations() {
		// TODO Auto-generated method stub
		isValidate = true;
		/*
		 * if(!edtCellPhone.getText().toString().trim().equals("")) { String
		 * CellPhone = edtCellPhone.getText().toString().trim();
		 * if(CellPhone.length()==12) { Character a = CellPhone.charAt(3); char
		 * b = CellPhone.charAt(7); //Log.e("", "CellPhone..."+a+b); if(a!='-'
		 * || b!='-' || CellPhone.length()!=12 ) {
		 * MandatoryList.add("Cell Phone format should be: 123-456-7890");
		 * isValidate =false; } } else if(CellPhone.length()<12) {
		 * MandatoryList.add("Cell Phone format should be: 123-456-7890");
		 * isValidate =false; } else if(CellPhone.length()==12 &&
		 * (CellPhone.charAt(1)=='-' || CellPhone.charAt(2)=='-' ||
		 * CellPhone.charAt(4)=='-' || CellPhone.charAt(5)=='-' ||
		 * CellPhone.charAt(6)=='-' || CellPhone.charAt(8)=='-' ||
		 * CellPhone.charAt(9)=='-' || CellPhone.charAt(10)=='-' ||
		 * CellPhone.charAt(11)=='-' || CellPhone.charAt(12)=='-')) {
		 * MandatoryList.add("Cell Phone format should be: 123-456-7890");
		 * isValidate =false; } else { isValidate =true; } }
		 * if(!edtHomePhone.getText().toString().trim().equals("")) { String
		 * HomePhone = edtHomePhone.getText().toString().trim();
		 * if(HomePhone.length()==12) { Character a = HomePhone.charAt(3); char
		 * b = HomePhone.charAt(7); //Log.e("", "CellPhone..."+a+b); if(a!='-'
		 * || b!='-' || HomePhone.length()!=12 ) {
		 * MandatoryList.add("Home Phone format should be: 123-456-7890");
		 * isValidate =false; } } else if(HomePhone.length()<12) {
		 * MandatoryList.add("Home Phone format should be: 123-456-7890");
		 * isValidate =false; } else if(HomePhone.charAt(1)=='-' ||
		 * HomePhone.charAt(2)=='-' || HomePhone.charAt(4)=='-' ||
		 * HomePhone.charAt(5)=='-' || HomePhone.charAt(6)=='-' ||
		 * HomePhone.charAt(8)=='-' || HomePhone.charAt(9)=='-' ||
		 * HomePhone.charAt(10)=='-' || HomePhone.charAt(11)=='-' ||
		 * HomePhone.charAt(12)=='-') {
		 * MandatoryList.add("Cell Phone format should be: 123-456-7890");
		 * isValidate =false; } else { isValidate =true; } }
		 */
		if (!strStatus.equals("Reset")) {
			if (edtTimein.getText().toString().equals("")) {
				MandatoryList.add("Start Time Required");
				objectFocus = edtTimein;
				isValidate = false;

			}
		}
		if (strStatus.equals("Reset")) {
			if (strServiceStatusReason.equals("Select")) {
				MandatoryList.add("Select Service status reason");
				// objectFocus=edtTimein;
				isValidate = false;

			}
		}

		/*
		 * if( edtFirstName.getText().toString().equals("") &&
		 * edtCompanyName.getText().toString().equals("")) {
		 * MandatoryList.add("Enter First Name or Company Name"); isValidate
		 * =false; } if(!edtFirstName.getText().toString().equals("") &&
		 * edtLastName.getText().toString().equals("")) {
		 * MandatoryList.add("Enter Last Name"); isValidate =false; }
		 * if(edtAccountNo.getText().toString().equals("")) {
		 * MandatoryList.add("Enter Account Number"); isValidate =false; }
		 * if(edtServiceTechNumber.getText().toString().equals("")) {
		 * MandatoryList.add("Enter Service Tech Number"); isValidate =false; }
		 * if(edtdateofService.getText().toString().equals("")) {
		 * MandatoryList.add("Enter Date of Service"); isValidate =false; }
		 * if(edttimeofService.getText().toString().equals("")) {
		 * MandatoryList.add("Enter Service Time"); isValidate =false; }
		 * if(edtRouteName.getText().toString().equals("")) {
		 * MandatoryList.add("Enter Route Name"); isValidate =false; }
		 * if(edtRouteNumber.getText().toString().equals("")) {
		 * MandatoryList.add("Enter Route Number"); isValidate =false; }
		 * if(edtOrderNo.getText().toString().equals("")) {
		 * MandatoryList.add("Enter Order Number"); isValidate =false; }
		 * if(edtServiceCode.getText().toString().equals("")) {
		 * MandatoryList.add("Enter Service Code"); isValidate =false; }
		 * if(edtDepartment.getText().toString().equals("")) {
		 * MandatoryList.add("Enter Department"); isValidate =false; }
		 * 
		 * 
		 * */
		
		if (edtEmailId.getText().toString().equals("")
				&& !checkboxNoEmail.isChecked()) {
			MandatoryList.add("Enter E-mail Id");
			isValidate = false;
		}
		if (!edtEmailId.getText().toString().equals("")) {
			if (eMailValidation(edtEmailId.getText().toString())) {

			} else {

				MandatoryList.add("Email Id is Wrong Format");
				isValidate = false;
				return false;
			}
		}
		return isValidate;
	}

	public static boolean eMailValidation(String emailstring) {
		Pattern emailPattern = Pattern.compile(".+@.+\\.[a-z]+");
		Matcher emailMatcher = emailPattern.matcher(emailstring);
		return emailMatcher.matches();
	}

	public void setValuestoDTO() {
		// TODO Auto-generated method stub
		strmultipleImageName = "";

		for (int i = 0; i < tempStoreImageName.size(); i++) {
			strmultipleImageName += tempStoreImageName.get(i).toString().trim()
					+ ",";
			// /Log.e("strmultipleImageName","strmultipleImageName"+strmultipleImageName);
		}
		if (strmultipleImageName.length() > 0) {
			strmultipleImageName = strmultipleImageName.substring(0,
					strmultipleImageName.length() - 1);
			// strmultipleImageName=strmultipleImageName.trim();
			Log.e("strmultipleImageName 2", "strmultipleImageName 2"
					+ strmultipleImageName);
		}
		if (CommonFunction.checkString(generalInfoDTO.getExpirationDate(), "")
				.equals("")) {
			generalInfoDTO.setExpirationDate("01/01/1900");
		}
		
		generalInfoDTO.setNewEmail(strNewEmail);
		generalInfoDTO.setService_Attribute(strServiceAttribute);
		generalInfoDTO.setResetComment(strResetDetail);
		generalInfoDTO.setRef_Source(spnHowDidUHear.getSelectedItem().toString());
		generalInfoDTO.setTimeIn(strTimeIn);
		generalInfoDTO.setPrv_Bal("0");
		generalInfoDTO.setTimeInDateTime(strTimeInDateTime);
		generalInfoDTO.setBeforeImage(strmultipleImageName);
		generalInfoDTO.setFirst_Name(strFirstName);
		generalInfoDTO.setLast_Name(strLastName);
		generalInfoDTO.setEmail_Id(generalInfoDTO.getEmail_Id());
		generalInfoDTO.setCell_Phone(strCellphone);
		generalInfoDTO.setHome_Phone(strHomePhone);
		generalInfoDTO.setAccountNo(strAccountnumber);
		generalInfoDTO.setService_Tech_Num(strServiceTechNumber);
		generalInfoDTO.setServiceDate(strdateofService);
		generalInfoDTO.setServiceTime(strtimeofService);
		generalInfoDTO.setRoute_Name(strRouteName);
		generalInfoDTO.setRoute_Number(strRouteNumber);
		generalInfoDTO.setOrder_Number(strOrderNo);
		generalInfoDTO.setKeyMap(strKeymap);
		generalInfoDTO.setServiceCode(strServiceCode);
		generalInfoDTO.setDepartment(strDepartment);
		generalInfoDTO.setCompanyName(strCompanyName);
		generalInfoDTO.setNotes(strNotes);
		generalInfoDTO.setServiceDescription(strServiceDescription);
		generalInfoDTO.setOtherInstruction(strOtherInstruction);
		generalInfoDTO.setSpecialInstruction(strSpecialinstructions);
		// generalInfoDTO.setServiceAddress(strServiceAddress);
		// generalInfoDTO.setBAddress_Line1(bAddress_Line1)
		generalInfoDTO.setServiceStatus(strStatus);
		generalInfoDTO.setService_Status_Reason(strServiceStatusReason);
		// generalInfoDTO.setServicePerson(strServicePerson);
		generalInfoDTO.setServiceID_new(generalInfoDTO.getServiceID_new());
		generalInfoDTO.setAddress_Line1(generalInfoDTO.getAddress_Line1());
		generalInfoDTO.setAddress_Line2(generalInfoDTO.getAddress_Line2());
		generalInfoDTO.setCity(generalInfoDTO.getCity());
		generalInfoDTO.setState(generalInfoDTO.getState());
		generalInfoDTO.setZipCode(generalInfoDTO.getZipCode());
		generalInfoDTO.setBAddress_Line1(generalInfoDTO.getBAddress_Line1());
		generalInfoDTO.setBAddress_Line2(generalInfoDTO.getBAddress_Line2());
		generalInfoDTO.setCity(generalInfoDTO.getCity());
		generalInfoDTO.setState(generalInfoDTO.getState());
		generalInfoDTO.setZipCode(generalInfoDTO.getZipCode());
		generalInfoDTO.setAddress_Line1(strServiceAddress1);
		generalInfoDTO.setAddress_Line2(strServiceAdderss2);
		generalInfoDTO.setCity(strServiceCity);
		generalInfoDTO.setState(strServiceState);
		generalInfoDTO.setZipCode(strServiceZipCode);
		generalInfoDTO.setBAddress_Line1(strBillAddress1);
		generalInfoDTO.setBAddress_Line2(strBillAddress2);
		generalInfoDTO.setBCity(strBillCity);
		generalInfoDTO.setBState(strBillState);
		generalInfoDTO.setBZipCode(strBillZipCode);
		if (CommonFunction.checkString(generalInfoDTO.getTax_value(), "").equals("")) {
			generalInfoDTO.setTax_value("0");
		} else {
			generalInfoDTO.setTax_value(generalInfoDTO.getTax_value());
		}
		generalInfoDTO.setCreate_Date(CommonFunction.getCurrentDateTime());
		generalInfoDTO.setUpdate_Date(CommonFunction.getCurrentDate());
		generalInfoDTO.setIsElectronicSignatureAvailable("false");
		generalInfoDTO.setOpenWONotificationFlag("false");
		generalInfoDTO.setIsCompleted("true");
		generalInfoDTO.setTaxCode(strtaxCode);
		generalInfoDTO.setCreate_By(userDTO.getPestPackId());
		generalInfoDTO.setCheckNo(generalInfoDTO.getCheckNo());
		generalInfoDTO.setServiceClass(generalInfoDTO.getServiceClass());
		// generalInfoDTO.setCreate_Date(userDTO.getCreate_Date());
	}

	private void createLogOutAlert() {
		// TODO Auto-generated method stub
		new AlertDialog.Builder(this)
				.setIcon(android.R.drawable.ic_dialog_alert)
				.setTitle("Exit")
				.setMessage("Are you sure want to Logout?")
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								SharedPreference.setSharedPrefer(
										getApplicationContext(),
										SharedPreference.LOGOUT_PREF, "logout");
								finish();
								System.exit(0);
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						SharedPreference.setSharedPrefer(
								getApplicationContext(),
								SharedPreference.LOGOUT_PREF, "notlogout");
						dialog.dismiss();
					}
				}).show();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		generalInfoDTO = new Gson().fromJson(houstonFlowFunctions
				.getSelectedService(strServiceIdNew).toString(),
				GeneralInfoDTO.class);
		// Log.e("onResume called","");
		super.onResume();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onRestart()
	 */
	/*
	 * @Override protected void onRestart() { // TODO Auto-generated method stub
	 * Log.e("onRestart calleed",""); generalInfoDTO = new
	 * Gson().fromJson(houstonFlowFunctions
	 * .getSelectedService(strServiceIdNew).toString(), GeneralInfoDTO.class);
	 * 
	 * super.onRestart(); } (non-Javadoc)
	 * 
	 * @see android.app.Activity#onResume()
	 * 
	 * @Override protected void onResume() { // TODO Auto-generated method stub
	 * generalInfoDTO = new
	 * Gson().fromJson(houstonFlowFunctions.getSelectedService
	 * (strServiceIdNew).toString(), GeneralInfoDTO.class);
	 * Log.e("onResume called",""); super.onResume(); }
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onBackPressed()
	 */
	/*
	 * @Override public void onBackPressed() { // TODO Auto-generated method
	 * stub super.onBackPressed(); Intent i = new
	 * Intent(GeneralInformationActivity.this,ServiceListAcitivity.class);
	 * i.putExtra("userDTO", ServiceListAcitivity.userDTO); //i.putExtra("back",
	 * "general"); startActivity(i); finish(); }
	 */

}
